using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Constants {
	private static Constants _instance;
	public static Constants instance {
		get {
			if (_instance == null) {
				_instance = new Constants();
			}
			return _instance;
		}
	}
	
	/// <summary>判定円の半径</summary>
	public float MAIMAI_OUTER_RADIUS { get { return 170; } }
	/// <summary>ノート移動開始位置は、中心からどのくらい空けるか</summary>
	public float NOTE_MOVE_START_MARGIN { get { return 50; } }
	/// <summary>スライドマーカーの描画間隔</summary>
	public float SLIDE_MARKER_TO_MARKER_INTERVAL { get { return 18; } }
	/// <summary>スライドマーカーの置き始めと置き終わりのマージン</summary>
	public float SLIDE_MARKER_TO_SENSOR_MARGIN { get { return 15; } }
	/// <summary>センサーの半径</summary>
	public float SENSOR_RADIUS { get { return 70; } }
		
	//内周センサーのノート基準値
	public float MAIMAI_INNER_RADIUS {
		get {
			var clossExLines = CircleCalculator.linesIntersect_st(GetOuterPieceAxis(0), GetOuterPieceAxis(3), GetOuterPieceAxis(6), GetOuterPieceAxis(2)); //1-4と7-3の交点
			return CircleCalculator.pointToPointDistance_st(this.centerAxis, clossExLines); //中心から交点までの距離を内周円上点の半径とする
		}
	}
	
	//中心からみたセンサーピースの角度 (上が0度)
	public float GetPieceDegree(int buttonNumber) {
		float numberOfSensor = 8.0f;
		return (360.0f / numberOfSensor) * buttonNumber + (360.0f / numberOfSensor / 2.0f);
	}

	//外周センサーピース位置
	public Vector2 GetOuterPieceAxis = function(int buttonNumber) {
		return CircleCalculator.pointOnCircle_prd(new Vector2(), MAIMAI_OUTER_RADIUS, GetPieceDegree(buttonNumber));
	}
	
	//内周センサーピース位置
	public Vector2 GetInnerPieceAxis(int buttonNumber) {
		return CircleCalculator.pointOnCircle_prd(new Vector2(), MAIMAI_INNER_RADIUS, GetPieceDegree(buttonNumber));
	}
	
	
}