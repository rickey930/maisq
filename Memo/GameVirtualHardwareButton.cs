using UnityEngine;
using System.Collections;

public class GameVirtualHardwareButton : MonoBehaviour {
	/*
	ボタンが指IDを覚える.
	さっきまでない指が触れていたらpush.
	さっきまで触れていた指がなければpull.
	指が1つでもあればhold.
	指が1つもなければfree.

	アップデートに頼るのは危ない.
	外部からCheck関数を呼ぶ.

	ポーズ明けは、pushにはならず、pullにはなる.
	さっきまで触れていた指がなければpull.
	指が1つでもあればhold.
	*/

	public Collider2D collision;
	
	private Hashset<int> _fingerIDs;
	private Hashset<int> fingerIDs { get { if (_fingerIDs == null) { _fingerIDs = new Hashset<int> (); } return _fingerIDs; } }
	public bool push { get; protected set; }
	public bool pull { get; protected set; }
	public bool hold { get; protected set; }
	public bool free { get { return !hold; } }
	public float holdTime { get; protected set; }
	public float freeTime { get; protected set; }
	public void Check () { Check (false); }
	// pauseFitr: ポーズ明け.
	public void Check (bool pauseFitr) {
		if (Input.touchSupported) {
			bool _push = false;
			bool _pull = false;
			Hashset<int> deltaTouch = new Hashset<int> ();
			foreach (Touch touch in Input.touches) {
				Vector2 tapPoint = Camera.main.ScreenToWorldPoint (touch.position);
				Collider2D collider = Physics2D.OverlapPoint (tapPoint);
				if (collider != null && collider == collision) {
					if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled) {
						if (fingerIDs.Add (touch.fingerId)) {
							if (!pauseFitr) {
								_push = true;
							}
						}
						deltaTouch.Add (touch.fingerId);
					}
				}
			}
			foreach (int id in fingerIDs) {
				if (!deltaTouch.Countains (id)) {
					fingerIDs.Remove (id);
					_pull = true;
				}
			}
		}
		push = _push;
		pull = _pull;
		hold = fingerIDs.Count > 0;
		
		if (hold) {
			holdTime += Time.deltaTime;
			freeTime = 0.0f;
		}
		else {
			freeTime += Time.deltaTime;
			holdTime = 0.0f;
		}
	}
}

