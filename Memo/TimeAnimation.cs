using UnityEngine;
using System.Collections;
using UnityEditor;

public class TimeAnimation : MonoBehaviour {
	[System.Serializable]
	public class TimeAnimationData {
		public float time;
		public Vector3Control translate;
		public Vector3Control rotate;
		public Vector3Control scale;
		
		[System.Serializable]
		public class Vector3Control {
//			[System.NonSerialized]
			public Vector3 start;
			public Vector3 target;
			public PercentUnlimitedFlag unlimited;
			
			[System.Serializable]
			public class PercentUnlimitedFlag {
				public bool under;
				public bool over;
			}
		}

		public void GetValues(float percent, out Vector3 translateValue, out Vector3 rotateValue, out Vector3 scaleValue) {
			var ctrls = new Vector3Control[] {
				translate, rotate, scale,
			};
			var results = new Vector3[ctrls.Length];

			for (int i = 0; i < ctrls.Length; i++) {
				var ctrl = ctrls[i];
				float rate = percent;
				if (!ctrl.unlimited.under && rate < 0) rate = 0;
				if (!ctrl.unlimited.over && rate > 1) rate = 1;
				Vector3 value = ctrl.start + ctrl.target * percent;
				results[i] = value;
			}
			translateValue = results[0];
			rotateValue = results[1];
			scaleValue = results[2];
		}
	}

	///<summary>あと何回ループするか. 負の数で無限にループ</summary>
	[SerializeField]
	private int loop = -1;
//	///<summary>初期値は前回の目標値を受け継ぎ、連続アニメーションにする</summary>
//	[SerializeField]
//	private bool continuity;
	///<summary>ループ終了時に毎回実行されるイベント</summary>
	[SerializeField]
	private EventDelegate loopEndEvent;
	///<summary>アニメーションが終わると実行されるイベント</summary>
	[SerializeField]
	private EventDelegate animationEndEvent;
	[SerializeField]
	private TimeAnimationData[] data;
	public TimeAnimationData[] GetData { get { return data; } }
	private float timer;
	private int index;

	private void Start () {
//		if (continuity) {
//			data[0].translate.start = transform.localPosition;
//			data[0].rotate.start = transform.localRotation.eulerAngles;
//			data[0].scale.start = transform.localScale;
//			for (int i = 1; i < data.Length; i++) {
//				data[i].translate.start = data[i - 1].translate.start + data[i - 1].translate.target;
//				data[i].rotate.start = data[i - 1].rotate.start + data[i - 1].rotate.target;
//				data[i].scale.start = data[i - 1].scale.start + data[i - 1].scale.target;
//			}
//		}
	}
	
	private void Update () {
		if (index >= data.Length) {
			bool end = false;
			if (loop > 0 || loop < 0) {
				if (loopEndEvent != null) loopEndEvent.Execute();
				if (loop > 0) loop--;
				if (loop == 0) {
					end = true;
				}
				else {
					index = 0;
				}
			}
			else {
				end = true;
			}
			if (end) {
				if (animationEndEvent != null) animationEndEvent.Execute();
				return;
			}
		}
		
		var info = data[index];
		
		float percent = timer / info.time;
		var values = new Vector3[3];
		info.GetValues (percent, out values [0], out values [1], out values [2]);
		transform.localPosition = values[0];
		transform.localRotation = Quaternion.Euler(values[1]);
		transform.localScale = values[2];
		
		timer += Time.deltaTime;
		if (timer >= info.time) {
			timer = 0;
			index++;
		}
	}
}

[CustomEditor(typeof(TimeAnimation))]
public class TimeAnimationDataEditor : Editor {
	
	private int index;
	
	public override void OnInspectorGUI () {
		serializedObject.Update ();
		EditorGUI.BeginChangeCheck ();
		
		var obj = this.target as TimeAnimation;
		
		base.OnInspectorGUI ();
		
		if (GUILayout.Button ("Continuity")) {
			var data = obj.GetData;
			if (data != null && data.Length > 0) {
				data[0].translate.start = obj.transform.localPosition;
				data[0].rotate.start = obj.transform.localRotation.eulerAngles;
				data[0].scale.start = obj.transform.localScale;
				for (int i = 1; i < data.Length; i++) {
					data[i].translate.start = data[i - 1].translate.start + data[i - 1].translate.target;
					data[i].rotate.start = data[i - 1].rotate.start + data[i - 1].rotate.target;
					data[i].scale.start = data[i - 1].scale.start + data[i - 1].scale.target;
				}
			}
		}
		
		if (EditorGUI.EndChangeCheck ()) {
			EditorUtility.SetDirty (target);
		}
	}
}