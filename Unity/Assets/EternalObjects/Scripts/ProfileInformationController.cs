﻿using UnityEngine;
using System.Collections;

public class ProfileInformationController : SingletonMonoBehaviour<ProfileInformationController> {
	public string player_name;
	public string player_title;
	public Color player_title_color;
	public string player_level;
	public float player_exp_rate;
	public float player_avater_tap_ability;
	public float player_avater_hold_ability;
	public float player_avater_slide_ability;
	public float player_avater_break_ability;
	public Sprite player_icon;
	public Sprite player_plate;
	public Sprite player_frame;
	public Sprite default_player_icon;
	public Sprite default_player_plate;
	public Sprite default_player_frame;
	public bool loaded_player_icon { get; private set; }
	public bool loaded_player_plate { get; private set; }
	public bool loaded_player_frame { get; private set; }

	public ProfileNameController profileNameCtrl { get; set; }
	public ProfileTitleController profileTitleCtrl { get; set; }
	public ProfileIconController profileIconCtrl { get; set; }
	public ProfilePlateController profilePlateCtrl { get; set; }
	public ProfileFrameController profileFrameCtrl { get; set; }
	public ProfileLevelController profileLevelCtrl { get; set; }
	public ProfileAvaterAbilityController profileAvaterAbilityCtrl { get; set; }

	IEnumerator Start () {
		while (UserDataController.instance == null) {
			yield return null;
		}
		PlayerNameReload ();
		PlayerTitleReload ();
		PlayerLevelReload ();
		PlayerAvaterAbilityReload ();
		PlayerIconReload ();
		PlayerPlateReload ();
		PlayerFrameReload ();
	}

	public void PlayerNameReload () {
		var user = UserDataController.instance.user;
		player_name = user.user_name;
		if (profileNameCtrl != null)
			profileNameCtrl.StartCustom ();
	}
	
	public void PlayerTitleReload () {
		var user = UserDataController.instance.user;
		player_title = user.user_title;
		player_title_color = new Color (user.user_title_color_r, user.user_title_color_g, user.user_title_color_b, 1.0f);
		if (profileTitleCtrl != null)
			profileTitleCtrl.StartCustom ();
	}

	public void PlayerLevelReload () {
		var user = UserDataController.instance.user;
		player_level = user.user_level_info.level.ToString ();
		player_exp_rate = user.user_level_info.GetExperienceRate ();
		if (profileLevelCtrl != null) 
			profileLevelCtrl.StartCustom ();
	}
	
	public void PlayerAvaterAbilityReload () {
		var user = UserDataController.instance.user;
		player_avater_tap_ability = user.avater_ability.average.tap_ability;
		player_avater_hold_ability = user.avater_ability.average.hold_ability;
		player_avater_slide_ability = user.avater_ability.average.slide_ability;
		player_avater_break_ability = user.avater_ability.average.break_ability;
		if (profileAvaterAbilityCtrl != null) 
			profileAvaterAbilityCtrl.StartCustom ();
	}

	public void PlayerIconReload () {
		loaded_player_icon = false;
		var user = UserDataController.instance.user;
		StartCoroutine (UserDataController.instance.LoadPlayerIcon (user.user_icon_extension, (data) => {
			if (data != null) {
				player_icon = data;
			}
			else {
				player_icon = default_player_icon;
			}
			loaded_player_icon = true;
			if (profileIconCtrl != null)
				profileIconCtrl.StartCustom ();
		}));
	}
	
	public void PlayerPlateReload () {
		loaded_player_plate = false;
		var user = UserDataController.instance.user;
		StartCoroutine (UserDataController.instance.LoadPlayerPlate (user.user_plate_extension, (data) => {
			if (data != null) {
				player_plate = data;
			}
			else {
				player_plate = default_player_plate;
			}
			loaded_player_plate = true;
			if (profilePlateCtrl != null)
				profilePlateCtrl.StartCustom ();
		}));
	}
	
	public void PlayerFrameReload () {
		loaded_player_frame = false;
		var user = UserDataController.instance.user;
		StartCoroutine (UserDataController.instance.LoadPlayerFrame (user.user_frame_extension, (data) => {
			if (data != null) {
				player_frame = data;
			}
			else {
				player_frame = default_player_frame;
			}
			loaded_player_frame = true;
			if (profileFrameCtrl != null)
				profileFrameCtrl.StartCustom ();
		}));
	}
}
