﻿using UnityEngine;
using System.Collections;

public class SummonEternalObjectController : MonoBehaviour {

	public GameObject eternalObject;

	// Use this for initialization
	void Awake () {
		if (GameObject.Find ("Eternal Object") == null) {
			GameObject createObj = Instantiate (eternalObject) as GameObject;
			createObj.name = "Eternal Object";
		}
	}
}
