﻿using UnityEngine;
using System.Collections;

public class ButtonEffectObjectController : MonoBehaviour {
	[SerializeField]
	private Transform rotateHierarchy;
	[SerializeField]
	private Transform translateHierarchy;
	
	private int buttonNumber;

	public void Setup (int buttonNumber) {
		this.buttonNumber = buttonNumber;
	}

	// Use this for initialization
	void Start () {
		rotateHierarchy.localRotation = Quaternion.Euler(0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(Constants.instance.GetPieceDegree(buttonNumber)));
		translateHierarchy.localPosition = new Vector3 (0, Constants.instance.MAIMAI_OUTER_RADIUS, 0);
		var renderer = GetComponentInChildren<SpriteRenderer> ();
		renderer.sprite = MaipadDynamicCreatedSpriteTank.instance.buttonTapCircleEffect;
	}
}
