﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CircleGraph : MonoBehaviour {

	[System.Serializable]
	public class GraphData {
		public float value;
		public Color color;
		public GraphData () {}
		public GraphData (float value, Color color) { 
			this.value = value;
			this.color = color;
		}
	}
	
	public Transform roulette;
	public GameObject plate;
	
	public GraphData[] graphDataList;
	
	protected virtual void Start()
	{
		Init();
	}
	
	public void Reset()
	{
		Init();
	}
	
	public void Show()
	{
		StartCoroutine(ShowAnim());
	}
	
	// 円グラフが表示される演出.
	private IEnumerator ShowAnim()
	{
		bool flg = true;
		roulette.GetComponent<Image>().fillAmount = 0;
		float speed = 0.05f;
		while(flg)
		{
			roulette.GetComponent<Image>().fillAmount += speed;
			if(roulette.GetComponent<Image>().fillAmount >= 1) flg = false;
			yield return new WaitForSeconds(0.01f);
		}
	}
	
	private void Init()
	{
		// 既に作成したグラフがあれば削除する.
		foreach(Transform t in roulette)
		{
			if(t != plate.transform) Destroy (t.gameObject);
		}
		float max = 1.0f; // グラフの比率 1が最大　ここからどんどん引いていく.
		for(int i = 0; i < graphDataList.Length; i++)
		{
			//if(max <= 0) break;
			// Plateをコピーしてサイズなどを調節.
			GameObject plateCopy = Instantiate(plate) as GameObject;
			plateCopy.transform.SetParent(roulette);
			plateCopy.transform.localPosition = Vector3.zero;
			plateCopy.transform.localScale = Vector3.one;

			// zの角度を設定.
			plateCopy.transform.localEulerAngles = new Vector3(0, 0, (1.0f - (float)max) * -360f);
			
			// 円のサイズをfillAmountに設定.
			plateCopy.GetComponent<Image>().fillAmount = graphDataList[i].value;

			plateCopy.GetComponent<Image>().color = graphDataList[i].color;
			plateCopy.SetActive(true);
			max -= graphDataList[i].value;
			
		}
		roulette.GetComponent<Image>().fillAmount = 1;
	}
}
