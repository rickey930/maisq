﻿using UnityEngine;
using System.Collections;

public class ExpBarController : MonoBehaviour {

	[SerializeField]
	private RectTransform barBorder;
	[SerializeField]
	private RectTransform barBase;
	[SerializeField]
	private RectTransform overBar;
	
	private float size { get; set; }
	
	float progress { get; set; }
	float? oldProgress { get; set; }
	public void SetProgress (float progress) {
		this.progress = progress;
	}
	
	// Use this for initialization
	void Start () {
		size = barBase.sizeDelta.x;
	}
	
	// Update is called once per frame
	void Update () {
		if (!oldProgress.HasValue || oldProgress.Value != progress) {
			float value = progress * size;
			var barsize2 = overBar.sizeDelta;
			barsize2.x = value;
			overBar.sizeDelta = barsize2;
			oldProgress = progress;
		}
	}
}
