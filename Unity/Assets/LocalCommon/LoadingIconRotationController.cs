﻿using UnityEngine;
using System.Collections;

public class LoadingIconRotationController : MonoBehaviour {

	private float z;
	public float rotateSpeed;
	// Update is called once per frame
	void Update () {
		transform.localRotation = Quaternion.Euler (0, 0, -z);
		z += rotateSpeed * Time.deltaTime;
		if (z > 360) z -= 360;
	}
}
