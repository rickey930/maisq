﻿using UnityEngine;
using System.Collections;

public class MaimaiSensor : GameVirtualHardwareButton {
	public Transform rotateHierarchy;
	public Transform positionHierarchy;
	public CircleCollider2D GetSensorCollider() {
		return (CircleCollider2D)buttonCollider;
	}

	public void Setup(Vector2 pos, float deg) {
		rotateHierarchy.localRotation = Quaternion.Euler (0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(deg));
		positionHierarchy.localPosition = pos;
		GetSensorCollider ().radius = Constants.instance.SENSOR_RADIUS;
	}

	public void SetupButtonNames(params string[] names) {
		if (names != null) {
			copeGamePadButtonNames = names;
		}
	}
}
