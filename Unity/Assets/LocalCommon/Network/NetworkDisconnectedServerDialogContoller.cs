﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NetworkDisconnectedServerDialogContoller : MonoBehaviour {
	[SerializeField]
	private GameObject content;
	[SerializeField]
	private Text label;

	private const string disconnectedHost = "Disconnected from host.\nReturn to title.";
	private const string disconnectedClient = "Disconnected from client.\nReturn to title.";

	// サーバーが初期化されたメッセージ.
	void OnServerInitialized () {
		content.SetActive (false);
	}

	// サーバーとの通信が切断されたメッセージ
	void OnDisconnectedFromServer () {
		if (!content.activeSelf) {
			label.text = disconnectedHost;
			content.SetActive (true);
		}
	}

	// クライアントとの通信が切断されたメッセージ.
	void OnPlayerDisconnected () {
		if (!content.activeSelf) {
			label.text = disconnectedClient;
			content.SetActive (true);
		}
	}

	public void OnOkButtonClick () {
		AudioManagerLite.Pause ();
		Application.LoadLevel ("SceneTitle");
	}
}
