﻿using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour {
	public GameObject objectPrefab;
	string ip      = "127.0.0.1";
	string port    = "1192";
	bool connected = false;

	protected GameObject netInst;
	private const int maxMultiPlayers = 2;

	private IEnumerator CreateObjectServerSide () {
		netInst = Network.Instantiate(objectPrefab, objectPrefab.transform.position, objectPrefab.transform.rotation, 1) as GameObject;
		netCtrl.networkView.RPC ("Counter", RPCMode.AllBuffered);

		while (netCtrl.initializedPlayers < maxMultiPlayers) {
			Debug.Log (netCtrl.initializedPlayers);
			yield return null;
		}
		Debug.Log (netCtrl.initializedPlayers);
	}

	[SerializeField]
	private RhythmGameSceneNetworkController netCtrl;
	private IEnumerator CreateObjectCliendSide () {
		while ((netInst = GameObject.Find (objectPrefab.name + "(Clone)")) == null) {
			Debug.Log ("not found");
			yield return null;
		}
		Debug.Log ("find");
		netCtrl.networkView.RPC ("Counter", RPCMode.AllBuffered);

		while (netCtrl.initializedPlayers < maxMultiPlayers) {
			Debug.Log (netCtrl.initializedPlayers);
			yield return null;
		}
		Debug.Log (netCtrl.initializedPlayers);
	}
	
	public void OnConnectedToServer() {
		connected = true;
		StartCoroutine (CreateObjectCliendSide ());
	}
	
	public void OnServerInitialized() {
		connected = true;
		StartCoroutine (CreateObjectServerSide ());
	}
	
	public void OnGUI() {
		if (!connected) {
			if (GUI.Button(new Rect(10, 10, 90, 90), "Client")) {
				Network.Connect(ip, int.Parse(port));
			}
			if (GUI.Button(new Rect(10, 110, 90, 90), "Master")) {
				Network.InitializeServer(10, int.Parse(port), false);
			}
		}
	}
}