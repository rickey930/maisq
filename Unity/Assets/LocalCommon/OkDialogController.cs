﻿using UnityEngine;
using System;
using UnityEngine.UI;

public class OkDialogController : MonoBehaviour {
	[SerializeField]
	private Text messageLabel;
	private Action buttonClickEvent { get; set; }
	
	public void Setup (string message, Action clickEvent) {
		messageLabel.text = message;
		buttonClickEvent = clickEvent;
	}
	
	public void OnButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		if (buttonClickEvent != null) {
			buttonClickEvent ();
		}
		Destroy (gameObject);
	}
}
