﻿using UnityEngine;
using System.Collections;

public class ProfileAvaterAbilityController : CircleGraph {

	CircleGraph circleGraph;

	IEnumerator StartCustomCoroutine () {
		while (ProfileInformationController.instance == null) {
			yield return null;
		}
		this.graphDataList = new GraphData[] {
			new GraphData (ProfileInformationController.instance.player_avater_tap_ability, MaimaiStyleDesigner.ByteToPercentRGBA (254,178,254,255)),
			new GraphData (ProfileInformationController.instance.player_avater_hold_ability, MaimaiStyleDesigner.ByteToPercentRGBA (251,32,251,255)),
			new GraphData (ProfileInformationController.instance.player_avater_slide_ability, MaimaiStyleDesigner.ByteToPercentRGBA (162,167,255,255)),
			new GraphData (ProfileInformationController.instance.player_avater_break_ability, MaimaiStyleDesigner.ByteToPercentRGBA (255,0,0,255)),
		};
		ProfileInformationController.instance.profileAvaterAbilityCtrl = this;
	}
	
	public void StartCustom () {
		StartCoroutine (StartCustomCoroutine ());
	}
	
	protected override void Start () {
		StartCustom ();
		base.Start ();
	}
}
