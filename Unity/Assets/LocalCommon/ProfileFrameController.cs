﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProfileFrameController : Image {
	protected virtual IEnumerator StartCustomCoroutine () {
		while (ProfileInformationController.instance == null || !ProfileInformationController.instance.loaded_player_frame) {
			yield return null;
		}
		this.sprite = ProfileInformationController.instance.player_frame;
		ProfileInformationController.instance.profileFrameCtrl = this;
	}
	
	public void StartCustom () {
		StartCoroutine (StartCustomCoroutine ());
	}
	
	protected override void Start () {
		base.Start ();
		StartCustom ();
	}
}
