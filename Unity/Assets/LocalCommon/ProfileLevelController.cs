﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProfileLevelController : Text {
	private ExpBarController expCtrl;

	IEnumerator StartCustomCoroutine () {
		while (ProfileInformationController.instance == null) {
			yield return null;
		}
		this.text = "Lv." + ProfileInformationController.instance.player_level;
		if (expCtrl == null) expCtrl = GetComponentInChildren<ExpBarController>();
		expCtrl.SetProgress (ProfileInformationController.instance.player_exp_rate);
		ProfileInformationController.instance.profileLevelCtrl = this;
	}
	
	public void StartCustom () {
		StartCoroutine (StartCustomCoroutine ());
	}
	
	protected override void Start () {
		base.Start ();
		StartCustom ();
	}
}
