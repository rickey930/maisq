﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProfilePlateController : Image {
	IEnumerator StartCustomCoroutine () {
		while (ProfileInformationController.instance == null || !ProfileInformationController.instance.loaded_player_plate) {
			yield return null;
		}
		this.sprite = ProfileInformationController.instance.player_plate;
		ProfileInformationController.instance.profilePlateCtrl = this;
	}

	public void StartCustom () {
		StartCoroutine (StartCustomCoroutine ());
	}
	
	protected override void Start () {
		base.Start ();
		StartCustom ();
	}
}
