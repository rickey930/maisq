﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProfileTitleController : Text {
	IEnumerator StartCustomCoroutine () {
		while (ProfileInformationController.instance == null) {
			yield return null;
		}
		this.text = ProfileInformationController.instance.player_title;
		this.color = ProfileInformationController.instance.player_title_color;
		ProfileInformationController.instance.profileTitleCtrl = this;
	}

	public void StartCustom () {
		StartCoroutine (StartCustomCoroutine ());
	}
	
	protected override void Start () {
		base.Start ();
		StartCustom ();
	}
}
