﻿using UnityEngine;
using System.Collections;

public class ScalingSprite : MonoBehaviour {

	public float baseWidth;
	public float baseHeight;

	private SpriteRenderer spRenderer;
	private Sprite sprite;

	// Use this for initialization
	void Start () {
		spRenderer = GetComponent<SpriteRenderer> ();
		Scaling ();
	}
	
//	// Update is called once per frame
	void Update () {
		Scaling ();
	}

	void Scaling () {
		if (spRenderer.sprite != null && sprite != spRenderer.sprite) {
			sprite = spRenderer.sprite;
			if (sprite.rect.width != 0 && sprite.rect.height != 0) {
				transform.localScale = new Vector3 (baseWidth / sprite.rect.width, baseHeight / sprite.rect.height, 1);
			}
		}
	}
}
