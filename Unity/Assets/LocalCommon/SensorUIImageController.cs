﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SensorUIImageController : MonoBehaviour {
	public Image icon;
	public RectTransform rotation;
	public RectTransform translation;
	public RectTransform reverseRotation;
	public RectTransform scaler;
	public GameObject buttonEffectPrefab;
	public System.Action onSensorClick { get; set; }
	public System.Action onSensorHoldDownRepeat { get; set; }
	public System.Action onSensorHoldUp { get; set; }
	public bool effectOff { get; set; }
	private float turboFirstWaitTime { get { return 0.5f; } }
	private float turboIntervalTime { get { return 0.1f; } }
	private float turboTimer { get; set; }
	private bool turboStarted { get; set; }
	private bool turboIntervaled { get; set; }
	public void SetPosition (float rotate, float translate) {
		SetPositionLeftHand (rotate, translate);
	}
	public void SetPositionRightHand (float rotate, float translate) {
		rotation.localEulerAngles = new Vector3(0, 0, rotate);
		translation.anchoredPosition = new Vector2 (0, translate);
		reverseRotation.localEulerAngles = new Vector3(0, 0, -rotate);
	}
	public void SetPositionLeftHand (float rotate, float translate) {
		rotation.localEulerAngles = new Vector3(0, 0, -rotate);
		translation.anchoredPosition = new Vector2 (0, translate);
		reverseRotation.localEulerAngles = new Vector3(0, 0, rotate);
	}
	public void OnSensorClick () {
		if (onSensorClick != null) {
			onSensorClick ();
		}
	}
	public void OnSensorHoldDown () {
		turboTimer = 0;
		turboStarted = true;
		turboIntervaled = false;
	}
	public void OnSensorHoldUp () {
		turboTimer = 0;
		turboStarted = false;
		turboIntervaled = false;
		if (onSensorHoldUp != null) {
			onSensorHoldUp ();
		}
	}
	private void Start () {
		float size = 70; //Constants.instance.SENSOR_RADIUS * 2; //SENSOR_RADIUSの直径はUI的にはデカすぎるかもしれない.
		scaler.sizeDelta = new Vector2(size, size);
	}
	private void Update () {
		if (onSensorHoldDownRepeat != null && turboStarted) {
			if (turboTimer == 0) {
				onSensorHoldDownRepeat ();
			}
			else if (turboTimer > turboFirstWaitTime && !turboIntervaled) {
				onSensorHoldDownRepeat ();
				turboIntervaled = true;
			}
			else {
				if (turboTimer > turboFirstWaitTime + turboIntervalTime) {
					onSensorHoldDownRepeat ();
					turboTimer = turboFirstWaitTime;
				}
			}
			turboTimer += Time.deltaTime;
		}
	}

	public void Setup (Sprite icon) {
		this.icon.sprite = icon;
		this.icon.rectTransform.sizeDelta = new Vector2 (icon.rect.width, icon.rect.height);
	}

	public void IconPushed () {
		icon.color = Color.Lerp (Color.white, Color.black, 0.4f);
		if (!effectOff && MaipadDynamicCreatedSpriteTank.instance.buttonTapCircleEffect != null) {
			var effect = Instantiate (buttonEffectPrefab) as GameObject;
			effect.SetParentEx (reverseRotation);
			var image = effect.GetComponent<Image> ();
			image.sprite = MaipadDynamicCreatedSpriteTank.instance.buttonTapCircleEffect;
		}
	}

	public void IconPulled () {
		icon.color = Color.white;
	}
}
