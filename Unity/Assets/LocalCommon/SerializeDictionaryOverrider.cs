﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SerializeDictionary;

/// <summary>
/// ジェネリックを隠すために継承してしまう
/// [System.Serializable]を書くのを忘れない
/// </summary>
[System.Serializable]
public class SampleTable : TableBase<string, Vector3, SamplePair> { }

/// <summary>
/// ジェネリックを隠すために継承してしまう
/// [System.Serializable]を書くのを忘れない
/// </summary>
[System.Serializable]
public class SamplePair : KeyAndValue<string, Vector3> { 
	public SamplePair (string key, Vector3 value) : base (key, value) { }
}


// サンプルここまで.
// ===================================================================================
// 




