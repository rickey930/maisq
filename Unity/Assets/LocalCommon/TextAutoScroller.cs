﻿using UnityEngine;
using UnityEngine.UI;

public class TextAutoScroller : MonoBehaviour {
	public float scrollStopTime = 3.0f;
	public float scrollSpeed = 10.0f;
	public bool stopAndCentering = false;
	public bool stopAndLefting = false;
	private float scroller;
	private float measure;
	private bool scrollStopOrder;
	private bool scrollStop;
	private float scrollStopTimer;
	private bool remeasured;

	private Text _label;
	private RectTransform _rectransform;
	private RectTransform _labelRectransform;
	private string oldText;

	private Text label {
		get {
			if (_label == null) {
				_label = GetComponentInChildren<Text>();
			}
			return _label;
		}
	}

	private RectTransform rTransfrom {
		get {
			if (_rectransform == null) {
				_rectransform = transform as RectTransform;
			}
			return _rectransform;
		}
	}

	private RectTransform labelRTransform {
		get {
			if (_labelRectransform == null) {
				_labelRectransform = label.transform as RectTransform;
			}
			return _labelRectransform;
		}
	}

	private void Start () {
		scroller = 0;
		oldText = label.text;
		measure = label.text.Length * label.fontSize;
		labelRTransform.sizeDelta = new Vector2 (measure, labelRTransform.sizeDelta.y);
	}

	private void OnGUI () {
		bool textChanged = false;
		if (oldText != label.text) {
			oldText = label.text;
			textChanged = true;
			ScrollStartAndLefting ();
		}
		if (!remeasured || textChanged) {
			// GUIの半角文字の大きさは、uGUIのTextのFontSize==11に該当する.
			const float GUI_TEXT_FONT_SIZE = 11.0f;
			float scale = (float)label.fontSize / GUI_TEXT_FONT_SIZE;
			var size = GUI.skin.label.CalcSize (new GUIContent (label.text));
			measure = size.x * scale;
			labelRTransform.sizeDelta = new Vector2 (measure, labelRTransform.sizeDelta.y);
			remeasured = true;
		}
	}

	private void Update () {
		if (stopAndCentering) {
			if (ScrollStopAndCentering ())
				return;
		}

		if (stopAndLefting) {
			if (ScrollStopAndLefting ())
				return;
		}

		if (!scrollStop) {
			labelRTransform.anchoredPosition = new Vector2 (-scroller, 0);
			scroller += scrollSpeed * Time.deltaTime;
			if (measure < scroller) {
				scroller = -rTransfrom.sizeDelta.x;
				scrollStopOrder = false;
			}
			if (!scrollStopOrder) {
				if (scroller > 0) {
					scrollStop = true;
					scrollStopOrder = true;
				}
			}
		}
		else {
			if (scrollStopTimer > scrollStopTime) {
				scrollStop = false;
				scrollStopTimer = 0;
			}
			else {
				scrollStopTimer += Time.deltaTime;
			}
		}
	}

	public void ScrollStartAndLefting () {
		if (remeasured && measure > rTransfrom.sizeDelta.x) {
			labelRTransform.anchorMin = new Vector2(0.0f, 0.0f);
			labelRTransform.anchorMax = new Vector2(0.0f, 1.0f);
			labelRTransform.pivot = new Vector2(0.0f, 0.5f);
			labelRTransform.anchoredPosition = new Vector2(0.0f, 0.0f);
			label.alignment = TextAnchor.MiddleLeft;
			//this.enabled = true;
		}
	}

	public bool ScrollStopAndCentering () {
		if (remeasured && measure <= rTransfrom.sizeDelta.x) {
			labelRTransform.anchorMin = new Vector2(0.5f, 0.0f);
			labelRTransform.anchorMax = new Vector2(0.5f, 1.0f);
			labelRTransform.pivot = new Vector2(0.5f, 0.5f);
			labelRTransform.anchoredPosition = new Vector2(0.0f, 0.0f);
			label.alignment = TextAnchor.MiddleCenter;
			//this.enabled = false;
			return true;
		}
		return false;
	}
	
	public bool ScrollStopAndLefting () {
		if (remeasured && measure <= rTransfrom.sizeDelta.x) {
			labelRTransform.anchorMin = new Vector2(0.0f, 0.0f);
			labelRTransform.anchorMax = new Vector2(0.0f, 1.0f);
			labelRTransform.pivot = new Vector2(0.0f, 0.5f);
			labelRTransform.anchoredPosition = new Vector2(0.0f, 0.0f);
			label.alignment = TextAnchor.MiddleLeft;
			//this.enabled = false;
			return true;
		}
		return false;
	}
}
