﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIBrightnessController : Image {
	IEnumerator StartCustom () {
		while (UserDataController.instance == null)
			yield return null;
		this.color = new Color (1, 1, 1, 1.0f - UserDataController.instance.config.brightness);
	}

	protected override void Start () {
		base.Start ();
		StartCoroutine (StartCustom ());
	}
}
