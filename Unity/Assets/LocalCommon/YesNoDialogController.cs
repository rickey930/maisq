﻿using UnityEngine;
using System;
using UnityEngine.UI;

public class YesNoDialogController : MonoBehaviour {
	[SerializeField]
	private Text messageLabel;
	private Action<bool> buttonClickEvent { get; set; }

	public void Setup (string message, Action<bool> clickEvent) {
		messageLabel.text = message;
		buttonClickEvent = clickEvent;
	}

	public void OnButtonClick (bool yes) {
		Utility.SoundEffectManager.Play ("se_decide");
		if (buttonClickEvent != null) {
			buttonClickEvent (yes);
		}
		Destroy (gameObject);
	}
}
