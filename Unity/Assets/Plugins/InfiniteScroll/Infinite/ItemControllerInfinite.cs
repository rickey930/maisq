﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof( InfiniteScroll))]
public class ItemControllerInfinite : UIBehaviour, IInfiniteScrollSetup
{
	public IEnumerator OnPostSetupItems ()
	{
		GetComponent<InfiniteScroll> ().onUpdateItem.AddListener (OnUpdateItem);
		GetComponentInParent<ScrollRect> ().movementType = ScrollRect.MovementType.Unrestricted;
		setupCompleted = true;
		yield break;
	}

	public void OnUpdateItem (int itemCount, GameObject obj)
	{
		var item = obj.GetComponentInChildren<Item> ();
		item.UpdateItem (itemCount);
	}

	public bool setupCompleted { get; set; }
	public bool forceUpdateContents { get; set; }
}
