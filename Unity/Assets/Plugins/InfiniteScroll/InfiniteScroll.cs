﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.EventSystems;

public class InfiniteScroll : UIBehaviour
{
	[SerializeField]
	private RectTransform m_ItemBase;

	[SerializeField, Range(0, 30)]
	int m_instantateItemCount = 9;

	public Direction direction;

	/// <summary>
	/// スクロール端の項目を中央にもってくるか.
	/// </summary>
	public bool centering;
	/// <summary>
	/// <para>スクロールビューの目に見える範囲 (directionがverticalならheight, horizontalならwidthを設定する)</para>
	/// <para>0でなければ生成アイテム上限も自動で決定されるようになるし、centeringがtrueならこれの半分の値が中央になる.</para>
	/// </summary>
	public float maskedRange;

	public OnItemPositionChange onUpdateItem = new OnItemPositionChange ();

	[System.NonSerialized]
	public List<RectTransform>	m_itemList = new List<RectTransform> ();

	protected float m_diffPreFramePosition = 0;

	protected int m_currentItemNo = 0;

	protected List<IInfiniteScrollSetup> controllers;

	public enum Direction
	{
		Vertical,
		Horizontal,
	}
	
	protected bool allControllersSetupCompleted;


	// cache component

	private RectTransform m_rectTransform;
	protected RectTransform _RectTransform {
		get {
			if (m_rectTransform == null)
				m_rectTransform = GetComponent<RectTransform> ();
			return m_rectTransform;
		}
	}

	private float AnchoredPosition
	{
		get{
			return  (direction == Direction.Vertical ) ? 
					-_RectTransform.anchoredPosition.y:
					_RectTransform.anchoredPosition.x;
		}
	}

	public float AnchordMargin {
		get {
			if (centering) {
				float margin = (maskedRange - ItemScale) / 2;
				return margin;
			}
			return 0;
		}
	}

	private float m_itemScale = -1;
	public float ItemScale {
		get {
			if (m_ItemBase != null && m_itemScale == -1) {
					m_itemScale = (direction == Direction.Vertical ) ? 
					m_ItemBase.sizeDelta.y : 
					m_ItemBase.sizeDelta.x ;
			}
			return m_itemScale;
		}
	}

	protected override void Start ()
	{
		controllers = GetComponents<MonoBehaviour>()
				.Where(item => item is IInfiniteScrollSetup )
				.Select(item => item as IInfiniteScrollSetup )
				.ToList();

		if (maskedRange != 0 && ItemScale != 0) {
			m_instantateItemCount = ((int)(decimal)(maskedRange / ItemScale)) + 4;
		}

		// create items

		var scrollRect = GetComponentInParent<ScrollRect>();
		scrollRect.horizontal = direction == Direction.Horizontal;
		scrollRect.vertical = direction == Direction.Vertical;
		scrollRect.content = _RectTransform;

		m_ItemBase.gameObject.SetActive (false);

		RectTransform[] items = new RectTransform[m_instantateItemCount];
		for (int i=0; i<m_instantateItemCount; i++) {
			var item = GameObject.Instantiate (m_ItemBase) as RectTransform;
			item.SetParent (transform, false);
			item.name = i.ToString ();
			item.anchoredPosition = 
					(direction == Direction.Vertical ) ?
					new Vector2 (0, -(ItemScale * (i) + AnchordMargin)) : 
					new Vector2 (ItemScale * (i) + AnchordMargin, 0) ;
			m_itemList.Add (item);

			item.gameObject.SetActive (true);

			items[i] = item;
		}

		foreach(  var controller in controllers  ){
			StartCoroutine(controller.OnPostSetupItems());
			StartCoroutine(InitializeItems(controller, items));
		}
		StartCoroutine (CheckSetupCompleted (controllers.ToArray()));
	}

	IEnumerator InitializeItems (IInfiniteScrollSetup controller, RectTransform[] items) {
		while (!controller.setupCompleted) {
			yield return null;
		}
		for (int i = 0; i < items.Length; i++) {
			controller.OnUpdateItem(i, items[i].gameObject);
		}
	}
	IEnumerator CheckSetupCompleted (IInfiniteScrollSetup[] controllers) {
		while (!allControllersSetupCompleted) {
			bool notyet = false;
			foreach (var controller in controllers) {
				if (!controller.setupCompleted) {
					notyet = true;
					break;
				}
			}
			if (!notyet) {
				allControllersSetupCompleted = true;
			}
			else {
				yield return null;
			}
		}
	}

	void Update ()
	{
		if (!allControllersSetupCompleted)
			return;

		while (AnchoredPosition - m_diffPreFramePosition  < -(ItemScale * 2 + AnchordMargin)) {

			m_diffPreFramePosition -= ItemScale;

			var item = m_itemList [0];
			m_itemList.RemoveAt (0);
			m_itemList.Add (item);

			var pos = ItemScale * m_instantateItemCount + ItemScale * m_currentItemNo + AnchordMargin;
			item.anchoredPosition = (direction == Direction.Vertical ) ? new Vector2 (0, -(pos)) : new Vector2 (pos, 0);

			onUpdateItem.Invoke (m_currentItemNo + m_instantateItemCount, item.gameObject);

			m_currentItemNo ++;

		}

		while (AnchoredPosition - m_diffPreFramePosition > -(ItemScale + AnchordMargin)) {

			m_diffPreFramePosition += ItemScale;

			var itemListLastCount = m_instantateItemCount - 1; 
			var item = m_itemList [itemListLastCount];
			m_itemList.RemoveAt (itemListLastCount);
			m_itemList.Insert (0, item);

			m_currentItemNo --;

			var pos = ItemScale * m_currentItemNo + AnchordMargin;
			item.anchoredPosition = (direction == Direction.Vertical ) ? new Vector2 (0, -(pos)): new Vector2 (pos, 0);
			onUpdateItem.Invoke (m_currentItemNo, item.gameObject);
		}

		bool forceUpdateContents = false;
		foreach (var ctrl in controllers) {
			if (ctrl.forceUpdateContents) {
				forceUpdateContents = true;
				ctrl.forceUpdateContents = false;
			}
		}
		if (forceUpdateContents) {
			for (int i = 0; i < m_itemList.Count; i++) {
				onUpdateItem.Invoke (m_currentItemNo + i, m_itemList[i].gameObject);
			}
		}

	}

	[System.Serializable]
	public class OnItemPositionChange : UnityEngine.Events.UnityEvent<int, GameObject>{}
}
