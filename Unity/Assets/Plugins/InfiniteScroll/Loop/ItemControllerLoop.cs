using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof( InfiniteScroll))]
public class ItemControllerLoop : UIBehaviour, IInfiniteScrollSetup
{
	private bool isSetuped = false;

	public IEnumerator OnPostSetupItems ()
	{
		GetComponentInParent<ScrollRect> ().movementType = ScrollRect.MovementType.Unrestricted;
		isSetuped = true;
		
		setupCompleted = true;
		yield break;
	}

	public void OnUpdateItem (int itemCount, GameObject obj)
	{
		if( isSetuped == true ) 
			return;

		var item = obj.GetComponentInChildren<Item> ();
		item.UpdateItem (itemCount);
	}

	public bool setupCompleted { get; set; }
	public bool forceUpdateContents { get; set; }
}
