{
	"savedata_version":1,
	"title":"TUTORIAL",
	"title_ruby":"ちゅうとりある",
	"artist":"チュートリアル",
	"artist_ruby":"ちゅうとりある",
	"whole_bpm":"100",
	"seek":0.05,
	"wait":0.0,
	"simai_first":null,
	"jacket":"",
	"audio":"",
	"imported_at":{"year":2016,"month":1,"day":1,"hour":0,"minute":0,"second":0},
	"scores":{
		"EASY":{
			"savedata_version":1,
			"level":"1",
			"script_type":"resource_maisq",
			"notes_design":"Teacher",
			"score":"Tutorial/score_info",
			"full_score":0
		}
	}
}