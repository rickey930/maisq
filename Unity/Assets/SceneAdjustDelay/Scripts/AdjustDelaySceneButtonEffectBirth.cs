﻿using UnityEngine;
using System.Collections;

public class AdjustDelaySceneButtonEffectBirth : MonoBehaviour {
	[SerializeField]
	private GameObject buttonEffectPrefab;
	
	public void Birth() {
		var obj = Instantiate (buttonEffectPrefab) as GameObject;
		obj.SetParentEx(transform);
	}
}
