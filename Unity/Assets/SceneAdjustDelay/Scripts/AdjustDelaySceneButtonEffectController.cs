﻿using UnityEngine;
using System.Collections;

public class AdjustDelaySceneButtonEffectController : MonoBehaviour {
	// Use this for initialization
	void Start () {
		var renderer = GetComponentInChildren<SpriteRenderer> ();
		renderer.sprite = MaipadDynamicCreatedSpriteTank.instance.buttonTapCircleEffect;
	}
}
