using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ConfigNodeController : MonoBehaviour {
	public ConfigListInformation info { get; set; }
	public ConfigListController manager;
	public Image button;
	public Text configName;
	public Text configValue;
	
	public void UpdateItem (ConfigListInformation info) {
		this.info = info;
		if (manager.manager.selectedId == info.id) {
			float alpha = button.color.a;
			button.color = MaimaiStyleDesigner.ByteToPercentRGBA(255, 150, 0, (255 * alpha).ToInt());
		}
		else {
			float alpha = button.color.a;
			button.color = MaimaiStyleDesigner.ByteToPercentRGBA(255, 255, 255, (255 * alpha).ToInt());
		}
		configName.text = info.key;
		configValue.text = info.value;
		configValue.color = info.valueColor;
	}
	
	public void OnButtonClick () {
		if (manager.manager.selectedId == info.id) {
			manager.manager.OnDoubleTappedCell (info);
		}
		else {
			manager.manager.selectedId = info.id;
			manager.forceUpdateContents = true;
			manager.manager.OnTappedCell (info);
		}
	}
}
