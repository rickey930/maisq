﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class ConfigSceneDetailUIController : MonoBehaviour {
	public Text itemNameLabel;
	public Toggle checkBox;
	public Text checkBoxLabel;
	public Slider slider;
	public Text sliderValueLabel;
	public GameObject switchObj;
	public Text switchValueNameLabel;
	public InputField textBox;

	protected Action<int> onOKCallbackTypeAssign;
	protected Action<bool> onOKCallbackSwitchAssign;
	protected Action<float> onOKCallbackValueAssign;
	protected Action<string> onOKCallbackTextAssign;
	protected Func<int, string> onValueChangedGetName;
	protected Func<float, string> onFValueChangedGetName;

	protected int switchValue;
	protected int switchValueMax;
	protected bool checkBoxLabelTypeIsYesNo;

	public virtual void Show (string itemName, int initialValue, Action<int> onOKCallback, int switchValueMax, Func<int, string> onGetName) {
		itemNameLabel.text = itemName;
		onOKCallbackTypeAssign = onOKCallback;
		onOKCallbackSwitchAssign = null;
		onOKCallbackValueAssign = null;
		onOKCallbackTextAssign = null;
		onValueChangedGetName = onGetName;
		onFValueChangedGetName = null;
		switchValue = initialValue;
		this.switchValueMax = switchValueMax;
		switchValueNameLabel.text = onGetName (initialValue);
		switchObj.SetActive (true);
		checkBox.gameObject.SetActive (false);
		slider.gameObject.SetActive (false);
		textBox.gameObject.SetActive (false);
		gameObject.SetActive (true);
	}
	
	public virtual void Show (string itemName, bool initialValue, Action<bool> onOKCallback, bool checkBoxLabelTypeIsYesNo = false) {
		itemNameLabel.text = itemName;
		this.checkBoxLabelTypeIsYesNo = checkBoxLabelTypeIsYesNo;
		onOKCallbackTypeAssign = null;
		onOKCallbackSwitchAssign = onOKCallback;
		onOKCallbackValueAssign = null;
		onOKCallbackTextAssign = null;
		onValueChangedGetName = null;
		onFValueChangedGetName = null;
		checkBox.isOn = initialValue;
		if (checkBoxLabelTypeIsYesNo) {
			checkBoxLabel.text = initialValue.ToConfigValueYesNoString ();
		}
		else {
			checkBoxLabel.text = initialValue.ToConfigValueString ();
		}
		switchObj.SetActive (false);
		checkBox.gameObject.SetActive (true);
		slider.gameObject.SetActive (false);
		textBox.gameObject.SetActive (false);
		gameObject.SetActive (true);
	}
	
	public virtual void Show (string itemName, float initialValue, Action<float> onOKCallback, float min, float max, Func<float, string> onGetName, bool wholeNumbers) {
		itemNameLabel.text = itemName;
		onOKCallbackTypeAssign = null;
		onOKCallbackSwitchAssign = null;
		onOKCallbackValueAssign = onOKCallback;
		onOKCallbackTextAssign = null;
		onValueChangedGetName = null;
		onFValueChangedGetName = onGetName;
		slider.minValue = min;
		slider.maxValue = max;
		slider.wholeNumbers = wholeNumbers;
		slider.value = initialValue;
		sliderValueLabel.text = onGetName (initialValue);
		switchObj.SetActive (false);
		checkBox.gameObject.SetActive (false);
		slider.gameObject.SetActive (true);
		textBox.gameObject.SetActive (false);
		gameObject.SetActive (true);
	}

	public virtual void Show (string itemName, string initialValue, Action<string> onOKCallback) {
		itemNameLabel.text = itemName;
		onOKCallbackTypeAssign = null;
		onOKCallbackSwitchAssign = null;
		onOKCallbackValueAssign = null;
		onOKCallbackTextAssign = onOKCallback;
		onValueChangedGetName = null;
		onFValueChangedGetName = null;
		textBox.text = initialValue ?? string.Empty;
		switchObj.SetActive (false);
		checkBox.gameObject.SetActive (false);
		slider.gameObject.SetActive (false);
		textBox.gameObject.SetActive (true);
		gameObject.SetActive (true);
	}

	public void Hide () {
		gameObject.SetActive (false);
	}

	public virtual void OnOKButtonClick () {
		if (onOKCallbackTypeAssign != null) {
			onOKCallbackTypeAssign (switchValue);
		}
		if (onOKCallbackSwitchAssign != null) {
			onOKCallbackSwitchAssign (checkBox.isOn);
		}
		if (onOKCallbackValueAssign != null) {
			onOKCallbackValueAssign (slider.value);
		}
		if (onOKCallbackTextAssign != null) {
			onOKCallbackTextAssign (textBox.text);
		}
		Utility.SoundEffectManager.Play ("se_decide");
		Hide ();
	}

	public void OnTypeChangeButtonClick (bool isPlus) {
		if (onValueChangedGetName != null) {
			Utility.SoundEffectManager.Play ("se_select");
			if (isPlus) {
				switchValue++;
				switchValue %= switchValueMax;
			}
			else {
				switchValue += switchValueMax - 1;
				switchValue %= switchValueMax;
			}
			switchValueNameLabel.text = onValueChangedGetName(switchValue);
		}
	}

	public void OnCheckedChanged () {
		if (gameObject.activeSelf) {
			Utility.SoundEffectManager.Play ("se_select");
		}
		string text;
		if (checkBoxLabelTypeIsYesNo) {
			text = checkBox.isOn.ToConfigValueYesNoString ();
		}
		else {
			text = checkBox.isOn.ToConfigValueString ();
		}
		checkBoxLabel.text = text;
	}

	public void OnSliderValueChanged () {
		sliderValueLabel.text = onFValueChangedGetName(slider.value);
	}

	public void CancelSoundEffectPlay () {
		Utility.SoundEffectManager.Play ("se_decide");
	}


}
