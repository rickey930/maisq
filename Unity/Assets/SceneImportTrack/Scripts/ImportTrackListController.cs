using UnityEngine;
using System.Collections;
using System.IO;

public class ImportTrackListController : MonoBehaviour, IInfiniteScrollSetup {

	public ImportTrackSceneManager manager;

	private ImportTrackListInformation[] contents { get { return manager.contents; } }

	#region IInfiniteScrollSetup
	public bool forceUpdateContents { get { return manager.forceUpdateContents; } set { manager.forceUpdateContents = value; } }
	public bool setupCompleted { get; set; }
	
	public IEnumerator OnPostSetupItems() {
		// リストの初期化.
		while (contents == null)
			yield return null;
		ResizeScrollView ();
		setupCompleted = true;
	}
	
	public void ResizeScrollView () {
		var infiniteScroll = GetComponent<InfiniteScroll> ();
		var rectTransform = GetComponent<RectTransform> ();
		var delta = rectTransform.sizeDelta;
		delta.y = (infiniteScroll.ItemScale * (contents.Length)) + (infiniteScroll.AnchordMargin * 2);
		rectTransform.sizeDelta = delta;
	}
	
	public void OnUpdateItem (int index, GameObject obj) {
		if (contents == null || index < 0 || index >= contents.Length) {
			if (obj.activeSelf) {
				obj.SetActive (false);
			}
		}
		else {
			if (!obj.activeSelf) {
				obj.SetActive (true);
			}
			var item = obj.GetComponent<ImportTrackNodeController> ();
			item.UpdateItem (contents [index]);
		}
	}
	#endregion

}
