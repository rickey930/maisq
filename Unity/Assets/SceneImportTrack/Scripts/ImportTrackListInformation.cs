﻿using UnityEngine;
using System.Collections;

public class ImportTrackListInformation {
	public enum ManagerState {
		IMPORT, SELECT_TRACK, EDIT_TRACK, SELECT_SCORE, EDIT_SCORE,
	}

	public enum ImportMenu {
		NONE,
		IMPORT_FILELIST, IMPORT_MAIDATA, IMPORT_OFFICIAL_WEB_SITE, TRACK_LIST,
		TRACK_ID, 
		EDIT_TRACK_ID, EDIT_TITLE, EDIT_TITLE_RUBY, EDIT_ARTIST, EDIT_ARTIST_RUBY, EDIT_BPM, EDIT_AUDIO, EDIT_JACKET, SCORE_LIST,
		SCORE_ID, 
		EDIT_SCORE_ID, EDIT_LEVEL, EDIT_DESIGNER, EDIT_SCORE,
	}

	public ImportMenu menuId { get; set; }
	public string trackOrScoreId { get; set; }
	public string key { get; set; }
	public string value { get; set; }
	public Color valueColor { get; set; }
	public bool useText { get; set; }
	public bool usePicture { get; set; }
	public Sprite sprite { get; set; }
	public AudioClip audioClip { get; set; }

	public ImportTrackListInformation (ImportMenu menuId, string key, string value) {
		this.menuId = menuId;
		this.key = key;
		this.value = value.ToImportMenuValueString ();
		this.valueColor = value.ToImportMenuValueColor ();
		this.useText = value != null;
	}

	public static ImportTrackListInformation CreateInfo (ImportMenu menuId, string key, string value) {
		var ret = new ImportTrackListInformation (menuId, key, value);
		return ret;
	}
	
	public static ImportTrackListInformation CreateJacketInfo (MonoBehaviour component, string key, string trackId, System.Action callback) {
		var ret = CreateInfo (ImportMenu.EDIT_JACKET, key, null);
		ret.trackOrScoreId = trackId;
		component.StartCoroutine (ret.LoadJacket(component, (data)=>{ if (callback != null) callback (); }));
		return ret;
	}
	
	public static ImportTrackListInformation CreateAudioInfo (MonoBehaviour component, string key, string trackId, System.Action callback) {
		var ret = CreateInfo (ImportMenu.EDIT_AUDIO, key, null);
		ret.trackOrScoreId = trackId;
		component.StartCoroutine (ret.LoadAudio(component, (data)=>{ if (callback != null) callback (); }));
		return ret;
	}
	
	public static ImportTrackListInformation CreateTrackInfo (string trackId) {
		var ret = CreateInfo (ImportMenu.TRACK_ID, trackId, null);
		ret.trackOrScoreId = trackId;
		return ret;
	}
	
	public static ImportTrackListInformation CreateScoreInfo (string scoreId) {
		var ret = CreateInfo (ImportMenu.SCORE_ID, scoreId, null);
		ret.trackOrScoreId = scoreId;
		return ret;
	}
	
	public IEnumerator LoadJacket (MonoBehaviour component, System.Action<Sprite> callback) {
		if (sprite == null) {
			var trackInfo = UserDataController.instance.GetTrack(trackOrScoreId);
			if (trackInfo != null) {
				string extension = System.IO.Path.GetExtension(trackInfo.jacket);
				yield return component.StartCoroutine (UserDataController.instance.LoadJacket (trackOrScoreId, extension, (data)=>sprite = data));
			}
		}
		if (sprite != null) {
			usePicture = true;
			useText = false;
		}
		else {
			usePicture = false;
			useText = true;
			value = "(EMPTY)";
			valueColor = Color.cyan;
		}
		callback (sprite);
	}
	
	public IEnumerator LoadAudio (MonoBehaviour component, System.Action<AudioClip> callback) {
		if (audioClip == null) {
			var trackInfo = UserDataController.instance.GetTrack(trackOrScoreId);
			if (trackInfo != null) {
				string extension = System.IO.Path.GetExtension(trackInfo.audio);
				yield return component.StartCoroutine (UserDataController.instance.LoadAudio (trackOrScoreId, extension, (data)=>audioClip = data));
			}
		}
		useText = true;
		usePicture = false;
		if (audioClip != null) {
			value = "Used";
			valueColor = Color.red;
		}
		else {
			value = "Un used";
			valueColor = Color.cyan;
		}
		callback (audioClip);
	}
}

public static class ImportTrackListInformationExtendMethods {
	public static string ToImportMenuValueString (this string value) {
		if (value == null) {
			value = string.Empty;
		}
		else if (value == string.Empty) {
			return "(EMPTY)";
		}
		return value;
	}
	
	public static Color ToImportMenuValueColor (this string value) {
		return value.ToProfileValueColor ();
	}
}
