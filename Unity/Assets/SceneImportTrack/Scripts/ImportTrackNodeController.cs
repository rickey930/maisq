using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ImportTrackNodeController : MonoBehaviour {
	public ImportTrackListInformation info { get; set; }
	public ImportTrackListController controller;
	public ImportTrackSceneManager manager { get { if (controller != null) return controller.manager; return null; } }
	public Image button;
	public Text nameLabel;
	public Text valueLabel;
	public Image pictureBox;

	public void UpdateItem (ImportTrackListInformation info) {
		this.info = info;

		if ((manager.state == ImportTrackListInformation.ManagerState.SELECT_TRACK && manager.selectedTrackId == info.trackOrScoreId) ||
		    (manager.state == ImportTrackListInformation.ManagerState.SELECT_SCORE && manager.selectedScoreId == info.trackOrScoreId) ||
			(manager.state != ImportTrackListInformation.ManagerState.SELECT_TRACK && manager.state != ImportTrackListInformation.ManagerState.SELECT_SCORE && manager.selectedMenuId == info.menuId)) {
			float alpha = button.color.a;
			button.color = MaimaiStyleDesigner.ByteToPercentRGBA(255, 150, 0, (255 * alpha).ToInt());
		}
		else {
			float alpha = button.color.a;
			button.color = MaimaiStyleDesigner.ByteToPercentRGBA(255, 255, 255, (255 * alpha).ToInt());
		}
		nameLabel.text = info.key;
		if (info.useText) {
			valueLabel.text = info.value;
			valueLabel.color = info.valueColor;
			valueLabel.gameObject.SetActive (true);
		}
		else {
			valueLabel.gameObject.SetActive (false);
		}
		if (info.usePicture) {
			pictureBox.sprite = info.sprite;
			pictureBox.gameObject.SetActive (true);
		}
		else {
			pictureBox.gameObject.SetActive (false);
		}
	}

	public void ButtonClick () {
		if (manager == null || info == null) return;

		if (manager.state == ImportTrackListInformation.ManagerState.SELECT_TRACK) {
			if (manager.selectedTrackId == info.trackOrScoreId) {
				manager.OnDoubleTappedCell (info);
			}
			else {
				manager.selectedTrackId = info.trackOrScoreId;
				controller.forceUpdateContents = true;
				manager.OnTappedCell (info);
			}
		}
		else if (manager.state == ImportTrackListInformation.ManagerState.SELECT_SCORE) {
			if (manager.selectedScoreId == info.trackOrScoreId) {
				manager.OnDoubleTappedCell (info);
			}
			else {
				manager.selectedScoreId = info.trackOrScoreId;
				controller.forceUpdateContents = true;
				manager.OnTappedCell (info);
			}
		}
		else {
			if (manager.selectedMenuId == info.menuId) {
				manager.OnDoubleTappedCell (info);
			}
			else {
				manager.selectedMenuId = info.menuId;
				controller.forceUpdateContents = true;
				manager.OnTappedCell (info);
			}
		}
	}
}
