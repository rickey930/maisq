﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ImportTrackScoreEditTextFieldController : MonoBehaviour {

	[SerializeField]
	private ImportTrackSceneManager manager;
	[SerializeField]
	private InputField scoreEditField;
	[SerializeField]
	private Button goEditorButton;
	[SerializeField]
	private GameObject yesNoDialogPrefab;
	[SerializeField]
	private GameObject yesNoDialogParent;

	private string scriptType;
	
	public void Show (string initText, string scriptType) {
		scoreEditField.text = initText;
		this.scriptType = scriptType;
		goEditorButton.gameObject.SetActive (string.IsNullOrEmpty(scriptType) || scriptType == "simai" || scriptType == "maisq");
		gameObject.SetActive (true);
	}

	public void Hide () {
		Utility.SoundEffectManager.Play ("se_decide");
		gameObject.SetActive (false);
	}

	public void OnOKButtonClick () {
		manager.ScoreScriptEditRef (scoreEditField.text);
		Hide ();
	}

	public void OnCancelButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		var dialog = Instantiate (yesNoDialogPrefab) as GameObject;
		dialog.SetParentEx (yesNoDialogParent);
		var controller = dialog.GetComponent<YesNoDialogController>();
		controller.Setup ("Do you really want to quit without saving?", (yes) => {
			if (yes) {
				Hide ();
			}
		});
	}

	public void OnGoEditorButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		StartCoroutine(GoEditor());
	}

	private IEnumerator GoEditor () {
		yield return StartCoroutine(EditorInformationController.instance.Setup (manager.selectedTrackId, manager.selectedScoreId, scriptType, scoreEditField.text));
		EditorInformationController.instance.selectionStartIndex = 0;
		EditorInformationController.instance.selectionEndIndex = 0;
		EditorInformationController.instance.timelineScrolledAmount = 0;
		EditorInformationController.instance.Save ();
		Application.LoadLevel ("SceneScoreEditor");
	}
}
