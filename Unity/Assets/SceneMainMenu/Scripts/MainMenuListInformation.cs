﻿using UnityEngine;
using System.Collections;

public class MainMenuListInformation {
	public enum MenuId {
		NONE,
		SELECT_TRACK, IMPORT_TRACK, ADJUST_DELAY, TUTORIAL, SYNC_HOST, SYNC_CLIENT, 
	}
	
	public MenuId menuId { get; set; }
	public string key { get; set; }

	public MainMenuListInformation (MenuId menuId, string key) {
		this.menuId = menuId;
		this.key = key;
	}

	public static MainMenuListInformation CreateInfo (MenuId menuId, string key) {
		var ret = new MainMenuListInformation (menuId, key);
		return ret;
	}
}
