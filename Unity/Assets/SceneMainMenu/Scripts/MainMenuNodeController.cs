﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenuNodeController : MonoBehaviour {
	public MainMenuListInformation info { get; set; }
	public MainMenuListController controller;
	public MainMenuSceneManager manager { get { if (controller != null) return controller.manager; return null; } }
	public Image button;
	public Text nameLabel;

	public void UpdateItem (MainMenuListInformation info) {
		this.info = info;
		
		if (manager.selectedMenuId == info.menuId) {
			float alpha = button.color.a;
			button.color = MaimaiStyleDesigner.ByteToPercentRGBA(255, 150, 0, (255 * alpha).ToInt());
		}
		else {
			float alpha = button.color.a;
			button.color = MaimaiStyleDesigner.ByteToPercentRGBA(255, 255, 255, (255 * alpha).ToInt());
		}
		nameLabel.text = info.key;
	}
	
	public void ButtonClick () {
		if (manager == null || info == null) return;

		if (manager.selectedMenuId == info.menuId) {
			manager.OnDoubleTappedCell (info);
		}
		else {
			manager.selectedMenuId = info.menuId;
			controller.forceUpdateContents = true;
			manager.OnTappedCell (info);
		}
	}
}
