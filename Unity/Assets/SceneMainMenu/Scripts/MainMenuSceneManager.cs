﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainMenuSceneManager : MonoBehaviour {
	[SerializeField]
	private MainMenuListController listCtrl;
	[SerializeField]
	private UnityEngine.UI.Image guideCircle;
	[SerializeField]
	private GameObject sensorUIPrefab;
	[SerializeField]
	private RectTransform sensorUIParent;
	[SerializeField]
	private FadeController fade;
	[SerializeField]
	private SyncLobbyHostDialogController hostDialog;
	[SerializeField]
	private SyncLobbyClientDialogController clientDialog;
	
	private Sprite[] sensorIcons;

	private Dictionary<MainMenuListInformation.MenuId, MainMenuListInformation> contentsSource { get; set; }
	private Dictionary<string, MainMenuListInformation> trackContentsSource { get; set; }
	public MainMenuListInformation[] contents { get; set; }
	public bool forceUpdateContents { get; set; }
	
	private SensorUIImageController[] sensorImages { get; set; }
	
	private float sensorTurboSpeed0 { get; set; }
	private float sensorTurboSpeed3 { get; set; }
	private float sensorTurboFirstSpeed { get { return 65.0f; } }
	private float sensorTurboAddSpeed { get { return 180.0f * Time.deltaTime; } }
	private float sensorTurboMaxSpeed { get { return sensorTurboFirstSpeed * 1.5f; } }

	public MainMenuListInformation.MenuId selectedMenuId { get; set; }

	IEnumerator Start () {
		while (UserDataController.instance == null) {
			yield return null;
		}
		
		selectedMenuId = MainMenuListInformation.MenuId.NONE;
		contentsSource = CreateMainMenuContents();
		contents = new MainMenuListInformation[contentsSource.Values.Count];
		contentsSource.Values.CopyTo (contents, 0);
		if (MiscInformationController.instance.syncClientAfterResult) {
			selectedMenuId = MainMenuListInformation.MenuId.SYNC_CLIENT;
			MiscInformationController.instance.syncClientAfterResult = false;
			clientDialog.ShowVerAfterResult ();
		}
		
		sensorIcons = new Sprite[] {
			SpriteTank.Get ("icon_allow_up"),
			SpriteTank.Get ("icon_dammy"),
			SpriteTank.Get ("icon_dammy"),
			SpriteTank.Get ("icon_allow_down"),
			SpriteTank.Get ("icon_step_decide"),
			SpriteTank.Get ("icon_step_cancel"),
			SpriteTank.Get ("icon_profile"),
			SpriteTank.Get ("icon_option"),
		};
		
		float radius = (Constants.instance.MAIMAI_OUTER_RADIUS + 5) * 2;
		guideCircle.transform.RectCast ().sizeDelta = new Vector2 (radius, radius);
		while (MaipadDynamicCreatedSpriteTank.instance.guideCircleMenu == null) {
			yield return null;
		}
		guideCircle.sprite = MaipadDynamicCreatedSpriteTank.instance.guideCircleMenu;
		CreateSensors ();

		if (MiscInformationController.instance.lastLoadedBgmKey != "maisq_bgm_main") {
			AudioManagerLite.Pause ();
			AudioManagerLite.Load (Utility.SoundEffectManager.GetClip ("maisq_bgm_main"));
			MiscInformationController.instance.lastLoadedBgmKey = "maisq_bgm_main";
			AudioManagerLite.Seek (0);
			AudioManagerLite.Play (true);
		}

		while (!listCtrl.setupCompleted) {
			yield return null;
		}
		(listCtrl.transform as RectTransform).anchoredPosition = new Vector2 (0, MiscInformationController.instance.mainMenuListScrollAmount);
	}
	
	private Dictionary<MainMenuListInformation.MenuId, MainMenuListInformation> CreateMainMenuContents () {
		var c = new Dictionary<MainMenuListInformation.MenuId, MainMenuListInformation> ();
		CreateContent (c, MainMenuListInformation.CreateInfo(MainMenuListInformation.MenuId.SELECT_TRACK, "Select Track"));
		CreateContent (c, MainMenuListInformation.CreateInfo(MainMenuListInformation.MenuId.IMPORT_TRACK, "Import Track"));
		CreateContent (c, MainMenuListInformation.CreateInfo(MainMenuListInformation.MenuId.ADJUST_DELAY, "Adjust Delay"));
		CreateContent (c, MainMenuListInformation.CreateInfo(MainMenuListInformation.MenuId.TUTORIAL, "Tutorial"));
		CreateContent (c, MainMenuListInformation.CreateInfo(MainMenuListInformation.MenuId.SYNC_HOST, "Sync Host"));
		CreateContent (c, MainMenuListInformation.CreateInfo(MainMenuListInformation.MenuId.SYNC_CLIENT, "Sync Client"));
		return c;
	}

	private void CreateContent (Dictionary<MainMenuListInformation.MenuId, MainMenuListInformation> contents, MainMenuListInformation content) {
		contents[content.menuId] = content;
	}

	private void CreateSensors() {
		sensorImages = new SensorUIImageController[8];
		for (int i = 0; i < 8; i++) {
			var sensorImageObj = Instantiate(sensorUIPrefab) as GameObject;
			sensorImageObj.name = "Sensor for UI " + (i + 1).ToString();
			sensorImageObj.SetParentEx (sensorUIParent);
			var sensorImage = sensorImageObj.GetComponent<SensorUIImageController>();
			sensorImage.SetPosition (Constants.instance.GetPieceDegree(i), Constants.instance.MAIMAI_OUTER_RADIUS);
			sensorImage.Setup(sensorIcons[i]);
			sensorImages[i] = sensorImage;
		}
		
		// Scroll Up.
		sensorImages [0].onSensorHoldDownRepeat = () => {
			Utility.SoundEffectManager.Play ("se_select");
			float speed;
			if (sensorTurboSpeed0 == 0) {
				speed = sensorTurboFirstSpeed;
			}
			else {
				speed = sensorTurboSpeed0;
			}
			sensorTurboSpeed0 += sensorTurboAddSpeed;
			if (speed > sensorTurboMaxSpeed) speed = sensorTurboMaxSpeed;
			var rect = listCtrl.transform.RectCast ();
			rect.anchoredPosition = new Vector2 (0, rect.anchoredPosition.y - speed);
		};
		sensorImages [0].onSensorHoldUp = () => {
			sensorTurboSpeed0 = 0;
		};
		// Level Up.
		// Level Down.
		// Scroll Down.
		sensorImages [3].onSensorHoldDownRepeat = () => {
			Utility.SoundEffectManager.Play ("se_select");
			float speed;
			if (sensorTurboSpeed3 == 0) {
				speed = sensorTurboFirstSpeed;
			}
			else {
				speed = sensorTurboSpeed3;
			}
			sensorTurboSpeed3 += sensorTurboAddSpeed;
			if (speed > sensorTurboMaxSpeed) speed = sensorTurboMaxSpeed;
			var rect = listCtrl.transform.RectCast ();
			rect.anchoredPosition = new Vector2 (0, rect.anchoredPosition.y + speed);
		};
		sensorImages [3].onSensorHoldUp = () => {
			sensorTurboSpeed3 = 0;
		};
		sensorImages [4].onSensorClick = () => {
			if (selectedMenuId != MainMenuListInformation.MenuId.NONE) {
				OnDoubleTappedCell (contentsSource [selectedMenuId]);
			}
		};
		sensorImages [5].onSensorClick = () => {
			AudioManagerLite.Pause ();
			MiscInformationController.instance.lastLoadedBgmKey = string.Empty;
			fade.Activate (()=> {
				SceneChangePrepare ();
				Application.LoadLevel ("SceneTitle");
			});
		};
		// Profile.
		sensorImages [6].onSensorClick = ()=> {
			Utility.SoundEffectManager.Play ("se_decide");
			MiscInformationController.instance.optionSceneCallbackSceneName = "SceneMainMenu";
			fade.Activate (() => {
				Application.LoadLevel ("SceneProfile");
			});
		};
		// Config.
		sensorImages [7].onSensorClick = ()=>{
			Utility.SoundEffectManager.Play ("se_decide");
			MiscInformationController.instance.optionSceneCallbackSceneName = "SceneMainMenu";
			fade.Activate (() => {
				Application.LoadLevel ("SceneConfig");
			});
		};
	}

	public void OnTappedCell (MainMenuListInformation info) {
		Utility.SoundEffectManager.Play ("se_decide");
	}
	
	public void OnDoubleTappedCell (MainMenuListInformation info) {
		switch (info.menuId) {
		// MAIN MENU.
		case MainMenuListInformation.MenuId.SELECT_TRACK:
		{
			Utility.SoundEffectManager.Play ("se_decide");
			fade.Activate (()=>{
				SceneChangePrepare ();
				MiscInformationController.instance.syncMode = 0; //シングルプレーである.
				MiscInformationController.instance.selectMusicVoiceCallRequest = true;
				Application.LoadLevel ("SceneSelectTrack");
			});
			break;
		}
		case MainMenuListInformation.MenuId.IMPORT_TRACK:
		{
			Utility.SoundEffectManager.Play ("se_decide");
			fade.Activate (()=>{
				SceneChangePrepare ();
				Application.LoadLevel ("SceneImportTrack");
			});
			break;
		}
		case MainMenuListInformation.MenuId.ADJUST_DELAY:
		{
			Utility.SoundEffectManager.Play ("se_decide");
			AudioManagerLite.Pause ();
			MiscInformationController.instance.lastLoadedBgmKey = string.Empty;
			fade.Activate(()=>{
				SceneChangePrepare ();
				Application.LoadLevel ("SceneAdjustDelay");
			});
			break;
		}
		case MainMenuListInformation.MenuId.TUTORIAL:
		{
			Utility.SoundEffectManager.Play ("se_decide");
			AudioManagerLite.Pause ();
			MiscInformationController.instance.lastLoadedBgmKey = string.Empty;
			fade.Activate (()=>{
				SceneChangePrepare ();
				StartCoroutine (TutorialCoroutine());
			});
			break;
		}
		case MainMenuListInformation.MenuId.SYNC_HOST:
		{
			Utility.SoundEffectManager.Play ("se_decide");
			hostDialog.Show ();
			break;
		}
		case MainMenuListInformation.MenuId.SYNC_CLIENT:
		{
			Utility.SoundEffectManager.Play ("se_decide");
			clientDialog.Show ();
			break;
		}
		}
	}

	private void SceneChangePrepare () {
		MiscInformationController.instance.mainMenuListScrollAmount = (listCtrl.transform as RectTransform).anchoredPosition.y;
	}

	private IEnumerator TutorialCoroutine () {
		MiscInformationController.instance.rhythmGameRequesterSceneName = "tutorial";
		yield return StartCoroutine (TrackInformationController.instance.SetupTutorial ());
		Application.LoadLevel ("SceneRhythmGame");
	}

}
