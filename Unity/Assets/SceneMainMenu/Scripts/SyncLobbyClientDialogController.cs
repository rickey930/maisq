﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SyncLobbyClientDialogController : MonoBehaviour {
	
	[SerializeField]
	private GameObject networkManagerPrefab;
	[SerializeField]
	private GameObject step1;
	[SerializeField]
	private InputField ipAddressTextBox;
	[SerializeField]
	private InputField portNumberTextBox;
	[SerializeField]
	private GameObject step2;
	[SerializeField]
	private GameObject step3;
	[SerializeField]
	private GameObject step4;
	[SerializeField]
	private GameObject step5;
	[SerializeField]
	private GameObject stepDisconnectedFromServer;
	[SerializeField]
	private GameObject syncServerDisconnectDialogObj;

	private GameObject networkManagerObj;
	private SyncLobbyNetworkStateController netCtrl;

	public void Show () {
		SyncLobbyNetworkStateController.DestoryInstance ();
		syncServerDisconnectDialogObj.SetActive (false);

		gameObject.SetActive (true);
		step1.SetActive (true);
		step2.SetActive (false);
		step3.SetActive (false);
		step4.SetActive (false);
		step5.SetActive (false);
		stepDisconnectedFromServer.SetActive (false);
	}

	// リザルトから流れてきてホストは選曲画面だ.
	public void ShowVerAfterResult () {
		gameObject.SetActive (true);
		step1.SetActive (false);
		step2.SetActive (true);
		step3.SetActive (false);
		step4.SetActive (false);
		step5.SetActive (false);
		stepDisconnectedFromServer.SetActive (false);
		networkManagerObj = GameObject.Find (networkManagerPrefab.name + "(Clone)");
		netCtrl = networkManagerObj.GetComponent<SyncLobbyNetworkStateController>();
		netCtrl.onTrackDecided = DownloadTrackInfo;
		StartCoroutine (CheckHostSelectingTrack());
	}
	
	public void Hide () {
		gameObject.SetActive (false);
		step1.SetActive (false);
		step2.SetActive (false);
		step3.SetActive (false);
		step4.SetActive (false);
		step5.SetActive (false);
		stepDisconnectedFromServer.SetActive (false);
	}

	public void OnConnectionButtonClick () {
		int port;
		if (int.TryParse (portNumberTextBox.text, out port)) {
			var error = Network.Connect (ipAddressTextBox.text, port);
			if (error == NetworkConnectionError.NoError) {
				Utility.SoundEffectManager.Play ("se_decide");
				step1.SetActive (false);
				step2.SetActive (true);
				step3.SetActive (false);
				step4.SetActive (false);
				step5.SetActive (false);
				stepDisconnectedFromServer.SetActive (false);
				StartCoroutine (CheckHostSelectingTrack());
			}
			else {
				Debug.LogError (error);
			}
		}
	}

	public void OnCancelButtonClick () {
		Network.Disconnect ();
		Hide ();
	}

	// ホストがロビーのOKボタンを押すのを待っている. -> ホストが曲を選択しています.
	IEnumerator CheckHostSelectingTrack () {
		while (netCtrl == null || !netCtrl.hostSelectingTrack) {
			yield return null;
		}
		step1.SetActive (false);
		step2.SetActive (false);
		step3.SetActive (true);
		step4.SetActive (false);
		step5.SetActive (false);
		stepDisconnectedFromServer.SetActive (false);
		StartCoroutine (CheckHostTrackDecided());
	}
	
	// ホストの曲選択待ち.
	IEnumerator CheckHostTrackDecided () {
		while (netCtrl == null || !netCtrl.hostTrackDecided) {
			if (netCtrl != null && netCtrl.hostDisconnectOrder) {
				Network.Disconnect ();
				yield break;
			}
			yield return null;
		}
		step1.SetActive (false);
		step2.SetActive (false);
		step3.SetActive (false);
		step4.SetActive (true);
		step5.SetActive (false);
		stepDisconnectedFromServer.SetActive (false);
		netCtrl.networkView.RPC ("NotificateClientDownloadStartPrepared", RPCMode.Server);
	}

	// 楽曲情報の受け取り. そもそもbyte[]って受け取れるのか？.
	public void DownloadTrackInfo (string trackInfoJson, string scoreId, string jacketExtension, byte[] jacketData, string audioExtension, byte[] audioData) {
		var trackInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<TrackInformation> (trackInfoJson);
		// リソースデータをコピー.
		if (jacketData != null) {
			System.IO.File.WriteAllBytes (DataPath.GetSyncTempDirectoryPath () + "bg" + jacketExtension, jacketData);
		}
		else {
			System.IO.File.Delete (DataPath.GetSyncTempDirectoryPath () + "bg" + jacketExtension);
		}
		if (audioData != null) {
			System.IO.File.WriteAllBytes (DataPath.GetSyncTempDirectoryPath () + "track" + audioExtension, audioData);
		}
		else {
			System.IO.File.Delete (DataPath.GetSyncTempDirectoryPath () + "track" + audioExtension);
		}

		// トラックデータ.
		StartCoroutine(TrackSetUp(trackInfo, scoreId, jacketExtension, audioExtension));
	}

	IEnumerator TrackSetUp (TrackInformation trackInfo, string scoreId, string jacketExtension, string audioExtension) {
		yield return StartCoroutine (TrackInformationController.instance.Setup (trackInfo, scoreId, jacketExtension, audioExtension));

		// ダウンロードが終わったらホストに通知.
		step1.SetActive (false);
		step2.SetActive (false);
		step3.SetActive (false);
		step4.SetActive (false);
		step5.SetActive (true);
		stepDisconnectedFromServer.SetActive (false);
		netCtrl.networkView.RPC ("NotificateClientDownloadCompleted", RPCMode.Server);
		StartCoroutine (CheckHostRhythmGameWait());
	}
	
	// ホストのシーン切り替え待ち.
	IEnumerator CheckHostRhythmGameWait () {
		while (netCtrl == null || !netCtrl.hostRhythmGameSceneMoved) {
			yield return null;
		}
		// ホストがリズムゲームシーンに入ったらクライアントもリズムゲームシーンに入る.
		MiscInformationController.instance.syncMode = 2; //シンクモードかつクライアントである.
		Application.LoadLevel ("SceneRhythmGame");
	}

	// サーバーにつながったメッセージ.
	void OnConnectedToServer() {
		StartCoroutine (CreateObjectCliendSide ());
	}

	private IEnumerator CreateObjectCliendSide () {
		while ((networkManagerObj = GameObject.Find (networkManagerPrefab.name + "(Clone)")) == null) {
			Debug.Log ("not found");
			yield return null;
		}
		Debug.Log ("find");
		netCtrl = networkManagerObj.GetComponent<SyncLobbyNetworkStateController>();
		while (netCtrl.hostVersion == 0) {
			yield return null;
		}
		if (netCtrl.hostVersion == Constants.instance.SYNC_MODE_VERSION) {
			// ホストから受け取る情報を初期化する.
			netCtrl.ResetHostFlags ();
			netCtrl.onTrackDecided = DownloadTrackInfo;
			// RPCを飛ばしてホストに接続を通達する.
			netCtrl.networkView.RPC ("NotificateClientConnected", RPCMode.Server);
		}
	}

	// サーバーとの通信が切断されたメッセージ
	void OnDisconnectedFromServer () {
		step1.SetActive (false);
		step2.SetActive (false);
		step3.SetActive (false);
		step4.SetActive (false);
		step5.SetActive (false);
		stepDisconnectedFromServer.SetActive (true);
	}
}
