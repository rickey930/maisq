﻿using UnityEngine;
using UnityEngine.UI;
using System.Net.Sockets;
using System.Net;
using System.Collections;

public class SyncLobbyHostDialogController : MonoBehaviour {

	[SerializeField]
	private GameObject networkManagerPrefab;
	[SerializeField]
	private GameObject step1;
	[SerializeField]
	private Text ipAddressLabel;
	[SerializeField]
	private Text portNumberLabel;
	[SerializeField]
	private GameObject step2;
	[SerializeField]
	private GameObject syncServerDisconnectDialogObj;
	
	private GameObject networkManagerObj;
	private SyncLobbyNetworkStateController netCtrl;

	public void Show () {
		SyncLobbyNetworkStateController.DestoryInstance ();
		syncServerDisconnectDialogObj.SetActive (true);

		gameObject.SetActive (true);
		step1.SetActive (true);
		step2.SetActive (false);

		// IPアドレスの取得 (…はUnityの機能にあるっぽいからコメントアウト).
//		var hostname = Dns.GetHostName ();
//		var ipAddresses = Dns.GetHostAddresses (hostname);
//		ipAddressLabel.text = string.Empty;
//		foreach (var ipAddress in ipAddresses) {
//			if (ipAddressLabel.text == null)
//			if (ipAddressLabel.text != string.Empty) {
//				ipAddressLabel.text += "\n";
//			}
//			ipAddressLabel.text += ipAddress;
//		}
		
		// ポート番号は自分で任意に決めることができるらしいのでランダムで生成.
		int portNumber = Random.Range (1024, 49152);
		Network.InitializeServer(1, portNumber, false);
		
		ipAddressLabel.text = Network.player.ipAddress;
		portNumberLabel.text = Network.player.port.ToString ();

		// クライアントの接続チェック.
		StartCoroutine (CheckClientConnection ());
	}

	public void Hide () {
		gameObject.SetActive (false);
		step1.SetActive (false);
		step2.SetActive (false);
	}
	
	public void OnCancelButtonClick () {
		if (networkManagerObj != null) {
			Network.Destroy (networkManagerObj);
			networkManagerObj = null;
		}
		Hide ();
	}
	
	public void OnOkButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		MiscInformationController.instance.syncMode = 1; //シンクモードかつホストである.
		MiscInformationController.instance.selectMusicVoiceCallRequest = true;
		Application.LoadLevel ("SceneSelectTrack");
	}

	IEnumerator CheckClientConnection () {
		while (!netCtrl.clientConnected) {
			yield return null;
		}
		step1.SetActive (false);
		step2.SetActive (true);
	}

	// サーバーをちゃんと建てられたメッセージ.
	void OnServerInitialized() {
		CreateObjectServerSide();
	}

	private void CreateObjectServerSide () {
		networkManagerObj = Network.Instantiate(networkManagerPrefab, networkManagerPrefab.transform.position, networkManagerPrefab.transform.rotation, 1) as GameObject;
		netCtrl = networkManagerObj.GetComponent <SyncLobbyNetworkStateController>();
		netCtrl.networkView.RPC ("NotificateHostVersion", RPCMode.AllBuffered, Constants.instance.SYNC_MODE_VERSION);
		// クライアントから受け取る情報を初期化する.
		// まだクライアントがいない前提なので、RPCではなく直接呼ぶ.
		netCtrl.ResetClientFlags ();
	}
}
