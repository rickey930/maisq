﻿using UnityEngine;
using System.Collections;

public class SyncLobbyNetworkStateController : SingletonMonoBehaviour<SyncLobbyNetworkStateController> {
	/// <summary>
	/// ホストのシンクモードバージョン番号.
	/// </summary>
	public int hostVersion { get; set; }
	/// <summary>
	/// クライアントがホストに接続した. (Client RPC > Host)
	/// </summary>
	public bool clientConnected { get; set; }
	/// <summary>
	/// ホストがトラックセレクトシーンに遷移した. (Host RPC > Client)
	/// </summary>
	public bool hostSelectingTrack { get; set; }
	/// <summary>
	/// ホストがスコアを決定した. (Host RPC > Client)
	/// </summary>
	public bool hostTrackDecided { get; set; }
	/// <summary>
	/// クライアントが楽曲情報のダウンロードの準備を完了した. (Client RPC > Host)
	/// </summary>
	public bool clientDownloadStartPrepared { get; set; }
	// ホストからクライアントへ楽曲データを送るためのデリゲート.
	public delegate void Action<T1, T2, T3, T4, T5, T6> (T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6);
	public Action<string, string, string, byte[], string, byte[]> onTrackDecided;
	/// <summary>
	/// クライアントが楽曲情報のダウンロードを終えた. (Client RPC > Host)
	/// </summary>
	public bool clientDownloadCompleted { get; set; }
	/// <summary>
	/// ホストがリズムゲームシーンに遷移した. (Host RPC > Client)
	/// </summary>
	public bool hostRhythmGameSceneMoved { get; set; }
	/// <summary>
	/// クライアントがリズムゲームシーンに遷移した. (Client RPC > Host)
	/// </summary>
	public bool clientRhythmGameSceneMoved { get; set; }
	/// <summary>
	/// ホストがリズムゲームシーンに遷移した. (Host RPC > Client)
	/// </summary>
	public bool hostRhythmGameWait { get; set; }
	/// <summary>
	/// クライアントがリズムゲームシーンに遷移した. (Client RPC > Host)
	/// </summary>
	public bool clientRhythmGameWait { get; set; }
	/// <summary>
	/// ホストがリズムゲームを終えて待っている. (Host RPC > Client)
	/// </summary>
	public bool hostRhythmGameEndWait { get; set; }
	/// <summary>
	/// クライアントがリズムゲームを終えて待っている. (Client RPC > Host)
	/// </summary>
	public bool clientRhythmGameEndWait { get; set; }
	/// <summary>
	/// <para>ホストがリザルト後の挙動を決定した. (Host RPC > Client)</para>
	/// <para>0 = 未決定</para>
	/// <para>1 = リトライ</para>
	/// <para>2 = 選曲</para>
	/// <para>3 = 切断</para>
	/// </summary>
	public int hostResultNextSceneOrder { get; set; }
	/// <summary>
	/// クライアントがリザルトでホストの命令を待っている. (Client RPC > Host)
	/// </summary>
	public bool clientResultWait { get; set; }
	/// <summary>
	/// ホストがクライアントに切断命令を下した. (Host RPC > Client)
	/// </summary>
	public bool hostDisconnectOrder { get; set; }

	// サーバーをたてたときに呼び出す.
	public void ResetClientFlags () {
		clientConnected = false;
		clientDownloadStartPrepared = false;
		clientDownloadCompleted = false;
		clientRhythmGameSceneMoved = false;
		clientRhythmGameWait = false;
		clientRhythmGameEndWait = false;
		clientResultWait = false;
	}

	// ホストにつないだに呼び出す.
	public void ResetHostFlags () {
		hostSelectingTrack = false;
		hostTrackDecided = false;
		hostRhythmGameSceneMoved = false;
		hostRhythmGameWait = false;
		hostRhythmGameEndWait = false;
		hostResultNextSceneOrder = 0;
		hostDisconnectOrder = false;
	}
	
	// 再曲選択時にホストが呼び出す.
	public void ResetClientFlagsFromReselectTrack () {
		clientDownloadStartPrepared = false;
		clientDownloadCompleted = false;
		clientRhythmGameSceneMoved = false;
		clientRhythmGameWait = false;
		clientRhythmGameEndWait = false;
		clientResultWait = false;
	}
	
	// 再曲選択時にクライアントが呼び出す.
	public void ResetHostFlagsFromReselectTrack () {
		hostSelectingTrack = false;
		hostTrackDecided = false;
		hostRhythmGameSceneMoved = false;
		hostRhythmGameWait = false;
		hostRhythmGameEndWait = false;
		hostResultNextSceneOrder = 0;
		hostDisconnectOrder = false;
	}

	// リトライ時にホストが呼び出す.
	public void ResetClientFlagsFromRetry () {
		clientRhythmGameSceneMoved = false;
		clientRhythmGameWait = false;
		clientRhythmGameEndWait = false;
		clientResultWait = false;
	}
	
	// リトライ時にクライアントが呼び出す.
	public void ResetHostFlagsFromRetry () {
		hostRhythmGameSceneMoved = false;
		hostRhythmGameWait = false;
		hostRhythmGameEndWait = false;
		hostResultNextSceneOrder = 0;
		hostDisconnectOrder = false;
	}
	
	[RPC]
	public void NotificateHostVersion (int version) {
		hostVersion = version;
	}
	
	[RPC]
	public void NotificateClientConnected () {
		clientConnected = true;
	}

	[RPC]
	public void NotificateHostSelectingTrack () {
		hostSelectingTrack = true;
	}
	
	[RPC]
	public void NotificateHostTrackDecided () {
		hostTrackDecided = true;
	}

	[RPC]
	public void NotificateClientDownloadStartPrepared () {
		clientDownloadStartPrepared = true;
	}

	[RPC]
	public void NotificateHostTrackDownload (byte[] trackInfoJsonBytes, string scoreId, string jacketExtension, byte[] jacketData, bool useJacket, string audioExtension, byte[] audioData, bool useAudio) {
		if (onTrackDecided != null) {
			string trackInfoJson = System.Text.Encoding.UTF8.GetString(trackInfoJsonBytes);
			if (!useJacket) jacketData = null;
			if (!useAudio) audioData = null;
			onTrackDecided (trackInfoJson, scoreId, jacketExtension, jacketData, audioExtension, audioData);
		}
	}
	
	[RPC]
	public void NotificateClientDownloadCompleted () {
		clientDownloadCompleted = true;
	}
	
	[RPC]
	public void NotificateHostRhythmGameSceneMoved () {
		hostRhythmGameSceneMoved = true;
	}
	
	[RPC]
	public void NotificateClientRhythmGameSceneMoved () {
		clientRhythmGameSceneMoved = true;
	}
	
	[RPC]
	public void NotificateHostRhythmGameWait () {
		hostRhythmGameWait = true;
	}
	
	[RPC]
	public void NotificateClientRhythmGameWait () {
		clientRhythmGameWait = true;
	}
	
	[RPC]
	public void NotificateHostRhythmGameEndWait () {
		hostRhythmGameEndWait = true;
	}
	
	[RPC]
	public void NotificateClientRhythmGameEndWait () {
		clientRhythmGameEndWait = true;
	}
	
	[RPC]
	public void NotificateHostResultNextSceneOrder (int order) {
		hostResultNextSceneOrder = order;
	}
	
	[RPC]
	public void NotificateClientResultWait () {
		clientResultWait = true;
	}
	
	[RPC]
	public void NotificateHostDisconnectOrder () {
		hostDisconnectOrder = true;
	}

	// サーバーから切断されたら直接破棄する.
	void OnDisconnectedFromServer () {
		Destroy (gameObject);
	}
}
