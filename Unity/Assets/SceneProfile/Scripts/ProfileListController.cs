﻿using UnityEngine;
using System.Collections;

public class ProfileListController : MonoBehaviour, IInfiniteScrollSetup {
	
	[SerializeField]
	public ProfileSceneManager manager;
	
	#region IInfiniteScrollSetup
	public bool forceUpdateContents { 
		get { return manager.forceUpdateContents; } 
		set { manager.forceUpdateContents = value; }
	}
	public bool setupCompleted { get; set; }
	
	public IEnumerator OnPostSetupItems() {
		// リストの初期化.
		while (manager.contents == null)
			yield return null;
		ResizeScrollView ();
		setupCompleted = true;
	}
	
	private void ResizeScrollView () {
		var infiniteScroll = GetComponent<InfiniteScroll> ();
		var rectTransform = GetComponent<RectTransform> ();
		var delta = rectTransform.sizeDelta;
		delta.y = (infiniteScroll.ItemScale * (manager.contents.Length)) + (infiniteScroll.AnchordMargin * 2);
		rectTransform.sizeDelta = delta;
	}
	
	public void OnUpdateItem (int index, GameObject obj) {
		if (manager.contents == null || index < 0 || index >= manager.contents.Length) {
			if (obj.activeSelf) {
				obj.SetActive (false);
			}
		}
		else {
			if (!obj.activeSelf) {
				obj.SetActive (true);
			}
			var item = obj.GetComponent<ProfileNodeController> ();
			item.UpdateItem (manager.contents [index]);
		}
	}
	#endregion
}
