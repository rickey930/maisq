﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProfileNodeController : MonoBehaviour {
	public ProfileListInformation info { get; set; }
	public ProfileListController manager;
	public Image button;
	public Text configName;
	public Text configValue;
	public Image configPicture;
	
	public void UpdateItem (ProfileListInformation info) {
		this.info = info;
		if (manager.manager.selectedId == info.id) {
			float alpha = button.color.a;
			button.color = MaimaiStyleDesigner.ByteToPercentRGBA(255, 150, 0, (255 * alpha).ToInt());
		}
		else {
			float alpha = button.color.a;
			button.color = MaimaiStyleDesigner.ByteToPercentRGBA(255, 255, 255, (255 * alpha).ToInt());
		}
		configName.text = info.key;
		if (info.useText) {
			configValue.text = info.value;
			configValue.color = info.valueColor;
			configValue.gameObject.SetActive (true);
		}
		else {
			configValue.gameObject.SetActive (false);
		}
		if (info.usePicture) {
			configPicture.sprite = info.sprite;
			configPicture.gameObject.SetActive (true);
		}
		else {
			configPicture.gameObject.SetActive (false);
		}
	}
	
	public void OnButtonClick () {
		if (manager.manager.selectedId == info.id) {
			manager.manager.OnDoubleTappedCell (info);
		}
		else {
			manager.manager.selectedId = info.id;
			manager.forceUpdateContents = true;
			manager.manager.OnTappedCell (info);
		}
	}
}
