﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class ProfileSceneDetailUIController : ConfigSceneDetailUIController {
	public GameObject inputFiledAndColorsObj;
	public InputField textBox2;
	public Text colorSampleLabel;
	public Slider textColorRElementSlider;
	public Slider textColorGElementSlider;
	public Slider textColorBElementSlider;

	protected Action<string, float, float, float> onOKCallbackTextAndColorAssign;

	public override void Show (string itemName, int initialValue, Action<int> onOKCallback, int switchValueMax, Func<int, string> onGetName) {
		onOKCallbackTextAndColorAssign = null;
		inputFiledAndColorsObj.gameObject.SetActive (false);
		base.Show (itemName, initialValue, onOKCallback, switchValueMax, onGetName);
	}
	
	public override void Show (string itemName, bool initialValue, Action<bool> onOKCallback, bool checkBoxLabelTypeIsYesNo = false) {
		onOKCallbackTextAndColorAssign = null;
		inputFiledAndColorsObj.gameObject.SetActive (false);
		base.Show (itemName, initialValue, onOKCallback, checkBoxLabelTypeIsYesNo);
	}
	
	public override void Show (string itemName, float initialValue, Action<float> onOKCallback, float min, float max, Func<float, string> onGetName, bool wholeNumbers) {
		onOKCallbackTextAndColorAssign = null;
		inputFiledAndColorsObj.gameObject.SetActive (false);
		base.Show (itemName, initialValue, onOKCallback, min, max, onGetName, wholeNumbers);
	}

	public override void Show (string itemName, string initialValue, Action<string> onOKCallback) {
		onOKCallbackTextAndColorAssign = null;
		inputFiledAndColorsObj.gameObject.SetActive (false);
		base.Show (itemName, initialValue, onOKCallback);
	}
	
	public virtual void Show (string itemName, string initialValue, float r, float g, float b, Action<string, float, float, float> onOKCallback) {
		onOKCallbackTextAndColorAssign = onOKCallback;
		textColorRElementSlider.value = r;
		textColorBElementSlider.value = b;
		textColorGElementSlider.value = g;
		inputFiledAndColorsObj.gameObject.SetActive (true);

		itemNameLabel.text = itemName;
		onOKCallbackTypeAssign = null;
		onOKCallbackSwitchAssign = null;
		onOKCallbackValueAssign = null;
		onOKCallbackTextAssign = null;
		onValueChangedGetName = null;
		onFValueChangedGetName = null;
		textBox2.text = initialValue ?? string.Empty;
		OnColorSliderValueChanged ();
		switchObj.SetActive (false);
		checkBox.gameObject.SetActive (false);
		slider.gameObject.SetActive (false);
		textBox.gameObject.SetActive (false);
		gameObject.SetActive (true);
	}
	
	public override void OnOKButtonClick () {
		if (onOKCallbackTextAndColorAssign != null) {
			onOKCallbackTextAndColorAssign (textBox2.text, textColorRElementSlider.value, textColorGElementSlider.value, textColorBElementSlider.value);
		}
		base.OnOKButtonClick ();
	}

	public void OnColorSliderValueChanged () {
		colorSampleLabel.color = new Color (textColorRElementSlider.value, textColorGElementSlider.value, textColorBElementSlider.value, 1);
	}

}
