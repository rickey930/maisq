using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class ProfileSceneManager : MonoBehaviour {
	
	[SerializeField]
	private ProfileListController listCtrl;
	[SerializeField]
	private UnityEngine.UI.Image guideCircle;
	[SerializeField]
	private GameObject sensorUIPrefab;
	[SerializeField]
	private RectTransform sensorUIParent;
	[SerializeField]
	private ProfileSceneDetailUIController detailUICtrl;
	[SerializeField]
	private GameObject pathSelectDialogPrefab;
	[SerializeField]
	private Transform pathSelectDialogParent;
	[SerializeField]
	private GameObject yesNoDialogPrefab;
	[SerializeField]
	private Transform yesNoDialogParent;
	[SerializeField]
	private FadeController fade;

	private Sprite[] sensorIcons;
	
	private Dictionary<ProfileCategory.CategoryId, ProfileListInformation> profileContents { get; set; }
	public ProfileListInformation[] contents { get; set; }
	public bool forceUpdateContents { get; set; }
	
	private UserInformation profileWork { get; set; }
	private SensorUIImageController[] sensorImages { get; set; }
	public ProfileCategory.CategoryId selectedId { get; set; }
	private float sensorTurboSpeed0 { get; set; }
	private float sensorTurboSpeed3 { get; set; }
	private float sensorTurboFirstSpeed { get { return 65.0f; } }
	private float sensorTurboAddSpeed { get { return 180.0f * Time.deltaTime; } }
	private float sensorTurboMaxSpeed { get { return sensorTurboFirstSpeed * 1.5f; } }

	private bool isSortRefineChanged { get; set; }

	// Use this for initialization
	IEnumerator Start () {
		while (UserDataController.instance == null || UserDataController.instance.user == null) {
			yield return null;
		}
		
		sensorIcons = new Sprite[] {
			SpriteTank.Get ("icon_allow_up"),
			SpriteTank.Get ("icon_level_plus"),
			SpriteTank.Get ("icon_level_minus"),
			SpriteTank.Get ("icon_allow_down"),
			SpriteTank.Get ("icon_step_decide"),
			SpriteTank.Get ("icon_step_cancel"),
			SpriteTank.Get ("icon_reset"),
			SpriteTank.Get ("icon_option"),
		};

		isSortRefineChanged = false;
		selectedId = ProfileCategory.CategoryId.NONE;
		profileWork = UserInformation.Clone (UserDataController.instance.user);
		
		profileContents = new Dictionary<ProfileCategory.CategoryId, ProfileListInformation> () {
			{ ProfileCategory.CategoryId.PLAYER_NAME, new ProfileListInformation (ProfileCategory.CategoryId.PLAYER_NAME, "Player Name", profileWork.user_name.ToProfileValueString(), profileWork.user_name.ToProfileValueColor(), true, false) },
			{ ProfileCategory.CategoryId.PLAYER_TITLE, new ProfileListInformation (ProfileCategory.CategoryId.PLAYER_TITLE, "Player Title", profileWork.user_title.ToProfileValueString(), new Color (profileWork.user_title_color_r, profileWork.user_title_color_g, profileWork.user_title_color_b, 1), true, false) },
			{ ProfileCategory.CategoryId.PLAYER_ICON, new ProfileListInformation (ProfileCategory.CategoryId.PLAYER_ICON, "Player Icon", string.Empty.ToProfileValueString(), string.Empty.ToProfileValueColor(), true, false) },
			{ ProfileCategory.CategoryId.PLAYER_PLATE, new ProfileListInformation (ProfileCategory.CategoryId.PLAYER_PLATE, "Player Plate", string.Empty.ToProfileValueString(), string.Empty.ToProfileValueColor(), true, false) },
			{ ProfileCategory.CategoryId.PLAYER_FRAME, new ProfileListInformation (ProfileCategory.CategoryId.PLAYER_FRAME, "Player Frame", string.Empty.ToProfileValueString(), string.Empty.ToProfileValueColor(), true, false) },
			{ ProfileCategory.CategoryId.SORT_TYPE, new ProfileListInformation (ProfileCategory.CategoryId.SORT_TYPE, "Sort Type", profileWork.sort_type.ToSortLevels().ToString(), Color.red, true, false) },
			{ ProfileCategory.CategoryId.REFINE_LEVEL, new ProfileListInformation (ProfileCategory.CategoryId.REFINE_LEVEL, "Refine Level", profileWork.refine_level.ToProfileValueString(), profileWork.refine_level.ToProfileValueColor(), true, false) },
			{ ProfileCategory.CategoryId.REFINE_DIFFICULTY, new ProfileListInformation (ProfileCategory.CategoryId.REFINE_DIFFICULTY, "Refine Difficulty", profileWork.refine_difficulty.ToProfileValueString(), profileWork.refine_difficulty.ToProfileValueColor(), true, false) },
			{ ProfileCategory.CategoryId.REFINE_NOTES_DESIGNER, new ProfileListInformation (ProfileCategory.CategoryId.REFINE_NOTES_DESIGNER, "Refine Notes Designer", profileWork.refine_designer.ToProfileValueString(), profileWork.refine_designer.ToProfileValueColor(), true, false) },
			{ ProfileCategory.CategoryId.REFINE_NO_MISS, new ProfileListInformation (ProfileCategory.CategoryId.REFINE_NO_MISS, "Refine Full Combo", profileWork.refine_no_miss.ToConditionAchievedString(), profileWork.refine_no_miss.ToConfigValueColor(), true, false) },
			{ ProfileCategory.CategoryId.REFINE_FC, new ProfileListInformation (ProfileCategory.CategoryId.REFINE_FC, "Refine Full Combo (Gold)", profileWork.refine_fc.ToConditionAchievedString(), profileWork.refine_fc.ToConfigValueColor(), true, false) },
			{ ProfileCategory.CategoryId.REFINE_AP, new ProfileListInformation (ProfileCategory.CategoryId.REFINE_AP, "Refine All Perfect", profileWork.refine_ap.ToConditionAchievedString(), profileWork.refine_ap.ToConfigValueColor(), true, false) },
		};
		var loadSpriteProcInfo = new Dictionary<string, object>[3] {
			new Dictionary<string, object> () { { "id", ProfileCategory.CategoryId.PLAYER_ICON }, { "extension", profileWork.user_icon_extension }, { "path", DataPath.GetPlayerIconPath (profileWork.user_icon_extension) } },  
			new Dictionary<string, object> () { { "id", ProfileCategory.CategoryId.PLAYER_PLATE }, { "extension", profileWork.user_plate_extension }, { "path", DataPath.GetPlayerPlatePath (profileWork.user_plate_extension) } }, 
			new Dictionary<string, object> () { { "id", ProfileCategory.CategoryId.PLAYER_FRAME }, { "extension", profileWork.user_frame_extension }, { "path", DataPath.GetPlayerFramePath (profileWork.user_frame_extension) } }, 
		};
		foreach (var info in loadSpriteProcInfo) {
			var pContent = profileContents[(ProfileCategory.CategoryId)info["id"]];
			string extension = (string)info["extension"];
			bool exists = File.Exists ((string)info["path"]);
			if (exists) {
				StartCoroutine (pContent.LoadSprite(this, extension, (data)=>{
					if (data != null) {
						forceUpdateContents = true;
					}
				}));
			}
		}
		contents = new ProfileListInformation[profileContents.Values.Count];
		profileContents.Values.CopyTo (contents, 0);
		
		float radius = (Constants.instance.MAIMAI_OUTER_RADIUS + 5) * 2;
		guideCircle.transform.RectCast ().sizeDelta = new Vector2 (radius, radius);
		while (MaipadDynamicCreatedSpriteTank.instance.guideCircleMenu == null) {
			yield return null;
		}
		guideCircle.sprite = MaipadDynamicCreatedSpriteTank.instance.guideCircleMenu;
		CreateSensors ();

		if (MiscInformationController.instance.lastLoadedBgmKey != "maisq_bgm_main") {
			AudioManagerLite.Pause ();
			AudioManagerLite.Load (Utility.SoundEffectManager.GetClip ("maisq_bgm_main"));
			MiscInformationController.instance.lastLoadedBgmKey = "maisq_bgm_main";
			AudioManagerLite.Seek (0);
			AudioManagerLite.Play (true);
		}
		
		while (!listCtrl.setupCompleted) {
			yield return null;
		}
		(listCtrl.transform as RectTransform).anchoredPosition = new Vector2 (0, UserDataController.instance.user.last_profile_list_scroll);
	}
	
	private void CreateSensors() {
		sensorImages = new SensorUIImageController[8];
		for (int i = 0; i < 8; i++) {
			var sensorImageObj = Instantiate(sensorUIPrefab) as GameObject;
			sensorImageObj.name = "Sensor for UI " + (i + 1).ToString();
			sensorImageObj.SetParentEx (sensorUIParent);
			var sensorImage = sensorImageObj.GetComponent<SensorUIImageController>();
			sensorImage.SetPosition (Constants.instance.GetPieceDegree(i), Constants.instance.MAIMAI_OUTER_RADIUS);
			sensorImage.Setup(sensorIcons[i]);
			sensorImages[i] = sensorImage;
		}
		
		// Scroll Up.
		sensorImages [0].onSensorHoldDownRepeat = () => {
			Utility.SoundEffectManager.Play ("se_select");
			float speed;
			if (sensorTurboSpeed0 == 0) {
				speed = sensorTurboFirstSpeed;
			}
			else {
				speed = sensorTurboSpeed0;
			}
			sensorTurboSpeed0 += sensorTurboAddSpeed;
			if (speed > sensorTurboMaxSpeed) speed = sensorTurboMaxSpeed;
			var rect = listCtrl.transform.RectCast ();
			rect.anchoredPosition = new Vector2 (0, rect.anchoredPosition.y - speed);
		};
		sensorImages [0].onSensorHoldUp = () => {
			sensorTurboSpeed0 = 0;
		};
		// Level Up.
		sensorImages [1].onSensorClick = ()=>OnPlusMinusButtonClick(true);
		// Level Down.
		sensorImages [2].onSensorClick = ()=>OnPlusMinusButtonClick(false);
		// Scroll Down.
		sensorImages [3].onSensorHoldDownRepeat = () => {
			Utility.SoundEffectManager.Play ("se_select");
			float speed;
			if (sensorTurboSpeed3 == 0) {
				speed = sensorTurboFirstSpeed;
			}
			else {
				speed = sensorTurboSpeed3;
			}
			sensorTurboSpeed3 += sensorTurboAddSpeed;
			if (speed > sensorTurboMaxSpeed) speed = sensorTurboMaxSpeed;
			var rect = listCtrl.transform.RectCast ();
			rect.anchoredPosition = new Vector2 (0, rect.anchoredPosition.y + speed);
		};
		sensorImages [3].onSensorHoldUp = () => {
			sensorTurboSpeed3 = 0;
		};
		// OK.
		System.Action reference = () => {
			string oldName = UserDataController.instance.user.user_name;
			string oldTitle = UserDataController.instance.user.user_title;
			float oldTitleColorR = UserDataController.instance.user.user_title_color_r;
			float oldTitleColorG = UserDataController.instance.user.user_title_color_g;
			float oldTitleColorB = UserDataController.instance.user.user_title_color_b;
			UserDataController.instance.user.Copy (profileWork);
			UserDataController.instance.user.last_profile_list_scroll = (listCtrl.transform as RectTransform).anchoredPosition.y;
			if (isSortRefineChanged) {
				UserDataController.instance.user.last_played_track_list_scroll = 0;
			}
			if (oldName != UserDataController.instance.user.user_name) {
				ProfileInformationController.instance.PlayerNameReload ();
			}
			if (oldTitle != UserDataController.instance.user.user_title ||
			    oldTitleColorR != UserDataController.instance.user.user_title_color_r || 
			    oldTitleColorG != UserDataController.instance.user.user_title_color_g || 
			    oldTitleColorB != UserDataController.instance.user.user_title_color_b) {
				ProfileInformationController.instance.PlayerTitleReload ();
			}
			UserDataController.instance.SaveUserInfo ();
		};
		sensorImages [4].onSensorClick = () => {
			Utility.SoundEffectManager.Play ("se_decide");
			reference ();
			fade.Activate (()=>{
				string sceneName;
				if (MiscInformationController.instance != null && !string.IsNullOrEmpty(MiscInformationController.instance.optionSceneCallbackSceneName)) {
					sceneName = MiscInformationController.instance.optionSceneCallbackSceneName;
				}
				else {
					sceneName = "SceneSelectTrack";
				}
				Application.LoadLevel (sceneName);
			});
		};
		// Cancel.
		sensorImages [5].onSensorClick = () => {
			Utility.SoundEffectManager.Play ("se_decide");
			UserDataController.instance.user.last_profile_list_scroll = (listCtrl.transform as RectTransform).anchoredPosition.y;
			fade.Activate(()=>{
				string sceneName;
				if (MiscInformationController.instance != null && !string.IsNullOrEmpty(MiscInformationController.instance.optionSceneCallbackSceneName)) {
					sceneName = MiscInformationController.instance.optionSceneCallbackSceneName;
				}
				else {
					sceneName = "SceneSelectTrack";
				}
				Application.LoadLevel (sceneName);
			});
		};
		// Reset.
		sensorImages [6].onSensorClick = () => {
			Utility.SoundEffectManager.Play ("se_decide");
			profileWork.sort_type = 0;
			profileWork.refine_level = string.Empty;
			profileWork.refine_difficulty = string.Empty;
			profileWork.refine_designer = string.Empty;
			profileWork.refine_no_miss = 0;
			profileWork.refine_fc = 0;
			profileWork.refine_ap = 0;
			string cellValue;
			Color cellValueColor;
			ProfileCategory.CategoryId[] ids = new ProfileCategory.CategoryId[] {
				ProfileCategory.CategoryId.SORT_TYPE,
				ProfileCategory.CategoryId.REFINE_LEVEL,
				ProfileCategory.CategoryId.REFINE_DIFFICULTY,
				ProfileCategory.CategoryId.REFINE_NOTES_DESIGNER,
				ProfileCategory.CategoryId.REFINE_NO_MISS,
				ProfileCategory.CategoryId.REFINE_AP,
			};
			foreach (var id in ids) {
				GatherContentData (id, out cellValue, out cellValueColor);
				profileContents[id].SetValue (cellValue, cellValueColor);
			}
			forceUpdateContents = true;
		};
		// Config.
		sensorImages [7].onSensorClick = () => {
			Utility.SoundEffectManager.Play ("se_decide");
			reference ();
			fade.Activate (()=>{
				Application.LoadLevel ("SceneConfig");
			});
		};
	}
	
	public void OnTappedCell (ProfileListInformation info) {
		Utility.SoundEffectManager.Play ("se_decide");
	}
	
	public void OnDoubleTappedCell (ProfileListInformation info) {
		Utility.SoundEffectManager.Play ("se_decide");
		var content = profileContents [selectedId];
		System.Action contentUpdate = () => {
			string cellValue;
			Color cellValueColor;
			GatherContentData (info.id, out cellValue, out cellValueColor);
			content.SetValue (cellValue, cellValueColor);
			forceUpdateContents = true;
		};
		
		switch (info.id) {
		case ProfileCategory.CategoryId.PLAYER_NAME:
		{
			detailUICtrl.Show (info.key,
			                   profileWork.user_name,
			                   (result)=> { 
									if (string.IsNullOrEmpty (result)) {
										profileWork.user_name = UserInformation.DEFAULT_USER_NAME;
									}
									else {
										profileWork.user_name = result; 
									}
									contentUpdate(); }
								);
			break;
		}
		case ProfileCategory.CategoryId.PLAYER_TITLE:
		{
			detailUICtrl.Show (info.key,
			                   profileWork.user_title,
			                   profileWork.user_title_color_r,
			                   profileWork.user_title_color_g,
			                   profileWork.user_title_color_b,
			                   (result, r, g, b)=> {
									if (string.IsNullOrEmpty (result)) {
										profileWork.user_title = UserInformation.DEFAULT_USER_TITLE;
									}
									else {
										profileWork.user_title = result; 
									}
									profileWork.user_title_color_r = r;
									profileWork.user_title_color_g = g;
									profileWork.user_title_color_b = b;
									contentUpdate();
								}
			);
			break;
		}
		case ProfileCategory.CategoryId.PLAYER_ICON:
		{
			var dialog = Instantiate (pathSelectDialogPrefab) as GameObject;
			dialog.SetParentEx (pathSelectDialogParent);
			var ctrl = dialog.GetComponent<PathSelectDialogListControllerHolder> ().controller;
			string lastImportedPathDirectory = profileWork.last_profile_imported_path;
			ctrl.Setup (lastImportedPathDirectory, (path)=>{
				profileWork.last_profile_imported_path = path;
				string extension = System.IO.Path.GetExtension (path);
				var pContent = profileContents[ProfileCategory.CategoryId.PLAYER_ICON];
				pContent.sprite = null;
				// アイコンなどリソース系は変更した時点で確定する.
				profileWork.user_icon_extension = extension;
				UserDataController.instance.user.user_icon_extension = extension;
				UserDataController.instance.SaveUserInfo ();
				string destPath = DataPath.GetPlayerIconPath (profileWork.user_icon_extension);
				File.Copy (path, destPath, true);
				#if !UNITY_EDITOR && UNITY_IOS
				iPhone.SetNoBackupFlag (destPath);
				#endif
				ProfileInformationController.instance.PlayerIconReload ();
				pContent.usePicture = true;
				pContent.useText = false;
				StartCoroutine (pContent.LoadSprite(this, extension, (data)=>{
					forceUpdateContents = true;
				}));
			});
			break;
		}
		case ProfileCategory.CategoryId.PLAYER_PLATE:
		{
			var dialog = Instantiate (pathSelectDialogPrefab) as GameObject;
			dialog.SetParentEx (pathSelectDialogParent);
			var ctrl = dialog.GetComponent<PathSelectDialogListControllerHolder> ().controller;
			string lastImportedPathDirectory = profileWork.last_profile_imported_path;
			ctrl.Setup (lastImportedPathDirectory, (path)=>{
				profileWork.last_profile_imported_path = path;
				string extension = System.IO.Path.GetExtension (path);
				var pContent = profileContents[ProfileCategory.CategoryId.PLAYER_PLATE];
				pContent.sprite = null;
				profileWork.user_plate_extension = extension;
				UserDataController.instance.user.user_plate_extension = extension;
				UserDataController.instance.SaveUserInfo ();
				string destPath = DataPath.GetPlayerPlatePath (profileWork.user_plate_extension);
				File.Copy (path, destPath, true);
				#if !UNITY_EDITOR && UNITY_IOS
				iPhone.SetNoBackupFlag (destPath);
				#endif
				ProfileInformationController.instance.PlayerPlateReload ();
				pContent.usePicture = true;
				pContent.useText = false;
				StartCoroutine (pContent.LoadSprite(this, extension, (data)=>{
					forceUpdateContents = true;
				}));
			});
			break;
		}
		case ProfileCategory.CategoryId.PLAYER_FRAME:
		{
			var dialog = Instantiate (pathSelectDialogPrefab) as GameObject;
			dialog.SetParentEx (pathSelectDialogParent);
			var ctrl = dialog.GetComponent<PathSelectDialogListControllerHolder> ().controller;
			string lastImportedPathDirectory = profileWork.last_profile_imported_path;
			ctrl.Setup (lastImportedPathDirectory, (path)=>{
				profileWork.last_profile_imported_path = path;
				string extension = System.IO.Path.GetExtension (path);
				var pContent = profileContents[ProfileCategory.CategoryId.PLAYER_FRAME];
				pContent.sprite = null;
				profileWork.user_frame_extension = extension;
				UserDataController.instance.user.user_frame_extension = extension;
				UserDataController.instance.SaveUserInfo ();
				string destPath = DataPath.GetPlayerFramePath (profileWork.user_frame_extension);
				File.Copy (path, destPath, true);
				#if !UNITY_EDITOR && UNITY_IOS
				iPhone.SetNoBackupFlag (destPath);
				#endif
				ProfileInformationController.instance.PlayerFrameReload ();
				pContent.usePicture = true;
				pContent.useText = false;
				StartCoroutine (pContent.LoadSprite(this, extension, (data)=>{
					forceUpdateContents = true;
				}));
			});
			break;
		}
		case ProfileCategory.CategoryId.SORT_TYPE:
		{
			detailUICtrl.Show (info.key,
			                   profileWork.sort_type,
			                   (result)=> { profileWork.sort_type = result; contentUpdate(); isSortRefineChanged = true; },
								System.Enum.GetNames(typeof(ConfigTypes.SortLevels)).Length,
								(value)=>value.ToSortLevels().ToString()
			);
			break;
		}
		case ProfileCategory.CategoryId.REFINE_LEVEL:
		{
			detailUICtrl.Show (info.key,
			                   profileWork.refine_level,
			                   (result)=> { profileWork.refine_level = result; contentUpdate(); isSortRefineChanged = true; }
			);
			break;
		}
		case ProfileCategory.CategoryId.REFINE_DIFFICULTY:
		{
			detailUICtrl.Show (info.key,
			                   profileWork.refine_difficulty,
			                   (result)=> { profileWork.refine_difficulty = result; contentUpdate(); isSortRefineChanged = true; }
			);
			break;
		}
		case ProfileCategory.CategoryId.REFINE_NOTES_DESIGNER:
		{
			detailUICtrl.Show (info.key,
			                   profileWork.refine_designer,
			                   (result)=> { profileWork.refine_designer = result; contentUpdate(); isSortRefineChanged = true; }
			);
			break;
		}
		case ProfileCategory.CategoryId.REFINE_NO_MISS:
		{
			detailUICtrl.Show (info.key,
			                   profileWork.refine_no_miss,
			                   (result)=> { profileWork.refine_no_miss = result; contentUpdate(); isSortRefineChanged = true; },
								3,
								(value)=>value.ToConditionAchievedString()
			);
			break;
		}
		case ProfileCategory.CategoryId.REFINE_FC:
		{
			detailUICtrl.Show (info.key,
			                   profileWork.refine_fc,
			                   (result)=> { profileWork.refine_fc = result; contentUpdate(); isSortRefineChanged = true; },
			3,
			(value)=>value.ToConditionAchievedString()
			);
			break;
		}
		case ProfileCategory.CategoryId.REFINE_AP:
		{
			detailUICtrl.Show (info.key,
			                   profileWork.refine_ap,
			                   (result)=> { profileWork.refine_ap = result; contentUpdate(); isSortRefineChanged = true; },
			3,
			(value)=>value.ToConditionAchievedString()
			);
			break;
		}

		}
	}

	public void OnPlusMinusButtonClick (bool isPlus) {
		if (selectedId == ProfileCategory.CategoryId.NONE)
			return;
		
		Utility.SoundEffectManager.Play ("se_select");
		var content = profileContents [selectedId];
		switch (selectedId) {
		case ProfileCategory.CategoryId.PLAYER_ICON:
		{
			if (!isPlus) {
				// YESNOダイアログを出して、YESなら削除.
				var dialog = Instantiate (yesNoDialogPrefab) as GameObject;
				dialog.SetParentEx (yesNoDialogParent);
				var controller = dialog.GetComponent<YesNoDialogController>();
				controller.Setup ("Delete icon?", (yes)=> {
					if (yes) {
						string path = DataPath.GetPlayerIconPath (profileWork.user_icon_extension);
						File.Delete (path);
						ProfileInformationController.instance.PlayerIconReload ();
						var pContent = profileContents[ProfileCategory.CategoryId.PLAYER_ICON];
						pContent.usePicture = false;
						pContent.useText = true;
						pContent.sprite = null;
						forceUpdateContents = true;
					}
				});
			}
			else OnDoubleTappedCell (profileContents[selectedId]);
			break;
		}
		case ProfileCategory.CategoryId.PLAYER_PLATE:
		{
			if (!isPlus) {
				var dialog = Instantiate (yesNoDialogPrefab) as GameObject;
				dialog.SetParentEx (yesNoDialogParent);
				var controller = dialog.GetComponent<YesNoDialogController>();
				controller.Setup ("Delete plate?", (yes)=> {
					if (yes) {
						string path = DataPath.GetPlayerPlatePath (profileWork.user_plate_extension);
						File.Delete (path);
						ProfileInformationController.instance.PlayerPlateReload ();
						var pContent = profileContents[ProfileCategory.CategoryId.PLAYER_PLATE];
						pContent.usePicture = false;
						pContent.useText = true;
						pContent.sprite = null;
						forceUpdateContents = true;
					}
				});
			}
			else OnDoubleTappedCell (profileContents[selectedId]);
			break;
		}
		case ProfileCategory.CategoryId.PLAYER_FRAME:
		{
			if (!isPlus) {
				var dialog = Instantiate (yesNoDialogPrefab) as GameObject;
				dialog.SetParentEx (yesNoDialogParent);
				var controller = dialog.GetComponent<YesNoDialogController>();
				controller.Setup ("Delete frame?", (yes)=> {
					if (yes) {
						string path = DataPath.GetPlayerFramePath (profileWork.user_frame_extension);
						File.Delete (path);
						ProfileInformationController.instance.PlayerFrameReload ();
						var pContent = profileContents[ProfileCategory.CategoryId.PLAYER_FRAME];
						pContent.usePicture = false;
						pContent.useText = true;
						pContent.sprite = null;
						forceUpdateContents = true;
					}
				});
			}
			else OnDoubleTappedCell (profileContents[selectedId]);
			break;
		}
		case ProfileCategory.CategoryId.SORT_TYPE:
		{
			ChangeTypeAssignHelper (isPlus, profileWork.sort_type,
			                        System.Enum.GetNames(typeof(ConfigTypes.SortLevels)).Length, (result)=>{
				profileWork.sort_type = result;
				isSortRefineChanged = true;
			});
			break;
		}
		case ProfileCategory.CategoryId.REFINE_LEVEL:
		{
			if (!isPlus) {
				// マイナスボタンでサーチワード削除.
				profileWork.refine_level = string.Empty;
			}
			else OnDoubleTappedCell (profileContents[selectedId]);
			break;
		}
		case ProfileCategory.CategoryId.REFINE_DIFFICULTY:
		{
			if (!isPlus) {
				profileWork.refine_difficulty = string.Empty;
			}
			else OnDoubleTappedCell (profileContents[selectedId]);
			break;
		}
		case ProfileCategory.CategoryId.REFINE_NOTES_DESIGNER:
		{
			if (!isPlus) {
				profileWork.refine_designer = string.Empty;
			}
			else OnDoubleTappedCell (profileContents[selectedId]);
			break;
		}
		case ProfileCategory.CategoryId.REFINE_NO_MISS:
		{
			ChangeTypeAssignHelper (isPlus, profileWork.refine_no_miss,
			                        3, (result)=>{
				profileWork.refine_no_miss = result;
				isSortRefineChanged = true;
			});
			break;
		}
		case ProfileCategory.CategoryId.REFINE_FC:
		{
			ChangeTypeAssignHelper (isPlus, profileWork.refine_fc,
			                        3, (result)=>{
				profileWork.refine_fc = result;
				isSortRefineChanged = true;
			});
			break;
		}
		case ProfileCategory.CategoryId.REFINE_AP:
		{
			ChangeTypeAssignHelper (isPlus, profileWork.refine_ap,
			                        3, (result)=>{
				profileWork.refine_ap = result;
				isSortRefineChanged = true;
			});
			break;
		}
		default:
		{
			OnDoubleTappedCell (profileContents[selectedId]);
			break;
		}
			
		}
		
		string cellValue;
		Color cellValueColor;
		GatherContentData (selectedId, out cellValue, out cellValueColor);
		if (!string.IsNullOrEmpty (cellValue)) {
			content.SetValue (cellValue, cellValueColor);
			forceUpdateContents = true;
		}
	}
	
	private void ChangeTypeAssignHelper (bool isPlus, int typeId, int elements, System.Action<int> assign) {
		if (isPlus) {
			typeId++;
		}
		else {
			typeId += elements - 1;
		}
		int result = typeId % elements;
		assign (result);
	}
	
	private void ChangeSwitchAssignHelper (bool srcValue, System.Action<bool> assign) {
		assign (!srcValue);
	}
	
	private void ChangeValueAssignHelper (bool isPlus, float srcValue, int volume, int max, int min, System.Action<float> assign) {
		int work = (srcValue * 1000.0f).ToInt();
		if (isPlus) {
			work += volume;
			work = work / volume * volume;
			if (work > max) {
				work = max;
			}
		}
		else {
			work -= volume;
			work = work / volume * volume;
			if (work < min) {
				work = min;
			}
		}
		float result = (float)work / 1000.0f;
		assign (result);
	}
	
	private void ChangeValueAssignHelper (bool isPlus, int srcValue, int volume, int max, int min, System.Action<float> assign) {
		int work = srcValue;
		if (isPlus) {
			work += volume;
			work = work / volume * volume;
			if (work > max) {
				work = max;
			}
		}
		else {
			work -= volume;
			work = work / volume * volume;
			if (work < min) {
				work = min;
			}
		}
		float result = (float)work;
		assign (result);
	}
	
	private void GatherContentData (ProfileCategory.CategoryId id, out string cellValue, out Color cellValueColor) {
		cellValue = string.Empty;
		cellValueColor = Color.white;
		switch (id) {
		case ProfileCategory.CategoryId.PLAYER_NAME: 
		{
			var type = profileWork.user_name;
			cellValue = type.ToProfileValueString ();
			cellValueColor = type.ToProfileValueColor ();
			break;
		}
		case ProfileCategory.CategoryId.PLAYER_TITLE: 
		{
			var type = profileWork.user_title;
			cellValue = type.ToProfileValueString ();
			cellValueColor = new Color (profileWork.user_title_color_r, profileWork.user_title_color_g, profileWork.user_title_color_b, 1);
			break;
		}
		case ProfileCategory.CategoryId.PLAYER_ICON: 
		{
			var type = string.Empty;
			cellValue = type.ToProfileValueString ();
			cellValueColor = type.ToProfileValueColor ();
			break;
		}
		case ProfileCategory.CategoryId.PLAYER_PLATE: 
		{
			var type = string.Empty;
			cellValue = type.ToProfileValueString ();
			cellValueColor = type.ToProfileValueColor ();
			break;
		}
		case ProfileCategory.CategoryId.PLAYER_FRAME: 
		{
			var type = string.Empty;
			cellValue = type.ToProfileValueString ();
			cellValueColor = type.ToProfileValueColor ();
			break;
		}
		case ProfileCategory.CategoryId.SORT_TYPE: 
		{
			var type = profileWork.sort_type;
			cellValue = type.ToSortLevels().ToString();
			cellValueColor = Color.red;
			break;
		}
		case ProfileCategory.CategoryId.REFINE_LEVEL: 
		{
			var type = profileWork.refine_level;
			cellValue = type.ToProfileValueString ();
			cellValueColor = type.ToProfileValueColor ();
			break;
		}
		case ProfileCategory.CategoryId.REFINE_DIFFICULTY: 
		{
			var type = profileWork.refine_difficulty;
			cellValue = type.ToProfileValueString ();
			cellValueColor = type.ToProfileValueColor ();
			break;
		}
		case ProfileCategory.CategoryId.REFINE_NOTES_DESIGNER: 
		{
			var type = profileWork.refine_designer;
			cellValue = type.ToProfileValueString ();
			cellValueColor = type.ToProfileValueColor ();
			break;
		}
		case ProfileCategory.CategoryId.REFINE_NO_MISS: 
		{
			var type = profileWork.refine_no_miss;
			cellValue = type.ToConditionAchievedString ();
			cellValueColor = type.ToConfigValueColor ();
			break;
		}
		case ProfileCategory.CategoryId.REFINE_FC: 
		{
			var type = profileWork.refine_fc;
			cellValue = type.ToConditionAchievedString ();
			cellValueColor = type.ToConfigValueColor ();
			break;
		}
		case ProfileCategory.CategoryId.REFINE_AP: 
		{
			var type = profileWork.refine_ap;
			cellValue = type.ToConditionAchievedString ();
			cellValueColor = type.ToConfigValueColor ();
			break;
		}

		}
	}
}
