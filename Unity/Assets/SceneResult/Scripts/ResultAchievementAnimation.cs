using UnityEngine;
using System.Collections;

public class ResultAchievementAnimation : MonoBehaviour {

	public float achievement;
	public float animationTime = 2.0f; // animationTime秒かけてアニメーションする.
	private float process;
	private float speed;
	private UnityEngine.UI.Image imageComponent;
	[SerializeField]
	private Material scoreMaterial;
	[SerializeField]
	private Material syncMaterial;

	void Awake () {
		imageComponent = GetComponent<UnityEngine.UI.Image> ();
		scoreMaterial.SetFloat ("_Degree", 0);
		syncMaterial.SetFloat ("_Degree", 0);
	}

	// Use this for initialization
	void Start () {
		if (!ResultInformationController.instance.isSyncMode) {
			achievement = (float)ResultInformationController.instance.achievement / 10000.0f;
			imageComponent.material = new Material(scoreMaterial);
			imageComponent.material.name = scoreMaterial.name + " (Instance)";
		}
		else {
			achievement = (float)ResultInformationController.instance.sync / 100.0f;
			imageComponent.material = new Material(syncMaterial);
			imageComponent.material.name = syncMaterial.name + " (Instance)";
		}
		process = 0;
		speed = achievement / animationTime;
		imageComponent.material.SetFloat ("_Degree", process);
	}
	
	// Update is called once per frame
	void Update () {
		if (process < achievement) {
			process += speed * Time.deltaTime;

			if (process > achievement) {
				process = achievement;
			}

			imageComponent.material.SetFloat ("_Degree", process);
		}
	}
}
