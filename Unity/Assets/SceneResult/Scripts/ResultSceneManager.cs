using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResultSceneManager : MonoBehaviour {
	[SerializeField]
	private Text tapScoreLabel;
	[SerializeField]
	private Text holdScoreLabel;
	[SerializeField]
	private Text slideScoreLabel;
	[SerializeField]
	private Text breakScoreLabel;
	[SerializeField]
	private Text perfectCountLabel;
	[SerializeField]
	private Text greatCountLabel;
	[SerializeField]
	private Text goodCountLabel;
	[SerializeField]
	private Text missCountLabel;
	[SerializeField]
	private Text comboCountLabel;
	[SerializeField]
	private Text playCountLabel;
	[SerializeField]
	private Text titleLabel;
	[SerializeField]
	private Text clearEvaluateLabel;
	[SerializeField]
	private Text achievementNameLabel;
	[SerializeField]
	private Text achievementValueLabel;
	[SerializeField]
	private Text achievementBestCompareLabel;
	[SerializeField]
	private Text totalScoreLabel;
	[SerializeField]
	private Text totalScoreBestCompareLabel;
	[SerializeField]
	private Text rankNameLabel;
	[SerializeField]
	private Text rankValueLabel;
	[SerializeField]
	private GameObject newRecordObject;
	[SerializeField]
	private Image jacketPictureFrame;
	[SerializeField]
	private GameObject exitMenuDialog;
	[SerializeField]
	private Text breakNameLabel;
	[SerializeField]
	private GameObject syncLabelObjectsParentObj;
	[SerializeField]
	private Text syncValueLabel;
	[SerializeField]
	private Text syncBestCompareLabel;
	[SerializeField]
	private FadeController fade;
	[SerializeField]
	private GameObject exitMenuSingle;
	[SerializeField]
	private GameObject exitMenuMultiHost;
	[SerializeField]
	private GameObject exitMenuMultiClient;
	[SerializeField]
	private GameObject exitMenuMultiHostStep1;
	[SerializeField]
	private GameObject exitMenuMultiHostStep2;
	[SerializeField]
	private GameObject syncServerDisconnectDialogObj;

	// Use this for initialization
	void Start () {
		// データを取得・計算.
		TrackInformationController track = TrackInformationController.instance;
		ResultInformationController result = ResultInformationController.instance;
		ResultInformation newSaveData = result.GetSavingData (track.saveData);
		float achievement = ((float)result.achievement / 100.0f);
		int comparedScore = result.score - (track.saveData != null ? track.saveData.score : 0);
		float comparedAchievement = ((float)(result.achievement - (track.saveData != null ? track.saveData.achievement : 0)) / 100.0f);
		int comparedSync = result.sync - (track.saveData != null ? track.saveData.sync : 0);

		// 背景を設定.
		if (track.jacket != null) {
			jacketPictureFrame.sprite = track.jacket;
			jacketPictureFrame.color = Color.white;
		}
		else {
			jacketPictureFrame.sprite = SpriteTank.Get ("bg_default");
			jacketPictureFrame.color = Color.white;
		}

		// テキストを設定.
		tapScoreLabel.text = result.tap_score.ToString("#,0");
		holdScoreLabel.text = result.hold_score.ToString("#,0");
		slideScoreLabel.text = result.slide_score.ToString("#,0");
		breakScoreLabel.text = result.break_score.ToString("#,0");
		perfectCountLabel.text = result.perfect_amount.ToString("#,0");
		greatCountLabel.text = result.great_amount.ToString("#,0");
		goodCountLabel.text = result.good_amount.ToString("#,0");
		missCountLabel.text = result.miss_amount.ToString("#,0");
		comboCountLabel.text = result.max_combo.ToString("#,0");
		playCountLabel.text = result.auto_play ? "AUTO" : (newSaveData.play_count).ToString("#,0");
		titleLabel.text = track.title;
		clearEvaluateLabel.text = MaimaiStyleDesigner.GetClearEvaluateResultString(achievement);
		achievementValueLabel.text = achievement.ToString("F2") + "%";
		achievementBestCompareLabel.text = (comparedAchievement < 0 ? "" : "+") + comparedAchievement.ToString("F2") + "%";
		totalScoreLabel.text = result.score.ToString("#,0");
		totalScoreBestCompareLabel.text = (comparedScore < 0 ? "" : "+") + comparedScore.ToString("#,0");
		if (UserDataController.instance.config.rank_version_type.ToRankVersionType() == ConfigTypes.RankVersion.PINK) {
			rankValueLabel.text = MaimaiStyleDesigner.GetRankString(achievement);
		}
		else if (UserDataController.instance.config.rank_version_type.ToRankVersionType() == ConfigTypes.RankVersion.CLASSIC) {
			rankValueLabel.text = MaimaiStyleDesigner.GetRankStringClassic(achievement);
		}
		if (result.isSyncMode) {
			syncValueLabel.text = result.sync.ToString() + "%";
			syncBestCompareLabel.text = (comparedSync < 0 ? "" : "+") + comparedSync.ToString() + "%";
			syncLabelObjectsParentObj.SetActive (true);
		}
		else {
			syncLabelObjectsParentObj.SetActive (false);
		}

		// カラーを設定および表示切替.
		Color achievementColor = MaimaiStyleDesigner.GetAchievementColor(achievement);
		achievementNameLabel.color = achievementColor;
		achievementValueLabel.color = achievementColor;
		rankNameLabel.color = achievementColor;
		rankValueLabel.color = achievementColor;
		tapScoreLabel.color = result.tap_score < result.full_tap_score ? MaimaiStyleDesigner.GetScoreColor () : MaimaiStyleDesigner.GetFullScoreColor ();
		holdScoreLabel.color = result.hold_score < result.full_hold_score ? MaimaiStyleDesigner.GetScoreColor () : MaimaiStyleDesigner.GetFullScoreColor ();
		slideScoreLabel.color = result.slide_score < result.full_slide_score ? MaimaiStyleDesigner.GetScoreColor () : MaimaiStyleDesigner.GetFullScoreColor ();
		breakScoreLabel.color = result.break_score < result.full_break_score ? MaimaiStyleDesigner.GetScoreColor () : MaimaiStyleDesigner.GetFullScoreColor ();
		totalScoreLabel.color = result.full_ap ? MaimaiStyleDesigner.GetFullScoreColor () : MaimaiStyleDesigner.GetScoreColor ();
		clearEvaluateLabel.color = MaimaiStyleDesigner.GetClearEvaluateColor(achievement);
		titleLabel.color = MaimaiStyleDesigner.GetLevelColor(track.difficulty);
		achievementBestCompareLabel.color = totalScoreBestCompareLabel.color = comparedScore < 0 ? MaimaiStyleDesigner.ByteToPercentRGBA(255,0,0,255) : MaimaiStyleDesigner.ByteToPercentRGBA(0,192,255,255);
		newRecordObject.SetActive (!result.auto_play && comparedScore > 0);
		if (result.change_break_type != ConfigTypes.ChangeBreak.NORMAL) {
			breakNameLabel.color = Color.red;
		}
		syncBestCompareLabel.color = comparedSync < 0 ? MaimaiStyleDesigner.ByteToPercentRGBA(255,0,0,255) : MaimaiStyleDesigner.ByteToPercentRGBA(0,192,255,255);

		// シンクモードごとにリザルトメニュー切り替え.
		if (MiscInformationController.instance.syncMode == 1) {
			exitMenuSingle.SetActive (false);
			exitMenuMultiHost.SetActive (true);
			exitMenuMultiClient.SetActive (false);
			exitMenuMultiHostStep1.SetActive (true);
			exitMenuMultiHostStep2.SetActive (false);
		}
		else if (MiscInformationController.instance.syncMode == 2) {
			exitMenuSingle.SetActive (false);
			exitMenuMultiHost.SetActive (false);
			exitMenuMultiClient.SetActive (true);
		}
		else {
			exitMenuSingle.SetActive (true);
			exitMenuMultiHost.SetActive (false);
			exitMenuMultiClient.SetActive (false);
		}

		// 最後にnewSaveDataを保存する.
		if (MiscInformationController.instance.syncMode != 2) { //シンクモードでクライアントなら保存しない.
			UserDataController.instance.SetResult (track.trackId, track.difficulty, newSaveData);
			UserDataController.instance.SaveRecords ();
		}

		// サウンド再生.
		
		AudioManagerLite.Pause ();
		AudioManagerLite.Load (Utility.SoundEffectManager.GetClip ("maisq_bgm_result"));
		MiscInformationController.instance.lastLoadedBgmKey = "maisq_bgm_result";
		AudioManagerLite.Seek (0);
		AudioManagerLite.Play (true);
		if (newRecordObject.activeSelf) {
			Utility.SoundEffectManager.Play ("maisq_voice_newrecord");
		}
		else {
			Utility.SoundEffectManager.Play ("maisq_voice_result");
		}
	}
	
	public void OpenExitMenuDialog () {
		Utility.SoundEffectManager.Play ("se_decide");
		if (MiscInformationController.instance.syncMode == 2) {
			StartCoroutine (MultiClientWait());
		}
		exitMenuDialog.SetActive (true);
	}
	
	public void CloseExitMenuDialog () {
		Utility.SoundEffectManager.Play ("se_decide");
		exitMenuDialog.SetActive (false);
	}
	
	public void ExitScene () {
		Utility.SoundEffectManager.Play ("se_decide");
		if (MiscInformationController.instance.syncMode == 1) {
			StartCoroutine (MultiHostWait (2));
		}
		else {
			fade.Activate (() => {
				MiscInformationController.instance.selectMusicVoiceCallRequest = true;
				Application.LoadLevel ("SceneSelectTrack");
			});
		}
	}
	
	public void Retry () {
		Utility.SoundEffectManager.Play ("se_decide");
		if (MiscInformationController.instance.syncMode == 1) {
			StartCoroutine (MultiHostWait (1));
		}
		else {
			fade.Activate (() => {
				Application.LoadLevel ("SceneRhythmGame");
			});
		}
	}

	public void MultiDisconnect () {
		Utility.SoundEffectManager.Play ("se_decide");
		if (MiscInformationController.instance.syncMode == 1) {
			StartCoroutine (MultiHostWait (3));
		}
		else {
			fade.Activate (() => {
				Application.LoadLevel ("SceneMainMenu");
			});
		}
	}

	private IEnumerator MultiHostWait (int order) {
		SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateHostResultNextSceneOrder", RPCMode.OthersBuffered, order);
		exitMenuMultiHostStep1.SetActive (false);
		exitMenuMultiHostStep2.SetActive (true);
		while (!SyncLobbyNetworkStateController.instance.clientResultWait) {
			yield return null;
		}
		switch (order) {
		case 1:
			// リトライ.
			SyncLobbyNetworkStateController.instance.ResetClientFlagsFromRetry ();
			fade.Activate (() => {
				Application.LoadLevel ("SceneRhythmGame");
			});
			break;
		case 2:
			// 選曲.
			SyncLobbyNetworkStateController.instance.ResetClientFlagsFromReselectTrack ();
			fade.Activate (() => {
				MiscInformationController.instance.selectMusicVoiceCallRequest = true;
				Application.LoadLevel ("SceneSelectTrack");
			});
			break;
		case 3:
			// 切断.
			syncServerDisconnectDialogObj.SetActive (false);
			fade.Activate (() => {
				Application.LoadLevel ("SceneMainMenu");
			});
			break;
		}
		exitMenuDialog.SetActive (false);
	}

	private IEnumerator MultiClientWait () {
		SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateClientResultWait", RPCMode.Server);
		while (SyncLobbyNetworkStateController.instance.hostResultNextSceneOrder == 0) {
			yield return null;
		}
		switch (SyncLobbyNetworkStateController.instance.hostResultNextSceneOrder) {
		case 1:
			// リトライ.
			SyncLobbyNetworkStateController.instance.ResetHostFlagsFromRetry ();
			fade.Activate (() => {
				Application.LoadLevel ("SceneRhythmGame");
			});
			break;
		case 2:
			// 選曲.
			MiscInformationController.instance.syncClientAfterResult = true;
			SyncLobbyNetworkStateController.instance.ResetHostFlagsFromReselectTrack ();
			fade.Activate (() => {
				Application.LoadLevel ("SceneMainMenu");
			});
			break;
		case 3:
			// 切断.
			syncServerDisconnectDialogObj.SetActive (false);
			Network.Disconnect ();
			fade.Activate (() => {
				Application.LoadLevel ("SceneMainMenu");
			});
			break;
		}
		exitMenuDialog.SetActive (false);
	}

}
