﻿Shader "Custom/SyncResultAchievementAnimationShader" {
	Properties {
		_MainTex ("Base (ARGB)", 2D) = "white" { }
		_InnerRadius ("Inner Radius", Range (0.0, 1.0)) = 0.3 // sliders
		_OuterRadius ("Outer Radius", Range (0.0, 1.0)) = 0.2 // sliders
		_BlurThickness ("Blur Thickness", Range (0.0, 1.0)) = 0.2 // sliders
		_RingColor ("Ring Color", Color) = (1.0,1.0,1.0,1.0) // colors
		_Degree ("Sync Achievement", Range (0.0, 1.0)) = 0.0 // sliders
	} 
	
	SubShader {
		Pass {
			Lighting Off
			Cull Off
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"

			uniform sampler2D _MainTex;
			uniform float _InnerRadius;
			uniform float _OuterRadius;
			uniform float _BlurThickness;
			uniform float4 _RingColor;
			uniform float _Degree;
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};
			
			float4 _MainTex_ST;
			
			// circle_calculator
			float point_to_degree (float x, float y) {
				float rad = atan2 (y, x);
				float deg = degrees (rad);
				return deg;
			}
			float point_to_degree (float2 pos) {
				return point_to_degree(pos.x, pos.y);
			}
			float point_to_degree (float2 start, float2 target) {
				return point_to_degree(start.x - target.x, start.y - target.y);
			}
			// end of circle_calculator
			
			v2f vert (appdata_base v)
			{
				v2f o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
				return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
				float play_result_percent = _Degree;
				float play_result_deg = (play_result_percent * 2.0 - 1.0) * 180.0;
				float2 UV_CENTER_POS = (0.5,0.5);
				float vertex_deg = point_to_degree(UV_CENTER_POS, i.uv);
				
				half4 texcol = _RingColor;
				float dist = distance(i.uv, UV_CENTER_POS);
				if(dist < _InnerRadius - _BlurThickness) {
					clip(-1.0);
				}
				else if(dist < _OuterRadius + _BlurThickness) {
					if (vertex_deg > play_result_deg) {
						clip(-1.0);
					}
					else {
						if(dist < _InnerRadius) {
							texcol.w = (dist - (_InnerRadius - _BlurThickness)) / _BlurThickness;
						}
						else if(dist < _OuterRadius) {
							texcol.r = 1.0;
						}
						else {
							texcol.w = 1.0 - (dist - _OuterRadius) / _BlurThickness;
						}
					}
				}
				else {
					clip(-1.0);
				}

				return texcol;
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
}
