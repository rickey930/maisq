﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BGInfoAnimationController : MonoBehaviour {
	
	public GameObject spriteRendererObjPrefab;
	public float spriteWidth; // 文字幅.
	public Sprite[] sprites;
	public Color spriteColor;
	
	private List<SpriteRenderer> renderers;

	public void LoadSprites (string text) {
		sprites = new Sprite[text.Length];
		for (int i = 0; i < text.Length; i++) {
			if (text[i] == ' ') {
				sprites[i] = SpriteTank.Get ("icon_dammy");
			}
			else if (text[i] == '.') {
				sprites[i] = SpriteTank.Get ("bginfo_dot");
			}
			else if (text[i] == ',') {
				sprites[i] = SpriteTank.Get ("bginfo_comma");
			}
			else if (text[i] == '%') {
				sprites[i] = SpriteTank.Get ("bginfo_percent");
			}
			else {
				sprites[i] = SpriteTank.Get ("bginfo_" + text[i]);
			}
		}
	}

	public void Setup (Color spriteColor, string text) {
		LoadSprites (text);

		this.spriteColor = spriteColor;

		renderers = new List<SpriteRenderer>();

		CreateSpriteRenderer ();
	}

	private void CreateSpriteRenderer () {
		MatchOfRendererObjAmount(sprites.Length);
		int size = renderers.Count;
		for (int i = 0; i < size; i++) {
			renderers[i].sprite = sprites[i];
			var rendererColor = new Color (spriteColor.r, spriteColor.g, spriteColor.b, 0);
			if (renderers[i].material.HasProperty ("_Color")) {
				renderers[i].material.SetColor ("_Color", rendererColor);
			}
			else {
				renderers[i].color = rendererColor;
			}
			var anim = renderers[i].GetComponent<TimeAnimationAlpha>();
			anim.GetData[0].time = (0.5f / (float)size) * i;
			anim.loopEndEvent = (rendererObj) => {
				rendererObj.SetActive (false);
			};
		}
	}

	public void ActivateSpriteRenderer () {
		int size = renderers.Count;
		for (int i = 0; i < size; i++) {
			renderers[i].GetComponent<TimeAnimationAlpha>().Restart();
			renderers[i].gameObject.SetActive (true);
		}
	}

	// 文字数とゲームオブジェクトの数を合わせる.
	private void MatchOfRendererObjAmount(int amount) {
		bool move = false;
		for (int i = 0; i < renderers.Count; i++) {
			if (renderers[i] == null) {
				renderers.RemoveAt(i);
				i = -1;
			}
		}
		// 削除.
		if (renderers.Count > amount) {
			move = true;
			while (renderers.Count != amount) {
				SpriteRenderer sr = renderers[renderers.Count - 1];
				renderers.Remove(sr);
				Destroy(sr.gameObject);
			}
		}
		// 生成.
		else if (renderers.Count < amount) {
			move = true;
			while (renderers.Count != amount) {
				GameObject item = Instantiate(spriteRendererObjPrefab) as GameObject;
				item.transform.parent = this.transform;
				SpriteRenderer sr = item.GetComponent<SpriteRenderer>();
				renderers.Add(sr);
				item.SetActive (false);
			}
		}
		// 中央揃えのため移動.
		if (move) {
			int size = renderers.Count;
			for (int i = 0; i < size; i++) {
				Transform obj = renderers[i].gameObject.transform;
				obj.localPosition = new Vector3(((float)i - (float)(amount - 1) / 2.0f) * spriteWidth, 0, 0);
			}
		}
	}


}
