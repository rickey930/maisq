using UnityEngine;
using System.Collections;

public class TestScoreCreater : MonoBehaviour {
	
	public static IEnumerator CreateTestScore(System.Action<MaimaiScore.Each[]> callback) {
		MaimaiScore.Each[] score = CreateTestScore13();
		callback (score);
		
		//yield return StartCoroutine (LoadTestFile (callback));
		
		yield break;
	}
	
	private static MaimaiScore.Each[] CreateTestScore1() {
		var reader = new MaimaiScoreReader ();
		
		// mai-script.
		reader.SetBpm(128);
		reader.SetBeat(4);
		reader.SetNote (
			MaimaiScore.NoteTap.Create (1),
			MaimaiScore.NoteSlide.Create (3, 
		        MaimaiScore.SlidePattern.Create (
					MaimaiScore.SlideChain.Create (MaimaiScore.SlideHeadCustomWaitDefault.Create (), MaimaiScore.StepLength.Create (1, 8), 
		                               MaimaiScore.SlideCommandStraight.Create (MaimaiScore.Position.OuterSensorPosition (MaimaiScore.Button.Create (3)).GetValue(), MaimaiScore.Position.OuterSensorPosition (MaimaiScore.Button.Create (8)).GetValue())))));
		reader.SetNote (
			MaimaiScore.NoteHold.Create (6, 4, 1));
		var b = MaimaiScore.NoteBreak.Create (7);
		b.option = MaimaiScore.NoteOption.Create (true);
		reader.SetNote (b
			);

		return reader.GetMaimaiScore();
	}
	
	private static MaimaiScore.Each[] CreateTestScore2() {
		var reader = new MaisqScriptReader ();
		string maiScript = "bpm(128);\nbeat(4);\nlog(\"お \\n金の\t単位は\\\"\\\\\\\"で　す。\")\nnote(slide(star(1), step(4, 1), pattern(chain(shape_p_axis_center(1, 4)))));\nnote(tap(1), hold(2,4,1));\n";
		reader.ReadMaiScript (maiScript);
		return reader.GetMaimaiScore ();
	}

	private static MaimaiScore.Each[] CreateTestScore3() {
		var reader = new SimaiReader ();
		string simai = @"(128){4}#""note(slide(star(1),step(1,1),pattern(chain(
			continued_curve(0,   0,-180),
			continued_curve(0,-0.1,-180),
			continued_curve(0, 0.1,-180),
			continued_curve(0,-0.1,-180),
			continued_curve(0, 0.1,-180),
			continued_curve(-0.1, 0.1,-180),
			continued_curve(0, 0.1,-90),
			continued_curve(0.1, 0,-45),
		))));"",E";
		string maiScript = reader.ToMaiScript (simai);
		Debug.Log (maiScript);
		reader.ReadMaiScript (maiScript);
		return reader.GetMaimaiScore ();
	}

	private static MaimaiScore.Each[] CreateTestScore4() {
		var reader = new SimaiReader ();
		string simai = @"(128){4}#""note(slide(star(1),step(1,1),pattern(chain(
			straight(-1, 1, 1, 1),
			continued_straight(0, 0.5),
			curve(0, 0, 0.5, 0.5, 0, 270),
			continued_straight(angle_x(120), angle_y(120)),
			continued_curve(angle_x(120), 0, 180),
		))));"",E
		";
		string maiScript = reader.ToMaiScript (simai);
		reader.ReadMaiScript (maiScript);
		return reader.GetMaimaiScore ();
	}

	private static MaimaiScore.Each[] CreateTestScore5() {
		var reader = new SimaiReader ();
		string simai = @"(128){4}#""note(slide(star(2),step(1,1),pattern(chain(
			continued_straight(0, 0),
			continued_curve(angle_x(135, 0.3), angle_y(135, 0.3), -360),
			//continued_curve(inner_sensor_y(3), inner_sensor_x(3), -360),
			continued_straight(outer_sensor_pos(5)),
		)))
		);"",E
		";
//		simai = @"(128){4}#""note(slide(2,1,1,
//			continued_straight(0, 0),
//			continued_curve(0, 0.45, 235),
//			continued_straight(outer_sensor_x(4), outer_sensor_y(4)),
//		),
//		);"",E
//		";
		string maiScript = reader.ToMaiScript (simai);
		reader.ReadMaiScript (maiScript);
		return reader.GetMaimaiScore ();
	}
	
	private static MaimaiScore.Each[] CreateTestScore6() {
		var reader = new SimaiReader ();
//		string simai = @"(128){4}#""note(slide(star(2),step(1,2),pattern(chain(
//			continued_straight(outer_sensor_x(1), outer_sensor_y(1)),
//			continued_straight(outer_sensor_x(6), outer_sensor_y(6)),
//			continued_curve(0, 0, -360),
//			continued_curve(0, 0, sensor_deg_distance_clockwise(2, 3)),
//			//outer_straight_outer(2,6),
//		)))
//		);"",E
//		";
//		string simai = @"(128){4}#""note(slide(star(2),step(8,1),pattern(chain(
//			//continued_straight(outer_sensor_x(6), outer_sensor_y(6)),
//			//continued__curve(0, 0, -360),
//			continued_straight(-1.1, -0.85),
//			//continued_straight(-2, outer_sensor_y(7)),
//			//continued_straight(7, inner_sensor_x(6), outer_sensor_y(7)),
//		)))
//		);"",E
//		";
		string simai = @"(128){4}#""note(slide(star(1),step(1,2),pattern(chain(
			continued_curve(0,   0,-180),
			continued_curve(0, angle_x(sensor_deg(1), 0.3), -180),
			continued_curve(0,   0,-180),
			continued_curve(0, angle_x(sensor_deg(1), 0.3), -180),
			continued_curve(0, angle_x(sensor_deg(1), 0),-180),
			continued_curve(0, angle_x(sensor_deg(1), 0.3), -180),
			continued_curve(0, angle_x(sensor_deg(1), 0),-180),
			continued_curve(0, angle_x(sensor_deg(1), 0.3), -180),
			continued_curve(0, angle_x(sensor_deg(1), 0),-180),
			continued_curve(0, angle_x(sensor_deg(1), 0.3), -180),
		)))
		);"",E
		";
		string maiScript = reader.ToMaiScript (simai);
		reader.ReadMaiScript (maiScript);
		return reader.GetMaimaiScore ();
	}

	private static MaimaiScore.Each[] CreateTestScore7() {
		var reader = new SimaiReader ();
		string simai = @"(128){4}#""note(slide(star(1),step(1,1),
			pattern(chain(shape_p_axis_center(1,4))), 
			pattern(chain(shape_q_axis_center(1,6)
		))), slide(break_star(8),step(8,1),pattern(chain(outer_straight_outer(8,3))))
		);"",E
		";
//		string simai = "(128){4}7w3[4:1],E";
		string maiScript = reader.ToMaiScript (simai);
		reader.ReadMaiScript (maiScript);
		return reader.GetMaimaiScore ();
	}
	
	private static MaimaiScore.Each[] CreateTestScore8() {
		var reader = new SimaiReader ();
		string simai = @"(128){4}#""note(slide(star(1),step(1,1),pattern(

			chain(outer_straight_outer(6,5)),
			chain(outer_straight_outer(7,5)),
			chain(outer_straight_outer(8,5)),
			chain(outer_straight_outer(1,5)),
			chain(outer_straight_outer(2,5)),
			chain(outer_straight_outer(3,5)),
			chain(outer_straight_outer(4,5))))


//			chain(outer_straight_outer(1,3)),
//			chain(outer_straight_outer(8,4)),
//			chain(outer_straight_outer(7,5)),
//			chain(outer_straight_outer(1,6)),
//			chain(outer_curve_counterclockwise_axis_center(1,7)),
//		), pattern(chain(outer_curve_counterclockwise_axis_center(1,6))))
//		slide(star(1),step(1,1),pattern(
//			chain(outer_straight_outer(1,4)),
//			chain(outer_straight_outer(1,5)),
//			chain(outer_straight_outer(1,6)),
//		)).h_mirror()
		);"",E
		";
		//
//		string simai = "(128){4}7w3[4:1],E";
		string maiScript = reader.ToMaiScript (simai);
		reader.ReadMaiScript (maiScript);
		return reader.GetMaimaiScore ();
	}

	private static MaimaiScore.Each[] CreateTestScore9() {
		var reader = new SimaiReader ();
		string simai = 
@"(128){4}#""note(
hold(8,step(4,4)),
//message(\""hey, yo!\"",pos(0, 0.5), 2, rgb(255,255,255),3),
//sound_message(\""se_select\""),
);
beat(4);
note(scroll_message(\""真っ赤な誓いいいいいいいいいい\"",1.0, 2, rgb(255,0,0),step(5)));
note(scroll_message(\""真っ赤な誓いいいいいいいいいい\"",0.8, 2, rgb(255,0,0),step(5)));
note(scroll_message(\""真っ赤な誓いいいいいいいいいい\"",0.6, 2, rgb(255,0,0),step(5)));
note(scroll_message(\""真っ赤な誓いいいいいいいいいい\"",0.4, 2, rgb(255,0,0),step(5)));
note(scroll_message(\""真っ赤な誓いいいいいいいいいい\"",0.2, 2, rgb(255,0,0),step(5)));
note(scroll_message(\""真っ赤な誓いいいいいいいいいい\"",0.0, 2, rgb(255,0,0),step(5)));
note(scroll_message(\""真っ赤な誓いいいいいいいいいい\"",-0.2, 2, rgb(255,0,0),step(5)));
note(scroll_message(\""真っ赤な誓いいいいいいいいいい\"",-0.4, 2, rgb(255,0,0),step(5)));
note(scroll_message(\""真っ赤な誓いいいいいいいいいい\"",-0.6, 2, rgb(255,0,0),step(5)));
note(scroll_message(\""真っ赤な誓いいいいいいいいいい\"",-0.8, 2, rgb(255,0,0),step(5)));
note(scroll_message(\""真っ赤な誓いいいいいいいいいい\"",-1.0, 2, rgb(255,0,0),step(5)));
"",E";
		string maiScript = reader.ToMaiScript (simai);
		reader.ReadMaiScript (maiScript);
		return reader.GetMaimaiScore ();
	}

	private static MaimaiScore.Each[] CreateTestScore10() {
		var reader = new SimaiReader ();
		string simai = @"(128){1}12/,3,,E";
		string maiScript = reader.ToMaiScript (simai);
		reader.ReadMaiScript (maiScript);
		return reader.GetMaimaiScore ();
	}

	private static MaimaiScore.Each[] CreateTestScore11() {
		var reader = new SimaiReader ();
		string simai = @"(128){1:4}
1pp4pp7[4:1],
1pp4[4:1]*#<6-B7-A8pp3[4:1],
3qq7[4:1]/3pp7[4:1],
//1pp4[4:1]/8qq5[4:1],
//7V51[4:1],
//7V52[4:1],
//7V53[4:1],
//7V54[4:1],
//8V52[4:1],
//8V53[4:1],
//8V54[4:1],
,E";
		string maiScript = reader.ToMaiScript (simai);
		Debug.Log (maiScript);
		reader.ReadMaiScript (maiScript);
		return reader.GetMaimaiScore ();
	}

	private static MaimaiScore.Each[] CreateTestScore12() {
		var reader = new SimaiReader ();
		string simai = @"
(128){1}
#""define(\""mmm\"",\""chain( outer_straight_outer($0, $1) ); chain( outer_straight_center($0), center_straight_outer($1) ); chain( outer_straight_outer($0, call(\""four\"")) );\"",\""$0\"", \""$1\"");"",
#""define(\""one\"",1);"",
#""define(\""four\"",4);"",
#""define(\""step\"",\""step(4,1)\"");"",
#""simai_expand(\""m\"", \""outer_straight_outer($0, $1)\"", \""$0\"", \""$1\"");"",
#""note(slide(star(call(\""one\"")),call(\""step\""), pattern( call(\""mmm\"",\""1\"",\""6\"") )));"",
,E";
		//simai = @"(400){4}#""define(\""1b\"", \""break(1)\""); loop(50, loop(2, note(call(\""1b\""))), loop(2, note(call(\""1b\"").h_mirror()) ));"",,E";
		string maiScript = reader.ToMaiScript (simai);
		Debug.Log (maiScript);
		reader.ReadMaiScript (maiScript);
		return reader.GetMaimaiScore ();
	}

	private static MaimaiScore.Each[] CreateTestScore13() {
		MsqScript.Formatter.FormatterResult result;
		string text = @"{ key:value, option:value2, key:value4, }; name()over;";
		// name()over; のoverはエラーにしたい.
		// , の後に値が無ければエラーにしたいが、""があったときでもエラーになりそう.


		var analyzed = MsqScript.Formatter.Analyze(text, 
	    new CommonScriptFormatter.DefineSpecialText[] {
			new CommonScriptFormatter.DefineSpecialText("/*", "*/", true),
			new CommonScriptFormatter.DefineSpecialText("//", "\n", false),
		},
		new CommonScriptFormatter.DefineSpecialText[] {
			new CommonScriptFormatter.DefineSpecialText("\"", "\"", true)
		},
		new string[] { "\r", "\n", " ", "\t", "　" },
		out result
		);
		var dic = analyzed [0].tables;

		return CreateTestScore1 ();
	}
	
	public static IEnumerator LoadTestFile(System.Action<MaimaiScore.Each[]> callback) {
		var reader = new SimaiReader ();
		string simai = "";
		string path =  @"file:///D:/test_maidata.txt";
		using (WWW www = new WWW (path)) {
			yield return www;
			if (string.IsNullOrEmpty(www.error)) {
				simai = www.text;
			}
			else {
				Debug.LogError(www.error);
				yield break;
			}
		}
		string maiScript = reader.ToMaiScript (simai);
		Debug.Log (maiScript);
		reader.ReadMaiScript (maiScript);
		callback (reader.GetMaimaiScore ());
	}
}
