﻿using UnityEngine;
using System.Collections;

public class ChainSlideEvaluateObjectController : StraightSlideEvaluateObjectController {

	protected MaimaiSlidePattern slidePatternInfo;

	public virtual void Setup (DrawableEvaluateSlideType evaluateType, MaimaiSlidePattern pattern) {
		this.evaluateType = evaluateType;
		this.slidePatternInfo = pattern;
	}

	// Use this for initialization
	protected override void Start () {
		targetDirectionHierarchy.localPosition = Constants.instance.ToLeftHandedCoordinateSystemPosition(target);
		targetDirectionHierarchy.localRotation = Quaternion.Euler (0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(deg));
		var renderer = GetComponentInChildren<MeshRenderer> ();
		string materialName = GetMaterialName ();
		//renderer.sprite = SpriteTank.Get (spriteName);
		renderer.sharedMaterial = MaterialTank.Get (materialName);

		
		var meshfilter = GetComponentInChildren<MeshFilter>();
		meshfilter.sharedMesh = SlideMeshCreater.EvaluaterCreate(slidePatternInfo.chains.Length);
		var mesh = meshfilter.mesh;

		float chainSlideEvaluaterSize = 32.0f;
		Vector3[] vertices = mesh.vertices;
		for (int i = 0; i < slidePatternInfo.chains.Length; i++) {
			if (slidePatternInfo.chains[i].commands.Length == 0) continue;
			// 最後のスライドコマンドを取得.
			MaimaiSlideCommand comdata = slidePatternInfo.chains[i].commands[slidePatternInfo.chains[i].commands.Length - 1];
			float angle = 0;
			// スライドコマンドがストレートならスライドコマンドの始点を見て、それを角度とする.
			if (comdata.GetCommandType () == MaimaiSlideCommandType.STRAIGHT) {
				angle = CircleCalculator.PointToDegree (comdata.targetPos, comdata.startPos);
			}
			// スライドコマンドがカーブでインデックスオーバーしないなら始点がある方向で一番近いマーカーの位置を見て、それを角度とする.
			else {
				angle = comdata.imgDeg;
			}
			// 上.
			var up = CircleCalculator.PointOnCircle (comdata.targetPos, chainSlideEvaluaterSize / 2, angle).ChangePosHandSystem ();
			vertices [i].x = up.x;
			vertices [i].y = up.y;
			// 下.
			var down = CircleCalculator.PointOnCircle (comdata.targetPos, chainSlideEvaluaterSize / 2, angle + 180).ChangePosHandSystem ();
			vertices [vertices.Length - 1 - i].x = down.x;
			vertices [vertices.Length - 1 - i].y = down.y;
		}
		mesh.vertices = vertices;
	}

	protected override string GetMaterialName() {
		switch (evaluateType) {
		case DrawableEvaluateSlideType.JUST:
			return "judge_slide_chain_just";
		case DrawableEvaluateSlideType.FASTGREAT:
			return "judge_slide_chain_great_fast";
		case DrawableEvaluateSlideType.LATEGREAT:
			return "judge_slide_chain_great_late";
		case DrawableEvaluateSlideType.FASTGOOD:
			return "judge_slide_chain_good_fast";
		case DrawableEvaluateSlideType.LATEGOOD:
			return "judge_slide_chain_good_late";
		case DrawableEvaluateSlideType.TOOFAST:
			return "judge_slide_chain_miss_fast";
		case DrawableEvaluateSlideType.TOOLATE:
			return "judge_slide_chain_miss_late";
		}
		return null;
	}

}
