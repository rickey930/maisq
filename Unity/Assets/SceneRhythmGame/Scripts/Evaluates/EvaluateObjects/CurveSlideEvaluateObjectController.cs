﻿using UnityEngine;
using System.Collections;

public class CurveSlideEvaluateObjectController : StraightSlideEvaluateObjectController {

	[SerializeField]
	protected Mesh clockwiseMesh;
	[SerializeField]
	protected Mesh counterClockwiseMesh;

	protected float radius;
	protected float vec;

	public virtual void Setup (DrawableEvaluateSlideType evaluateType, Vector2 center, float deg, float vec, float radius) {
		base.Setup (evaluateType, center, deg);
		this.vec = vec;
		this.radius = radius;
	}

	// Use this for initialization
	protected override void Start () {
		base.Start ();
		var meshfilter = GetComponentInChildren<MeshFilter>();
		if (this.vec > 0) {
			meshfilter.sharedMesh = clockwiseMesh;
		}
		else {
			meshfilter.sharedMesh = counterClockwiseMesh;
		}
		var mesh = meshfilter.mesh;

		float height = 32.0f;
		float centerYRate = 0.5f;
		
		float axisDeg = -5.0f * this.vec;//45.0f * 0 + 22.5f; //曲線スライドの中心から終点を見た角度を渡す.
		float vec = this.vec; //曲線スライドの方向(時計なら1/反時計なら(-1))を渡す.
		float distanceDeg = 35.0f; // 判定画像をどれだけ曲げるか.
		float radius = this.radius; //曲線スライドの半径.
		// このメッシュを使うゲームオブジェクトは曲線スライドの中心位置に移動させておく.
		// axisDegを0にする代わりにゲームオブジェクト自体を(曲線スライドのstartDeg + (曲線スライドのdistanceDeg * 曲線スライドのvec))で回していてもいいかも.
		
		
		// 半径レートを求めて判定画像のサイズを調整するのもアリではある.
		//float radiusRate = radius / Constants.instance.MAIMAI_OUTER_RADIUS;
		//height *= radiusRate;

		Vector3[] vertices = mesh.vertices;
		for (int i = 0; i < vertices.Length / 2; i++) {
			float progress = (float)i / (float)(vertices.Length / 2 - 1);
			// 上.
			var up = CircleCalculator.PointOnCircle(Vector2.zero, radius +height-(height * centerYRate), axisDeg.ChangeDegHandSystem() + distanceDeg * vec * progress).ChangePosHandSystem();
			vertices[i].x = -up.x;
			vertices[i].y = up.y;
			// 下.
			var down = CircleCalculator.PointOnCircle(Vector2.zero, radius +0-(height * centerYRate), axisDeg.ChangeDegHandSystem() + distanceDeg * vec * progress).ChangePosHandSystem();
			vertices[vertices.Length - 1 - i].x = -down.x;
			vertices[vertices.Length - 1 - i].y = down.y;
		}
		mesh.vertices = vertices;
	}
}
