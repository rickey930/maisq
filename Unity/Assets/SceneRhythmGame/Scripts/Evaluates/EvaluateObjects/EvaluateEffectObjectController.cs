﻿using UnityEngine;
using System.Collections;

public class EvaluateEffectObjectController : MonoBehaviour {
	[SerializeField]
	private Transform rotateHierarchy;
	[SerializeField]
	protected Transform translateHierarchy;

	private int buttonNumber;
	protected DrawableEvaluateTapType evaluateTapType;
	protected DrawableEvaluateBreakType evaluateBreakType;
	private bool isBreak;
	protected bool visualizeFastLate;

	public void Setup (int buttonNumber, DrawableEvaluateTapType evaluateTapType, bool visualizeFastLate) {
		this.buttonNumber = buttonNumber;
		this.evaluateTapType = evaluateTapType;
		this.isBreak = false;
		this.visualizeFastLate = visualizeFastLate;
	}

	public void Setup (int buttonNumber, DrawableEvaluateBreakType evaluateBreakType, bool visualizeFastLate) {
		this.buttonNumber = buttonNumber;
		this.evaluateBreakType = evaluateBreakType;
		this.isBreak = true;
		this.visualizeFastLate = visualizeFastLate;
	}

	// Use this for initialization
	protected virtual void Start () {
		rotateHierarchy.localRotation = Quaternion.Euler(0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(Constants.instance.GetPieceDegree(buttonNumber)));
		translateHierarchy.localPosition = new Vector3 (0, Constants.instance.MAIMAI_OUTER_RADIUS, 0);
		var renderer = GetComponentInChildren<SpriteRenderer> ();
		string spriteName;
		bool fsView = visualizeFastLate;
		if (!isBreak) {
			spriteName = !fsView ? GetTapSpriteName () : GetFSTapSpriteName ();
		}
		else {
			spriteName = !fsView ? GetBreakSpriteName () : GetFSBreakSpriteName ();
		}
		if (!string.IsNullOrEmpty (spriteName)) {
			renderer.sprite = SpriteTank.Get (spriteName);
		}
	}

	protected virtual string GetTapSpriteName() {
		switch (evaluateTapType) {
		case DrawableEvaluateTapType.PERFECT:
			return "effect_perfect";
		case DrawableEvaluateTapType.FASTGREAT:
		case DrawableEvaluateTapType.LATEGREAT:
			return "effect_great";
		case DrawableEvaluateTapType.FASTGOOD:
		case DrawableEvaluateTapType.LATEGOOD:
			return "effect_good";
		}
		return null;
	}

	protected virtual string GetFSTapSpriteName() {
		switch (evaluateTapType) {
		case DrawableEvaluateTapType.PERFECT:
			return "effect_perfect";
		case DrawableEvaluateTapType.FASTGREAT:
		case DrawableEvaluateTapType.FASTGOOD:
			return "effect_fast";
		case DrawableEvaluateTapType.LATEGREAT:
		case DrawableEvaluateTapType.LATEGOOD:
			return "effect_slow";
		}
		return null;
	}

	protected virtual string GetBreakSpriteName() {
		switch (evaluateBreakType) {
		case DrawableEvaluateBreakType.P2600:
			return "effect_break_2600";
		case DrawableEvaluateBreakType.P2550F:
		case DrawableEvaluateBreakType.P2550L:
		case DrawableEvaluateBreakType.P2500F:
		case DrawableEvaluateBreakType.P2500L:
			return "effect_break_perfect";
		case DrawableEvaluateBreakType.P2000F:
		case DrawableEvaluateBreakType.P2000L:
		case DrawableEvaluateBreakType.P1500F:
		case DrawableEvaluateBreakType.P1500L:
		case DrawableEvaluateBreakType.P1250F:
		case DrawableEvaluateBreakType.P1250L:
			return "effect_break_great";
		case DrawableEvaluateBreakType.P1000F:
		case DrawableEvaluateBreakType.P1000L:
			return "effect_break_good";
		}
		return null;
	}
	
	protected virtual string GetFSBreakSpriteName() {
		switch (evaluateBreakType) {
		case DrawableEvaluateBreakType.P2600:
			return "effect_perfect";
		case DrawableEvaluateBreakType.P2550F:
		case DrawableEvaluateBreakType.P2500F:
		case DrawableEvaluateBreakType.P2000F:
		case DrawableEvaluateBreakType.P1500F:
		case DrawableEvaluateBreakType.P1250F:
		case DrawableEvaluateBreakType.P1000F:
			return "effect_fast";
		case DrawableEvaluateBreakType.P2550L:
		case DrawableEvaluateBreakType.P2500L:
		case DrawableEvaluateBreakType.P2000L:
		case DrawableEvaluateBreakType.P1500L:
		case DrawableEvaluateBreakType.P1250L:
		case DrawableEvaluateBreakType.P1000L:
			return "effect_slow";
		}
		return null;
	}
}
