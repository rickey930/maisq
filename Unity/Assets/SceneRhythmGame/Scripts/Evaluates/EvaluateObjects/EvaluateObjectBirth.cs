using UnityEngine;
using System.Collections;

public class EvaluateObjectBirth : MonoBehaviour {
/*
ボタンをタップしたら、エフェクトをInstantiateするもの.
ホールド中なら、エフェクトを一定の間隔でInstantiateし続けるもの.
Perfectとかそのエフェクトとかを、表示するもの.
Justを正しい方向に向けて表示するもの.

このクラスはInstantiateするクラス.
Instantiate先で表示時間の管理とか拡大とかいろいろ行う (ポーズで止めるのもそこで。ポーズのフラグは別のどこかで).
ホールドのエフェクト自動InstantiateクラスをInstantiateし、ホールドオブジェクトに教える.
*/
	[SerializeField]
	private GameObject evaluateEffectPrefab; //TAP, BREAK用のエフェクトのプレハブ.
	[SerializeField]
	private GameObject evaluateTexturePrefab;
	[SerializeField]
	private GameObject starightSlideEvaluatePrefab;
	[SerializeField]
	private GameObject curveSlideEvaluatePrefab;
	[SerializeField]
	private GameObject chainSlideEvaluatePrefab;
	[SerializeField]
	private GameObject holdEffectPrefab;
	
	public void Birth(DrawableEvaluateTapType type, int buttonId, bool visualizeFastLate) {
		var obj = Instantiate (evaluateTexturePrefab) as GameObject;
		obj.transform.parent = transform;
		var ctrl = obj.GetComponent<EvaluateEffectObjectController> ();
		ctrl.Setup (buttonId, type, visualizeFastLate);
		if (type != DrawableEvaluateTapType.MISS) {
			obj = Instantiate (evaluateEffectPrefab) as GameObject;
			obj.transform.parent = transform;
			ctrl = obj.GetComponent<EvaluateEffectObjectController> ();
			ctrl.Setup (buttonId, type, visualizeFastLate);
		}
	}

	/// <summary>
	/// ストレート用.
	/// </summary>
	public void Birth(DrawableEvaluateSlideType type, Vector2 target, float deg) {
		var obj = Instantiate (starightSlideEvaluatePrefab) as GameObject;
		obj.transform.parent = transform;
		var ctrl = obj.GetComponent<StraightSlideEvaluateObjectController> ();
		ctrl.Setup (type, target, deg);
	}

	/// <summary>
	/// カーブ用.
	/// </summary>
	public void Birth(DrawableEvaluateSlideType type, Vector2 center, float deg, float vec, float radius) {
		var obj = Instantiate (curveSlideEvaluatePrefab) as GameObject;
		obj.transform.parent = transform;
		var ctrl = obj.GetComponent<CurveSlideEvaluateObjectController> ();
		ctrl.Setup (type, center, deg, vec, radius);
	}
	
	/// <summary>
	/// チェイン用.
	/// </summary>
	public void Birth(DrawableEvaluateSlideType type, MaimaiSlidePattern patternInfo) {
		var obj = Instantiate (chainSlideEvaluatePrefab) as GameObject;
		obj.transform.parent = transform;
		var ctrl = obj.GetComponent<ChainSlideEvaluateObjectController> ();
		ctrl.Setup (type, patternInfo);
	}
	
	public void Birth(DrawableEvaluateBreakType type, int buttonId, bool visualizeFastLate) {
		var obj = Instantiate (evaluateTexturePrefab) as GameObject;
		obj.transform.parent = transform;
		var ctrl = obj.GetComponent<EvaluateEffectObjectController> ();
		ctrl.Setup (buttonId, type, visualizeFastLate);
		if (type != DrawableEvaluateBreakType.MISS) {
			obj = Instantiate (evaluateEffectPrefab) as GameObject;
			obj.transform.parent = transform;
			ctrl = obj.GetComponent<EvaluateEffectObjectController> ();
			ctrl.Setup (buttonId, type, visualizeFastLate);
		}
	}
	
	public HoldEffectObjectController HoldHeadEffectBirth(DrawableEvaluateTapType type, int buttonId) {
		var obj = Instantiate (holdEffectPrefab) as GameObject;
		obj.transform.parent = transform;
		var ctrl = obj.GetComponent<HoldEffectObjectController> ();
		ctrl.Setup (buttonId, type);
		return ctrl;
	}





}

public enum DrawableEvaluateTapType {
	PERFECT, FASTGREAT, LATEGREAT, FASTGOOD, LATEGOOD, MISS, NONE
}
public enum DrawableEvaluateSlideType {
	JUST, FASTGREAT, LATEGREAT, FASTGOOD, LATEGOOD, TOOFAST, TOOLATE, NONE
}
public enum DrawableEvaluateBreakType {
	P2600, P2550F, P2550L, P2500F, P2500L, P2000F, P2000L, P1500F, P1500L,  P1250F, P1250L, P1000F,P1000L, MISS, NONE
}

