﻿using UnityEngine;
using System.Collections;

public class EvaluateTextureObjectController : EvaluateEffectObjectController {
	protected override void Start () {
		base.Start ();
		translateHierarchy.localPosition = new Vector3 (0, Constants.instance.EVALUATE_SHOW_RADIUS, 0);
	}
	protected override string GetTapSpriteName () {
		switch (evaluateTapType) {
		case DrawableEvaluateTapType.PERFECT:
			return "judge_perfect";
		case DrawableEvaluateTapType.FASTGREAT:
		case DrawableEvaluateTapType.LATEGREAT:
			return "judge_great";
		case DrawableEvaluateTapType.FASTGOOD:
		case DrawableEvaluateTapType.LATEGOOD:
			return "judge_good";
		case DrawableEvaluateTapType.MISS:
			return "judge_miss";
		}
		return null;
	}

	protected override string GetBreakSpriteName () {
		switch (evaluateBreakType) {
		case DrawableEvaluateBreakType.P2600:
			return "judge_break_2600";
		case DrawableEvaluateBreakType.P2550F:
		case DrawableEvaluateBreakType.P2550L:
			return "judge_break_2550";
		case DrawableEvaluateBreakType.P2500F:
		case DrawableEvaluateBreakType.P2500L:
			return "judge_break_2500";
		case DrawableEvaluateBreakType.P2000F:
		case DrawableEvaluateBreakType.P2000L:
			return "judge_break_2000";
		case DrawableEvaluateBreakType.P1500F:
		case DrawableEvaluateBreakType.P1500L:
			return "judge_break_1500";
		case DrawableEvaluateBreakType.P1250F:
		case DrawableEvaluateBreakType.P1250L:
			return "judge_break_1250";
		case DrawableEvaluateBreakType.P1000F:
		case DrawableEvaluateBreakType.P1000L:
			return "judge_break_1000";
		}
		return null;
	}

	protected override string GetFSTapSpriteName () {
		return GetTapSpriteName ();
	}

	protected override string GetFSBreakSpriteName () {
		return GetBreakSpriteName ();
	}
}
