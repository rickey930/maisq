﻿using UnityEngine;
using System.Collections;

public class HoldEffectObjectController : MonoBehaviour, IHoldingEffect {
	
	[SerializeField]
	private Transform rotateHierarchy;
	[SerializeField]
	private Transform translateHierarchy;
	
	private int buttonNumber;
	private DrawableEvaluateTapType evaluateType;

	[SerializeField]
	private float time;
	private float timer;

	[SerializeField]
	private GameObject effectSpriteRendererObjectPrefab;
	
	public void Setup (int buttonNumber, DrawableEvaluateTapType evaluateType) {
		this.buttonNumber = buttonNumber;
		this.evaluateType = evaluateType;
	}
	
	// Use this for initialization
	void Start () {
		rotateHierarchy.localRotation = Quaternion.Euler(0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(Constants.instance.GetPieceDegree(buttonNumber)));
		translateHierarchy.localPosition = new Vector3 (0, Constants.instance.MAIMAI_OUTER_RADIUS, 0);
		timer = time + 1;
	}

	void Update () {
		if (timer > time) {
			var obj = Instantiate(effectSpriteRendererObjectPrefab) as GameObject;
			obj.transform.parent = translateHierarchy;
			obj.transform.localPosition = Vector3.zero;
			obj.transform.localRotation = Quaternion.Euler(Vector3.zero);
			obj.transform.localScale = Vector3.one;
			var renderer = obj.GetComponent<SpriteRenderer> ();
			renderer.sprite = GetSprite ();
			timer = 0;
		}
		timer += Time.deltaTime;
	}

	public void SetVisible(bool value) {
		Destroy (gameObject);
	}

	private Sprite GetSprite() {
		switch (evaluateType) {
		case DrawableEvaluateTapType.PERFECT:
			return MaipadDynamicCreatedSpriteTank.instance.holdingPerfectCircleEffect;
		case DrawableEvaluateTapType.FASTGREAT:
		case DrawableEvaluateTapType.LATEGREAT:
			return MaipadDynamicCreatedSpriteTank.instance.holdingGreatCircleEffect;
		case DrawableEvaluateTapType.FASTGOOD:
		case DrawableEvaluateTapType.LATEGOOD:
			return MaipadDynamicCreatedSpriteTank.instance.holdingGoodCircleEffect;
		}
		return null;
	}
}
