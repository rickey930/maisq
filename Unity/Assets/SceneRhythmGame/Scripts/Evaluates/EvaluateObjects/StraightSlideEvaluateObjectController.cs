﻿using UnityEngine;
using System.Collections;

public class StraightSlideEvaluateObjectController : MonoBehaviour {
	[SerializeField]
	protected Transform targetDirectionHierarchy;

	protected DrawableEvaluateSlideType evaluateType;
	protected Vector2 target;
	protected float deg;

	public virtual void Setup (DrawableEvaluateSlideType evaluateType, Vector2 target, float deg) {
		this.evaluateType = evaluateType;
		this.target = target;
		this.deg = deg;
	}

	// Use this for initialization
	protected virtual void Start () {
		targetDirectionHierarchy.localPosition = Constants.instance.ToLeftHandedCoordinateSystemPosition(target);
		targetDirectionHierarchy.localRotation = Quaternion.Euler (0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(deg));
		var renderer = GetComponentInChildren<MeshRenderer> ();
		string materialName = GetMaterialName ();
		//renderer.sprite = SpriteTank.Get (spriteName);
		renderer.sharedMaterial = MaterialTank.Get (materialName);
	}

	protected virtual string GetMaterialName() {
		switch (evaluateType) {
		case DrawableEvaluateSlideType.JUST:
			return "judge_slide_just";
		case DrawableEvaluateSlideType.FASTGREAT:
			return "judge_slide_great_fast";
		case DrawableEvaluateSlideType.LATEGREAT:
			return "judge_slide_great_late";
		case DrawableEvaluateSlideType.FASTGOOD:
			return "judge_slide_good_fast";
		case DrawableEvaluateSlideType.LATEGOOD:
			return "judge_slide_good_late";
		case DrawableEvaluateSlideType.TOOFAST:
			return "judge_slide_miss_fast";
		case DrawableEvaluateSlideType.TOOLATE:
			return "judge_slide_miss_late";
		}
		return null;
	}
}
