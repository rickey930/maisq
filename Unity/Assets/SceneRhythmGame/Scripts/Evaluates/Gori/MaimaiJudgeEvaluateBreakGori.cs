﻿using UnityEngine;
using System.Collections;

public class MaimaiJudgeEvaluateBreakGori : MaimaiJudgeEvaluateBreakMaji {
	public MaimaiJudgeEvaluateBreakGori(MaimaiJudgeEvaluateManager manager, 
	                                    int p2600Rate,
	                                    long inMissFastTime, long inP2600Time, long inMissLateTime)
		: base(manager, p2600Rate, 0, 0, 0, 0, 0, 0,
		       inMissFastTime, 0, 0, 0, 0, 0, 0, inP2600Time,
		       0, 0, 0, 0, 0, 0, inMissLateTime) {

	}
	
	public override void Evaluate (long timing, MaimaiNote note) {
		if (timing >= -this.inFastMissTime && timing < -this.inP2500FastTime) {
			this.MissCallBack(note);
		}
		else if (timing >= -this.inP2600Time && timing <= this.inP2600Time) {
			this.P2600CallBack(note);
		}
		else if (timing > this.inP2500LateTime && timing <= this.inLateMissTime) {
			this.MissCallBack(note);
		}
	}
}
