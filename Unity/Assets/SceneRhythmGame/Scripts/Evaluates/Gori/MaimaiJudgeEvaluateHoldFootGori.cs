﻿using UnityEngine;
using System.Collections;

public class MaimaiJudgeEvaluateHoldFootGori : MaimaiJudgeEvaluateHoldFootMaji {
	public MaimaiJudgeEvaluateHoldFootGori(MaimaiJudgeEvaluateManager manager, MaimaiJudgeEvaluateHoldHead headEvaluater, long inIntrustHeadEvaluateTime, long inMajiHoldOverMissTime)
	: base (manager, headEvaluater, inIntrustHeadEvaluateTime, inMajiHoldOverMissTime) {

	}

	public override void Evaluate (long timing, MaimaiNote note) {
		MaimaiHoldFootNote fNote = (MaimaiHoldFootNote)note;
		if (fNote.relativeNote.pushed) {
			//離すのが早い.
			if (timing < -this.inIntrustHeadEvaluateTime) {
				this.MissCallBack(note);
			}
			//離したけど遅い.
			else if (timing > this.inMajiHoldOverMissTime) {
				this.MissCallBack(note);
			}
			//タイミングばっちり.
			else {
				this.OkPullCallBack(note);
			}
			//離さなかったのはミス判定に任せる.
		}
	}
}
