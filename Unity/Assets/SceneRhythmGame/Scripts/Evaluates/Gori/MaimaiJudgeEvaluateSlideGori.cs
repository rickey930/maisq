﻿using UnityEngine;
using System.Collections;

public class MaimaiJudgeEvaluateSlideGori : MaimaiJudgeEvaluateSlideMaji {
	public MaimaiJudgeEvaluateSlideGori(MaimaiJudgeEvaluateManager manager, MaimaiJudgeEvaluateTap judgeEvaluateStarTap, long backSlideTime,
	                                    int perfectRate, 
	                               		long inPerfectTime, long inLateMissTime)
	: base (manager, judgeEvaluateStarTap, backSlideTime, perfectRate, 0, 0, 0, 0, inPerfectTime, 0, 0, inLateMissTime) {

	}
}
