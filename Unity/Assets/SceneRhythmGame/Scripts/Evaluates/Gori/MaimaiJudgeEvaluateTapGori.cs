﻿using UnityEngine;
using System.Collections;

public class MaimaiJudgeEvaluateTapGori : MaimaiJudgeEvaluateTapMaji {
	public MaimaiJudgeEvaluateTapGori(MaimaiJudgeEvaluateManager manager,
	                                  int perfectRate,
	                                  long inFastMissTime, long inPerfectTime, long inLateMissTime)
	: base (manager, perfectRate, 0, 0, inFastMissTime, 0, 0, 0, 0, 0, inLateMissTime) {

	}
	
	public override void Evaluate (long timing, MaimaiNote note) {
		if (timing >= -this.inFastMissTime && timing < -this.inPerfectTime) {
			this.MissCallBack(note);
		}
		else if (timing >= -this.inPerfectTime && timing <= this.inPerfectTime) {
			this.PerfectCallBack(note);
		}
		else if (timing > this.inPerfectTime && timing <= this.inLateMissTime) {
			this.MissCallBack(note);
		}
	}
}
