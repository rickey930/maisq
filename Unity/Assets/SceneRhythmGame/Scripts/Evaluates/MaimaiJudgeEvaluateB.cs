﻿using UnityEngine;
using System.Collections;

public abstract class MaimaiJudgeEvaluateB : MaimaiJudgeEvaluate {
	public MaimaiJudgeEvaluateB(MaimaiJudgeEvaluateManager manager) : base (manager) {
		miss = new MaimaiJudgeEvaluateInfo(0);
	}

	public MaimaiJudgeEvaluateInfo miss { get; protected set; }
	public override int GetMissCount ()	{
		return miss.count;
	}

	public virtual void MissCallBack(MaimaiNote note) {
		var noteB = (MaimaiNoteB)note;
		manager.ShowEvaluate (DrawableEvaluateTapType.MISS, noteB.GetButtonId ());
		miss.count++;
		manager.ResetCombo ();
		manager.ResetPCombo ();
		manager.ResetBPCombo ();
		note.judged = true;
	}
	
	public virtual void Reset () {
		miss.count = 0;
	}
}
