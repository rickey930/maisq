using UnityEngine;
using System.Collections;

public class MaimaiJudgeEvaluateHoldFoot : MaimaiJudgeEvaluate {
	public MaimaiJudgeEvaluateHoldFoot(MaimaiJudgeEvaluateManager manager, MaimaiJudgeEvaluateHoldHead headEvaluater, long inIntrustHeadEvaluateTime) : base (manager) {
		this.headEvaluater = headEvaluater;
		this.inIntrustHeadEvaluateTime = inIntrustHeadEvaluateTime;
	}

	public MaimaiJudgeEvaluateHoldHead headEvaluater { get; protected set; }
	public long inIntrustHeadEvaluateTime { get; protected set; }

	public override void Evaluate (long timing, MaimaiNote note) {
		MaimaiHoldFootNote fNote = (MaimaiHoldFootNote)note;
		if (fNote.relativeNote.pushed) {
			//離すのが早い
			if (timing < -this.inIntrustHeadEvaluateTime) {
				this.EarlyPullCallBack(note);
			}
			//タイミングばっちり
			else {
				this.OkPullCallBack(note);
			}
			//離すのが遅いのはミス判定に任せる
		}
	}

	public virtual void EarlyPullCallBack(MaimaiNote note) {
		MaimaiHoldFootNote fNote = (MaimaiHoldFootNote)note;
		manager.PlayNoteSE("se_tap_good");
		manager.ShowEvaluate(DrawableEvaluateTapType.FASTGOOD, fNote.relativeNote.GetButtonId());
		headEvaluater.good.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		note.judged = true;
		note.visible = false;
		fNote.relativeNote.visible = false;
		//エフェクトが設定されてあれば非表示にする
		if (fNote.relativeNote.effectInstance != null) {
			fNote.relativeNote.effectInstance.SetVisible(false);
			fNote.relativeNote.effectInstance = null;
		}
		//シンクはミスにする
		manager.SyncEvaluation(note, SyncEvaluate.MISS);
	}

	
	public virtual void OkPullCallBack(MaimaiNote note) {
		manager.PlayNoteSE("se_tap_perfect");
		var fNote = (MaimaiHoldFootNote)note;
		manager.ShowEvaluate(fNote.relativeNote.evaluate, fNote.relativeNote.GetButtonId());
		if (fNote.relativeNote.evaluate == DrawableEvaluateTapType.PERFECT) {
			headEvaluater.perfect.count++;
			manager.AddPCombo();
			manager.AddBPCombo();
			manager.SyncEvaluation(note, SyncEvaluate.PERFECT);
		}
		else if (fNote.relativeNote.evaluate == DrawableEvaluateTapType.FASTGREAT || fNote.relativeNote.evaluate == DrawableEvaluateTapType.LATEGREAT) {
			headEvaluater.great.count++;
			manager.ResetPCombo();
			manager.ResetBPCombo();
			if (fNote.relativeNote.evaluate == DrawableEvaluateTapType.FASTGREAT)
				manager.SyncEvaluation(note, SyncEvaluate.FASTGREAT);
			else
				manager.SyncEvaluation(note, SyncEvaluate.LATEGREAT);
		}
		else if (fNote.relativeNote.evaluate == DrawableEvaluateTapType.FASTGOOD || fNote.relativeNote.evaluate == DrawableEvaluateTapType.LATEGOOD) {
			headEvaluater.good.count++;
			manager.ResetPCombo();
			manager.ResetBPCombo();
			if (fNote.relativeNote.evaluate == DrawableEvaluateTapType.FASTGOOD)
				manager.SyncEvaluation(note, SyncEvaluate.FASTGOOD);
			else
				manager.SyncEvaluation(note, SyncEvaluate.LATEGOOD);
		}
		manager.AddCombo();
		note.judged = true;
		note.visible = false;
		fNote.relativeNote.visible = false;
		//エフェクトが設定されてあれば非表示にする
		if (fNote.relativeNote.effectInstance != null) {
			fNote.relativeNote.effectInstance.SetVisible(false);
			fNote.relativeNote.effectInstance = null;
		}
	}
	
	public virtual void LateCallBack(MaimaiNote note) {
		manager.PlayNoteSE("se_tap_good");
		var fNote = (MaimaiHoldFootNote)note;
		manager.ShowEvaluate(DrawableEvaluateTapType.LATEGOOD, fNote.relativeNote.GetButtonId());
		headEvaluater.good.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		note.judged = true;
		note.visible = false;
		fNote.relativeNote.visible = false;
		//エフェクトが設定されてあれば非表示にする
		if (fNote.relativeNote.effectInstance != null) {
			fNote.relativeNote.effectInstance.SetVisible(false);
			fNote.relativeNote.effectInstance = null;
		}
		//シンクはミスにする
		manager.SyncEvaluation(note, SyncEvaluate.MISS);
	}

	public virtual void MissCallBack(MaimaiNote note) {
		manager.PlayNoteSE("se_tap_good");
		var fNote = (MaimaiHoldFootNote)note;
		manager.ShowEvaluate(DrawableEvaluateTapType.MISS, fNote.relativeNote.GetButtonId());
		headEvaluater.miss.count++;
		manager.ResetCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		note.judged = true;
		note.visible = false;
		fNote.relativeNote.visible = false;
		//エフェクトが設定されてあれば非表示にする
		if (fNote.relativeNote.effectInstance != null) {
			fNote.relativeNote.effectInstance.SetVisible(false);
			fNote.relativeNote.effectInstance = null;
		}
		//シンクはミスにする
		manager.SyncEvaluation(note, SyncEvaluate.MISS);
	}

	public override int GetPerfectCount() { return this.headEvaluater.GetPerfectCount(); }
	public override int GetPerfectScore() { return this.headEvaluater.GetPerfectScore(); }
	public override int GetGreatCount() { return this.headEvaluater.GetGreatCount(); }
	public override int GetGreatScore() { return this.headEvaluater.GetGreatScore(); }
	public override int GetGoodCount() { return this.headEvaluater.GetGoodCount(); }
	public override int GetGoodScore() { return this.headEvaluater.GetGoodScore(); }
	public override int GetMissCount() { return this.headEvaluater.GetMissCount(); }
	public override int GetNoteCount() { return this.headEvaluater.GetNoteCount (); }
}
