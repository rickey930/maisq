using UnityEngine;
using System.Collections;

public class MaimaiJudgeEvaluateHoldHead : MaimaiJudgeEvaluateTap {
	public MaimaiJudgeEvaluateHoldHead(MaimaiJudgeEvaluateManager manager,
	                              int perfectRate, int greatRate, int goodRate, 
	                              long inFastGoodTime, long inFastGreatTime, long inPerfectTime, long inLateGreatTime, long inLateGoodTime)
	: base (manager, perfectRate, greatRate, goodRate, inFastGoodTime, inFastGreatTime, inPerfectTime, inLateGreatTime, inLateGoodTime) {

	}

	public override void FastGoodCallBack (MaimaiNote note)
	{
		manager.PlayNoteSE("se_tap_good");
		var hNote = (MaimaiHoldHeadNote)note;
		hNote.evaluate = DrawableEvaluateTapType.FASTGOOD;
		hNote.pushed = true;
		hNote.effectInstance = manager.ShowHoldHeadEffect(hNote.evaluate, hNote.GetButtonId());
		hNote.judged = true;
		manager.SetDrawableCommentNote(note);
	}

	public override void FastGreatCallBack (MaimaiNote note)
	{
		manager.PlayNoteSE("se_tap_great");
		var hNote = (MaimaiHoldHeadNote)note;
		hNote.evaluate = DrawableEvaluateTapType.FASTGREAT;
		hNote.pushed = true;
		hNote.effectInstance = manager.ShowHoldHeadEffect(hNote.evaluate, hNote.GetButtonId());
		hNote.judged = true;
		manager.SetDrawableCommentNote(note);
	}

	public override void PerfectCallBack (MaimaiNote note)
	{
		manager.PlayNoteSE("se_tap_perfect");
		var hNote = (MaimaiHoldHeadNote)note;
		hNote.evaluate = DrawableEvaluateTapType.PERFECT;
		hNote.pushed = true;
		hNote.effectInstance = manager.ShowHoldHeadEffect(hNote.evaluate, hNote.GetButtonId());
		hNote.judged = true;
		manager.SetDrawableCommentNote(note);
	}

	public override void LateGreatCallBack (MaimaiNote note)
	{
		manager.PlayNoteSE("se_tap_great");
		var hNote = (MaimaiHoldHeadNote)note;
		hNote.evaluate = DrawableEvaluateTapType.LATEGREAT;
		hNote.pushed = true;
		hNote.effectInstance = manager.ShowHoldHeadEffect(hNote.evaluate, hNote.GetButtonId());
		hNote.judged = true;
		manager.SetDrawableCommentNote(note);
	}

	public override void LateGoodCallBack (MaimaiNote note)
	{
		manager.PlayNoteSE("se_tap_good");
		var hNote = (MaimaiHoldHeadNote)note;
		hNote.evaluate = DrawableEvaluateTapType.LATEGOOD;
		hNote.pushed = true;
		hNote.effectInstance = manager.ShowHoldHeadEffect(hNote.evaluate, hNote.GetButtonId());
		hNote.judged = true;
		manager.SetDrawableCommentNote(note);
	}

	public override void MissCallBack (MaimaiNote note)
	{
		var hNote = (MaimaiHoldHeadNote)note;
		manager.ShowEvaluate(DrawableEvaluateTapType.MISS, hNote.GetButtonId());
		miss.count++;
		manager.ResetCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		hNote.judged = true;
		hNote.visible = false;
		manager.SyncEvaluation(hNote.relativeNote, SyncEvaluate.MISS);
		hNote.relativeNote.judged = true;
	}
}
