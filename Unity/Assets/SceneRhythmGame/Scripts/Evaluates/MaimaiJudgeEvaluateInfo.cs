﻿using UnityEngine;
using System.Collections;

public class MaimaiJudgeEvaluateInfo {
	public int count { get; set; }
	public int rate { get; set; }

	public int score {
		get {
			return count * rate;
		}
	}

	public MaimaiJudgeEvaluateInfo(int rate) {
		this.count = 0;
		this.rate = rate;
	}
}
