using UnityEngine;
using RhythmGameLibrary;
using System.Collections;
using System.Collections.Generic;

public class MaimaiJudgeEvaluateManager : RhythmGameEvaluateOnBeat<int, MaimaiNote> {
	public MaimaiJudgeEvaluateManager(MaimaiTimer timer, MaimaiNote[] notes, long timeLag, float firstBpm, EvaluateObjectBirth mother, bool startBeatCountSoundEnabled, ConfigTypes.SoundEffect soundEffectType, ConfigTypes.AnswerSound answerSoundType, ConfigTypes.Judgement judgementType, bool visualizeFastLate) : base (timer, NotesPartition(notes), timeLag, 0, firstBpm, START_COUNT_START_TIME) {
		effectMother = mother;
		this.startBeatCountSoundEnabled = startBeatCountSoundEnabled;
		this.soundEffectType = soundEffectType;
		this.answerSoundType = answerSoundType;
		this.judgementType = judgementType;
		this.visualizeFastLate = visualizeFastLate;

		SetupEvaluaterNormal ();
		SetAchievementInfo(notes);

		if (judgementType == ConfigTypes.Judgement.MAJI) {
			SetupEvaluaterMaji ();
		}
		else if (judgementType == ConfigTypes.Judgement.GACHI) {
			SetupEvaluaterGachi ();
		}
		else if (judgementType == ConfigTypes.Judgement.GORI) {
			SetupEvaluaterGori ();
		}
		else {
			SetupEvaluaterNormal ();
		}
	}

	private void SetupEvaluaterNormal () {
		long miss = MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.TAP_CIRCLE);
		tapEvaluater = new MaimaiJudgeEvaluateTap(this, 500, 400, 250,
		                                          TapFastGoodTime, 125L, 90L, 200L, miss);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.HOLD_HEAD);
		holdHeadEvaluater = new MaimaiJudgeEvaluateHoldHead(this, 1000, 800, 500,
		                                                    150L, 125L, 90L, 200L, miss);
		holdFootEvaluater = new MaimaiJudgeEvaluateHoldFoot(this, holdHeadEvaluater,
		                                                    200L);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.SLIDE);
		slideEvaluater = new MaimaiJudgeEvaluateSlide(this, this.tapEvaluater, 50L, 1500, 1200, 750,
		                                              425L, 250L, 425L, miss);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.BREAK_CIRCLE);
		breakEvaluater = new MaimaiJudgeEvaluateBreak(this, 2600, 2550, 2500, 2000, 1500, 1250, 1000,
		                                              //InP1000FastTime, InP1250FastTime, InP1500FastTime, InP2000FastTime, InP2500FastTime, InP2550FastTime, InP2600Time, InP2550LateTime, InP2500LateTime, InP2000LateTime, InP1500LateTime, InP1250LateTime, InP1000LateTime
		                                              150L, 137L, 125L, 112L, 90L, 63L, 36L, 63L, 90L, 150L, 200L, 250L, miss);
	}

	private void SetupEvaluaterMaji () {
		long miss = MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.TAP_CIRCLE);
		tapEvaluater = new MaimaiJudgeEvaluateTapMaji(this, 500, 400, 250,
		                                              150L, 125L, 90L, 63L, 90L, 150L, miss);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.HOLD_HEAD);
		holdHeadEvaluater = new MaimaiJudgeEvaluateHoldHeadMaji(this, 1000, 800, 500,
		                                                        150L, 125L, 90L, 63L, 90L, 150L, miss);
		holdFootEvaluater = new MaimaiJudgeEvaluateHoldFootMaji(this, holdHeadEvaluater,
		                                                        125L, 125L);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.SLIDE);
		slideEvaluater = new MaimaiJudgeEvaluateSlideMaji(this, this.tapEvaluater, 50L, 1500, 1200, 750,
		                                                  425L, 250L, 125L, 250L, 425L, miss);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.BREAK_CIRCLE);
		breakEvaluater = new MaimaiJudgeEvaluateBreakMaji(this, 2600, 2550, 2500, 2000, 1500, 1250, 1000,
		                                              //InFastMissTime, InP1000FastTime, InP1250FastTime, InP1500FastTime, InP2000FastTime, InP2500FastTime, InP2550FastTime, InP2600Time, InP2550LateTime, InP2500LateTime, InP2000LateTime, InP1500LateTime, InP1250LateTime, InP1000LateTime, InLateMissTime
		                                                  150L, 137L, 125L, 112L, 90L, 63L, 36L, 18L, 36L, 63L, 90L, 112L, 125L, 150L, miss);
	}

	private void SetupEvaluaterGachi () {
		long miss = MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.TAP_CIRCLE);
		tapEvaluater = new MaimaiJudgeEvaluateTapMaji(this, 500, 400, 250,
		                                              150L, 90L, 63L, 36L, 63L, 90L, miss);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.HOLD_HEAD);
		holdHeadEvaluater = new MaimaiJudgeEvaluateHoldHeadMaji(this, 1000, 800, 500,
		                                                        150L, 90L, 63L, 52L, 63L, 90L, miss);
		holdFootEvaluater = new MaimaiJudgeEvaluateHoldFootMaji(this, holdHeadEvaluater,
		                                                        90L, 90L);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.SLIDE);
		slideEvaluater = new MaimaiJudgeEvaluateSlideMaji(this, this.tapEvaluater, 50L, 1500, 1200, 750,
		                                                  180L, 120L, 90L, 120L, 180L, miss);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.BREAK_CIRCLE);
		breakEvaluater = new MaimaiJudgeEvaluateBreakMaji(this, 2600, 2550, 2500, 2000, 1500, 1250, 1000,
		                                                  //InFastMissTime, InP1000FastTime, InP1250FastTime, InP1500FastTime, InP2000FastTime, InP2500FastTime, InP2550FastTime, InP2600Time, InP2550LateTime, InP2500LateTime, InP2000LateTime, InP1500LateTime, InP1250LateTime, InP1000LateTime, InLateMissTime
		                                                  150L, 126L, 108L, 90L, 72L, 54L, 36L, 18L, 36L, 54L, 72L, 90L, 108L, 126L, miss);
	}

	private void SetupEvaluaterGori () {
		long miss = MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.TAP_CIRCLE);
		tapEvaluater = new MaimaiJudgeEvaluateTapGori(this, 500,
		                                              150L, 18L, miss);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.HOLD_HEAD);
		holdHeadEvaluater = new MaimaiJudgeEvaluateHoldHeadGori(this, 1000,
		                                                        150L, 36L, miss);
		holdFootEvaluater = new MaimaiJudgeEvaluateHoldFootGori(this, holdHeadEvaluater,
		                                                        63L, 36L);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.SLIDE);
		slideEvaluater = new MaimaiJudgeEvaluateSlideGori(this, this.tapEvaluater, 50L, 1500, 
		                                                  90L, miss);
		miss = MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.BREAK_CIRCLE);
		breakEvaluater = new MaimaiJudgeEvaluateBreakGori(this, 2600,
		                                                  //InFastMissTime, InP2600Time, InLateMissTime
		                                                  150L, 18L, miss);
	}

	/// <summary>
	/// 評価に必要なノートを、アクションIDで振り分ける.
	/// </summary>
	private static Dictionary<int, MaimaiNote[]> NotesPartition(MaimaiNote[] notes) {
		var _ret = new Dictionary<int, List<MaimaiNote>>();
		foreach (var note in notes) {
			if (!_ret.ContainsKey(note.actionId)) {
				_ret[note.actionId] = new List<MaimaiNote>();
			}
			_ret[note.actionId].Add(note);
		}
		var ret = new Dictionary<int, MaimaiNote[]>();
		foreach (var key in _ret.Keys) {
			ret[key] = _ret[key].ToArray();
		}
		return ret;
	}

	public static long START_COUNT_START_TIME { get { return 500L; } }
	public static long MissTimeBank(MaimaiNoteType type) {
		if (type == MaimaiNoteType.TAP_CIRCLE ||
		    type == MaimaiNoteType.TAP_STAR)
			return 300L;
		else if (type == MaimaiNoteType.HOLD_HEAD)
			return 300L;
		else if (type == MaimaiNoteType.HOLD_FOOT) //押しっぱなしgood時間
			return 300L;
		else if (type == MaimaiNoteType.SLIDE)
			return 600L;
		else if (type == MaimaiNoteType.BREAK_CIRCLE ||
		         type == MaimaiNoteType.BREAK_STAR)
			return 300L;
		return 0L;
	}
	public static long TapFastGoodTime = 150L;

	public MaimaiTimer GetTimer() { return (MaimaiTimer)timer; }
	
	public void PlayNoteSE(string name) {
		PlayNoteSE (name, false);
	}
	public void PlayNoteSE(string name, bool isBreak2600) {
		// オプションによってON/OFFする.
		bool option = 
			soundEffectType == ConfigTypes.SoundEffect.ON || 
			(soundEffectType == ConfigTypes.SoundEffect.ONLY_BREAK2600 && isBreak2600);
		if (option && !resetting) {
			Utility.SoundEffectManager.Play (name);
		}
	}

	//ノートコメント描画用ノート. //旧maiPad仕様.
	//private MaimaiNote drawableCommentNote = null;
	public void SetDrawableCommentNote(MaimaiNote note) {
		//this.drawableCommentNote = note;
	}

	protected bool startBeatCountSoundEnabled { get; set; }
	protected ConfigTypes.SoundEffect soundEffectType { get; set; }
	protected ConfigTypes.AnswerSound answerSoundType { get; set; }
	protected ConfigTypes.Judgement judgementType { get; set; }
	protected bool visualizeFastLate { get; set; }

	#region 評価や時間で動作する関数群.

	/*
	 * スライドに関して、.
	 * OnJustやOnMissはSlideNoteで判別する.
	 * EvaluateはSlidePatternNoteで判別する.
	 * OnGuideはSlideNoteMoveStartで音を出す.
	 */

	protected override void Evaluate (long timing, long oldTiming, MaimaiNote note) {
		MaimaiNote mNote = (MaimaiNote)note;
		if (mNote.GetNoteType() == MaimaiNoteType.TAP_CIRCLE ||
		    mNote.GetNoteType() == MaimaiNoteType.TAP_STAR) {
			tapEvaluater.Evaluate(timing, mNote);
		}
		else if (mNote.GetNoteType() == MaimaiNoteType.HOLD_HEAD) {
			holdHeadEvaluater.Evaluate(timing, mNote);
		}
		else if (mNote.GetNoteType() == MaimaiNoteType.HOLD_FOOT) {
			holdFootEvaluater.Evaluate(timing, mNote);
		}
		else if (mNote.GetNoteType() == MaimaiNoteType.SLIDE) {
			slideEvaluater.Evaluate(timing, mNote);
		}
		else if (mNote.GetNoteType() == MaimaiNoteType.BREAK_CIRCLE ||
		         mNote.GetNoteType() == MaimaiNoteType.BREAK_STAR) {
			breakEvaluater.Evaluate(timing, mNote);
		}
		else if (mNote.GetNoteType() == MaimaiNoteType.SLIDE_MOVE_START) {
			base.OnJust(mNote);
		}
		else if (mNote.GetNoteType() == MaimaiNoteType.SLIDE_HEAD) {
			base.OnJust(mNote);
		}
	}

	protected override void OnJust (MaimaiNote note) {
		MaimaiNote mNote = (MaimaiNote)note;
		if (mNote.GetNoteType() == MaimaiNoteType.TAP_CIRCLE ||
		    mNote.GetNoteType() == MaimaiNoteType.TAP_STAR) {
			tapEvaluater.PerfectCallBack(mNote);
		}
		else if (mNote.GetNoteType() == MaimaiNoteType.HOLD_HEAD) {
			holdHeadEvaluater.PerfectCallBack(mNote);
		}
		else if (mNote.GetNoteType() == MaimaiNoteType.HOLD_FOOT) {
			holdFootEvaluater.OkPullCallBack(mNote);
		}
		else if (mNote.GetNoteType() == MaimaiNoteType.SLIDE) {
			MaimaiSlideNote slNote = (MaimaiSlideNote)mNote;
			if (slNote.chain == slNote.pattern.GetLatestJudgeChain ()) {
				slideEvaluater.PerfectCallBack(slNote.chain.GetLastCheckPoint());
			}
		}
		else if (mNote.GetNoteType() == MaimaiNoteType.SLIDE_MOVE_START) {
			base.OnJust(mNote);
		}
		else if (mNote.GetNoteType() == MaimaiNoteType.SLIDE_HEAD) {
			base.OnJust(mNote);
		}
		else if (mNote.GetNoteType() == MaimaiNoteType.BREAK_CIRCLE ||
		         mNote.GetNoteType() == MaimaiNoteType.BREAK_STAR) {
			breakEvaluater.P2600CallBack(mNote);
		}
	}

	protected override void OnMiss (MaimaiNote note) {
		base.OnMiss (note);
		MaimaiNote mNote = (MaimaiNote)note;
		if (mNote.GetNoteType () == MaimaiNoteType.TAP_CIRCLE ||
		    mNote.GetNoteType () == MaimaiNoteType.TAP_STAR) {
			this.tapEvaluater.MissCallBack(mNote);
		}
		else if (mNote.GetNoteType() == MaimaiNoteType.BREAK_CIRCLE ||
		    mNote.GetNoteType() == MaimaiNoteType.BREAK_STAR) {
			this.breakEvaluater.MissCallBack(mNote);
		}
		else if (mNote.GetNoteType() == MaimaiNoteType.SLIDE) {
			//スライドは5つのうち4つ判定できてたらMISSではなくLateGood。.
			MaimaiSlideNote sNote = (MaimaiSlideNote)mNote;
			if (!sNote.pattern.IsEvaluated) {
				if (sNote.pattern.IsLateGoodPassible() && judgementType != ConfigTypes.Judgement.GORI) {
					slideEvaluater.LateGoodCallBack(mNote, true);
				}
				else {
					slideEvaluater.MissCallBack(mNote);
				}
			}
		}
		else if (mNote.GetNoteType() == MaimaiNoteType.HOLD_FOOT) {
			MaimaiHoldFootNote fNote = (MaimaiHoldFootNote)mNote;
			if (judgementType != ConfigTypes.Judgement.GORI) {
				//ホールドフットはミスにならない。.
				if (fNote.relativeNote.pushed) { //ヘッドが押せていればグドる。.
					this.holdFootEvaluater.LateCallBack(mNote);
				}
			}
			else {
				this.holdFootEvaluater.MissCallBack (mNote);
			}
		}
		else if (mNote.GetNoteType() == MaimaiNoteType.HOLD_HEAD) {
			//ホールドヘッドのミスはすぐに非表示にする。（他のノートは消さずに過ぎ去ればそのうち消える(消す処理を入れてる)。）.
			this.holdHeadEvaluater.MissCallBack(mNote);
		}
	}

	protected override void OnGuide (MaimaiNote note) {
		base.OnGuide(note);
		MaimaiNote mNote = (MaimaiNote)note;
		if (mNote.GetNoteType() != MaimaiNoteType.SLIDE) {
			//AnswerSoundがBasisまたはBasis+なら、ANSWERSOUNDを鳴らす。
			if (answerSoundType == ConfigTypes.AnswerSound.BASIS ||
			    answerSoundType == ConfigTypes.AnswerSound.BASIS_PLUS) {
				if (answerSoundType == ConfigTypes.AnswerSound.BASIS_PLUS && mNote.GetNoteType() == MaimaiNoteType.SLIDE_MOVE_START) {
					Utility.SoundEffectManager.Play("se_simai_slide");
				}
				else if (mNote.GetNoteType() == MaimaiNoteType.TAP_CIRCLE ||
				    mNote.GetNoteType() == MaimaiNoteType.HOLD_HEAD ||
				    mNote.GetNoteType() == MaimaiNoteType.TAP_STAR ||
				    mNote.GetNoteType() == MaimaiNoteType.HOLD_FOOT ||
				    mNote.GetNoteType() == MaimaiNoteType.BREAK_CIRCLE ||
				    mNote.GetNoteType() == MaimaiNoteType.BREAK_STAR) {
					Utility.SoundEffectManager.Play("se_answersound");
				}
			}
			//SpecialまたはSpecial+なら、ノートタイプによって音を分ける
			else if (answerSoundType == ConfigTypes.AnswerSound.SPECIAL ||
			         answerSoundType == ConfigTypes.AnswerSound.SPECIAL_PLUS) {
				if (mNote.GetNoteType() == MaimaiNoteType.TAP_CIRCLE ||
				    mNote.GetNoteType() == MaimaiNoteType.HOLD_HEAD ||
				    mNote.GetNoteType() == MaimaiNoteType.TAP_STAR) {
					Utility.SoundEffectManager.Play("se_simai_tap");
				}
				else if (mNote.GetNoteType() == MaimaiNoteType.HOLD_FOOT) {
					Utility.SoundEffectManager.Play("se_simai_holdfoot");
				}
				else if (answerSoundType == ConfigTypes.AnswerSound.SPECIAL_PLUS && mNote.GetNoteType() == MaimaiNoteType.SLIDE_MOVE_START) {
					Utility.SoundEffectManager.Play("se_simai_slide");
				}
				else if (mNote.GetNoteType() == MaimaiNoteType.BREAK_CIRCLE ||
				         mNote.GetNoteType() == MaimaiNoteType.BREAK_STAR) {
					Utility.SoundEffectManager.Play("se_simai_break");
				}
			}
		}
	}

	protected override void OnBeat () {
		//最初の4拍だけカウント音を鳴らす.
		if (beatCnt < 4) {
			if (this.startBeatCountSoundEnabled) {
				Utility.SoundEffectManager.Play("se_startcount");
			}
		}
	}

	#endregion

	#region 評価を初期化する関数群.
	
	// OnJust中で音やエフェクトを出さないフラグ.
	protected bool resetting { get; set; }

	// スライドノートのパターンのハッシュ.
	protected HashSet<MaimaiSlidePattern> slidePatternHash;

	protected override void ResetEvaluatePrepare () {
		tapEvaluater.Reset ();
		holdHeadEvaluater.Reset ();
		slideEvaluater.Reset ();
		breakEvaluater.Reset ();
		ResetCombo();
		ResetPCombo();
		ResetBPCombo();
		if (slidePatternHash == null) {
			slidePatternHash = new HashSet<MaimaiSlidePattern> ();
		}
		slidePatternHash.Clear ();
	}
	
	protected override void OnResetJust(MaimaiNote note, bool justTimeBefore) {
		// 「途中から開始」の時間までのノートの評価を引き継ぎたかったら、評価を覚えておく必要がある.
		// 今作はオートプレーのときのみリピート機能が有効なので、『「途中から開始」の時間までのノートの評価』は全部JustでOK.
		if (justTimeBefore) {
			// 「途中から開始」の時間以前のノートは全部Just.
			resetting = true;
			note.Reset ();
			var sNote = note as MaimaiSlideNote;
			if (sNote == null) {
				OnJust (note);
			}
			else if (slidePatternHash.Add (sNote.pattern)){
				OnJust (note);
			}
			resetting = false;
		}
		else {
			// 「途中から開始」の時間以後のノートは初期化.
			note.Reset ();
			base.OnResetJust (note, justTimeBefore);
		}
	}
	
	#endregion

	#region 評価を表示する関数群.

	private EvaluateObjectBirth effectMother;

	public void ShowEvaluate(DrawableEvaluateTapType evaluate, int buttonId) {
		if (!resetting) {
			effectMother.Birth (evaluate, buttonId, visualizeFastLate);
		}
	}
	public void ShowEvaluate(DrawableEvaluateSlideType evaluate, Vector2 target, float deg) {
		if (!resetting) {
			effectMother.Birth (evaluate, target, deg);
		}
	}
	public void ShowEvaluate(DrawableEvaluateSlideType evaluate, Vector2 target, float deg, float vec, float radius) {
		if (!resetting) {
			effectMother.Birth (evaluate, target, deg, vec, radius);
		}
	}
	public void ShowEvaluate(DrawableEvaluateSlideType evaluate, MaimaiSlidePattern patternInfo) {
		if (!resetting) {
			effectMother.Birth (evaluate, patternInfo);
		}
	}
	public void ShowEvaluate(DrawableEvaluateBreakType evaluate, int buttonId) {
		if (!resetting) {
			effectMother.Birth (evaluate, buttonId, visualizeFastLate);
		}
	}
	public IHoldingEffect ShowHoldHeadEffect(DrawableEvaluateTapType evaluate, int buttonId) { 
		if (!resetting) {
			return effectMother.HoldHeadEffectBirth(evaluate, buttonId);
		}
		return null;
	}

	#endregion

	#region 評価やコンボ用の変数/関数群.
	
	protected MaimaiJudgeEvaluateTap tapEvaluater { get; set; }
	protected MaimaiJudgeEvaluateHoldHead holdHeadEvaluater { get; set; }
	protected MaimaiJudgeEvaluateHoldFoot holdFootEvaluater { get; set; }
	protected MaimaiJudgeEvaluateSlide slideEvaluater { get; set; }
	protected MaimaiJudgeEvaluateBreak breakEvaluater { get; set; }
	
	public int combo { get; set; }
	public int maxCombo { get; set; }
	public int pcombo { get; set; } //パーフェクトのつなぎ.
	public int bpcombo { get; set; } //理論値のつなぎ.
	private int notesCount { get; set; }
	public int achievementScore { get; protected set; } //100.00%スコア.
	public int theoreticalScore { get; protected set; } //理論値.

	public void AddCombo() {
		this.combo++;
		if (this.combo > this.maxCombo)
			this.maxCombo = this.combo;
	}
	public void AddPCombo() { this.pcombo++; }
	public void AddBPCombo() { this.bpcombo++; }
	public void ResetCombo() { this.combo = 0; }
	public void ResetPCombo() { this.pcombo = 0; }
	public void ResetBPCombo() { this.bpcombo = 0; }
	public int GetScore() {
		return GetTapScore() + GetHoldScore() + GetSlideScore() + GetBreakScore();
	}
	public int GetTapScore() {
		return tapEvaluater.GetPerfectScore() + tapEvaluater.GetGreatScore() + tapEvaluater.GetGoodScore();
	}
	public int GetHoldScore() {
		return holdHeadEvaluater.GetPerfectScore() + holdHeadEvaluater.GetGreatScore() + holdHeadEvaluater.GetGoodScore();
	}
	public int GetSlideScore() {
		return slideEvaluater.GetPerfectScore() + slideEvaluater.GetGreatScore() + slideEvaluater.GetGoodScore();
	}
	public int GetBreakScore() {
		return breakEvaluater.GetPerfectScore() + breakEvaluater.GetGreatScore() + breakEvaluater.GetGoodScore();
	}
	public int GetFullTapScore () {
		return tapEvaluater.GetFullScore ();
	}
	public int GetFullHoldScore () {
		return holdHeadEvaluater.GetFullScore ();
	}
	public int GetFullSlideScore () {
		return slideEvaluater.GetFullScore ();
	}
	public int GetFullBreakScore () {
		return breakEvaluater.GetFullScore ();
	}
	public int GetBFullBreakScore () {
		return breakEvaluater.GetBFullScore ();
	}
	public int GetTapCount() {
		return tapEvaluater.GetNoteCount();
	}
	public int GetHoldCount() {
		return holdHeadEvaluater.GetNoteCount();
	}
	public int GetSlideCount() {
		return slideEvaluater.GetNoteCount();
	}
	public int GetBreakCount() {
		return breakEvaluater.GetNoteCount();
	}
	public int GetFullScore() {
		return tapEvaluater.GetFullScore() + holdHeadEvaluater.GetFullScore() + slideEvaluater.GetFullScore() + breakEvaluater.GetFullScore();
	}
	public int GetBFullScore() {
		return tapEvaluater.GetFullScore() + holdHeadEvaluater.GetFullScore() + slideEvaluater.GetFullScore() + breakEvaluater.GetBFullScore();
	}
	public int GetLostScore() {
		return tapEvaluater.GetLostScore() + holdHeadEvaluater.GetLostScore() + slideEvaluater.GetLostScore() + breakEvaluater.GetLostScore();
	}
	public int GetBLostScore() {
		return tapEvaluater.GetLostScore() + holdHeadEvaluater.GetLostScore() + slideEvaluater.GetLostScore() + breakEvaluater.GetBLostScore();
	}
	public int GetPerfectCount() {
		return tapEvaluater.GetPerfectCount() + holdHeadEvaluater.GetPerfectCount() + slideEvaluater.GetPerfectCount() + breakEvaluater.GetPerfectCount();
	}
	public int GetGreatCount() {
		return tapEvaluater.GetGreatCount() + holdHeadEvaluater.GetGreatCount() + slideEvaluater.GetGreatCount() + breakEvaluater.GetGreatCount();
	}
	public int GetGoodCount() {
		return tapEvaluater.GetGoodCount() + holdHeadEvaluater.GetGoodCount() + slideEvaluater.GetGoodCount() + breakEvaluater.GetGoodCount();
	}
	public int GetMissCount() {
		return tapEvaluater.GetMissCount() + holdHeadEvaluater.GetMissCount() + slideEvaluater.GetMissCount() + breakEvaluater.GetMissCount();
	}
	public int GetNotesCountTotal () {
		return GetPerfectCount() + GetGreatCount() + GetGoodCount() + GetMissCount();
	}
	public float GetAchievement() {
		return achievementScore == 0 ? 0 : (float)GetScore() / (float)achievementScore;
	}
	public float GetPaceAchievement() {
		int fs = GetFullScore();
		return fs == 0 ? 0 : (float)GetScore() / (float)fs;
	}
	public float GetHazardBreakPaceAchievement() { // Breakボーナスで増える.
		return achievementScore == 0 ? 1 : (float)
			(achievementScore - (// MAX点から.
			                     GetLostScore ())) / //ロスト点を引いて.
			(float)achievementScore; // MAX点で割る.
	}
	public float GetHazardAchievement() { // 最初からBreak点込み.
		return achievementScore == 0 ? 1 : (float)
			(theoreticalScore - (// MAX点から.
			                     GetBLostScore ())) / //ロスト点を引いて.
			(float)achievementScore; // MAX点で割る.
	}
	public float GetTheoreticalAchievement() { // 理論値を100%としてみる.
		return theoreticalScore == 0 ? 0 : (float)GetScore() / (float)theoreticalScore;
	}
	public float GetTheoreticalPaceAchievement() {
		int fs = GetBFullScore();
		return fs == 0 ? 0 : (float)GetScore() / (float)fs;
	}
	public float GetTheoreticalHazardAchievement() {
		return theoreticalScore == 0 ? 1 : (float)
			(theoreticalScore - (// MAX点から.
			                     GetBLostScore ())) / //ロスト点を引いて.
			(float)theoreticalScore; // MAX点で割る.
	}
	public bool IsAllPerfect() {
		return notesCount == GetPerfectCount();
	}
	public bool IsFullCombo() {
		return notesCount == GetPerfectCount() + GetGreatCount();
	}
	public bool IsNoMiss() {
		return notesCount == GetPerfectCount() + GetGreatCount() + GetGoodCount();
	}
	private void SetAchievementInfo(MaimaiNote[] notes) {
		achievementScore = 0;
		if (notes == null) return;
		var tapcount = tapEvaluater.perfect.count;
		var slidecount = slideEvaluater.perfect.count;
		var breakcount = breakEvaluater.p2500.count;
		var breakcount2 = breakEvaluater.p2600.count;
		var breakcount3 = breakEvaluater.p2550.count;
		var holdcount = holdHeadEvaluater.perfect.count;
		
		tapEvaluater.perfect.count = 0;
		slideEvaluater.perfect.count = 0;
		breakEvaluater.p2500.count = 0;
		breakEvaluater.p2600.count = 0;
		breakEvaluater.p2550.count = 0;
		holdHeadEvaluater.perfect.count = 0;
		HashSet<MaimaiSlidePattern> slideLeaders = new HashSet<MaimaiSlidePattern> ();
		for (var i = 0; i < notes.Length; i++) {
			if (notes[i].GetNoteType() == MaimaiNoteType.TAP_CIRCLE ||
			    notes[i].GetNoteType() == MaimaiNoteType.TAP_STAR)
				tapEvaluater.perfect.count++;
			else if (notes[i].GetNoteType() == MaimaiNoteType.HOLD_HEAD)
				holdHeadEvaluater.perfect.count++;
			else if (notes[i].GetNoteType() == MaimaiNoteType.SLIDE) {
				if (slideLeaders.Add ((notes[i] as MaimaiSlideNote).pattern)) {
					slideEvaluater.perfect.count++;
				}
			}
			else if (notes[i].GetNoteType() == MaimaiNoteType.BREAK_CIRCLE ||
			         notes[i].GetNoteType() == MaimaiNoteType.BREAK_STAR)
				breakEvaluater.p2500.count++;
		}
		slideLeaders.Clear ();
		slideLeaders = null;

		achievementScore = 
			tapEvaluater.GetPerfectScore () + 
			holdHeadEvaluater.GetPerfectScore () +
			slideEvaluater.GetPerfectScore () + 
			breakEvaluater.GetPerfectScore ();
		notesCount = 
			tapEvaluater.GetPerfectCount () + 
			holdHeadEvaluater.GetPerfectCount () +
			slideEvaluater.GetPerfectCount () + 
			breakEvaluater.GetPerfectCount ();
		breakEvaluater.p2600.count = breakEvaluater.p2500.count;
		breakEvaluater.p2500.count = 0;
		theoreticalScore = 
			tapEvaluater.GetPerfectScore () + 
			holdHeadEvaluater.GetPerfectScore () +
			slideEvaluater.GetPerfectScore () + 
			breakEvaluater.GetPerfectScore ();
		
		tapEvaluater.perfect.count = tapcount;
		slideEvaluater.perfect.count = slidecount;
		breakEvaluater.p2500.count = breakcount;
		breakEvaluater.p2600.count = breakcount2;
		breakEvaluater.p2550.count = breakcount3;
		holdHeadEvaluater.perfect.count = holdcount;
	}

	#endregion

	#region 譜面の理論値だけ知りたい.
	private MaimaiJudgeEvaluateManager(MaimaiNote[] notes) : base (null, NotesPartition(notes), 0, 0, 0, START_COUNT_START_TIME) {
		SetupEvaluaterNormal ();
		SetAchievementInfo(notes);
	}
	public static int CalcBFullScore (MaimaiNote[] notes) {
		var work = new MaimaiJudgeEvaluateManager (notes);
		return work.theoreticalScore;
	}
	#endregion

	#region シンク関連.
	public NetworkView syncEvaluater { get; set; } // 外部から設定させる.

	public void SyncEvaluation (MaimaiNote note, SyncEvaluate evaluateType) {
		if (syncEvaluater != null) {
			syncEvaluater.RPC ("SyncEvaluateTypeServerSide", RPCMode.AllBuffered, note.uniqueId, (int)note.GetNoteType(), (int)evaluateType);
		}
	}

	#endregion
}

public interface IHoldingEffect {
	void SetVisible(bool value);
}