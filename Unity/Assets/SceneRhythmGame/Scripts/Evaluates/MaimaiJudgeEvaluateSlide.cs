using UnityEngine;
using System.Collections;

public class MaimaiJudgeEvaluateSlide : MaimaiJudgeEvaluateB {
	//情報はスライド(これ)が持ち、判定するのはスライドパターンの方

	public MaimaiJudgeEvaluateSlide(MaimaiJudgeEvaluateManager manager, MaimaiJudgeEvaluateTap judgeEvaluateStarTap, long backSlideTime,
	                                int perfectRate, int greatRate, int goodRate, 
	                                long inFastGreatTime, long inPerfectTime, long inLateGreatTime, long inLateGoodTime) : base (manager) {
		this.perfect = new MaimaiJudgeEvaluateInfo (perfectRate);
		this.great = new MaimaiJudgeEvaluateInfo (greatRate);
		this.good = new MaimaiJudgeEvaluateInfo (goodRate);
		this.inPerfectTime = Mathf.Abs ((float)inPerfectTime).ToLong ();
		this.inFastGreatTime = Mathf.Abs ((float)inFastGreatTime).ToLong ();
		this.inLateGreatTime = Mathf.Abs ((float)inLateGreatTime).ToLong ();
		this.inLateGoodTime = Mathf.Abs ((float)inLateGoodTime).ToLong ();
		this.inFastGoodTime = judgeEvaluateStarTap.inFastGoodTime;
		this.backSlideTime = backSlideTime;
	}

	public MaimaiJudgeEvaluateInfo perfect { get; protected set; }
	public MaimaiJudgeEvaluateInfo great { get; protected set; }
	public MaimaiJudgeEvaluateInfo good { get; protected set; }
	public long inPerfectTime { get; protected set; }
	public long inFastGreatTime { get; protected set; }
	public long inLateGreatTime { get; protected set; }
	public long inFastGoodTime { get; protected set; }
	public long inLateGoodTime { get; protected set; }
	public virtual long inSlideEvaluateStartTime { get { return inFastGoodTime; } }
	protected long backSlideTime;

	//評価していい時間になっているか。
	public bool IsEvaluateStartTime(long timing, MaimaiSlideNote sNote) {
		//スライドのFastGOOD判定は☆の早GOOD判定から有効 というかスライドのなぞる部分も早GOODから。
		return timing >= (sNote.pattern.relativeNote.GetSlideReadyTime() - sNote.justTime - inSlideEvaluateStartTime);
	}

	public override void Evaluate (long timing, MaimaiNote note) {
		var sNote = (MaimaiSlideNote)note;
		//スライドのFastGOOD判定は☆の早GOOD判定から有効 というかスライドのなぞる部分も早GOODから。.
		if (IsEvaluateStartTime(timing, sNote)) {
			// 触れた時間を記憶.
			sNote.lastCensoredTime = manager.GetTimer().gameTime;
			//もし来たノートが最後のノートなら、.
			if (SlidePatternProgression(sNote)) {
				//判定する.
				if (timing < -inFastGreatTime) {
					FastGoodCallBack(note);
				}
				else if (timing >= -inFastGreatTime && timing < -inPerfectTime)
				{
					FastGreatCallBack(note);
				}
				else if (timing >= -inPerfectTime && timing <= inPerfectTime) {
					PerfectCallBack(note);
				}
				else if (timing > inPerfectTime && timing <= inLateGreatTime) {
					LateGreatCallBack(note);
				}
				else if (timing > inLateGreatTime && timing <= inLateGoodTime) {
					LateGoodCallBack(note, false);
				}
			}
			else {
				//最初のスライドパターンノートであればサウンドエフェクトを鳴らす.
				if (!sNote.pattern.slideSoundCalled && note == sNote.chain.GetSoundCheckPoint()) { 
					manager.PlayNoteSE("se_slide");
					sNote.pattern.slideSoundCalled = true;
				}
			}
		}
	}

	// すべてのチェインのスライド進捗が完了したら評価していい.
	protected bool SlidePatternProgression(MaimaiSlideNote sNote) {
		if (SlideChainProgression (sNote)) {
			foreach (var chain in sNote.pattern.chains) {
				if (!chain.isCompleted) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	/// <summary>
	/// スライドの進捗度を進める. 評価してよければtrueを返す.
	/// </summary>
	protected bool SlideChainProgression(MaimaiSlideNote sNote) {
		MaimaiTimer timer = manager.GetTimer ();

		if (sNote.chain.checkPointProgress >= sNote.chain.checkPoints.Length) return true;
		
		if (sNote.chain.checkPointProgress < 0) {
			if (sNote.chain.checkPoints[0] == sNote) {
				sNote.chain.checkPoints[0].judged = true;
				sNote.chain.checkPointProgress = 1;
				if (sNote.chain.checkPointProgress >= sNote.chain.checkPoints.Length) return true;
				if (sNote.chain.checkPointProgress + 1 >= sNote.chain.checkPoints.Length) return false;
			}
			else if (sNote.chain.checkPoints.Length > 2 && sNote.chain.checkPoints[1] == sNote) {
				sNote.chain.checkPoints[0].judged = true;
				sNote.chain.checkPoints[1].judged = true;
				sNote.chain.checkPointProgress = 2;
				if (sNote.chain.checkPointProgress >= sNote.chain.checkPoints.Length) return true;
			}
			else return false;
		}
		
		if (sNote.chain.checkPoints[sNote.chain.checkPointProgress] == sNote) {
			sNote.chain.checkPoints[sNote.chain.checkPointProgress].judged = true;
			sNote.chain.checkPointProgress++;
		}
		else if (sNote.chain.checkPoints.Length - 1 >= sNote.chain.checkPointProgress + 1 && sNote.chain.checkPoints[sNote.chain.checkPointProgress + 1] == sNote) {
			sNote.chain.checkPoints[sNote.chain.checkPointProgress].judged = true;
			sNote.chain.checkPoints[sNote.chain.checkPointProgress + 1].judged = true;
			sNote.chain.checkPointProgress += 2;
		}
		
		if (sNote.chain.checkPointProgress >= sNote.chain.checkPoints.Length) return true;
		
		while (sNote.chain.checkPointProgress < sNote.chain.checkPoints.Length) {
			if (!sNote.chain.checkPoints[sNote.chain.checkPointProgress].lastCensoredTime.HasValue) {
				return false;
			}
			if (timer.gameTime - sNote.chain.checkPoints[sNote.chain.checkPointProgress].lastCensoredTime.Value < backSlideTime) {
				sNote.chain.checkPoints[sNote.chain.checkPointProgress].judged = true;
				sNote.chain.checkPointProgress++;
			}
			else {
				return false;
			}
		}
		
		if (sNote.chain.checkPointProgress >= sNote.chain.checkPoints.Length) return true;
		return false;
	}

	public virtual void FastGoodCallBack(MaimaiNote note) {
		var sNote = (MaimaiSlideNote)note;
		if (sNote.pattern.chains.Length < 2) {
			this.SendEvaluate (DrawableEvaluateSlideType.FASTGOOD, sNote.chain.GetLastCommand ());
		}
		else {
			this.SendEvaluate (DrawableEvaluateSlideType.FASTGOOD, sNote.pattern);
		}
		this.good.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		SetJudged(sNote);
		sNote.pattern.SetEvaluated(true);
		manager.SyncEvaluation(sNote.GetSyncEvaluateRepresentativeNote(), SyncEvaluate.FASTGOOD);
	}
	public virtual void FastGreatCallBack(MaimaiNote note) {
		var sNote = (MaimaiSlideNote)note;
		if (sNote.pattern.chains.Length < 2) {
			this.SendEvaluate (DrawableEvaluateSlideType.FASTGREAT, sNote.chain.GetLastCommand ());
		}
		else {
			this.SendEvaluate (DrawableEvaluateSlideType.FASTGREAT, sNote.pattern);
		}
		this.great.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		SetJudged(sNote);
		sNote.pattern.SetEvaluated(true);
		manager.SyncEvaluation(sNote.GetSyncEvaluateRepresentativeNote(), SyncEvaluate.FASTGREAT);
	}
	public virtual void PerfectCallBack(MaimaiNote note) {
		var sNote = (MaimaiSlideNote)note;
		if (sNote.pattern.chains.Length < 2) {
			this.SendEvaluate (DrawableEvaluateSlideType.JUST, sNote.chain.GetLastCommand ());
		}
		else {
			this.SendEvaluate (DrawableEvaluateSlideType.JUST, sNote.pattern);
		}
		this.perfect.count++;
		manager.AddCombo();
		manager.AddPCombo();
		manager.AddBPCombo();
		SetJudged(sNote);
		sNote.pattern.SetEvaluated(true);
		manager.SyncEvaluation(sNote.GetSyncEvaluateRepresentativeNote(), SyncEvaluate.PERFECT);
	}
	public virtual void LateGreatCallBack(MaimaiNote note) {
		var sNote = (MaimaiSlideNote)note;
		if (sNote.pattern.chains.Length < 2) {
			this.SendEvaluate (DrawableEvaluateSlideType.LATEGREAT, sNote.chain.GetLastCommand ());
		}
		else {
			this.SendEvaluate (DrawableEvaluateSlideType.LATEGREAT, sNote.pattern);
		}
		this.great.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		SetJudged(sNote);
		sNote.pattern.SetEvaluated(true);
		manager.SyncEvaluation(sNote.GetSyncEvaluateRepresentativeNote(), SyncEvaluate.LATEGREAT);
	}
	public virtual void LateGoodCallBack(MaimaiNote note, bool missCall) {
		MaimaiSlideNote sNote = (MaimaiSlideNote)note;
		if (sNote.pattern.chains.Length < 2) {
			this.SendEvaluate (DrawableEvaluateSlideType.LATEGOOD, sNote.chain.GetLastCommand ());
		}
		else {
			this.SendEvaluate (DrawableEvaluateSlideType.LATEGOOD, sNote.pattern);
		}
		this.good.count++; 
		manager.AddCombo(); 
		manager.ResetPCombo();
		manager.ResetBPCombo();
		SetJudged(sNote);
		sNote.pattern.SetEvaluated(true);
		if (missCall) manager.SyncEvaluation(note, SyncEvaluate.MISS);
		else manager.SyncEvaluation(sNote.GetSyncEvaluateRepresentativeNote(), SyncEvaluate.LATEGOOD);
	}

	public override void MissCallBack(MaimaiNote note) {
		MaimaiSlideNote sNote = (MaimaiSlideNote)note;
		if (sNote.pattern.chains.Length < 2) {
			this.SendEvaluate (DrawableEvaluateSlideType.TOOLATE, sNote.chain.GetLastCommand ());
		}
		else {
			this.SendEvaluate (DrawableEvaluateSlideType.TOOLATE, sNote.pattern);
		}
		this.miss.count++; 
		manager.ResetCombo(); 
		manager.ResetPCombo(); 
		manager.ResetBPCombo();
		SetJudged(sNote);
		sNote.pattern.SetEvaluated(true);
		manager.SyncEvaluation(sNote.GetSyncEvaluateRepresentativeNote(), SyncEvaluate.MISS);
	}

	public void SendEvaluate(DrawableEvaluateSlideType evaluate, MaimaiSlideCommand lastCommand) {
		if (lastCommand.GetCommandType () == MaimaiSlideCommandType.STRAIGHT) {
			manager.ShowEvaluate (evaluate, lastCommand.GetTargetPosition (), lastCommand.GetImageDegree ());
		}
		else {
			var curveCommand = lastCommand as MaimaiSlideCurveCommand;
			manager.ShowEvaluate (evaluate, curveCommand.centerPos, curveCommand.GetImageDegree (), curveCommand.vec, Mathf.Max (curveCommand.radius.x, curveCommand.radius.y));
		}
	}

	// チェイン用.
	public void SendEvaluate(DrawableEvaluateSlideType evaluate, MaimaiSlidePattern patternInfo) {
		manager.ShowEvaluate (evaluate, patternInfo);
	}

	protected void SetJudged (MaimaiSlideNote sNote) {
		sNote.judged = true;
		sNote.visible = false;
		foreach (var chain in sNote.pattern.chains) {
			foreach (var checkPoint in chain.checkPoints) {
				checkPoint.judged = true;
				checkPoint.visible = false;
			}
		}
	}

	public override int GetPerfectCount() { return this.perfect.count; }
	public override int GetPerfectScore() { return this.perfect.score; }
	public override int GetGreatCount() { return this.great.count; }
	public override int GetGreatScore() { return this.great.score; }
	public override int GetGoodCount() { return this.good.count; }
	public override int GetGoodScore() { return this.good.score; }
	public override int GetNoteCount() {
		return GetPerfectCount() + GetGreatCount() + GetGoodCount() + GetMissCount();
	}
	
	public int GetFullScore() {
		return (this.GetPerfectCount() + this.GetGreatCount() + this.GetGoodCount() + this.GetMissCount()) * this.perfect.rate;
	}
	
	public int GetLostScore() {
		return
			(this.perfect.rate - this.perfect.rate) * this.GetPerfectCount() + 
			(this.perfect.rate - this.great.rate) * this.GetGreatCount() + 
			(this.perfect.rate - this.good.rate) * this.GetGoodCount() + 
			(this.perfect.rate - this.miss.rate) * this.GetMissCount();
		
	}
	
	public override void Reset () {
		this.perfect.count = 0;
		this.great.count = 0;
		this.good.count = 0;
		base.Reset ();
	}
}
