using UnityEngine;
using System.Collections;

public class MaimaiJudgeEvaluateTap : MaimaiJudgeEvaluateB {
	public MaimaiJudgeEvaluateTap(MaimaiJudgeEvaluateManager manager,
	int perfectRate, int greatRate, int goodRate, 
	long inFastGoodTime, long inFastGreatTime, long inPerfectTime, long inLateGreatTime, long inLateGoodTime)
	: base (manager) {
		this.perfect = new MaimaiJudgeEvaluateInfo (perfectRate);
		this.great = new MaimaiJudgeEvaluateInfo (greatRate);
		this.good = new MaimaiJudgeEvaluateInfo (goodRate);
		this.inPerfectTime = Mathf.Abs ((float)inPerfectTime).ToLong();
		this.inFastGreatTime = Mathf.Abs ((float)inFastGreatTime).ToLong();
		this.inLateGreatTime = Mathf.Abs ((float)inLateGreatTime).ToLong();
		this.inFastGoodTime = Mathf.Abs ((float)inFastGoodTime).ToLong();
		this.inLateGoodTime = Mathf.Abs ((float)inLateGoodTime).ToLong();
	}

	public MaimaiJudgeEvaluateInfo perfect { get; protected set; }
	public MaimaiJudgeEvaluateInfo great { get; protected set; }
	public MaimaiJudgeEvaluateInfo good { get; protected set; }
	public long inPerfectTime { get; protected set; }
	public long inFastGreatTime { get; protected set; }
	public long inLateGreatTime { get; protected set; }
	public long inFastGoodTime { get; protected set; }
	public long inLateGoodTime { get; protected set; }

	public override void Evaluate (long timing, MaimaiNote note) {
		if (timing >= -this.inFastGoodTime && timing < -this.inFastGreatTime) {
			this.FastGoodCallBack(note);
		}
		else if (timing >= -this.inFastGreatTime && timing < -this.inPerfectTime) {
			this.FastGreatCallBack(note);
		}
		else if (timing >= -this.inPerfectTime && timing <= this.inPerfectTime) {
			this.PerfectCallBack(note);
		}
		else if (timing > this.inPerfectTime && timing <= this.inLateGreatTime) {
			this.LateGreatCallBack(note);
		}
		else if (timing > this.inLateGreatTime && timing <= this.inLateGoodTime) {
			this.LateGoodCallBack(note);
		}
	}

	public virtual void FastGoodCallBack(MaimaiNote note) {
		manager.PlayNoteSE("se_tap_good");
		MaimaiTapNote tNote = (MaimaiTapNote)note;
		manager.ShowEvaluate(DrawableEvaluateTapType.FASTGOOD, tNote.GetButtonId());
		this.good.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		tNote.judged = true;
		tNote.visible = false;
		manager.SyncEvaluation(note, SyncEvaluate.FASTGOOD);
		manager.SetDrawableCommentNote(note);
	}
	public virtual void FastGreatCallBack(MaimaiNote note) {
		manager.PlayNoteSE("se_tap_great");
		MaimaiTapNote tNote = (MaimaiTapNote)note;
		manager.ShowEvaluate(DrawableEvaluateTapType.FASTGREAT, tNote.GetButtonId());
		this.great.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		tNote.judged = true;
		tNote.visible = false;
		manager.SyncEvaluation(note, SyncEvaluate.FASTGREAT);
		manager.SetDrawableCommentNote(note);
	}
	public virtual void PerfectCallBack(MaimaiNote note) {
		manager.PlayNoteSE("se_tap_perfect");
		MaimaiTapNote tNote = (MaimaiTapNote)note;
		manager.ShowEvaluate(DrawableEvaluateTapType.PERFECT, tNote.GetButtonId());
		this.perfect.count++;
		manager.AddCombo();
		manager.AddPCombo();
		manager.AddBPCombo();
		tNote.judged = true;
		tNote.visible = false;
		manager.SyncEvaluation(note, SyncEvaluate.PERFECT);
		manager.SetDrawableCommentNote(note);
	}
	public virtual void LateGreatCallBack(MaimaiNote note) {
		manager.PlayNoteSE("se_tap_great");
		MaimaiTapNote tNote = (MaimaiTapNote)note;
		manager.ShowEvaluate(DrawableEvaluateTapType.LATEGREAT, tNote.GetButtonId());
		this.great.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		tNote.judged = true;
		tNote.visible = false;
		manager.SyncEvaluation(note, SyncEvaluate.LATEGREAT);
		manager.SetDrawableCommentNote(note);
	}
	public virtual void LateGoodCallBack(MaimaiNote note) {
		manager.PlayNoteSE("se_tap_good");
		MaimaiTapNote tNote = (MaimaiTapNote)note;
		manager.ShowEvaluate(DrawableEvaluateTapType.LATEGOOD, tNote.GetButtonId());
		this.good.count++;
		manager.AddCombo();
		manager.ResetPCombo();
		manager.ResetBPCombo();
		tNote.judged = true;
		tNote.visible = false;
		manager.SyncEvaluation(note, SyncEvaluate.LATEGOOD);
		manager.SetDrawableCommentNote(note);
	}
	public override void MissCallBack(MaimaiNote note) {
		base.MissCallBack (note);
		MaimaiTapNote tNote = (MaimaiTapNote)note;
		tNote.visible = false;
		manager.SyncEvaluation(note, SyncEvaluate.MISS);
	}

	public override int GetPerfectCount() { return this.perfect.count; }
	public override int GetPerfectScore() { return this.perfect.score; }
	public override int GetGreatCount() { return this.great.count; }
	public override int GetGreatScore() { return this.great.score; }
	public override int GetGoodCount() { return this.good.count; }
	public override int GetGoodScore() { return this.good.score; }
	public override int GetNoteCount() {
		return GetPerfectCount() + GetGreatCount() + GetGoodCount() + GetMissCount();
	}
	
	public int GetFullScore() {
		return (this.GetPerfectCount() + this.GetGreatCount() + this.GetGoodCount() + this.GetMissCount()) * this.perfect.rate;
	}
	
	public int GetLostScore() {
		return
			(this.perfect.rate - this.perfect.rate) * this.GetPerfectCount() + 
			(this.perfect.rate - this.great.rate) * this.GetGreatCount() + 
			(this.perfect.rate - this.good.rate) * this.GetGoodCount() + 
			(this.perfect.rate - this.miss.rate) * this.GetMissCount();
	}
	
	public override void Reset () {
		this.perfect.count = 0;
		this.great.count = 0;
		this.good.count = 0;
		base.Reset ();
	}

}
