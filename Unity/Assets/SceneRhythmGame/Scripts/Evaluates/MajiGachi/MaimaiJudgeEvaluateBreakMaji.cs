﻿using UnityEngine;
using System.Collections;

public class MaimaiJudgeEvaluateBreakMaji : MaimaiJudgeEvaluateBreak {
	public MaimaiJudgeEvaluateBreakMaji(MaimaiJudgeEvaluateManager manager, 
	                                int p2600Rate, int p2550Rate, int p2500Rate, int p2000Rate, int p1500Rate, int p1250Rate, int p1000Rate,
	                                long inMissFastTime, long inP1000FastTime, long inP1250FastTime, long inP1500FastTime, long inP2000FastTime, long inP2500FastTime, long inP2550FastTime, long inP2600Time,
	                                long inP2550LateTime, long inP2500LateTime, long inP2000LateTime, long inP1500LateTime, long inP1250LateTime, long inP1000LateTime, long inMissLateTime)
	: base(manager, p2600Rate, p2550Rate, p2500Rate, p2000Rate, p1500Rate, p1250Rate, p1000Rate,
		      inP1000FastTime, inP1250FastTime, inP1500FastTime, inP2000FastTime, inP2500FastTime, inP2550FastTime, inP2600Time,
		      inP2550LateTime, inP2500LateTime, inP2000LateTime, inP1500LateTime, inP1250LateTime, inP1000LateTime) {
		inFastMissTime = inMissFastTime;
		inLateMissTime = inMissLateTime;
	}

	public long inFastMissTime { get; protected set; }
	public long inLateMissTime { get; protected set; }

	public override void Evaluate (long timing, MaimaiNote note) {
		if (timing >= -this.inFastMissTime && timing < -this.inP1000FastTime) {
			this.MissCallBack(note);
		}
		else if (timing >= -this.inP1000FastTime && timing < -this.inP1250FastTime) {
			this.P1000FastCallBack(note);
		}
		else if (timing >= -this.inP1250FastTime && timing < -this.inP1500FastTime) {
			this.P1250FastCallBack(note);
		}
		else if (timing >= -this.inP1500FastTime && timing < -this.inP2000FastTime) {
			this.P1500FastCallBack(note);
		}
		else if (timing >= -this.inP2000FastTime && timing < -this.inP2500FastTime) {
			this.P2000FastCallBack(note);
		}
		else if (timing >= -this.inP2500FastTime && timing < -this.inP2550FastTime) {
			this.P2500FastCallBack(note);
		}
		else if (timing >= -this.inP2550FastTime && timing < -this.inP2600Time) {
			this.P2550FastCallBack(note);
		}
		else if (timing >= -this.inP2600Time && timing <= this.inP2600Time) {
			this.P2600CallBack(note);
		}
		else if (timing > this.inP2600Time && timing <= this.inP2550LateTime) {
			this.P2550LateCallBack(note);
		}
		else if (timing > this.inP2550LateTime && timing <= this.inP2500LateTime) {
			this.P2500LateCallBack(note);
		}
		else if (timing > this.inP2500LateTime && timing <= this.inP2000LateTime) {
			this.P2000LateCallBack(note);
		}
		else if (timing > this.inP2000LateTime && timing <= this.inP1500LateTime) {
			this.P1500LateCallBack(note);
		}
		else if (timing > this.inP1500LateTime && timing <= this.inP1250LateTime) {
			this.P1250LateCallBack(note);
		}
		else if (timing > this.inP1250LateTime && timing <= this.inP1000LateTime) {
			this.P1000LateCallBack(note);
		}
		else if (timing > this.inP1000LateTime && timing <= this.inLateMissTime) {
			this.MissCallBack(note);
		}
	}
}
