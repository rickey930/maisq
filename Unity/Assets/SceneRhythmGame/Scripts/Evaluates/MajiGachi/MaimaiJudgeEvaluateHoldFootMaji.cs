﻿using UnityEngine;
using System.Collections;

public class MaimaiJudgeEvaluateHoldFootMaji : MaimaiJudgeEvaluateHoldFoot {
	public MaimaiJudgeEvaluateHoldFootMaji(MaimaiJudgeEvaluateManager manager, MaimaiJudgeEvaluateHoldHead headEvaluater, long inIntrustHeadEvaluateTime, long inMajiHoldOverMissTime)
	: base (manager, headEvaluater, inIntrustHeadEvaluateTime) {
		this.inMajiHoldOverMissTime = inMajiHoldOverMissTime;
	}

	public long inMajiHoldOverMissTime { get; protected set; }

	public override void Evaluate (long timing, MaimaiNote note) {
		MaimaiHoldFootNote fNote = (MaimaiHoldFootNote)note;
		if (fNote.relativeNote.pushed) {
			//離すのが早い.
			if (timing < -this.inIntrustHeadEvaluateTime) {
				this.EarlyPullCallBack(note);
			}
			//離したけど遅い.
			else if (timing > this.inMajiHoldOverMissTime) {
				this.LateCallBack(note);
			}
			//タイミングばっちり.
			else {
				this.OkPullCallBack(note);
			}
			//離さなかったのはミス判定に任せる.
		}
	}
}
