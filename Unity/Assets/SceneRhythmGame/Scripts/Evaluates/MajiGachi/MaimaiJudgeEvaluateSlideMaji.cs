using UnityEngine;
using System.Collections;

public class MaimaiJudgeEvaluateSlideMaji : MaimaiJudgeEvaluateSlide {
	public MaimaiJudgeEvaluateSlideMaji(MaimaiJudgeEvaluateManager manager, MaimaiJudgeEvaluateTap judgeEvaluateStarTap, long backSlideTime,
	                                int perfectRate, int greatRate, int goodRate, 
	                                long inFastGoodTime, long inFastGreatTime, long inPerfectTime, long inLateGreatTime, long inLateGoodTime, long inLateMissTime)
	: base (manager, judgeEvaluateStarTap, backSlideTime, perfectRate, greatRate, goodRate, inFastGreatTime, inPerfectTime, inLateGreatTime, inLateGoodTime) {
		this.inFastMissTime = judgeEvaluateStarTap.inFastGoodTime;
		this.inFastGoodTime = inFastGoodTime;
		this.inLateMissTime = inLateMissTime;
	}
	
	public long inFastMissTime { get; protected set; }
	public long inLateMissTime { get; protected set; }
	public override long inSlideEvaluateStartTime { get { return inFastMissTime; } }

	public virtual void TooFastCallBack(MaimaiNote note) {
		var sNote = (MaimaiSlideNote)note;
		if (sNote.pattern.chains.Length < 2) {
			this.SendEvaluate (DrawableEvaluateSlideType.TOOFAST, sNote.chain.GetLastCommand ());
		}
		else {
			this.SendEvaluate (DrawableEvaluateSlideType.TOOFAST, sNote.pattern);
		}
		this.miss.count++; 
		manager.ResetCombo(); 
		manager.ResetPCombo(); 
		manager.ResetBPCombo();
		SetJudged(sNote);
		sNote.pattern.SetEvaluated(true);
		manager.SyncEvaluation(sNote.GetSyncEvaluateRepresentativeNote(), SyncEvaluate.MISS);
	}

}
