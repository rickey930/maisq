﻿using UnityEngine;
using System.Collections;

public class MaimaiJudgeEvaluateTapMaji : MaimaiJudgeEvaluateTap {
	public MaimaiJudgeEvaluateTapMaji(MaimaiJudgeEvaluateManager manager,
	                                  int perfectRate, int greatRate, int goodRate, 
	                                  long inFastMissTime, long inFastGoodTime, long inFastGreatTime, long inPerfectTime, long inLateGreatTime, long inLateGoodTime, long inLateMissTime)
	: base (manager, perfectRate, greatRate, goodRate, inFastGoodTime, inFastGreatTime, inPerfectTime, inLateGreatTime, inLateGoodTime) {
		this.inFastMissTime = inFastMissTime;
		this.inLateMissTime = inLateMissTime;
	}

	public long inFastMissTime { get; protected set; }
	public long inLateMissTime { get; protected set; }

	public override void Evaluate (long timing, MaimaiNote note) {
		if (timing >= -this.inFastMissTime && timing < -this.inFastGoodTime) {
			this.MissCallBack(note);
		}
		else if (timing >= -this.inFastGoodTime && timing < -this.inFastGreatTime) {
			this.FastGoodCallBack(note);
		}
		else if (timing >= -this.inFastGreatTime && timing < -this.inPerfectTime) {
			this.FastGreatCallBack(note);
		}
		else if (timing >= -this.inPerfectTime && timing <= this.inPerfectTime) {
			this.PerfectCallBack(note);
		}
		else if (timing > this.inPerfectTime && timing <= this.inLateGreatTime) {
			this.LateGreatCallBack(note);
		}
		else if (timing > this.inLateGreatTime && timing <= this.inLateGoodTime) {
			this.LateGoodCallBack(note);
		}
		else if (timing > this.inLateGoodTime && timing <= this.inLateMissTime) {
			this.MissCallBack(note);
		}
	}
}
