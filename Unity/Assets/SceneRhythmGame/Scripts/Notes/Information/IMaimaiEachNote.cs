﻿using UnityEngine;
using System.Collections.Generic;

public interface IMaimaiEachNote {
	void AddEachNote(IMaimaiEachNote note);
	HashSet<IMaimaiEachNote> GetEachNotes();
}
