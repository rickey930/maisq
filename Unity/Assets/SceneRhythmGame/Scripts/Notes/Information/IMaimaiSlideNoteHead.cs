using UnityEngine;
using System.Collections;

public interface IMaimaiSlideNoteHead {
	long GetSlideReadyTime();
	void SetRelativeNote(MaimaiSlidePattern note);
	MaimaiSlidePattern[] GetRelativeNote();
	float GetImageDegree();
}
