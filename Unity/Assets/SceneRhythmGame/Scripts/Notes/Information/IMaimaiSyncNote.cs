﻿using UnityEngine;
using System.Collections;

public interface IMaimaiSyncNote { //未使用.
	SyncEvaluate GetSyncEvaluate();
	void SetSyncEvaluate(SyncEvaluate evaluate);
	MaimaiNote GetSyncNote();
	void SetSyncNote(MaimaiNote note);
	bool GetSyncEvaluated();
	void SetSyncEvaluated(bool value);
	void CheckSync();
}
public enum SyncEvaluate {
	NONE, MISS, FASTGOOD, FASTGREAT, PERFECT, LATEGREAT, LATEGOOD,
}
