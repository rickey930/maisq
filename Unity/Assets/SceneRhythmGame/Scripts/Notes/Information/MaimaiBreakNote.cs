using UnityEngine;
using System.Collections;

public class MaimaiBreakNote : MaimaiTapNote {
	public MaimaiBreakNote (string uniqueId, long justTime, long missTime, int actionId, int buttonId) : base (uniqueId, justTime, missTime, actionId, buttonId) {
		
	}
	
	public override MaimaiNoteType GetNoteType() {
		return MaimaiNoteType.BREAK_CIRCLE;
	}
}
