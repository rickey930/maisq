using UnityEngine;
using System.Collections;

public class MaimaiHoldFootNote : MaimaiNote {
	public MaimaiHoldFootNote(string uniqueId, long justTime, long missTime, int actionId) : base (uniqueId, justTime, missTime, actionId) {
	}

	public override MaimaiNoteType GetNoteType() {
		return MaimaiNoteType.HOLD_FOOT;
	}

	public MaimaiHoldHeadNote relativeNote { get; set; }
	public void SetRelativeNote(MaimaiHoldHeadNote headNote) {
		if (relativeNote == null) {
			relativeNote = headNote;
			relativeNote.SetRelativeNote (this);
		}
	}

	public override void Release() {
		base.Release();
		relativeNote = null;
	}
}
