using UnityEngine;
using System.Collections;

public class MaimaiHoldHeadNote : MaimaiTapNote {
	public MaimaiHoldHeadNote (string uniqueId, long justTime, long missTime, int actionId, int buttonId) : base (uniqueId, justTime, missTime, actionId, buttonId) {
		evaluate = DrawableEvaluateTapType.NONE;
	}
	
	public override MaimaiNoteType GetNoteType() {
		return MaimaiNoteType.HOLD_HEAD;
	}

	public MaimaiHoldFootNote relativeNote { get; set; }
	public void SetRelativeNote(MaimaiHoldFootNote footNote) {
		if (relativeNote == null) {
			relativeNote = footNote;
			relativeNote.SetRelativeNote (this);
		}
	}
	
	//評価用.
	//ヘッドでの評価を記憶する（点数に反映されるのはフットのとき）.
	public DrawableEvaluateTapType evaluate { get; set; }
	//ボタンを押したフラグ.
	public bool pushed { get; set; }
	//ボタンを押してから、離すか押しっぱなしミスになるまで表示されるエフェクトのインスタンス.
	public IHoldingEffect effectInstance { get; set; }
	
	public override void Reset () {
		base.Reset ();
		this.pushed = false;
		if (this.effectInstance != null) {
			this.effectInstance.SetVisible (false);
		}
		this.effectInstance = null;
		this.evaluate = DrawableEvaluateTapType.NONE;
	}

	public override void Release() {
		base.Release();
		relativeNote = null;
	}
}
