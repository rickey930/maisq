﻿using UnityEngine;
using System.Collections;

public class MaimaiImportedSoundNote : MaimaiNote {
	public MaimaiImportedSoundNote(string uniqueId, long justTime,
	                               string soundId)
	: base (uniqueId, justTime, 0, Constants.instance.MAIMAI_NOTE_OTHER_ACTION_ID) {
		this.soundId = soundId;
	}
	
	public string soundId { get; set; }
	
	public override MaimaiNoteType GetNoteType() {
		return MaimaiNoteType.MESSAGE_SOUND;
	}
}
