﻿using UnityEngine;
using System.Collections;

public class MaimaiMessageNote : MaimaiNote {
	public MaimaiMessageNote(string uniqueId, long justTime,
	                         string message, Vector2 position, float scale, Color color, long visibleTime)
	: base (uniqueId, justTime, 0, Constants.instance.MAIMAI_NOTE_OTHER_ACTION_ID) {
		this.message = message;
		this.position = new Vector2(position.x * Constants.instance.MAIMAI_OUTER_RADIUS, position.y * Constants.instance.MAIMAI_OUTER_RADIUS);
		this.scale = scale;
		this.color = color;
		this.visibleTime = visibleTime;
	}

	public string message { get; set; }
	public Vector2 position { get; set; }
	public float scale { get; set; }
	public Color color { get; set; }
	public long visibleTime { get; set; }

	public override MaimaiNoteType GetNoteType() {
		return MaimaiNoteType.MESSAGE;
	}

}
