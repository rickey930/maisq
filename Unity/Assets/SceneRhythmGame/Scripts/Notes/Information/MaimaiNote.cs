﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RhythmGameLibrary;

public abstract class MaimaiNote : RhythmGameNote {
	public MaimaiNote(string uniqueId, long justTime, long missTime, int actionId) : base (justTime, missTime) {
		this.uniqueId = uniqueId;
		this.actionId = actionId;
		this.visible = true;
		this.alphaPercent = 0.0f;
		this.scalePercent = 0.0f;
	}
	public string uniqueId { get; set; }
	public int actionId { get; set; }
	public bool visible { get; set; }
	public float alphaPercent { get; set; }
	public float scalePercent { get; set; }
	
	/// <summary>
	/// 非表示ノートである.
	/// </summary>
	public bool secret { get; set; }

	public abstract MaimaiNoteType GetNoteType ();

	public virtual void Release() {

	}
	
	public virtual void Reset () {
		this.visible = true;
	}

	/// <summary>
	/// ノートオブジェクト生成に対応するノートタイプ.
	/// </summary>
	public bool IsVisualNoteType() {
		switch (GetNoteType ()) {
		case MaimaiNoteType.TAP_CIRCLE:
		case MaimaiNoteType.TAP_STAR:
		case MaimaiNoteType.SLIDE_HEAD:
		case MaimaiNoteType.HOLD_HEAD:
		case MaimaiNoteType.BREAK_CIRCLE:
		case MaimaiNoteType.BREAK_STAR:
		case MaimaiNoteType.MESSAGE:
		case MaimaiNoteType.MESSAGE_SCROLL:
		case MaimaiNoteType.MESSAGE_SOUND:
			return true;
		}
		return false;
	}

	/// <summary>
	/// イーチに対応するノートタイプ.
	/// </summary>
	public bool IsEachableNoteType() {
		switch (GetNoteType ()) {
		case MaimaiNoteType.TAP_CIRCLE:
		case MaimaiNoteType.TAP_STAR:
		case MaimaiNoteType.HOLD_HEAD:
		case MaimaiNoteType.BREAK_CIRCLE:
		case MaimaiNoteType.BREAK_STAR:
			return true;
		}
		return false;
	}

	/// <summary>
	/// シンク評価に対応するノートタイプ.
	/// </summary>
	public bool IsSyncableNoteType() {
		switch (GetNoteType ()) {
		case MaimaiNoteType.TAP_CIRCLE:
		case MaimaiNoteType.TAP_STAR:
		case MaimaiNoteType.SLIDE:
		case MaimaiNoteType.HOLD_FOOT:
		case MaimaiNoteType.BREAK_CIRCLE:
		case MaimaiNoteType.BREAK_STAR:
			return true;
		}
		return false;
	}
}

public enum MaimaiNoteType {
	TAP_CIRCLE, HOLD_HEAD, HOLD_FOOT, TAP_STAR, BREAK_STAR, SLIDE_HEAD, BREAK_CIRCLE, SLIDE, SLIDE_MOVE_START, MESSAGE, MESSAGE_SCROLL, MESSAGE_SOUND, 
}
