﻿using UnityEngine;
using System.Collections;

public abstract class MaimaiNoteB : MaimaiNote {
	public MaimaiNoteB(string uniqueId, long justTime, long missTime, int actionId, int buttonId) : base (uniqueId, justTime, missTime, actionId) {
		this.buttonId = buttonId;
	}
	public int buttonId { get; set; }
	
	public virtual int GetButtonId() {
		return buttonId;
	}

	public float GetImageDegree() {
		return 45.0f * GetButtonId () + 22.5f;
	}
}
