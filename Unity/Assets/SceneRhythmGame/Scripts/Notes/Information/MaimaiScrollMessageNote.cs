﻿using UnityEngine;
using System.Collections;

public class MaimaiScrollMessageNote : MaimaiNote {
	public MaimaiScrollMessageNote(string uniqueId, long justTime,
	                               string message, float y, float scale, Color color, long visibleTime)
	: base (uniqueId, justTime, 0, Constants.instance.MAIMAI_NOTE_OTHER_ACTION_ID) {
		this.message = message;
		this.y = y * Constants.instance.MAIMAI_OUTER_RADIUS;
		this.scale = scale;
		this.color = color;
		this.visibleTime = visibleTime;
	}
	
	public string message { get; set; }
	public float y { get; set; }
	public float scale { get; set; }
	public Color color { get; set; }
	public long visibleTime { get; set; }
	
	public override MaimaiNoteType GetNoteType() {
		return MaimaiNoteType.MESSAGE_SCROLL;
	}
}
