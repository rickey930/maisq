﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MaimaiSlideChain {
	public MaimaiSlideChain (long waitTime, // スターノートをタップしてから、☆の移動開始までの待ち時間(つまり1拍の時間).)
							 long justTime, 
	                         MaimaiSlideCommand[] commands,
	                         MaimaiSlideNote[] checkPoints) {
		onStarMoveStartWaitTimeRange = waitTime;
		onStarMoveEndTime = justTime;
		this.commands = commands;
		this.checkPoints = checkPoints;
		checkPointProgress = -1;
	}

	public void Setup (MaimaiSlidePattern pattern,
	                   long slideReadyTime // ☆が移動する1拍前の時間. 基本的にスターノートのjustTimeを入れればよい.
	                   ) {
		this.pattern = pattern;
		foreach (var checkPoint in checkPoints) {
			checkPoint.Setup (pattern, this);
		}
		onStarMoveStartTime = slideReadyTime + onStarMoveStartWaitTimeRange; //pattern.relativeNote.GetSlideReadyTime()
		SetCheckPointsJustTime ();
	}

	public MaimaiSlidePattern pattern { get; set; }
	public MaimaiSlideCommand[] commands { get; set; }
	public MaimaiSlideNote[] checkPoints { get; set; }
	public int checkPointProgress { get; set; } //進捗度インデックス.
	
	public MaimaiSlideCommand GetFirstCommand () { 
		return commands[0];
	}
	public MaimaiSlideCommand GetLastCommand () {
		return commands[commands.Length - 1];
	}
	public MaimaiSlideNote GetSoundCheckPoint () { 
		if (checkPoints.Length == 0)
			return null;
		if (checkPoints.Length == 1)
			return checkPoints[0];
		return checkPoints [1];
	}
	public MaimaiSlideNote GetLastCheckPoint () {
		return checkPoints[checkPoints.Length - 1];
	}

	/// <summary>
	/// ☆が始点に完全に表示されてから(TAP_STARのJustTimeから)☆の移動が始まるまでの待ち時間.
	/// </summary>
	public long onStarMoveStartWaitTimeRange { get; set; }
	/// <summary>
	/// ☆の移動が始まる時間.
	/// </summary>
	public long onStarMoveStartTime { get; set; }
	/// <summary>
	/// ☆の移動が終わる時間.
	/// </summary>
	public long onStarMoveEndTime { get; set; }
	/// <summary>
	/// ☆の移動に要する時間.
	/// </summary>
	public long onStarMoveTimeRange { get { return onStarMoveEndTime - onStarMoveStartTime; } }
	/// <summary>
	/// 調整後のJustTime.
	/// </summary>
	public long justTime { get; set; }

	/// <summary>
	/// スライドコマンドの合計距離.
	/// </summary>
	public float GetTotalDistance() {
		if (!_totalDistance.HasValue) {
			float ret = 0.0f;
			foreach (var command in commands) {
				ret += command.distance;
			}
			_totalDistance = ret;
		}
		return _totalDistance.Value;
	}
	private float? _totalDistance;
	/// <summary>
	/// ☆の移動スピード.
	/// </summary>
	public float onStarMoveSpeed { get { return GetTotalDistance () / (float)onStarMoveTimeRange; } }

	/// <summary>
	/// スライド操作をひとつ以上操作した上で、まだあとひとつ残っている.
	/// </summary>
	public bool isLeft {
		get {
			if (checkPoints.Length > 1) {
				return checkPointProgress == checkPoints.Length - 1;
			}
			return false; //始点と終点の間のセンサーは無い場合はTooLateにする
		}
	}

	/// <summary>
	/// スライド操作が全て終わった.
	/// </summary>
	public bool isCompleted {
		get {
			return checkPointProgress == checkPoints.Length;
		}
	}

	/// <summary>
	/// スライド進捗度.
	/// </summary>
	public float progressRate {
		get {
			if (checkPoints.Length > 0) 
				return (float)checkPointProgress / (float)checkPoints.Length;
			return 0;
		}
	}

	public void SetCheckPointsJustTime () {
		// 最後のスライドコマンドの始点と終点を取得する.
		Vector2 lastCommandStartPos = this.GetLastCommand().GetStartPosition ();
		Vector2 lastCommandTargetPos = this.GetLastCommand().GetTargetPosition ();
		// 最後のスライドチェックポイントのActionIDからセンサーを見つけてセンサーの位置を取得する.
		Constants.SensorId lastSensor = this.GetLastCheckPoint().sensorId;
		Vector2 lastSensorPos;
		switch (lastSensor) {
		case Constants.SensorId.OUTER_1:
			lastSensorPos = Constants.instance.GetOuterPieceAxis (0);
			break;
		case Constants.SensorId.OUTER_2:
			lastSensorPos = Constants.instance.GetOuterPieceAxis (1);
			break;
		case Constants.SensorId.OUTER_3:
			lastSensorPos = Constants.instance.GetOuterPieceAxis (2);
			break;
		case Constants.SensorId.OUTER_4:
			lastSensorPos = Constants.instance.GetOuterPieceAxis (3);
			break;
		case Constants.SensorId.OUTER_5:
			lastSensorPos = Constants.instance.GetOuterPieceAxis (4);
			break;
		case Constants.SensorId.OUTER_6:
			lastSensorPos = Constants.instance.GetOuterPieceAxis (5);
			break;
		case Constants.SensorId.OUTER_7:
			lastSensorPos = Constants.instance.GetOuterPieceAxis (6);
			break;
		case Constants.SensorId.OUTER_8:
			lastSensorPos = Constants.instance.GetOuterPieceAxis (7);
			break;
		case Constants.SensorId.INNER_1:
			lastSensorPos = Constants.instance.GetInnerPieceAxis (0);
			break;
		case Constants.SensorId.INNER_2:
			lastSensorPos = Constants.instance.GetInnerPieceAxis (1);
			break;
		case Constants.SensorId.INNER_3:
			lastSensorPos = Constants.instance.GetInnerPieceAxis (2);
			break;
		case Constants.SensorId.INNER_4:
			lastSensorPos = Constants.instance.GetInnerPieceAxis (3);
			break;
		case Constants.SensorId.INNER_5:
			lastSensorPos = Constants.instance.GetInnerPieceAxis (4);
			break;
		case Constants.SensorId.INNER_6:
			lastSensorPos = Constants.instance.GetInnerPieceAxis (5);
			break;
		case Constants.SensorId.INNER_7:
			lastSensorPos = Constants.instance.GetInnerPieceAxis (6);
			break;
		case Constants.SensorId.INNER_8:
			lastSensorPos = Constants.instance.GetInnerPieceAxis (7);
			break;
		default:
			lastSensorPos = Vector2.zero;
			break;
		}
		
		float sensorPlunge = 0;
		//			// 終点とセンサーの位置を比べて、どれくらいでセンサーに突入するかを見る.
		//			Vector2[] lineAndSensorIntersections = CircleCalculator.LineAndCircleIntersectPoints (lastSensorPos, Constants.instance.SENSOR_RADIUS, lastPatternStartPos, lastPatternTargetPos);
		//			float distanceOfTargetToSensor = Vector2.Distance (lastPatternTargetPos, lastSensorPos);
		//			if (distanceOfTargetToSensor < Constants.instance.SENSOR_RADIUS) {
		//				// 始点がすでにセンサーの中である.
		//				sensorPlunge = Constants.instance.SENSOR_RADIUS;
		//			}
		//			else if (lineAndSensorIntersections.Length == 2) {
		//				if (Vector2.Distance (lineAndSensorIntersections[0], lastPatternStartPos) < Vector2.Distance (lineAndSensorIntersections[1], lastPatternStartPos)) {
		//					sensorPlunge = Vector2.Distance (lineAndSensorIntersections[0], lastPatternTargetPos);
		//				}
		//				else {
		//					sensorPlunge = Vector2.Distance (lineAndSensorIntersections[1], lastPatternTargetPos);
		//				}
		//			}
		//			else if (lineAndSensorIntersections.Length == 1) {
		//				sensorPlunge = Vector2.Distance (lineAndSensorIntersections[0], lastPatternTargetPos);
		//			}
		//			else {
		//				Vector2 verticalPoint = CircleCalculator.GetVerticalPoint(lastSensorPos, lastPatternStartPos, lastPatternTargetPos);
		//				float verticalPointToSensorDegree = CircleCalculator.PointToDegree (verticalPoint, lastSensorPos);
		//				float verticalPointToSensorDistance = CircleCalculator.PointToPointDistance (verticalPoint, lastSensorPos);
		//				Vector2 parallelStartPos = CircleCalculator.PointOnCircle (lastPatternStartPos, verticalPointToSensorDistance, verticalPointToSensorDegree);
		//				Vector2 parallelTargetPos = CircleCalculator.PointOnCircle (lastPatternTargetPos, verticalPointToSensorDistance, verticalPointToSensorDegree);
		//				Vector2[] parallelLineAndSensorIntersections = CircleCalculator.LineAndCircleIntersectPoints (lastSensorPos, Constants.instance.SENSOR_RADIUS, parallelStartPos, parallelTargetPos);
		//				distanceOfTargetToSensor = Vector2.Distance (parallelTargetPos, parallelStartPos);
		//				if (distanceOfTargetToSensor < Constants.instance.SENSOR_RADIUS) {
		//					// 平行始点がすでにセンサーの中である.
		//					sensorPlunge = Constants.instance.SENSOR_RADIUS;
		//				}
		//				else if (parallelLineAndSensorIntersections.Length == 2) {
		//					if (Vector2.Distance (parallelLineAndSensorIntersections[0], parallelStartPos) < Vector2.Distance (parallelLineAndSensorIntersections[1], parallelStartPos)) {
		//						sensorPlunge = Vector2.Distance (parallelLineAndSensorIntersections[0], parallelTargetPos);
		//					}
		//					else {
		//						sensorPlunge = Vector2.Distance (parallelLineAndSensorIntersections[1], parallelTargetPos);
		//					}
		//				}
		//				else if (parallelLineAndSensorIntersections.Length == 1) {
		//					sensorPlunge = Vector2.Distance (parallelLineAndSensorIntersections[0], parallelTargetPos);
		//				}
		//			}
		
		// 最後のパターンの、終点から始点へのベクトル (右手座標)
		Vector2 targetToStart = lastCommandStartPos - lastCommandTargetPos;
		// 最後のパターンの、終点からセンサーへのベクトル (右手座標)
		Vector2 targetToSensor = lastSensorPos - lastCommandTargetPos;
		// 力の方向を算出.
		float power = Vector2.Dot (targetToStart, targetToSensor);
		if (power < 0) {
			sensorPlunge = Constants.instance.SENSOR_RADIUS - Vector2.Distance (lastCommandTargetPos, lastSensorPos);
			if (sensorPlunge < 0) {
				sensorPlunge = 0;
			}
		}
		else {
			sensorPlunge = Constants.instance.SENSOR_RADIUS + Vector2.Distance (lastCommandTargetPos, lastSensorPos);
		}
		
		// ☆の移動開始時間に、☆が最後のセンサーに突入する時間を足す.
		if (onStarMoveSpeed > 0) {
			justTime = onStarMoveStartTime + ((GetTotalDistance() - sensorPlunge) / onStarMoveSpeed).ToLong();
			foreach (var checkPoint in checkPoints) {
				checkPoint.justTime = justTime;
			}
		}
		else {
			Debug.LogError ("Slide Distance is Zero. JustTime Deferment");
		}
	}

	public void Reset () {
		foreach (var command in commands) {
			command.Reset ();
		}
		checkPointProgress = -1;
	}

	public void Release () {
		foreach (var command in commands) {
			command.Release ();
		}
		commands = null;
		checkPoints = null;
	}





	// センサーIDが連続して同じIDだったら抜いて、新しいリストで返す.
	public static Constants.SensorId[] CreateCheckPointSensors (Constants.SensorId[][] createdIds) {
		List<Constants.SensorId> ret = new List<Constants.SensorId> ();
		if (createdIds != null) {
			foreach (var ids in createdIds) {
				if (ids != null) {
					foreach (var id in ids) {
						if (ret.Count == 0 || ret [ret.Count - 1] != id) {
							ret.Add (id);
						}
					}
				}
			}
		}
		return ret.ToArray ();
	}
}
