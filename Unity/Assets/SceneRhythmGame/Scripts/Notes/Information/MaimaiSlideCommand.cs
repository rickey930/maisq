﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class MaimaiSlideCommand {
	public MaimaiSlideCommand (float thisDistance, float totalDistance) {
		this.distance = thisDistance;
		this.totalDistance = totalDistance;
	}

	public virtual void Setup (MaimaiSlidePattern pattern) {
		this.pattern = pattern;
	}

	public MaimaiSlidePattern pattern { get; protected set; }
	/// <summary>
	/// このコマンドで進む距離.
	/// </summary>
	public float distance { get; protected set; }
	/// <summary>
	/// このコマンドまでの合計距離. このコマンドで進む距離は含めない.
	/// </summary>
	public float totalDistance { get; protected set; }
	/// <summary>
	/// 始点 (右手座標系).
	/// </summary>
	public Vector2 startPos { get; protected set; }
	/// <summary>
	/// 終点 (右手座標系).
	/// </summary>
	public Vector2 targetPos { get; protected set; }
	/// <summary>
	/// <para>判定画像の回転角度 (右手座標系)</para>
	/// <para>直線なら、始点から終点を見た角度</para>
	/// <para>曲線なら、終点の回転角度(startDeg+distanceDeg*vec)</para>
	/// </summary>
	public float imgDeg { get; protected set; }

	/// <summary>
	/// このスライドパターンの距離.
	/// </summary>
	public float GetDistance() { return distance; }
	/// <summary>
	/// このスライドパターンが始まる前までのスライドの合計距離.
	/// </summary>
	public float GetTotalDistanceOfUntilThisMoveStart() { return this.totalDistance; }
	/// <summary>
	/// このスライドパターンが終わった後のスライドの合計距離.
	/// </summary>
	public float GetTotalDistanceOfWhileThisMoveEnd() { return GetDistance () + GetTotalDistanceOfUntilThisMoveStart (); }
	/// <summary>
	/// スライドパターンの始点 (右手座標系).
	/// </summary>
	public Vector2 GetStartPosition() {
		return startPos;
	}
	/// <summary>
	/// スライドパターンの終点 (右手座標系).
	/// </summary>
	public Vector2 GetTargetPosition() {
		return targetPos;
	}
	
	public float GetImageDegree () {
		return imgDeg;
	}
	public abstract Constants.SensorId[] GetCheckPointSensorIds ();
	public abstract MaimaiSlideCommandType GetCommandType ();
	
	public void Reset () {
	}
	
	public void Release() {
		this.pattern = null;
	}
}

public class MaimaiSlideStraightCommand : MaimaiSlideCommand {
	public MaimaiSlideStraightCommand (Vector2 drawStart, Vector2 drawTarget, float totalDistance)
	: base (CircleCalculator.PointToPointDistance(drawStart * Constants.instance.MAIMAI_OUTER_RADIUS, drawTarget * Constants.instance.MAIMAI_OUTER_RADIUS), totalDistance) {
		// 右手座標.
		this.startPos = Constants.instance.ToLeftHandedCoordinateSystemPosition(drawStart * Constants.instance.MAIMAI_OUTER_RADIUS);
		this.targetPos = Constants.instance.ToLeftHandedCoordinateSystemPosition(drawTarget * Constants.instance.MAIMAI_OUTER_RADIUS);
		this.imgDeg = CircleCalculator.PointToDegree(startPos, targetPos);
	}
	
	public override Constants.SensorId[] GetCheckPointSensorIds () {
		return CreateCheckPointSensorsStraight (startPos.x, startPos.y, targetPos.x, targetPos.y);
	}
	
	public override MaimaiSlideCommandType GetCommandType () {
		return MaimaiSlideCommandType.STRAIGHT;
	}

	
	// コマンドがどのセンサーを通るか (直線版).
	public static Constants.SensorId[] CreateCheckPointSensorsStraight(float start_x, float start_y, float target_x, float target_y) {
		/*
		sensorは推測する.
		<一個分の大きさを進んでみて、一番近いセンサーを探す.
		前と同じセンサーなら前の終点をこれにする.
		距離を超えそうなら、最後の処理とする.
		*/
		var ret = new List<Constants.SensorId>();
		// 右手座標.
		Vector2 startPos = new Vector2 (start_x, start_y);
		Vector2 targetPos = new Vector2 (target_x, target_y);
		float totalDistance = CircleCalculator.PointToPointDistance (startPos, targetPos);
		if (totalDistance < 0) {
			totalDistance *= -1;
			Debug.LogError ("totalDistance is minus");
		}
		float progress = 0;
		float workQuantity = Constants.instance.SLIDE_MARKER_TO_SENSOR_MARGIN; //一度に進む距離.
		bool finalCheck = false;
		while (!finalCheck) {
			if (progress >= totalDistance) {
				finalCheck = true;
				progress = totalDistance;
			}
			var nowTargetPos = CircleCalculator.PointToPointMovePoint(startPos, targetPos, progress);
			float nearestDistance = float.PositiveInfinity;
			Constants.SensorId sensor = Constants.SensorId.CENTER;
			// 中央.
			{
				var sensorPos = Vector2.zero;
				float toSensorDistance = Vector2.Distance (nowTargetPos, sensorPos);
				if (toSensorDistance < nearestDistance) {
					nearestDistance = toSensorDistance;
				}
			}
			// 内周.
			for (int i = 0; i < 8; i++) {
				var sensorPos = Constants.instance.GetInnerPieceAxis(i);
				float toSensorDistance = Vector2.Distance (nowTargetPos, sensorPos);
				if (toSensorDistance < nearestDistance) {
					sensor = Constants.InnerSensorIds[i];
					nearestDistance = toSensorDistance;
				}
			}
			// 外周.
			for (int i = 0; i < 8; i++) {
				var sensorPos = Constants.instance.GetOuterPieceAxis(i);
				float toSensorDistance = Vector2.Distance (nowTargetPos, sensorPos);
				if (toSensorDistance < nearestDistance) {
					sensor = Constants.OuterSensorIds[i];
					nearestDistance = toSensorDistance;
				}
			}
			
			if (ret.Count == 0 || ret[ret.Count - 1] != sensor) {
				ret.Add (sensor);
			}
			progress += workQuantity;
		}
		return ret.ToArray ();
	}
	public static Constants.SensorId[] CreateCheckPointSensorsLeftHandRateStraight(float start_x, float start_y, float target_x, float target_y) {
		float realRadius = Constants.instance.MAIMAI_OUTER_RADIUS;
		return CreateCheckPointSensorsStraight (start_x * realRadius, start_y * realRadius * -1, target_x * realRadius, target_y * realRadius * -1);
	}
}

public class MaimaiSlideCurveCommand : MaimaiSlideCommand {
	/// <summary>
	/// 中心 (右手座標系).
	/// </summary>
	public Vector2 centerPos { get; protected set; }
	/// <summary>
	/// 半径 (右手座標系).
	/// </summary>
	public Vector2 radius { get; protected set; }
	/// <summary>
	/// 開始角度 (右手座標系).
	/// </summary>
	public float startDeg { get; protected set; }
	/// <summary>
	/// 開始角度から進む角度(通常は45度) (右手座標系).
	/// </summary>
	public float distanceDeg { get; protected set; }
	/// <summary>
	/// 時計周り(1)か反時計回り(-1)かの方向.
	/// </summary>
	public float vec { get; protected set; }

	public MaimaiSlideCurveCommand (Vector2 centerPos, float radius, float startDeg, float distanceDeg, float vec, float totalDistance)
	: base (CircleCalculator.ArcDistance(radius * Constants.instance.MAIMAI_OUTER_RADIUS, distanceDeg), totalDistance) {
		this.centerPos = Constants.instance.ToLeftHandedCoordinateSystemPosition(centerPos * Constants.instance.MAIMAI_OUTER_RADIUS);
		this.radius = new Vector2(radius, radius) * Constants.instance.MAIMAI_OUTER_RADIUS;
		this.startDeg = startDeg;
		this.distanceDeg = distanceDeg;
		this.vec = vec < 0 ? -1 : 1;
		this.startPos = CircleCalculator.PointOnEllipse(this.centerPos, this.radius, this.startDeg);
		this.targetPos = CircleCalculator.PointOnEllipse(this.centerPos, this.radius, this.startDeg + this.distanceDeg * this.vec);
		//this.imgDeg = CircleCalculator.PointToDegree (this.startPos, this.targetPos); //(startDeg + distanceDeg) * vec;
		//this.imgDeg = (this.startDeg + 90.0f + this.distanceDeg * this.vec + (this.vec > 0 ? 0 : 180));
		//this.imgDeg = CircleCalculator.PointToDegree (this.centerPos, this.targetPos);
		this.imgDeg = this.startDeg + this.distanceDeg * this.vec;
	}
	
	public MaimaiSlideCurveCommand (Vector2 centerPos, Vector2 radius, float startDeg, float distanceDeg, float vec, float totalDistance)
	: base (CircleCalculator.ArcDistance(Mathf.Max(radius.x, radius.y) * Constants.instance.MAIMAI_OUTER_RADIUS, distanceDeg), totalDistance) {
		this.centerPos = Constants.instance.ToLeftHandedCoordinateSystemPosition(centerPos * Constants.instance.MAIMAI_OUTER_RADIUS);
		this.radius = radius * Constants.instance.MAIMAI_OUTER_RADIUS;
		this.startDeg = startDeg;
		this.distanceDeg = distanceDeg;
		this.vec = vec < 0 ? -1 : 1;
		this.startPos = CircleCalculator.PointOnEllipse(this.centerPos, this.radius, this.startDeg);
		this.targetPos = CircleCalculator.PointOnEllipse(this.centerPos, this.radius, this.startDeg + this.distanceDeg * this.vec);
		//this.imgDeg = CircleCalculator.PointToDegree (this.startPos, this.targetPos); //(startDeg + distanceDeg) * vec;
		//this.imgDeg = (this.startDeg + 90.0f + this.distanceDeg * this.vec + (this.vec > 0 ? 0 : 180));
		//this.imgDeg = CircleCalculator.PointToDegree (this.centerPos, this.targetPos);
		this.imgDeg = this.startDeg + this.distanceDeg * this.vec;
	}
	
	public override Constants.SensorId[] GetCheckPointSensorIds () {
		return CreateCheckPointSensorsCurve (centerPos.x, centerPos.y, radius.x, radius.y, startDeg, distanceDeg * vec);
	}
	
	public override MaimaiSlideCommandType GetCommandType () {
		return MaimaiSlideCommandType.CURVE;
	}
	
	// コマンドがどのセンサーを通るか (曲線版).
	public static Constants.SensorId[] CreateCheckPointSensorsCurve(float center_x, float center_y, float radius_x, float radius_y, float start_degree, float distance_degree) {
		if (distance_degree == 0) {
			return new Constants.SensorId[0];
		}
		/*
		sensorは推測する.
		<一個分の大きさを進んでみて、一番近いセンサーを探す.
		前と同じセンサーなら前の終点をこれにする.
		距離を超えそうなら、最後の処理とする.
		*/
		var ret = new List<Constants.SensorId>();
		var centerPos = new Vector2 (center_x, center_y);
		var radius = new Vector2 (radius_x, radius_y);
		float radiusInterim = Mathf.Max(radius.x, radius.y);
		float totalDistance = CircleCalculator.ArcDistance(radiusInterim, Mathf.Abs(distance_degree));
		float vec = distance_degree < 0 ? -1 : 1;
		//float startDegree = 0;
		float progress = 0;
		float workQuantity = Constants.instance.SLIDE_MARKER_TO_SENSOR_MARGIN; //一度に進む距離.
		bool finalCheck = false;
		while (!finalCheck) {
			if (progress >= totalDistance) {
				finalCheck = true;
				progress = totalDistance;
			}
			var nowProgressDegree = CircleCalculator.ArcDegreeFromArcDistance(radiusInterim, progress * vec);
			//var nowDistanceDegree = nowProgressDegree - startDegree;
			var nowTargetPos = CircleCalculator.PointOnEllipse(centerPos, radius, start_degree + nowProgressDegree);
			float nearestDistance = float.PositiveInfinity;
			Constants.SensorId sensor = Constants.SensorId.CENTER;
			// 中央.
			{
				var sensorPos = Vector2.zero;
				float toSensorDistance = Vector2.Distance (nowTargetPos, sensorPos);
				if (toSensorDistance < nearestDistance) {
					nearestDistance = toSensorDistance;
				}
			}
			// 内周.
			for (int i = 0; i < 8; i++) {
				var sensorPos = Constants.instance.GetInnerPieceAxis(i);
				float toSensorDistance = Vector2.Distance (nowTargetPos, sensorPos);
				if (toSensorDistance < nearestDistance) {
					sensor = Constants.InnerSensorIds[i];
					nearestDistance = toSensorDistance;
				}
			}
			// 外周.
			for (int i = 0; i < 8; i++) {
				var sensorPos = Constants.instance.GetOuterPieceAxis(i);
				float toSensorDistance = Vector2.Distance (nowTargetPos, sensorPos);
				if (toSensorDistance < nearestDistance) {
					sensor = Constants.OuterSensorIds[i];
					nearestDistance = toSensorDistance;
				}
			}
			
			if (ret.Count == 0 || ret[ret.Count - 1] != sensor) {
				ret.Add (sensor);
			}
			progress += workQuantity;
		}
		return ret.ToArray ();
	}
	public static Constants.SensorId[] CreateCheckPointSensorsLeftHandRateCurve(float center_x, float center_y, float radius_x, float radius_y, float start_degree, float distance_degree) {
		float realRadius = Constants.instance.MAIMAI_OUTER_RADIUS;
		return CreateCheckPointSensorsCurve (center_x * realRadius, center_y * realRadius * -1, radius_x * realRadius, radius_y * realRadius, start_degree, distance_degree);
	}
}

public enum MaimaiSlideCommandType {
	STRAIGHT, CURVE
}
