﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MaimaiSlideNote : MaimaiNote {
	public MaimaiSlideNote (string uniqueId, long justTime, long missTime, int actionId, Constants.SensorId sensorId)
	: base (uniqueId, justTime, missTime, actionId) {
		this.sensorId = sensorId;
	}

	public override MaimaiNoteType GetNoteType () {
		return MaimaiNoteType.SLIDE;
	}

	public Constants.SensorId sensorId { get; set; }

	public MaimaiSlidePattern pattern { get; set; }
	public MaimaiSlideChain chain { get; set; }

	public void Setup (MaimaiSlidePattern pattern, MaimaiSlideChain chain) {
		this.pattern = pattern;
		this.chain = chain;
	}

	/// <summary>
	/// 最後にセンサーに触れた時間.
	/// </summary>
	public long? lastCensoredTime { get; set; }

	/// <summary>
	/// シンク評価するためのスライドパターンの代表的なノートを取得する.
	/// </summary>
	public MaimaiSlideNote GetSyncEvaluateRepresentativeNote () {
		return pattern.chains [0].checkPoints [0]; //最初の最初なら確実にノートあるよね...
	}

	public override void Reset () {
		base.Reset ();
		pattern.Reset ();
		lastCensoredTime = null; 
	}
	
	public override void Release() {
		base.Release();
		pattern.Reset ();
		pattern = null;
		chain = null;
	}

}
