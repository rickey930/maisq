using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MaimaiSlideNoteHead : MaimaiNote, IMaimaiSlideNoteHead {
	public MaimaiSlideNoteHead(string uniqueId, long readyTime) : base (uniqueId + "_HEAD", readyTime, 0, Constants.instance.MAIMAI_NOTE_SLIDE_HEAD_ACTION_ID) {
		eachNotes = new HashSet<IMaimaiEachNote>();
		relativeNotes = new List<MaimaiSlidePattern> ();
	}
	
	public override MaimaiNoteType GetNoteType () {
		return MaimaiNoteType.SLIDE_HEAD;
	}
	
	public long GetSlideReadyTime() {
		return justTime;
	}

	public HashSet<IMaimaiEachNote> eachNotes; //ダミー.
	public void AddEachNote(IMaimaiEachNote note) { }
	public HashSet<IMaimaiEachNote> GetEachNotes() {
		return eachNotes;
	}
	
	public List<MaimaiSlidePattern> relativeNotes { get; set; }
	public void SetRelativeNote(MaimaiSlidePattern note) {
		if (!relativeNotes.Contains (note)) {
			relativeNotes.Add (note);
			note.SetRelativeNote(this);
		}
	}
	public MaimaiSlidePattern[] GetRelativeNote() {
		return relativeNotes.ToArray ();
	}

	public float GetImageDegree() {
		return CircleCalculator.PointToDegree (relativeNotes[0].chains[0].commands[0].GetStartPosition ());
	}

	public override void Release () {
		base.Release ();
		eachNotes = null;
		relativeNotes = null;
	}

}
