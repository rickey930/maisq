using UnityEngine;
using System.Collections;

public class MaimaiSlideNoteMoveStart : MaimaiNote {
	// ActionIdには絶対にEvaluateで使わない値を入れる.
	public MaimaiSlideNoteMoveStart(string uniqueId, MaimaiSlideChain slideChain) : base (uniqueId, slideChain.onStarMoveStartTime, 0, Constants.instance.MAIMAI_NOTE_SLIDE_SOUND_ACTION_ID) {
		relativeNote = slideChain;
	}

	public MaimaiSlideChain relativeNote { get; set; }

	public override MaimaiNoteType GetNoteType () {
		return MaimaiNoteType.SLIDE_MOVE_START;
	}

	public override void Release() {
		base.Release();
		relativeNote = null;
	}
}
