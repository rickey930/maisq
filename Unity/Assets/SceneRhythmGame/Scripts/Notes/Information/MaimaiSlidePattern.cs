﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MaimaiSlidePattern : IMaimaiEachNote {
	public MaimaiSlidePattern (MaimaiSlideChain[] chains) {
		this.chains = chains;
		slideNoteAmount = 0;
		foreach (var chain in chains) {
			slideNoteAmount += chain.checkPoints.Length;
		}
		
		evaluated = false;
		eachNotes = new HashSet<IMaimaiEachNote>();
	}
	
	/*
	 * 頭とチェックポイントと描画コマンドとイーチ.
	 * IMaimaiSlideNoteHead
	 * MaimaiSlideNote[パターンindex][チェックポイントindex]
	 * MaimaiSlideCommand[パターンindex][コマンドindex]
	 * List<MaimaiSlideLeader>
	 * 
	 * ガイドスターの移動関連の値.
	 * 
	 * パターンが複数あるときは、最後に判定したパターンのJustTimeで最終的な判定をする.
	 * 
	 * ひとつのスライド判定がパターン
	 * ひとつの☆の動き方がチェイン
	 * ひとつの描画命令がコマンド
	 */

	// スターノート.
	public IMaimaiSlideNoteHead relativeNote;
	public void SetRelativeNote(IMaimaiSlideNoteHead note) {
		if (relativeNote == null) {
			relativeNote = note;
			relativeNote.SetRelativeNote(this);
		}
	}
	
	// スライドチェイン.
	public MaimaiSlideChain[] chains { get; set; }
	// スライドノート数.
	public int slideNoteAmount { get; protected set; }

	/// <summary>
	/// イーチノーツ. スライドにおいては、justTimeではなくreadyTimeが同じであるノーツを意味する.
	/// </summary>
	public HashSet<IMaimaiEachNote> eachNotes;
	public void AddEachNote(IMaimaiEachNote note) {
		if (note != this) {
			if (note is MaimaiSlidePattern) {
				eachNotes.Add(note);
			}
		}
	}
	public HashSet<IMaimaiEachNote> GetEachNotes() {
		return eachNotes;
	}

	/// <summary>
	/// スライド操作がひとつ前で終わったならTooLateではなく緑Late. 
	/// </summary>
	public bool IsLateGoodPassible () {
		for (int i = 0; i < chains.Length; i++) {
			if (!chains[i].isLeft && !chains[i].isCompleted) {
				return false;
			}
		}
		return true; //すべてのパターンが一つ残すか全部処理しきっているなら緑Lateでもいい. (全部処理しきっているならこのメソッドが呼ばれる前に判定処理がされているはず)
	}

	private int resetRequestCount;
	public void Reset () {
		resetRequestCount++;
		if (resetRequestCount >= slideNoteAmount) {
			foreach (var chain in chains) {
				chain.Reset ();
			}
			evaluated = false;
			slideSoundCalled = false;
			resetRequestCount = 0;
		}
	}
	
	private int releaseRequestCount;
	public void Release() {
		releaseRequestCount++;
		if (releaseRequestCount >= slideNoteAmount) {
			eachNotes.Clear ();
			eachNotes = null;
			relativeNote = null;
			foreach (var chain in chains) {
				chain.Release ();
			}
			chains = null;
			releaseRequestCount = 0;
		}
	}
	
	public bool evaluated;
	public bool IsEvaluated { get { return evaluated; } }
	public void SetEvaluated(bool value) { evaluated = value; }

	// se_slideが鳴った. (チェインごとに鳴らしたくない)
	public bool slideSoundCalled { get; set; }

	/// <summary>
	/// スライドの判定が最後に終わるチェインを取得.
	/// </summary> //OnJust用.
	public MaimaiSlideChain GetLatestJudgeChain () {
		if (_latestJudgeChain == null) {
			long time = -1;
			foreach (var chain in chains) {
				if (time < chain.justTime) {
					time = chain.justTime;
					_latestJudgeChain = chain;
				}
			}
		}
		return _latestJudgeChain;
	}
	private MaimaiSlideChain _latestJudgeChain;

	/// <summary>
	/// スライド進捗度 チェインのスライド進捗度の中で一番低い値を返す.
	/// </summary> //スライドマーカー非表示アニメーション用.
	public float progressRate {
		get {
			float min = float.MaxValue;
			foreach (var chain in chains) {
				if (min > chain.progressRate) {
					min = chain.progressRate;
				}
			}
			return min;
		}
	}

	/// <summary>
	/// スライドの距離が最も長い値を取得.
	/// </summary> // スライドマーカー非表示アニメーション用.
	public float GetLongestChainTotalDistance () {
		if (_longestChainTotalDistance == null) {
			float distance = 0;
			foreach (var chain in chains) {
				if (distance < chain.GetTotalDistance()) {
					distance = chain.GetTotalDistance();
				}
			}
			_longestChainTotalDistance = distance;
		}
		return _longestChainTotalDistance.Value;
	}
	private float? _longestChainTotalDistance;
}
