using UnityEngine;
using System.Collections;

public class MaimaiStarBreakNote : MaimaiStarNote {
	public MaimaiStarBreakNote (string uniqueId, long justTime, long missTime, int actionId, int buttonId) : base (uniqueId, justTime, missTime, actionId, buttonId) {
		
	}
	
	public override MaimaiNoteType GetNoteType() {
		return MaimaiNoteType.BREAK_STAR;
	}
}
