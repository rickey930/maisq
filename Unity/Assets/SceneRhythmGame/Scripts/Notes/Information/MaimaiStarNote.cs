using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MaimaiStarNote : MaimaiTapNote, IMaimaiSlideNoteHead {
	public MaimaiStarNote (string uniqueId, long justTime, long missTime, int actionId, int buttonId) : base (uniqueId, justTime, missTime, actionId, buttonId) {
		relativeNotes = new List<MaimaiSlidePattern> ();
	}
	
	public override MaimaiNoteType GetNoteType() {
		return MaimaiNoteType.TAP_STAR;
	}
	
	public List<MaimaiSlidePattern> relativeNotes { get; set; }
	public void SetRelativeNote(MaimaiSlidePattern note) {
		if (!relativeNotes.Contains (note)) {
			relativeNotes.Add (note);
			note.SetRelativeNote(this);
		}
	}
	public MaimaiSlidePattern[] GetRelativeNote() {
		return relativeNotes.ToArray ();
	}

	public long GetSlideReadyTime() {
		return justTime;
	}
	
	public override void Release() {
		base.Release();
		relativeNotes = null;
	}
}
