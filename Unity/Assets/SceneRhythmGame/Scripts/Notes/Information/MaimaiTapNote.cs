using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MaimaiTapNote : MaimaiNoteB, IMaimaiEachNote {
	public MaimaiTapNote (string uniqueId, long justTime, long missTime, int actionId, int buttonId) : base (uniqueId, justTime, missTime, actionId, buttonId) {
		this.eachNotes = new HashSet<IMaimaiEachNote>();
		createdEachOval = false;
	}

	public override MaimaiNoteType GetNoteType() {
		return MaimaiNoteType.TAP_CIRCLE;
	}

	public HashSet<IMaimaiEachNote> eachNotes;
	public void AddEachNote(IMaimaiEachNote note) { 
		if (note != this) {
			if (note is MaimaiNote) {
				eachNotes.Add(note);
			}
		}
	}
	public HashSet<IMaimaiEachNote> GetEachNotes() { return eachNotes; }
	public bool createdEachOval { get; set; }

	public override void Reset () {
		base.Reset ();
		createdEachOval = false;
	}

	public override void Release() {
		base.Release();
		this.eachNotes.Clear();
	}
}
