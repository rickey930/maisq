using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SlidePatternFactory {
	
	/*
	public enum Circumference {
		OUTER, INNER, CENTER,
	};
	public enum SlideVector {
		STRAIGHT, CURVE_CLOCK, CURVE_REVERSE_CLOCK,
	};
	//楽譜生成時に、まずはスライドノートを作って、アクションIDに始点を入れる。
	//その次にパターンファクトリのインスタンスを作成して、this.setPatternを呼び出す。そのとき改めて始点と、それと次点を使う。
	//例えば#A1^4^7[x:y]で、A1のアクションIDでスライドノートを作った後、パターンファクトリの(略)、this.setPatternでA1とA4を使う。

	public SlidePatternFactory(MaimaiSlideNote parent, int uniqueId, int iniActId) {
		this.parent = parent;
		this.pattern = new List<MaimaiSlideCommand>();
		this.totalDistance = 0;
		this.errorMessage = "";
		this.initActionId = iniActId;
		this.uniqueId = uniqueId;
	}

	private MaimaiSlideNote parent { get; set; }
	private List<MaimaiSlideCommand> pattern { get; set; }
	private float totalDistance { get; set; }
	public string errorMessage { get; set; }
	private int initActionId { get; set; }
	private int uniqueId { get; set; }

	public const int UN_USE_PARAM = -1;
	
	public string GetErrorMessage() { return this.errorMessage; }
	public MaimaiSlideCommand[] GetPattern() { return this.pattern.ToArray(); }

	//楽譜生成時に、これに情報を入れてパターンを作っていく。falseが返ってきたら何らかのエラーがあったということ
	public bool SetPattern(Circumference S, Circumference T, int start, int target, SlideVector slideVector) {
		while (start < 0) start += 8;
		start %= 8;
		while (target < 0) target += 8;
		target %= 8;
		//全部に言えるけど、カーブだったら再帰する必要がある。現状、1^4を認識しない(1-4と認識している)
		if (S == Circumference.OUTER) {
			if (T == Circumference.OUTER) { 
				//Ax->Ay
				//進み方によって分岐する
				if (slideVector == SlideVector.STRAIGHT) {
					//同じボタンだからエラー
					if (target == (start + 0) % 8) { 
						this.SetError(1);
					}
					//隣のボタンなら確定
					else if (target == (start + 1) % 8 || target == (start + 7) % 8 ||
					         target == (start + 2) % 8 || target == (start + 6) % 8) {
						this.AddPattern(S, T, start, target, slideVector);
					}
					else if (target == (start + 3) % 8) {
						//再帰 例：A1-A4
						if (this.SetPattern(S, Circumference.INNER, start, (start + 1) % 8, slideVector)) { 
							//例：A1-B2
							this.SetPattern(Circumference.INNER, T, (start + 1) % 8, target, slideVector); //例：B2-A4
						}
					}
					else if (target == (start + 4) % 8) { 
						//再帰 例：A1-A5
						if (this.SetPattern(S, Circumference.INNER, start, start, slideVector)) { 
							//例：A1-B1
							this.SetPattern(Circumference.INNER, T, start, target, slideVector); //例：B1-A5
						}
					}
					else if (target == (start + 5) % 8) {
						//再帰 例：A1-A6
						if (this.SetPattern(S, Circumference.INNER, start, (start + 7) % 8, slideVector)) { 
							//例：A1-B8
							this.SetPattern(Circumference.INNER, T, (start + 7) % 8, target, slideVector); //例：B8-A6
						}
					}
				}
				else { 
					if (slideVector == SlideVector.CURVE_CLOCK) { 
						//進行方向隣なら確定
						if (target == (start + 1) % 8) { 
							this.AddPattern(S, T, start, target, slideVector);
						}
						//曲線 時計回り 再帰 例：A1^4
						else if (this.SetPattern(S, T, start, (start + 1) % 8, slideVector)) { 
							//例：A1^2
							this.SetPattern(S, T, (start + 1) % 8, target, slideVector); //例：A2^4
						}
					}
					else if (slideVector == SlideVector.CURVE_REVERSE_CLOCK) { 
						//進行方向隣なら確定
						if (target == (start + 7) % 8) { 
							this.AddPattern(S, T, start, target, slideVector);
						}
						//曲線 反時計回り 再帰 例：A1^6
						else if (this.SetPattern(S, T, start, (start + 7) % 8, slideVector)) { 
							//例：A1^8
							this.SetPattern(S, T, (start + 7) % 8, target, slideVector); //例：A8^6
						}
					}
				}
			}
			else if (T == Circumference.INNER) { 
				//Ax->By
				//直線でのみ移動可
				if (slideVector == SlideVector.STRAIGHT) { 
					//隣接しているなら直で行ける
					//または通過点が無い場合も直で行く
					if (target == (start + 0) % 8 || target == (start + 1) % 8 || target == (start + 7) % 8 ||
					    target == (start + 3) % 8 || target == (start + 5) % 8) { 
						this.AddPattern(S, T, start, target, slideVector);
					}
					//通過点がある場合は分岐する
					else if (target == (start + 2) % 8) { 
						//再帰 例：A1-B3
						if (this.SetPattern(S, Circumference.INNER, start, (start + 1) % 8, slideVector)) { 
							//例：A1-B2
							this.SetPattern(Circumference.INNER, T, (start + 1) % 8, target, slideVector); //例：B2-B3
						}
					}
					else if (target == (start + 4) % 8) { 
						//再帰 例：A1-B5
						if (this.SetPattern(S, Circumference.INNER, start, start, slideVector)) { 
							//例：A1-B1
							this.SetPattern(Circumference.INNER, T, start, target, slideVector); //例：B1-B5
						}
					}
					else if (target == (start + 6) % 8) { 
						//再帰 例：A1-B7
						if (this.SetPattern(S, Circumference.INNER, start, (start + 7) % 8, slideVector)) { 
							//例：A1-B8
							this.SetPattern(Circumference.INNER, T, (start + 7) % 8, target, slideVector); //例：B8-B7
						}
					}
				}
				else {
					this.SetError(5); //曲線で別周侵入禁止
				}
			}
			else if (T == Circumference.CENTER) { 
				//再帰
				if (slideVector == SlideVector.STRAIGHT) { 
					//例：A1-C
					if (this.SetPattern(S, Circumference.INNER, start, start, slideVector)) { 
						//例：A1-B1
						this.SetPattern(Circumference.INNER, T, start, SlidePatternFactory.UN_USE_PARAM, slideVector); //例：B1-C
					}
				}
				else { 
					this.SetError(5); //曲線で別周侵入禁止
				}
			}
		}
		else if (S == Circumference.INNER) { 
			if (T == Circumference.OUTER) { 
				//Bx->Ay
				//直線でのみ移動可
				if (slideVector == SlideVector.STRAIGHT) { 
					//隣接しているなら直で行ける
					//または通過点が無い場合も直で行く
					if (target == (start + 0) % 8 || target == (start + 1) % 8 || target == (start + 7) % 8 || 
					    target == (start + 3) % 8 || target == (start + 5) % 8) { 
						this.AddPattern(S, T, start, target, slideVector);
					}
					//通過点がある場合は分岐する
					else if (target == (start + 2) % 8) { 
						//再帰 例：B1-A3
						if (this.SetPattern(S, Circumference.INNER, start, (start + 1) % 8, slideVector)) { 
							//例：B1-B2
							this.SetPattern(Circumference.INNER, T, (start + 1) % 8, target, slideVector); //例：B2-A3
						}
					}
					else if (target == (start + 4) % 8) { 
						//再帰 例：B1-A5
						if (this.SetPattern(S, Circumference.CENTER, start, SlidePatternFactory.UN_USE_PARAM, slideVector)) { 
							//例：B1-C
							this.SetPattern(Circumference.CENTER, T, SlidePatternFactory.UN_USE_PARAM, target, slideVector); //例：C-A5
						}
					}
					else if (target == (start + 6) % 8) { 
						//再帰 例：B1-A7
						if (this.SetPattern(S, Circumference.INNER, start, (start + 7) % 8, slideVector)) { 
							//例：B1-B8
							this.SetPattern(Circumference.INNER, T, (start + 7) % 8, target, slideVector); //例：B8-A7
						}
					}
				}
				else {
					this.SetError(5); //曲線で別周侵入禁止
				}
			}
			else if (T == Circumference.INNER) { 
				//Bx->By
				//進み方によって分岐する
				if (slideVector == SlideVector.STRAIGHT) { 
					//同じボタンだからエラー
					if (target == (start + 0) % 8) { 
						this.SetError(1); //同じボタン
					}
					if (target == (start + 4) % 8) { 
						//例：B1-B5
						if (this.SetPattern(S, Circumference.CENTER, start, SlidePatternFactory.UN_USE_PARAM, slideVector)) { 
							//例：B1-C
							this.SetPattern(Circumference.CENTER, T, SlidePatternFactory.UN_USE_PARAM, target, slideVector); //例：C-B5
						}
					}
					else {
						this.AddPattern(S, T, start, target, slideVector);
					}
				}
				else {
					if (slideVector == SlideVector.CURVE_CLOCK) { 
						//進行方向隣なら確定
						if (target == (start + 1) % 8) { 
							this.AddPattern(S, T, start, target, slideVector);
						}
						//曲線 時計回り 再帰 例：A1^4
						else if (this.SetPattern(S, T, start, (start + 1) % 8, slideVector)) { 
							//例：B1^2
							this.SetPattern(S, T, (start + 1) % 8, target, slideVector); //例：B2^4
						}
					}
					else if (slideVector == SlideVector.CURVE_REVERSE_CLOCK) { 
						//進行方向隣なら確定
						if (target == (start + 7) % 8) { 
							this.AddPattern(S, T, start, target, slideVector);
						}
						//曲線 反時計回り 再帰 例：A1^6
						else if (this.SetPattern(S, T, start, (start + 7) % 8, slideVector)) { 
							//例：B1^8
							this.SetPattern(S, T, (start + 7) % 8, target, slideVector); //例：B8^6
						}
					}
				}
			}
			else if (T == Circumference.CENTER) { 
				//Bx->C
				if (slideVector == SlideVector.STRAIGHT) { 
					//Bx->C
					this.AddPattern(S, T, start, SlidePatternFactory.UN_USE_PARAM, slideVector);
				}
				else {
					this.SetError(5); //曲線で別周侵入禁止
				}
			}
		}
		else if (S == Circumference.CENTER) { 
			if (T == Circumference.OUTER) { 
				//再帰
				if (slideVector == SlideVector.STRAIGHT) { 
					//例：C-A5
					if (this.SetPattern(S, Circumference.INNER, SlidePatternFactory.UN_USE_PARAM, target, slideVector)) { 
						//例：C-B5
						this.SetPattern(Circumference.INNER, T, target, target, slideVector); //例：B5-A5
					}
				}
				else { 
					this.SetError(5); //曲線で別周侵入禁止
				}
			}
			else if (T == Circumference.INNER) { 
				if (slideVector == SlideVector.STRAIGHT) { 
					//C ->By
					this.AddPattern(S, T, SlidePatternFactory.UN_USE_PARAM, target, slideVector);
				}
				else { 
					this.SetError(5); //曲線で別周侵入禁止
				}
			}
			else if (T == Circumference.CENTER) { 
				this.SetError(1); //同じボタン
			}
		}
		return !this.CheckError();
	}
	
	public void AddPattern(Circumference S, Circumference T, int start, int target, SlideVector vec) {
		if (vec == SlideVector.STRAIGHT) { 
			this.AddStraightPattern(S, T, start, target);
		}
		else if (S != Circumference.CENTER && S == T) { 
			this.AddCurvePattern(S, start, target, vec);
		}
		else {
			this.SetError(5); //曲線で別周侵入禁止
		}
	}
	
	public void AddStraightPattern(Circumference S, Circumference T, int start, int target) {
		//初回ならstartの値を確認。
		if (this.pattern.Count == 0) { 
			if (S == Circumference.INNER || S == Circumference.CENTER) { 
				this.SetError(3); //スライドはAから始める
			}
		}
		
		var start_p = new Vector2(); //Sのstartのセンサー位置
		var target_p = new Vector2(); //Tのtargetのセンサー位置
		
		if (S == Circumference.OUTER) start_p = Constants.instance.GetOuterPieceAxis(start);
		else if (S == Circumference.INNER) start_p = Constants.instance.GetInnerPieceAxis(start);
		else if (S == Circumference.CENTER) start_p = Vector2.zero;
		if (T == Circumference.OUTER) target_p = Constants.instance.GetOuterPieceAxis(target);
		else if (T == Circumference.INNER) target_p = Constants.instance.GetInnerPieceAxis(target);
		else if (T == Circumference.CENTER) target_p = Vector2.zero;
		
		var note = MaimaiSlideCommand.CreateStraight(this.parent, this.uniqueId.ToString(), this.DefineActionId(T, target), start_p, target_p, this.totalDistance);
		this.pattern.Add(note);
		this.uniqueId++;
		this.totalDistance += note.GetDistance();
	}
	
	public void AddCurvePattern(Circumference c, int start, int target, SlideVector vec) {
		//初回ならstartの値を確認。.
		if (this.pattern.Count == 0) { 
			if (c == Circumference.INNER || c == Circumference.CENTER) { 
				this.SetError(3); //スライドはAから始める
			}
		}
		
		// 半径.
		var radius = c == Circumference.OUTER ? Constants.instance.MAIMAI_OUTER_RADIUS :
			c == Circumference.INNER ? Constants.instance.MAIMAI_INNER_RADIUS :
				0;
		// 距離(角度).
		float distanceDeg = 45.0f;
		// 曲線.
		if (vec == SlideVector.CURVE_CLOCK) {
			var note = MaimaiSlideCommand.CreateCurve(this.parent, this.uniqueId.ToString(), this.DefineActionId(c, target), Vector2.zero, radius, Constants.instance.GetPieceDegree(start), distanceDeg, 1, this.totalDistance);
			this.pattern.Add(note);
			this.uniqueId++;
			this.totalDistance += note.GetDistance();
		}
		else {
			var note = MaimaiSlideCommand.CreateCurve(this.parent, this.uniqueId.ToString(), this.DefineActionId(c, target), Vector2.zero, radius, Constants.instance.GetPieceDegree(start), distanceDeg, -1, this.totalDistance);
			this.pattern.Add(note);
			this.uniqueId++;
			this.totalDistance += note.GetDistance();
		}
	}
	
	public void SetError(int index) {
		switch (index) {
		case 1: this.errorMessage = "連続して同じセンサー番号は許可されていません。"; break;
		case 2: this.errorMessage = "曲線スライドで対角線上のセンサーは、方向を求めることができないため禁止です。"; break; //未使用
		case 3: this.errorMessage = "スライドは必ずAのセンサーから始めてください。"; break;
		case 4: this.errorMessage = "プログラムエラーです。アプリ開発者に問い合わせてください。"; break; //真ん中に1ボタンだけがあるわけではないらしい
		case 5: this.errorMessage = "曲線スライドで別の周に進むことはできません。"; break;
		default: this.errorMessage = ""; break;
		}
	}
	
	public bool CheckError() { 
		return !string.IsNullOrEmpty(this.errorMessage);
	}
	
	public static int ACTION_ID_CENTER { get { return 0; } }
	public static int ACTION_ID_INNER_BASE { get { return 1; } }
	public static int ACTION_ID_OUTER_BASE { get { return 9; } }
	public int DefineActionId(Circumference circ, int button) { 
		return DefineActionId (this.initActionId, circ, button);
	}
	public static int DefineActionId(int initActId, Circumference circ, int button) { 
		//アクションIDは、ファクトリー初期値 + センサー番号。中円は0、内周は1～8、外周は9～16
		var cic = circ == Circumference.CENTER ? ACTION_ID_CENTER : circ == Circumference.INNER ? ACTION_ID_INNER_BASE : ACTION_ID_OUTER_BASE;
		if (circ == Circumference.CENTER) button = 0;
		return initActId + cic + button;
	}
	
	public void Release() {
		this.pattern = null;
		this.parent = null;
	}

	// startとtargetは-1～1の範囲で指定 (左手座標系).
	public void AddStraightPattern(Vector2 start, Vector2 target, Circumference actionCircum, int actionButtonNumber) {
		// 右手座標系.
		var start_p = start * Constants.instance.MAIMAI_OUTER_RADIUS;
		var target_p = target * Constants.instance.MAIMAI_OUTER_RADIUS;
		
		var note = MaimaiSlideCommand.CreateStraight(this.parent, this.uniqueId.ToString(), this.DefineActionId(actionCircum, actionButtonNumber), start_p, target_p, this.totalDistance);
		this.pattern.Add(note);
		this.uniqueId++;
		this.totalDistance += note.GetDistance();
	}
	
	// centerは-1～1の範囲で指定 (左手座標系). radiusは0～1の範囲.
	public void AddCurvePattern(Vector2 center, float radius, float startDeg, float distanceDeg, Circumference actionCircum, int actionButtonNumber) {
		// 右手座標系.
		var center_p = center * Constants.instance.MAIMAI_OUTER_RADIUS;
		radius = Mathf.Abs(radius * Constants.instance.MAIMAI_OUTER_RADIUS);

		// 曲線.
		float vec = distanceDeg < 0 ? -1 : 1;
		var note = MaimaiSlideCommand.CreateCurve(this.parent, this.uniqueId.ToString(), this.DefineActionId(actionCircum, actionButtonNumber), center_p, radius, startDeg, Mathf.Abs(distanceDeg), vec, this.totalDistance);
		this.pattern.Add(note);
		this.uniqueId++;
		this.totalDistance += note.GetDistance();
	}
	
	// centerは-1～1の範囲で指定. radiusは0～1の範囲.
	public void AddCurvePattern(Vector2 center, Vector2 radius, float startDeg, float distanceDeg, Circumference actionCircum, int actionButtonNumber) {
		// 右手座標系.
		var center_p = center * Constants.instance.MAIMAI_OUTER_RADIUS;
		var radius_p = new Vector2 (Mathf.Abs (radius.x * Constants.instance.MAIMAI_OUTER_RADIUS), Mathf.Abs (radius.y * Constants.instance.MAIMAI_OUTER_RADIUS));
		
		// 曲線.
		float vec = distanceDeg < 0 ? -1 : 1;
		var note = MaimaiSlideCommand.CreateCurve(this.parent, this.uniqueId.ToString(), this.DefineActionId(actionCircum, actionButtonNumber), center_p, radius_p, startDeg, Mathf.Abs(distanceDeg), vec, this.totalDistance);
		this.pattern.Add(note);
		this.uniqueId++;
		this.totalDistance += note.GetDistance();
	}
*/

}
