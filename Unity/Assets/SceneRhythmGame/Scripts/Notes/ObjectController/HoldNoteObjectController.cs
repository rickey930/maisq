﻿using UnityEngine;
using System.Collections;

public class HoldNoteObjectController : NoteObjectControllerBase {

	[SerializeField]
	private Transform rotateHierarchy;
	[SerializeField]
	private Transform headTimeMoveHierarchy;
	[SerializeField]
	private Transform headTimeScaleHierarchy;
	[SerializeField]
	private Transform footTimeMoveHierarchy;
	[SerializeField]
	private Transform footTimeScaleHierarchy;
	[SerializeField]
	private Transform bodyTimeMoveHierarchy;
	[SerializeField]
	private Transform bodyTimeScaleHierarchy;
	[SerializeField]
	private Transform arcTimeScaleHierarchy;
	
	[SerializeField]
	private SpriteRenderer headNoteSpriteRenderer;
	[SerializeField]
	private SpriteRenderer headCenterSpriteRenderer;
	[SerializeField]
	private SpriteRenderer footNoteSpriteRenderer;
	[SerializeField]
	private SpriteRenderer footCenterSpriteRenderer;
	[SerializeField]
	private SpriteRenderer bodyNoteSpriteRenderer;
	[SerializeField]
	private SpriteRenderer arcSpriteRenderer;
	
	public MaimaiHoldHeadNote holdHeadNoteInfo { get; set; }

	protected bool eachArcSpriteChanged { get; set; }
	
	// Use this for initialization
	protected override void Start () {
		base.Start ();
		holdHeadNoteInfo = noteInfo as MaimaiHoldHeadNote;
		IMaimaiEachNote eachInfo = noteInfo as IMaimaiEachNote;
		
		SetSortingOrder (footNoteSpriteRenderer, 5);
		SetSortingOrder (footCenterSpriteRenderer, 4);
		SetSortingOrder (headNoteSpriteRenderer, 3);
		SetSortingOrder (headCenterSpriteRenderer, 2);
		SetSortingOrder (bodyNoteSpriteRenderer, 1);
		SetSortingOrder (arcSpriteRenderer, 0);

		rotateHierarchy.localRotation = Quaternion.Euler(new Vector3(0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(holdHeadNoteInfo.GetImageDegree())));
		Vector3 iniScale = new Vector3 (0, 0, 1);
		headTimeScaleHierarchy.localScale = iniScale;
		footTimeScaleHierarchy.localScale = iniScale;
		bodyTimeScaleHierarchy.localScale = new Vector3 (1, 0, 1);;
		arcTimeScaleHierarchy.localScale = iniScale;
		
		if (eachInfo != null && eachInfo.GetEachNotes ().Count > 0) {
			if (objectInfo.angulatedHold) {
				headNoteSpriteRenderer.sprite = SpriteTank.Get ("note_hold_each_corn_head");
				footNoteSpriteRenderer.sprite = SpriteTank.Get ("note_hold_each_corn_foot");
			}
			else {
				headNoteSpriteRenderer.sprite = SpriteTank.Get ("note_hold_each_arc_head");
				footNoteSpriteRenderer.sprite = SpriteTank.Get ("note_hold_each_arc_foot");
			}
			headCenterSpriteRenderer.sprite = SpriteTank.Get ("center_yellow");
			footCenterSpriteRenderer.sprite = SpriteTank.Get ("center_yellow");
			bodyNoteSpriteRenderer.sprite = SpriteTank.Get ("note_hold_each_body");
			arcSpriteRenderer.sprite = GetEachOval(holdHeadNoteInfo);
		}
		else {
			if (objectInfo.angulatedHold) {
				headNoteSpriteRenderer.sprite = SpriteTank.Get ("note_hold_corn_head");
				footNoteSpriteRenderer.sprite = SpriteTank.Get ("note_hold_corn_foot");
			}
			else {
				headNoteSpriteRenderer.sprite = SpriteTank.Get ("note_hold_arc_head");
				footNoteSpriteRenderer.sprite = SpriteTank.Get ("note_hold_arc_foot");
			}
			headCenterSpriteRenderer.sprite = SpriteTank.Get ("center_pink");
			footCenterSpriteRenderer.sprite = SpriteTank.Get ("center_pink");
			bodyNoteSpriteRenderer.sprite = SpriteTank.Get ("note_hold_body");
			arcSpriteRenderer.sprite = MaipadDynamicCreatedSpriteTank.instance.tapArc;
		}
		
		footCenterSpriteRenderer.gameObject.SetActive (false);
	}
	
	protected override void Move() {
		float fadePercent = CalcTimePercent(holdHeadNoteInfo.justTime, objectInfo.guideSpeed, objectInfo.guideFadeSpeed);
		if (fadePercent < 0) fadePercent = 0;
		if (fadePercent > 1) fadePercent = 1;
		// ノートの不透明化.
		SetAlpha (fadePercent, headNoteSpriteRenderer, headCenterSpriteRenderer, footNoteSpriteRenderer, footCenterSpriteRenderer, bodyNoteSpriteRenderer, arcSpriteRenderer);
		
		// 【下半身の移動】.
		float footMovePercent = CalcTimePercent(holdHeadNoteInfo.relativeNote.justTime, objectInfo.guideMoveSpeed, objectInfo.guideMoveSpeed);
		if (footMovePercent < 0) footMovePercent = 0;
//		if (footMovePercent > 1) footMovePercent = 1;
		// ノートの拡大.
		footTimeScaleHierarchy.localScale = new Vector3 (fadePercent, fadePercent, 1);
		// ノートの移動.
		float fy = (footMovePercent * (Constants.instance.MAIMAI_OUTER_RADIUS - Constants.instance.NOTE_MOVE_START_MARGIN));
		footTimeMoveHierarchy.localPosition = new Vector3(0, fy, 0);
		// センターのちっこいの表示.
		if (footMovePercent > 0 && !footCenterSpriteRenderer.gameObject.activeSelf) footCenterSpriteRenderer.gameObject.SetActive (true);
		
		// 【上半身の移動】.
		float headMovePercent = CalcTimePercent(holdHeadNoteInfo.justTime, objectInfo.guideMoveSpeed, objectInfo.guideMoveSpeed);
		if (headMovePercent < 0) headMovePercent = 0;
		if (headMovePercent > 1) {
			if (footMovePercent < 1)
				headMovePercent = 1;
			//フットが目標値まで動き切ったら、マーカーの元の形(円)になるようにする。
			else
				headMovePercent = footMovePercent;
		}
		// ノートの拡大.
		headTimeScaleHierarchy.localScale = new Vector3 (fadePercent, fadePercent, 1);
		// ノートの移動.
		float hy = (headMovePercent * (Constants.instance.MAIMAI_OUTER_RADIUS - Constants.instance.NOTE_MOVE_START_MARGIN));
		headTimeMoveHierarchy.localPosition = new Vector3(0, hy, 0);
		
		// 【体の伸縮】.
		float optionScale = objectInfo.ringSize;
		float bodyScaleOffset = 1 / optionScale;
		float bodyHeight = hy - fy;
		bodyTimeScaleHierarchy.localScale = new Vector3 (1, bodyHeight * bodyScaleOffset, 1);
		bodyTimeMoveHierarchy.localPosition = new Vector3 (0, bodyHeight / 2.0f + fy, 0);
		
		// 【円弧の移動 (に見せかけた拡大)】.
		float arcInitScale = Constants.instance.NOTE_MOVE_START_MARGIN / Constants.instance.MAIMAI_OUTER_RADIUS;
		float arcRemainScale = 1.0f - arcInitScale;
		float arcPercent = headMovePercent * arcRemainScale + arcInitScale;
		arcTimeScaleHierarchy.localScale = new Vector3 (arcPercent, arcPercent, 1);

		// ヘッドがフットを待っている間、イーチの円弧は通常のものにする.
		if (!eachArcSpriteChanged && headMovePercent >= 1) {
			if (holdHeadNoteInfo.eachNotes.Count > 0) {
				arcSpriteRenderer.sprite = MaipadDynamicCreatedSpriteTank.instance.eachArc0;
			}
			eachArcSpriteChanged = true;
		}
	}
	
	protected override void Release () {
		if ((!noteInfo.visible && noteInfo.judged) || restartId != objectInfo.restartId) {
			Destroy (gameObject);
		}
	}

	protected override void OnDestroy () {
		base.OnDestroy ();
		holdHeadNoteInfo = null;
	}
}
