﻿using UnityEngine;
using System.Collections;

public class MessageNoteObjectController : MonoBehaviour {

	[SerializeField]
	private TextMesh textMesh;
	[SerializeField]
	private TimeAnimationAlpha timeAnimation;
	
	private MaimaiMessageNote noteInfo { get; set; }
	private NoteObjectCommonInfo objectInfo { get; set; }

	public void Setup (MaimaiMessageNote noteInfo, NoteObjectCommonInfo objectInfo) {
		this.noteInfo = noteInfo;
		this.objectInfo = objectInfo;
	}

	// Use this for initialization
	IEnumerator Start () {
		textMesh.text = string.Empty;
		if (noteInfo == null || objectInfo == null) yield break;
		while (noteInfo.justTime + objectInfo.judgeTimeLag > objectInfo.timer.gameTime) {
			yield return null;
		}
		textMesh.text = noteInfo.message;
		transform.localPosition = noteInfo.position;
		transform.localScale = new Vector3 (noteInfo.scale, noteInfo.scale, 1);
		textMesh.color = noteInfo.color;
		timeAnimation.GetData [0].time = ((float)noteInfo.visibleTime / 1000.0f);
		timeAnimation.enabled = true;
	}
}
