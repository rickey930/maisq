﻿using UnityEngine;
using System.Collections;

public class NoteObjectCommonInfo {
	public long guideFadeSpeed { get; set; }
	public long guideMoveSpeed { get; set; }
	public long guideSpeed { get { return guideMoveSpeed + guideFadeSpeed; } }
	public long judgeTimeLag { get; set; }
	public MaimaiTimer timer { get; set; }
	public bool starRotation { get; set; }
	public bool angulatedHold { get; set; }
	public bool pine { get; set; }
	public bool autoPlay { get; set; }
	public float ringSize { get; set; }
	public float markerSize { get; set; }
	public int restartId { get; set; }

	public NoteObjectCommonInfo() {
		
	}
	
	public NoteObjectCommonInfo(MaimaiTimer timer, long guideFadeSpeed, long guideMoveSpeed, long judgeTimeLag, bool starRotation, bool angulatedHold, bool pine, bool autoPlay, float ringSize, float markerSize) {
		this.timer = timer;
		this.guideFadeSpeed = guideFadeSpeed;
		this.guideMoveSpeed = guideMoveSpeed;
		this.judgeTimeLag = judgeTimeLag;
		this.starRotation = starRotation;
		this.angulatedHold = angulatedHold;
		this.pine = pine;
		this.autoPlay = autoPlay;
		this.ringSize = ringSize;
		this.markerSize = markerSize;
	}

	/*
	 * 	this.sensor = sensor;
	this.guideSpeed = guideSpeed;
	this.markerSize = markerSize;
	this.slideMarkerSizeAdaptation = slideMarkerSizeAdaptation;
	this.judgeTimer = judgeTimer;
	this.starRotation = starRotation;
	this.angulatedHold = angulatedHold;
	this.timeOffset = timeOffset;
	this.gameStartTime = (60.0 / firstBPM) * 4.0 + RhythmActionJudgeNote.START_COUNT_START_TIME;
	this.autoDeleteSlideMarker = autoDeleteSlideMarker;
	this.setting = null;
	this.config = null;*/
}
