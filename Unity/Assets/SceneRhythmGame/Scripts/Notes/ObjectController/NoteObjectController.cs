using UnityEngine;
using System.Collections;

public class NoteObjectController : NoteObjectControllerBase {

	[SerializeField]
	private Transform rotateHierarchy;
	[SerializeField]
	private Transform timeMoveHierarchy;
	[SerializeField]
	private Transform noteTimeScaleHierarchy;
	[SerializeField]
	private Transform arcTimeScaleHierarchy;
	
	[SerializeField]
	private SpriteRenderer noteSpriteRenderer;
	[SerializeField]
	private SpriteRenderer centerSpriteRenderer;
	[SerializeField]
	private SpriteRenderer arcSpriteRenderer;
	[SerializeField]
	private SpriteRenderer noteSprite2Renderer; //☆複数パターン用
	
	public MaimaiStarNote starNoteInfo { get; set; }
	private float[] starMoveSpeed { get; set; }

	// Use this for initialization
	protected override void Start () {
		base.Start ();
		if (noteInfo.GetNoteType() == MaimaiNoteType.TAP_STAR ||
		    noteInfo.GetNoteType() == MaimaiNoteType.BREAK_STAR) {
			starNoteInfo = noteInfo as MaimaiStarNote;

			starMoveSpeed = new float[starNoteInfo.relativeNotes.Count];
			for (int i = 0; i < starNoteInfo.relativeNotes.Count; i++) {
				float latest = float.MaxValue;
				foreach (var chain in starNoteInfo.relativeNotes[i].chains) {
					latest = Mathf.Min (latest, chain.onStarMoveSpeed);
				}
				starMoveSpeed[i] = latest;
			}
		}
		MaimaiTapNote tapNoteInfo = noteInfo as MaimaiTapNote;
		IMaimaiEachNote eachInfo = noteInfo as IMaimaiEachNote;
		
		SetSortingOrder (noteSpriteRenderer, 3);
		SetSortingOrder (noteSprite2Renderer, 2);
		SetSortingOrder (centerSpriteRenderer, 1);
		SetSortingOrder (arcSpriteRenderer, 0);

		rotateHierarchy.localRotation = Quaternion.Euler(new Vector3(0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(tapNoteInfo.GetImageDegree())));
		noteTimeScaleHierarchy.localScale = new Vector3 (0, 0, 1);
		arcTimeScaleHierarchy.localScale = new Vector3 (0, 0, 1);

		noteSprite2Renderer.gameObject.SetActive (false);

		switch (noteInfo.GetNoteType()) {
		case MaimaiNoteType.TAP_CIRCLE:
			if (eachInfo != null && eachInfo.GetEachNotes().Count > 0) {
				if (!objectInfo.pine) {
					noteSpriteRenderer.sprite = SpriteTank.Get ("note_tap_each");
				}
				else {
					noteSpriteRenderer.sprite = SpriteTank.Get ("pine_tap_each");
				}
				centerSpriteRenderer.sprite = SpriteTank.Get ("center_yellow");
				arcSpriteRenderer.sprite = GetEachOval(tapNoteInfo);
			}
			else {
				if (!objectInfo.pine) {
					noteSpriteRenderer.sprite = SpriteTank.Get ("note_tap");
				}
				else {
					noteSpriteRenderer.sprite = SpriteTank.Get ("pine_tap");
				}
				centerSpriteRenderer.sprite = SpriteTank.Get ("center_pink");
				arcSpriteRenderer.sprite = MaipadDynamicCreatedSpriteTank.instance.tapArc;
			}
			break;
		case MaimaiNoteType.TAP_STAR:
			if (eachInfo != null && eachInfo.GetEachNotes().Count > 0) {
				noteSpriteRenderer.sprite = SpriteTank.Get ("note_star_each");
				centerSpriteRenderer.sprite = SpriteTank.Get ("center_yellow");
				arcSpriteRenderer.sprite = GetEachOval(tapNoteInfo);
				if (starNoteInfo.relativeNotes.Count > 1) {
					noteSprite2Renderer.sprite = SpriteTank.Get ("note_star_each");
					noteSprite2Renderer.gameObject.SetActive (true);
				}
			}
			else {
				noteSpriteRenderer.sprite = SpriteTank.Get ("note_star");
				centerSpriteRenderer.sprite = SpriteTank.Get ("center_blue");
				arcSpriteRenderer.sprite = MaipadDynamicCreatedSpriteTank.instance.starArc;
				if (starNoteInfo.relativeNotes.Count > 1) {
					noteSprite2Renderer.sprite = SpriteTank.Get ("note_star");
					noteSprite2Renderer.gameObject.SetActive (true);
				}
			}
			break;
		case MaimaiNoteType.BREAK_CIRCLE:
			if (!objectInfo.pine) {
				noteSpriteRenderer.sprite = SpriteTank.Get ("note_break");
			}
			else {
				noteSpriteRenderer.sprite = SpriteTank.Get ("pine_break");
			}
			centerSpriteRenderer.sprite = SpriteTank.Get ("center_red");
			if (eachInfo != null && eachInfo.GetEachNotes().Count > 0) {
				arcSpriteRenderer.sprite = GetEachOval(tapNoteInfo);
			}
			else {
				arcSpriteRenderer.sprite = MaipadDynamicCreatedSpriteTank.instance.breakArc;
			}
			break;
		case MaimaiNoteType.BREAK_STAR:
			noteSpriteRenderer.sprite = SpriteTank.Get ("note_star_break");
			centerSpriteRenderer.sprite = SpriteTank.Get ("center_red");
			if (eachInfo != null && eachInfo.GetEachNotes().Count > 0) {
				arcSpriteRenderer.sprite = GetEachOval(tapNoteInfo);
			}
			else {
				arcSpriteRenderer.sprite = MaipadDynamicCreatedSpriteTank.instance.breakArc;
			}
			if (starNoteInfo.relativeNotes.Count > 1) {
				noteSprite2Renderer.sprite = SpriteTank.Get ("note_star_break");
				noteSprite2Renderer.gameObject.SetActive (true);
			}
			break;
		}
	}

	protected override void Move() {
		float fadePercent = CalcTimePercent(noteInfo.justTime, objectInfo.guideSpeed, objectInfo.guideFadeSpeed);
		float movePercent = CalcTimePercent(noteInfo.justTime, objectInfo.guideMoveSpeed, objectInfo.guideMoveSpeed);
		if (movePercent < 0) movePercent = 0;
//		if (movePercent > 1) movePercent = 1;
		if (fadePercent < 0) fadePercent = 0;
		if (fadePercent > 1) fadePercent = 1;
		// ノートの不透明化.
		SetAlpha (fadePercent, noteSpriteRenderer, arcSpriteRenderer, centerSpriteRenderer);
		// ノートの拡大
		noteTimeScaleHierarchy.localScale = new Vector3 (fadePercent, fadePercent, 1);
		// ノートの移動
		float y = (movePercent * (Constants.instance.MAIMAI_OUTER_RADIUS - Constants.instance.NOTE_MOVE_START_MARGIN));
		timeMoveHierarchy.localPosition = new Vector3(0, y, 0);
		
		// 円弧の移動 (に見せかけた拡大)
		float arcInitScale = Constants.instance.NOTE_MOVE_START_MARGIN / Constants.instance.MAIMAI_OUTER_RADIUS;
		float arcRemainScale = 1.0f - arcInitScale;
		float arcPercent = movePercent * arcRemainScale + arcInitScale;
		arcTimeScaleHierarchy.localScale = new Vector3 (arcPercent, arcPercent, 1);
		
		// スターノートの回転.
		StarRotation();
	}
	
	private void StarRotation() {
		if (objectInfo.starRotation && starNoteInfo != null && !objectInfo.timer.pause) {
			for (int i = 0; i < starMoveSpeed.Length; i++) {
				noteSpriteRenderer.gameObject.transform.Rotate(new Vector3(0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(starMoveSpeed[i] * 22.5f)));
			}
		}
	}

	protected override void OnDestroy () {
		base.OnDestroy ();
		starNoteInfo = null;
	}
}
