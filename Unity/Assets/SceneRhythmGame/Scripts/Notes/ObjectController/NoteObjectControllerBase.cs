using UnityEngine;
using System.Collections;

public abstract class NoteObjectControllerBase : MonoBehaviour {

	// Instantiateで作ったら、このクラスをGetComponentで取得して以下3項目を必ず設定すること.
	public MaimaiNote noteInfo { get; set; }
	public NoteObjectCommonInfo objectInfo { get; set; }
	/// <summary>
	/// 最初のノートほど数値が大きい(手前に描画される).
	/// </summary>
	public int zBuffer { get; set; }
	
	[SerializeField]
	private NoteScaleInitializer[] noteScaleInitializers;

	protected int restartId { get; set; }
	
	// Use this for initialization
	protected virtual void Start () {
		if (noteScaleInitializers != null) {
			foreach (var s in noteScaleInitializers)  {
				s.Setup (objectInfo.ringSize);
			}
		}
		restartId = objectInfo.restartId;
	}
	
	// Update is called once per frame
	protected virtual void Update () {
		Move ();
		Release ();
	}
	
	protected abstract void Move();
	
	/// <summary>
	/// 「判定時間」「その何秒前から表示」「何秒かけて移動」を指定して移動量パーセントを求める.
	/// </summary>
	protected float CalcTimePercent(long justTime, long timeBefore, long timeRange) {
		long startTime = (justTime + objectInfo.judgeTimeLag) - timeBefore;
		return (float)(objectInfo.timer.gameTime - startTime) / (float)timeRange;
	}
	
	protected void SetSortingOrder(SpriteRenderer renderer, int priority) {
		renderer.sortingOrder = zBuffer * 10 + priority;
	}
	
	protected virtual void Release () {
		if ((!noteInfo.visible && noteInfo.judged) || restartId != objectInfo.restartId) {
			Destroy (gameObject);
		}
	}

	protected void SetAlpha(float fade, params SpriteRenderer[] renderers) {
		foreach (var renderer in renderers) {
			if (renderer.sharedMaterial.HasProperty ("_Color")) {
				Color temp = renderer.sharedMaterial.GetColor ("_Color");
				temp.a = fade;
				renderer.material.SetColor ("_Color", temp);
			}
			else {
				var color = renderer.color;
				color.a = fade;
				renderer.color = color;
			}
		}
	}
	
	protected virtual void OnDestroy() {
		noteInfo = null;
		objectInfo = null;
	}

	protected Sprite GetEachOval(MaimaiTapNote note) {
		bool isBreak = false;
		foreach (var each in note.eachNotes) {
			var tap = each as MaimaiTapNote;
			if (tap.createdEachOval) {
				return null;
			}
			// 1つでもBREAKなら赤.
			if (tap.GetNoteType() == MaimaiNoteType.BREAK_CIRCLE ||
			    tap.GetNoteType() == MaimaiNoteType.BREAK_STAR) {
				isBreak = true;
			}
		}
		int find = -1;
		// +4, +3, +2, +1, +0, のいずれか分だけ離れていて.
		for (int i = 4; i >= 0; i--) {
			foreach (var each in note.eachNotes) {
				int button1 = note.GetButtonId();
				int button2 = ((MaimaiTapNote)each).GetButtonId();
				if ((button1 + i) % 8 == button2) {
					find = i;
					break;
				}
			}
			if (find >= 0) break;
		}
		if (find >= 0 && find < 4) {
			bool outbreak = false;
			// -3, -2, -1, のいずれか分にはない.
			foreach (var each in note.eachNotes) {
				int button1 = note.GetButtonId();
				int button2 = ((MaimaiTapNote)each).GetButtonId();
				for (int i = 5; i < 8; i++) {
					if ((button1 + i) % 8 == button2) {
						// ただし左右に3ずつ離れてたら4にする.
						if (i == 5 && 8 - i == find) {
							find = 4;
							outbreak = true;
							break;
						}
						else {
							find = -1;
							outbreak = true;
							break;
						}
					}
				}
				if (outbreak) break;
			}
		}
		Sprite ret = null;
		if (find == 0) {
			if (!isBreak) {
				ret = MaipadDynamicCreatedSpriteTank.instance.eachArc0;
			}
			else {
				ret = MaipadDynamicCreatedSpriteTank.instance.breakArc;
			}
		}
		else if (find == 1) {
			if (!isBreak) {
				ret = MaipadDynamicCreatedSpriteTank.instance.eachArc1;
			}
			else {
				ret = MaipadDynamicCreatedSpriteTank.instance.breakEachArc1;
			}
		}
		else if (find == 2) {
			if (!isBreak) {
				ret = MaipadDynamicCreatedSpriteTank.instance.eachArc2;
			}
			else {
				ret = MaipadDynamicCreatedSpriteTank.instance.breakEachArc2;
			}
		}
		else if (find == 3) {
			if (!isBreak) {
				ret = MaipadDynamicCreatedSpriteTank.instance.eachArc3;
			}
			else {
				ret = MaipadDynamicCreatedSpriteTank.instance.breakEachArc3;
			}
		}
		else if (find == 4) {
			if (!isBreak) {
				ret = MaipadDynamicCreatedSpriteTank.instance.eachArc4;
			}
			else {
				ret = MaipadDynamicCreatedSpriteTank.instance.breakEachArc4;
			}
		}
		
		if (ret != null) {
			foreach (var each in note.eachNotes) {
				var tap = each as MaimaiTapNote;
				tap.createdEachOval = true;
			}
		}
		return ret;
	}
}
