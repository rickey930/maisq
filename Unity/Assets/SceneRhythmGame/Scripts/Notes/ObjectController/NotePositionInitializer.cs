﻿using UnityEngine;
using System.Collections;

public class NotePositionInitializer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		transform.localPosition = new Vector3 (0, Constants.instance.NOTE_MOVE_START_MARGIN, 0);
	}

}
