﻿using UnityEngine;
using System.Collections;

public class NoteScaleInitializer : MonoBehaviour {

	// Use this for initialization
	public void Setup (float ringSize) {
		float optionScale = ringSize;
		transform.localScale = new Vector3 (optionScale, optionScale, 1);
	}
}
