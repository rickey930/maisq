﻿using UnityEngine;
using System.Collections;

public class ScrollMessageNoteObjectController : MonoBehaviour {
	
	[SerializeField]
	private TextMesh textMesh;
	[SerializeField]
	private TimeAnimationTranslate timeAnimation;
	
	private MaimaiScrollMessageNote noteInfo { get; set; }
	private NoteObjectCommonInfo objectInfo { get; set; }
	
	public void Setup (MaimaiScrollMessageNote noteInfo, NoteObjectCommonInfo objectInfo) {
		this.noteInfo = noteInfo;
		this.objectInfo = objectInfo;
	}
	
	// Use this for initialization
	IEnumerator Start () {
		textMesh.text = string.Empty;
		if (noteInfo == null || objectInfo == null) yield break;
		while (noteInfo.justTime + objectInfo.judgeTimeLag > objectInfo.timer.gameTime) {
			yield return null;
		}
		textMesh.text = noteInfo.message;
		textMesh.anchor = TextAnchor.MiddleLeft;
		textMesh.alignment = TextAlignment.Left;
		transform.localPosition = new Vector3(192, noteInfo.y, 0);
		transform.localScale = new Vector3 (noteInfo.scale, noteInfo.scale, 1);
		textMesh.color = noteInfo.color;
		timeAnimation.GetData [0].time = ((float)noteInfo.visibleTime / 1000.0f);
		timeAnimation.GetData [0].start.x = 192;
		timeAnimation.GetData [0].start.y = noteInfo.y;
		timeAnimation.GetData [0].target.x = -192 - (textMesh.characterSize * textMesh.text.Length * noteInfo.scale * 2);
		timeAnimation.GetData [0].target.y = noteInfo.y;
		timeAnimation.enabled = true;
	}
}
