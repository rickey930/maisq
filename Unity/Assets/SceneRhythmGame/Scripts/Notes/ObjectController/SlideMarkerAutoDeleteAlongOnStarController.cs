﻿using UnityEngine;
using System.Collections;

public class SlideMarkerAutoDeleteAlongOnStarController : MonoBehaviour {
	public NoteScaleInitializer markerScaleInitializer;
	public SliderStarObjectController manager { get; set; }
	public float totalDistanceUntilMoveStart { get; set; }
	
	// Update is called once per frame
	void Update () {
		// オートプレーだったら.
		if (manager.objectInfo.autoPlay) {
			// ☆の移動に合わせてスライドマーカーを非表示にする.
			if (manager.onStarMovedDistance > totalDistanceUntilMoveStart) {
				gameObject.SetActive(false);
			}
		}
		else {
			// スライドセンサーを処理した割合に合わせてスライドマーカーを消す.
			if (manager.chainInfo.pattern.progressRate * manager.chainInfo.pattern.GetLongestChainTotalDistance() > totalDistanceUntilMoveStart) {
				gameObject.SetActive(false);
				//Destroy (gameObject);
			}
		}
	}

	void OnDestroy() {
		manager = null;
	}
}
