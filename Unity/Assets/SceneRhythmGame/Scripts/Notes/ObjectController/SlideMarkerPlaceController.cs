﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SlideMarkerPlaceController : NoteObjectControllerBase {

	[SerializeField]
	private GameObject markerPrefab;
	[SerializeField]
	private GameObject chainMarkerPrefab;
	[SerializeField]
	private GameObject starObjectPrefab;
	[SerializeField]
	private Shader chainSlideMarkerShader;

	public MaimaiSlidePattern slidePatternInfo { get; set; }

	private SpriteRenderer[] slideMarkerAllRenderer;
	private Material[] chainSlideMarkerAllMaterial;
	private SliderStarObjectController[] starObjCtrlList;

	protected override void Start () {
		base.Start ();
		var headNoteInfo = (IMaimaiSlideNoteHead)noteInfo;
		var _slideMarkerAllRenderer = new List<SpriteRenderer> ();
		var _chainSlideMarkerAllMaterial = new List<Material> ();
		var _starObjCtrlList = new List<SliderStarObjectController> (); 

		// == ☆オブジェクト ==

		foreach (var chain in slidePatternInfo.chains) {
			var starObj = Instantiate (starObjectPrefab) as GameObject;
			starObj.SetParentEx (transform);
			var ctrl = starObj.GetComponent<SliderStarObjectController>();
			ctrl.headNoteInfo = headNoteInfo;
			ctrl.chainInfo = chain;
			ctrl.objectInfo = objectInfo;
			ctrl.starMoveDistance = chain.GetTotalDistance();
			ctrl.zBuffer = zBuffer;
			_starObjCtrlList.Add (ctrl);
		}

		// == スライドマーカー ==

		Vector2 drawloc;
		//画像の大きさ変更に伴う距離の変更のためのスケールサイズ取得.
		float scale = objectInfo.markerSize;
		//スライドマーカーを☆の移動に合わせて消す. Unity版ではSlideMarkerAutoDeleteAlongOnStarControllerクラスに任せた.
		float autoMarkerDeleteDistance = 0;
//		if (this.isAutoDeleteSlideMarker()) {
//			autoMarkerDeleteDistance = this.onStarMovedDistance;
//		}

		if (slidePatternInfo.chains.Length == 1) {
			var chain = slidePatternInfo.chains[0];

			Sprite sprite;
			
			if (slidePatternInfo.GetEachNotes ().Count > 0) {
				sprite = SpriteTank.Get("note_slide_each");
			}
			else {
				sprite = SpriteTank.Get("note_slide");
			}

			GameObject markerObj = null;

			//スライド先の方から描画.
			var distance = chain.GetTotalDistance() - (Constants.instance.SLIDE_MARKER_TO_SENSOR_MARGIN * scale) * 2; // 始点・終点分で*2.
			int zBuffer = 0;
			for (var i = chain.commands.Length - 1; i >= 0; i--) {
				var command = chain.commands[i];
				
				var onStarToTotalDistance = autoMarkerDeleteDistance - command.GetTotalDistanceOfUntilThisMoveStart(); //スライドマーカーを☆の移動に合わせて消す. //Unity版では別のに任せてるから意味がない？.
				if (onStarToTotalDistance < 0) onStarToTotalDistance = 0;

				while (distance >= command.GetTotalDistanceOfUntilThisMoveStart() + onStarToTotalDistance) {
					if (i == 0 && distance < command.GetTotalDistanceOfUntilThisMoveStart() + Constants.instance.SLIDE_MARKER_TO_SENSOR_MARGIN * scale) {
						//始点でマージン以下なら描くのやめる.
						break;
					}
					var pDistance = distance - command.GetTotalDistanceOfUntilThisMoveStart(); //パターンからの距離.
					if (command.GetCommandType() == MaimaiSlideCommandType.STRAIGHT) { 
						//直線.
						//2点間の角度を求めて距離を半径にして移動する.
						drawloc = CircleCalculator.PointToPointMovePoint(command.startPos, command.targetPos, pDistance);

						// 前ループのスライドマーカーを今回生成するマーカーの反対方向に向かせる (スライド先から描くから、反対方向).
						if (markerObj != null) {
							var oldloc = Constants.instance.ToLeftHandedCoordinateSystemPosition(markerObj.transform.localPosition);
							float deg = CircleCalculator.PointToDegree(oldloc, drawloc);
							markerObj.transform.localRotation = Quaternion.Euler(0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(deg + 180));
						}

						markerObj = Instantiate(markerPrefab) as GameObject;
						markerObj.SetParentEx (gameObject);
						markerObj.transform.localPosition = Constants.instance.ToLeftHandedCoordinateSystemPosition(drawloc);
						markerObj.transform.localRotation = Quaternion.Euler(0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(command.GetImageDegree()));
						var autoDeleteManager = markerObj.GetComponent<SlideMarkerAutoDeleteAlongOnStarController>();
						autoDeleteManager.manager = _starObjCtrlList[0];
						autoDeleteManager.totalDistanceUntilMoveStart = distance;
						autoDeleteManager.markerScaleInitializer.Setup (objectInfo.markerSize);
						SpriteRenderer renderer = markerObj.GetComponentInChildren<SpriteRenderer>();
						_slideMarkerAllRenderer.Add (renderer);
						renderer.sprite = sprite;
						renderer.sortingOrder = this.zBuffer * 1000 + zBuffer;
//						var args = [this.markerImage];
//						TechnicalDraw.scaleDraw_ps(canvas, drawloc, scale, function() {
//							TechnicalDraw.rotateDrawBitmap_xy(canvas, args[0], 0, 0, sp.getImageDegree() - 90.0, fade);
//						});
					}
					else {
						var cc = command as MaimaiSlideCurveCommand;
						//曲線.
						//円の中心(getCenterAxis())、外周/内周の半径(getOuterAxis()/getInnerAxis())、方向(sp.vec)、開始角度 から、pDistance角度まで移動して、その地点.
						//drawloc = //円の中心と周の半径と開始角度は引数、pDistanceと方向は掛けてひとつの引数。.
						//drawlocの位置にsp.dStartDeg回転して(0度のとき上向きなので90度回転する)、vecが-1なら反転して(180度回転でもいいかも)、描画。.
						var pDeg = CircleCalculator.ArcDegreeFromArcDistance(Mathf.Max(cc.radius.x, cc.radius.y), pDistance);
						//drawloc = CircleCalculator.PointOnCircle(sp.curve_centerPos, sp.curve_radius, sp.curve_dStartDeg + pDeg * sp.curve_vec);
						drawloc = CircleCalculator.PointOnEllipse(cc.centerPos, cc.radius.x, cc.radius.y, cc.startDeg + pDeg * cc.vec);
						
						// 前ループのスライドマーカーを今回生成するマーカーの反対方向に向かせる (スライド先から描くから、反対方向).
						if (markerObj != null) {
							var oldloc = Constants.instance.ToLeftHandedCoordinateSystemPosition(markerObj.transform.localPosition);
							float deg = CircleCalculator.PointToDegree(oldloc, drawloc);
							markerObj.transform.localRotation = Quaternion.Euler(0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(deg + 180));
						}

						markerObj = Instantiate(markerPrefab) as GameObject;
						markerObj.SetParentEx (gameObject);
						markerObj.transform.localPosition = Constants.instance.ToLeftHandedCoordinateSystemPosition(drawloc);
						markerObj.transform.localRotation = Quaternion.Euler(0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree((cc.startDeg + 90.0f + pDeg * cc.vec + (cc.vec > 0 ? 0 : 180))));
						var autoDeleteManager = markerObj.GetComponent<SlideMarkerAutoDeleteAlongOnStarController>();
						autoDeleteManager.manager = _starObjCtrlList[0];
						autoDeleteManager.totalDistanceUntilMoveStart = distance;
						autoDeleteManager.markerScaleInitializer.Setup (objectInfo.markerSize);
						SpriteRenderer renderer = markerObj.GetComponentInChildren<SpriteRenderer>();
						_slideMarkerAllRenderer.Add (renderer);
						renderer.sprite = sprite;
						renderer.sortingOrder = this.zBuffer * 1000 + zBuffer;
//						var args = [this.markerImage];
//						TechnicalDraw.scaleDraw_ps(canvas, drawloc, scale, function() {
//							TechnicalDraw.rotateDrawBitmap_xy(canvas, args[0], 0, 0, sp.getStartDegree() + pDeg * sp.getVector() + (sp.getVector() == 1 ? 0 : 180), fade);
//						});
					}
					distance -= Constants.instance.SLIDE_MARKER_TO_MARKER_INTERVAL * scale;
					zBuffer++;
				}
			}
		}
		else {
			// チェインが複数あるとき.


			int vertexAmount = 10; // 頂点数(分割数).
			var chainsPos = new Vector2[slidePatternInfo.chains.Length, vertexAmount];
			var chainsDistance = new float[slidePatternInfo.chains.Length, vertexAmount];

			GameObject markerObj = null;
			int zBuffer = 0;
			int chainCount = 0;

			foreach (var chain in slidePatternInfo.chains) {
				var distance = chain.GetTotalDistance() - (Constants.instance.SLIDE_MARKER_TO_SENSOR_MARGIN * scale) * 2; // 始点・終点分で*2.
				float progressAmount = distance / (float)vertexAmount;
				int vertexCount = 0;

				for (var i = chain.commands.Length - 1; i >= 0; i--) {
					var command = chain.commands[i];
					while (distance >= command.GetTotalDistanceOfUntilThisMoveStart()) {
						if (vertexCount >= vertexAmount) {
							break;
						}
						var pDistance = distance - command.GetTotalDistanceOfUntilThisMoveStart(); //パターンからの距離.
						if (command.GetCommandType() == MaimaiSlideCommandType.STRAIGHT) { 
							//直線.
							//2点間の角度を求めて距離を半径にして移動する.
							drawloc = CircleCalculator.PointToPointMovePoint(command.startPos, command.targetPos, pDistance);
							chainsPos[chainCount, vertexCount] = drawloc;
						}
						else {
							var cc = command as MaimaiSlideCurveCommand;
							//曲線.
							var pDeg = CircleCalculator.ArcDegreeFromArcDistance(Mathf.Max(cc.radius.x, cc.radius.y), pDistance);
							drawloc = CircleCalculator.PointOnEllipse(cc.centerPos, cc.radius.x, cc.radius.y, cc.startDeg + pDeg * cc.vec);
							chainsPos[chainCount, vertexCount] = drawloc;
						}
						chainsDistance[chainCount, vertexCount] = distance;
						distance -= progressAmount;
						vertexCount++;
					}
				}
				chainCount++;
			}

			int chainsLength = chainsPos.GetLength(0);
			int vertexsLength = chainsPos.GetLength(1);
			Mesh chainMesh = SlideMeshCreater.Create (chainsLength);
			var material = new Material (chainSlideMarkerShader);
			Sprite sprite;
			if (slidePatternInfo.GetEachNotes ().Count > 0) {
				sprite = SpriteTank.Get("note_slide_chain_each");
			}
			else {
				sprite = SpriteTank.Get("note_slide_chain");
			}
			material.mainTexture = sprite.texture;

			for (int j = 0; j < vertexsLength; j++) {
				float mostLengthDistance = 0;
				SliderStarObjectController star = null;
				for (int i = 0; i < chainsLength; i++) {
					if (mostLengthDistance < chainsDistance[i, j]) {
						mostLengthDistance = chainsDistance[i, j];
						star = _starObjCtrlList[i];
					}
				}
				markerObj = Instantiate(chainMarkerPrefab) as GameObject;
				markerObj.SetParentEx (gameObject);
				var filter = markerObj.GetComponentInChildren<MeshFilter>();
				filter.sharedMesh = chainMesh;
				float chainSlideMarkerSize = 16 * scale; //チェインスライド画像の縦の大きさが16.
				var vertices = filter.mesh.vertices;
				for (int i = 0; i < chainsLength; i++) {
					MaimaiSlideCommand comdata = null;
					// 描画しようとしているマーカーの位置を管理しているスライドコマンドを取得.
					foreach (var command in slidePatternInfo.chains[i].commands) {
						if (command.GetTotalDistanceOfUntilThisMoveStart() < chainsDistance[i, j]) {
							comdata = command;
						}
					}
					if (comdata == null) continue;
					float angle = 0;
					// スライドコマンドがストレートならスライドコマンドの始点を見て、それを角度とする.
					if (comdata.GetCommandType() == MaimaiSlideCommandType.STRAIGHT || j >= vertexsLength - 1) {
						angle = CircleCalculator.PointToDegree (chainsPos[i, j], comdata.startPos);
					}
					// スライドコマンドがカーブでインデックスオーバーしないなら始点がある方向で一番近いマーカーの位置を見て、それを角度とする.
					else {
						angle = CircleCalculator.PointToDegree (chainsPos[i, j], chainsPos[i, j + 1]);
					}
					// 上.
					var up = CircleCalculator.PointOnCircle(chainsPos[i, j], chainSlideMarkerSize / 2, angle).ChangePosHandSystem();
					vertices[i].x = up.x;
					vertices[i].y = up.y;
					// 下.
					var down = CircleCalculator.PointOnCircle(chainsPos[i, j], chainSlideMarkerSize / 2, angle + 180).ChangePosHandSystem();
					vertices[vertices.Length - 1 - i].x = down.x;
					vertices[vertices.Length - 1 - i].y = down.y;
				}
				filter.mesh.vertices = vertices;
				// その他の情報.
				var autoDeleteManager = markerObj.GetComponent<SlideMarkerAutoDeleteAlongOnStarController>();
				autoDeleteManager.manager = star;
				autoDeleteManager.totalDistanceUntilMoveStart = mostLengthDistance;
				var renderer = markerObj.GetComponentInChildren<MeshRenderer>();
				renderer.sharedMaterial = material;
				_chainSlideMarkerAllMaterial.Add (renderer.material);
				zBuffer++;
			}
		}


		slideMarkerAllRenderer = _slideMarkerAllRenderer.ToArray();
		chainSlideMarkerAllMaterial = _chainSlideMarkerAllMaterial.ToArray ();
		starObjCtrlList = _starObjCtrlList.ToArray ();
		_slideMarkerAllRenderer.Clear();
		_chainSlideMarkerAllMaterial.Clear();
		_starObjCtrlList.Clear ();
		SetAlpha(0, slideMarkerAllRenderer);
		SetAlpha(0, chainSlideMarkerAllMaterial);
	}

	protected override void Move () {
		var chainInfo = slidePatternInfo.chains[0];
		float fadePercent = CalcTimePercent(chainInfo.pattern.relativeNote.GetSlideReadyTime(), objectInfo.guideSpeed, objectInfo.guideSpeed);
		if (fadePercent < 0) fadePercent = 0;
		if (fadePercent > 1) fadePercent = 1;
		// ノートの不透明化.
		SetAlpha(fadePercent, slideMarkerAllRenderer);
		SetAlpha(fadePercent, chainSlideMarkerAllMaterial);
	}

	protected override void Release () {
		if (slidePatternInfo.IsEvaluated || restartId != objectInfo.restartId) {
			foreach (var star in starObjCtrlList) {
				if (star != null) {
					star.visible = false;
				}
			}
			if (gameObject != null) {
				Destroy (gameObject);
			}
		}
	}

	protected override void OnDestroy () {
		base.OnDestroy ();
		slidePatternInfo = null;
		slideMarkerAllRenderer = null;
	}

	// マテリアルのシェーダーに_Colorというプロパティがあること前提.
	protected void SetAlpha(float fade, params Material[] materials) {
		foreach (var material in materials) {
			var color = material.GetColor ("_Color");
			color.a = fade;
			material.SetColor ("_Color", color);
		}
	}
}
