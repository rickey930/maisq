﻿using UnityEngine;
using System.Collections;

public class SlideMeshCreater {
	public static Mesh Create (int chainCount) {
		if (chainCount == 0) return null;
		// メッシュに使う画像のサイズ.
		float width = 1.0f;
		float height = 16.0f;
		// 横方向の頂点数.
		int holizontalVertexAmount = chainCount;
		// メッシュに使う画像のpovit (0.5が中心)
		float centerXRate = 0.0f;
		float centerYRate = 0.5f;
		
		// 回転方向 (メッシュを作るときに(つまりここで)いじるべきではない)
		float vec = 1;
		
		const int verticalVertexAmount = 2;
		
		Mesh mesh = new Mesh();
		Vector3 [] newVertices = new Vector3[holizontalVertexAmount * verticalVertexAmount];
		Vector2 [] newUV       = new Vector2[holizontalVertexAmount * verticalVertexAmount];
		// 三角形の頂点数は3で、四角形を作るには三角形が2つ必要で、四角形が何個あるかで掛け算する.
		// 四角形用の頂点が2*2で三角形用の頂点が6個. 3*2で12個. 4*2で18個. 3*3で24個. 4*4で36個
		// 頂点数-1個の四角形が作れる.
		int[] newTriangles     = new int[3 * 2 * (holizontalVertexAmount - 1) * (verticalVertexAmount - 1)];
		
		// 鏡コの字を書くように頂点を配置する.
		
		// 頂点とUVを作成.
		for (int h = 0; h < holizontalVertexAmount; h++) {
			float progress = (float)h / (float)(holizontalVertexAmount - 1);
			int index = h;
			newVertices[index] = new Vector3(((width * progress) + (width * centerXRate)) * vec, -(height * centerYRate)).ChangePosHandSystem();
			// vecがマイナスのとき文字が鏡になるのを避けるためuvのy値を反転して回転しているように見せているけど別の画像用意すべきかも.
			newUV[index] = new Vector2((progress), 0).ChangeUVHandSystem();
		}
		for (int h = holizontalVertexAmount - 1; h >= 0; h--) {
			float progress = (float)h / (float)(holizontalVertexAmount - 1);
			int index = (holizontalVertexAmount - 1) + (verticalVertexAmount - 1) + (holizontalVertexAmount - 1 - h);
			newVertices[index] = new Vector3(((width * progress) + (width * centerXRate)) * vec, height - (height * centerYRate)).ChangePosHandSystem();
			// vecがマイナスのとき文字が鏡になるのを避けるためuvのy値を反転して回転しているように見せているけど別の画像用意すべきかも.
			newUV[index] = new Vector2((progress), 1).ChangeUVHandSystem();
		}
		// 三角形に頂点インデックス割り当て.
		
		// ※【ここはverticalVertexAmountが2固定である前提で作るよ】.
		// 0, 1, newVertices.Length-1
		// 0, newVertices.Length-1, newVertices.Length-2
		// 0～は次のループで+1, newVertices.Length系は次のループで-1
		// newVertices.Length-2 == holizontalVertexAmount で終了.
		int triangleIndex = 0;
		for (int i = 0; i < holizontalVertexAmount - 1; i++) {
			newTriangles[triangleIndex + 0] = i + 1;
			newTriangles[triangleIndex + 1] = i + 0;
			newTriangles[triangleIndex + 2] = newVertices.Length - 2 - i;
			newTriangles[triangleIndex + 3] = i + 0;
			newTriangles[triangleIndex + 4] = newVertices.Length - 1 - i;
			newTriangles[triangleIndex + 5] = newVertices.Length - 2 - i;
			triangleIndex += 6;
		}
		
		mesh.vertices  = newVertices;
		mesh.uv        = newUV;
		mesh.triangles = newTriangles;
		mesh.name = "Chain" + chainCount.ToString() + "SlideMesh";
		
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		
		return mesh;
	}

	public static Mesh EvaluaterCreate (int chainCount) {
		if (chainCount == 0) return null;
		// メッシュに使う画像のサイズ.
		float width = 1.0f;
		float height = 16.0f;
		// 横方向の頂点数.
		int holizontalVertexAmount = chainCount;
		// メッシュに使う画像のpovit (0.5が中心)
		float centerXRate = 0.0f;
		float centerYRate = 0.5f;
		
		// 回転方向 (メッシュを作るときに(つまりここで)いじるべきではない)
		float vec = 1;
		
		const int verticalVertexAmount = 2;
		
		Mesh mesh = new Mesh();
		Vector3 [] newVertices = new Vector3[holizontalVertexAmount * verticalVertexAmount];
		Vector2 [] newUV       = new Vector2[holizontalVertexAmount * verticalVertexAmount];
		// 三角形の頂点数は3で、四角形を作るには三角形が2つ必要で、四角形が何個あるかで掛け算する.
		// 四角形用の頂点が2*2で三角形用の頂点が6個. 3*2で12個. 4*2で18個. 3*3で24個. 4*4で36個
		// 頂点数-1個の四角形が作れる.
		int[] newTriangles     = new int[3 * 2 * (holizontalVertexAmount - 1) * (verticalVertexAmount - 1)];
		
		// 鏡コの字を書くように頂点を配置する.
		
		// 頂点とUVを作成.
		for (int h = 0; h < holizontalVertexAmount; h++) {
			float progress = (float)h / (float)(holizontalVertexAmount - 1);
			int index = h;
			newVertices[index] = new Vector3((1 - (width * progress) + (width * centerXRate)) * vec, -(height * centerYRate)).ChangePosHandSystem();
			// vecがマイナスのとき文字が鏡になるのを避けるためuvのy値を反転して回転しているように見せているけど別の画像用意すべきかも.
			newUV[index] = new Vector2((1 - progress), 0).ChangeUVHandSystem();
		}
		for (int h = holizontalVertexAmount - 1; h >= 0; h--) {
			float progress = (float)h / (float)(holizontalVertexAmount - 1);
			int index = (holizontalVertexAmount - 1) + (verticalVertexAmount - 1) + (holizontalVertexAmount - 1 - h);
			newVertices[index] = new Vector3((1 - (width * progress) + (width * centerXRate)) * vec, height - (height * centerYRate)).ChangePosHandSystem();
			// vecがマイナスのとき文字が鏡になるのを避けるためuvのy値を反転して回転しているように見せているけど別の画像用意すべきかも.
			newUV[index] = new Vector2((1 - progress), 1).ChangeUVHandSystem();
		}
		// 三角形に頂点インデックス割り当て.
		
		// ※【ここはverticalVertexAmountが2固定である前提で作るよ】.
		// 0, 1, newVertices.Length-1
		// 0, newVertices.Length-1, newVertices.Length-2
		// 0～は次のループで+1, newVertices.Length系は次のループで-1
		// newVertices.Length-2 == holizontalVertexAmount で終了.
		int triangleIndex = 0;
		for (int i = 0; i < holizontalVertexAmount - 1; i++) {
			newTriangles[triangleIndex + 0] = i + 1;
			newTriangles[triangleIndex + 1] = i + 0;
			newTriangles[triangleIndex + 2] = newVertices.Length - 2 - i;
			newTriangles[triangleIndex + 3] = i + 0;
			newTriangles[triangleIndex + 4] = newVertices.Length - 1 - i;
			newTriangles[triangleIndex + 5] = newVertices.Length - 2 - i;
			triangleIndex += 6;
		}
		
		mesh.vertices  = newVertices;
		mesh.uv        = newUV;
		mesh.triangles = newTriangles;
		mesh.name = "Chain" + chainCount.ToString() + "SlideMesh";
		
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		
		return mesh;
	}
}
