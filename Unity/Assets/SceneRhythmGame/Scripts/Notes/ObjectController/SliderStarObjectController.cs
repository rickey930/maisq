﻿using UnityEngine;
using System.Collections;

public class SliderStarObjectController : MonoBehaviour {
	[SerializeField]
	public Transform rotateHierarchy;
	[SerializeField]
	public Transform timeMoveHierarchy;
	[SerializeField]
	public Transform noteTimeScaleHierarchy;
	
	[SerializeField]
	private NoteScaleInitializer[] noteScaleInitializers;

	// オブジェクトを作ったら設定する.
	public IMaimaiSlideNoteHead headNoteInfo { get; set; }
	public MaimaiSlideChain chainInfo { get; set; }
	public NoteObjectCommonInfo objectInfo { get; set; }
	public int zBuffer { get; set; }

	/// <summary>
	/// ☆がどれだけの距離を進んだか.
	/// </summary>
	public float onStarMovedDistance { get; protected set; }

	/// <summary>
	/// ☆の移動予定距離.
	/// </summary>
	public float starMoveDistance { get; set; }

	private SpriteRenderer starObjectRenderer;
	
	protected int restartId { get; set; }

	public bool visible { get; set; }

	void Start () {
		if (noteScaleInitializers != null) {
			foreach (var s in noteScaleInitializers)  {
				s.Setup (objectInfo.ringSize);
			}
		}
		rotateHierarchy.localRotation = Quaternion.Euler(new Vector3(0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(headNoteInfo.GetImageDegree())));
		noteTimeScaleHierarchy.localScale = new Vector3 (0, 0, 1);
		starObjectRenderer = GetComponentInChildren<SpriteRenderer>();
		SetAlpha(0, starObjectRenderer);
		if (chainInfo.pattern.GetEachNotes ().Count > 0) {
			starObjectRenderer.sprite = SpriteTank.Get("note_star_each");
		}
		else {
			starObjectRenderer.sprite = SpriteTank.Get("note_star");
		}
		starObjectRenderer.sortingOrder = zBuffer;
		restartId = objectInfo.restartId;
		visible = true;
	}

	void Update () {
		Move ();
		Release ();
	}

	protected void Move () {
		float fadePercent = CalcTimePercent(chainInfo.pattern.relativeNote.GetSlideReadyTime(), objectInfo.guideSpeed, objectInfo.guideSpeed);
		float movePercent = CalcTimePercent(chainInfo.onStarMoveEndTime, chainInfo.onStarMoveTimeRange, chainInfo.onStarMoveTimeRange);
		if (movePercent < 0) movePercent = 0;
		if (movePercent > 1) movePercent = 1;
		if (fadePercent < 0) fadePercent = 0;
		if (fadePercent > 1) fadePercent = 1;
		// ノートの不透明化.
		SetAlpha(fadePercent, starObjectRenderer);
		// スターの拡大.
		noteTimeScaleHierarchy.localScale = new Vector3 (fadePercent, fadePercent, 1);
		// スターの移動(計算開始).
		var distance = chainInfo.GetTotalDistance() * movePercent; //始点から今まで移動した距離.
		onStarMovedDistance = distance;
		
		var ptn = 0; //どのパターンを使うべきか.
		while (ptn < chainInfo.commands.Length - 1 && distance >= chainInfo.commands[ptn + 1].GetTotalDistanceOfUntilThisMoveStart()) { 
			//必要な合計距離を越えていたら、それを使う。.
			ptn++;
		}
		
		var command = chainInfo.commands[ptn];
		var pDistance = distance - command.GetTotalDistanceOfUntilThisMoveStart(); //パターンからの距離.
		
		Vector2 newloc;
		if (command.GetCommandType() == MaimaiSlideCommandType.STRAIGHT) {
			//直線.
			//2点間の角度を求めて距離を半径にして移動する.
			newloc = CircleCalculator.PointToPointMovePoint(command.startPos, command.targetPos, pDistance);
		}
		else {
			var cc = (MaimaiSlideCurveCommand)command;
			//曲線
			//円の中心(getCenterAxis())、外周/内周の半径(getOuterAxis()/getInnerAxis())、方向(sp.vec)、開始角度 から、pDistance角度まで移動して、その地点.
			//newloc = //円の中心と周の半径と開始角度は引数、pDistanceと方向は掛けてひとつの引数。.
			var pDeg = CircleCalculator.ArcDegreeFromArcDistance(Mathf.Max(cc.radius.x, cc.radius.y), pDistance);
			//newloc = CircleCalculator.PointOnCircle(sp.curve_centerPos, radius, sp.curve_dStartDeg + pDeg * sp.curve_vec);
			newloc = CircleCalculator.PointOnEllipse(cc.centerPos, cc.radius.x, cc.radius.y, cc.startDeg + pDeg * cc.vec);
		}
		var starloc = Constants.instance.ToLeftHandedCoordinateSystemPosition(newloc);
		// スターの移動.
		timeMoveHierarchy.localPosition = starloc;
		
		//☆と共にスライドマーカーを消すオプションなら、MISS判定より前に消しちゃいたい.
		if (objectInfo.autoPlay && chainInfo.onStarMoveEndTime + objectInfo.judgeTimeLag < objectInfo.timer.gameTime) 
			visible = false;
	}

	/// <summary>
	/// 「判定時間」「その何秒前から表示」「何秒かけて移動」を指定して移動量パーセントを求める.
	/// </summary>
	protected float CalcTimePercent(long justTime, long timeBefore, long timeRange) {
		long startTime = (justTime + objectInfo.judgeTimeLag) - timeBefore;
		return (float)(objectInfo.timer.gameTime - startTime) / (float)timeRange;
	}
	
	protected virtual void Release () {
		if ((!visible) || restartId != objectInfo.restartId) {
			Destroy (gameObject);
		}
	}
	
	protected void SetAlpha(float fade, params SpriteRenderer[] renderers) {
		foreach (var renderer in renderers) {
			var color = renderer.color;
			color.a = fade;
			renderer.color = color;
		}
	}
	
	protected virtual void OnDestroy() {
		headNoteInfo = null;
		chainInfo = null;
		objectInfo = null;
	}
}
