﻿using UnityEngine;
using System.Collections;

public class SoundMessageNoteObjectController : MonoBehaviour {
	
	private MaimaiImportedSoundNote noteInfo { get; set; }
	private NoteObjectCommonInfo objectInfo { get; set; }
	
	public void Setup (MaimaiImportedSoundNote noteInfo, NoteObjectCommonInfo objectInfo) {
		this.noteInfo = noteInfo;
		this.objectInfo = objectInfo;
	}

	IEnumerator Start () {
		if (noteInfo == null || objectInfo == null) yield break;
		while (noteInfo.justTime + objectInfo.judgeTimeLag > objectInfo.timer.gameTime) {
			yield return null;
		}
		Utility.SoundEffectManager.Play (noteInfo.soundId);
	}
}
