﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NumberSpriteController : MonoBehaviour {

	public virtual float value { get; set; }
	private float? oldValue;

	public GameObject ComboValueSpriteObjPrefab;
	public float spriteWidth; // 文字幅.
	public Sprite[] NumberSprites;
	public Sprite commaSprite;
	public Sprite dotSprite;
	public Sprite percentSprite;
	public int digit; //小数点以下第何位切り捨てで表示するか.
	public bool isPercent; // パーセント記号を表示するか.
	public bool visibleComma; //桁区切りを表示するか
	public Color spriteColor;
	private Color? oldSpriteColor;
	private bool initialized;
	private System.IFormatProvider formatProvider;
	private bool? oldAcitveState;

	private List<SpriteRenderer> renderers;

	public void Setup (int digit, bool isPercent, bool visibleComma, Color spriteColor) {
		this.digit = digit;
		this.isPercent = isPercent;
		this.visibleComma = visibleComma;
		this.spriteColor = spriteColor;

		oldSpriteColor = spriteColor;
		renderers = new List<SpriteRenderer>();
		//MatchOfRendererObjAmount(1);
		//renderers [0].sprite = NumberSprites [0];
		formatProvider = new System.Globalization.CultureInfo ("en-US");
		initialized = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (!initialized) return;

		if (!oldValue.HasValue || oldValue.Value != value) {
			string format = visibleComma ? "#,0" : "0";
			if (digit > 0) format += ".";
			for (int i = 0; i < digit; i++) format += "0";
			if (isPercent) format += "'%'";
			string vstr = value.ToString(format, formatProvider);
			if (!oldValue.HasValue || oldValue.Value.ToString(format, formatProvider).Length != vstr.Length) {
				MatchOfRendererObjAmount(vstr.Length);
			}
			int size = renderers.Count;
			for (int i = 0; i < size; i++) {
				string sub = vstr.Substring(i, 1);
				if (sub == ".") {
					renderers[i].sprite = dotSprite;
				}
				else if (sub == ",") {
					renderers[i].sprite = commaSprite;
				}
				else if (sub == "%") {
					renderers[i].sprite = percentSprite;
				}
				else {
					renderers[i].sprite = NumberSprites[int.Parse(sub)];
				}
			}
			oldValue = value;
		}

		if (!oldSpriteColor.HasValue || oldSpriteColor.Value.r != spriteColor.r ||
			oldSpriteColor.Value.g != spriteColor.g || oldSpriteColor.Value.b != spriteColor.b) {
			int size = renderers.Count;
			for (int i = 0; i < size; i++) {
				if (renderers[i].material.HasProperty("_Color")) {
					renderers[i].material.SetColor ("_Color", spriteColor);
				}
				else {
					renderers[i].color = spriteColor;
				}
			}
			oldSpriteColor = spriteColor;
		}
	}

	// 文字数とゲームオブジェクトの数を合わせる.
	private void MatchOfRendererObjAmount(int amount) {
		bool move = false;
		// 削除.
		if (renderers.Count > amount) {
			move = true;
			while (renderers.Count != amount) {
				SpriteRenderer sr = renderers[renderers.Count - 1];
				renderers.Remove(sr);
				Destroy(sr.gameObject);
			}
		}
		// 生成.
		else if (renderers.Count < amount) {
			move = true;
			while (renderers.Count != amount) {
				GameObject item = Instantiate(ComboValueSpriteObjPrefab) as GameObject;
				item.transform.parent = this.transform;
				SpriteRenderer sr = item.GetComponent<SpriteRenderer>();
				if (sr.material.HasProperty("_Color")) {
					sr.material.SetColor ("_Color", spriteColor);
				}
				else {
					sr.color = spriteColor;
				}
				renderers.Add(sr);
			}
		}
		// 中央揃えのため移動.
		if (move) {
			int size = renderers.Count;
			for (int i = 0; i < size; i++) {
				Transform obj = renderers[i].gameObject.transform;
				obj.localPosition = new Vector3(((float)i - (float)(amount - 1) / 2.0f) * spriteWidth, 0, 0);
				if (!oldAcitveState.HasValue) oldAcitveState = true;
				obj.gameObject.SetActive (oldAcitveState.Value);
			}
		}
	}

	public void SetActive (bool value) {
		if (renderers != null) {
			if (!oldAcitveState.HasValue || value != oldAcitveState.Value) {
				int size = renderers.Count;
				for (int i = 0; i < size; i++) {
					if (renderers [i] != null) {
						renderers [i].gameObject.SetActive (value);
					}
				}
				oldAcitveState = value;
			}
		}
	}
}
