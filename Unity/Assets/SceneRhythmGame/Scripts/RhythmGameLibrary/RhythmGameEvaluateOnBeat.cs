﻿using System.Collections.Generic;

namespace RhythmGameLibrary {
	public class RhythmGameEvaluateOnBeat<NoteActionKey, NoteClass> : RhythmGameEvaluate<NoteActionKey, NoteClass> where NoteClass : RhythmGameNote {
		private float bpm { get; set; }
		protected int beatCnt { get; private set; }
		private long beatCountStartTime { get; set; }

		public RhythmGameEvaluateOnBeat (RhythmGameTimer timer, Dictionary<NoteActionKey, NoteClass[]> notes, long timeLag, long justTimeLag, float fixedBpm, long beatCountStartTime) : base (timer, notes, timeLag, justTimeLag) {
			this.bpm = fixedBpm;
			this.beatCountStartTime = beatCountStartTime;
			this.beatCnt = 0;
		}

		/// <summary>
		/// 毎拍のタイミングで何か処理したいときに、毎ループ呼び出す。.
		/// </summary>
		public void AutoBeatProc() {
			if (timer.gameTime >= (((60.0f / bpm) * (float)beatCnt) * 1000.0f).ToLong() + beatCountStartTime) { 
				OnBeat();
				beatCnt++;
			}
		}
		
		/// <summary>
		/// 毎拍呼び出されるメソッド.
		/// </summary>
		protected virtual void OnBeat(){

		}

		/// <summary>
		/// 指定した時間からの拍カウントを再計算する.
		/// </summary>
		public void ResetBeatCount (long gameTime) {
			long time = gameTime - beatCountStartTime;
			if (time > 0) {
				beatCnt = (int)(time / ((60.0f / bpm) * 1000.0f));
			}
			else {
				beatCnt = 0;
			}
		}

	}
}
