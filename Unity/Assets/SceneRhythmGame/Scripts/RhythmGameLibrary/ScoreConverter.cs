﻿using UnityEngine;
using System.Collections.Generic;

namespace RhythmGameLibrary {
	namespace Score {
		public abstract class ScoreConverter<ScoreClass, NoteClass> where ScoreClass : ScoreNoteData where NoteClass : RhythmGameNote {
			public ScoreConverter(ScoreClass[] score_data, float start_count_beat, float start_count_length, decimal time_seek, int[] get_time_request_score_indexes) {
				m_score_data = score_data;
				m_error_code = 0;
				m_time = time_seek;
				m_start_count_beat = start_count_beat;
				m_start_count_length = start_count_length;
				first_bpm = 0;
				m_bpm = null;
				m_beat = null;
				m_interval = null;
				if (get_time_request_score_indexes == null) {
					m_get_time_request_score_indexes = new int[0];
					m_get_time_request_score_indexes_result = new decimal?[0];
				}
				else {
					m_get_time_request_score_indexes = (int[])get_time_request_score_indexes.Clone ();
					m_get_time_request_score_indexes_result = new decimal?[get_time_request_score_indexes.Length];
				}
			}

			public ScoreConverter(ScoreClass[] score_data, float start_count_beat, float start_count_length, long time_seek_ms, int[] get_time_request_score_indexes)
				: this (score_data, start_count_beat, start_count_length, (decimal)time_seek_ms / 1000.0M, get_time_request_score_indexes) {
			}

			private ScoreClass[] m_score_data;
			protected decimal? m_bpm { get; private set; }
			protected decimal? m_beat { get; private set; }
			protected decimal? m_interval { get; private set; }
			private decimal m_time;
			private List<NoteClass> m_notes;
			private float m_start_count_beat;
			private float m_start_count_length;
			private int[] m_get_time_request_score_indexes; // 時間を取得したいスコアのインデックスのリスト.
			private decimal?[] m_get_time_request_score_indexes_result;

			protected int m_error_code;

			public float first_bpm { get; private set; }

			public NoteClass[] ReadScore() {
				m_notes = new List<NoteClass>();
				int index = 0;
				int reqIndex = 0;
				foreach (var data in m_score_data) {
					// BPM情報をセット.
					if (data.change_bpm.HasValue) {
						SetBpm(data.change_bpm.Value);
					}
					// Step情報をセット.
					if (data.change_step != null) {
						if (!data.change_step.is_interval) {
							SetBeat(data.change_step.step);
						}
						else {
							SetInterval(data.change_step.step);
						}
					}
					// ノート情報を読み込む.
					ReadScoreRoutine(data);

					// リクエストがあれば、指定されたスコアのインデックスの時間を記憶する.
					if (reqIndex < m_get_time_request_score_indexes.Length &&
					    m_get_time_request_score_indexes[reqIndex] == index) {
						if (m_get_time_request_score_indexes_result != null) {
							m_get_time_request_score_indexes_result[reqIndex] = GetTime ();
						}
						reqIndex++;
					}
					index++;
				}
				var ret = m_notes.ToArray();
				ReadEndScoreEvent (ret);
				return ret;
			}

			protected abstract void ReadScoreRoutine (ScoreClass data);
			protected abstract void ReadEndScoreEvent (NoteClass[] notes);

			protected void SetBpm(float value) {
				if (!m_bpm.HasValue) {
					first_bpm = value;
					m_time += CalcTime(first_bpm, m_start_count_beat, m_start_count_length);
				}
				m_bpm = (decimal)value;
			}

			protected void SetBeat(float value) {
				m_beat = (decimal)value;
				m_interval = null;
			}

			protected void SetInterval(float interval) {
				m_interval = (decimal)interval;
				m_beat = null;
			}

			protected void AddNotes(params NoteClass[] objs) {
				foreach (var obj in objs) {
					m_notes.Add(obj);
				}
				TimeForward ();
			}

			private void TimeForward() {
				if (m_beat.HasValue) {
					m_time += CalcTime (1);
				}
				else if (m_interval.HasValue) {
					m_time += m_interval.Value;
				}
				else {
					m_error_code = 1; // 拍が未設定.
				}
			}

			protected decimal GetTime() {
				return m_time;
			}

			public long GetTimeMS() {
				return (long)(m_time * 1000.0M);
			}

			private decimal CalcTime(decimal length) {
				if (m_bpm.HasValue && m_beat.HasValue) {
					return CalcTime (m_bpm.Value, m_beat.Value, length);
				}
				else {
					m_error_code = 1; // BPMまたは拍が未設定.
				}
				return 0;
			}

			protected decimal CalcTime(decimal beat, decimal length) {
				if (m_bpm.HasValue) {
					return CalcTime (m_bpm.Value, beat, length);
				}
				else {
					m_error_code = 1; // BPMが未設定.
				}
				return 0;
			}

			protected long CalcTimeMS(float beat, float length) {
				if (m_bpm.HasValue) {
					return CalcTimeMS ((float)m_bpm.Value, beat, length);
				}
				else {
					m_error_code = 1; // BPMが未設定.
				}
				return 0;
			}

			public static decimal CalcTime(decimal bpm, decimal beat, decimal length) {
				if (bpm == 0 || beat == 0) { 
					Debug.LogError("bpm or beat is 0.");
					return 0;
				}
				return 60.0M / bpm * ((4.0M / beat) * length);
			}

			public static decimal CalcTime(float bpm, float beat, float length) {
				return CalcTime ((decimal)bpm, (decimal)beat, (decimal)length);
			}

			public static long CalcTimeMS(float bpm, float beat, float length) {
				return (long)(CalcTime(bpm, beat, length) * 1000.0M);
			}

			public decimal?[] GetResultsOfGetTimeRequestScoreIndexes () {
				return (decimal?[])m_get_time_request_score_indexes_result.Clone();
			}
			
			public long?[] GetResultsOfGetTimeMSRequestScoreIndexes () {
				int size = m_get_time_request_score_indexes_result.Length;
				var ret = new long?[size];
				for (int i = 0; i < size; i++) {
					if (m_get_time_request_score_indexes_result[i].HasValue) {
						ret[i] = (long)(m_get_time_request_score_indexes_result[i].Value * 1000.0M);
					}
					else {
						ret[i] = null;
					}
				}
				return ret;
			}
		}
		
		[System.Serializable]
		public class ScoreNoteData {
			public float? change_bpm;
			public ScoreStepData change_step;
			protected ScoreNoteData () {
			}
			protected ScoreNoteData (float? changeBpm, ScoreStepData changeStep) {
				this.change_bpm = changeBpm;
				this.change_step = changeStep;
			}
			protected static ScoreNoteData Constractor (float? changeBpm, ScoreStepData changeStep) {
				if (changeBpm.HasValue && !(changeBpm.Value > 0))
					changeBpm = null;
				return new ScoreNoteData (changeBpm, changeStep);
			}
			public static ScoreNoteData Create () {
				return Constractor (null, null);
			}
			public static ScoreNoteData Create_Bpm (float bpm) {
				return Constractor (bpm, null);
			}
			public static ScoreNoteData Create_Beat (float beat) {
				return Constractor (null, ScoreStepData.Create (beat, false));
			}
			public static ScoreNoteData Create_Interval (float interval) {
				return Constractor (null, ScoreStepData.Create (interval, true));
			}
			public static ScoreNoteData Create_Bpm_Beat (float bpm, float beat) {
				return Constractor (bpm, ScoreStepData.Create (beat, false));
			}
			public static ScoreNoteData Create_Bpm_Interval (float bpm, float interval) {
				return Constractor (bpm, ScoreStepData.Create (interval, true));
			}
			public ScoreNoteData Clone () {
				var ret = new ScoreNoteData ();
				ret.change_bpm = change_bpm;
				ret.change_step = change_step != null ? change_step.Clone () : null;
				return ret;
			}
		}

		[System.Serializable]
		public class ScoreStepData {
			public float step;
			public bool is_interval;
			protected ScoreStepData () {
			}
			protected ScoreStepData (float step, bool isInterval) {
				this.step = step;
				this.is_interval = isInterval;
			}
			protected static ScoreStepData Constractor (float step, bool isInterval) {
				if (step > 0) {
					return new ScoreStepData (step, isInterval);
				}
				return null;
			}
			public static ScoreStepData Create (float step, bool isInterval) {
				return Constractor (step, isInterval);
			}
			public ScoreStepData Clone () {
				var ret = new ScoreStepData ();
				ret.step = step;
				ret.is_interval = is_interval;
				return ret;
			}
		}
	}
}
