﻿using UnityEngine;
using System.Collections;

public class RhythmGameSceneButtonEffectBirth : MonoBehaviour {
	[SerializeField]
	private GameObject buttonEffectPrefab;

	public void Birth(int buttonNumber) {
		var obj = Instantiate (buttonEffectPrefab) as GameObject;
		obj.transform.parent = transform;
		var ctrl = obj.GetComponent<ButtonEffectObjectController> ();
		ctrl.Setup (buttonNumber);
	}
}
