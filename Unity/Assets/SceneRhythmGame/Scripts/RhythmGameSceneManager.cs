using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RhythmGameSceneManager : MonoBehaviour {
	[SerializeField]
	private Transform gameTable;
	[SerializeField]
	private NumberSpriteController bgInfoNumberSpriteCtrl;
	[SerializeField]
	private SpriteRenderer guideCircle;
	[SerializeField]
	private MeshRenderer backgroundImage;
	[SerializeField]
	private Shader backgroundImageShader;
	[SerializeField]
	private Renderer achievementBarImage;
	[SerializeField]
	private SpriteRenderer achievementBarText;
	[SerializeField]
	private Renderer syncBarImage;
	[SerializeField]
	private NumberSpriteController syncBarText;
	[SerializeField]
	private BGInfoAnimationController bgInfoNameSpriteCtrl;
	[SerializeField]
	private BGInfoAnimationController fullComboAnimationCtrl;
	[SerializeField]
	private BGInfoAnimationController allPerfectAnimationCtrl;
	[SerializeField]
	private BGInfoAnimationController hundredPercentSynchronizeAnimationCtrl;
	private RhythmGameSceneUIController uiCtrl;

	private MaimaiTimer timer;
	private MaimaiJudgeEvaluateManager evaluater;
	private MaimaiSyncEvaluateManager syncEvaluater;

	private MaimaiNote[] allNotes;
	private MaimaiNote[] visualNotes;
	
	private long finishTime { get; set; }
	private long restartTime { get; set; }

	private int nextDisplayNoteIndex;
	private NoteObjectCommonInfo noteObjectInfo;
	private bool oldPauseState;
	private int syncNoteCount;
	private float bgInfoVisualizeTarget; // コンボなら2コンボ目と、10コンボに1回のペースで表示するための記録用.
	private float bgInfoValue;
	private float bgInfoOldValue;
	private float bgInfoFirstTarget;
	private const float bgInfoStepValue = 10.0f;
	private bool fullComboAnimationIsDone;
	private GameVirtualHardwareButton[] sensors { get; set; }
	private bool initialized;
	[SerializeField]
	private GameObject notePrefab;
	[SerializeField]
	private GameObject holdNotePrefab;
	[SerializeField]
	private GameObject slideNotePrefab;
	[SerializeField]
	private GameObject messageNotePrefab;
	[SerializeField]
	private GameObject scrollMessageNotePrefab;
	[SerializeField]
	private GameObject soundMessageNotePrefab;
	[SerializeField]
	private GameObject sensorPrefab;
	[SerializeField]
	private EvaluateObjectBirth effectMother;
	[SerializeField]
	private RhythmGameSceneButtonEffectBirth buttonEffectMother;
	[SerializeField]
	private GameObject syncEvaluaterPrefab;

	private const int maxMultiPlayers = 2;

	// ノートオブジェクト用コンフィグ.
	protected bool autoPlay { get; set; }
	protected long guideFadeSpeed { get; set; }
	protected long guideMoveSpeed { get; set; }
	protected long judgeTimeLag { get; set; }
	protected bool starRotation { get; set; }
	protected bool angulatedHold { get; set; }
	protected bool pine { get; set; }
	protected float ringSize { get; set; }
	protected float markerSize { get; set; }
	// 楽譜用コンフィグ.
	protected ConfigTypes.ChangeBreak changeBreakType { get; set; }
	protected int turn { get; set; }
	protected bool mirror { get; set; }
	// 評価機用コンフィグ.
	protected bool startBeatCountSoundEnabled { get; set; }
	protected ConfigTypes.SoundEffect soundEffectType { get; set; }
	protected ConfigTypes.AnswerSound answerSoundType { get; set; }
	protected ConfigTypes.Judgement judgementType { get; set; }
	protected bool visualizeFastLate { get; set; }
	// 表示用コンフィグ.
	protected ConfigTypes.BackGroundInfomation bgInfoType { get; set; }
	protected ConfigTypes.UpScreenAchievementVision achievementVisionType { get; set; }
	protected ConfigTypes.RankVersion rankVisionVersion { get; set; }
	// リピート用データ.
	protected float repeatStartTimeRate { get; set; }
	protected long repeatTime { get; set; }

	private GameObject _noteObjectsParent;
	private GameObject noteObjectsParent {
		get {
			if (_noteObjectsParent == null) {
				_noteObjectsParent = new GameObject("Note Objtects");
				_noteObjectsParent.transform.parent = gameTable;
			}
			return _noteObjectsParent;
		}
	}

	// ポーズ状態制御.
	private float pauseTimer { get; set; }
	private float pauseTime { get { return 1.0f; } }

	private bool gameEndProcCalled { get; set; }

	// Use this for initialization
	protected virtual IEnumerator Start () {
		bool debugAutoPlay = true;
		bool debugAutoPlayAndEndless = true;

		initialized = false;
		uiCtrl = GetComponent<RhythmGameSceneUIController> ();
		uiCtrl.loadingScreen.SetActive (true);

		while (UserDataController.instance == null) {
			yield return null;
		}

		
		// シンクモードでホストなら相手をリザルトゲームシーンにいざなう.
		if (MiscInformationController.instance.syncMode == 1) {
			SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateHostRhythmGameSceneMoved", RPCMode.OthersBuffered);
			while (!SyncLobbyNetworkStateController.instance.clientRhythmGameSceneMoved) {
				yield return null;
			}
		}
		else if (MiscInformationController.instance.syncMode == 2) {
			SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateClientRhythmGameSceneMoved", RPCMode.Server);
			while (!SyncLobbyNetworkStateController.instance.hostRhythmGameWait) {
				yield return null;
			}
		}
		
		// コンフィグを反映.
		SetGameConfig ();
		// センサーを生成.
		CreateSensors ();
		// タイマーのインスタンスを生成.
		timer = new MaimaiTimer(AudioManagerLite.GetManager());
		// 楽譜を読み込む.
		MaimaiScore.Each[] score = null;
		if (TrackInformationController.instance.calledSetup) {
			score = TrackInformationController.instance.score;
			if (TrackInformationController.instance.audioClip != null) {
				AudioManagerLite.Load(TrackInformationController.instance.audioClip);
				MiscInformationController.instance.lastLoadedBgmKey = "track_audio";
				AudioManagerLite.SetVolume ();
			}
			else {
				AudioManagerLite.Pause ();
				AudioManagerLite.Load (null);
				MiscInformationController.instance.lastLoadedBgmKey = "track_audio";
			}
			if (TrackInformationController.instance.jacket != null) {
				//backgroundImage.sprite = TrackInformationController.instance.jacket;
				var mat = new Material(backgroundImageShader);
				mat.name = "LoadedJacket";
				mat.mainTexture = TrackInformationController.instance.jacket.texture;
				mat.SetColor("_Color", new Color(1, 1, 1, UserDataController.instance.config.brightness));
				backgroundImage.sharedMaterial = mat;
			}
			else {
				// ジャケットが無ければデフォ画像を表示.
				//backgroundImage.sprite = SpriteTank.Get ("bg_default");
				var mat = new Material(backgroundImageShader);
				mat.name = "DefaultJacket";
				mat.mainTexture = SpriteTank.Get ("bg_default").texture;
				mat.SetColor("_Color", new Color(1, 1, 1, UserDataController.instance.config.brightness));
				backgroundImage.sharedMaterial = mat;
			}
			while (MaimaiStyleDesigner.GetGuideCircleSprite(TrackInformationController.instance.difficulty) == null) {
				yield return null;
			}
			guideCircle.sprite = MaimaiStyleDesigner.GetGuideCircleSprite(TrackInformationController.instance.difficulty);
		}
		else {
			var mat = new Material(backgroundImageShader);
			mat.mainTexture = SpriteTank.Get ("bg_default").texture;
			mat.SetColor("_Color", new Color(1, 1, 1, UserDataController.instance.config.brightness));
			backgroundImage.sharedMaterial = mat;
			yield return StartCoroutine (TestScoreCreater.CreateTestScore ((data)=>score=data));
			while (MaipadDynamicCreatedSpriteTank.instance.guideCircle6 == null) {
				yield return null;
			}
			guideCircle.sprite = MaipadDynamicCreatedSpriteTank.instance.guideCircle6;
			if (debugAutoPlay || debugAutoPlayAndEndless) {
				autoPlay = true;
			}
			else {
				autoPlay = false;
			}
		}
		// 楽譜からノーツを生成する.
		bool tapToBreak = changeBreakType == ConfigTypes.ChangeBreak.TAP_TO_BREAK || changeBreakType == ConfigTypes.ChangeBreak.TAP_EXCHANGE_BREAK;
		bool breakToTap = changeBreakType == ConfigTypes.ChangeBreak.BREAK_TO_TAP || changeBreakType == ConfigTypes.ChangeBreak.TAP_EXCHANGE_BREAK;
		int[] edtiorOption = null;
		if (MiscInformationController.instance.rhythmGameRequesterSceneName == "editor") {
			// エディタからの起動ならエディタのインデックスからスタート位置とリピート位置を取得.
			if (EditorInformationController.instance.multiSelectableMode) {
				edtiorOption = new int[] {
					EditorInformationController.instance.selectionStartIndex,
					EditorInformationController.instance.selectionEndIndex,
				};
			}
			else {
				edtiorOption = new int[] {
					EditorInformationController.instance.selectionStartIndex,
				};
			}
		}
		var converter = new MaimaiScoreConverter (score, mirror, turn, tapToBreak, breakToTap, edtiorOption);
		allNotes = converter.ReadScore ();
		MaimaiNote[] syncableNotes;
		NotesPartition (allNotes, out visualNotes, out syncableNotes);
		// エディタからの起動ならエディタのインデックスからスタート位置とリピート位置を設定など.
		if (edtiorOption != null) {
			var results = converter.GetResultsOfGetTimeMSRequestScoreIndexes();
			if (results[0].HasValue) {
				if (EditorInformationController.instance.selectionStartIndex == 0) {
					// インデックスが0ならホントに初めから再生させる.
					restartTime = 0;
				}
				else {
					// ある程度前から再生させたいのでスタート位置からSTART_COUNT_START_TIMEだけ前にずらす.
					restartTime = results[0].Value - MaimaiJudgeEvaluateManager.START_COUNT_START_TIME;
					if (restartTime < 0) {
						restartTime = 0;
					}
				}
			}
			else {
				restartTime = 0;
			}
			if (edtiorOption.Length < 2 || !results[1].HasValue || !results[0].HasValue) {
				repeatTime = 0;
			}
			else {
				repeatTime = results[1].Value + MaimaiJudgeEvaluateManager.START_COUNT_START_TIME - results[0].Value; // end - startで区間を取得.
				if (repeatTime < 0) {
					repeatTime = 0;
				}
			}
			
			autoPlay = true;
		}
		else if (MiscInformationController.instance.rhythmGameRequesterSceneName == "tutorial") {
			// チュートリアルのときオートプレーにしないなど.
			autoPlay = false;
			bgInfoType = ConfigTypes.BackGroundInfomation.OFF;
		}
		else if (MiscInformationController.instance.syncMode > 0) {
			// シンクモードのときオートプレーにしない.
			autoPlay = false;
		}
		// 評価機を生成する.
		nextDisplayNoteIndex = 0;
		evaluater = new MaimaiJudgeEvaluateManager (timer, allNotes, judgeTimeLag, converter.first_bpm, effectMother, startBeatCountSoundEnabled, soundEffectType, answerSoundType, judgementType, visualizeFastLate);
		// bgInfo.
		if (bgInfoType == ConfigTypes.BackGroundInfomation.COMBO) {
			bgInfoNumberSpriteCtrl.Setup (0, false, true, MaimaiStyleDesigner.GetComboColor());
			bgInfoNameSpriteCtrl.Setup (MaimaiStyleDesigner.GetComboColor(), "combo");
			bgInfoNumberSpriteCtrl.value = evaluater.combo;
			bgInfoNumberSpriteCtrl.SetActive (evaluater.combo > 1 && !fullComboAnimationIsDone);
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.PCOMBO) {
			bgInfoNumberSpriteCtrl.Setup (0, false, true, MaimaiStyleDesigner.GetPComboColor());
			bgInfoNameSpriteCtrl.Setup (MaimaiStyleDesigner.GetPComboColor(), "pcombo");
			bgInfoNumberSpriteCtrl.value = evaluater.pcombo;
			bgInfoNumberSpriteCtrl.SetActive (evaluater.pcombo > 1 && !fullComboAnimationIsDone);
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.BPCOMBO) {
			bgInfoNumberSpriteCtrl.Setup (0, false, true, MaimaiStyleDesigner.GetBPComboColor());
			bgInfoNameSpriteCtrl.Setup (MaimaiStyleDesigner.GetBPComboColor(), "bpcombo");
			bgInfoNumberSpriteCtrl.value = evaluater.bpcombo;
			bgInfoNumberSpriteCtrl.SetActive (evaluater.bpcombo > 1 && !fullComboAnimationIsDone);
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.ACHIEVEMENT) {
			float value = evaluater.GetAchievement().ToPercent2();
			bgInfoNumberSpriteCtrl.Setup (2, true, true, MaimaiStyleDesigner.GetAchievementColor(value));
			bgInfoNameSpriteCtrl.Setup (MaimaiStyleDesigner.GetAchievementColor(value), "achievement");
			bgInfoNumberSpriteCtrl.value = value;
			bgInfoNumberSpriteCtrl.SetActive (!fullComboAnimationIsDone);
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.SCORE) {
			bgInfoNumberSpriteCtrl.Setup (0, false, true, MaimaiStyleDesigner.GetScoreColor());
			bgInfoNameSpriteCtrl.Setup (MaimaiStyleDesigner.GetScoreColor(), "score");
			bgInfoNumberSpriteCtrl.value = evaluater.GetScore();
			bgInfoNumberSpriteCtrl.SetActive (!fullComboAnimationIsDone);
		}
		else {
			bgInfoNumberSpriteCtrl.SetActive (false);
			bgInfoNameSpriteCtrl.gameObject.SetActive (false);
		}
		SetBgInfoVisualizeTargetInit ();
		Color fullComboColor = MaimaiStyleDesigner.ByteToPercentRGBA(255,128,0,255);
		fullComboAnimationCtrl.Setup (fullComboColor, "full combo");
		allPerfectAnimationCtrl.Setup (fullComboColor, "all perfect");
		hundredPercentSynchronizeAnimationCtrl.Setup (fullComboColor, "100% sync");
		fullComboAnimationIsDone = false;

		bool isSyncMode = MiscInformationController.instance.syncMode > 0;
		// サーバーならシンク評価機を作る.
		if (MiscInformationController.instance.syncMode == 1) {
			var syncEvaluaterObj = Network.Instantiate (syncEvaluaterPrefab, Vector3.zero, Quaternion.Euler(Vector3.zero), 0) as GameObject;
			syncEvaluater = syncEvaluaterObj.GetComponent<MaimaiSyncEvaluateManager>();
			syncEvaluater.networkView.RPC ("Setup", RPCMode.AllBuffered, maxMultiPlayers, syncNoteCount);
			evaluater.syncEvaluater = syncEvaluater.networkView;
		}
		// クライアントならサーバーが作ったシンク評価機を見つけて参照する.
		else if (MiscInformationController.instance.syncMode == 2) {
			GameObject syncEvaluaterObj = null;
			while ((syncEvaluaterObj = GameObject.Find (syncEvaluaterPrefab.name + "(Clone)")) == null) {
				yield return null;
			}
			syncEvaluater = syncEvaluaterObj.GetComponent<MaimaiSyncEvaluateManager>();
			evaluater.syncEvaluater = syncEvaluater.networkView;
		}
		// ノートオブジェクトの共有情報を作成する (オートプレーの設定なんかはこれより前に行わないとノートに反映されない).
		noteObjectInfo = new NoteObjectCommonInfo(timer, guideFadeSpeed, guideMoveSpeed, judgeTimeLag, starRotation, angulatedHold, pine, autoPlay, ringSize, markerSize);
		// 時間の設定.
		var _converterTime = converter.GetFinishTimeMS ();
		finishTime = _converterTime + 3000L;
		if (edtiorOption == null) {
			restartTime = (_converterTime * repeatStartTimeRate).ToLong ();
		}
		// simaiのfirstマクロがあるならseekとwaitにそれを反映させる.
		TrackInformationController.instance.SetSimaiFirstData (converter.first_bpm, MaimaiJudgeEvaluateManager.START_COUNT_START_TIME);
		// UIコントローラーへの情報伝達.
		uiCtrl.isAutoPlay = autoPlay;
		uiCtrl.achievement = GetAchievement ();
		uiCtrl.judgementType = judgementType;
		if (isSyncMode) {
			uiCtrl.syncPoint = syncEvaluater.syncPercent;
			syncEvaluater.onCountChanged = (count) => {
				uiCtrl.syncPoint = syncEvaluater.syncPercent;
			};
			achievementBarImage.gameObject.SetActive (false);
			syncBarImage.gameObject.SetActive (true);
			syncBarImage.material.SetFloat ("_Width", 0);
			syncBarText.value = syncEvaluater.syncPercent.ToPercent2().ToInt();
			syncBarText.Setup (0, true, false, Color.yellow);
			syncBarText.SetActive (true);
		}
		else {
			achievementBarImage.gameObject.SetActive (true);
			syncBarImage.gameObject.SetActive (false);
			achievementBarImage.material.SetFloat ("_Width", 0);
			syncBarText.value = 0;
			syncBarText.SetActive (false);
		}
		// 情報リセット.
		oldPauseState = false;
		gameEndProcCalled = false;

		// ポーズボタンの挙動設定 (オートプレーなら即時ポーズ、そうでないなら長押しでポーズ)
		uiCtrl.pauseButtonAnimation.fillAmount = 0;
		if (autoPlay) {
			uiCtrl.pauseButton.onClick.AddListener (OnPauseButtonClick);
		}
		else {
			var trigger = uiCtrl.pauseButton.GetComponent<UnityEngine.EventSystems.EventTrigger>();
			trigger.delegates = new List<UnityEngine.EventSystems.EventTrigger.Entry>();
			
			var entry = new UnityEngine.EventSystems.EventTrigger.Entry();
			entry.eventID = UnityEngine.EventSystems.EventTriggerType.PointerDown;
			entry.callback.AddListener( (x) => { uiCtrl.OnPauseButtonDown(); });
			trigger.delegates.Add(entry);
			
			entry = new UnityEngine.EventSystems.EventTrigger.Entry();
			entry.eventID = UnityEngine.EventSystems.EventTriggerType.PointerUp;
			entry.callback.AddListener( (x) => { uiCtrl.OnPauseButtonUp(); });
			trigger.delegates.Add(entry);
		}
		
		// シンクモードでホストなら相手を待つ.
		if (MiscInformationController.instance.syncMode == 1) {
			SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateHostRhythmGameWait", RPCMode.OthersBuffered);
			while (!SyncLobbyNetworkStateController.instance.clientRhythmGameWait) {
				yield return null;
			}
		}
		else if (MiscInformationController.instance.syncMode == 2) {
			SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateClientRhythmGameWait", RPCMode.Server);
			yield return null;
		}

		// タイマーを初期化する.
		if (TrackInformationController.instance.calledSetup) {
			if (!autoPlay) {
				timer.initialize (TrackInformationController.instance.seek, TrackInformationController.instance.wait, finishTime);
			}
			else {
				Restart ();
			}
		}
		else {
			if (!debugAutoPlay || debugAutoPlayAndEndless) {
				timer.initialize_ModeEndless ();
			}
			else {
				Restart ();
			}
		}

		initialized = true;
		uiCtrl.loadingScreen.SetActive (false);
	}

	private void Restart () {
		noteObjectInfo.restartId++;
		timer.initialize (TrackInformationController.instance.seek, TrackInformationController.instance.wait, finishTime, restartTime);
		evaluater.ResetEvaluate (restartTime, !autoPlay);
		evaluater.ResetBeatCount (restartTime);
		ResetNextDisplayNoteIndex (restartTime);
		SetBgInfoVisualizeTargetInit ();
		fullComboAnimationIsDone = false;
	}

	protected virtual void SetGameConfig () {
		var c = UserDataController.instance.config;
		// ノートオブジェクト用コンフィグ.
		autoPlay = c.auto_play;
		guideMoveSpeed = (c.move_guide_speed * 1000.0f).ToLong ();
		guideFadeSpeed = (c.fade_guide_speed * 1000.0f).ToLong ();
		judgeTimeLag = (c.judge_offset * 1000.0f).ToLong ();
		starRotation = c.star_rotation;
		angulatedHold = c.angulated_hold;
		pine = c.pine;
		ringSize = c.ring_size;
		markerSize = c.marker_size;
		// 楽譜用コンフィグ.
		changeBreakType = c.change_break_type.ToChangeBreakType();
		turn = c.turn;
		mirror = c.mirror;
		// 評価機用コンフィグ.
		startBeatCountSoundEnabled = true;
		soundEffectType = c.sound_effect_type.ToSoundEffectType ();
		answerSoundType = c.answer_sound_type.ToAnswerSoundType ();
		judgementType = c.judgement_type.ToJudgementType ();
		visualizeFastLate = c.visualize_fast_late;
		// 表示用コンフィグ.
		bgInfoType = c.background_infomation_type.ToBackGroundInfomationType ();
		achievementVisionType = c.up_screen_achievement_vision_type.ToUpScreenAchievementVisionType ();
		rankVisionVersion = c.rank_version_type.ToRankVersionType ();
		// リピート用データ.
		repeatStartTimeRate = UserDataController.instance.user.repeat_start_time_rate;
		repeatTime = UserDataController.instance.user.repeat_time * 1000L;
	}

	private void NotesPartition(MaimaiNote[] notes, out MaimaiNote[] forMarker, out MaimaiNote[] forSync) {
		// 表示に対応するノートを集める.
		var _forMarker = new List<MaimaiNote>();
		foreach (var note in notes) {
			if (note.IsVisualNoteType() && !note.secret) {
				_forMarker.Add(note);
			}
		}
		forMarker = _forMarker.ToArray();

		// シンクに対応するノートを集める.
		syncNoteCount = 0;
		HashSet<MaimaiSlidePattern> patterns = new HashSet<MaimaiSlidePattern> ();
		var _forSync = new List<MaimaiNote>();
		foreach (var note in notes) {
			if (note.IsSyncableNoteType()) {
				_forSync.Add(note);
				if (note.GetNoteType() != MaimaiNoteType.SLIDE || patterns.Add((note as MaimaiSlideNote).pattern)) {
					syncNoteCount++;
				}
			}
		}
		forSync = _forSync.ToArray();
	}

	private void CreateSensors() {
		var sensorParent = new GameObject ("Sensors");
		sensorParent.transform.SetParentEx (gameTable);

		sensors = new GameVirtualHardwareButton[17];
		for (int i = 0; i < 8; i++) {
			var sensorObj = Instantiate(sensorPrefab) as GameObject;
			sensorObj.name = "Sensor Outer " + (i + 1).ToString();
			sensorObj.transform.SetParentEx (sensorParent.transform);
			var sensor = sensorObj.GetComponent<MaimaiSensor>();
			sensor.Setup(new Vector2(0, Constants.instance.MAIMAI_OUTER_RADIUS), Constants.instance.GetPieceDegree(i));
			sensor.SetupButtonNames ("MaiOuterSensor" + (i + 1).ToString ());
			sensors[i] = sensor;
		}
		for (int i = 0; i < 8; i++) {
			var sensorObj = Instantiate(sensorPrefab) as GameObject;
			sensorObj.name = "Sensor Inner " + (i + 1).ToString();
			sensorObj.transform.SetParentEx (sensorParent.transform);
			var sensor = sensorObj.GetComponent<MaimaiSensor>();
			sensor.Setup(new Vector2(0, Constants.instance.MAIMAI_INNER_RADIUS), Constants.instance.GetPieceDegree(i));
			sensor.SetupButtonNames ("MaiInnerSensor" + (i + 1).ToString ());
			sensors[i + 8] = sensor;
		}
		{
			int i = 16;
			var sensorObj = Instantiate(sensorPrefab) as GameObject;
			sensorObj.name = "Sensor Center";
			sensorObj.transform.SetParentEx (sensorParent.transform);
			var sensor = sensorObj.GetComponent<MaimaiSensor>();
			sensor.Setup(Vector2.zero, 0);
			sensor.SetupButtonNames ("MaiCenterSensor");
			sensors[i] = sensor;
		}
	}
	private GameVirtualHardwareButton[] GetSensor(Constants.Circumference place) {
		if (place == Constants.Circumference.OUTER) {
			return new GameVirtualHardwareButton[] {
				sensors[0], sensors[1], sensors[2], sensors[3], sensors[4], sensors[5], sensors[6], sensors[7], 
			};
		}
		else if (place == Constants.Circumference.INNER) {
			return new GameVirtualHardwareButton[] {
				sensors[8], sensors[9], sensors[10], sensors[11], sensors[12], sensors[13], sensors[14], sensors[15], 
			};
		}
		else {
			return new GameVirtualHardwareButton[] {
				sensors[16],
			};
		}
	}
	
	// Update is called once per frame
	protected virtual void Update () {
		if (!initialized) return;
		
		if (!timer.finish || timer.restarted) {
			timer.timeUpdate ();

			if (!timer.pause) {
				if (!autoPlay) {
					// 操作系の処理.
					foreach (var sensor in sensors) {
						sensor.Check(oldPauseState, true);
					}
					var outerSensors = GetSensor(Constants.Circumference.OUTER);
					for (int i = 0; i < outerSensors.Length; i++) {
						var sensor = outerSensors[i];
						if (sensor.push) {
							buttonEffectMother.Birth(i);
							evaluater.JudgeNote(Constants.instance.MAIMAI_NOTE_ACTION_ID_BASE + i);
						}
						if (sensor.pull && !sensor.hold) {
							evaluater.JudgeNote(Constants.instance.MAIMAI_NOTE_HOLD_FOOT_ACTION_ID_BASE + i);
						}
						if (sensor.hold) {
							evaluater.JudgementableNotes(Constants.instance.MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_OUTER_BASE + i);
						}
					}
					var innerSensors = GetSensor(Constants.Circumference.INNER);
					for (int i = 0; i < innerSensors.Length; i++) {
						var sensor = innerSensors[i];
						if (sensor.hold) {
							evaluater.JudgementableNotes(Constants.instance.MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_INNER_BASE + i);
						}
					}
					var centerSensors = GetSensor(Constants.Circumference.CENTER);
					for (int i = 0; i < centerSensors.Length; i++) {
						var sensor = centerSensors[i];
						if (sensor.hold) {
							evaluater.JudgementableNotes(Constants.instance.MAIMAI_NOTE_SLIDE_PATTERN_ACTION_ID_CENTER);
						}
					}
				}
				else if (repeatTime > 0) {
					// リピート再生系の処理.
					if (timer.gameTime > restartTime + repeatTime) {
						Restart ();
					}
				}

				if (!timer.restarted) {
					evaluater.AutoBeatProc();
					evaluater.AutoGuideProc();
					if (autoPlay)
						evaluater.AutoJustProc();
					evaluater.AutoMissProc();
					evaluater.NextUpdatePrepare();
					CreateNoteObject ();
				}

				uiCtrl.achievement = GetAchievement ();
				// 達成率バーの更新.
				if (syncEvaluater == null) {
					float value = evaluater.GetAchievement();
					achievementBarImage.material.SetFloat ("_Width", value);
					float pvalue = value.ToPercent2();
					achievementBarText.sprite = rankVisionVersion == ConfigTypes.RankVersion.PINK ? MaimaiStyleDesigner.GetRankSprite (pvalue) :
						rankVisionVersion == ConfigTypes.RankVersion.CLASSIC ? MaimaiStyleDesigner.GetRankSpriteClassic (pvalue) : null;
					achievementBarText.material.SetColor ("_Color", MaimaiStyleDesigner.GetAchievementColor (pvalue));
				}
				else {
					syncBarImage.material.SetFloat ("_Width", syncEvaluater.syncPercent);
					//syncBarText.text = syncEvaluater.syncPercent.ToPercent2().ToInt().ToString() + "%";
					syncBarText.value = syncEvaluater.syncPercent.ToPercent2().ToInt();
				}
				
				oldPauseState = false;
			}
			else {
				oldPauseState = true;
			}

			// bgInfo.
			if (bgInfoType == ConfigTypes.BackGroundInfomation.COMBO) {
				bgInfoNumberSpriteCtrl.value = evaluater.combo;
				bgInfoNumberSpriteCtrl.SetActive (evaluater.combo > 1 && !fullComboAnimationIsDone);
			}
			else if (bgInfoType == ConfigTypes.BackGroundInfomation.PCOMBO) {
				bgInfoNumberSpriteCtrl.value = evaluater.pcombo;
				bgInfoNumberSpriteCtrl.SetActive (evaluater.pcombo > 1 && !fullComboAnimationIsDone);
			}
			else if (bgInfoType == ConfigTypes.BackGroundInfomation.BPCOMBO) {
				bgInfoNumberSpriteCtrl.value = evaluater.bpcombo;
				bgInfoNumberSpriteCtrl.SetActive (evaluater.bpcombo > 1 && !fullComboAnimationIsDone);
			}
			else if (bgInfoType == ConfigTypes.BackGroundInfomation.ACHIEVEMENT) {
				float value = evaluater.GetAchievement().ToPercent2();
				if (bgInfoNumberSpriteCtrl.value != value) {
					bgInfoNumberSpriteCtrl.value = value;
					bgInfoNumberSpriteCtrl.spriteColor = MaimaiStyleDesigner.GetAchievementColor(value);
				}
				bgInfoNumberSpriteCtrl.SetActive (!fullComboAnimationIsDone);
			}
			else if (bgInfoType == ConfigTypes.BackGroundInfomation.SCORE) {
				float value = evaluater.GetScore();
				bgInfoNumberSpriteCtrl.value = value;
				bgInfoNumberSpriteCtrl.SetActive (!fullComboAnimationIsDone);
			}
			if (SetBgInfoVisualizeTarget ()) {
				bgInfoNameSpriteCtrl.ActivateSpriteRenderer();
			}

			if (!fullComboAnimationIsDone && MiscInformationController.instance.rhythmGameRequesterSceneName != "editor" && MiscInformationController.instance.rhythmGameRequesterSceneName != "tutorial") {
				const int NONE = 0, AP = 1, FC = 2, SYNC = 3;
				int type = NONE;
				if (syncEvaluater != null) {
					if (syncEvaluater.isAllSyncEvaluated) {
						if (syncEvaluater.syncPercent == 1.0f) {
							type = SYNC;
						}
						else if (evaluater.IsAllPerfect ()) {
							type = AP;
						}
						else if (evaluater.IsNoMiss ()) {
							type = FC;
						}
					}
				}
				else if (evaluater.IsAllPerfect ()) {
					type = AP;
				}
				else if (evaluater.IsNoMiss ()) {
					type = FC;
				}
				if (type > NONE) {
					bgInfoNameSpriteCtrl.gameObject.SetActive (false);
					bgInfoNumberSpriteCtrl.gameObject.SetActive (false);
					if (type == AP) {
						Utility.SoundEffectManager.Play ("maisq_voice_ap");
						allPerfectAnimationCtrl.ActivateSpriteRenderer();
					}
					else if (type == FC) {
						Utility.SoundEffectManager.Play ("maisq_voice_fc");
						fullComboAnimationCtrl.ActivateSpriteRenderer();
					}
					else if (type == SYNC) {
						Utility.SoundEffectManager.Play ("maisq_voice_100persync");
						hundredPercentSynchronizeAnimationCtrl.ActivateSpriteRenderer ();
					}
					fullComboAnimationIsDone = true;
				}
			}
		}
		else {
			if (autoPlay && repeatTime > 0) {
				// リピート再生系の処理.
				Restart ();
			}
			else {
				if (!gameEndProcCalled) {
					// リピートが無効ならゲーム終了.
					StartCoroutine (OnGameEnd());
					gameEndProcCalled = true;
				}
			}
		}

		// ポーズ状態管理.
		if (!autoPlay) {
			uiCtrl.pauseButtonAnimation.fillAmount = pauseTimer;
			if (!timer.pause) {
				if (uiCtrl.pauseButtonHolding) {
					if (pauseTimer < pauseTime) {
						pauseTimer += Time.deltaTime;
					}
					else {
						Utility.SoundEffectManager.Play ("se_decide");
						PauseGame ();
					}
				}
				else {
					if (pauseTimer > 0f) {
						pauseTimer -= Time.deltaTime;
					}
					else {
						pauseTimer = 0f;
					}
				}
			}
		}
	}

	private void CreateNoteObject() {
		for (int i = nextDisplayNoteIndex; i < visualNotes.Length; i++) {
			MaimaiNote note = visualNotes[i];
			if (timer.gameTime >= (note.justTime + judgeTimeLag) - noteObjectInfo.guideSpeed) {
				var type = note.GetNoteType();
				var objs = new List<GameObject>();
				var slideObjs = new List<GameObject>();
				if (type == MaimaiNoteType.TAP_CIRCLE ||
					type == MaimaiNoteType.BREAK_CIRCLE) {
					var obj = Instantiate(notePrefab) as GameObject;
					objs.Add(obj);
				}
				else if (type == MaimaiNoteType.HOLD_HEAD) {
					var obj = Instantiate(holdNotePrefab) as GameObject;
					objs.Add(obj);
				}
				else if (type == MaimaiNoteType.TAP_STAR ||
				         type == MaimaiNoteType.BREAK_STAR) {
					var obj = Instantiate(notePrefab) as GameObject;
					objs.Add(obj);
					int patterns = (note as IMaimaiSlideNoteHead).GetRelativeNote().Length;
					for (int p = 0; p < patterns; p++) {
						obj = Instantiate(slideNotePrefab) as GameObject;
						slideObjs.Add (obj);
					}
				}
				else if (type == MaimaiNoteType.SLIDE_HEAD) {
					int patterns = (note as IMaimaiSlideNoteHead).GetRelativeNote().Length;
					for (int p = 0; p < patterns; p++) {
						var obj = Instantiate(slideNotePrefab) as GameObject;
						slideObjs.Add (obj);
					}
				}
				else if (type == MaimaiNoteType.MESSAGE) {
					var obj = Instantiate(messageNotePrefab) as GameObject;
					obj.GetComponent<MessageNoteObjectController>().Setup (note as MaimaiMessageNote, noteObjectInfo);
					obj.SetParentEx (noteObjectsParent);
				}
				else if (type == MaimaiNoteType.MESSAGE_SCROLL) {
					var obj = Instantiate(scrollMessageNotePrefab) as GameObject;
					obj.GetComponent<ScrollMessageNoteObjectController>().Setup (note as MaimaiScrollMessageNote, noteObjectInfo);
					obj.SetParentEx (noteObjectsParent);
				}
				else if (type == MaimaiNoteType.MESSAGE_SOUND) {
					var obj = Instantiate(soundMessageNotePrefab) as GameObject;
					obj.GetComponent<SoundMessageNoteObjectController>().Setup (note as MaimaiImportedSoundNote, noteObjectInfo);
					obj.SetParentEx (noteObjectsParent);
				}
				for (int oi = 0; oi < objs.Count; oi++) {
					var obj = objs[oi];
					if (obj != null) {
						obj.transform.parent = noteObjectsParent.transform;
						NoteObjectControllerBase ctrl = obj.GetComponent<NoteObjectControllerBase>();
						if (ctrl != null) {
							ctrl.noteInfo = note;
							ctrl.objectInfo = noteObjectInfo;
							ctrl.zBuffer = visualNotes.Length - i;
							ctrl.transform.localPosition = new Vector3(0, 0, 0);
						}
					}
				}
				for (int oi = 0; oi < slideObjs.Count; oi++) {
					var obj = slideObjs[oi];
					if (obj != null) {
						obj.transform.parent = noteObjectsParent.transform;
						SlideMarkerPlaceController ctrl = obj.GetComponent<SlideMarkerPlaceController>();
						if (ctrl != null) {
							ctrl.noteInfo = note;
							ctrl.objectInfo = noteObjectInfo;
							ctrl.zBuffer = visualNotes.Length - i;
							ctrl.transform.localPosition = new Vector3(0, 0, 0);
							ctrl.slidePatternInfo = (note as IMaimaiSlideNoteHead).GetRelativeNote()[oi];
						}
					}
				}
					
				nextDisplayNoteIndex = i + 1;
			}
			else {
				break;
			}
		}
	}
	
	private void ResetNextDisplayNoteIndex (long gameTime) {
		for (int i = 0; i < allNotes.Length; i++) {
			MaimaiNote note = allNotes[i];
			var type = note.GetNoteType ();
			bool through = false;

			if (gameTime < (note.justTime + judgeTimeLag)) {
				// visualNotesに親が存在するノートなら親を探す.
				if (type == MaimaiNoteType.HOLD_FOOT) {
					note = (note as MaimaiHoldFootNote).relativeNote;
				}
				else if (type == MaimaiNoteType.SLIDE_MOVE_START) {
					note = (note as MaimaiSlideNoteMoveStart).relativeNote.pattern.relativeNote as MaimaiNote;
				}
				else if (type == MaimaiNoteType.SLIDE) {
					note = (note as MaimaiSlideNote).pattern.relativeNote as MaimaiNote;
				}
				// allNotesから取得したノートをvisualNotesから探す.
				for (int j = 0; j < visualNotes.Length; j++) {
					if (note == visualNotes[j]) {
						nextDisplayNoteIndex = j;
						through = true;
						break;
					}
				}
				if (through) {
					break;
				}
			}
		}
	}

	protected IEnumerator OnGameEnd () {
		if (MiscInformationController.instance.rhythmGameRequesterSceneName == "editor") {
			TrackInformationController.instance.SavingSeekWait ();
			Application.LoadLevel ("SceneScoreEditor");
		}
		else if (MiscInformationController.instance.rhythmGameRequesterSceneName == "tutorial") {
			Application.LoadLevel ("SceneMainMenu");
		}
		else {
			// シンクモードなら相手を待つ.
			if (MiscInformationController.instance.syncMode == 1) {
				SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateHostRhythmGameEndWait", RPCMode.OthersBuffered);
				while (!SyncLobbyNetworkStateController.instance.clientRhythmGameEndWait) {
					yield return null;
				}
			}
			else if (MiscInformationController.instance.syncMode == 2) {
				SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateClientRhythmGameEndWait", RPCMode.Server);
				while (!SyncLobbyNetworkStateController.instance.hostRhythmGameEndWait) {
					yield return null;
				}
			}

			int syncPoint = 0;
			if (syncEvaluater != null) {
				syncPoint = syncEvaluater.syncPercent.ToPercent2().ToInt();
			}
			ResultInformationController.instance.Setup(
				evaluater.GetScore(), evaluater.GetAchievement(),
				evaluater.IsAllPerfect(), evaluater.IsFullCombo(), evaluater.IsNoMiss(),
				syncPoint, evaluater.theoreticalScore,
				evaluater.GetTapScore(), evaluater.GetHoldScore(), evaluater.GetSlideScore(), evaluater.GetBreakScore(),
				evaluater.GetFullTapScore(), evaluater.GetFullHoldScore(), evaluater.GetFullSlideScore(), evaluater.GetBFullBreakScore(), 
				evaluater.GetPerfectCount(), evaluater.GetGreatCount(), evaluater.GetGoodCount(), evaluater.GetMissCount(), evaluater.maxCombo,
				autoPlay, changeBreakType, syncEvaluater != null);
			if (!autoPlay) {
				SetUserExperience ();
				SetAvaterAbility ();
				UserDataController.instance.SaveUserInfo ();
			}
			TrackInformationController.instance.SavingSeekWait ();
			SavingRepeatData ();
			Application.LoadLevel ("SceneResult");
		}
	}

	protected float GetAchievement() {
		if (uiCtrl == null || evaluater == null)
			return 0;
		var type = achievementVisionType;
		switch (type) {
		case ConfigTypes.UpScreenAchievementVision.NORMAL:
			return evaluater.GetAchievement();
		case ConfigTypes.UpScreenAchievementVision.PACE:
			return evaluater.GetPaceAchievement();
		case ConfigTypes.UpScreenAchievementVision.HAZARD_BREAKPACE:
			return evaluater.GetHazardBreakPaceAchievement();
		case ConfigTypes.UpScreenAchievementVision.HAZARD:
			return evaluater.GetHazardAchievement();
		case ConfigTypes.UpScreenAchievementVision.T_NORMAL:
			return evaluater.GetTheoreticalAchievement();
		case ConfigTypes.UpScreenAchievementVision.T_PACE:
			return evaluater.GetTheoreticalPaceAchievement();
		case ConfigTypes.UpScreenAchievementVision.T_HAZARD:
			return evaluater.GetTheoreticalHazardAchievement();
		}
		return 0;
	}
	
	protected void SetBgInfoVisualizeTargetInit () {
		float initValue = 0;
		float stepValue = bgInfoStepValue;
		if (bgInfoType == ConfigTypes.BackGroundInfomation.COMBO) {
			initValue = 2;
			bgInfoValue = evaluater.combo;
		}
		if (bgInfoType == ConfigTypes.BackGroundInfomation.PCOMBO) {
			initValue = 2;
			bgInfoValue = evaluater.pcombo;
		}
		if (bgInfoType == ConfigTypes.BackGroundInfomation.BPCOMBO) {
			initValue = 2;
			bgInfoValue = evaluater.bpcombo;
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.ACHIEVEMENT ||
		         bgInfoType == ConfigTypes.BackGroundInfomation.SCORE) {
			initValue = 0;
			bgInfoValue = evaluater.GetAchievement().ToPercent2();
		}
		bgInfoOldValue = bgInfoValue;
		if (bgInfoValue < initValue) {
			bgInfoVisualizeTarget = initValue;
			bgInfoNameSpriteCtrl.gameObject.SetActive (false);
		}
		else {
			float target = bgInfoValue.ToInt() / 10 * 10;
			if (target == bgInfoValue) {
				bgInfoVisualizeTarget = bgInfoValue;
			}
			else {
				bgInfoVisualizeTarget = target + stepValue;
			}
			bgInfoNameSpriteCtrl.gameObject.SetActive (!fullComboAnimationIsDone);
		}
		bgInfoFirstTarget = bgInfoVisualizeTarget;
	}

	protected bool SetBgInfoVisualizeTarget () {
		bool ret = false;
		float initValue = 0;
		float stepValue = bgInfoStepValue;
		if (bgInfoType == ConfigTypes.BackGroundInfomation.COMBO) {
			initValue = 2;
			bgInfoValue = evaluater.combo;
		}
		if (bgInfoType == ConfigTypes.BackGroundInfomation.PCOMBO) {
			initValue = 2;
			bgInfoValue = evaluater.pcombo;
		}
		if (bgInfoType == ConfigTypes.BackGroundInfomation.BPCOMBO) {
			initValue = 2;
			bgInfoValue = evaluater.bpcombo;
		}
		else if (bgInfoType == ConfigTypes.BackGroundInfomation.ACHIEVEMENT ||
			bgInfoType == ConfigTypes.BackGroundInfomation.SCORE) {
			initValue = 0;
			bgInfoValue = evaluater.GetAchievement().ToPercent2();
		}
		if (bgInfoOldValue > bgInfoValue) {
			bgInfoVisualizeTarget = initValue;
			ret = true;
		}
		else if (bgInfoVisualizeTarget <= bgInfoValue && bgInfoType != ConfigTypes.BackGroundInfomation.OFF) {
			bgInfoVisualizeTarget = bgInfoVisualizeTarget.ToInt() / 10 * 10 + stepValue;
			ret = true;
		}
		if (bgInfoValue < bgInfoFirstTarget) {
			if (bgInfoNameSpriteCtrl.gameObject.activeSelf) {
				bgInfoNameSpriteCtrl.gameObject.SetActive (false);
			}
		}
		else if (!bgInfoNameSpriteCtrl.gameObject.activeSelf) {
			bgInfoNameSpriteCtrl.gameObject.SetActive (!fullComboAnimationIsDone);
		}
		bgInfoOldValue = bgInfoValue;
		return ret;
	}

	public void SetUserExperience () {
		float syncRate = syncEvaluater != null ? syncEvaluater.syncPercent : 0.0f;
		UserDataController.instance.SetUserExperience (evaluater.GetAchievement (), evaluater.GetNotesCountTotal (), evaluater.GetMissCount (), evaluater.IsAllPerfect (), evaluater.IsFullCombo(), syncRate);
		ProfileInformationController.instance.PlayerLevelReload ();
	}

	public void SetAvaterAbility () {
		UserDataController.instance.SetAvaterAbility (
			evaluater.GetTapCount(), evaluater.GetTapScore(), evaluater.GetFullTapScore(),
			evaluater.GetHoldCount(), evaluater.GetHoldScore(), evaluater.GetFullHoldScore(),
			evaluater.GetSlideCount(), evaluater.GetSlideScore(), evaluater.GetFullSlideScore(),
			evaluater.GetBreakCount(), evaluater.GetBreakScore(), evaluater.GetFullBreakScore());
		ProfileInformationController.instance.PlayerAvaterAbilityReload ();
	}

	public virtual void OnPauseButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		PauseGame ();
	}

	public virtual void OnResumeButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		ResumeGame ();
	}

	public virtual void OnRetryButtonClick() {
		Utility.SoundEffectManager.Play ("se_decide");
		RetryGame ();
	}
	
	public virtual void OnExitButtonClick() {
		Utility.SoundEffectManager.Play ("se_decide");
		ExitGame ();
	}

	public void OnSeekWaitSettingButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		uiCtrl.seekInputLabel.text = ((float)TrackInformationController.instance.seek / 1000.0f).ToString ();
		uiCtrl.waitInputLabel.text = ((float)TrackInformationController.instance.wait / 1000.0f).ToString ();
		uiCtrl.seekWaitSettingDialog.SetActive (true);
	}

	public void OnSeekWaitSettingOKButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		TrackInformationController.instance.SetSeekWait (uiCtrl.seekInputLabel.text, uiCtrl.waitInputLabel.text);
		uiCtrl.seekWaitSettingDialog.SetActive (false);
		RetryGame ();
	}

	public void OnSeekWaitSettingCancelButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		uiCtrl.seekWaitSettingDialog.SetActive (false);
	}

	public void OnRepeatSettingButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		uiCtrl.repeatStartSlider.value = repeatStartTimeRate;
		uiCtrl.repeatInputLabel.text = (repeatTime / 1000L).ToString ();
		uiCtrl.repeatSettingDialog.SetActive (true);
	}
	
	public void OnRepeatSettingOKButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		repeatStartTimeRate = uiCtrl.repeatStartSlider.value;
		//restartTime = (finishTime * repeatStartTimeRate).ToLong ();
		int time;
		if (string.IsNullOrEmpty (uiCtrl.repeatInputLabel.text)) {
			repeatTime = 0;
		}
		else if (int.TryParse (uiCtrl.repeatInputLabel.text, out time)) {
			if (time < 0) {
				repeatTime = 0;
			}
			else {
				repeatTime = time * 1000L;
			}
		}
		UserDataController.instance.user.repeat_start_time_rate = repeatStartTimeRate;
		UserDataController.instance.user.repeat_time = (int)(repeatTime / 1000L);
		MiscInformationController.instance.repeatOptionChanged = true;
		uiCtrl.repeatSettingDialog.SetActive (false);
		if (autoPlay) {
			RetryGame ();
		}
	}
	
	public void OnRepeatSettingCancelButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		uiCtrl.repeatSettingDialog.SetActive (false);
	}

	private void SavingRepeatData () {
		if (MiscInformationController.instance.repeatOptionChanged) {
			UserDataController.instance.user.repeat_start_time_rate = repeatStartTimeRate;
			UserDataController.instance.user.repeat_time = (int)(repeatTime / 1000L);
			UserDataController.instance.SaveUserInfo ();
			MiscInformationController.instance.repeatOptionChanged = false;
		}
	}
	
	
	// アプリケーションを中断したらタイマーをポーズする.
	protected virtual void OnApplicationPause(bool pause) {
		if (pause == true) {
			if (MiscInformationController.instance.syncMode > 0) {
				timer.pause = true;
				Network.Disconnect ();
			}
			else {
				uiCtrl.pauseButtonHolding = false;
				PauseGame ();
				TrackInformationController.instance.SavingSeekWait ();
				SavingRepeatData ();
			}
		}
	}

	// サーバーから切断されたらポーズする.
	protected virtual void OnDisconnectedFromServer () {
		timer.pause = true;
	}

	// クライアントが切断されたらポーズする.
	protected virtual void OnPlayerDisconnected () {
		timer.pause = true;
	}

	private void PauseGame () {
		timer.pause = true;
		if (!autoPlay) {
			pauseTimer = 1.0f;
		}
		uiCtrl.pauseDialog.SetActive (true);
	}

	private void ResumeGame () {
		timer.pause = false;
		if (!autoPlay) {
			pauseTimer = 0.0f;
		}
		uiCtrl.pauseDialog.SetActive (false);
	}
	
	private void RetryGame () {
		uiCtrl.pauseDialog.SetActive (false);
		if (!autoPlay && MiscInformationController.instance.rhythmGameRequesterSceneName != "editor" && MiscInformationController.instance.rhythmGameRequesterSceneName == "tutorial") {
			SetAvaterAbility ();
		}
		Application.LoadLevel ("SceneRhythmGame");
	}
	
	private void ExitGame () {
		uiCtrl.pauseDialog.SetActive (false);
		if (!autoPlay && MiscInformationController.instance.rhythmGameRequesterSceneName != "editor" && MiscInformationController.instance.rhythmGameRequesterSceneName != "tutorial") {
			SetAvaterAbility ();
			UserDataController.instance.SaveUserInfo ();
		}
		if (MiscInformationController.instance.rhythmGameRequesterSceneName != "tutorial") {
			TrackInformationController.instance.SavingSeekWait ();
		}
		if (MiscInformationController.instance.rhythmGameRequesterSceneName != "editor" && MiscInformationController.instance.rhythmGameRequesterSceneName != "tutorial") {
			SavingRepeatData ();
		}
		BackSceneLoad ();
	}

	protected virtual void BackSceneLoad() {
		if (MiscInformationController.instance.rhythmGameRequesterSceneName == "editor") {
			Application.LoadLevel ("SceneScoreEditor");
		}
		else if (MiscInformationController.instance.rhythmGameRequesterSceneName == "tutorial") {
			Application.LoadLevel ("SceneMainMenu");
		}
		else {
			MiscInformationController.instance.selectMusicVoiceCallRequest = true;
			Application.LoadLevel ("SceneSelectTrack");
		}
	}

}
