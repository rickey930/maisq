using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RhythmGameLibrary.Score;

namespace MaimaiScore {
	#region Global
	[System.Serializable]
	public class Each : ScoreNoteData {
		public INote[] notes;
		protected Each () : base () {
		}
		public static Each Upgrade (ScoreNoteData basedata, INote[] notes) {
			return new Each () {
				change_bpm = basedata.change_bpm,
				change_step = basedata.change_step,
				notes = notes,
			};
		}
		public static Each Create(params INote[] notes) {
			return Upgrade (ScoreNoteData.Create (), notes);
		}
		public static Each Create_Bpm(float bpm, params INote[] notes) {
			return Upgrade (ScoreNoteData.Create_Bpm (bpm), notes);
		}
		public static Each Create_Beat(float beat, params INote[] notes) {
			return Upgrade (ScoreNoteData.Create_Beat (beat), notes);
		}
		public static Each Create_Interval(float interval, params INote[] notes) {
			return Upgrade (ScoreNoteData.Create_Interval (interval), notes);
		}
		public static Each Create_Bpm_Beat(float bpm, float beat, params INote[] notes) {
			return Upgrade (ScoreNoteData.Create_Bpm_Beat (bpm, beat), notes);
		}
		public static Each Create_Bpm_Interval(float bpm, float interval, params INote[] notes) {
			return Upgrade (ScoreNoteData.Create_Bpm_Interval (bpm, interval), notes);
		}
		public new virtual Each Clone () {
			var cloneNotes = new INote[notes.Length];
			for (int i = 0; i < cloneNotes.Length; i++) {
				cloneNotes[i] = notes[i].Clone ();
			}
			return Upgrade (base.Clone(), cloneNotes);
		}
	}
	
	#endregion
	#region Number
	[System.Serializable]
	public class Number {
		public int number;
		protected Number () {
		}
		private Number(int init) {
			this.number = init;
		}
		private static Number Constractor (int init) {
			return new Number (init);
		}
		public static Number Create (int init) {
			return Constractor (init);
		}
		public static Number FromMaiScript (string script) {
			int init;
			if (int.TryParse (script, out init)) {
				return Constractor (init);
			}
			return null;
		}
		public int GetValue () {
			return this.number;
		}
		public virtual Number Clone () {
			return Create (this.GetValue ());
		}
		public Number MathAddition (Number a) {
			if (a == null) return null;
			this.number = this.GetValue() + a.GetValue();
			return this;
		}
		public Number MathSubtraction (Number a) {
			if (a == null) return null;
			this.number = this.GetValue() - a.GetValue();
			return this;
		}
		public Number MathMultiplication (Number a) {
			if (a == null) return null;
			this.number = this.GetValue() * a.GetValue();
			return this;
		}
		public Number MathDivision (Number a) {
			if (a == null) return null;
			if (a.GetValue() != 0) {
				this.number = this.GetValue() / a.GetValue();
			}
			else {
				this.number = 0;
			}
			return this;
		}
		public Number MathPower (Number a) {
			if (a == null) return null;
			this.number = Mathf.Pow(this.GetValue(), a.GetValue()).ToInt();
			return this;
		}
		public Number MathModulo (Number a) {
			if (a == null) return null;
			if (a.GetValue() != 0) {
				this.number = this.GetValue () % a.GetValue ();
			}
			else {
				this.number = 0;
			}
			return this;
		}
		public static Number MathAddition (Number a, Number b) {
			return a.Clone().MathAddition(b);
		}
		public static Number MathSubtraction (Number a, Number b) {
			return a.Clone().MathSubtraction(b);
		}
		public static Number MathMultiplication (Number a, Number b) {
			return a.Clone().MathMultiplication(b);
		}
		public static Number MathDivision (Number a, Number b) {
			return a.Clone().MathDivision(b);
		}
		public static Number MathPower (Number a, Number b) {
			return a.Clone().MathPower(b);
		}
		public static Number MathModulo (Number a, Number b) {
			return a.Clone().MathModulo(b);
		}
	}
	
	#endregion
	#region Value
	[System.Serializable]
	public class Value {
		public float value;
		protected Value () {
		}
		private Value(float init) {
			this.value = init;
		}
		private static Value Constractor (float init) {
			return new Value (init);
		}
		public static Value Create (float init) {
			return Constractor (init);
		}
		public static Value FromMaiScript (string script) {
			float init;
			if (float.TryParse (script, out init)) {
				return Constractor (init);
			}
			return null;
		}
		public float GetValue () {
			return this.value;
		}
		public virtual Value Clone () {
			return Create (this.GetValue ());
		}
		public Value MathAddition (Value a) {
			if (a == null) return null;
			this.value = this.GetValue() + a.GetValue();
			return this;
		}
		public Value MathSubtraction (Value a) {
			if (a == null) return null;
			this.value = this.GetValue() - a.GetValue();
			return this;
		}
		public Value MathMultiplication (Value a) {
			if (a == null) return null;
			this.value = this.GetValue() * a.GetValue();
			return this;
		}
		public Value MathDivision (Value a) {
			if (a == null) return null;
			if (a.GetValue() != 0) {
				this.value = this.GetValue() / a.GetValue();
			}
			else {
				this.value = 0;
			}
			return this;
		}
		public Value MathPower (Value a) {
			if (a == null) return null;
			this.value = Mathf.Pow(this.GetValue(), a.GetValue());
			return this;
		}
		public static Value MathAddition (Value a, Value b) {
			return a.Clone().MathAddition(b);
		}
		public static Value MathSubtraction (Value a, Value b) {
			return a.Clone().MathSubtraction(b);
		}
		public static Value MathMultiplication (Value a, Value b) {
			return a.Clone().MathMultiplication(b);
		}
		public static Value MathDivision (Value a, Value b) {
			return a.Clone().MathDivision(b);
		}
		public static Value MathPower (Value a, Value b) {
			return a.Clone().MathPower(b);
		}
		public static Value SensorDegree (Button button) {
			if (button == null) return null;
			return Create (Constants.instance.GetPieceDegree (button.GetIndex()));
		}
		public static Value SensorDegreeDistanceClockwise (Button button1, Button button2) {
			if (button1 == null || button2 == null) return null;
			int startSensor = button1.GetIndex ();
			int targetSensor = button2.GetIndex ();
			if (startSensor > targetSensor) {
				float startDeg = Constants.instance.GetPieceDegree (startSensor);
				float targetDeg = Constants.instance.GetPieceDegree (targetSensor) + 360f;
				return Create (targetDeg - startDeg);
			}
			else if (startSensor < targetSensor) {
				float startDeg = Constants.instance.GetPieceDegree (startSensor);
				float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
				return Create (targetDeg - startDeg);
			}
			return Create (360.0f);
		}
		public static Value SensorDegreeDistanceCounterClockwise (Button button1, Button button2) {
			if (button1 == null || button2 == null) return null;
			int startSensor = button1.GetIndex ();
			int targetSensor = button2.GetIndex ();
			if (startSensor > targetSensor) {
				float startDeg = Constants.instance.GetPieceDegree (startSensor);
				float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
				return Create (-(startDeg - targetDeg));
			}
			else if (startSensor < targetSensor) {
				float startDeg = Constants.instance.GetPieceDegree (startSensor) + 360f;
				float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
				return Create (-(startDeg - targetDeg));
			}
			return Create (-360.0f);
		}
		public static Value SensorDegreeDistanceRight (Button button1, Button button2) {
			if (button1 == null || button2 == null) return null;
			int startSensor = button1.GetValue ();
			if (startSensor > 6 || startSensor < 3) {
				return SensorDegreeDistanceClockwise(button1, button2);
			}
			return SensorDegreeDistanceCounterClockwise(button1, button2);
		}
		public static Value SensorDegreeDistanceLeft (Button button1, Button button2) {
			if (button1 == null || button2 == null) return null;
			int startSensor = button1.GetValue ();
			if (startSensor > 6 || startSensor < 3) {
				return SensorDegreeDistanceCounterClockwise(button1, button2);
			}
			return SensorDegreeDistanceClockwise(button1, button2);
		}
		public static Value AngleX (Value degree, Value radius) {
			if (degree == null || radius == null) return null;
			var pos = Position.Angle (degree, radius);
			if (pos == null) return null;
			return Create (pos.x);
		}
		public static Value AngleY (Value degree, Value radius) {
			if (degree == null || radius == null) return null;
			var pos = Position.Angle (degree, radius);
			if (pos == null) return null;
			return Create (pos.y);
		}
		public static Value AngleX (Value degree) {
			if (degree == null) return null;
			var pos = Position.Angle (degree);
			if (pos == null) return null;
			return Create (pos.x);
		}
		public static Value AngleY (Value degree) {
			if (degree == null) return null;
			var pos = Position.Angle (degree);
			if (pos == null) return null;
			return Create (pos.y);
		}
		public static Value OuterSensorPositionX (Button button) {
			if (button == null) return null;
			var pos = Position.OuterSensorPosition (button);
			if (pos == null) return null;
			return Create (pos.x);
		}
		public static Value OuterSensorPositionY (Button button) {
			if (button == null) return null;
			var pos = Position.OuterSensorPosition (button);
			if (pos == null) return null;
			return Create (pos.y);
		}
		public static Value InnerSensorPositionX (Button button) {
			if (button == null) return null;
			var pos = Position.InnerSensorPosition (button);
			if (pos == null) return null;
			return Create (pos.x);
		}
		public static Value InnerSensorPositionY (Button button) {
			if (button == null) return null;
			var pos = Position.InnerSensorPosition (button);
			if (pos == null) return null;
			return Create (pos.y);
		}
		public static Value CenterSensorPositionX () {
			var pos = Position.CenterSensorPosition ();
			if (pos == null) return null;
			return Create (pos.x);
		}
		public static Value CenterSensorPositionY () {
			var pos = Position.CenterSensorPosition ();
			if (pos == null) return null;
			return Create (pos.y);
		}
		public static Value OuterSensorRadius () {
			return Create (1.0f);
		}
		public static Value InnerSensorRadius () {
			var clossExLines = CircleCalculator.LinesIntersect(
				CircleCalculator.PointOnCircle(Vector2.zero, 1, Constants.instance.GetPieceDegree(0)),
				CircleCalculator.PointOnCircle(Vector2.zero, 1, Constants.instance.GetPieceDegree(3)),
				CircleCalculator.PointOnCircle(Vector2.zero, 1, Constants.instance.GetPieceDegree(6)),
				CircleCalculator.PointOnCircle(Vector2.zero, 1, Constants.instance.GetPieceDegree(2))
				);
			var radius = CircleCalculator.PointToPointDistance(Vector2.zero, clossExLines);
			return Create (radius);
		}
		public static Value CenterSensorRadius () {
			return Create (0.0f);
		}
		public static Value SimaiPPTurnDegree (Button start, Button target) {
			int complementary1 = ((target.GetIndex() + 16 - (start.GetIndex() + 3)) % 8 - 7) * -1;

			int complementary2 = (complementary1 - 4) * -1;
			int complementary3;
			if (complementary2 == 0) {
				complementary3 = 0;
			}
			else {
				complementary3 = (int)(Mathf.Pow (2, Mathf.Abs (complementary2) - 1));
			}

			float ret = -270.0f - (22.5f * (0xf & complementary3) * (complementary2 < 0 ? 1 : -1)) - (45.0f / 8.0f * complementary1);

			return Value.Create (ret);
		}
		public static Value SimaiQQTurnDegree (Button start, Button target) {
			int complementary1 = ((target.GetIndex() + 16 - (start.GetIndex() - 2)) % 8);
			
			int complementary2 = (complementary1 - 4) * -1;
			int complementary3;
			if (complementary2 == 0) {
				complementary3 = 0;
			}
			else {
				complementary3 = (int)(Mathf.Pow (2, Mathf.Abs (complementary2) - 1));
			}
			
			float ret = 270.0f - (22.5f * (0xf & complementary3) * (complementary2 < 0 ? -1 : 1)) + (45.0f / 8.0f * complementary1);
			
			return Value.Create (ret);
		}
	}
	
	#endregion
	#region Flag
	[System.Serializable]
	public class Flag {
		public bool flag;
		protected Flag () {
		}
		private Flag(bool init) {
			this.flag = init;
		}
		private static Flag Constractor (bool init) {
			return new Flag (init);
		}
		public static Flag Create (bool init) {
			return Constractor (init);
		}
		public static Flag FromMaiScript (string script) {
			string low = script.ToLower ();
			bool init;
			if (bool.TryParse (low, out init)) {
				return Constractor (init);
			}
			switch (low) {
			case "true":
			case "on":
			case "yes":
				return Constractor (true);
			case "false":
			case "off":
			case "no":
				return Constractor (false);
			}
			return null;
		}
		public bool GetValue () {
			return this.flag;
		}
		public virtual Flag Clone () {
			return Create (this.GetValue ());
		}
	}
	
	#endregion
	#region Position
	[System.Serializable]
	public class Position {
		public float x;
		public float y;
		protected Position () {
		}
		private Position (float x, float y) {
			this.x = x;
			this.y = y;
		}
		private static Position Constractor (float x, float y) {
			return new Position (x, y);
		}
		public static Position Create (float x, float y) {
			return Constractor (x, y);
		}
		public static Position Create (Vector2 pos) {
			return Constractor(pos.x, pos.y);
		}
		public static Position FromMaiScript (Value x, Value y) {
			if (x == null || y == null) return null;
			return Constractor (x.GetValue(), y.GetValue());
		}
		public Vector2 GetValue () {
			return new Vector2(x, y);
		}
		public virtual Position Clone () {
			return Create (this.GetValue ());
		}
		public static Position Angle (Value degree, Value radius) {
			if (degree == null || radius == null) return null;
			// 引数は右手座標だが、Unityでは回転が左手座標系だとか0度は(-1.0,0)であることだとかを考慮して、90度回したりy値をマイナスにしたりして加工する.
			return Create (
				Mathf.Sin (CircleCalculator.ToRadian(degree.GetValue() + 90)) * radius.GetValue(), 
				-Mathf.Cos (CircleCalculator.ToRadian(degree.GetValue() + 270)) * radius.GetValue()
				);
		}
		public static Position Angle (Value degree) {
			if (degree == null) return null;
			return Angle (degree, Value.Create (1.0f));
		}
		public static Position OuterSensorPosition (Button button) {
			if (button == null) return null;
			var pos = CircleCalculator.PointOnCircle (Vector2.zero, 1, Constants.instance.GetPieceDegree (button.GetIndex ()));
			return Create (new Vector2(pos.x, -pos.y));
		}
		public static Position InnerSensorPosition (Button button) {
			if (button == null) return null;
			var radius = Value.InnerSensorRadius ().GetValue();
			var pos = CircleCalculator.PointOnCircle (Vector2.zero, radius, Constants.instance.GetPieceDegree (button.GetIndex ()));
			return Create (new Vector2(pos.x, -pos.y));
		}
		public static Position CenterSensorPosition () {
			return Create (Vector2.zero);
		}
		private const float SimaiPPQQFirstStraightTargetPositionAdjustDegree = 22.5f * 0.65f;
		private const float SimaiPPQQFirstStraightTargetPositionRadius = 0.15f;
		/// <summary>
		/// ppスライドにおいて始点から直線を引く際に目指す位置.
		/// </summary>
		/// <param name="start">始点</param>
		public static Position SimaiPPFirstStraightTargetPosition (Button start) {
			return Angle (Value.SensorDegree (start).MathSubtraction (Value.Create (SimaiPPQQFirstStraightTargetPositionAdjustDegree)), Value.Create (SimaiPPQQFirstStraightTargetPositionRadius));
		}
		/// <summary>
		/// qqスライドにおいて始点から直線を引く際に目指す位置.
		/// </summary>
		/// <param name="start">始点</param>
		public static Position SimaiQQFirstStraightTargetPosition (Button start) {
			return Angle (Value.SensorDegree (start).MathAddition (Value.Create (SimaiPPQQFirstStraightTargetPositionAdjustDegree)), Value.Create (SimaiPPQQFirstStraightTargetPositionRadius));
		}
		/// <summary>
		/// simai内部におけるppスライドやqqスライドの中心点のA～H。 □に8つの頂点を均等に並べたとき、1番が右上で、時計回りに番号が増える.
		/// </summary>
		/// <param name="button">始点</param>
		public static Position SimaiPPQQCenterPosition (Button button) {
			if (button == null) return null;
			var radius = 0.5f; //(205.5f - 140.0f) / 140.0f;
			var pos = CircleCalculator.PointOnCircle (Vector2.zero, radius, 45.0f * button.GetValue ());
			return Create (new Vector2 (pos.x, -pos.y));
		}
		/// <summary>
		/// ppスライドにおいて円を描く際に必要とする中心点.
		/// </summary>
		/// <param name="button">始点</param>
		public static Position SimaiPPCenterPosition (Button button) {
			return SimaiPPQQCenterPosition (Button.Create((button.GetIndex() + 1) % 8 + 1));
		}
		/// <summary>
		/// qqスライドにおいて円を描く際に必要とする中心点.
		/// </summary>
		/// <param name="button">始点</param>
		public static Position SimaiQQCenterPosition (Button button) {
			return SimaiPPQQCenterPosition (Button.Create((button.GetIndex() + 6) % 8 + 1));
		}
		public void Turn (int amount) {
			Turn (Constants.instance.GetPieceDegree (amount) - 22.5f);
		}
		public void Turn (float degree) {
			// 左手座標.
			Vector2 pos = this.GetValue ();
			// 右手座標.
			pos = Constants.instance.ToLeftHandedCoordinateSystemPosition (pos);
			float addDeg = degree;
			float deg = CircleCalculator.PointToDegree (pos) + 180.0f;
			float radius = CircleCalculator.PointToPointDistance (Vector2.zero, pos);
			var turned = CircleCalculator.PointOnCircle (Vector2.zero, radius, deg + addDeg);
			// 左手座標.
			var ret = Constants.instance.ToLeftHandedCoordinateSystemPosition (turned);
			this.x = ret.x;
			this.y = ret.y;
		}
		public void MirrorHolizontal () {
			this.x *= -1;
		}
		public void MirrorVertical () {
			this.y *= -1;
		}
		public static Position Turn (Position pos, Number amount) {
			if (pos == null || amount == null) return null;
			var ret = pos.Clone ();
			ret.Turn (amount.GetValue ());
			return ret;
		}
		public static Position Turn (Position pos, Value degree) {
			if (pos == null || degree == null) return null;
			var ret = pos.Clone ();
			ret.Turn (degree.GetValue ());
			return ret;
		}
		public static Position MirrorHolizontal (Position pos) {
			if (pos == null) return null;
			var ret = pos.Clone ();
			ret.MirrorHolizontal ();
			return ret;
		}
		public static Position MirrorVertical (Position pos) {
			if (pos == null) return null;
			var ret = pos.Clone ();
			ret.MirrorVertical ();
			return ret;
		}
	}
	
	#endregion
	#region Color
	public class Color {
		public int r;
		public int g;
		public int b;
		public int a;
		protected Color () {
		}
		private Color (int r, int g, int b, int a) {
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = a;
		}
		private static Color Constractor (int r, int g, int b, int a) {
			if (r < 0)
				r = 0;
			if (r > 255)
				r = 255;
			if (g < 0)
				g = 0;
			if (g > 255)
				g = 255;
			if (b < 0)
				b = 0;
			if (b > 255)
				b = 255;
			if (a < 0)
				a = 0;
			if (a > 255)
				a = 255;
			return new Color (r, g, b, a);
		}
		public static Color CreateRGBA (int r, int g, int b, int a) {
			return Constractor (r, g, b, a);
		}
		public static Color CreateARGB (int a, int r, int g, int b) {
			return Constractor (r, g, b, a);
		}
		public static Color CreateRGB (int r, int g, int b) {
			return Constractor (r, g, b, 255);
		}
		public static Color CreateRGBA (float r, float g, float b, float a) {
			return Constractor ((int)(255 * r), (int)(255 * g), (int)(255 * b), (int)(255 * a));
		}
		public static Color CreateARGB (float a, float r, float g, float b) {
			return Constractor ((int)(255 * r), (int)(255 * g), (int)(255 * b), (int)(255 * a));
		}
		public static Color CreateRGB (float r, float g, float b) {
			return Constractor ((int)(255 * r), (int)(255 * g), (int)(255 * b), 255);
		}
		public static Color FromMaiScriptRGBA (Number r, Number g, Number b, Number a) {
			if (r == null || g == null || b == null || a == null) {
				return null;
			}
			return Constractor (r.GetValue(), g.GetValue(), b.GetValue(), a.GetValue());
		}
		public static Color FromMaiScriptARGB (Number a, Number r, Number g, Number b) {
			if (r == null || g == null || b == null || a == null) {
				return null;
			}
			return Constractor (r.GetValue(), g.GetValue(), b.GetValue(), a.GetValue());
		}
		public static Color FromMaiScriptRGB (Number r, Number g, Number b) {
			if (r == null || g == null || b == null) {
				return null;
			}
			return Constractor (r.GetValue(), g.GetValue(), b.GetValue(), 255);
		}
		public static Color FromMaiScriptRGBA (Value r, Value g, Value b, Value a) {
			if (r == null || g == null || b == null || a == null) {
				return null;
			}
			return Constractor ((int)(255 * r.GetValue()), (int)(255 * g.GetValue()), (int)(255 * b.GetValue()), (int)(255 * a.GetValue()));
		}
		public static Color FromMaiScriptARGB (Value a, Value r, Value g, Value b) {
			if (r == null || g == null || b == null || a == null) {
				return null;
			}
			return Constractor ((int)(255 * r.GetValue()), (int)(255 * g.GetValue()), (int)(255 * b.GetValue()), (int)(255 * a.GetValue()));
		}
		public static Color FromMaiScriptRGB (Value r, Value g, Value b) {
			if (r == null || g == null || b == null) {
				return null;
			}
			return Constractor ((int)(255 * r.GetValue()), (int)(255 * g.GetValue()), (int)(255 * b.GetValue()), 255);
		}
		public UnityEngine.Color GetUnityColor () {
			return new UnityEngine.Color ((float)r / 255.0f, (float)g / 255.0f, (float)b / 255.0f, (float)a / 255.0f); 
		}
	}
	
	#endregion
	#region Button
	[System.Serializable]
	public class Button {
		public int button;
		protected Button () {
		}
		private Button (int value) {
			button = value;
		}
		private static Button Constractor (int value) {
			if (value > 0 && value < 9) {
				return new Button (value);
			}
			return null;
		}
		public static Button Create (int value) {
			return Constractor (value);
		}
		public static Button FromMaiScript (string script) {
			int value;
			if (int.TryParse (script, out value)) {
				return Constractor (value);
			}
			return null;
		}
		public static Button FromMaiScript (Number number) {
			if (number != null) {
				int n = number.GetValue ();
				while (n < 1) n += 8;
				if (n > 8) n = (n - 1) % 8 + 1;
				return Constractor (n);
			}
			return null;
		}
		public int GetValue () {
			return this.button;
		}
		public int GetIndex () {
			return this.GetValue () - 1;
		}
		public virtual Button Clone () {
			return Constractor (this.GetValue ());
		}
		public void Turn (int amount) {
			while (amount < 0) amount += 8;
			this.button = (this.GetValue () - 1 + amount) % 8 + 1;
		}
		public void MirrorHolizontal () {
			this.button = (9 - this.GetValue ());
		}
		public void MirrorVertical () {
			this.Turn (4);
			this.MirrorHolizontal ();
		}
		public static Button Turn (Button button, Number amount) {
			if (button == null || amount == null) return null;
			var ret = button.Clone ();
			ret.Turn (amount.GetValue());
			return ret;
		}
		public static Button MirrorHolizontal (Button button) {
			if (button == null) return null;
			var ret = button.Clone ();
			ret.MirrorHolizontal ();
			return ret;
		}
		public static Button MirrorVertical (Button button) {
			if (button == null) return null;
			var ret = button.Clone ();
			ret.MirrorVertical ();
			return ret;
		}
	}
	
	#endregion
	#region INote
	public interface INote {
		INote Clone();
		void Turn(int amount);
		void MirrorHolizontal();
		void MirrorVertical();
		NoteOption option { get; set; }
	}
	
	[System.Serializable]
	public class NoteTap : INote{
		public Button button;
		public NoteOption option { get; set; }
		protected NoteTap () {
		}
		private NoteTap (Button button) {
			this.button = button;
		}
		private static NoteTap Constractor (Button button) {
			if (button == null) return null;
			return new NoteTap (button);
		}
		public static NoteTap Create (int button) {
			return Constractor (Button.Create (button));
		}
		public static NoteTap FromMaiScript (Button button) {
			if (button == null) return null;
			return Constractor (button);
		}
		public virtual INote Clone () {
			var ret = new NoteTap (this.button.Clone());
			if (this.option != null) {
				ret.option = this.option.Clone ();
			}
			return ret;
		}
		public NoteBreak ToBreak () {
			return NoteBreak.FromMaiScript (this.button);
		}
		public void Turn (int amount) {
			this.button.Turn (amount);
		}
		public void MirrorHolizontal () {
			this.button.MirrorHolizontal ();
		}
		public void MirrorVertical () {
			this.button.MirrorVertical ();
		}
		public static NoteTap Turn (NoteTap note, Number amount) {
			if (note == null || amount == null) return null;
			var ret = note.Clone () as NoteTap;
			ret.Turn (amount.GetValue());
			return ret;
		}
		public static NoteTap MirrorHolizontal (NoteTap note) {
			if (note == null) return null;
			var ret = note.Clone () as NoteTap;
			ret.MirrorHolizontal ();
			return ret;
		}
		public static NoteTap MirrorVertical (NoteTap note) {
			if (note == null) return null;
			var ret = note.Clone () as NoteTap;
			ret.MirrorVertical ();
			return ret;
		}
	}
	
	[System.Serializable]
	public class NoteHold : INote{
		public Button button;
		public IStep step;
		public NoteOption option { get; set; }
		protected NoteHold () {
		}
		private NoteHold (Button button, IStep step) {
			this.button = button;
			this.step = step;
		}
		private static NoteHold Constractor (Button button, IStep step) {
			if (button == null || step == null) return null;
			return new NoteHold (button, step);
		}
		public static NoteHold Create (int button, float beat, float length) {
			return Constractor (Button.Create (button), StepLength.Create(beat, length));
		}
		public static NoteHold Create (int button, float localBpm, float beat, float length) {
			return Constractor (Button.Create (button), StepLocalBpm.Create(localBpm, beat, length));
		}
		public static NoteHold Create (int button, float interval) {
			return Constractor (Button.Create (button), StepInterval.Create(interval));
		}
		public static NoteHold FromMaiScript (Button button, IStep step) {
			if (button == null || step == null) return null;
			return Constractor (button, step);
		}
		public virtual INote Clone () {
			var ret = new NoteHold (this.button.Clone (), this.step.Clone ());
			if (this.option != null) {
				ret.option = this.option.Clone ();
			}
			return ret;
		}
		public void Turn (int amount) {
			this.button.Turn (amount);
		}
		public void MirrorHolizontal () {
			this.button.MirrorHolizontal ();
		}
		public void MirrorVertical () {
			this.button.MirrorVertical ();
		}
		public static NoteHold Turn (NoteHold note, Number amount) {
			if (note == null || amount == null) return null;
			var ret = note.Clone () as NoteHold;
			ret.Turn (amount.GetValue());
			return ret;
		}
		public static NoteHold MirrorHolizontal (NoteHold note) {
			if (note == null) return null;
			var ret = note.Clone () as NoteHold;
			ret.MirrorHolizontal ();
			return ret;
		}
		public static NoteHold MirrorVertical (NoteHold note) {
			if (note == null) return null;
			var ret = note.Clone () as NoteHold;
			ret.MirrorVertical ();
			return ret;
		}
	}
	
	[System.Serializable]
	public class NoteSlide : INote{
		public ISlideHead head;
		public ISlideHeadCustomWait wait;
		public IStep step;
		public ISlidePattern[] patterns;
		public NoteOption option { get; set; }
		protected NoteSlide () {
		}
		private NoteSlide (ISlideHead head, ISlideHeadCustomWait wait, IStep step, ISlidePattern[] patterns) {
			this.head = head;
			this.wait = wait;
			this.step = step;
			this.patterns = patterns;
		}
		private static NoteSlide Constractor (ISlideHead head, ISlideHeadCustomWait wait, IStep step, ISlidePattern[] patterns) {
			if (patterns == null || patterns.Length == 0) return null;
			return new NoteSlide (head, wait, step, patterns);
		}
		public static NoteSlide Create (int button, params ISlidePattern[] patterns) {
			return Constractor (SlideHeadStar.Create(button), null, null, patterns);
		}
		public static NoteSlide Create (int button, ISlideHeadCustomWait wait, params ISlidePattern[] patterns) {
			return Constractor (SlideHeadStar.Create(button), wait, null, patterns);
		}
		public static NoteSlide Create (int button, IStep step, params ISlidePattern[] patterns) {
			return Constractor (SlideHeadStar.Create(button), null, step, patterns);
		}
		public static NoteSlide Create (int button, ISlideHeadCustomWait wait, IStep step, params ISlidePattern[] patterns) {
			return Constractor (SlideHeadStar.Create(button), wait, step, patterns);
		}
		public static NoteSlide Create_Break (int button, params ISlidePattern[] patterns) {
			return Constractor (SlideHeadBreakStar.Create(button), null, null, patterns);
		}
		public static NoteSlide Create_Break (int button, ISlideHeadCustomWait wait, params ISlidePattern[] patterns) {
			return Constractor (SlideHeadBreakStar.Create(button), wait, null, patterns);
		}
		public static NoteSlide Create_Break (int button, IStep step, params ISlidePattern[] patterns) {
			return Constractor (SlideHeadBreakStar.Create(button), null, step, patterns);
		}
		public static NoteSlide Create_Break (int button, ISlideHeadCustomWait wait, IStep step, params ISlidePattern[] patterns) {
			return Constractor (SlideHeadBreakStar.Create(button), wait, step, patterns);
		}
		public static NoteSlide Create_Nothing (params ISlidePattern[] patterns) {
			return Constractor (SlideHeadNothing.Create(), null, null, patterns);
		}
		public static NoteSlide Create_Nothing (ISlideHeadCustomWait wait, params ISlidePattern[] patterns) {
			return Constractor (SlideHeadNothing.Create(), wait, null, patterns);
		}
		public static NoteSlide Create_Nothing (IStep step, params ISlidePattern[] patterns) {
			return Constractor (SlideHeadNothing.Create(), null, step, patterns);
		}
		public static NoteSlide Create_Nothing (ISlideHeadCustomWait wait, IStep step, params ISlidePattern[] patterns) {
			return Constractor (SlideHeadNothing.Create(), wait, step, patterns);
		}
		public static NoteSlide FromMaiScript (ISlideHead head, params ISlidePattern[] patterns) {
			if (patterns.Length == 0) return null;
			return Constractor (head, null, null, patterns);
		}
		public static NoteSlide FromMaiScript (ISlideHead head, ISlideHeadCustomWait wait, params ISlidePattern[] patterns) {
			if (patterns.Length == 0) return null;
			return Constractor (head, wait, null, patterns);
		}
		public static NoteSlide FromMaiScript (ISlideHead head, IStep step, params ISlidePattern[] patterns) {
			if (patterns.Length == 0) return null;
			return Constractor (head, null, step, patterns);
		}
		public static NoteSlide FromMaiScript (ISlideHead head, ISlideHeadCustomWait wait, IStep step, params ISlidePattern[] patterns) {
			if (patterns.Length == 0) return null;
			return Constractor (head, wait, step, patterns);
		}
		public virtual INote Clone () {
			var clones = new ISlidePattern[this.patterns.Length];
			for (int i = 0; i < clones.Length; i++) {
				clones[i] = this.patterns[i].Clone ();
			}
			var head = this.head != null ? this.head.Clone () : null;
			var wait = this.wait != null ? this.wait.Clone () : null;
			var step = this.step != null ? this.step.Clone () : null;
			var ret = new NoteSlide (head, wait, step, clones);
			if (this.option != null) {
				ret.option = this.option.Clone ();
			}
			return ret;
		}
		public void Turn (int amount) {
			head.Turn (amount);
			foreach (var pattern in patterns) {
				pattern.Turn (amount);
			}
		}
		public void MirrorHolizontal () {
			head.MirrorHolizontal ();
			foreach (var pattern in patterns) {
				pattern.MirrorHolizontal ();
			}
		}
		public void MirrorVertical () {
			head.MirrorVertical ();
			foreach (var pattern in patterns) {
				pattern.MirrorVertical ();
			}
		}
		public static NoteSlide Turn (NoteSlide note, Number amount) {
			if (note == null || amount == null) return null;
			var ret = note.Clone () as NoteSlide;
			ret.Turn (amount.GetValue());
			return ret;
		}
		public static NoteSlide MirrorHolizontal (NoteSlide note) {
			if (note == null) return null;
			var ret = note.Clone () as NoteSlide;
			ret.MirrorHolizontal ();
			return ret;
		}
		public static NoteSlide MirrorVertical (NoteSlide note) {
			if (note == null) return null;
			var ret = note.Clone () as NoteSlide;
			ret.MirrorVertical ();
			return ret;
		}
	}
	
	[System.Serializable]
	public class NoteBreak : INote {
		public Button button;
		public NoteOption option { get; set; }
		protected NoteBreak () {
		}
		private NoteBreak (Button button) {
			this.button = button;
		}
		private static NoteBreak Constractor (Button button) {
			if (button == null) return null;
			return new NoteBreak (button);
		}
		public static NoteBreak Create (int button) {
			return Constractor (Button.Create (button));
		}
		public static NoteBreak FromMaiScript (Button button) {
			if (button == null) return null;
			return Constractor (button);
		}
		public virtual INote Clone () {
			var ret = new NoteBreak (this.button.Clone());
			if (this.option != null) {
				ret.option = this.option.Clone ();
			}
			return ret;
		}
		public NoteTap ToTap () {
			return NoteTap.FromMaiScript (this.button);
		}
		public void Turn (int amount) {
			this.button.Turn (amount);
		}
		public void MirrorHolizontal () {
			this.button.MirrorHolizontal ();
		}
		public void MirrorVertical () {
			this.button.MirrorVertical ();
		}
		public static NoteBreak Turn (NoteBreak note, Number amount) {
			if (note == null || amount == null) return null;
			var ret = note.Clone () as NoteBreak;
			ret.Turn (amount.GetValue());
			return ret;
		}
		public static NoteBreak MirrorHolizontal (NoteBreak note) {
			if (note == null) return null;
			var ret = note.Clone () as NoteBreak;
			ret.MirrorHolizontal ();
			return ret;
		}
		public static NoteBreak MirrorVertical (NoteBreak note) {
			if (note == null) return null;
			var ret = note.Clone () as NoteBreak;
			ret.MirrorVertical ();
			return ret;
		}
	}

	[System.Serializable]
	public class NoteMessage : INote {
		public string message;
		public Vector2 position;
		public float scale;
		public UnityEngine.Color color;
		public IStep step;
		public NoteOption option { get; set; }
		protected NoteMessage () {
		}
		private NoteMessage (string message, Vector2 position, float scale, UnityEngine.Color color, IStep step) {
			this.message = message;
			this.position = position;
			this.scale = scale;
			this.color = color;
			this.step = step;
		}
		private static NoteMessage Constractor (string message, Vector2 position, float scale, UnityEngine.Color color, IStep step) {
			if (step == null) return null;
			return new NoteMessage (message, position, scale, color, step);
		}
		public static NoteMessage Create (string message, Vector2 position, float scale, UnityEngine.Color color, IStep step) {
			return Constractor (message, position, scale, color, step);
		}
		public static NoteMessage FromMaiScript (string message, Position position, Value scale, MaimaiScore.Color color, IStep step) {
			if (message == null || position == null || scale == null || color == null || step == null) return null;
			return Constractor (message, position.GetValue(), scale.GetValue(), color.GetUnityColor(), step);
		}
		public virtual INote Clone () {
			var ret = new NoteMessage (this.message, this.position, this.scale, this.color, this.step);
			if (this.option != null) {
				ret.option = this.option.Clone ();
			}
			return ret;
		}
		public void Turn (int amount) {
		}
		public void MirrorHolizontal () {
		}
		public void MirrorVertical () {
		}
	}
	
	[System.Serializable]
	public class NoteScrollMessage : INote {
		public string message;
		public float y;
		public float scale;
		public UnityEngine.Color color;
		public IStep step;
		public NoteOption option { get; set; }
		protected NoteScrollMessage () {
		}
		private NoteScrollMessage (string message, float y, float scale, UnityEngine.Color color, IStep step) {
			this.message = message;
			this.y = y;
			this.scale = scale;
			this.color = color;
			this.step = step;
		}
		private static NoteScrollMessage Constractor (string message, float y, float scale, UnityEngine.Color color, IStep step) {
			if (step == null) {
				return null;
			}
			return new NoteScrollMessage (message, y, scale, color, step);
		}
		public static NoteScrollMessage Create (string message, float y, float scale, UnityEngine.Color color, IStep step) {
			return Constractor (message, y, scale, color, step);
		}
		public static NoteScrollMessage FromMaiScript (string message, Value y, Value scale, MaimaiScore.Color color, IStep step) {
			if (message == null || y == null || scale == null || color == null || step == null) return null;
			return Constractor (message, y.GetValue(), scale.GetValue(), color.GetUnityColor(), step);
		}
		public virtual INote Clone () {
			var ret = new NoteScrollMessage (this.message, this.y, this.scale, this.color, this.step);
			if (this.option != null) {
				ret.option = this.option.Clone ();
			}
			return ret;
		}
		public void Turn (int amount) {
		}
		public void MirrorHolizontal () {
		}
		public void MirrorVertical () {
		}
	}
	
	[System.Serializable]
	public class NoteSoundMessage : INote {
		public string soundId;
		public NoteOption option { get; set; }
		protected NoteSoundMessage () {
		}
		private NoteSoundMessage (string soundId) {
			this.soundId = soundId;
		}
		private static NoteSoundMessage Constractor (string soundId) {
			return new NoteSoundMessage (soundId);
		}
		public static NoteSoundMessage Create (string soundId) {
			return Constractor (soundId);
		}
		public static NoteSoundMessage FromMaiScript (string soundId) {
			if (soundId == null || !Utility.SoundEffectManager.Contains(soundId)) return null;
			return Constractor (soundId);
		}
		public virtual INote Clone () {
			var ret = new NoteSoundMessage (this.soundId);
			if (this.option != null) {
				ret.option = this.option.Clone ();
			}
			return ret;
		}
		public void Turn (int amount) {
		}
		public void MirrorHolizontal () {
		}
		public void MirrorVertical () {
		}
	}
	
	[System.Serializable]
	public sealed class NoteOption {
		public bool secret;
		private NoteOption () {
		}
		private NoteOption (bool secret) {
			this.secret = secret;
		}
		private static NoteOption Constractor (bool secret) {
			return new NoteOption (secret);
		}
		public static NoteOption Create (bool secret) {
			return Constractor (secret);
		}
		public static NoteOption FromMaiScript (NoteOption based, Flag secret) {
			// 引数全部がnullなら作らない.
			if (secret == null) return based;
			
			// ひとつでもnullでないなら作る.
			var ret = based != null ? based : new NoteOption();
			if (secret != null) {
				ret.secret = secret.GetValue();
			}
			return ret;
		}
		public NoteOption Clone () {
			return new NoteOption (this.secret);
		}
	}
	
	#endregion
	#region IStep
	public interface IStep {
		IStep Clone ();
	}
	
	[System.Serializable]
	public class StepLength : IStep {
		public float beat;
		public float length;
		protected StepLength () {
		}
		private StepLength (float beat, float length) {
			this.beat = beat;
			this.length = length;
		}
		private static StepLength Constractor (float beat, float length) {
			return new StepLength (beat, length);
		}
		public static StepLength Create (float beat, float length) {
			return Constractor (beat, length);
		}
		public static StepLength FromMaiScript (Value beat, Value length) {
			if (beat == null || length == null) return null;
			return Constractor (beat.GetValue(), length.GetValue());
		}
		public virtual IStep Clone () {
			return new StepLength (this.beat, this.length);
		}
	}
	
	[System.Serializable]
	public class StepLocalBpm : IStep {
		public float local_bpm;
		public float beat;
		public float length;
		protected StepLocalBpm () {
		}
		private StepLocalBpm (float localBpm, float beat, float length) {
			this.local_bpm = localBpm;
			this.beat = beat;
			this.length = length;
		}
		private static StepLocalBpm Constractor (float localBpm, float beat, float length) {
			return new StepLocalBpm (localBpm, beat, length);
		}
		public static StepLocalBpm Create (float localBpm, float beat, float length) {
			return Constractor (localBpm, beat, length);
		}
		public static StepLocalBpm FromMaiScript (Value localBpm, Value beat, Value length) {
			if (localBpm == null || beat == null || length == null) return null;
			return Constractor (localBpm.GetValue(), beat.GetValue(), length.GetValue());
		}
		public virtual IStep Clone () {
			return new StepLocalBpm (this.local_bpm, this.beat, this.length);
		}
	}
	
	[System.Serializable]
	public class StepInterval : IStep {
		public float interval;
		protected StepInterval () {
		}
		private StepInterval (float interval) {
			this.interval = interval;
		}
		private static StepInterval Constractor (float interval) {
			return new StepInterval (interval);
		}
		public static StepInterval Create (float interval) {
			return Constractor (interval);
		}
		public static StepInterval FromMaiScript (Value interval) {
			if (interval == null) return null;
			return Constractor (interval.GetValue());
		}
		public virtual IStep Clone () {
			return new StepInterval (this.interval);
		}
	}
	
	#endregion
	#region ISlideHead
	public interface ISlideHead {
		ISlideHead Clone();
		void Turn(int amount);
		void MirrorHolizontal();
		void MirrorVertical();
	}
	
	[System.Serializable]
	public class SlideHeadStar : ISlideHead {
		public Button button;
		protected SlideHeadStar () {
		}
		private SlideHeadStar (Button button) {
			this.button = button;
		}
		private static SlideHeadStar Constractor (Button button) {
			if (button == null) return null;
			return new SlideHeadStar (button);
		}
		public static SlideHeadStar Create (int button) {
			return Constractor (Button.Create (button));
		}
		public static SlideHeadStar FromMaiScript (Button button) {
			if (button == null) return null;
			return Constractor (button);
		}
		public virtual ISlideHead Clone () {
			return new SlideHeadStar (this.button.Clone ());
		}
		public SlideHeadBreakStar ToBreakStar () {
			return SlideHeadBreakStar.FromMaiScript (this.button);
		}
		public void Turn (int amount) {
			this.button.Turn (amount);
		}
		public void MirrorHolizontal () {
			this.button.MirrorHolizontal ();
		}
		public void MirrorVertical () {
			this.button.MirrorVertical ();
		}
		public static SlideHeadStar Turn (SlideHeadStar head, Number amount) {
			if (head == null || amount == null) return null;
			var ret = head.Clone () as SlideHeadStar;
			ret.Turn (amount.GetValue());
			return ret;
		}
		public static SlideHeadStar MirrorHolizontal (SlideHeadStar head) {
			if (head == null) return null;
			var ret = head.Clone () as SlideHeadStar;
			ret.MirrorHolizontal ();
			return ret;
		}
		public static SlideHeadStar MirrorVertical (SlideHeadStar head) {
			if (head == null) return null;
			var ret = head.Clone () as SlideHeadStar;
			ret.MirrorVertical ();
			return ret;
		}
	}
	
	[System.Serializable]
	public class SlideHeadBreakStar : ISlideHead {
		public Button button;
		protected SlideHeadBreakStar () {
		}
		private SlideHeadBreakStar (Button button) {
			this.button = button;
		}
		private static SlideHeadBreakStar Constractor (Button button) {
			if (button == null) return null;
			return new SlideHeadBreakStar (button);
		}
		public static SlideHeadBreakStar Create (int button) {
			return Constractor (Button.Create (button));
		}
		public static SlideHeadBreakStar FromMaiScript (Button button) {
			if (button == null) return null;
			return Constractor (button);
		}
		public virtual ISlideHead Clone () {
			return new SlideHeadBreakStar (this.button.Clone ());
		}
		public SlideHeadStar ToStar () {
			return SlideHeadStar.FromMaiScript (this.button);
		}
		public void Turn (int amount) {
			this.button.Turn (amount);
		}
		public void MirrorHolizontal () {
			this.button.MirrorHolizontal ();
		}
		public void MirrorVertical () {
			this.button.MirrorVertical ();
		}
		public static SlideHeadBreakStar Turn (SlideHeadBreakStar head, Number amount) {
			if (head == null || amount == null) return null;
			var ret = head.Clone () as SlideHeadBreakStar;
			ret.Turn (amount.GetValue());
			return ret;
		}
		public static SlideHeadBreakStar MirrorHolizontal (SlideHeadBreakStar head) {
			if (head == null) return null;
			var ret = head.Clone () as SlideHeadBreakStar;
			ret.MirrorHolizontal ();
			return ret;
		}
		public static SlideHeadBreakStar MirrorVertical (SlideHeadBreakStar head) {
			if (head == null) return null;
			var ret = head.Clone () as SlideHeadBreakStar;
			ret.MirrorVertical ();
			return ret;
		}
	}
	
	[System.Serializable]
	public class SlideHeadNothing : ISlideHead {
		protected SlideHeadNothing () {
		}
		private static SlideHeadNothing Constractor () {
			return new SlideHeadNothing ();
		}
		public static SlideHeadNothing Create () {
			return Constractor ();
		}
		public static SlideHeadNothing FromMaiScript () {
			return Constractor ();
		}
		public virtual ISlideHead Clone () {
			return new SlideHeadNothing ();
		}
		public void Turn (int amount) {
		}
		public void MirrorHolizontal () {
		}
		public void MirrorVertical () {
		}
	}
	
	#endregion
	#region ISlideHeadWait
	public interface ISlideHeadCustomWait {
		ISlideHeadCustomWait Clone();
	}
	
	[System.Serializable]
	public class SlideHeadCustomWaitLocalBpm : ISlideHeadCustomWait {
		public float local_bpm;
		protected SlideHeadCustomWaitLocalBpm () {
		}
		private SlideHeadCustomWaitLocalBpm (float localBpm) {
			this.local_bpm = localBpm;
		}
		private static SlideHeadCustomWaitLocalBpm Constractor (float localBpm) {
			return new SlideHeadCustomWaitLocalBpm (localBpm);
		}
		public static SlideHeadCustomWaitLocalBpm Create (float localBpm) {
			return Constractor (localBpm);
		}
		public static SlideHeadCustomWaitLocalBpm FromMaiScript (Value localBpm) {
			if (localBpm == null) return null;
			return Constractor (localBpm.GetValue());
		}
		public virtual ISlideHeadCustomWait Clone () {
			return new SlideHeadCustomWaitLocalBpm (this.local_bpm);
		}
	}
	
	[System.Serializable]
	public class SlideHeadCustomWaitInterval : ISlideHeadCustomWait{
		public float interval;
		protected SlideHeadCustomWaitInterval () {
		}
		private SlideHeadCustomWaitInterval (float interval) {
			this.interval = interval;
		}
		private static SlideHeadCustomWaitInterval Constractor (float interval) {
			return new SlideHeadCustomWaitInterval (interval);
		}
		public static SlideHeadCustomWaitInterval Create (float interval) {
			return Constractor (interval);
		}
		public static SlideHeadCustomWaitInterval FromMaiScript (Value interval) {
			if (interval == null) return null;
			return Constractor (interval.GetValue());
		}
		public virtual ISlideHeadCustomWait Clone () {
			return new SlideHeadCustomWaitInterval (this.interval);
		}
	}
	
	[System.Serializable]
	public class SlideHeadCustomWaitDefault : ISlideHeadCustomWait{
		protected SlideHeadCustomWaitDefault () {
		}
		private static SlideHeadCustomWaitDefault Constractor () {
			return new SlideHeadCustomWaitDefault ();
		}
		public static SlideHeadCustomWaitDefault Create () {
			return Constractor ();
		}
		public static SlideHeadCustomWaitDefault FromMaiScript () {
			return Constractor ();
		}
		public virtual ISlideHeadCustomWait Clone () {
			return new SlideHeadCustomWaitDefault ();
		}
	}
	
	#endregion
	#region ISlidePattern
	public interface ISlidePattern {
		ISlidePattern Clone();
		void Turn(int amount);
		void Turn(float degree);
		void MirrorHolizontal();
		void MirrorVertical();
		ISlideHeadCustomWait GetWait ();
		IStep GetStep ();
	}
	
	public class SlidePattern : ISlidePattern {
		public ISlideHeadCustomWait wait;
		public IStep step;
		public ISlideChain[] chains;
		protected SlidePattern () {
		}
		private SlidePattern (ISlideHeadCustomWait wait, IStep step, ISlideChain[] chains) {
			this.wait = wait;
			this.step = step;
			this.chains = chains;
		}
		private static SlidePattern Constractor (ISlideHeadCustomWait wait, IStep step, ISlideChain[] chains) {
			if (chains == null || chains.Length == 0) return null;
			return new SlidePattern (wait, step, chains);
		}
		public static SlidePattern Create (params ISlideChain[] chains) {
			return Constractor (null, null, chains);
		}
		public static SlidePattern Create (ISlideHeadCustomWait wait, params ISlideChain[] chains) {
			return Constractor (wait, null, chains);
		}
		public static SlidePattern Create (IStep step, params ISlideChain[] chains) {
			return Constractor (null, step, chains);
		}
		public static SlidePattern Create (ISlideHeadCustomWait wait, IStep step, params ISlideChain[] chains) {
			return Constractor (wait, step, chains);
		}
		public static SlidePattern FromMaiScript (params ISlideChain[] chains) {
			if (chains == null || chains.Length == 0) return null;
			return Constractor (null, null, chains);
		}
		public static SlidePattern FromMaiScript (ISlideHeadCustomWait wait, params ISlideChain[] chains) {
			if (chains == null || chains.Length == 0) return null;
			return Constractor (wait, null, chains);
		}
		public static SlidePattern FromMaiScript (IStep step, params ISlideChain[] chains) {
			if (chains == null || chains.Length == 0) return null;
			return Constractor (null, step, chains);
		}
		public static SlidePattern FromMaiScript (ISlideHeadCustomWait wait, IStep step, params ISlideChain[] chains) {
			if (chains == null || chains.Length == 0) return null;
			return Constractor (wait, step, chains);
		}
		public virtual ISlidePattern Clone () {
			var cloneChains = new ISlideChain[chains.Length];
			for (int i = 0; i < cloneChains.Length; i++) {
				cloneChains[i] = chains[i].Clone ();
			}
			var wait = this.wait != null ? this.wait.Clone () : null;
			var step = this.step != null ? this.step.Clone () : null;
			return new SlidePattern (wait, step, cloneChains);
		}
		public void Turn (int amount) {
			foreach (var chain in chains) {
				chain.Turn (amount);
			}
		}
		public void Turn (float degree) {
			foreach (var chain in chains) {
				chain.Turn (degree);
			}
		}
		public void MirrorHolizontal () {
			foreach (var chain in chains) {
				chain.MirrorHolizontal ();
			}
		}
		public void MirrorVertical () {
			foreach (var chain in chains) {
				chain.MirrorVertical ();
			}
		}
		public ISlideHeadCustomWait GetWait () {
			return wait;
		}
		public IStep GetStep () {
			return step;
		}
		public static SlidePattern Turn (SlidePattern pattern, Number amount) {
			if (pattern == null || amount == null) return null;
			var ret = pattern.Clone () as SlidePattern;
			ret.Turn (amount.GetValue());
			return ret;
		}
		public static SlidePattern Turn (SlidePattern pattern, Value degree) {
			if (pattern == null || degree == null) return null;
			var ret = pattern.Clone () as SlidePattern;
			ret.Turn (degree.GetValue());
			return ret;
		}
		public static SlidePattern MirrorHolizontal (SlidePattern pattern) {
			if (pattern == null) return null;
			var ret = pattern.Clone () as SlidePattern;
			ret.MirrorHolizontal ();
			return ret;
		}
		public static SlidePattern MirrorVertical (SlidePattern pattern) {
			if (pattern == null) return null;
			var ret = pattern.Clone () as SlidePattern;
			ret.MirrorVertical ();
			return ret;
		}
	}
	
	#endregion
	#region ISlideChain
	public interface ISlideChain {
		ISlideChain Clone();
		void Turn(int amount);
		void Turn(float degree);
		void MirrorHolizontal();
		void MirrorVertical();
		ISlideHeadCustomWait GetWait ();
		IStep GetStep ();
	}
	
	public class SlideChain : ISlideChain {
		public ISlideHeadCustomWait wait;
		public IStep step;
		public ISlideCommand[] commands;
		protected SlideChain () {
		}
		private SlideChain (ISlideHeadCustomWait wait, IStep step, ISlideCommand[] commands) {
			this.wait = wait;
			this.step = step;
			this.commands = commands;
		}
		private static SlideChain Constractor (ISlideHeadCustomWait wait, IStep step, ISlideCommand[] commands) {
			if (commands == null) return null;
			return new SlideChain (wait, step, commands);
		}
		public static SlideChain Create (params ISlideCommand[] commands) {
			return Constractor (null, null, commands);
		}
		public static SlideChain Create (ISlideHeadCustomWait wait, params ISlideCommand[] commands) {
			return Constractor (wait, null, commands);
		}
		public static SlideChain Create (IStep step, params ISlideCommand[] commands) {
			return Constractor (null, step, commands);
		}
		public static SlideChain Create (ISlideHeadCustomWait wait, IStep step, params ISlideCommand[] commands) {
			return Constractor (wait, step, commands);
		}
		public static SlideChain FromMaiScript (params ISlideCommand[] commands) {
			if (commands == null) return null;
			return Constractor (null, null, commands);
		}
		public static SlideChain FromMaiScript (ISlideHeadCustomWait wait, params ISlideCommand[] commands) {
			if (commands == null) return null;
			return Constractor (wait, null, commands);
		}
		public static SlideChain FromMaiScript (IStep step, params ISlideCommand[] commands) {
			if (commands == null) return null;
			return Constractor (null, step, commands);
		}
		public static SlideChain FromMaiScript (ISlideHeadCustomWait wait, IStep step, params ISlideCommand[] commands) {
			if (commands == null) return null;
			return Constractor (wait, step, commands);
		}
		public virtual ISlideChain Clone () {
			var cloneCommands = new ISlideCommand[commands.Length];
			for (int i = 0; i < cloneCommands.Length; i++) {
				cloneCommands[i] = this.commands[i].Clone ();
			}
			var wait = this.wait != null ? this.wait.Clone() : null;
			var step = this.step != null ? this.step.Clone() : null;
			return new SlideChain (wait, step, cloneCommands);
		}
		public void Turn (int amount) {
			foreach (var command in commands) {
				command.Turn (amount);
			}
		}
		public void Turn (float degree) {
			foreach (var command in commands) {
				command.Turn (degree);
			}
		}
		public void MirrorHolizontal () {
			foreach (var command in commands) {
				command.MirrorHolizontal ();
			}
		}
		public void MirrorVertical () {
			foreach (var command in commands) {
				command.MirrorVertical ();
			}
		}
		public ISlideHeadCustomWait GetWait () {
			return wait;
		}
		public IStep GetStep () {
			return step;
		}
		public static SlideChain Turn (SlideChain pattern, Number amount) {
			if (pattern == null || amount == null) return null;
			var ret = pattern.Clone () as SlideChain;
			ret.Turn (amount.GetValue());
			return ret;
		}
		public static SlideChain Turn (SlideChain pattern, Value degree) {
			if (pattern == null || degree == null) return null;
			var ret = pattern.Clone () as SlideChain;
			ret.Turn (degree.GetValue());
			return ret;
		}
		public static SlideChain MirrorHolizontal (SlideChain pattern) {
			if (pattern == null) return null;
			var ret = pattern.Clone () as SlideChain;
			ret.MirrorHolizontal ();
			return ret;
		}
		public static SlideChain MirrorVertical (SlideChain pattern) {
			if (pattern == null) return null;
			var ret = pattern.Clone () as SlideChain;
			ret.MirrorVertical ();
			return ret;
		}
	}
	
	#endregion
	#region ISlideCommand
	public interface ISlideCommand {
		ISlideCommand Clone ();
		void Turn(int amount);
		void Turn(float degree);
		void MirrorHolizontal();
		void MirrorVertical();
	}
	
	[System.Serializable]
	public class SlideCommandStraight : ISlideCommand {
		public Vector2 start;
		public Vector2 target;
		protected SlideCommandStraight () {
		}
		private SlideCommandStraight (Vector2 start, Vector2 target) {
			this.start = start;
			this.target = target;
		}
		private SlideCommandStraight (Vector2 target, ISlideHead head, ISlideCommand beforeCommand) {
			this.target = target;
			float sx, sy;
			if (beforeCommand == null) {
				CalcContinuedStraightPosition (head, out sx, out sy);
			}
			else {
				CalcContinuedStraightPosition (beforeCommand, out sx, out sy);
			}
			this.start = new Vector2(sx, sy);
		}
		private static SlideCommandStraight Constractor (Vector2 start, Vector2 target) {
			return new SlideCommandStraight (start, target);
		}
		private static SlideCommandStraight Constractor (Vector2 target, ISlideHead head, ISlideCommand beforeCommand) {
			return new SlideCommandStraight (target, head, beforeCommand);
		}
		public static SlideCommandStraight Create (Vector2 start, Vector2 target) {
			return Constractor (start, target);
		}
		public static SlideCommandStraight Create (float sx, float sy, float tx, float ty) {
			return Constractor (new Vector2(sx, sy), new Vector2(tx, ty));
		}
		public static SlideCommandStraight Create (Vector2 target, ISlideHead head, ISlideCommand beforeCommand) {
			return Constractor (target, head, beforeCommand);
		}
		public static SlideCommandStraight Create (float tx, float ty, ISlideHead head, ISlideCommand beforeCommand) {
			return Constractor (new Vector2(tx, ty), head, beforeCommand);
		}
		public static SlideCommandStraight FromMaiScript (Position start, Position target) {
			if (start == null || target == null) return null;
			return Constractor (start.GetValue(), target.GetValue());
		}
		public static SlideCommandStraight FromMaiScript (Value sx, Value sy, Value tx, Value ty) {
			if (sx == null || sy == null || tx == null || ty == null) return null;
			return Constractor (new Vector2(sx.GetValue(), sy.GetValue()), new Vector2(tx.GetValue(), ty.GetValue()));
		}
		public static SlideCommandStraight FromMaiScript (Position target, ISlideHead head, ISlideCommand beforeCommand) {
			if (target == null) return null;
			return Constractor (target.GetValue(), head, beforeCommand);
		}
		public static SlideCommandStraight FromMaiScript (Value tx, Value ty, ISlideHead head, ISlideCommand beforeCommand) {
			if (tx == null || ty == null) return null;
			return Constractor (new Vector2(tx.GetValue(), ty.GetValue()), head, beforeCommand);
		}
		public virtual ISlideCommand Clone () {
			var ret = new SlideCommandStraight (this.start, this.target);
			return ret;
		}
		public void Turn (int amount) {
			var s = Position.Create (this.start);
			s.Turn (amount);
			this.start = s.GetValue ();
			var t = Position.Create (this.target);
			t.Turn (amount);
			this.target = t.GetValue ();
		}
		public void Turn (float degree) {
			var s = Position.Create (this.start);
			s.Turn (degree);
			this.start = s.GetValue ();
			var t = Position.Create (this.target);
			t.Turn (degree);
			this.target = t.GetValue ();
		}
		public void MirrorHolizontal () {
			var s = Position.Create (this.start);
			s.MirrorHolizontal ();
			this.start = s.GetValue ();
			var t = Position.Create (this.target);
			t.MirrorHolizontal ();
			this.target = t.GetValue ();
		}
		public void MirrorVertical () {
			var s = Position.Create (this.start);
			s.MirrorVertical ();
			this.start = s.GetValue ();
			var t = Position.Create (this.target);
			t.MirrorVertical ();
			this.target = t.GetValue ();
		}
		public static SlideCommandStraight Turn (SlideCommandStraight command, Number amount) {
			if (command == null || amount == null) return null;
			var ret = command.Clone () as SlideCommandStraight;
			ret.Turn (amount.GetValue());
			return ret;
		}
		public static SlideCommandStraight Turn (SlideCommandStraight command, Value degree) {
			if (command == null || degree == null) return null;
			var ret = command.Clone () as SlideCommandStraight;
			ret.Turn (degree.GetValue());
			return ret;
		}
		public static SlideCommandStraight MirrorHolizontal (SlideCommandStraight command) {
			if (command == null) return null;
			var ret = command.Clone () as SlideCommandStraight;
			ret.MirrorHolizontal ();
			return ret;
		}
		public static SlideCommandStraight MirrorVertical (SlideCommandStraight command) {
			if (command == null) return null;
			var ret = command.Clone () as SlideCommandStraight;
			ret.MirrorVertical ();
			return ret;
		}
		public static void CalcContinuedStraightPosition(ISlideHead head, out float start_x, out float start_y) {
			if (head == null) {
				head = SlideHeadNothing.Create();
			}
			if (head is SlideHeadNothing) {
				start_x = 0;
				start_y = 0;
			}
			int? star = null;
			if (head is SlideHeadStar) {
				star = (head as SlideHeadStar).button.GetIndex();
			}
			else if (head is SlideHeadBreakStar) {
				star = (head as SlideHeadBreakStar).button.GetIndex();
			}
			if (star.HasValue) {
				int button = star.Value;
				// 右手.
				var pos = CircleCalculator.PointOnCircle(Vector2.zero, 1, Constants.instance.GetPieceDegree(button));
				// 左手.
				pos = Constants.instance.ToLeftHandedCoordinateSystemPosition(pos);
				start_x = pos.x;
				start_y = pos.y;
			}
			else {
				start_x = 0;
				start_y = 0;
			}
		}
		public static bool CalcContinuedStraightPosition(ISlideCommand beforeCommand, out float start_x, out float start_y) {
			// 前回が直線で今回も直線なら前回の終点をそのまま今回の始点に持ってくる.
			if (beforeCommand is SlideCommandStraight) {
				var command = beforeCommand as SlideCommandStraight;
				// 左手.
				start_x = command.target.x;
				start_y = command.target.y;
				return true;
			}
			// 前回が曲線で今回は直線なら前回の終点を算出して、今回の始点に持ってくる.
			else if (beforeCommand is SlideCommandCurve) {
				var command = beforeCommand as SlideCommandCurve;
				// 右手.
				var pos = CircleCalculator.PointOnEllipse(Constants.instance.ToLeftHandedCoordinateSystemPosition(command.center), command.radius, command.start_degree + command.distance_degree);
				// 左手.
				pos = Constants.instance.ToLeftHandedCoordinateSystemPosition(pos);
				start_x = pos.x;
				start_y = pos.y;
				return true;
			}
			start_x = start_y = 0;
			return false;
		}
	}
	
	[System.Serializable]
	public class SlideCommandCurve : ISlideCommand {
		public Vector2 center;
		public Vector2 radius;
		public float start_degree;
		public float distance_degree;
		protected SlideCommandCurve () {
		}
		private SlideCommandCurve (Vector2 center, Vector2 radius, float startDegree, float distanceDegree) {
			this.center = center;
			this.radius = radius;
			this.start_degree = startDegree;
			this.distance_degree = distanceDegree;
		}
		private SlideCommandCurve (Vector2 center, float distanceDegree, ISlideHead head, ISlideCommand beforeCommand) {
			this.center = center;
			this.distance_degree = distanceDegree;
			float rx, ry, sd;
			if (beforeCommand == null) {
				CalcContinuedCurvePosition (head, center, out rx, out ry, out sd);
			}
			else {
				CalcContinuedCurvePosition (beforeCommand, center, out rx, out ry, out sd);
			}
			this.radius = new Vector2 (rx, ry);
			this.start_degree = sd;
		}
		private static SlideCommandCurve Constractor (Vector2 center, Vector2 radius, float startDegree, float distanceDegree) {
			return new SlideCommandCurve (center, radius, startDegree, distanceDegree);
		}
		private static SlideCommandCurve Constractor (Vector2 center, float distanceDegree, ISlideHead head, ISlideCommand beforeCommand) {
			return new SlideCommandCurve (center, distanceDegree, head, beforeCommand);
		}
		public static SlideCommandCurve Create (Vector2 center, Vector2 radius, float startDegree, float distanceDegree) {
			return Constractor (center, radius, startDegree, distanceDegree);
		}
		public static SlideCommandCurve Create (float cx, float cy, float rx, float ry, float startDegree, float distanceDegree) {
			return Constractor (new Vector2(cx, cy), new Vector2(rx, ry), startDegree, distanceDegree);
		}
		public static SlideCommandCurve Create (Vector2 center, float radius, float startDegree, float distanceDegree) {
			return Constractor (center, new Vector2(radius, radius), startDegree, distanceDegree);
		}
		public static SlideCommandCurve Create (float cx, float cy, float radius, float startDegree, float distanceDegree) {
			return Constractor (new Vector2(cx, cy), new Vector2(radius, radius), startDegree, distanceDegree);
		}
		public static SlideCommandCurve Create (Vector2 center, float distanceDegree, ISlideHead head, ISlideCommand beforeCommand) {
			return Constractor (center, distanceDegree, head, beforeCommand);
		}
		public static SlideCommandCurve Create (float cx, float cy, float distanceDegree, ISlideHead head, ISlideCommand beforeCommand) {
			return Constractor (new Vector2(cx, cy), distanceDegree, head, beforeCommand);
		}
		public static SlideCommandCurve FromMaiScript (Position center, Position radius, Value startDegree, Value distanceDegree) {
			if (center == null || radius == null || startDegree == null || distanceDegree == null) return null;
			return Constractor (center.GetValue(), radius.GetValue(), startDegree.GetValue(), distanceDegree.GetValue());
		}
		public static SlideCommandCurve FromMaiScript (Value cx, Value cy, Value rx, Value ry, Value startDegree, Value distanceDegree) {
			if (cx == null || cy == null || rx == null || ry == null || startDegree == null || distanceDegree == null) return null;
			return Constractor (new Vector2(cx.GetValue(), cy.GetValue()), new Vector2(rx.GetValue(), ry.GetValue()), startDegree.GetValue(), distanceDegree.GetValue());
		}
		public static SlideCommandCurve FromMaiScript (Position center, Value radius, Value startDegree, Value distanceDegree) {
			if (center == null || radius == null || startDegree == null || distanceDegree == null) return null;
			return Constractor (center.GetValue(), new Vector2(radius.GetValue(), radius.GetValue()), startDegree.GetValue(), distanceDegree.GetValue());
		}
		public static SlideCommandCurve FromMaiScript (Value cx, Value cy, Value radius, Value startDegree, Value distanceDegree) {
			if (cx == null || cy == null || radius == null || startDegree == null || distanceDegree == null) return null;
			return Constractor (new Vector2(cx.GetValue(), cy.GetValue()), new Vector2(radius.GetValue(), radius.GetValue()), startDegree.GetValue(), distanceDegree.GetValue());
		}
		public static SlideCommandCurve FromMaiScript (Position center, Value distanceDegree, ISlideHead head, ISlideCommand beforeCommand) {
			if (center == null || distanceDegree == null) return null;
			return Constractor (center.GetValue(), distanceDegree.GetValue(), head, beforeCommand);
		}
		public static SlideCommandCurve FromMaiScript (Value cx, Value cy, Value distanceDegree, ISlideHead head, ISlideCommand beforeCommand) {
			if (cx == null || cy == null || distanceDegree == null) return null;
			return Constractor (new Vector2(cx.GetValue(), cy.GetValue()), distanceDegree.GetValue(), head, beforeCommand);
		}
		public float GetRadius () { // 楕円に自信がないので...データとしては楕円として入れられるようにしておく.
			return Mathf.Max (this.radius.x, this.radius.y);
		}
		public virtual ISlideCommand Clone () {
			return new SlideCommandCurve (this.center, this.radius, this.start_degree, this.distance_degree);
		}
		public void Turn (int amount) {
			var s = Position.Create (this.center);
			s.Turn (amount);
			this.center = s.GetValue ();
			this.start_degree += amount * (360.0f / 8.0f);
			while (this.start_degree > 360.0f) this.start_degree -= 360.0f;
			while (this.start_degree < 0) this.start_degree += 360.0f;
		}
		public void Turn (float degree) {
			var s = Position.Create (this.center);
			s.Turn (degree);
			this.center = s.GetValue ();
			this.start_degree += degree;
			while (this.start_degree > 360.0f) this.start_degree -= 360.0f;
			while (this.start_degree < 0) this.start_degree += 360.0f;
		}
		public void MirrorHolizontal () {
			var s = Position.Create (this.center);
			s.MirrorHolizontal ();
			this.center = s.GetValue ();
			this.start_degree = 360.0f - this.start_degree;
			while (this.start_degree > 360.0f) this.start_degree -= 360.0f;
			while (this.start_degree < 0) this.start_degree += 360.0f;
			this.distance_degree *= -1;
		}
		public void MirrorVertical () {
			var s = Position.Create (this.center);
			s.MirrorVertical ();
			this.center = s.GetValue ();
			this.start_degree = 180.0f - this.start_degree;
			while (this.start_degree > 360.0f) this.start_degree -= 360.0f;
			while (this.start_degree < 0) this.start_degree += 360.0f;
			this.distance_degree *= -1;
		}
		public static SlideCommandCurve Turn (SlideCommandCurve command, Number amount) {
			if (command == null || amount == null) return null;
			var ret = command.Clone () as SlideCommandCurve;
			ret.Turn (amount.GetValue());
			return ret;
		}
		public static SlideCommandCurve Turn (SlideCommandCurve command, Value degree) {
			if (command == null || degree == null) return null;
			var ret = command.Clone () as SlideCommandCurve;
			ret.Turn (degree.GetValue());
			return ret;
		}
		public static SlideCommandCurve MirrorHolizontal (SlideCommandCurve command) {
			if (command == null) return null;
			var ret = command.Clone () as SlideCommandCurve;
			ret.MirrorHolizontal ();
			return ret;
		}
		public static SlideCommandCurve MirrorVertical (SlideCommandCurve command) {
			if (command == null) return null;
			var ret = command.Clone () as SlideCommandCurve;
			ret.MirrorVertical ();
			return ret;
		}
		public static void CalcContinuedCurvePosition(ISlideHead head, Vector2 center, out float radius_x, out float radius_y, out float start_degree) {
			Vector2 pos;
			center = Constants.instance.ToLeftHandedCoordinateSystemPosition (center);
			// 右手.
			int? star = null;
			if (head == null) {
				head = SlideHeadNothing.Create();
			}
			if (head is SlideHeadStar) {
				star = (head as SlideHeadStar).button.GetIndex();
			}
			else if (head is SlideHeadBreakStar) {
				star = (head as SlideHeadBreakStar).button.GetIndex();
			}
			if (star.HasValue) {
				int button = star.Value;
				// 右手.
				pos = CircleCalculator.PointOnCircle (Vector2.zero, 1, Constants.instance.GetPieceDegree (button));
				float radius = CircleCalculator.PointToPointDistance (center, pos);
				radius_x = radius;
				radius_y = radius;
				start_degree = CircleCalculator.PointToDegree(center, pos);
			}
			else {
				pos = Vector2.zero;
				float radius = CircleCalculator.PointToPointDistance (center, pos);
				radius_x = radius;
				radius_y = radius;
				start_degree = CircleCalculator.PointToDegree (center, pos);
			}
		}
		public static bool CalcContinuedCurvePosition(ISlideCommand beforeCommand, Vector2 center, out float radius_x, out float radius_y, out float start_degree) {
			// 前回が直線で今回は曲線なら、前回の終点と、今回の回転軸から、今回の半径と始点角度を決める.
			if (beforeCommand is SlideCommandStraight) {
				var command = beforeCommand as SlideCommandStraight;
				// 右手.
				center = Constants.instance.ToLeftHandedCoordinateSystemPosition(center);
				Vector2 pos = Constants.instance.ToLeftHandedCoordinateSystemPosition(command.target);
				float radius = CircleCalculator.PointToPointDistance(center, pos);
				radius_x = radius;
				radius_y = radius;
				start_degree = CircleCalculator.PointToDegree(center, pos);
				return true;
			}
			// 前回が曲線で今回も曲線なら、前回の回転軸・半径から求めた終点と、今回の回転軸から、今回の半径と始点角度を決める.
			else if (beforeCommand is SlideCommandCurve) {
				var command = beforeCommand as SlideCommandCurve;
				// 右手.
				center = Constants.instance.ToLeftHandedCoordinateSystemPosition(center);
				Vector2 pos = CircleCalculator.PointOnEllipse(Constants.instance.ToLeftHandedCoordinateSystemPosition(command.center), command.radius, command.start_degree + command.distance_degree);
				float radius = CircleCalculator.PointToPointDistance(center, pos);
				radius_x = radius;
				radius_y = radius;
				start_degree = CircleCalculator.PointToDegree(center, pos);
				return true;
			}
			radius_x = radius_y = start_degree = 0;
			return false;
		}
	}
	#endregion
}