using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RhythmGameLibrary.Score;

public class MaimaiScoreConverter : ScoreConverter<MaimaiScore.Each, MaimaiNote> {
	public MaimaiScoreConverter(MaimaiScore.Each[] score_data, bool mirror, int turn, bool tapToBreak, bool breakToTap, int[] getTimeRequestScoreIndexes) : base(score_data, 4, 4, MaimaiJudgeEvaluateManager.START_COUNT_START_TIME, getTimeRequestScoreIndexes) {
		uniqueId = 0;
		this.mirror = mirror;
		this.turn = turn;
		this.tapToBreak = tapToBreak;
		this.breakToTap = breakToTap;
		subTimeMaxValue = 0;
	}
	public MaimaiScoreConverter(MaimaiScore.Each[] score_data) : this (score_data, false, 0, false, false, null) { }

	private int uniqueId;
	private bool mirror;
	private int turn;
	private bool tapToBreak;
	private bool breakToTap;
	private long subTimeMaxValue; //スライドやホールドの終点時間の、全ノート中の最大値.
	
//	protected int AddTurnRefMirror (int button) {
//		return SlidePatternFactoryGenericPattern.AddTurnRefMirror (button, turn, mirror);
//	}
//	
//	protected float turnDeg { 
//		get {
//			const float based = 360.0f / 8.0f;
//			return (float)turn * based; // 右手座標系
//		}
//	}
//	
//	protected Vector2 AddTurnRefMirror (Vector2 pointLeftHand) {
//		// 右手座標 (引数の左手座標を渡して右手座標に変換してる)
//		var pointDeg = Constants.instance.ToLeftHandedCoordinateSystemDegree(CircleCalculator.PointToDegree (pointLeftHand));
//		// 右手座標
//		var pointRadius = CircleCalculator.PointToPointDistance (Vector2.zero, pointLeftHand);
//		// 右手座標
//		var turnAddedPoint = CircleCalculator.PointOnCircle (Vector2.zero, pointRadius, pointDeg + turnDeg);
//		var mirrorRef = mirror ? -1 : 1;
//		// 右手座標
//		var retRightHand = new Vector2 (turnAddedPoint.x * mirrorRef, turnAddedPoint.y);
//		// 左手座標
//		var retLeftHand = Constants.instance.ToLeftHandedCoordinateSystemPosition (retRightHand);
//		return retLeftHand;
//	}

	protected override void ReadScoreRoutine (MaimaiScore.Each each_data) {
		if (each_data.notes == null) return;

		List<MaimaiNote> createNotes = new List<MaimaiNote>();
		foreach (var note_data in each_data.notes) {
			var maisqNote = note_data.Clone();
			if (turn > 0) maisqNote.Turn (turn);
			if (mirror) maisqNote.MirrorHolizontal ();
			List<MaimaiNote> tempNotes = new List<MaimaiNote>();
			if (maisqNote is MaimaiScore.NoteTap) {
				var note = Convert (maisqNote as MaimaiScore.NoteTap);
				if (note != null) tempNotes.Add (note);
			}
			else if (maisqNote is MaimaiScore.NoteHold) {
				var note = Convert (maisqNote as MaimaiScore.NoteHold);
				if (note != null) tempNotes.AddRange (note);
			}
			else if (maisqNote is MaimaiScore.NoteSlide) {
				var note = Convert (maisqNote as MaimaiScore.NoteSlide);
				if (note != null) tempNotes.AddRange (note);
			}
			else if (maisqNote is MaimaiScore.NoteBreak) {
				var note = Convert (maisqNote as MaimaiScore.NoteBreak);
				if (note != null) tempNotes.Add (note);
			}
			else if (maisqNote is MaimaiScore.NoteMessage) {
				var note = Convert (maisqNote as MaimaiScore.NoteMessage);
				if (note != null) tempNotes.Add (note);
			}
			else if (maisqNote is MaimaiScore.NoteScrollMessage) {
				var note = Convert (maisqNote as MaimaiScore.NoteScrollMessage);
				if (note != null) tempNotes.Add (note);
			}
			else if (maisqNote is MaimaiScore.NoteSoundMessage) {
				var note = Convert (maisqNote as MaimaiScore.NoteSoundMessage);
				if (note != null) tempNotes.Add (note);
			}
			
			foreach (var note in tempNotes) {
				if (maisqNote.option != null) {
					note.secret = maisqNote.option.secret;
				}
				createNotes.Add (note);
			}
		}

		// EACH設定.
		List<IMaimaiEachNote> eachNotes = new List<IMaimaiEachNote>();
		foreach (var note in createNotes) {
			if (note.IsEachableNoteType()) {
				eachNotes.Add((IMaimaiEachNote)note);
			}
		}
		for (int i = 0; i < eachNotes.Count; i++) {
			for (int j = 0; j < eachNotes.Count; j++) {
				eachNotes[i].AddEachNote(eachNotes[j]);
			}
		}

		AddNotes(createNotes.ToArray());
	}

	protected override void ReadEndScoreEvent (MaimaiNote[] notes) {
		// スライドのEACH設定.
		HashSet<MaimaiSlidePattern> patterns = new HashSet<MaimaiSlidePattern> ();
		foreach (var note in notes) {
			var slNote = note as MaimaiSlideNote;
			if (slNote != null) {
				if (slNote.pattern.chains.Length > 0) {
					// chains内すべての☆が動き出す時間が同じである場合、EACH候補に入れる.
					long time = slNote.pattern.chains[0].onStarMoveStartTime;
					foreach (var chain in slNote.pattern.chains) {
						if (time != chain.onStarMoveStartTime) {
							continue;
						}
					}
					patterns.Add (slNote.pattern);
				}
			}
		}
		MaimaiSlidePattern[] patterns1 = new MaimaiSlidePattern[patterns.Count];
		patterns.CopyTo (patterns1);
		for (int i = 0; i < patterns1.Length; i++) {
			for (int j = 0; j < patterns1.Length; j++) {
				if (i != j) {
					// ☆が動き出す時間が両者一致しているならEACHとする.
					if (patterns1[i].chains[0].onStarMoveStartTime == patterns1[j].chains[0].onStarMoveStartTime) {
						patterns1[i].AddEachNote (patterns1[j]);
					}
				}
			}
		}


	}

	public MaimaiNote Convert (MaimaiScore.NoteTap data) {
		MaimaiNote result;
		int b = data.button.GetIndex ();
		if (!this.tapToBreak) {
			result = new MaimaiTapNote (uniqueId.ToString (), GetTimeMS (), MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.TAP_CIRCLE), Constants.instance.MAIMAI_NOTE_ACTION_ID_BASE + b, b);
		}
		else {
			result = new MaimaiBreakNote (uniqueId.ToString (), GetTimeMS (), MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.BREAK_CIRCLE), Constants.instance.MAIMAI_NOTE_ACTION_ID_BASE + b, b);
		}
		uniqueId++;
		return result;
	}

	public MaimaiNote[] Convert (MaimaiScore.NoteHold data) {
		decimal subTime;
		decimal? tempSubTime = Convert (data.step);
		if (tempSubTime.HasValue)
			subTime = tempSubTime.Value;
		else
			return null;
		
		int b = data.button.GetIndex ();
		var results = new MaimaiNote[2];
		var hNote = new MaimaiHoldHeadNote (uniqueId.ToString(), GetTimeMS (), MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.HOLD_HEAD), Constants.instance.MAIMAI_NOTE_ACTION_ID_BASE + b, b);
		uniqueId++;

		decimal leaveTime = GetTime () + subTime;
		long leaveTimeMS = (long)(leaveTime * 1000.0M);
		subTimeMaxValue = MathlMax (subTimeMaxValue, leaveTimeMS);
		var fNote = new MaimaiHoldFootNote (uniqueId.ToString(), leaveTimeMS, MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.HOLD_FOOT), Constants.instance.MAIMAI_NOTE_HOLD_FOOT_ACTION_ID_BASE + b);
		uniqueId++;

		fNote.SetRelativeNote (hNote);

		results [0] = hNote;
		results [1] = fNote;
		return results;
	}
	
	public MaimaiNote[] Convert (MaimaiScore.NoteSlide data) {
		var head = Convert (data.head);
		if (head == null)
			head = Convert (MaimaiScore.SlideHeadNothing.Create ());

		var iWait = data.wait;
		var iStep = data.step;
		var mNote = head as MaimaiNote;
		if (mNote == null) Debug.LogError ("Slide head is not extend MaimaiNote!!");
		var createList = new List<MaimaiSlidePattern> ();
		foreach (var data_pattern in data.patterns) {
			var pattern = Convert (data_pattern, iWait, iStep);
			if (pattern != null) {
				createList.Add (pattern);
			}
		}
		if (createList.Count == 0) return null;

		var results = new List<MaimaiNote>();
		var soundTimes = new HashSet<long> ();
		results.Add (mNote);
		foreach (var pattern in createList) {
			pattern.SetRelativeNote (head);
			foreach (var chain in pattern.chains) {
				chain.Setup (pattern, head.GetSlideReadyTime());
				foreach (var slideNote in chain.checkPoints) {
					// スライドノートの判定の開始を許可する時間を設定する.
					slideNote.judgementableTime = head.GetSlideReadyTime() - MaimaiJudgeEvaluateManager.TapFastGoodTime;
				}
				// スライドパターン内(同一のヘッドを持つスライド群)においては、同じスライド開始時間であるならSEを複数鳴らさないようにする.
				if (!soundTimes.Contains (chain.onStarMoveStartTime)) {
					results.Add (new MaimaiSlideNoteMoveStart (uniqueId.ToString (), chain));
					uniqueId++;
					soundTimes.Add (chain.onStarMoveStartTime);
				}
				results.AddRange (chain.checkPoints);
			}
		}

		return results.ToArray();
	}
	
	public MaimaiNote Convert (MaimaiScore.NoteBreak data) {
		MaimaiNote result;
		int b = data.button.GetIndex ();
		if (!this.breakToTap) {
			result = new MaimaiBreakNote (uniqueId.ToString (), GetTimeMS (), MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.BREAK_CIRCLE), Constants.instance.MAIMAI_NOTE_ACTION_ID_BASE + b, b);
		}
		else {
			result = new MaimaiTapNote (uniqueId.ToString (), GetTimeMS (), MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.TAP_CIRCLE), Constants.instance.MAIMAI_NOTE_ACTION_ID_BASE + b, b);
		}
		uniqueId++;
		return result;
	}
	
	public MaimaiNote Convert (MaimaiScore.NoteMessage data) {
		MaimaiNote result;
		decimal subTime;
		decimal? tempSubTime = Convert (data.step);
		if (tempSubTime.HasValue) subTime = tempSubTime.Value;
		else return null;
		result = new MaimaiMessageNote (uniqueId.ToString (), GetTimeMS (), data.message, data.position, data.scale, data.color, (long)(subTime * 1000.0M));
		decimal leaveTime = GetTime () + subTime;
		long leaveTimeMS = (long)(leaveTime * 1000.0M);
		subTimeMaxValue = MathlMax (subTimeMaxValue, leaveTimeMS);
		uniqueId++;
		return result;
	}
	
	public MaimaiNote Convert (MaimaiScore.NoteScrollMessage data) {
		MaimaiNote result;
		decimal subTime;
		decimal? tempSubTime = Convert (data.step);
		if (tempSubTime.HasValue) subTime = tempSubTime.Value;
		else return null;
		result = new MaimaiScrollMessageNote (uniqueId.ToString (), GetTimeMS (), data.message, data.y, data.scale, data.color, (long)(subTime * 1000.0M));
		decimal leaveTime = GetTime () + subTime;
		long leaveTimeMS = (long)(leaveTime * 1000.0M);
		subTimeMaxValue = MathlMax (subTimeMaxValue, leaveTimeMS);
		uniqueId++;
		return result;
	}
	
	public MaimaiNote Convert (MaimaiScore.NoteSoundMessage data) {
		MaimaiNote result;
		result = new MaimaiImportedSoundNote (uniqueId.ToString (), GetTimeMS (), data.soundId);
		uniqueId++;
		return result;
	}
	
	private decimal? Convert (MaimaiScore.IStep iStep) {
		if (iStep is MaimaiScore.StepLength) {
			if (!m_bpm.HasValue) {
				return null;
			}
			var step = iStep as MaimaiScore.StepLength;
			return (CalcTime (m_bpm.Value, (decimal)step.beat, (decimal)step.length));
		}
		else if (iStep is MaimaiScore.StepLocalBpm) {
			var step = iStep as MaimaiScore.StepLocalBpm;
			return (CalcTime ((decimal)step.local_bpm, (decimal)step.beat, (decimal)step.length));
		}
		else if (iStep is MaimaiScore.StepInterval) {
			var step = iStep as MaimaiScore.StepInterval;
			return (decimal)step.interval;
		}
		return null;
	}
	
	private IMaimaiSlideNoteHead Convert (MaimaiScore.ISlideHead iHead) {
		if (iHead is MaimaiScore.SlideHeadStar) {
			var head = iHead as MaimaiScore.SlideHeadStar;
			int b = head.button.GetIndex ();
			if (!this.tapToBreak) {
				var ret = new MaimaiStarNote (uniqueId.ToString (), GetTimeMS (), MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.TAP_STAR), Constants.instance.MAIMAI_NOTE_ACTION_ID_BASE + b, b);
				uniqueId++;
				return ret;
			}
			else {
				var ret =  new MaimaiStarBreakNote (uniqueId.ToString (), GetTimeMS (), MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.TAP_STAR), Constants.instance.MAIMAI_NOTE_ACTION_ID_BASE + b, b);
				uniqueId++;
				return ret;
			}
		}
		else if (iHead is MaimaiScore.SlideHeadBreakStar) {
			var head = iHead as MaimaiScore.SlideHeadBreakStar;
			int b = head.button.GetIndex ();
			if (!this.breakToTap) {
				var ret =  new MaimaiStarBreakNote (uniqueId.ToString (), GetTimeMS (), MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.TAP_STAR), Constants.instance.MAIMAI_NOTE_ACTION_ID_BASE + b, b);
				uniqueId++;
				return ret;
			}
			else {
				var ret =  new MaimaiStarNote (uniqueId.ToString (), GetTimeMS (), MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.TAP_STAR), Constants.instance.MAIMAI_NOTE_ACTION_ID_BASE + b, b);
				uniqueId++;
				return ret;
			}
		}
		else if (iHead is MaimaiScore.SlideHeadNothing) {
			var ret = new MaimaiSlideNoteHead(uniqueId.ToString(), GetTimeMS());
			uniqueId++;
			return ret;
		}
		return null;
	}
	
	private decimal? Convert (MaimaiScore.ISlideHeadCustomWait iWait) {
		if (iWait is MaimaiScore.SlideHeadCustomWaitDefault) {
			if (!m_bpm.HasValue) {
				return null;
			}
			return (CalcTime (m_bpm.Value, 4M, 1M));
		}
		else if (iWait is MaimaiScore.SlideHeadCustomWaitLocalBpm) {
			var wait = iWait as MaimaiScore.SlideHeadCustomWaitLocalBpm;
			return (CalcTime ((decimal)wait.local_bpm, 4M, 1M));
		}
		else if (iWait is MaimaiScore.SlideHeadCustomWaitInterval) {
			var wait = iWait as MaimaiScore.SlideHeadCustomWaitInterval;
			return (decimal)wait.interval;
		}
		return null;
	}
	
	private MaimaiSlidePattern Convert (MaimaiScore.ISlidePattern iPattern, MaimaiScore.ISlideHeadCustomWait wholeWait, MaimaiScore.IStep wholeStep) {
		if (iPattern is MaimaiScore.SlidePattern) {
			var pattern = iPattern as MaimaiScore.SlidePattern;
			var iWait = pattern.wait == null ? wholeWait : pattern.wait;
			var iStep = pattern.step == null ? wholeStep : pattern.step;
			var createList = new List<MaimaiSlideChain>();
			foreach (var chain in pattern.chains) {
				var data = Convert (chain, iWait, iStep);
				if (data != null) {
					createList.Add (data);
				}
			}
			if (createList.Count == 0) return null;
			var ret = new MaimaiSlidePattern (createList.ToArray());
			uniqueId++;
			return ret;
		}
		return null;
	}
	
	private MaimaiSlideChain Convert (MaimaiScore.ISlideChain iChain, MaimaiScore.ISlideHeadCustomWait wholeWait, MaimaiScore.IStep wholeStep) {
		if (iChain is MaimaiScore.SlideChain) {
			var chain = iChain as MaimaiScore.SlideChain;
			var _wait = Convert (chain.wait);
			if (!_wait.HasValue) _wait = Convert (wholeWait);
			if (!_wait.HasValue) _wait = Convert (MaimaiScore.SlideHeadCustomWaitDefault.Create());
			var _step = Convert (chain.step);
			if (!_step.HasValue) _step = Convert (wholeStep);
			if (!_wait.HasValue || !_step.HasValue) {
				return null;
			}
			// 時間を作成.
			long waitTimeMS = (long)(_wait.Value * 1000.0M);
			long stepTimeMS = (long)((GetTime () + _step.Value) * 1000.0M);
			subTimeMaxValue = MathlMax (subTimeMaxValue, stepTimeMS);

			// コマンドを作成.
			float totalDistance = 0;
			var createList = new List<MaimaiSlideCommand>();
			foreach (var command in chain.commands) {
				var data = Convert (command, totalDistance);
				if (data != null) {
					totalDistance += data.distance;
					createList.Add (data);
				}
			}

			// チェックポイントを作成.
			var commandSensorIds = new Constants.SensorId[createList.Count][];
			for (int i = 0; i < createList.Count; i++) {
				commandSensorIds[i] = createList[i].GetCheckPointSensorIds ();
			}
			var sensorIds = MaimaiSlideChain.CreateCheckPointSensors (commandSensorIds);
			var createList2 = new List<MaimaiSlideNote>();
			foreach (var sensorId in sensorIds) {
				createList2.Add (new MaimaiSlideNote (uniqueId.ToString (), waitTimeMS + stepTimeMS, MaimaiJudgeEvaluateManager.MissTimeBank (MaimaiNoteType.SLIDE), Constants.instance.GetSlideActionId (sensorId), sensorId)); 
			}

			if (createList.Count == 0 || createList2.Count == 0) return null;
			return new MaimaiSlideChain (waitTimeMS, waitTimeMS + stepTimeMS, createList.ToArray(), createList2.ToArray());
		}
		return null;
	}

	private MaimaiSlideCommand Convert (MaimaiScore.ISlideCommand iCommand, float totalDistance) {
		if (iCommand is MaimaiScore.SlideCommandStraight) {
			var command = iCommand as MaimaiScore.SlideCommandStraight;
			return new MaimaiSlideStraightCommand (command.start, command.target, totalDistance);
		}
		else if (iCommand is MaimaiScore.SlideCommandCurve) {
			var command = iCommand as MaimaiScore.SlideCommandCurve;
			var dd = command.distance_degree < 0 ? -command.distance_degree : command.distance_degree;
			var vec = command.distance_degree < 0 ? -1 : 1;
			return new MaimaiSlideCurveCommand (command.center, command.radius, command.start_degree, dd, vec, totalDistance);
		}
		return null;
	}

	public static long MathlMax (long value1, long value2) {
		if (value2 > value1)
			return value2;
		return value1;
	}

	public long GetFinishTimeMS () {
		return MathlMax (subTimeMaxValue, GetTimeMS ());
	}
}






















/*
public static class SlidePatternFactoryGenericPattern {
	public static int AddTurnRefMirror (int button, int turn, bool mirror) {
		int ret = (button + turn) % 8;
		return mirror ? 7 - ret : ret;
	}
	
	public static bool AddPattern(this SlidePatternFactory factory, MaimaiScoreSlidePatternFactoryCommandData command, int turn, bool mirror) {
		int start = AddTurnRefMirror (command.factory.start - 1, turn, mirror);
		int target = AddTurnRefMirror (command.factory.target - 1, turn, mirror);
		
		switch (command.factory.command_type) {
		case MaimaiScoreSlidePatternFactoryCommandData.PatternFactoryCommandData.FACTORY_COMMAND_OUTER_STRAIGHT_OUTER:
			return factory.AddPatternOuterStraightOuter(start, target);
		case MaimaiScoreSlidePatternFactoryCommandData.PatternFactoryCommandData.FACTORY_COMMAND_OUTER_STRAIGHT_INNER:
			return factory.AddPatternOuterStraightInner(start, target);
		case MaimaiScoreSlidePatternFactoryCommandData.PatternFactoryCommandData.FACTORY_COMMAND_OUTER_STRAIGHT_CENTER:
			return factory.AddPatternOuterStraightCenter(start);
		case MaimaiScoreSlidePatternFactoryCommandData.PatternFactoryCommandData.FACTORY_COMMAND_OUTER_CURVE_CLOCKWISE:
			if (mirror) return factory.AddPatternOuterCurveCounterClockwise(start, target);
			return factory.AddPatternOuterCurveClockwise(start, target);
		case MaimaiScoreSlidePatternFactoryCommandData.PatternFactoryCommandData.FACTORY_COMMAND_OUTER_CURVE_COUNTERCLOCKWISE:
			if (mirror) return factory.AddPatternOuterCurveClockwise(start, target);
			return factory.AddPatternOuterCurveCounterClockwise(start, target);
		case MaimaiScoreSlidePatternFactoryCommandData.PatternFactoryCommandData.FACTORY_COMMAND_INNER_STRAIGHT_OUTER:
			return factory.AddPatternInnerStraightOuter(start, target);
		case MaimaiScoreSlidePatternFactoryCommandData.PatternFactoryCommandData.FACTORY_COMMAND_INNER_STRAIGHT_INNER:
			return factory.AddPatternInnerStraightInner(start, target);
		case MaimaiScoreSlidePatternFactoryCommandData.PatternFactoryCommandData.FACTORY_COMMAND_INNER_STRAIGHT_CENTER:
			return factory.AddPatternInnerStraightCenter(start);
		case MaimaiScoreSlidePatternFactoryCommandData.PatternFactoryCommandData.FACTORY_COMMAND_INNER_CURVE_CLOCKWISE:
			if (mirror) return factory.AddPatternInnerCurveCounterClockwise(start, target);
			return factory.AddPatternInnerCurveClockwise(start, target);
		case MaimaiScoreSlidePatternFactoryCommandData.PatternFactoryCommandData.FACTORY_COMMAND_INNER_CURVE_COUNTERCLOCKWISE:
			if (mirror) return factory.AddPatternInnerCurveClockwise(start, target);
			return factory.AddPatternInnerCurveCounterClockwise(start, target);
		case MaimaiScoreSlidePatternFactoryCommandData.PatternFactoryCommandData.FACTORY_COMMAND_CENTER_STRAIGHT_OUTER:
			return factory.AddPatternCenterStraightOuter(target);
		case MaimaiScoreSlidePatternFactoryCommandData.PatternFactoryCommandData.FACTORY_COMMAND_CENTER_STRAIGHT_INNER:
			return factory.AddPatternCenterStraightInner(target);
		case MaimaiScoreSlidePatternFactoryCommandData.PatternFactoryCommandData.FACTORY_COMMAND_SHAPE_P:
			if (mirror) return factory.AddPatternShapeQ(start, target);
			return factory.AddPatternShapeP(start, target);
		case MaimaiScoreSlidePatternFactoryCommandData.PatternFactoryCommandData.FACTORY_COMMAND_SHAPE_Q:
			if (mirror) return factory.AddPatternShapeP(start, target);
			return factory.AddPatternShapeQ(start, target);
		case MaimaiScoreSlidePatternFactoryCommandData.PatternFactoryCommandData.FACTORY_COMMAND_SHAPE_S:
			if (mirror) return factory.AddPatternShapeZ(start, target);
			return factory.AddPatternShapeS(start, target);
		case MaimaiScoreSlidePatternFactoryCommandData.PatternFactoryCommandData.FACTORY_COMMAND_SHAPE_Z:
			if (mirror) return factory.AddPatternShapeS(start, target);
			return factory.AddPatternShapeZ(start, target);
		case MaimaiScoreSlidePatternFactoryCommandData.PatternFactoryCommandData.FACTORY_COMMAND_SHAPE_V:
			return factory.AddPatternShapeV(start, target);
		}
		return false;
	}
	
	public static bool AddPatternOuterStraightOuter(this SlidePatternFactory factory, int start, int target) {
		bool failed = false;
		if (!factory.SetPattern (SlidePatternFactory.Circumference.OUTER, SlidePatternFactory.Circumference.OUTER, start, target, SlidePatternFactory.SlideVector.STRAIGHT)) {
			failed = true;
		}
		return !failed;
	}
	
	public static bool AddPatternOuterStraightInner(this SlidePatternFactory factory, int start, int target) {
		bool failed = false;
		if (!factory.SetPattern (SlidePatternFactory.Circumference.OUTER, SlidePatternFactory.Circumference.INNER, start, target, SlidePatternFactory.SlideVector.STRAIGHT)) {
			failed = true;
		}
		return !failed;
	}
	
	public static bool AddPatternOuterStraightCenter(this SlidePatternFactory factory, int start) {
		bool failed = false;
		if (!factory.SetPattern (SlidePatternFactory.Circumference.OUTER, SlidePatternFactory.Circumference.CENTER, start, SlidePatternFactory.UN_USE_PARAM, SlidePatternFactory.SlideVector.STRAIGHT)) {
			failed = true;
		}
		return !failed;
	}
	
	public static bool AddPatternOuterCurveClockwise(this SlidePatternFactory factory, int start, int target) {
		bool failed = false;
		if (!factory.SetPattern (SlidePatternFactory.Circumference.OUTER, SlidePatternFactory.Circumference.OUTER, start, target, SlidePatternFactory.SlideVector.CURVE_CLOCK)) {
			failed = true;
		}
		return !failed;
	}
	
	public static bool AddPatternOuterCurveCounterClockwise(this SlidePatternFactory factory, int start, int target) {
		bool failed = false;
		if (!factory.SetPattern (SlidePatternFactory.Circumference.OUTER, SlidePatternFactory.Circumference.OUTER, start, target, SlidePatternFactory.SlideVector.CURVE_REVERSE_CLOCK)) {
			failed = true;
		}
		return !failed;
	}
	
	public static bool AddPatternInnerStraightOuter(this SlidePatternFactory factory, int start, int target) {
		bool failed = false;
		if (!factory.SetPattern (SlidePatternFactory.Circumference.INNER, SlidePatternFactory.Circumference.OUTER, start, target, SlidePatternFactory.SlideVector.STRAIGHT)) {
			failed = true;
		}
		return !failed;
	}
	
	public static bool AddPatternInnerStraightInner(this SlidePatternFactory factory, int start, int target) {
		bool failed = false;
		if (!factory.SetPattern (SlidePatternFactory.Circumference.INNER, SlidePatternFactory.Circumference.INNER, start, target, SlidePatternFactory.SlideVector.STRAIGHT)) {
			failed = true;
		}
		return !failed;
	}
	
	public static bool AddPatternInnerStraightCenter(this SlidePatternFactory factory, int start) {
		bool failed = false;
		if (!factory.SetPattern (SlidePatternFactory.Circumference.INNER, SlidePatternFactory.Circumference.CENTER, start, SlidePatternFactory.UN_USE_PARAM, SlidePatternFactory.SlideVector.STRAIGHT)) {
			failed = true;
		}
		return !failed;
	}
	
	public static bool AddPatternInnerCurveClockwise(this SlidePatternFactory factory, int start, int target) {
		bool failed = false;
		if (!factory.SetPattern (SlidePatternFactory.Circumference.INNER, SlidePatternFactory.Circumference.INNER, start, target, SlidePatternFactory.SlideVector.CURVE_CLOCK)) {
			failed = true;
		}
		return !failed;
	}
	
	public static bool AddPatternInnerCurveCounterClockwise(this SlidePatternFactory factory, int start, int target) {
		bool failed = false;
		if (!factory.SetPattern (SlidePatternFactory.Circumference.INNER, SlidePatternFactory.Circumference.INNER, start, target, SlidePatternFactory.SlideVector.CURVE_REVERSE_CLOCK)) {
			failed = true;
		}
		return !failed;
	}
	
	public static bool AddPatternCenterStraightOuter(this SlidePatternFactory factory, int target) {
		bool failed = false;
		if (!factory.SetPattern (SlidePatternFactory.Circumference.CENTER, SlidePatternFactory.Circumference.OUTER, SlidePatternFactory.UN_USE_PARAM, target, SlidePatternFactory.SlideVector.STRAIGHT)) {
			failed = true;
		}
		return !failed;
	}
	
	public static bool AddPatternCenterStraightInner(this SlidePatternFactory factory, int target) {
		bool failed = false;
		if (!factory.SetPattern (SlidePatternFactory.Circumference.CENTER, SlidePatternFactory.Circumference.INNER, SlidePatternFactory.UN_USE_PARAM, target, SlidePatternFactory.SlideVector.STRAIGHT)) {
			failed = true;
		}
		return !failed;
	}
	
	public static bool AddPatternShapeP(this SlidePatternFactory factory, int start, int target) {
		bool failed = false;
		//if (t==(s+4)%8) As-B((s+6)%8)-At
		//else As-B((s+6)%8)<B((t+2)%8)-At
		if (factory.SetPattern(SlidePatternFactory.Circumference.OUTER, SlidePatternFactory.Circumference.INNER, start, start - 2, SlidePatternFactory.SlideVector.STRAIGHT)) {
			if (target == (start + 4) % 8) {
				if (!factory.SetPattern(SlidePatternFactory.Circumference.INNER, SlidePatternFactory.Circumference.OUTER, start - 2, target, SlidePatternFactory.SlideVector.STRAIGHT)) {
					failed = true;
				}
			}
			else {
				if (factory.SetPattern(SlidePatternFactory.Circumference.INNER, SlidePatternFactory.Circumference.INNER, start - 2, target + 2, SlidePatternFactory.SlideVector.CURVE_REVERSE_CLOCK)) {
					if (!factory.SetPattern(SlidePatternFactory.Circumference.INNER, SlidePatternFactory.Circumference.OUTER, target + 2, target, SlidePatternFactory.SlideVector.STRAIGHT)) {
						failed = true;
					}
				}
				else {
					failed = true;
				}
			}
		}
		else {
			failed = true;
		}
		return !failed;
	}
	
	public static bool AddPatternShapeQ(this SlidePatternFactory factory, int start, int target) {
		bool failed = false;
		//if (t==(s+4)%8) As-B((s+2)%8)-At
		//else As-B((s+2)%8)>B((t+6)%8)-At
		if (factory.SetPattern(SlidePatternFactory.Circumference.OUTER, SlidePatternFactory.Circumference.INNER, start, start + 2, SlidePatternFactory.SlideVector.STRAIGHT)) {
			if (target == (start + 4) % 8) {
				if (!factory.SetPattern(SlidePatternFactory.Circumference.INNER, SlidePatternFactory.Circumference.OUTER, start + 2, target, SlidePatternFactory.SlideVector.STRAIGHT)) {
					failed = true;
				}
			}
			else {
				if (factory.SetPattern(SlidePatternFactory.Circumference.INNER, SlidePatternFactory.Circumference.INNER, start + 2, target - 2, SlidePatternFactory.SlideVector.CURVE_CLOCK)) {
					if (!factory.SetPattern(SlidePatternFactory.Circumference.INNER, SlidePatternFactory.Circumference.OUTER, target - 2, target, SlidePatternFactory.SlideVector.STRAIGHT)) {
						failed = true;
					}
				}
				else {
					failed = true;
				}
			}
		}
		else {
			failed = true;
		}
		return !failed;
	}
	
	public static bool AddPatternShapeS(this SlidePatternFactory factory, int start, int target) {
		bool failed = false;
		//As-B((s+6)%8)-C-B((t+2)%8)-At
		if (factory.SetPattern(SlidePatternFactory.Circumference.OUTER, SlidePatternFactory.Circumference.INNER, start, start - 2, SlidePatternFactory.SlideVector.STRAIGHT)) {
			if (factory.SetPattern(SlidePatternFactory.Circumference.INNER, SlidePatternFactory.Circumference.INNER, start - 2, target - 2, SlidePatternFactory.SlideVector.STRAIGHT)) {
				if (!factory.SetPattern(SlidePatternFactory.Circumference.INNER, SlidePatternFactory.Circumference.OUTER, target - 2, target, SlidePatternFactory.SlideVector.STRAIGHT)) {
					failed = true;
				}
			}
			else {
				failed = true;
			}
		}
		else {
			failed = true;
		}
		return !failed;
	}
	
	public static bool AddPatternShapeZ(this SlidePatternFactory factory, int start, int target) {
		bool failed = false;
		//As-B((s+2)%8)-C-B((t+2)%8)-At
		if (factory.SetPattern(SlidePatternFactory.Circumference.OUTER, SlidePatternFactory.Circumference.INNER, start, start + 2, SlidePatternFactory.SlideVector.STRAIGHT)) {
			if (factory.SetPattern(SlidePatternFactory.Circumference.INNER, SlidePatternFactory.Circumference.INNER, start + 2, target + 2, SlidePatternFactory.SlideVector.STRAIGHT)) {
				if (!factory.SetPattern(SlidePatternFactory.Circumference.INNER, SlidePatternFactory.Circumference.OUTER, target + 2, target, SlidePatternFactory.SlideVector.STRAIGHT)) {
					failed = true;
				}
			}
			else {
				failed = true;
			}
		}
		else {
			failed = true;
		}
		return !failed;
	}
	
	public static bool AddPatternShapeV(this SlidePatternFactory factory, int start, int target) {
		bool failed = false;
		//As-Bs-C-Bt-At
		if (factory.SetPattern(SlidePatternFactory.Circumference.OUTER, SlidePatternFactory.Circumference.CENTER, start, SlidePatternFactory.UN_USE_PARAM, SlidePatternFactory.SlideVector.STRAIGHT)) {
			if (!factory.SetPattern(SlidePatternFactory.Circumference.CENTER, SlidePatternFactory.Circumference.OUTER, SlidePatternFactory.UN_USE_PARAM, target, SlidePatternFactory.SlideVector.STRAIGHT)) {
				failed = true;
			}
		}
		else {
			failed = true;
		}
		return !failed;
	}
}
*/
