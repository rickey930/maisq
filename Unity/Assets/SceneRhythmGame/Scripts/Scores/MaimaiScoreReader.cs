﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MaimaiScoreReader {
	public MaimaiScoreReader() {
		score = new List<MaimaiScore.Each> ();
	}

	// member variable.
	private List<MaimaiScore.Each> score;
	protected float bpm;
	protected float beat;
	protected float interval;
	protected float oldBpm;
	protected float oldBeat;
	protected float oldInterval;

	// member method.
	public virtual void SetBpm (float value) {
		bpm = value;
	}

	public virtual void SetBeat (float value) {
		beat = value;
	}

	public virtual void SetInterval (float value) {
		interval = value;
	}

	public void SetNote (params MaimaiScore.INote[] notes) {
		if (!(bpm > 0) || !(beat > 0 || interval > 0)) {
			return;
		}
		
		MaimaiScore.Each each;
		if (oldBpm != bpm && oldBeat != beat) {
			each = MaimaiScore.Each.Create_Bpm_Beat(bpm, beat, notes);
		}
		else if (oldBpm != bpm && oldInterval != interval) {
			each = MaimaiScore.Each.Create_Bpm_Interval(bpm, interval, notes);
		}
		else if (oldBpm != bpm) {
			each = MaimaiScore.Each.Create_Bpm(bpm, notes);
		}
		else if (oldBeat != beat) {
			each = MaimaiScore.Each.Create_Beat(beat, notes);
		}
		else if (oldInterval != interval) {
			each = MaimaiScore.Each.Create_Interval(interval, notes);
		}
		else {
			each = MaimaiScore.Each.Create(notes);
		}
		score.Add(each);
		
		oldBpm = bpm;
		oldBeat = beat;
		oldInterval = interval;
	}

	public MaimaiScore.Each[] GetMaimaiScore() {
		return score.ToArray();
	}




}
