using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MaisqScore {
	public static class CommandNameDocument {
		public const string BPM = "bpm";
		public const string BEAT = "beat";
		public const string INTERVAL = "interval";
		public const string NOTE = "note";
		public const string REST = "rest";
		public const string LOG = "log";
		public const string LOOP = "loop";
		public const string DEFINE = "define";
		public const string CALL = "call";
		public const string MATH_ADDITION = "add";
		public const string MATH_SUBTRACTION = "sub";
		public const string MATH_MULTIPLICATION = "mul";
		public const string MATH_DIVISION = "div";
		public const string MATH_POWER = "pow";
		public const string MATH_MODULO = "mod";
		public const string ANGLE_X = "angle_x";
		public const string ANGLE_Y = "angle_y";
		public const string SENSOR_DEGREE = "sensor_deg";
		public const string OUTER_SENSOR_POSITION_X = "outer_sensor_x";
		public const string OUTER_SENSOR_POSITION_Y = "outer_sensor_y";
		public const string INNER_SENSOR_POSITION_X = "inner_sensor_x";
		public const string INNER_SENSOR_POSITION_Y = "inner_sensor_y";
		public const string CENTER_SENSOR_POSITION_X = "center_sensor_x";
		public const string CENTER_SENSOR_POSITION_Y = "center_sensor_y";
		public const string OUTER_SENSOR_RADIUS = "outer_sensor_radius";
		public const string INNER_SENSOR_RADIUS = "inner_sensor_radius";
		public const string CENTER_SENSOR_RADIUS = "center_sensor_radius";
		public const string SENSOR_DEGREE_DISTANCE_CLOCKWISE = "sensor_deg_distance_clockwise";
		public const string SENSOR_DEGREE_DISTANCE_COUNTERCLOCKWISE = "sensor_deg_distance_counterclockwise";
		public const string SENSOR_DEGREE_DISTANCE_RIGHT = "sensor_deg_distance_right";
		public const string SENSOR_DEGREE_DISTANCE_LEFT = "sensor_deg_distance_left";
		public const string TURN = "turn";
		public const string MIRROR_HOLIZONTAL = "h_mirror";
		public const string MIRROR_VERTICAL = "v_mirror";
		public const string POSITION = "pos";
		public const string ANGLE = "angle";
		public const string OUTER_SENSOR_POSITION = "outer_sensor_pos";
		public const string INNER_SENSOR_POSITION = "inner_sensor_pos";
		public const string CENTER_SENSOR_POSITION = "center_sensor_pos";
		public const string RGB = "rgb";
		public const string RGBA = "rgba";
		public const string ARGB = "argb";
		public const string TAP = "tap";
		public const string HOLD = "hold";
		public const string SLIDE = "slide";
		public const string BREAK = "break";
		public const string MESSAGE = "message";
		public const string SCROLL_MESSAGE = "scroll_message";
		public const string SOUND_MESSAGE = "sound_message";
		public const string STEP = "step";
		public const string STAR = "star";
		public const string BREAK_STAR = "break_star";
		public const string NOTHING = "nothing";
		public const string WAIT_DEFAULT = "wait_default";
		public const string WAIT_CUSTOM_LOCAL_BPM = "wait_custom_bpm";
		public const string WAIT_CUSTOM_INTERVAL = "wait_custom_interval";
		public const string PATTERN = "pattern";
		public const string CHAIN = "chain";
		public const string STRAIGHT = "straight";
		public const string CONTINUED_STRAIGHT = "continued_straight";
		public const string CURVE = "curve";
		public const string CONTINUED_CURVE = "continued_curve";
		public const string NULL = "null";
		public const string SIMAI_EXPAND = "simai_expand";
		public const string SIMAI_EXPAND_AA = "simai_expand_AA";
		public const string SIMAI_EXPAND_AB = "simai_expand_AB";
		public const string SIMAI_EXPAND_AC = "simai_expand_AC";
		public const string SIMAI_EXPAND_BA = "simai_expand_BA";
		public const string SIMAI_EXPAND_BB = "simai_expand_BB";
		public const string SIMAI_EXPAND_BC = "simai_expand_BC";
		public const string SIMAI_EXPAND_CA = "simai_expand_CA";
		public const string SIMAI_EXPAND_CB = "simai_expand_CB";
		public const string SIMAI_EXPAND_CC = "simai_expand_CC";

		// option
		public const string SECRET = "secret";

		// colors
		public const string BLACK = "black";
		public const string WHITE = "white";
		public const string GRAY = "gray";
		public const string RED = "red";
		public const string GREEN = "green";
		public const string BLUE = "blue";
		public const string YELLOW = "yellow";
		public const string MAGENTA = "magenta";
		public const string CYAN = "cyan";

		// [直線]_[始点]_から_[終点]
		// [曲線]_[始点と終点の半径]_[回転方向]_[中心軸]
		public const string OUTER_STRAIGHT_OUTER = "outer_straight_outer";
		public const string OUTER_STRAIGHT_INNER = "outer_straight_inner";
		public const string OUTER_STRAIGHT_CENTER = "outer_straight_center";
		public const string OUTER_CURVE_CLOCKWISE_AXIS_CENTER = "outer_curve_clockwise_axis_center";
		public const string OUTER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER = "outer_curve_counterclockwise_axis_center";
		public const string OUTER_CURVE_RIGHT_AXIS_CENTER = "outer_curve_right_axis_center";
		public const string OUTER_CURVE_LEFT_AXIS_CENTER = "outer_curve_left_axis_center";
		public const string INNER_STRAIGHT_OUTER = "inner_straight_outer";
		public const string INNER_STRAIGHT_INNER = "inner_straight_inner";
		public const string INNER_STRAIGHT_CENTER = "inner_straight_center";
		public const string INNER_CURVE_CLOCKWISE_AXIS_CENTER = "inner_curve_clockwise_axis_center";
		public const string INNER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER = "inner_curve_counterclockwise_axis_center";
		public const string INNER_CURVE_RIGHT_AXIS_CENTER = "inner_curve_right_axis_center";
		public const string INNER_CURVE_LEFT_AXIS_CENTER = "inner_curve_left_axis_center";
		public const string CENTER_STRAIGHT_OUTER = "center_straight_outer";
		public const string CENTER_STRAIGHT_INNER = "center_straight_inner";
		public const string SHAPE_P_AXIS_CENTER = "shape_p_axis_center";
		public const string SHAPE_Q_AXIS_CENTER = "shape_q_axis_center";
		public const string SHAPE_V_AXIS_CENTER = "shape_v_axis_center";
		public const string SHAPE_S = "shape_s";
		public const string SHAPE_Z = "shape_z";
		public const string SHAPE_PP = "shape_pp";
		public const string SHAPE_QQ = "shape_qq";
		public const string SHAPE_V_AXIS_OUTER = "shape_v_axis_outer";
		
	}
}