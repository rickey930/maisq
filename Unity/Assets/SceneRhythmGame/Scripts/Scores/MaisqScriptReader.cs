using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

public class MaisqScriptReader : MaimaiScoreReader {
	public MaisqScriptReader() : base() {
	}

	private Dictionary<string, MaisqScriptUserDefine> _userDefine;
	protected Dictionary<string, MaisqScriptUserDefine> userDefine {
		get {
			if (_userDefine == null) {
				_userDefine = new Dictionary<string, MaisqScriptUserDefine>();
			}
			return _userDefine;
		}
	}
	
	/// <summary>
	/// MaiScriptテキストデータを読み込む. 読み込んだゲーム用譜面データはGetMaimaiScore()で取得. 
	/// </summary>
	/// <param name="maiScript">Mai script.</param>
	public void ReadMaiScript(string maiScript) {
		MaisqScriptTree[] trees = MaisqScriptTree.CreateTreeFromMaiScript (maiScript).ToArray();
		foreach (var tree in trees) {
			ReadCreateCommandTypeOfGlobal(tree);
		}
	}
	
	/// <summary>
	/// テキストからコメントを抜く. 「//から\n」「/*から*/」を条件に抜く.
	/// </summary>
	/// <param name="simaiRule">「>>から/n」も条件に加える</para>」</para>
	public static string RemoveComment(string text, bool simaiRule) {
		string text1 = text.Replace ("\r", "");
		string text2 = "";
		bool lineComment = false;
		bool blockComment = false;
		for (int i = 0; i < text1.Length; i++) {
			string sub0 = text1.Substring(i, 1);
			string sub1 = "";
			if (i + 1 < text1.Length) sub1 = text1.Substring(i, 2);
			if (sub0 == "/") {
				if (!blockComment && sub1 == "//") {
					lineComment = true;
				}
				if (!lineComment && sub1 == "/*") {
					blockComment = true;
				}
			}
			else if (simaiRule && sub0 == ">") {
				if (!blockComment && sub1 == ">>") {
					lineComment = true;
				}
			}
			if (!lineComment && !blockComment) {
				text2 += sub0;
			}
			if (sub0 == "\n" && lineComment) {
				lineComment = false;
			}
			if (sub1 == "*/" && blockComment) {
				blockComment = false;
				i++;
			}
		}
		return text2;
	}

	public void MaisqScriptReadErrorCommon (string commandType, MaisqScriptTree tree) {
		string message = "\"" + tree.data + "\" (" + tree.children.Count + " params) is not command of " + commandType + ".";
		string script = tree.Deploy ();
		Debug.LogError (message + " \n script:" + script);
	}
	
	#region Global
	public virtual void SetBpm (MaimaiScore.Value bpm) {
		base.SetBpm (bpm.GetValue ());
	}
	
	public virtual void SetBeat (MaimaiScore.Value beat) {
		base.SetBeat (beat.GetValue ());
	}
	
	public virtual void SetInterval (MaimaiScore.Value interval) {
		base.SetInterval (interval.GetValue ());
	}
	
	public virtual void SetBeat (MaimaiScore.Value beat, MaimaiScore.Number countInBar) {
		base.SetBeat (beat.GetValue ());
	}
	
	public virtual void SetInterval (MaimaiScore.Value interval, MaimaiScore.Number countInBar) {
		base.SetInterval (interval.GetValue ());
	}
	
	/// <summary>
	/// maiScriptのグローバル領域で使う関数の定義.
	/// </summary>
	protected virtual void ReadCreateCommandTypeOfGlobal(MaisqScriptTree tree) {
		var switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.BPM:
		{
			if (tree.children.Count == 1) {
				var value = ReadCreateCommandTypeOfValue (tree.children[0]);
				if (value != null) {
					SetBpm (value);
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.BEAT:
		{
			if (tree.children.Count == 1) {
				var value = ReadCreateCommandTypeOfValue (tree.children[0]);
				if (value != null) {
					SetBeat(value);
				}
			}
			else if (tree.children.Count == 2) {
				var value = ReadCreateCommandTypeOfValue (tree.children[0]);
				var number = ReadCreateCommandTypeOfNumber (tree.children[1]);
				if (value != null && number != null) {
					SetBeat(value, number);
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.INTERVAL:
		{
			if (tree.children.Count == 1) {
				var value = ReadCreateCommandTypeOfValue (tree.children[0]);
				if (value != null) {
					SetInterval(value);
				}
			}
			else if (tree.children.Count == 2) {
				var value = ReadCreateCommandTypeOfValue (tree.children[0]);
				var number = ReadCreateCommandTypeOfNumber (tree.children[1]);
				if (value != null && number != null) {
					SetInterval(value, number);
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.LOG:
		{
			if (tree.children.Count == 1) {
				Debug.Log(tree.children[0].data);
			}
			break;
		}
		case MaisqScore.CommandNameDocument.LOOP:
		{
			if (tree.children.Count > 1) {
				var number = ReadCreateCommandTypeOfNumber(tree.children[0]);
				int num = number.GetValue();
				for (int i = 0; i < num; i++) {
					for (int j = 1; j < tree.children.Count; j++) {
						ReadCreateCommandTypeOfGlobal (tree.children[j]);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.DEFINE:
		{
			var define = MaisqScriptUserDefine.FromMaiScriptTree (tree);
			if (define != null) {
				userDefine[define.name] = define;
			}
			break;
		}
		case MaisqScore.CommandNameDocument.NOTE:
		{
			List<MaimaiScore.INote> notes = new List<MaimaiScore.INote>();
			foreach (var child in tree.children) {
				var note = ReadCreateCommandTypeOfNote (child);
				if (note != null) {
					notes.Add (note);
				}
			}
			var array = notes.ToArray ();
			SetNote (array);
			break;
		}
		case MaisqScore.CommandNameDocument.REST:
		{
			if (tree.children.Count == 0) {
				var rest = new MaimaiScore.INote[0];
				SetNote (rest);
			}
			else if (tree.children.Count == 1) {
				var value = ReadCreateCommandTypeOfNumber (tree.children[0]);
				if (value != null) {
					int amount = value.GetValue ();
					for (int i = 0; i < amount; i++) {
						var rest = new MaimaiScore.INote[0];
						SetNote (rest);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CALL:
		{
			if (tree.children.Count >= 1) {
				if (userDefine.ContainsKey(tree.children[0].data)) {
					var replaced = userDefine[tree.children[0].data].Call (MaisqScriptUserDefine.CreateCallMethodParamsFromMaiScriptTree(tree));
					var calledTrees = MaisqScriptTree.CreateTreeFromMaiScript(replaced);
					foreach (var calledTree in calledTrees) {
						ReadCreateCommandTypeOfGlobal (calledTree);
					}
				}
				else {
					MaisqScriptReadErrorCommon ("define", tree);
				}
			}
			break;
		}
		default:
		{
			MaisqScriptReadErrorCommon ("global", tree);
			break;
		}
		}
	}

	protected T[] ReadCallCommand<T> (MaisqScriptTree tree, System.Func<MaisqScriptTree, T> readCommandMethod) {
		if (tree.children.Count >= 1) {
			var list = new List<T>();
			if (userDefine.ContainsKey(tree.children[0].data)) {
				var replaced = userDefine[tree.children[0].data].Call (MaisqScriptUserDefine.CreateCallMethodParamsFromMaiScriptTree(tree));
				var calledTrees = MaisqScriptTree.CreateTreeFromMaiScript(replaced);
				foreach (var calledTree in calledTrees) {
					list.Add(readCommandMethod (calledTree));
				}
				return list.ToArray();
			}
			else {
				MaisqScriptReadErrorCommon ("define", tree);
			}
		}
		return new T[0];
	}
	
	#endregion
	#region Number
	/// <summary>
	/// maiScriptを呼んで整数を返す.
	/// </summary>
	public MaimaiScore.Number ReadCreateCommandTypeOfNumber (MaisqScriptTree tree) {
		if (tree == null || tree.data.Length == 0) return null;

		MaimaiScore.Number instance = null;
		bool minus = false;
		string switchKey = tree.data;
		if (tree.data.Length > 0 && tree.data [0] == '-') {
			switchKey = tree.data.Substring (1);
			minus = true;
		}
		Func<MaimaiScore.Number, MaimaiScore.Number> signed = (num) => {
			if (minus)
			return MaimaiScore.Number.MathMultiplication (num, MaimaiScore.Number.Create(-1));
			return num;
		};
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.MATH_ADDITION:
		case MaisqScore.CommandNameDocument.MATH_SUBTRACTION:
		case MaisqScore.CommandNameDocument.MATH_MULTIPLICATION:
		case MaisqScore.CommandNameDocument.MATH_DIVISION:
		case MaisqScore.CommandNameDocument.MATH_POWER:
		case MaisqScore.CommandNameDocument.MATH_MODULO:
		{
			if (tree.children.Count == 2) {
				var value1 = ReadCreateCommandTypeOfNumber (tree.children [0]);
				var value2 = ReadCreateCommandTypeOfNumber (tree.children [1]);
				if (value1 != null && value2 != null) {
					switch (switchKey) {
					case MaisqScore.CommandNameDocument.MATH_ADDITION:
						instance = signed(MaimaiScore.Number.MathAddition (value1, value2));
						break;
					case MaisqScore.CommandNameDocument.MATH_SUBTRACTION:
						instance = signed(MaimaiScore.Number.MathSubtraction (value1, value2));
						break;
					case MaisqScore.CommandNameDocument.MATH_MULTIPLICATION:
						instance = signed(MaimaiScore.Number.MathMultiplication (value1, value2));
						break;
					case MaisqScore.CommandNameDocument.MATH_DIVISION:
						instance = signed(MaimaiScore.Number.MathDivision (value1, value2));
						break;
					case MaisqScore.CommandNameDocument.MATH_POWER:
						instance = signed(MaimaiScore.Number.MathPower (value1, value2));
						break;
					case MaisqScore.CommandNameDocument.MATH_MODULO:
						instance = signed(MaimaiScore.Number.MathModulo (value1, value2));
						break;
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CALL:
		{
			var called = ReadCallCommand<MaimaiScore.Number>(tree, ReadCreateCommandTypeOfNumber);
			if (called.Length > 0)
				instance = called[0];
			break;
		}
		default:
		{
			if (tree.children.Count == 0) {
				instance = MaimaiScore.Number.FromMaiScript (tree.data);
			}
			break;
		}
		}
		if (instance != null) {
			instance = ReadExtendCommandTypeOfNumber (instance, tree.extend);
			ReadOptionTypeOfNumber (instance, tree);
			return instance;
		}
		MaisqScriptReadErrorCommon ("number", tree);
		return null;
	}
	
	public MaimaiScore.Number ReadExtendCommandTypeOfNumber (MaimaiScore.Number instance, MaisqScriptTree tree) {
		if (instance == null || tree == null || tree.data.Length == 0) return instance;
		
		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.MATH_ADDITION:
		case MaisqScore.CommandNameDocument.MATH_SUBTRACTION:
		case MaisqScore.CommandNameDocument.MATH_MULTIPLICATION:
		case MaisqScore.CommandNameDocument.MATH_DIVISION:
		case MaisqScore.CommandNameDocument.MATH_POWER:
		case MaisqScore.CommandNameDocument.MATH_MODULO:
		{
			if (tree.children.Count == 1) {
				var value1 = ReadCreateCommandTypeOfNumber (tree.children [0]);
				if (value1 != null) {
					switch (switchKey) {
					case MaisqScore.CommandNameDocument.MATH_ADDITION:
						instance = instance.MathAddition (value1);
						break;
					case MaisqScore.CommandNameDocument.MATH_SUBTRACTION:
						instance = instance.MathSubtraction (value1);
						break;
					case MaisqScore.CommandNameDocument.MATH_MULTIPLICATION:
						instance = instance.MathMultiplication (value1);
						break;
					case MaisqScore.CommandNameDocument.MATH_DIVISION:
						instance = instance.MathDivision (value1);
						break;
					case MaisqScore.CommandNameDocument.MATH_POWER:
						instance = instance.MathPower (value1);
						break;
					case MaisqScore.CommandNameDocument.MATH_MODULO:
						instance = instance.MathModulo (value1);
						break;
					}
				}
			}
			break;
		}
		}
		instance = ReadExtendCommandTypeOfNumber (instance, tree.extend);
		ReadOptionTypeOfNumber (instance, tree);
		return instance;
	}
	
	public void ReadOptionTypeOfNumber (MaimaiScore.Number instance, MaisqScriptTree tree) {
		
	}
	
	#endregion
	#region Value
	/// <summary>
	/// maiScriptを呼んで小数を返す.
	/// </summary>
	public MaimaiScore.Value ReadCreateCommandTypeOfValue (MaisqScriptTree tree) {
		if (tree == null || tree.data.Length == 0) return null;

		MaimaiScore.Value instance = null;
		bool minus = false;
		string switchKey = tree.data;
		if (tree.data.Length > 0 && tree.data [0] == '-') {
			switchKey = tree.data.Substring (1);
			minus = true;
		}
		Func<MaimaiScore.Value, MaimaiScore.Value> signed = (num) => {
			if (minus)
			return MaimaiScore.Value.MathMultiplication (num, MaimaiScore.Value.Create(-1));
			return num;
		};
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.MATH_ADDITION:
		case MaisqScore.CommandNameDocument.MATH_SUBTRACTION:
		case MaisqScore.CommandNameDocument.MATH_MULTIPLICATION:
		case MaisqScore.CommandNameDocument.MATH_DIVISION:
		case MaisqScore.CommandNameDocument.MATH_POWER:
		case MaisqScore.CommandNameDocument.ANGLE_X:
		case MaisqScore.CommandNameDocument.ANGLE_Y:
		{
			if (tree.children.Count == 2) {
				var value1 = ReadCreateCommandTypeOfValue (tree.children [0]);
				var value2 = ReadCreateCommandTypeOfValue (tree.children [1]);
				if (value1 != null && value2 != null) {
					switch (switchKey) {
					case MaisqScore.CommandNameDocument.MATH_ADDITION:
						instance = signed (MaimaiScore.Value.MathAddition (value1, value2));
						break;
					case MaisqScore.CommandNameDocument.MATH_SUBTRACTION:
						instance = signed (MaimaiScore.Value.MathSubtraction (value1, value2));
						break;
					case MaisqScore.CommandNameDocument.MATH_MULTIPLICATION:
						instance = signed (MaimaiScore.Value.MathMultiplication (value1, value2));
						break;
					case MaisqScore.CommandNameDocument.MATH_DIVISION:
						instance = signed (MaimaiScore.Value.MathDivision (value1, value2));
						break;
					case MaisqScore.CommandNameDocument.MATH_POWER:
						instance = signed (MaimaiScore.Value.MathPower (value1, value2));
						break;
					case MaisqScore.CommandNameDocument.ANGLE_X:
						instance = signed (MaimaiScore.Value.AngleX (value1, value2));
						break;
					case MaisqScore.CommandNameDocument.ANGLE_Y:
						instance = signed (MaimaiScore.Value.AngleY (value1, value2));
						break;
					}
				}
			}
			else if (tree.children.Count == 1) {
				var value = ReadCreateCommandTypeOfValue (tree.children [0]);
				if (value != null) {
					switch (switchKey) {
					case MaisqScore.CommandNameDocument.ANGLE_X:
						instance = signed (MaimaiScore.Value.AngleX (value));
						break;
					case MaisqScore.CommandNameDocument.ANGLE_Y:
						instance = signed (MaimaiScore.Value.AngleY (value));
						break;
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SENSOR_DEGREE:
		case MaisqScore.CommandNameDocument.OUTER_SENSOR_POSITION_X:
		case MaisqScore.CommandNameDocument.OUTER_SENSOR_POSITION_Y:
		case MaisqScore.CommandNameDocument.INNER_SENSOR_POSITION_X:
		case MaisqScore.CommandNameDocument.INNER_SENSOR_POSITION_Y:
		{
			if (tree.children.Count == 1) {
				var button = ReadCreateCommandTypeOfButton (tree.children [0]);
				if (button != null) {
					switch (switchKey) {
					case MaisqScore.CommandNameDocument.SENSOR_DEGREE:
						instance = signed (MaimaiScore.Value.SensorDegree (button));
						break;
					case MaisqScore.CommandNameDocument.OUTER_SENSOR_POSITION_X:
						instance = signed (MaimaiScore.Value.OuterSensorPositionX (button));
						break;
					case MaisqScore.CommandNameDocument.OUTER_SENSOR_POSITION_Y:
						instance = signed (MaimaiScore.Value.OuterSensorPositionY (button));
						break;
					case MaisqScore.CommandNameDocument.INNER_SENSOR_POSITION_X:
						instance = signed (MaimaiScore.Value.InnerSensorPositionX (button));
						break;
					case MaisqScore.CommandNameDocument.INNER_SENSOR_POSITION_Y:
						instance = signed (MaimaiScore.Value.InnerSensorPositionY (button));
						break;
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SENSOR_DEGREE_DISTANCE_CLOCKWISE:
		case MaisqScore.CommandNameDocument.SENSOR_DEGREE_DISTANCE_COUNTERCLOCKWISE:
		case MaisqScore.CommandNameDocument.SENSOR_DEGREE_DISTANCE_RIGHT:
		case MaisqScore.CommandNameDocument.SENSOR_DEGREE_DISTANCE_LEFT:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children [0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children [1]);
				if (button1 != null && button2 != null) {
					switch (switchKey) {
					case MaisqScore.CommandNameDocument.SENSOR_DEGREE_DISTANCE_CLOCKWISE:
						instance = signed (MaimaiScore.Value.SensorDegreeDistanceClockwise (button1, button2));
						break;
					case MaisqScore.CommandNameDocument.SENSOR_DEGREE_DISTANCE_COUNTERCLOCKWISE:
						instance = signed (MaimaiScore.Value.SensorDegreeDistanceCounterClockwise (button1, button2));
						break;
					case MaisqScore.CommandNameDocument.SENSOR_DEGREE_DISTANCE_RIGHT:
						instance = signed (MaimaiScore.Value.SensorDegreeDistanceRight (button1, button2));
						break;
					case MaisqScore.CommandNameDocument.SENSOR_DEGREE_DISTANCE_LEFT:
						instance = signed (MaimaiScore.Value.SensorDegreeDistanceLeft (button1, button2));
						break;
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CENTER_SENSOR_POSITION_X:
		case MaisqScore.CommandNameDocument.CENTER_SENSOR_POSITION_Y:
		case MaisqScore.CommandNameDocument.OUTER_SENSOR_RADIUS:
		case MaisqScore.CommandNameDocument.INNER_SENSOR_RADIUS:
		case MaisqScore.CommandNameDocument.CENTER_SENSOR_RADIUS:
		{
			if (tree.children.Count == 0) {
				switch (switchKey) {
				case MaisqScore.CommandNameDocument.CENTER_SENSOR_POSITION_X:
					instance = signed (MaimaiScore.Value.CenterSensorPositionX ());
					break;
				case MaisqScore.CommandNameDocument.CENTER_SENSOR_POSITION_Y:
					instance = signed (MaimaiScore.Value.CenterSensorPositionY ());
					break;
				case MaisqScore.CommandNameDocument.OUTER_SENSOR_RADIUS:
					instance = signed (MaimaiScore.Value.OuterSensorRadius ());
					break;
				case MaisqScore.CommandNameDocument.INNER_SENSOR_RADIUS:
					instance = signed (MaimaiScore.Value.InnerSensorRadius ());
					break;
				case MaisqScore.CommandNameDocument.CENTER_SENSOR_RADIUS:
					instance = signed (MaimaiScore.Value.CenterSensorRadius ());
					break;
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CALL:
		{
			var called = ReadCallCommand<MaimaiScore.Value> (tree, ReadCreateCommandTypeOfValue);
			if (called.Length > 0)
				instance = called[0];
			break;
		}
		default:
		{
			if (tree.children.Count == 0) {
				instance = MaimaiScore.Value.FromMaiScript (tree.data);
				break;
			}
			break;
		}
		}
		if (instance != null) {
			instance = ReadExtendCommandTypeOfValue (instance, tree.extend);
			ReadOptionTypeOfValue (instance, tree);
			return instance;
		}
		MaisqScriptReadErrorCommon ("value", tree);
		return null;
	}
	
	public MaimaiScore.Value ReadExtendCommandTypeOfValue (MaimaiScore.Value instance, MaisqScriptTree tree) {
		if (instance == null || tree == null || tree.data.Length == 0) return instance;
		
		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.MATH_ADDITION:
		case MaisqScore.CommandNameDocument.MATH_SUBTRACTION:
		case MaisqScore.CommandNameDocument.MATH_MULTIPLICATION:
		case MaisqScore.CommandNameDocument.MATH_DIVISION:
		case MaisqScore.CommandNameDocument.MATH_POWER:
		{
			if (tree.children.Count == 1) {
				var value1 = ReadCreateCommandTypeOfValue (tree.children [0]);
				if (value1 != null) {
					switch (switchKey) {
					case MaisqScore.CommandNameDocument.MATH_ADDITION:
						instance = instance.MathAddition (value1);
						break;
					case MaisqScore.CommandNameDocument.MATH_SUBTRACTION:
						instance = instance.MathSubtraction (value1);
						break;
					case MaisqScore.CommandNameDocument.MATH_MULTIPLICATION:
						instance = instance.MathMultiplication (value1);
						break;
					case MaisqScore.CommandNameDocument.MATH_DIVISION:
						instance = instance.MathDivision (value1);
						break;
					case MaisqScore.CommandNameDocument.MATH_POWER:
						instance = instance.MathPower (value1);
						break;
					}
				}
			}
			break;
		}
		}
		instance = ReadExtendCommandTypeOfValue (instance, tree.extend);
		ReadOptionTypeOfValue (instance, tree);
		return instance;
	}
	
	public void ReadOptionTypeOfValue (MaimaiScore.Value instance, MaisqScriptTree tree) {
		
	}
	
	#endregion
	#region Flag
	/// <summary>
	/// maiScriptを呼んでフラグを返す.
	/// </summary>
	public MaimaiScore.Flag ReadCreateCommandTypeOfFlag (MaisqScriptTree tree) {
		if (tree == null || tree.data.Length == 0) return null;

		MaimaiScore.Flag instance = null;
		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.CALL:
		{
			var called = ReadCallCommand<MaimaiScore.Flag> (tree, ReadCreateCommandTypeOfFlag);
			if (called.Length > 0)
				instance = called[0];
			break;
		}
		default:
		{
			if (tree.children.Count == 0) {
				instance = MaimaiScore.Flag.FromMaiScript (tree.data);
			}
			break;
		}
		}
		if (instance != null) {
			instance = ReadExtendCommandTypeOfFlag (instance, tree.extend);
			ReadOptionTypeOfFlag (instance, tree);
			return instance;
		}
		MaisqScriptReadErrorCommon ("flag", tree);
		return null;
	}
	
	public MaimaiScore.Flag ReadExtendCommandTypeOfFlag (MaimaiScore.Flag instance, MaisqScriptTree tree) {
		if (instance == null || tree == null || tree.data.Length == 0) return instance;
		
		instance = ReadExtendCommandTypeOfFlag (instance, tree.extend);
		ReadOptionTypeOfFlag (instance, tree);
		return instance;
	}
	
	public void ReadOptionTypeOfFlag (MaimaiScore.Flag instance, MaisqScriptTree tree) {
		
	}
	
	#endregion
	#region Position
	/// <summary>
	/// maiScriptを呼んで位置を返す.
	/// </summary>
	public MaimaiScore.Position ReadCreateCommandTypeOfPosition (MaisqScriptTree tree) {
		if (tree == null || tree.data.Length == 0) return null;

		MaimaiScore.Position instance = null;
		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.POSITION:
		{
			if (tree.children.Count == 2) {
				instance = MaimaiScore.Position.FromMaiScript (ReadCreateCommandTypeOfValue (tree.children[0]), ReadCreateCommandTypeOfValue (tree.children[1]));
			}
			break;
		}
		case MaisqScore.CommandNameDocument.TURN:
		{
			if (tree.children.Count == 2) {
				var pos = ReadCreateCommandTypeOfPosition (tree.children[0]);
				if (pos != null) {
					var amount = ReadCreateCommandTypeOfNumber (tree.children [1]);
					if (amount != null) {
						instance = MaimaiScore.Position.Turn (pos, amount);
						break;
					}
					else {
						var degree = ReadCreateCommandTypeOfValue (tree.children [1]);
						instance = MaimaiScore.Position.Turn (pos, degree);
						break;
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
		case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
		{
			if (tree.children.Count == 1) {
				var pos = ReadCreateCommandTypeOfPosition (tree.children[0]);
				if (pos != null) {
					switch (switchKey) {
					case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
						instance = MaimaiScore.Position.MirrorHolizontal (pos);
						break;
					case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
						instance = MaimaiScore.Position.MirrorVertical (pos);
						break;
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.ANGLE:
		{
			if (tree.children.Count == 2) {
				var value1 = ReadCreateCommandTypeOfValue (tree.children[0]);
				var value2 = ReadCreateCommandTypeOfValue (tree.children[1]);
				if (value1 != null && value2 != null) {
					instance = MaimaiScore.Position.Angle (value1, value2);
				}
			}
			else if (tree.children.Count == 1) {
				var value1 = ReadCreateCommandTypeOfValue (tree.children[0]);
				if (value1 != null) {
					instance = MaimaiScore.Position.Angle (value1);
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.OUTER_SENSOR_POSITION:
		{
			if (tree.children.Count == 1) {
				var value1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				if (value1 != null) {
					instance = MaimaiScore.Position.OuterSensorPosition (value1);
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.INNER_SENSOR_POSITION:
		{
			if (tree.children.Count == 1) {
				var value1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				if (value1 != null) {
					instance = MaimaiScore.Position.InnerSensorPosition (value1);
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CENTER_SENSOR_POSITION:
		{
			if (tree.children.Count == 0) {
				instance = MaimaiScore.Position.CenterSensorPosition ();
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CALL:
		{
			var called = ReadCallCommand<MaimaiScore.Position> (tree, ReadCreateCommandTypeOfPosition);
			if (called.Length > 0)
				instance = called[0];
			break;
		}
		}
		if (instance != null) {
			instance = ReadExtendCommandTypeOfPosition (instance, tree.extend);
			ReadOptionTypeOfPosition (instance, tree);
			return instance;
		}
		MaisqScriptReadErrorCommon ("position", tree);
		return null;
	}
	
	public MaimaiScore.Position ReadExtendCommandTypeOfPosition (MaimaiScore.Position instance, MaisqScriptTree tree) {
		if (instance == null || tree == null || tree.data.Length == 0) return instance;

		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.TURN:
		{
			if (tree.children.Count == 1) {
				var amount = ReadCreateCommandTypeOfNumber (tree.children [0]);
				if (amount != null) {
					instance.Turn (amount.GetValue());
				}
				else {
					var degree = ReadCreateCommandTypeOfValue (tree.children [0]);
					if (degree != null) {
						instance.Turn (degree.GetValue());
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
		case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
		{
			if (tree.children.Count == 0) {
				switch (switchKey) {
				case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
					instance.MirrorHolizontal ();
					break;
				case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
					instance.MirrorVertical ();
					break;
				}
			}
			break;
		}
		}
		instance = ReadExtendCommandTypeOfPosition (instance, tree.extend);
		ReadOptionTypeOfPosition (instance, tree);
		return instance;
	}
	
	public void ReadOptionTypeOfPosition (MaimaiScore.Position instance, MaisqScriptTree tree) {
		
	}
	
	#endregion
	#region Color
	/// <summary>
	/// maiScriptを呼んで整数を返す.
	/// </summary>
	public MaimaiScore.Color ReadCreateCommandTypeOfColor (MaisqScriptTree tree) {
		if (tree == null || tree.data.Length == 0) return null;

		MaimaiScore.Color instance = null;
		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.RGBA:
		{
			if (tree.children.Count == 4) {
				var value1 = ReadCreateCommandTypeOfNumber (tree.children [0]);
				var value2 = ReadCreateCommandTypeOfNumber (tree.children [1]);
				var value3 = ReadCreateCommandTypeOfNumber (tree.children [2]);
				var value4 = ReadCreateCommandTypeOfNumber (tree.children [3]);
				if (value1 != null && value2 != null && value3 != null && value4 != null) {
					instance = MaimaiScore.Color.FromMaiScriptRGBA (value1, value2, value3, value4);
				}
				else {
					var value1i = ReadCreateCommandTypeOfValue (tree.children [0]);
					var value2i = ReadCreateCommandTypeOfValue (tree.children [1]);
					var value3i = ReadCreateCommandTypeOfValue (tree.children [2]);
					var value4i = ReadCreateCommandTypeOfValue (tree.children [3]);
					if (value1i != null && value2i != null && value3i != null && value4i != null) {
						instance = MaimaiScore.Color.FromMaiScriptRGBA (value1i, value2i, value3i, value4i);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.ARGB:
		{
			if (tree.children.Count == 4) {
				var value1 = ReadCreateCommandTypeOfNumber (tree.children [0]);
				var value2 = ReadCreateCommandTypeOfNumber (tree.children [1]);
				var value3 = ReadCreateCommandTypeOfNumber (tree.children [2]);
				var value4 = ReadCreateCommandTypeOfNumber (tree.children [3]);
				if (value1 != null && value2 != null && value3 != null && value4 != null) {
					instance = MaimaiScore.Color.FromMaiScriptARGB (value1, value2, value3, value4);
				}
				else {
					var value1i = ReadCreateCommandTypeOfValue (tree.children [0]);
					var value2i = ReadCreateCommandTypeOfValue (tree.children [1]);
					var value3i = ReadCreateCommandTypeOfValue (tree.children [2]);
					var value4i = ReadCreateCommandTypeOfValue (tree.children [3]);
					if (value1i != null && value2i != null && value3i != null && value4i != null) {
						instance = MaimaiScore.Color.FromMaiScriptARGB (value1i, value2i, value3i, value4i);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.RGB:
		{
			if (tree.children.Count == 3) {
				var value1 = ReadCreateCommandTypeOfNumber (tree.children [0]);
				var value2 = ReadCreateCommandTypeOfNumber (tree.children [1]);
				var value3 = ReadCreateCommandTypeOfNumber (tree.children [2]);
				if (value1 != null && value2 != null && value3 != null) {
					instance = MaimaiScore.Color.FromMaiScriptRGB (value1, value2, value3);
				}
				else {
					var value1i = ReadCreateCommandTypeOfValue (tree.children [0]);
					var value2i = ReadCreateCommandTypeOfValue (tree.children [1]);
					var value3i = ReadCreateCommandTypeOfValue (tree.children [2]);
					if (value1i != null && value2i != null && value3i != null) {
						instance = MaimaiScore.Color.FromMaiScriptRGB (value1i, value2i, value3i);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.BLACK:
		{
			if (tree.children.Count == 0) {
				instance = MaimaiScore.Color.CreateRGB (0,0,0);
			}
			break;
		}
		case MaisqScore.CommandNameDocument.WHITE:
		{
			if (tree.children.Count == 0) {
				instance = MaimaiScore.Color.CreateRGB (255,255,255);
			}
			break;
		}
		case MaisqScore.CommandNameDocument.GRAY:
		{
			if (tree.children.Count == 0) {
				instance = MaimaiScore.Color.CreateRGB (128,128,128);
			}
			break;
		}
		case MaisqScore.CommandNameDocument.RED:
		{
			if (tree.children.Count == 0) {
				instance = MaimaiScore.Color.CreateRGB (255,0,0);
			}
			break;
		}
		case MaisqScore.CommandNameDocument.GREEN:
		{
			if (tree.children.Count == 0) {
				instance = MaimaiScore.Color.CreateRGB (0,255,0);
			}
			break;
		}
		case MaisqScore.CommandNameDocument.BLUE:
		{
			if (tree.children.Count == 0) {
				instance = MaimaiScore.Color.CreateRGB (0,0,255);
			}
			break;
		}
		case MaisqScore.CommandNameDocument.YELLOW:
		{
			if (tree.children.Count == 0) {
				instance = MaimaiScore.Color.CreateRGB (255,255,0);
			}
			break;
		}
		case MaisqScore.CommandNameDocument.MAGENTA:
		{
			if (tree.children.Count == 0) {
				instance = MaimaiScore.Color.CreateRGB (255,0,255);
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CYAN:
		{
			if (tree.children.Count == 0) {
				instance = MaimaiScore.Color.CreateRGB (0,255,255);
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CALL:
		{
			var called = ReadCallCommand<MaimaiScore.Color> (tree, ReadCreateCommandTypeOfColor);
			if (called.Length > 0)
				instance = called[0];
			break;
		}
		}
		if (instance != null) {
			instance = ReadExtendCommandTypeOfColor (instance, tree.extend);
			ReadOptionTypeOfColor (instance, tree);
			return instance;
		}
		MaisqScriptReadErrorCommon ("color", tree);
		return null;
	}
	
	public MaimaiScore.Color ReadExtendCommandTypeOfColor (MaimaiScore.Color instance, MaisqScriptTree tree) {
		return instance;
	}
	
	public void ReadOptionTypeOfColor (MaimaiScore.Color instance, MaisqScriptTree tree) {
		
	}
	
	#endregion
	#region Button
	/// <summary>
	/// maiScriptを呼んでボタンを返す.
	/// </summary>
	public MaimaiScore.Button ReadCreateCommandTypeOfButton (MaisqScriptTree tree) {
		if (tree == null || tree.data.Length == 0) return null;

		MaimaiScore.Button instance = null;
		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.TURN:
		{
			if (tree.children.Count == 2) {
				var button = ReadCreateCommandTypeOfButton (tree.children [0]);
				var amount = ReadCreateCommandTypeOfNumber (tree.children [1]);
				if (button != null && amount != null) {
					instance = MaimaiScore.Button.Turn (button, amount);
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
		case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
		{
			if (tree.children.Count == 1) {
				var button = ReadCreateCommandTypeOfButton (tree.children [0]);
				if (button != null) {
					switch (switchKey) {
					case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
						instance = MaimaiScore.Button.MirrorHolizontal (button);
						break;
					case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
						instance = MaimaiScore.Button.MirrorVertical (button);
						break;
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CALL:
		{
			var called = ReadCallCommand<MaimaiScore.Button> (tree, ReadCreateCommandTypeOfButton);
			if (called.Length > 0)
				instance = called[0];
			break;
		}
		default:
		{
			var number = ReadCreateCommandTypeOfNumber (tree);
			if (number != null) {
				instance = MaimaiScore.Button.FromMaiScript (number);
			}
			break;
		}
		}
		if (instance != null) {
			instance = ReadExtendCommandTypeOfButton (instance, tree.extend);
			ReadOptionTypeOfButton (instance, tree);
			return instance;
		}
		MaisqScriptReadErrorCommon ("button", tree);
		return null;
	}
	
	public MaimaiScore.Button ReadExtendCommandTypeOfButton (MaimaiScore.Button instance, MaisqScriptTree tree) {
		if (instance == null || tree == null || tree.data.Length == 0) return instance;
		
		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.TURN:
		{
			if (tree.children.Count == 1) {
				var amount = ReadCreateCommandTypeOfNumber (tree.children [0]);
				if (amount != null) {
					instance.Turn (amount.GetValue());
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
		case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
		{
			if (tree.children.Count == 0) {
				switch (switchKey) {
				case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
					instance.MirrorHolizontal ();
					break;
				case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
					instance.MirrorVertical ();
					break;
				}
			}
			break;
		}
		}
		instance = ReadExtendCommandTypeOfButton (instance, tree.extend);
		ReadOptionTypeOfButton (instance, tree);
		return instance;
	}
	
	public void ReadOptionTypeOfButton (MaimaiScore.Button instance, MaisqScriptTree tree) {
		
	}
	
	
	#endregion
	#region INote
	/// <summary>
	/// maiScriptを呼んでノートを返す.
	/// </summary>
	public MaimaiScore.INote ReadCreateCommandTypeOfNote (MaisqScriptTree tree) {
		if (tree == null || tree.data.Length == 0) return null;

		MaimaiScore.INote instance = null;
		var switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.TAP:
		{
			if (tree.children.Count == 1) {
				var button = ReadCreateCommandTypeOfButton (tree.children [0]);
				if (button != null) {
					instance = MaimaiScore.NoteTap.FromMaiScript (button);
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.HOLD:
		{
			if (tree.children.Count == 2) {
				var button = ReadCreateCommandTypeOfButton (tree.children [0]);
				var step = ReadCreateCommandTypeOfStep (tree.children [1]);
				if (button != null && step != null) {
					instance = MaimaiScore.NoteHold.FromMaiScript (button, step);
				}
				else return null;
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SLIDE:
		{
			if (tree.children.Count > 1) {
				int index = 0;
				var head = ReadCreateCommandTypeOfSlideHead (tree.children [0]);
				if (head != null) {
					index++;
				}
				var wait = ReadCreateCommandTypeOfSlideHeadCustomWait (tree.children[index]);
				if (wait != null) {
					index++;
				}
				var step = ReadCreateCommandTypeOfStep (tree.children [index]);
				if (step != null) {
					index++;
				}
				bool needWait = false;
				bool needStep = false;
				var patterns = new List<MaimaiScore.ISlidePattern> ();
				for (int i = index; i < tree.children.Count; i++) {
					var pattern = ReadCreateCommandTypeOfSlidePattern (tree.children [i], head);
					if (pattern != null && pattern.Length > 0) {
						foreach (var p in pattern) {
							if (p.GetWait () == null) needWait = true;
							if (p.GetStep () == null) needStep = true;
							patterns.Add (p);
						}
					}
				}
				if (patterns != null && patterns.Count > 0) {
					if (needWait && wait == null) {}
					else if (!needWait && wait != null) wait = null;
					if (needStep && step == null) return null;
					else if (!needStep && step != null) step = null;
					instance = MaimaiScore.NoteSlide.FromMaiScript (head, wait, step, patterns.ToArray ());
				}
				else return null; // patternやchainやcommandのエラーメッセージに委託する
			}
			break;
		}
		case MaisqScore.CommandNameDocument.BREAK:
		{
			if (tree.children.Count == 1) {
				var button = ReadCreateCommandTypeOfButton (tree.children [0]);
				if (button != null) {
					instance = MaimaiScore.NoteBreak.FromMaiScript (button);
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.MESSAGE:
		{
			if (tree.children.Count == 5) {
				var message = tree.children [0].data;
				var position = ReadCreateCommandTypeOfPosition (tree.children [1]);
				var scale = ReadCreateCommandTypeOfValue (tree.children [2]);
				var color = ReadCreateCommandTypeOfColor (tree.children [3]);
				var step = ReadCreateCommandTypeOfStep (tree.children [4]);
				if (message != null && position != null && scale != null && color != null && step != null) {
					instance = MaimaiScore.NoteMessage.FromMaiScript (message, position, scale, color, step);
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SCROLL_MESSAGE:
		{
			if (tree.children.Count == 5) {
				var message = tree.children [0].data;
				var y = ReadCreateCommandTypeOfValue (tree.children [1]);
				var scale = ReadCreateCommandTypeOfValue (tree.children [2]);
				var color = ReadCreateCommandTypeOfColor (tree.children [3]);
				var step = ReadCreateCommandTypeOfStep (tree.children [4]);
				if (message != null && y != null && scale != null && color != null && step != null) {
					instance = MaimaiScore.NoteScrollMessage.FromMaiScript (message, y, scale, color, step);
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SOUND_MESSAGE:
		{
			if (tree.children.Count == 1) {
				var soundId = tree.children [0].data;
				if (soundId != null) {
					instance = MaimaiScore.NoteSoundMessage.FromMaiScript (soundId);
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.TURN:
		{
			if (tree.children.Count == 2) {
				var note = ReadCreateCommandTypeOfNote (tree.children[0]);
				var amount = ReadCreateCommandTypeOfNumber (tree.children [1]);
				if (note != null && amount != null) {
					var ret = note.Clone ();
					ret.Turn (amount.GetValue ());
					instance = ret;
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
		case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
		{
			if (tree.children.Count == 1) {
				var note = ReadCreateCommandTypeOfNote (tree.children[0]);
				if (note != null) {
					switch (switchKey) {
					case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
					{
						var ret = note.Clone ();
						ret.MirrorHolizontal ();
						instance = ret;
						break;
					}
					case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
					{
						var ret = note.Clone ();
						ret.MirrorVertical ();
						instance = ret;
						break;
					}
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CALL:
		{
			var called = ReadCallCommand<MaimaiScore.INote> (tree, ReadCreateCommandTypeOfNote);
			if (called.Length > 0)
				instance = called[0];
			break;
		}
		}
		if (instance != null) {
			instance = ReadExtendCommandTypeOfNote (instance, tree.extend);
			ReadOptionTypeOfNote (instance, tree);
			return instance;
		}
		MaisqScriptReadErrorCommon ("note", tree);
		return null;
	}
	
	public MaimaiScore.INote ReadExtendCommandTypeOfNote (MaimaiScore.INote instance, MaisqScriptTree tree) {
		if (instance == null || tree == null || tree.data.Length == 0) return instance;
		
		var switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.TURN:
		{
			if (tree.children.Count == 1) {
				var amount = ReadCreateCommandTypeOfNumber (tree.children [0]);
				if (amount != null) {
					instance.Turn (amount.GetValue ());
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
		case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
		{
			if (tree.children.Count == 0) {
				switch (switchKey) {
				case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
				{
					instance.MirrorHolizontal ();
					break;
				}
				case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
				{
					instance.MirrorVertical ();
					break;
				}
				}
			}
			break;
		}
		}
		instance = ReadExtendCommandTypeOfNote (instance, tree.extend);
		ReadOptionTypeOfNote (instance, tree);
		return instance;
	}
	
	public void ReadOptionTypeOfNote (MaimaiScore.INote instance, MaisqScriptTree tree) {
		if (instance != null) {
			// optionはDictionaryでありMaiScriptTreeではないからReadCreateCommand系ではなくFromMaiScriptを堂々と使う.
			MaimaiScore.Flag secret = null;
			if (tree.option.ContainsKey (MaisqScore.CommandNameDocument.SECRET)) {
				secret = MaimaiScore.Flag.FromMaiScript (tree.option [MaisqScore.CommandNameDocument.SECRET]);
			}
			instance.option = MaimaiScore.NoteOption.FromMaiScript (instance.option, secret);
		}
	}
	
	#endregion
	#region IStep
	/// <summary>
	/// maiScriptを呼んで時間を返す.
	/// </summary>
	public MaimaiScore.IStep ReadCreateCommandTypeOfStep (MaisqScriptTree tree) {
		if (tree == null || tree.data.Length == 0) return null;

		MaimaiScore.IStep instance = null;
		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.STEP:
		{
			if (tree.children.Count == 2) {
				var beat = ReadCreateCommandTypeOfValue (tree.children [0]);
				var length = ReadCreateCommandTypeOfValue (tree.children [1]);
				if (beat != null && length != null) {
					instance = MaimaiScore.StepLength.FromMaiScript (beat, length);
				}
			}
			else if (tree.children.Count == 3) {
				var localBpm = ReadCreateCommandTypeOfValue (tree.children [0]);
				var beat = ReadCreateCommandTypeOfValue (tree.children [1]);
				var length = ReadCreateCommandTypeOfValue (tree.children [2]);
				if (localBpm != null && beat != null && length != null) {
					instance = MaimaiScore.StepLocalBpm.FromMaiScript (localBpm, beat, length);
				}
			}
			else if (tree.children.Count == 1) {
				var interval = ReadCreateCommandTypeOfValue (tree.children [0]);
				if (interval != null) {
					instance = MaimaiScore.StepInterval.FromMaiScript (interval);
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CALL:
		{
			var called = ReadCallCommand<MaimaiScore.IStep> (tree, ReadCreateCommandTypeOfStep);
			if (called.Length > 0)
				instance = called[0];
			break;
		}
		}
		if (instance != null) {
			instance = ReadExtendCommandTypeOfStep (instance, tree.extend);
			ReadOptionTypeOfStep (instance, tree);
			return instance;
		}
		if (ThroughErrorCommand(switchKey)) return null; // nullableだけどログがうっとうしいのでここで返す.
		MaisqScriptReadErrorCommon ("step", tree);
		return null;
	}
	
	public MaimaiScore.IStep ReadExtendCommandTypeOfStep (MaimaiScore.IStep instance, MaisqScriptTree tree) {
		if (instance == null || tree == null || tree.data.Length == 0) return instance;
		
		instance = ReadExtendCommandTypeOfStep (instance, tree.extend);
		ReadOptionTypeOfStep (instance, tree);
		return instance;
	}
	
	public void ReadOptionTypeOfStep (MaimaiScore.IStep instance, MaisqScriptTree tree) {
		
	}
	
	#endregion
	#region ISlideHead
	/// <summary>
	/// maiScriptを呼んでスターを返す.
	/// </summary>
	public MaimaiScore.ISlideHead ReadCreateCommandTypeOfSlideHead (MaisqScriptTree tree) {
		if (tree == null || tree.data.Length == 0) return null;

		MaimaiScore.ISlideHead instance = null;
		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.TURN:
		{
			if (tree.children.Count == 2) {
				var head = ReadCreateCommandTypeOfSlideHead (tree.children[0]);
				var amount = ReadCreateCommandTypeOfNumber (tree.children [1]);
				if (head != null && amount != null) {
					var ret = head.Clone ();
					ret.Turn (amount.GetValue ());
					instance = ret;
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
		case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
		{
			if (tree.children.Count == 1) {
				var head = ReadCreateCommandTypeOfSlideHead (tree.children[0]);
				if (head != null) {
					switch (switchKey) {
					case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
					{
						var ret = head.Clone ();
						ret.MirrorHolizontal ();
						instance = ret;
						break;
					}
					case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
					{
						var ret = head.Clone ();
						ret.MirrorVertical ();
						instance = ret;
						break;
					}
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.STAR:
		case MaisqScore.CommandNameDocument.BREAK_STAR:
		{
			if (tree.children.Count == 1) {
				var button = ReadCreateCommandTypeOfButton (tree.children[0]);
				if (button != null) {
					switch (switchKey) {
					case MaisqScore.CommandNameDocument.STAR:
						instance = MaimaiScore.SlideHeadStar.FromMaiScript (button);
						break;
					case MaisqScore.CommandNameDocument.BREAK_STAR:
						instance = MaimaiScore.SlideHeadBreakStar.FromMaiScript (button);
						break;
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.NOTHING:
		case MaisqScore.CommandNameDocument.NULL:
		{
			if (tree.children.Count == 0) {
				instance = MaimaiScore.SlideHeadNothing.FromMaiScript ();
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CALL:
		{
			var called = ReadCallCommand<MaimaiScore.ISlideHead> (tree, ReadCreateCommandTypeOfSlideHead);
			if (called.Length > 0)
				instance = called[0];
			break;
		}
		}
		if (instance != null) {
			instance = ReadExtendCommandTypeOfSlideHead (instance, tree.extend);
			ReadOptionTypeOfSlideHead (instance, tree);
			return instance;
		}
		if (ThroughErrorCommand(switchKey)) return null; // nullableだけどログがうっとうしいのでここで返す.
		MaisqScriptReadErrorCommon ("slide head", tree);
		return null;
	}
	
	public MaimaiScore.ISlideHead ReadExtendCommandTypeOfSlideHead (MaimaiScore.ISlideHead instance, MaisqScriptTree tree) {
		if (instance == null || tree == null || tree.data.Length == 0) return instance;
		
		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.TURN:
		{
			if (tree.children.Count == 1) {
				var amount = ReadCreateCommandTypeOfNumber (tree.children [0]);
				if (amount != null) {
					instance.Turn (amount.GetValue ());
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
		case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
		{
			if (tree.children.Count == 0) {
				switch (switchKey) {
				case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
				{
					instance.MirrorHolizontal ();
					break;
				}
				case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
				{
					instance.MirrorVertical ();
					break;
				}
				}
			}
			break;
		}
		}
		instance = ReadExtendCommandTypeOfSlideHead (instance, tree.extend);
		ReadOptionTypeOfSlideHead (instance, tree);
		return instance;
	}
	
	public void ReadOptionTypeOfSlideHead (MaimaiScore.ISlideHead instance, MaisqScriptTree tree) {
		
	}
	
	#endregion
	#region ISlideHeadWait
	/// <summary>
	/// maiScriptを呼んでカスタマイズされたスライドの頭の待ち時間を返す.
	/// </summary>
	public MaimaiScore.ISlideHeadCustomWait ReadCreateCommandTypeOfSlideHeadCustomWait (MaisqScriptTree tree) {
		if (tree == null || tree.data.Length == 0) return null;

		MaimaiScore.ISlideHeadCustomWait instance = null;
		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.WAIT_CUSTOM_LOCAL_BPM:
		case MaisqScore.CommandNameDocument.WAIT_CUSTOM_INTERVAL:
		{
			if (tree.children.Count == 1) {
				var value = ReadCreateCommandTypeOfValue (tree.children[0]);
				if (value != null) {
					switch (switchKey) {
					case MaisqScore.CommandNameDocument.WAIT_CUSTOM_LOCAL_BPM:
						instance = MaimaiScore.SlideHeadCustomWaitLocalBpm.FromMaiScript (value);
						break;
					case MaisqScore.CommandNameDocument.WAIT_CUSTOM_INTERVAL:
						instance = MaimaiScore.SlideHeadCustomWaitInterval.FromMaiScript (value);
						break;
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.NULL:
		case MaisqScore.CommandNameDocument.WAIT_DEFAULT:
		{
			if (tree.children.Count == 0) {
				instance = MaimaiScore.SlideHeadCustomWaitDefault.FromMaiScript ();
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CALL:
		{
			var called = ReadCallCommand<MaimaiScore.ISlideHeadCustomWait> (tree, ReadCreateCommandTypeOfSlideHeadCustomWait);
			if (called.Length > 0)
				instance = called[0];
			break;
		}
		}
		if (instance != null) {
			instance = ReadExtendCommandTypeOfSlideHeadCustomWait (instance, tree.extend);
			ReadOptionTypeOfSlideHeadCustomWait (instance, tree);
			return instance;
		}
		if (ThroughErrorCommand(switchKey)) return null; // nullableだけどログがうっとうしいのでここで返す.
		MaisqScriptReadErrorCommon ("slide head custom wait", tree);
		return null;
	}
	
	public MaimaiScore.ISlideHeadCustomWait ReadExtendCommandTypeOfSlideHeadCustomWait (MaimaiScore.ISlideHeadCustomWait instance, MaisqScriptTree tree) {
		if (instance == null || tree == null || tree.data.Length == 0) return instance;
		
		instance = ReadExtendCommandTypeOfSlideHeadCustomWait (instance, tree.extend);
		ReadOptionTypeOfSlideHeadCustomWait (instance, tree);
		return instance;
	}
	
	public void ReadOptionTypeOfSlideHeadCustomWait (MaimaiScore.ISlideHeadCustomWait instance, MaisqScriptTree tree) {
		
	}
	
	#endregion
	#region ISlidePattern
	/// <summary>
	/// maiScriptを呼んでスライドパターンを返す.
	/// </summary>
	public MaimaiScore.ISlidePattern[] ReadCreateCommandTypeOfSlidePattern (MaisqScriptTree tree, MaimaiScore.ISlideHead head) {
		if (tree == null || tree.data.Length == 0) return null;

		var instances = new List<MaimaiScore.ISlidePattern>();
		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.TURN:
		{
			if (tree.children.Count == 2) {
				var pattern = ReadCreateCommandTypeOfSlidePattern (tree.children[0], head);
				if (pattern != null) {
					var amount = ReadCreateCommandTypeOfNumber (tree.children [1]);
					if (amount != null) {
						foreach (var target in pattern) {
							var ret = target.Clone();
							ret.Turn (amount.GetValue ());
							instances.Add(ret);
						}
					}
					else {
						var degree = ReadCreateCommandTypeOfValue (tree.children [1]);
						if (degree != null) {
							foreach (var target in pattern) {
								var ret = target.Clone();
								ret.Turn (degree.GetValue ());
								instances.Add(ret);
							}
						}
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
		case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
		{
			if (tree.children.Count == 1) {
				var pattern = ReadCreateCommandTypeOfSlidePattern (tree.children[0], head);
				if (pattern != null) {
					switch (switchKey) {
					case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
					{
						foreach (var target in pattern) {
							var ret = target.Clone();
							ret.MirrorHolizontal ();
							instances.Add(ret);
						}
						break;
					}
					case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
					{
						foreach (var target in pattern) {
							var ret = target.Clone();
							ret.MirrorVertical ();
							instances.Add(ret);
						}
						break;
					}
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.PATTERN:
		{
			if (tree.children.Count > 0) {
				int index = 0;
				var wait = ReadCreateCommandTypeOfSlideHeadCustomWait (tree.children[index]);
				if (wait != null) {
					index++;
				}
				var step = ReadCreateCommandTypeOfStep (tree.children [index]);
				if (step != null) {
					index++;
				}
				bool needWait = false;
				bool needStep = false;
				var chains = new List<MaimaiScore.ISlideChain>();
				for (int i = index; i < tree.children.Count; i++) {
					var patternData = tree.children[i];
					var chain = ReadCreateCommandTypeOfSlideChain (patternData, head);
					if (chain != null && chain.Length > 0) {
						foreach (var c in chain) {
							if (c.GetWait () == null) needWait = true;
							if (c.GetStep () == null) needStep = true;
							chains.Add (c);
						}
					}
				}
				if (!needWait) wait = null;
				if (!needStep) step = null;
				var instance = MaimaiScore.SlidePattern.FromMaiScript (wait, step, chains.ToArray());
				if (instance == null) {
					return null; // chainやcommandのエラーメッセージに委託する.
				}
				else {
					instances.Add (instance);
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CALL:
		{
			if (tree.children.Count >= 1) {
				if (userDefine.ContainsKey(tree.children[0].data)) {
					var replaced = userDefine[tree.children[0].data].Call (MaisqScriptUserDefine.CreateCallMethodParamsFromMaiScriptTree(tree));
					var calledTrees = MaisqScriptTree.CreateTreeFromMaiScript(replaced);
					foreach (var calledTree in calledTrees) {
						var instance = ReadCreateCommandTypeOfSlidePattern (calledTree, head);
						if (instance != null && instance.Length > 0) {
							instances.AddRange (instance);
						}
					}
				}
				else {
					MaisqScriptReadErrorCommon ("define", tree);
				}
			}
			break;
		}
		}
		if (instances != null) {
			var ret = instances.ToArray ();
			ret = ReadExtendCommandTypeOfSlidePattern (ret, tree.extend);
			ReadOptionTypeOfSlidePattern (ret, tree);
			return ret;
		}
		MaisqScriptReadErrorCommon ("slide pattern", tree);
		return null;
	}
	
	public MaimaiScore.ISlidePattern[] ReadExtendCommandTypeOfSlidePattern (MaimaiScore.ISlidePattern[] instances, MaisqScriptTree tree) {
		if (instances == null || tree == null || tree.data.Length == 0) return instances;
		
		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.TURN:
		{
			if (tree.children.Count == 1) {
				var amount = ReadCreateCommandTypeOfNumber (tree.children [0]);
				if (amount != null) {
					foreach (var instance in instances) {
						instance.Turn (amount.GetValue ());
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
		case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
		{
			if (tree.children.Count == 0) {
				switch (switchKey) {
				case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
				{
					foreach (var instance in instances) {
						instance.MirrorHolizontal ();
					}
					break;
				}
				case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
				{
					foreach (var instance in instances) {
						instance.MirrorVertical ();
					}
					break;
				}
				}
			}
			break;
		}
		}
		instances = ReadExtendCommandTypeOfSlidePattern (instances, tree.extend);
		ReadOptionTypeOfSlidePattern (instances, tree);
		return instances;
	}
	
	public void ReadOptionTypeOfSlidePattern (MaimaiScore.ISlidePattern[] instance, MaisqScriptTree tree) {
		
	}
	
	#endregion
	#region ISlideChain
	/// <summary>
	/// maiScriptを呼んでスライドチェインを返す.
	/// </summary>
	public MaimaiScore.ISlideChain[] ReadCreateCommandTypeOfSlideChain (MaisqScriptTree tree, MaimaiScore.ISlideHead head) {
		if (tree == null || tree.data.Length == 0) return null;

		var instances = new List<MaimaiScore.ISlideChain>();
		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.TURN:
		{
			if (tree.children.Count == 2) {
				var chain = ReadCreateCommandTypeOfSlideChain (tree.children[0], head);
				if (chain != null) {
					var amount = ReadCreateCommandTypeOfNumber (tree.children [1]);
					if (amount != null) {
						foreach (var target in chain) {
							var ret = target.Clone();
							ret.Turn (amount.GetValue ());
							instances.Add (ret);
						}
					}
					else {
						var degree = ReadCreateCommandTypeOfValue (tree.children [1]);
						if (degree != null) {
							foreach (var target in chain) {
								var ret = target.Clone();
								ret.Turn (degree.GetValue ());
								instances.Add (ret);
							}
						}
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
		case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
		{
			if (tree.children.Count == 1) {
				var chain = ReadCreateCommandTypeOfSlideChain (tree.children[0], head);
				if (chain != null) {
					switch (switchKey) {
					case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
					{
						foreach (var target in chain) {
							var ret = target.Clone();
							ret.MirrorHolizontal ();
							instances.Add (ret);
						}
						break;
					}
					case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
					{
						foreach (var target in chain) {
							var ret = target.Clone();
							ret.MirrorVertical ();
							instances.Add (ret);
						}
						break;
					}
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CHAIN:
		{
			if (tree.children.Count == 0) { // エディタのときは正しい.
				instances.Add (MaimaiScore.SlideChain.FromMaiScript (null, null, new MaimaiScore.ISlideCommand[0]));
				Debug.LogWarning ("chain param counts is 0. This is the correct motion if editor.");
			}
			else if (tree.children.Count > 0) {
				int index = 0;
				var wait = ReadCreateCommandTypeOfSlideHeadCustomWait (tree.children[index]);
				if (wait != null) {
					index++;
				}
				else {
					wait = MaimaiScore.SlideHeadCustomWaitDefault.Create();
				}
				var step = ReadCreateCommandTypeOfStep (tree.children [index]);
				if (step != null) {
					index++;
				}
				var commands = new List<MaimaiScore.ISlideCommand>();
				MaimaiScore.ISlideCommand beforeCommand = null;
				for (int i = index; i < tree.children.Count; i++) {
					var chainData = tree.children[i];
					var command = ReadCreateCommandTypeOfSlideCommand (chainData, head, beforeCommand);
					if (command != null && command.Length > 0) {
						beforeCommand = command[command.Length - 1];
						commands.AddRange (command);
					}
				}
				var instance = MaimaiScore.SlideChain.FromMaiScript (wait, step, commands.ToArray());
				if (instance == null) {
					return null; // commandのエラーメッセージに委託する.
				}
				else {
					instances.Add (instance);
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CALL:
		{
			if (tree.children.Count >= 1) {
				if (userDefine.ContainsKey(tree.children[0].data)) {
					var replaced = userDefine[tree.children[0].data].Call (MaisqScriptUserDefine.CreateCallMethodParamsFromMaiScriptTree(tree));
					var calledTrees = MaisqScriptTree.CreateTreeFromMaiScript(replaced);
					foreach (var calledTree in calledTrees) {
						var instance = ReadCreateCommandTypeOfSlideChain (calledTree, head);
						if (instance != null && instance.Length > 0) {
							instances.AddRange (instance);
						}
					}
				}
				else {
					MaisqScriptReadErrorCommon ("define", tree);
				}
			}
			break;
		}
		}
		if (instances != null) {
			var ret = instances.ToArray ();
			ret = ReadExtendCommandTypeOfSlideChain (ret, tree.extend);
			ReadOptionTypeOfSlideChain (ret, tree);
			return ret;
		}
		MaisqScriptReadErrorCommon ("slide chain", tree);
		return null;
	}
	
	public MaimaiScore.ISlideChain[] ReadExtendCommandTypeOfSlideChain (MaimaiScore.ISlideChain[] instances, MaisqScriptTree tree) {
		if (instances == null || tree == null || tree.data.Length == 0) return instances;
		
		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.TURN:
		{
			if (tree.children.Count == 1) {
				var amount = ReadCreateCommandTypeOfNumber (tree.children [0]);
				if (amount != null) {
					foreach (var instance in instances) {
						instance.Turn (amount.GetValue ());
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
		case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
		{
			if (tree.children.Count == 0) {
				switch (switchKey) {
				case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
				{
					foreach (var instance in instances) {
						instance.MirrorHolizontal ();
					}
					break;
				}
				case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
				{
					foreach (var instance in instances) {
						instance.MirrorVertical ();
					}
					break;
				}
				}
			}
			break;
		}
		}
		instances = ReadExtendCommandTypeOfSlideChain (instances, tree.extend);
		ReadOptionTypeOfSlideChain (instances, tree);
		return instances;
	}
	
	public void ReadOptionTypeOfSlideChain (MaimaiScore.ISlideChain[] instances, MaisqScriptTree tree) {
		
	}
	
	#endregion
	#region ISlideCommand
	/// <summary>
	/// maiScriptを呼んでスライドコマンド(描画命令)を返す.
	/// </summary>
	public MaimaiScore.ISlideCommand[] ReadCreateCommandTypeOfSlideCommand (MaisqScriptTree tree, MaimaiScore.ISlideHead head, MaimaiScore.ISlideCommand beforeCommand) {
		if (tree == null || tree.data.Length == 0) return null;

		var instances = new List<MaimaiScore.ISlideCommand>();
		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.TURN:
		{
			if (tree.children.Count == 2) {
				var command = ReadCreateCommandTypeOfSlideCommand (tree.children[0], head, beforeCommand);
				if (command != null) {
					var amount = ReadCreateCommandTypeOfNumber (tree.children [1]);
					if (amount != null) {
						foreach (var target in command) {
							var ret = target.Clone();
							ret.Turn (amount.GetValue ());
							instances.Add (ret);
						}
					}
					else {
						var degree = ReadCreateCommandTypeOfValue (tree.children [1]);
						if (degree != null) {
							foreach (var target in command) {
								var ret = target.Clone();
								ret.Turn (degree.GetValue ());
								instances.Add (ret);
							}
						}
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
		case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
		{
			if (tree.children.Count == 1) {
				var command = ReadCreateCommandTypeOfSlideCommand (tree.children[0], head, beforeCommand);
				if (command != null) {
					switch (switchKey) {
					case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
					{
						foreach (var target in command) {
							var ret = target.Clone();
							ret.MirrorHolizontal ();
							instances.Add (ret);
						}
						break;
					}
					case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
					{
						foreach (var target in command) {
							var ret = target.Clone();
							ret.MirrorVertical ();
							instances.Add (ret);
						}
						break;
					}
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.STRAIGHT:
		{
			if (tree.children.Count == 2) {
				var start = ReadCreateCommandTypeOfPosition (tree.children[0]);
				var target = ReadCreateCommandTypeOfPosition (tree.children[1]);
				if (start != null && target != null) {
					var instance = MaimaiScore.SlideCommandStraight.FromMaiScript (start, target);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			else if (tree.children.Count == 4) {
				var sx = ReadCreateCommandTypeOfValue (tree.children[0]);
				var sy = ReadCreateCommandTypeOfValue (tree.children[1]);
				var tx = ReadCreateCommandTypeOfValue (tree.children[2]);
				var ty = ReadCreateCommandTypeOfValue (tree.children[3]);
				if (sx != null && sy != null && tx != null && ty != null) {
					var instance = MaimaiScore.SlideCommandStraight.FromMaiScript (sx, sy, tx, ty);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CONTINUED_STRAIGHT:
		{
			if (tree.children.Count == 1) {
				var target = ReadCreateCommandTypeOfPosition (tree.children[0]);
				if (target != null) {
					var instance = MaimaiScore.SlideCommandStraight.FromMaiScript (target, head, beforeCommand);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			else if (tree.children.Count == 2) {
				var tx = ReadCreateCommandTypeOfValue (tree.children[0]);
				var ty = ReadCreateCommandTypeOfValue (tree.children[1]);
				if (tx != null && ty != null) {
					var instance = MaimaiScore.SlideCommandStraight.FromMaiScript (tx, ty, head, beforeCommand);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CURVE:
		{
			if (tree.children.Count == 4) {
				var center = ReadCreateCommandTypeOfPosition (tree.children[0]);
				var radius2d = ReadCreateCommandTypeOfPosition (tree.children[1]);
				var startDeg = ReadCreateCommandTypeOfValue (tree.children[2]);
				var distanceDeg = ReadCreateCommandTypeOfValue (tree.children[3]);
				if (center != null && startDeg != null && distanceDeg != null) {
					if (radius2d != null) {
						var instance = MaimaiScore.SlideCommandCurve.FromMaiScript (center, radius2d, startDeg, distanceDeg);
						if (instance != null) {
							instances.Add (instance);
						}
					}
					else {
						var radius = ReadCreateCommandTypeOfValue (tree.children[1]);
						if (radius != null) {
							var instance = MaimaiScore.SlideCommandCurve.FromMaiScript (center, radius, startDeg, distanceDeg);
							if (instance != null) {
								instances.Add (instance);
							}
						}
					}
				}
			}
			else if (tree.children.Count == 6) {
				var cx = ReadCreateCommandTypeOfValue (tree.children[0]);
				var cy = ReadCreateCommandTypeOfValue (tree.children[1]);
				var rx = ReadCreateCommandTypeOfValue (tree.children[2]);
				var ry = ReadCreateCommandTypeOfValue (tree.children[3]);
				var startDeg = ReadCreateCommandTypeOfValue (tree.children[4]);
				var distanceDeg = ReadCreateCommandTypeOfValue (tree.children[5]);
				if (cx != null && cy != null && rx != null && ry != null && startDeg != null && distanceDeg != null) {
					var instance = MaimaiScore.SlideCommandCurve.FromMaiScript (cx, cy, rx, ry, startDeg, distanceDeg);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			else if (tree.children.Count == 5) {
				var cx = ReadCreateCommandTypeOfValue (tree.children[0]);
				var cy = ReadCreateCommandTypeOfValue (tree.children[1]);
				var radius = ReadCreateCommandTypeOfValue (tree.children[2]);
				var startDeg = ReadCreateCommandTypeOfValue (tree.children[3]);
				var distanceDeg = ReadCreateCommandTypeOfValue (tree.children[4]);
				if (cx != null && cy != null && radius != null && startDeg != null && distanceDeg != null) {
					var instance = MaimaiScore.SlideCommandCurve.FromMaiScript (cx, cy, radius, startDeg, distanceDeg);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CONTINUED_CURVE:
		{
			if (tree.children.Count == 2) {
				var center = ReadCreateCommandTypeOfPosition (tree.children[0]);
				var distanceDeg = ReadCreateCommandTypeOfValue (tree.children[1]);
				if (center != null && distanceDeg != null) {
					var instance = MaimaiScore.SlideCommandCurve.FromMaiScript (center, distanceDeg, head, beforeCommand);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			else if (tree.children.Count == 3) {
				var cx = ReadCreateCommandTypeOfValue (tree.children[0]);
				var cy = ReadCreateCommandTypeOfValue (tree.children[1]);
				var distanceDeg = ReadCreateCommandTypeOfValue (tree.children[2]);
				if (cx != null && cy != null && distanceDeg != null) {
					var instance = MaimaiScore.SlideCommandCurve.FromMaiScript (cx, cy, distanceDeg, head, beforeCommand);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
			
		// outer_straight_outerみたいなショートカットは用意する.
		// 実態はbuttonからpositionを取得するなど.
		// OUTER_STRAIGHT
		case MaisqScore.CommandNameDocument.OUTER_STRAIGHT_OUTER:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					var instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.OuterSensorPosition (button1),
						MaimaiScore.Position.OuterSensorPosition (button2)
						);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.OUTER_STRAIGHT_INNER:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					var instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.OuterSensorPosition (button1),
						MaimaiScore.Position.InnerSensorPosition (button2)
						);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.OUTER_STRAIGHT_CENTER:
		{
			if (tree.children.Count == 1) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				if (button1 != null) {
					var instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.OuterSensorPosition (button1),
						MaimaiScore.Position.CenterSensorPosition ()
						);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		// OUTER_CURVE
		case MaisqScore.CommandNameDocument.OUTER_CURVE_CLOCKWISE_AXIS_CENTER:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					var instance = MaimaiScore.SlideCommandCurve.FromMaiScript (
						MaimaiScore.Position.CenterSensorPosition (),
						MaimaiScore.Value.OuterSensorRadius (),
						MaimaiScore.Value.SensorDegree (button1),
						MaimaiScore.Value.SensorDegreeDistanceClockwise (button1, button2)
						);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.OUTER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					var instance = MaimaiScore.SlideCommandCurve.FromMaiScript (
						MaimaiScore.Position.CenterSensorPosition (),
						MaimaiScore.Value.OuterSensorRadius (),
						MaimaiScore.Value.SensorDegree (button1),
						MaimaiScore.Value.SensorDegreeDistanceCounterClockwise (button1, button2)
						);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.OUTER_CURVE_RIGHT_AXIS_CENTER:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					var instance = MaimaiScore.SlideCommandCurve.FromMaiScript (
						MaimaiScore.Position.CenterSensorPosition (),
						MaimaiScore.Value.OuterSensorRadius (),
						MaimaiScore.Value.SensorDegree (button1),
						MaimaiScore.Value.SensorDegreeDistanceRight (button1, button2)
						);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.OUTER_CURVE_LEFT_AXIS_CENTER:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					var instance = MaimaiScore.SlideCommandCurve.FromMaiScript (
						MaimaiScore.Position.CenterSensorPosition (),
						MaimaiScore.Value.OuterSensorRadius (),
						MaimaiScore.Value.SensorDegree (button1),
						MaimaiScore.Value.SensorDegreeDistanceLeft (button1, button2)
						);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		// INNER_STRAIGHT
		case MaisqScore.CommandNameDocument.INNER_STRAIGHT_OUTER:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					var instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.InnerSensorPosition (button1),
						MaimaiScore.Position.OuterSensorPosition (button2)
						);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.INNER_STRAIGHT_INNER:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					var instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.InnerSensorPosition (button1),
						MaimaiScore.Position.InnerSensorPosition (button2)
						);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.INNER_STRAIGHT_CENTER:
		{
			if (tree.children.Count == 1) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				if (button1 != null) {
					var instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.InnerSensorPosition (button1),
						MaimaiScore.Position.CenterSensorPosition ()
						);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		// INNER_CURVE
		case MaisqScore.CommandNameDocument.INNER_CURVE_CLOCKWISE_AXIS_CENTER:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					var instance = MaimaiScore.SlideCommandCurve.FromMaiScript (
						MaimaiScore.Position.CenterSensorPosition (),
						MaimaiScore.Value.InnerSensorRadius (),
						MaimaiScore.Value.SensorDegree (button1),
						MaimaiScore.Value.SensorDegreeDistanceClockwise (button1, button2)
						);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.INNER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					var instance = MaimaiScore.SlideCommandCurve.FromMaiScript (
						MaimaiScore.Position.CenterSensorPosition (),
						MaimaiScore.Value.InnerSensorRadius (),
						MaimaiScore.Value.SensorDegree (button1),
						MaimaiScore.Value.SensorDegreeDistanceCounterClockwise (button1, button2)
						);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.INNER_CURVE_RIGHT_AXIS_CENTER:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					var instance = MaimaiScore.SlideCommandCurve.FromMaiScript (
						MaimaiScore.Position.CenterSensorPosition (),
						MaimaiScore.Value.InnerSensorRadius (),
						MaimaiScore.Value.SensorDegree (button1),
						MaimaiScore.Value.SensorDegreeDistanceRight (button1, button2)
						);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.INNER_CURVE_LEFT_AXIS_CENTER:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					var instance = MaimaiScore.SlideCommandCurve.FromMaiScript (
						MaimaiScore.Position.CenterSensorPosition (),
						MaimaiScore.Value.InnerSensorRadius (),
						MaimaiScore.Value.SensorDegree (button1),
						MaimaiScore.Value.SensorDegreeDistanceLeft (button1, button2)
						);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		// CENTER_STRAIGHT
		case MaisqScore.CommandNameDocument.CENTER_STRAIGHT_OUTER:
		{
			if (tree.children.Count == 1) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				if (button1 != null) {
					var instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.CenterSensorPosition (),
						MaimaiScore.Position.OuterSensorPosition (button1)
						);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.CENTER_STRAIGHT_INNER:
		{
			if (tree.children.Count == 1) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				if (button1 != null) {
					var instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.CenterSensorPosition (),
						MaimaiScore.Position.InnerSensorPosition (button1)
						);
					if (instance != null) {
						instances.Add (instance);
					}
				}
			}
			break;
		}
		// SHAPE
		case MaisqScore.CommandNameDocument.SHAPE_P_AXIS_CENTER:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					//if (t==(s+4)%8) As-B((s+6)%8)-At
					//else As-B((s+6)%8)<B((t+2)%8)-At
					var continueButton1 = MaimaiScore.Button.Create ((button1.GetIndex() + 8 - 2) % 8 + 1);
					MaimaiScore.ISlideCommand instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.OuterSensorPosition (button1), // Start A
						MaimaiScore.Position.InnerSensorPosition (continueButton1) // Start-2 B
						);
					instances.Add (instance);
					if (button2.GetIndex() == (button1.GetIndex() + 4) % 8) {
						instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
							MaimaiScore.Position.InnerSensorPosition (continueButton1),
							MaimaiScore.Position.OuterSensorPosition (button2) // Target A
						);
						instances.Add (instance);
					}
					else {
						var continueButton2 = MaimaiScore.Button.Create ((button2.GetIndex() + 8 + 2) % 8 + 1);
						instance = MaimaiScore.SlideCommandCurve.FromMaiScript (
							MaimaiScore.Position.CenterSensorPosition (), // Axis Center
							MaimaiScore.Value.InnerSensorRadius (),  // Target+2 B
							MaimaiScore.Value.SensorDegree (continueButton1),
							MaimaiScore.Value.SensorDegreeDistanceCounterClockwise (continueButton1, continueButton2)
							);
						instances.Add (instance);
						instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
							MaimaiScore.Position.InnerSensorPosition (continueButton2),
							MaimaiScore.Position.OuterSensorPosition (button2) //Target A
						);
						instances.Add (instance);
					}
					if (instances.Contains (null)) {
						instances.Clear();
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SHAPE_Q_AXIS_CENTER:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					//if (t==(s+4)%8) As-B((s+2)%8)-At
					//else As-B((s+2)%8)>B((t+6)%8)-At
					var continueButton1 = MaimaiScore.Button.Create ((button1.GetIndex() + 8 + 2) % 8 + 1);
					MaimaiScore.ISlideCommand instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.OuterSensorPosition (button1), // Start A
						MaimaiScore.Position.InnerSensorPosition (continueButton1) // Start+2 B
						);
					instances.Add (instance);
					if (button2.GetIndex() == (button1.GetIndex() + 4) % 8) {
						instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
							MaimaiScore.Position.InnerSensorPosition (continueButton1),
							MaimaiScore.Position.OuterSensorPosition (button2) // Target A
							);
						instances.Add (instance);
					}
					else {
						var continueButton2 = MaimaiScore.Button.Create ((button2.GetIndex() + 8 - 2) % 8 + 1);
						instance = MaimaiScore.SlideCommandCurve.FromMaiScript (
							MaimaiScore.Position.CenterSensorPosition (), // Axis Center
							MaimaiScore.Value.InnerSensorRadius (), // Target-2 B
							MaimaiScore.Value.SensorDegree (continueButton1),
							MaimaiScore.Value.SensorDegreeDistanceClockwise (continueButton1, continueButton2)
							);
						if (instance != null) {
							instances.Add (instance);
							instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
								MaimaiScore.Position.InnerSensorPosition (continueButton2),
								MaimaiScore.Position.OuterSensorPosition (button2) // Target A
								);
							instances.Add (instance);
						}
					}
					if (instances.Contains (null)) {
						instances.Clear();
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SHAPE_S:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					//As-B((s+6)%8)-B((t+2)%8)-At
					var continueButton1 = MaimaiScore.Button.Create ((button1.GetIndex() + 8 - 2) % 8 + 1);
					MaimaiScore.ISlideCommand instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.OuterSensorPosition (button1), // Start A
						MaimaiScore.Position.InnerSensorPosition (continueButton1) // Start-2 B
						);
					instances.Add (instance);
					var continueButton2 = MaimaiScore.Button.Create ((button2.GetIndex() + 8 - 2) % 8 + 1);
					instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.InnerSensorPosition (continueButton1),
						MaimaiScore.Position.InnerSensorPosition (continueButton2) // Target-2 B
						);
					instances.Add (instance);
					instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.InnerSensorPosition (continueButton2),
						MaimaiScore.Position.OuterSensorPosition (button2) // Target A
						);
					instances.Add (instance);
					if (instances.Contains (null)) {
						instances.Clear();
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SHAPE_Z:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					//As-B((s+2)%8)-C-B((t+2)%8)-At
					var continueButton1 = MaimaiScore.Button.Create ((button1.GetIndex() + 8 + 2) % 8 + 1);
					MaimaiScore.ISlideCommand instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.OuterSensorPosition (button1), // Start A
						MaimaiScore.Position.InnerSensorPosition (continueButton1) // Start+2 B
						);
					instances.Add (instance);
					var continueButton2 = MaimaiScore.Button.Create ((button2.GetIndex() + 8 + 2) % 8 + 1);
					instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.InnerSensorPosition (continueButton1),
						MaimaiScore.Position.InnerSensorPosition (continueButton2) // Target+2 B
						);
					instances.Add (instance);
					instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.InnerSensorPosition (continueButton2),
						MaimaiScore.Position.OuterSensorPosition (button2) // Target A
						);
					instances.Add (instance);
					if (instances.Contains (null)) {
						instances.Clear();
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SHAPE_V_AXIS_CENTER:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					//As-C-At
					MaimaiScore.ISlideCommand instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.OuterSensorPosition (button1), // Start A
						MaimaiScore.Position.CenterSensorPosition () // C
						);
					instances.Add (instance);
					instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.CenterSensorPosition (),
						MaimaiScore.Position.OuterSensorPosition (button2) // Target A
						);
					instances.Add (instance);
					if (instances.Contains (null)) {
						instances.Clear();
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SHAPE_PP:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					//As-C
					MaimaiScore.ISlideCommand instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.OuterSensorPosition (button1), // Start A
						MaimaiScore.Position.SimaiPPFirstStraightTargetPosition (button1) // ほぼ中心.
						);
					instances.Add (instance);
					// Turn
					instance = MaimaiScore.SlideCommandCurve.FromMaiScript (
						MaimaiScore.Position.SimaiPPCenterPosition (button1),
						MaimaiScore.Value.SimaiPPTurnDegree (button1, button2),
						head,
						instance
						);
					instances.Add (instance);
					// - At
					instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.OuterSensorPosition (button2), // Target A
						head,
						instance
						);
					instances.Add (instance);
					if (instances.Contains (null)) {
						instances.Clear();
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SHAPE_QQ:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 != null && button2 != null) {
					//As-C
					MaimaiScore.ISlideCommand instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.OuterSensorPosition (button1), // Start A
						MaimaiScore.Position.SimaiQQFirstStraightTargetPosition (button1) // ほぼ中心.
						);
					instances.Add (instance);
					// Turn
					instance = MaimaiScore.SlideCommandCurve.FromMaiScript (
						MaimaiScore.Position.SimaiQQCenterPosition (button1),
						MaimaiScore.Value.SimaiQQTurnDegree (button1, button2),
						head,
						instance
						);
					instances.Add (instance);
					// - At
					instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.OuterSensorPosition (button2), // Target A
						head,
						instance
						);
					instances.Add (instance);
					if (instances.Contains (null)) {
						instances.Clear();
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SHAPE_V_AXIS_OUTER:
		{
			if (tree.children.Count == 3) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]); //start
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]); //turn
				var button3 = ReadCreateCommandTypeOfButton (tree.children[2]); //target
				if (button1 != null && button2 != null && button3 != null) {
					// Start-Turn
					MaimaiScore.ISlideCommand instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.OuterSensorPosition (button1), // Start
						MaimaiScore.Position.OuterSensorPosition (button2) // Turn
						);
					instances.Add (instance);
					// Turn-Target
					instance = MaimaiScore.SlideCommandStraight.FromMaiScript (
						MaimaiScore.Position.OuterSensorPosition (button3), // Target
						head,
						instance
						);
					instances.Add (instance);
					if (instances.Contains (null)) {
						instances.Clear();
					}
				}
			}
			break;
		}
			
			
			
		case MaisqScore.CommandNameDocument.CALL:
		{
			if (tree.children.Count >= 1) {
				if (userDefine.ContainsKey(tree.children[0].data)) {
					var replaced = userDefine[tree.children[0].data].Call (MaisqScriptUserDefine.CreateCallMethodParamsFromMaiScriptTree(tree));
					var calledTrees = MaisqScriptTree.CreateTreeFromMaiScript(replaced);
					foreach (var calledTree in calledTrees) {
						var sc = ReadCreateCommandTypeOfSlideCommand (calledTree, head, beforeCommand);
						if (sc != null && sc.Length > 0) {
							beforeCommand = sc[sc.Length - 1];
							instances.AddRange (sc);
						}
					}
				}
				else {
					MaisqScriptReadErrorCommon ("define", tree);
				}
			}
			break;
		}
		}
		if (instances != null) {
			var ret = instances.ToArray ();
			ret = ReadExtendCommandTypeOfSlideCommand (ret, tree.extend);
			ReadOptionTypeOfSlideCommand (ret, tree);
			return ret;
		}
		MaisqScriptReadErrorCommon ("slide command", tree);
		return null;
	}
	
	public MaimaiScore.ISlideCommand[] ReadExtendCommandTypeOfSlideCommand (MaimaiScore.ISlideCommand[] instances, MaisqScriptTree tree) {
		if (instances == null || tree == null || tree.data.Length == 0) return instances;
		
		string switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.TURN:
		{
			if (tree.children.Count == 1) {
				var amount = ReadCreateCommandTypeOfNumber (tree.children [0]);
				if (amount != null) {
					foreach (var instance in instances) {
						instance.Turn (amount.GetValue ());
					}
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
		case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
		{
			if (tree.children.Count == 0) {
				switch (switchKey) {
				case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
				{
					foreach (var instance in instances) {
						instance.MirrorHolizontal ();
					}
					break;
				}
				case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
				{
					foreach (var instance in instances) {
						instance.MirrorVertical ();
					}
					break;
				}
				}
			}
			break;
		}
		}
		instances = ReadExtendCommandTypeOfSlideCommand (instances, tree.extend);
		ReadOptionTypeOfSlideCommand (instances, tree);
		return instances;
	}
	
	public void ReadOptionTypeOfSlideCommand (MaimaiScore.ISlideCommand[] instance, MaisqScriptTree tree) {
		
	}
	#endregion
	
	#region Compress
	public MaisqScriptTree Compress (MaimaiScore.Each[] score) {
		var root = MaisqScriptTree.CreateRoot ("mai_script_score");
		foreach (var each in score) {
			if (each.change_bpm.HasValue) {
				MaisqScriptTree bpmBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.BPM);
				bpmBranch.AddChild (each.change_bpm.Value);
				root.children.Add (bpmBranch);
			}
			if (each.change_step != null) {
				MaisqScriptTree stepBranch;
				if (!each.change_step.is_interval) {
					stepBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.BEAT);
				}
				else {
					stepBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.INTERVAL);
				}
				stepBranch.AddChild (each.change_bpm.Value);
				root.children.Add (stepBranch);
			}
			foreach (var note in each.notes) {
				MaisqScriptTree noteBranch = Compress (note as MaimaiScore.INote);
				if (noteBranch != null) {
					root.children.Add (noteBranch);
				}
			}
		}
		return root;
	}
	
	public MaisqScriptTree Compress (MaimaiScore.INote iNote) {
		MaisqScriptTree noteBranch = null;
		if (iNote is MaimaiScore.NoteTap) {
			var note = iNote as MaimaiScore.NoteTap;
			noteBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.TAP);
			noteBranch.AddChild (note.button.GetValue());
		}
		else if (iNote is MaimaiScore.NoteHold) {
			var note = iNote as MaimaiScore.NoteHold;
			noteBranch.AddChild (note.button.GetValue());
			MaisqScriptTree stepBranch = Compress (note.step as MaimaiScore.IStep);
			if (stepBranch != null) {
				noteBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.HOLD);
				noteBranch.children.Add (stepBranch);
			}
		}
		else if (iNote is MaimaiScore.NoteSlide) {
			var note = iNote as MaimaiScore.NoteSlide;
			var headBranch = Compress (note.head as MaimaiScore.ISlideHead);
			var waitBranch = Compress (note.wait);
			var stepBranch = Compress (note.step);
			List<MaisqScriptTree> patternBranches = new List<MaisqScriptTree>();
			foreach (var pattern in note.patterns) {
				var patternBranch = Compress(pattern as MaimaiScore.ISlidePattern);
				if (patternBranch != null) {
					patternBranches.Add (patternBranch);
				}
			}
			if (headBranch != null && patternBranches.Count > 0) {
				noteBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.SLIDE);
				noteBranch.children.Add (headBranch);
				if (waitBranch != null) {
					noteBranch.children.Add (waitBranch);
				}
				if (stepBranch != null) {
					noteBranch.children.Add (stepBranch);
				}
				noteBranch.children.AddRange (patternBranches);
			}
		}
		else if (iNote is MaimaiScore.NoteBreak) {
			var note = iNote as MaimaiScore.NoteBreak;
			noteBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.BREAK);
			noteBranch.AddChild (note.button.GetValue());
		}
		if (noteBranch != null) {
			if (iNote.option != null) {
				noteBranch.option[MaisqScore.CommandNameDocument.SECRET] = iNote.option.secret.ToString().ToLower();
			}
		}
		return noteBranch;
	}
	
	public MaisqScriptTree Compress (MaimaiScore.IStep iStep) {
		MaisqScriptTree stepBranch = null;
		if (iStep is MaimaiScore.StepLength) {
			var step = iStep as MaimaiScore.StepLength;
			stepBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.STEP);
			stepBranch.AddChildren (step.beat, step.length);
		}
		else if (iStep is MaimaiScore.StepLocalBpm) {
			var step = iStep as MaimaiScore.StepLocalBpm;
			stepBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.STEP);
			stepBranch.AddChildren (step.local_bpm, step.beat, step.length);
		}
		else if (iStep is MaimaiScore.StepInterval) {
			var step = iStep as MaimaiScore.StepInterval;
			stepBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.STEP);
			stepBranch.AddChild (step.interval);
		}
		return stepBranch;
	}
	
	public MaisqScriptTree Compress (MaimaiScore.ISlideHead iHead) {
		MaisqScriptTree headBranch = null;
		if (iHead is MaimaiScore.SlideHeadStar) {
			var head = iHead as MaimaiScore.SlideHeadStar;
			headBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.STAR);
			headBranch.AddChild (head.button.GetValue());
		}
		else if (iHead is MaimaiScore.SlideHeadBreakStar) {
			var head = iHead as MaimaiScore.SlideHeadBreakStar;
			headBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.BREAK_STAR);
			headBranch.AddChild (head.button.GetValue());
		}
		return headBranch;
	}
	
	public MaisqScriptTree Compress (MaimaiScore.ISlideHeadCustomWait iWait) {
		MaisqScriptTree waitBranch = null;
		if (iWait is MaimaiScore.SlideHeadCustomWaitLocalBpm) {
			var wait = iWait as MaimaiScore.SlideHeadCustomWaitLocalBpm;
			waitBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.WAIT_CUSTOM_LOCAL_BPM);
			waitBranch.AddChild (wait.local_bpm);
		}
		else if (iWait is MaimaiScore.SlideHeadCustomWaitInterval) {
			var wait = iWait as MaimaiScore.SlideHeadCustomWaitInterval;
			waitBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.WAIT_CUSTOM_INTERVAL);
			waitBranch.AddChild (wait.interval);
		}
		return waitBranch;
	}
	
	public MaisqScriptTree Compress (MaimaiScore.ISlidePattern iPattern) {
		MaisqScriptTree patternBranch = null;
		if (iPattern is MaimaiScore.SlidePattern) {
			var pattern = iPattern as MaimaiScore.SlidePattern;
			var waitBranch = Compress (pattern.wait);
			var stepBranch = Compress (pattern.step);
			List<MaisqScriptTree> chainBranches = new List<MaisqScriptTree>();
			foreach (var chain in pattern.chains) {
				var commandBranch = Compress(chain as MaimaiScore.ISlideChain);
				if (commandBranch != null) {
					chainBranches.Add (commandBranch);
				}
			}
			if (chainBranches.Count > 0) {
				patternBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.PATTERN);
				if (waitBranch != null) {
					patternBranch.children.Add (waitBranch);
				}
				if (stepBranch != null) {
					patternBranch.children.Add (stepBranch);
				}
				patternBranch.children.AddRange (chainBranches);
			}
		}
		return patternBranch;
	}
	
	public MaisqScriptTree Compress (MaimaiScore.ISlideChain iChain) {
		MaisqScriptTree chainBranch = null;
		if (iChain is MaimaiScore.SlideChain) {
			var chain = iChain as MaimaiScore.SlideChain;
			var waitBranch = Compress (chain.wait as MaimaiScore.ISlideHeadCustomWait);
			var stepBranch = Compress (chain.step as MaimaiScore.IStep);
			List<MaisqScriptTree> commandBranches = new List<MaisqScriptTree>();
			foreach (var command in chain.commands) {
				var commandBranch = Compress(command as MaimaiScore.ISlideCommand);
				if (commandBranch != null) {
					commandBranches.Add (commandBranch);
				}
			}
			if (commandBranches.Count > 0) {
				chainBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.CHAIN);
				if (waitBranch != null) {
					chainBranch.children.Add (waitBranch);
				}
				if (stepBranch != null) {
					chainBranch.children.Add (stepBranch);
				}
				chainBranch.children.AddRange (commandBranches);
			}
		}
		return chainBranch;
	}
	
	public MaisqScriptTree Compress (MaimaiScore.ISlideCommand iCommand) {
		MaisqScriptTree commandBranch = null;
		if (iCommand is MaimaiScore.SlideCommandStraight) {
			var command = iCommand as MaimaiScore.SlideCommandStraight;
			commandBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.STRAIGHT);
			var positionBranch1 = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.POSITION);
			positionBranch1.AddChildren (command.start.x, command.start.y);
			commandBranch.children.Add (positionBranch1);
			var positionBranch2 = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.POSITION);
			positionBranch2.AddChildren (command.target.x, command.target.y);
			commandBranch.children.Add (positionBranch2);
		}
		else if (iCommand is MaimaiScore.SlideCommandCurve) {
			var command = iCommand as MaimaiScore.SlideCommandCurve;
			commandBranch = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.CURVE);
			var positionBranch1 = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.POSITION);
			positionBranch1.AddChildren (command.center.x, command.center.y);
			commandBranch.children.Add (positionBranch1);
			var positionBranch2 = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.POSITION);
			positionBranch2.AddChildren (command.radius.x, command.radius.y);
			commandBranch.children.Add (positionBranch2);
			commandBranch.AddChild (command.start_degree);
			commandBranch.AddChild (command.distance_degree);
		}
		return commandBranch;
	}
	#endregion
	
	
	// waitとstepにおけるエラーをスルーするコマンド一覧.
	private bool ThroughErrorCommand (string command) {
		switch (command) {
		case MaisqScore.CommandNameDocument.WAIT_DEFAULT:
		case MaisqScore.CommandNameDocument.WAIT_CUSTOM_LOCAL_BPM:
		case MaisqScore.CommandNameDocument.WAIT_CUSTOM_INTERVAL:
		case MaisqScore.CommandNameDocument.STEP:
		case MaisqScore.CommandNameDocument.PATTERN:
		case MaisqScore.CommandNameDocument.CHAIN:
		case MaisqScore.CommandNameDocument.TURN:
		case MaisqScore.CommandNameDocument.MIRROR_HOLIZONTAL:
		case MaisqScore.CommandNameDocument.MIRROR_VERTICAL:
		case MaisqScore.CommandNameDocument.STRAIGHT:
		case MaisqScore.CommandNameDocument.CONTINUED_STRAIGHT:
		case MaisqScore.CommandNameDocument.CURVE:
		case MaisqScore.CommandNameDocument.CONTINUED_CURVE:
		case MaisqScore.CommandNameDocument.OUTER_STRAIGHT_OUTER:
		case MaisqScore.CommandNameDocument.OUTER_STRAIGHT_INNER:
		case MaisqScore.CommandNameDocument.OUTER_STRAIGHT_CENTER:
		case MaisqScore.CommandNameDocument.OUTER_CURVE_CLOCKWISE_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.OUTER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.OUTER_CURVE_RIGHT_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.OUTER_CURVE_LEFT_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.INNER_STRAIGHT_OUTER:
		case MaisqScore.CommandNameDocument.INNER_STRAIGHT_INNER:
		case MaisqScore.CommandNameDocument.INNER_STRAIGHT_CENTER:
		case MaisqScore.CommandNameDocument.INNER_CURVE_CLOCKWISE_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.INNER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.INNER_CURVE_RIGHT_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.INNER_CURVE_LEFT_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.CENTER_STRAIGHT_OUTER:
		case MaisqScore.CommandNameDocument.CENTER_STRAIGHT_INNER:
		case MaisqScore.CommandNameDocument.SHAPE_P_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.SHAPE_Q_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.SHAPE_S:
		case MaisqScore.CommandNameDocument.SHAPE_Z:
		case MaisqScore.CommandNameDocument.SHAPE_V_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.SHAPE_PP:
		case MaisqScore.CommandNameDocument.SHAPE_QQ:
		case MaisqScore.CommandNameDocument.SHAPE_V_AXIS_OUTER:
		case MaisqScore.CommandNameDocument.CALL:
			return true;
		}
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//	// maiScriptのスライドパターン関数の種類を増やすならここに定義を追加.
	//	/// <summary>
	//	/// maiScriptのスライドパターンデータが欲しい関数(MaisqScore.MaisqScriptCommandNameDocument.SLIDEなど)で使う関数の定義.
	//	/// </summary>
	//	protected virtual MaimaiScoreSlidePatternFactoryCommandData ReadSlidePatternCommand(MaimaiScoreNoteData.TapData star, MaimaiScoreSlidePatternFactoryCommandData beforeCommand, MaiScriptTree tree) {
	//		var switchKey = tree.data;
	//		switch (switchKey) {
	//		case "outer_straight_outer":{
	//			int button1;
	//			int button2;
	//			if (tree.children.Count == 2) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				if (!int.TryParse(tree.children[1].data, out button2)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandOuterStraightOuter(button1, button2);
	//			}
	//			break;
	//		}
	//		case "outer_straight_inner":{
	//			int button1;
	//			int button2;
	//			if (tree.children.Count == 2) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				if (!int.TryParse(tree.children[1].data, out button2)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandOuterStraightInner(button1, button2);
	//			}
	//			break;
	//		}
	//		case "outer_straight_center":{
	//			int button1;
	//			if (tree.children.Count == 1) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandOuterStraightCenter(button1);
	//			}
	//			break;
	//		}
	//		case "inner_straight_outer":{
	//			int button1;
	//			int button2;
	//			if (tree.children.Count == 2) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				if (!int.TryParse(tree.children[1].data, out button2)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandInnerStraightOuter(button1, button2);
	//			}
	//			break;
	//		}
	//		case "inner_straight_inner":{
	//			int button1;
	//			int button2;
	//			if (tree.children.Count == 2) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				if (!int.TryParse(tree.children[1].data, out button2)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandInnerStraightInner(button1, button2);
	//			}
	//			break;
	//		}
	//		case "inner_straight_center":{
	//			int button1;
	//			if (tree.children.Count == 1) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandInnerStraightCenter(button1);
	//			}
	//			break;
	//		}
	//		case "center_straight_outer":{
	//			int button1;
	//			if (tree.children.Count == 1) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandCenterStraightOuter(button1);
	//			}
	//			break;
	//		}
	//		case "center_straight_inner":{
	//			int button1;
	//			if (tree.children.Count == 1) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandCenterStraightInner(button1);
	//			}
	//			break;
	//		}
	//		case "outer_curve_clockwise":{
	//			int button1;
	//			int button2;
	//			if (tree.children.Count == 2) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				if (!int.TryParse(tree.children[1].data, out button2)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandOuterCurveClockwise(button1, button2);
	//			}
	//			break;
	//		}
	//		case "outer_curve_counterclockwise":{
	//			int button1;
	//			int button2;
	//			if (tree.children.Count == 2) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				if (!int.TryParse(tree.children[1].data, out button2)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandOuterCurveCounterClockwise(button1, button2);
	//			}
	//			break;
	//		}
	//		case "outer_curve_right":
	//		case "outer_curve_left": {
	//			string ltgt = switchKey == "outer_curve_right" ? ">" : "<";
	//			int button1;
	//			int button2;
	//			if (tree.children.Count == 2) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				if (!int.TryParse(tree.children[1].data, out button2)) {
	//					return null;
	//				}
	//				int start = button1 - 1;
	//				while (start < 0) start += 8;
	//				start %= 8;
	//				start += 1;
	//				if (start == 1 || start == 2 || start == 7 || start == 8) {
	//					if (ltgt == ">") {
	//						return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandOuterCurveClockwise(button1, button2);
	//					}
	//					else if (ltgt == "<") {
	//						return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandOuterCurveCounterClockwise(button1, button2);
	//					}
	//				}
	//				else if (start == 3 || start == 4 || start == 5 || start == 6) {
	//					if (ltgt == ">") {
	//						return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandOuterCurveCounterClockwise(button1, button2);
	//					}
	//					else if (ltgt == "<") {
	//						return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandOuterCurveClockwise(button1, button2);
	//					}
	//				}
	//				return null;
	//			}
	//			break;
	//		}
	//		case "inner_curve_clockwise":{
	//			int button1;
	//			int button2;
	//			if (tree.children.Count == 2) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				if (!int.TryParse(tree.children[1].data, out button2)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandInnerCurveClockwise(button1, button2);
	//			}
	//			break;
	//		}
	//		case "inner_curve_counterclockwise":{
	//			int button1;
	//			int button2;
	//			if (tree.children.Count == 2) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				if (!int.TryParse(tree.children[1].data, out button2)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandInnerCurveCounterClockwise(button1, button2);
	//			}
	//			break;
	//		}
	//		case "inner_curve_right":
	//		case "inner_curve_left": {
	//			string ltgt = switchKey == "inner_curve_right" ? ">" : "<";
	//			int button1;
	//			int button2;
	//			if (tree.children.Count == 2) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				if (!int.TryParse(tree.children[1].data, out button2)) {
	//					return null;
	//				}
	//				int start = button1 - 1;
	//				while (start < 0) start += 8;
	//				start %= 8;
	//				start += 1;
	//				if (start == 1 || start == 2 || start == 7 || start == 8) {
	//					if (ltgt == ">") {
	//						return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandInnerCurveClockwise(button1, button2);
	//					}
	//					else if (ltgt == "<") {
	//						return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandInnerCurveCounterClockwise(button1, button2);
	//					}
	//				}
	//				else if (start == 3 || start == 4 || start == 5 || start == 6) {
	//					if (ltgt == ">") {
	//						return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandInnerCurveCounterClockwise(button1, button2);
	//					}
	//					else if (ltgt == "<") {
	//						return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandInnerCurveClockwise(button1, button2);
	//					}
	//				}
	//				return null;
	//			}
	//			break;
	//		}
	//		case "shape_p":{
	//			int button1;
	//			int button2;
	//			if (tree.children.Count == 2) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				if (!int.TryParse(tree.children[1].data, out button2)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandShapeP(button1, button2);
	//			}
	//			break;
	//		}
	//		case "shape_q":{
	//			int button1;
	//			int button2;
	//			if (tree.children.Count == 2) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				if (!int.TryParse(tree.children[1].data, out button2)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandShapeQ(button1, button2);
	//			}
	//			break;
	//		}
	//		case "shape_s":{
	//			int button1;
	//			int button2;
	//			if (tree.children.Count == 2) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				if (!int.TryParse(tree.children[1].data, out button2)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandShapeS(button1, button2);
	//			}
	//			break;
	//		}
	//		case "shape_z":{
	//			int button1;
	//			int button2;
	//			if (tree.children.Count == 2) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				if (!int.TryParse(tree.children[1].data, out button2)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandShapeZ(button1, button2);
	//			}
	//			break;
	//		}
	//		case "shape_v":{
	//			int button1;
	//			int button2;
	//			if (tree.children.Count == 2) {
	//				if (!int.TryParse(tree.children[0].data, out button1)) {
	//					return null;
	//				}
	//				if (!int.TryParse(tree.children[1].data, out button2)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateFactoryCommandShapeV(button1, button2);
	//			}
	//			break;
	//		}
	//		case "outer_straight":{
	//			int button;
	//			float start_x, start_y, target_x, target_y;
	//			if (tree.children.Count == 5) {
	//				if (!int.TryParse(tree.children[0].data, out button)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[1], out start_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[2], out start_y, true)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[3], out target_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[4], out target_y, true)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateOuterStraight(button, start_x, start_y, target_x, target_y);
	//			}
	//			break;
	//		}
	//		case "inner_straight":{
	//			int button;
	//			float start_x, start_y, target_x, target_y;
	//			if (tree.children.Count == 5) {
	//				if (!int.TryParse(tree.children[0].data, out button)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[1], out start_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[2], out start_y, true)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[3], out target_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[4], out target_y, true)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateInnerStraight(button, start_x, start_y, target_x, target_y);
	//			}
	//			break;
	//		}
	//		case "center_straight":{
	//			float start_x, start_y, target_x, target_y;
	//			if (tree.children.Count == 4) {
	//				if (!ReadMathCommand(tree.children[0], out start_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[1], out start_y, true)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[2], out target_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[3], out target_y, true)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateCenterStraight(start_x, start_y, target_x, target_y);
	//			}
	//			break;
	//		}
	//		case "continued_outer_straight":{
	//			int button;
	//			float target_x, target_y;
	//			if (tree.children.Count == 3) {
	//				if (!int.TryParse(tree.children[0].data, out button)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[1], out target_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[2], out target_y, true)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateContinuedOuterStraight(star, beforeCommand, button, target_x, target_y);
	//			}
	//			break;
	//		}
	//		case "continued_inner_straight":{
	//			int button;
	//			float target_x, target_y;
	//			if (tree.children.Count == 3) {
	//				if (!int.TryParse(tree.children[0].data, out button)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[1], out target_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[2], out target_y, true)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateContinuedInnerStraight(star, beforeCommand, button, target_x, target_y);
	//			}
	//			break;
	//		}
	//		case "continued_center_straight":{
	//			float target_x, target_y;
	//			if (tree.children.Count == 2) {
	//				if (!ReadMathCommand(tree.children[0], out target_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[1], out target_y, true)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateContinuedCenterStraight(star, beforeCommand, target_x, target_y);
	//			}
	//			break;
	//		}
	//		case "outer_curve":{
	//			int button;
	//			float center_x, center_y, radius_x, radius_y, start_degree, distance_degree;
	//			if (tree.children.Count == 7) {
	//				if (!int.TryParse(tree.children[0].data, out button)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[1], out center_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[2], out center_y, true)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[3], out radius_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[4], out radius_y, true)) {
	//					return null;
	//				}
	//				if (!ReadDegreeCommand(tree.children[5], out start_degree)) {
	//					return null;
	//				}
	//				if (!ReadDegreeCommand(tree.children[6], out distance_degree)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateOuterCurve(button, center_x, center_y, radius_x, radius_y, start_degree, distance_degree);
	//			}
	//			break;
	//		}
	//		case "inner_curve":{
	//			int button;
	//			float center_x, center_y, radius_x, radius_y, start_degree, distance_degree;
	//			if (tree.children.Count == 7) {
	//				if (!int.TryParse(tree.children[0].data, out button)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[1], out center_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[2], out center_y, true)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[3], out radius_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[4], out radius_y, true)) {
	//					return null;
	//				}
	//				if (!ReadDegreeCommand(tree.children[5], out start_degree)) {
	//					return null;
	//				}
	//				if (!ReadDegreeCommand(tree.children[6], out distance_degree)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateInnerCurve(button, center_x, center_y, radius_x, radius_y, start_degree, distance_degree);
	//			}
	//			break;
	//		}
	//		case "center_curve":{
	//			float center_x, center_y, radius_x, radius_y, start_degree, distance_degree;
	//			if (tree.children.Count == 6) {
	//				if (!ReadMathCommand(tree.children[0], out center_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[1], out center_y, true)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[2], out radius_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[3], out radius_y, true)) {
	//					return null;
	//				}
	//				if (!ReadDegreeCommand(tree.children[4], out start_degree)) {
	//					return null;
	//				}
	//				if (!ReadDegreeCommand(tree.children[5], out distance_degree)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateCenterCurve(center_x, center_y, radius_x, radius_y, start_degree, distance_degree);
	//			}
	//			break;
	//		}
	//		case "continued_outer_curve":{
	//			int button;
	//			float center_x, center_y, distance_degree;
	//			if (tree.children.Count == 4) {
	//				if (!int.TryParse(tree.children[0].data, out button)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[1], out center_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[2], out center_y, true)) {
	//					return null;
	//				}
	//				if (!ReadDegreeCommand(tree.children[3], out distance_degree)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateContinuedOuterCurve(star, beforeCommand, button, center_x, center_y, distance_degree);
	//			}
	//			break;
	//		}
	//		case "continued_inner_curve":{
	//			int button;
	//			float center_x, center_y, distance_degree;
	//			if (tree.children.Count == 4) {
	//				if (!int.TryParse(tree.children[0].data, out button)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[1], out center_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[2], out center_y, true)) {
	//					return null;
	//				}
	//				if (!ReadDegreeCommand(tree.children[3], out distance_degree)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateContinuedInnerCurve(star, beforeCommand, button, center_x, center_y, distance_degree);
	//			}
	//			break;
	//		}
	//		case "continued_center_curve":{
	//			float center_x, center_y, distance_degree;
	//			if (tree.children.Count == 3) {
	//				if (!ReadMathCommand(tree.children[0], out center_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[1], out center_y, true)) {
	//					return null;
	//				}
	//				if (!ReadDegreeCommand(tree.children[2], out distance_degree)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateContinuedCenterCurve(star, beforeCommand, center_x, center_y, distance_degree);
	//			}
	//			break;
	//		}
	//
	//		}
	//		return null;
	//	}
	//
	//	
	//	protected virtual MaimaiScoreSlidePatternFactoryCommandData[] ReadSlidePatternsCommand(MaimaiScoreNoteData.TapData star, MaimaiScoreSlidePatternFactoryCommandData beforeCommand, MaiScriptTree tree) {
	//		var switchKey = tree.data;
	//		switch (switchKey) {
	//		case "allocate_straight":{
	//			float start_x, start_y, target_x, target_y;
	//			if (tree.children.Count == 4) {
	//				if (!ReadMathCommand(tree.children[0], out start_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[1], out start_y, true)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[2], out target_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[3], out target_y, true)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateAutomaticPartisionStraight(start_x, start_y, target_x, target_y);
	//			}
	//			break;
	//		}
	//		case "continued_allocate_straight":{
	//			float target_x, target_y;
	//			if (tree.children.Count == 2) {
	//				if (!ReadMathCommand(tree.children[0], out target_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[1], out target_y, true)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateAutomaticPartisionStraight(star, beforeCommand, target_x, target_y);
	//			}
	//			break;
	//		}
	//		case "allocate_curve":{
	//			float center_x, center_y, radius_x, radius_y, start_degree, distance_degree;
	//			if (tree.children.Count == 6) {
	//				if (!ReadMathCommand(tree.children[0], out center_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[1], out center_y, true)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[2], out radius_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[3], out radius_y, true)) {
	//					return null;
	//				}
	//				if (!ReadDegreeCommand(tree.children[4], out start_degree)) {
	//					return null;
	//				}
	//				if (!ReadDegreeCommand(tree.children[5], out distance_degree)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateAutomaticPartisionCurve(center_x, center_y, radius_x, radius_y, start_degree, distance_degree);
	//			}
	//			break;
	//		}
	//		case "continued_allocate_curve":{
	//			float center_x, center_y, distance_degree;
	//			if (tree.children.Count == 3) {
	//				if (!ReadMathCommand(tree.children[0], out center_x, false)) {
	//					return null;
	//				}
	//				if (!ReadMathCommand(tree.children[1], out center_y, true)) {
	//					return null;
	//				}
	//				if (!ReadDegreeCommand(tree.children[2], out distance_degree)) {
	//					return null;
	//				}
	//				return MaimaiScoreSlidePatternFactoryCommandData.CreateAutomaticPartisionCurve(star, beforeCommand, center_x, center_y, distance_degree);
	//			}
	//			break;
	//		}
	//		}
	//		Debug.LogError ("\"" + switchKey + "\" (" + tree.children.Count + " params) is not slide pattern command.");
	//		return null;
	//	}
	//
	//	public virtual bool ReadMathCommand(MaiScriptTree tree, out float value, bool vertical) {
	//		float lefthandSystem = vertical ? 180 : 0;
	//		value = 0;
	//		var switchKey = tree.data;
	//		switch (switchKey) {
	//		case "sin":
	//		case "-sin":
	//		case MaisqScore.MaisqScriptCommandNameDocument.ANGLE_Y:
	//		case "-angle_y": {
	//			if (tree.children.Count == 1 || tree.children.Count == 2) {
	//				float deg, radius;
	//				if (!ReadDegreeCommand(tree.children[0], out deg)) {
	//					value = 0;
	//					return false;
	//				}
	//				if (tree.children.Count == 2) {
	//					if (!float.TryParse(tree.children[1].data, out radius)) {
	//						value = 0;
	//						return false;
	//					}
	//				}
	//				else {
	//					radius = 1.0f;
	//				}
	//				value = Mathf.Sin (CircleCalculator.ToRadian(deg + lefthandSystem)) * radius;
	//				if (switchKey[0] == '-') value *= -1;
	//				return true;
	//			}
	//			break;
	//		}
	//		case "cos":
	//		case "-cos":
	//		case MaisqScore.MaisqScriptCommandNameDocument.ANGLE_X:
	//		case "-angle_x": {
	//			if (tree.children.Count >= 1 || tree.children.Count == 2) {
	//				float deg, radius;
	//				if (!ReadDegreeCommand(tree.children[0], out deg)) {
	//					value = 0;
	//					return false;
	//				}
	//				if (tree.children.Count == 2) {
	//					if (!float.TryParse(tree.children[1].data, out radius)) {
	//						value = 0;
	//						return false;
	//					}
	//				}
	//				else {
	//					radius = 1.0f;
	//				}
	//				value = Mathf.Cos (CircleCalculator.ToRadian(deg + lefthandSystem)) * radius;
	//				if (switchKey[0] == '-') value *= -1;
	//				return true;
	//			}
	//			break;
	//		}
	//		case "outer_sensor_x":
	//		case "outer_sensor_y": {
	//			if (tree.children.Count == 1) {
	//				int sensor;
	//				if (!int.TryParse(tree.children[0].data, out sensor)) {
	//					sensor = 0;
	//					return false;
	//				}
	//				sensor -= 1;
	//				while (sensor < 0) sensor += 8;
	//				sensor %= 8;
	//				var pos = CircleCalculator.PointOnCircle(Vector2.zero, 1, Constants.instance.GetPieceDegree(sensor));
	//				if (switchKey == "outer_sensor_y") value = -pos.y;
	//				else value = pos.x;
	//				return true;
	//			}
	//			break;
	//		}
	//		case "inner_sensor_x":
	//		case "inner_sensor_y": {
	//			if (tree.children.Count == 1) {
	//				int sensor;
	//				if (!int.TryParse(tree.children[0].data, out sensor)) {
	//					sensor = 0;
	//					return false;
	//				}
	//				sensor -= 1;
	//				while (sensor < 0) sensor += 8;
	//				sensor %= 8;
	//				var clossExLines = CircleCalculator.LinesIntersect(
	//					CircleCalculator.PointOnCircle(Vector2.zero, 1, Constants.instance.GetPieceDegree(0)),
	//					CircleCalculator.PointOnCircle(Vector2.zero, 1, Constants.instance.GetPieceDegree(3)),
	//					CircleCalculator.PointOnCircle(Vector2.zero, 1, Constants.instance.GetPieceDegree(6)),
	//					CircleCalculator.PointOnCircle(Vector2.zero, 1, Constants.instance.GetPieceDegree(2))
	//				);
	//				var radius = CircleCalculator.PointToPointDistance(Vector2.zero, clossExLines);
	//				var pos = CircleCalculator.PointOnCircle(Vector2.zero, radius, Constants.instance.GetPieceDegree(sensor));
	//				if (switchKey == "inner_sensor_y") value = -pos.y;
	//				else value = pos.x;
	//				return true;
	//			}
	//			break;
	//		}
	//		case "center_sensor_x":
	//		case "center_sensor_y": {
	//			if (tree.children.Count == 0) {
	//				value = 0;
	//				return true;
	//			}
	//			break;
	//		}
	//		default : {
	//			if (!float.TryParse(tree.data, out value)) {
	//				value = 0;
	//				return false;
	//			}
	//			return true;
	//		}
	//		}
	//		Debug.LogError ("\"" + switchKey + "\" (" + tree.children.Count + " params) is not math command.");
	//		return false;
	//	}
	//
	//	public virtual bool ReadDegreeCommand(MaiScriptTree tree, out float value) {
	//		value = 0;
	//		var switchKey = tree.data;
	//		switch (switchKey) {
	//		case MaisqScore.MaisqScriptCommandNameDocument.SENSOR_DEGREE: {
	//			if (tree.children.Count == 1) {
	//				int sensor;
	//				if (!int.TryParse (tree.children [0].data, out sensor)) {
	//					value = 0;
	//					return false;
	//				}
	//				sensor -= 1;
	//				while (sensor < 0) sensor += 8;
	//				sensor %= 8;
	//				value = Constants.instance.GetPieceDegree (sensor);
	//				return true;
	//			}
	//			break;
	//		}
	//		case MaisqScore.MaisqScriptCommandNameDocument.SENSOR_DEGREE_DISTANCE_CLOCKWISE:
	//		case "-sensor_degree_distance_clockwise":{
	//			if (tree.children.Count == 2) {
	//				int startSensor, targetSensor;
	//				if (!int.TryParse (tree.children [0].data, out startSensor)) {
	//					value = 0;
	//					return false;
	//				}
	//				if (!int.TryParse (tree.children [1].data, out targetSensor)) {
	//					value = 0;
	//					return false;
	//				}
	//				startSensor -= 1;
	//				while (startSensor < 0) startSensor += 8;
	//				startSensor %= 8;
	//				targetSensor -= 1;
	//				while (targetSensor < 0) targetSensor += 8;
	//				targetSensor %= 8;
	//				if (startSensor > targetSensor) {
	//					float startDeg = Constants.instance.GetPieceDegree (startSensor);
	//					float targetDeg = Constants.instance.GetPieceDegree (targetSensor) + 360f;
	//					value = targetDeg - startDeg;
	//				}
	//				else if (startSensor < targetSensor) {
	//					float startDeg = Constants.instance.GetPieceDegree (startSensor);
	//					float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
	//					value = targetDeg - startDeg;
	//				}
	//				else {
	//					value = 0;
	//				}
	//				if (switchKey[0] == '-') value *= -1;
	//				return true;
	//			}
	//			break;
	//		}
	//		case MaisqScore.MaisqScriptCommandNameDocument.SENSOR_DEGREE_DISTANCE_COUNTERCLOCKWISE:
	//		case "-sensor_degree_distance_counterclockwise":{
	//			if (tree.children.Count == 2) {
	//				int startSensor, targetSensor;
	//				if (!int.TryParse (tree.children [0].data, out startSensor)) {
	//					value = 0;
	//					return false;
	//				}
	//				if (!int.TryParse (tree.children [1].data, out targetSensor)) {
	//					value = 0;
	//					return false;
	//				}
	//				startSensor -= 1;
	//				while (startSensor < 0) startSensor += 8;
	//				startSensor %= 8;
	//				targetSensor -= 1;
	//				while (targetSensor < 0) targetSensor += 8;
	//				targetSensor %= 8;
	//				if (startSensor > targetSensor) {
	//					float startDeg = Constants.instance.GetPieceDegree (startSensor);
	//					float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
	//					value = -(startDeg - targetDeg);
	//				}
	//				else if (startSensor < targetSensor) {
	//					float startDeg = Constants.instance.GetPieceDegree (startSensor) + 360f;
	//					float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
	//					value = -(startDeg - targetDeg);
	//				}
	//				else {
	//					value = 0;
	//				}
	//				if (switchKey[0] == '-') value *= -1;
	//				return true;
	//			}
	//			break;
	//		}
	//		case MaisqScore.MaisqScriptCommandNameDocument.SENSOR_DEGREE_DISTANCE_RIGHT:
	//		case "-sensor_degree_distance_right": {
	//			if (tree.children.Count == 2) {
	//				int startSensor, targetSensor;
	//				if (!int.TryParse (tree.children [0].data, out startSensor)) {
	//					value = 0;
	//					return false;
	//				}
	//				if (!int.TryParse (tree.children [1].data, out targetSensor)) {
	//					value = 0;
	//					return false;
	//				}
	//				startSensor -= 1;
	//				while (startSensor < 0) startSensor += 8;
	//				startSensor %= 8;
	//				targetSensor -= 1;
	//				while (targetSensor < 0) targetSensor += 8;
	//				targetSensor %= 8;
	//				if (startSensor == 6 || startSensor == 7 || startSensor == 0 || startSensor == 1) {
	//					if (startSensor > targetSensor) {
	//						float startDeg = Constants.instance.GetPieceDegree (startSensor);
	//						float targetDeg = Constants.instance.GetPieceDegree (targetSensor) + 360f;
	//						value = targetDeg - startDeg;
	//					}
	//					else if (startSensor < targetSensor) {
	//						float startDeg = Constants.instance.GetPieceDegree (startSensor);
	//						float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
	//						value = targetDeg - startDeg;
	//					}
	//					else {
	//						value = 0;
	//					}
	//				}
	//				else {
	//					if (startSensor > targetSensor) {
	//						float startDeg = Constants.instance.GetPieceDegree (startSensor);
	//						float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
	//						value = -(startDeg - targetDeg);
	//					}
	//					else if (startSensor < targetSensor) {
	//						float startDeg = Constants.instance.GetPieceDegree (startSensor) + 360f;
	//						float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
	//						value = -(startDeg - targetDeg);
	//					}
	//					else {
	//						value = 0;
	//					}
	//				}
	//				if (switchKey[0] == '-') value *= -1;
	//				return true;
	//			}
	//			break;
	//		}
	//		case MaisqScore.MaisqScriptCommandNameDocument.SENSOR_DEGREE_DISTANCE_LEFT:
	//		case "-sensor_degree_distance_left": {
	//			if (tree.children.Count == 2) {
	//				int startSensor, targetSensor;
	//				if (!int.TryParse (tree.children [0].data, out startSensor)) {
	//					value = 0;
	//					return false;
	//				}
	//				if (!int.TryParse (tree.children [1].data, out targetSensor)) {
	//					value = 0;
	//					return false;
	//				}
	//				startSensor -= 1;
	//				while (startSensor < 0) startSensor += 8;
	//				startSensor %= 8;
	//				targetSensor -= 1;
	//				while (targetSensor < 0) targetSensor += 8;
	//				targetSensor %= 8;
	//				if (startSensor == 6 || startSensor == 7 || startSensor == 0 || startSensor == 1) {
	//					if (startSensor > targetSensor) {
	//						float startDeg = Constants.instance.GetPieceDegree (startSensor);
	//						float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
	//						value = -(startDeg - targetDeg);
	//					}
	//					else if (startSensor < targetSensor) {
	//						float startDeg = Constants.instance.GetPieceDegree (startSensor) + 360f;
	//						float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
	//						value = -(startDeg - targetDeg);
	//					}
	//					else {
	//						value = 0;
	//					}
	//				}
	//				else {
	//					if (startSensor > targetSensor) {
	//						float startDeg = Constants.instance.GetPieceDegree (startSensor);
	//						float targetDeg = Constants.instance.GetPieceDegree (targetSensor) + 360f;
	//						value = targetDeg - startDeg;
	//					}
	//					else if (startSensor < targetSensor) {
	//						float startDeg = Constants.instance.GetPieceDegree (startSensor);
	//						float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
	//						value = targetDeg - startDeg;
	//					}
	//					else {
	//						value = 0;
	//					}
	//				}
	//				if (switchKey[0] == '-') value *= -1;
	//				return true;
	//			}
	//			break;
	//		}
	//		default : {
	//			if (!float.TryParse(tree.data, out value)) {
	//				value = 0;
	//				return false;
	//			}
	//			return true;
	//		}
	//		}
	//		Debug.LogError ("\"" + switchKey + "\" (" + tree.children.Count + " params) is not degree command.");
	//		return false;
	//	}
	
}

