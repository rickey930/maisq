using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// <para>maiScriptの関数名か値が入るツリー</para>
/// <para>dataには関数名("bpm"とか)または値("128"とか)が入る.</para>
/// <para>基本的にはchildren.Countが0ならばdataは値、1以上あるならばdataは関数名としてプログラムを作った.</para>
/// </summary>
public class MaisqScriptTree {
	public MaisqScriptTree parent;
	public string data;
	public bool quot; // dataが""に囲まれていたか.
	public Dictionary<string, string> option;
	public List<MaisqScriptTree> children;
	public MaisqScriptTree based;
	public MaisqScriptTree extend;
	public MaisqScriptTree(MaisqScriptTree parent, string data) {
		this.parent = parent;
		this.data = data;
		children = new List<MaisqScriptTree>();
		option = new Dictionary<string, string> ();
		extend = null;
	}
	public MaisqScriptTree (string data) : this (null, data) {

	}
	public MaisqScriptTree[] getChildren { get { return children.ToArray (); } }
	public static MaisqScriptTree CreateRoot(string data) {
		return new MaisqScriptTree (data);
	}
	public MaisqScriptTree AddChild (string data) {
		var child = new MaisqScriptTree (this, data);
		children.Add (child);
		return child;
	}
	public MaisqScriptTree[] AddChildren (params string[] data) {
		List<MaisqScriptTree> ret = new List<MaisqScriptTree> ();
		foreach (var d in data) {
			ret.Add (AddChild (d));
		}
		return ret.ToArray ();
	}
	public MaisqScriptTree AddChild (object data) {
		return AddChild (data.ToString ());
	}
	public MaisqScriptTree[] AddChildren (params object[] data) {
		List<MaisqScriptTree> ret = new List<MaisqScriptTree> ();
		foreach (var d in data) {
			ret.Add (AddChild (d));
		}
		return ret.ToArray ();
	}
	public void SetExtend (MaisqScriptTree extendTree) {
		this.extend = extendTree;
		if (extendTree != null) {
			extendTree.based = this;
		}
	}
	public MaisqScriptTree Clone () {
		var ret = new MaisqScriptTree (this.parent, this.data);
		foreach (var key in this.option.Keys) {
			ret.option[key] = this.option[key];
		}
		foreach (var child in this.children) {
			ret.children.Add (child.Clone ());
		}
		if (this.extend != null) {
			ret.extend = this.extend.Clone();
		}
		return ret;
	}
	
	public static string RemoveComment (string text) {
		return MaisqScriptReader.RemoveComment(text, false);
	}

	/// <summary>
	/// maiScriptテキストデータからメソッド名(または値)のツリーデータを作る.
	/// </summary>
	public static List<MaisqScriptTree> CreateTreeFromMaiScript(string maiScript) {
		string text1 = RemoveComment(maiScript);
		string text = text1.Replace ("\n", "");

		List<MaisqScriptTree> list = new List<MaisqScriptTree> ();
		MaisqScriptTree current = MaisqScriptTree.CreateRoot ("");
		int curlyBracketCount = 0;
		string optionKey = "";
		string objectValue = "";
		bool readStringDoubleQuote = false;
		bool readStringSingleQuote = false;
		string subSingle;
		string subNext;
		string subMulti;
		int subNextIsNumberChecker;
		for (int a = 0; a < text.Length; a++) {
			subSingle = text.Substring(a, 1);
			subNext = "";
			subMulti = subSingle;
			if (a + 1 < text.Length) {
				subNext = text.Substring (a + 1, 1);
				subMulti += subNext;
			}
			// 文字列読み込みフラグ切り替え.
			if (!readStringSingleQuote && subSingle == "\"") {
				readStringDoubleQuote = !readStringDoubleQuote;
				if (current != null) {
					current.quot = true;
				}
			}
			else if (!readStringDoubleQuote && subSingle == "\'") {
				readStringSingleQuote = !readStringSingleQuote;
				if (current != null) {
					current.quot = true;
				}
			}
			// "文字列"を読み込むとき.
			else if (readStringDoubleQuote || readStringSingleQuote) {
				if (subMulti == "\\\\") { //maiスクリプトの\\をC#の\\に変える.
					current.data += "\\";
					a++;
				}
				else if (readStringDoubleQuote && subMulti == "\\\"") { //maiスクリプトの""内の\"をC#の\"に変える.
					current.data += "\"";
					a++;
				}
				else if (readStringSingleQuote && subMulti == "\\\'") { //maiスクリプトの''内の\'をC#の\'に変える.
					current.data += "\'";
					a++;
				}
				else if (subMulti == "\\n") { //maiスクリプトの\nをC#の\nに変える.
					current.data += "\n";
					a++;
				}
				else {
					current.data += subSingle;
				}
			}
			// "文字列"でないとき.
			else {
				if (subSingle == "(" && curlyBracketCount == 0) {
					// 自分に子供を追加してそれをカレントにする.
					var tree = new MaisqScriptTree(current, "");
					current.children.Add (tree);
					current = tree;
				}
				else if (subSingle == "," && curlyBracketCount == 0) {
					// 親に子供を追加してそれをカレントにする.
					if (current.parent != null) {
						var tree = new MaisqScriptTree(current.parent, "");
						current.parent.children.Add (tree);
						current = tree;
					}
					else {
						// 親がいないから兄弟が作れない.
						Debug.LogError ("MaisqScript Syntax Error.");
					}
				}
				else if (subSingle == ")" && curlyBracketCount == 0) {
					// カレントを親に戻す.
					if (current.parent != null) {
						// なにもデータが無ければ親から消す.
						if (string.IsNullOrEmpty (current.data) && current.children.Count == 0) {
							current.parent.children.Remove (current);
						}
						current = current.parent;
					}
					else {
						// 親がいないからカレント移動ができない.
						Debug.LogError ("MaisqScript Syntax Error.");
					}
				}
				else if (subSingle == "." && curlyBracketCount == 0 && !int.TryParse(subNext, out subNextIsNumberChecker)) {
					// 派生データを作り、それをカレントにする.
					current.SetExtend (new MaisqScriptTree(current.parent, ""));
					current = current.extend;
				}
				else if (subSingle == "{" && curlyBracketCount == 0) {
					// パーソナルデータの入力開始フラグを立てる.
					curlyBracketCount++;
				}
				else if ((subSingle == ":" || subSingle == "=") && curlyBracketCount > 0) {
					// パーソナルデータのキーに覚えさせる.
					if (objectValue != "") {
						optionKey = objectValue;
					}
					objectValue = "";
				}
				else if (subSingle == "," && curlyBracketCount > 0) {
					// パーソナルデータの値に覚えさせる.
					if (optionKey != "" && objectValue != "") {
						current.option[optionKey] = objectValue;
					}
					objectValue = "";
				}
				else if (subSingle == "}" && curlyBracketCount > 0) {
					// パーソナルデータの値に覚えさせて、パーソナルデータの入力開始フラグを折る.
					if (optionKey != "" && objectValue != "") {
						current.option[optionKey] = objectValue;
					}
					objectValue = "";
					curlyBracketCount--;
				}
				else if (subSingle == ";" && curlyBracketCount == 0) {
					// 現在のカレントを終了する.
					while (current.based != null) {
						current = current.based;
					}
					list.Add (current);
					current = MaisqScriptTree.CreateRoot ("");
				}
				else if (subSingle == " " || subSingle == "　" || subSingle == "\t") {
					// スペースは無視. ただし"文字列"には入れたいのでReplaceでは処理しない.
				}
				else if (curlyBracketCount > 0) {
					objectValue += subSingle;
				}
				else if (current.children.Count == 0) {
					// data(child1, child2)after; のafterは認識させないようにテキストを読む.
					current.data += subSingle;
				}
			}
		}
		if (list.Count == 0 && !string.IsNullOrEmpty(current.data)) list.Add (current);
		return list;
	}
	
	/// <summary>
	/// maiScriptテキストデータからメソッド名(または値)のツリーデータを作る.
	/// </summary>
	public static MaisqScriptTree CreateTreeFromMaiScript(string rootName, string maiScript) {
		if (rootName == null) rootName = "maisq_script"; // null指定されたらデフォルトネーム.
		MaisqScriptTree root = MaisqScriptTree.CreateRoot (rootName);
		root.children = CreateTreeFromMaiScript (maiScript);
		return root;
	}
	
	/// <summary>
	/// maisqScriptとして展開.
	/// </summary>
	public static string Deploy (params MaisqScriptTree[] trees) {
		if (trees == null)
			return string.Empty;

		string ret = string.Empty;
		foreach (var tree in trees) {
			ret += tree.Deploy();
		}
		ret += ";";
		return ret;
	}

	/// <summary>
	/// maisqScriptとして展開.
	/// </summary>
	public static string Deploy (List<MaisqScriptTree> trees) {
		return Deploy (trees.ToArray ());
	}
	
	/// <summary>
	/// maisqScriptとして展開.
	/// </summary>
	public string Deploy () {
		string ret = string.Empty;
		if (quot) ret += "\"";
		ret += this.data;
		if (quot) ret += "\"";
		bool firstChild = true;
		foreach (var child in this.children) {
			if (firstChild) {
				ret += "(";
				firstChild = false;
			}
			else {
				ret += ", ";
			}
			ret += child.Deploy();
		}
		if (!firstChild) {
			ret += ")";
		}
		
		bool firstOption = true;
		foreach (var opt in this.option) {
			if (firstOption) {
				ret += " { ";
				firstOption = false;
			}
			else {
				ret += ", ";
			}
			ret += opt.Key + ":" + opt.Value;
		}
		if (!firstOption) {
			ret += " }";
		}
		
		if (this.extend != null) {
			ret += "." + this.extend.Deploy();
		}
		
		return ret;
	}
}

