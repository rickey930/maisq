﻿public class MaisqScriptUserDefine {
	public string name { get; private set; }
	public string content { get; private set; }
	public string[] args { get; private set; }
	public MaisqScriptUserDefine (string name, string content, params string[] args) {
		this.name = name;
		this.content = content;
		this.args = args;
	}

	public static MaisqScriptUserDefine FromMaiScriptTree (MaisqScriptTree tree) {
		if (tree.children.Count >= 2) {
			string[] args = new string[tree.children.Count - 2];
			for (int i = 0; i < args.Length; i++) {
				args[i] = tree.children[i + 2].data;
			}
			return new MaisqScriptUserDefine(
				tree.children[0].data, tree.children[1].data, args);
		}
		return null;
	}

	public static string[] CreateCallMethodParamsFromMaiScriptTree (MaisqScriptTree tree) {
		if (tree.children.Count >= 1) {
			string[] args = new string[tree.children.Count - 1];
			for (int i = 0; i < args.Length; i++) {
				args[i] = tree.children[i + 1].data;
			}
			return args;
		}
		return new string[0];
	}
	
	public string Call (params string[] replaces) {
		if (args == null || replaces == null || args.Length == 0 || replaces.Length == 0) {
			return content;
		}

		string temp = "";
		int tempIndex = 0;
		string ret = "";
		for (int i = 0; i < content.Length; i++) {
			temp += content[i];
			for (int j = 0; j < args.Length; j++) {
				int argLen = args[j].Length;
				if (tempIndex >= argLen - 1) {
					string check = temp.Substring(tempIndex - (argLen - 1), argLen);
					if (check == args[j]) {
						ret += temp.Substring(0, tempIndex - (argLen - 1));
						if (j < replaces.Length) {
							ret += replaces[j];
						}
						else {
							ret += args[j];
						}
						temp = "";
						tempIndex = -1;
					}
				}
			}
			tempIndex++;
		}
		ret += temp;
		return ret;
	}
}
