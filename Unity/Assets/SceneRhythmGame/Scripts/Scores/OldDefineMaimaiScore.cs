﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RhythmGameLibrary.Score;

/*
 * データはJSONで来る. JSONはMaimaiScoreEachDataに変換される.
 * データはsimaiで来る. simaiはMaimaiScoreEachDataに変換される.
 * データはmaiScriptで来る. maiScriptはMaimaiScoreEachDataに変換される.
 * 
 * MaimaiScoreEachDataをMaimaiNote[]に変換することでゲ－ムに使える.
 */
/*
[System.Serializable]
public class MaimaiScoreEachData : ScoreNoteData {
	public MaimaiScoreNoteData[] notes;
	
	public static MaimaiScoreEachData Push(params MaimaiScoreNoteData[] data) {
		return Create(0, null, data);
	}
	
	public static MaimaiScoreEachData PushAfterSetBpm(float bpm, params MaimaiScoreNoteData[] data) {
		return Create(bpm, null, data);
	}
	
	public static MaimaiScoreEachData PushAfterSetBeat(float beat, params MaimaiScoreNoteData[] data) {
		return Create(0, ScoreStepData.Create(beat, false), data);
	}
	
	public static MaimaiScoreEachData PushAfterSetInterval(float interval, params MaimaiScoreNoteData[] data) {
		return Create(0, ScoreStepData.Create(interval, true), data);
	}
	
	public static MaimaiScoreEachData PushAfterSetBpmAndSetBeat(float bpm, float beat, params MaimaiScoreNoteData[] data) {
		return Create(bpm, ScoreStepData.Create(beat, false), data);
	}
	
	public static MaimaiScoreEachData PushAfterSetBpmAndSetInterval(float bpm, float interval, params MaimaiScoreNoteData[] data) {
		return Create(bpm, ScoreStepData.Create(interval, true), data);
	}
	
	private static MaimaiScoreEachData Create(float bpm, ScoreStepData step, params MaimaiScoreNoteData[] data) {
		var ret = new MaimaiScoreEachData();
		if (bpm > 0) ret.change_bpm = bpm;
		if (step != null && step.step > 0) {
			ret.change_step = step;
		}
		ret.notes = data;
		return ret;
	}
}

[System.Serializable]
public class MaimaiScoreNoteData {
	public TapData tap_data;
	public HoldData hold_data;
	public SlideData slide_data;
	public CommonOptionData option_data;
	
	[System.Serializable]
	public class TapData {
		/// <summary>
		/// ボタン. 1～8指定.
		/// </summary>
		public int button;
		public bool is_break;
	}
	[System.Serializable]
	public class HoldData {
		/// <summary>
		/// ボタン. 1～8指定.
		/// </summary>
		public int button;
		public float? beat;
		public float? length;
		public float? interval;
		public float? local_bpm;
	}
	[System.Serializable]
	public class SlideData {
		/// <summary>
		/// ボタン. 1～8指定. null可. 
		/// </summary>
		public TapData star;
		public float? beat;
		public float? length;
		public float? interval;
		public float? local_bpm;
		public float? wait_local_bpm;
		public float? wait_interval;
		public MaimaiScoreSlidePatternFactoryCommandData[] commands;
	}
	[System.Serializable]
	public class CommonOptionData {
		public bool secret;
	}
	
	public static MaimaiScoreNoteData CreateTap(int button, bool isBreak) {
		var ret1 = new MaimaiScoreNoteData();
		var ret = new TapData();
		ret.button = button;
		ret.is_break = isBreak;
		ret1.tap_data = ret;
		ret1.option_data = new CommonOptionData();
		return ret1;
	}
	
	public static MaimaiScoreNoteData CreateHold(int button, float beat, float length) {
		var ret1 = new MaimaiScoreNoteData();
		var ret = new HoldData();
		ret.button = button;
		ret.beat = beat;
		ret.length = length;
		ret1.hold_data = ret;
		ret1.option_data = new CommonOptionData();
		return ret1;
	}
	
	public static MaimaiScoreNoteData CreateHoldSetLocalBpm(int button, float local_bpm, float beat, float length) {
		var ret1 = new MaimaiScoreNoteData();
		var ret = new HoldData();
		ret.button = button;
		ret.beat = beat;
		ret.length = length;
		ret.local_bpm = local_bpm;
		ret1.hold_data = ret;
		ret1.option_data = new CommonOptionData();
		return ret1;
	}
	
	public static MaimaiScoreNoteData CreateHoldSetInterval(int button, float interval) {
		var ret1 = new MaimaiScoreNoteData();
		var ret = new HoldData();
		ret.button = button;
		ret.interval = interval;
		ret1.hold_data = ret;
		ret1.option_data = new CommonOptionData();
		return ret1;
	}
	
	public static MaimaiScoreNoteData CreateSlide(TapData star, float beat, float length, params MaimaiScoreSlidePatternFactoryCommandData[] commands) {
		var ret1 = new MaimaiScoreNoteData();
		var ret = new SlideData();
		ret.star = star;
		ret.beat = beat;
		ret.length = length;
		ret.commands = commands;
		ret1.slide_data = ret;
		ret1.option_data = new CommonOptionData();
		return ret1;
	}
	
	public static MaimaiScoreNoteData CreateSlideSetLocalBpm(TapData star, float local_bpm, float beat, float length, params MaimaiScoreSlidePatternFactoryCommandData[] commands) {
		var ret1 = new MaimaiScoreNoteData();
		var ret = new SlideData();
		ret.star = star;
		ret.beat = beat;
		ret.length = length;
		ret.local_bpm = local_bpm;
		ret.commands = commands;
		ret1.slide_data = ret;
		ret1.option_data = new CommonOptionData();
		return ret1;
	}
	
	public static MaimaiScoreNoteData CreateSlideSetInterval(TapData star, float interval, params MaimaiScoreSlidePatternFactoryCommandData[] commands) {
		var ret1 = new MaimaiScoreNoteData();
		var ret = new SlideData();
		ret.star = star;
		ret.interval = interval;
		ret.commands = commands;
		ret1.slide_data = ret;
		ret1.option_data = new CommonOptionData();
		return ret1;
	}
	
	public static MaimaiScoreNoteData CreateSlideSetWaitLocalBpm(TapData star, float wait_local_bpm, float beat, float length, params MaimaiScoreSlidePatternFactoryCommandData[] commands) {
		var ret1 = new MaimaiScoreNoteData();
		var ret = new SlideData();
		ret.star = star;
		ret.beat = beat;
		ret.length = length;
		ret.wait_local_bpm = wait_local_bpm;
		ret.commands = commands;
		ret1.slide_data = ret;
		ret1.option_data = new CommonOptionData();
		return ret1;
	}
	
	public static MaimaiScoreNoteData CreateSlideSetWaitLocalBpmSetLocalBpm(TapData star, float wait_local_bpm, float local_bpm, float beat, float length, params MaimaiScoreSlidePatternFactoryCommandData[] commands) {
		var ret1 = new MaimaiScoreNoteData();
		var ret = new SlideData();
		ret.star = star;
		ret.beat = beat;
		ret.length = length;
		ret.local_bpm = local_bpm;
		ret.wait_local_bpm = wait_local_bpm;
		ret.commands = commands;
		ret1.slide_data = ret;
		ret1.option_data = new CommonOptionData();
		return ret1;
	}
	
	public static MaimaiScoreNoteData CreateSlideSetWaitLocalBpmSetInterval(TapData star, float wait_local_bpm, float interval, params MaimaiScoreSlidePatternFactoryCommandData[] commands) {
		var ret1 = new MaimaiScoreNoteData();
		var ret = new SlideData();
		ret.star = star;
		ret.interval = interval;
		ret.wait_local_bpm = wait_local_bpm;
		ret.commands = commands;
		ret1.slide_data = ret;
		ret1.option_data = new CommonOptionData();
		return ret1;
	}
	
	public static MaimaiScoreNoteData CreateSlideSetWaitInterval(TapData star, float wait_interval, float beat, float length, params MaimaiScoreSlidePatternFactoryCommandData[] commands) {
		var ret1 = new MaimaiScoreNoteData();
		var ret = new SlideData();
		ret.star = star;
		ret.beat = beat;
		ret.length = length;
		ret.wait_interval = wait_interval;
		ret.commands = commands;
		ret1.slide_data = ret;
		ret1.option_data = new CommonOptionData();
		return ret1;
	}
	
	public static MaimaiScoreNoteData CreateSlideSetWaitIntervalSetLocalBpm(TapData star, float wait_interval, float local_bpm, float beat, float length, params MaimaiScoreSlidePatternFactoryCommandData[] commands) {
		var ret1 = new MaimaiScoreNoteData();
		var ret = new SlideData();
		ret.star = star;
		ret.beat = beat;
		ret.length = length;
		ret.local_bpm = local_bpm;
		ret.wait_interval = wait_interval;
		ret.commands = commands;
		ret1.slide_data = ret;
		ret1.option_data = new CommonOptionData();
		return ret1;
	}
	
	public static MaimaiScoreNoteData CreateSlideSetWaitIntervalSetInterval(TapData star, float wait_interval, float interval, params MaimaiScoreSlidePatternFactoryCommandData[] commands) {
		var ret1 = new MaimaiScoreNoteData();
		var ret = new SlideData();
		ret.star = star;
		ret.interval = interval;
		ret.wait_interval = wait_interval;
		ret.commands = commands;
		ret1.slide_data = ret;
		ret1.option_data = new CommonOptionData();
		return ret1;
	}
}

[System.Serializable]
public class MaimaiScoreSlidePatternFactoryCommandData {
	public PatternFactoryCommandData factory;
	public StraightPatternData straight;
	public CurvePatternData curve;
	
	[System.Serializable]
	public class PatternFactoryCommandData {
		public int command_type;
		public int start;
		public int target;
		
		[System.NonSerialized]
		public const int FACTORY_COMMAND_OUTER_STRAIGHT_OUTER = 0;
		[System.NonSerialized]
		public const int FACTORY_COMMAND_OUTER_STRAIGHT_INNER = 1;
		[System.NonSerialized]
		public const int FACTORY_COMMAND_OUTER_STRAIGHT_CENTER = 2;
		[System.NonSerialized]
		public const int FACTORY_COMMAND_INNER_STRAIGHT_OUTER = 3;
		[System.NonSerialized]
		public const int FACTORY_COMMAND_INNER_STRAIGHT_INNER = 4;
		[System.NonSerialized]
		public const int FACTORY_COMMAND_INNER_STRAIGHT_CENTER = 5;
		[System.NonSerialized]
		public const int FACTORY_COMMAND_CENTER_STRAIGHT_OUTER = 6;
		[System.NonSerialized]
		public const int FACTORY_COMMAND_CENTER_STRAIGHT_INNER = 7;
		[System.NonSerialized]
		public const int FACTORY_COMMAND_OUTER_CURVE_CLOCKWISE = 8;
		[System.NonSerialized]
		public const int FACTORY_COMMAND_OUTER_CURVE_COUNTERCLOCKWISE = 9;
		[System.NonSerialized]
		public const int FACTORY_COMMAND_INNER_CURVE_CLOCKWISE = 10;
		[System.NonSerialized]
		public const int FACTORY_COMMAND_INNER_CURVE_COUNTERCLOCKWISE = 11;
		[System.NonSerialized]
		public const int FACTORY_COMMAND_SHAPE_P = 12;
		[System.NonSerialized]
		public const int FACTORY_COMMAND_SHAPE_Q = 13;
		[System.NonSerialized]
		public const int FACTORY_COMMAND_SHAPE_S = 14;
		[System.NonSerialized]
		public const int FACTORY_COMMAND_SHAPE_Z = 15;
		[System.NonSerialized]
		public const int FACTORY_COMMAND_SHAPE_V = 16;
	}
	
	[System.Serializable]
	public class StraightPatternData {
		/// <summary>
		/// <para>センサー番号</para>
		/// <para>中円は0、内周は1～8、外周は9～16</para>
		/// </summary>
		public int sensor;
		public float start_x;
		public float start_y;
		public float target_x;
		public float target_y;
	}
	
	[System.Serializable]
	public class CurvePatternData {
		/// <summary>
		/// <para>センサー番号</para>
		/// <para>中円は0、内周は1～8、外周は9～16</para>
		/// </summary>
		public int sensor;
		public float center_x;
		public float center_y;
		public float radius_x;
		public float radius_y;
		public float start_degree;
		public float distance_degree;
	}
	
	private static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommand(int command_type, int start, int target) {
		var ret1 = new MaimaiScoreSlidePatternFactoryCommandData();
		var ret = new PatternFactoryCommandData ();
		ret.command_type = command_type;
		ret.start = start;
		ret.target = target;
		ret1.factory = ret;
		return ret1;
	}
	
	private static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandTargetCenter(int command_type, int start) {
		var ret1 = new MaimaiScoreSlidePatternFactoryCommandData();
		var ret = new PatternFactoryCommandData ();
		ret.command_type = command_type;
		ret.start = start;
		ret.target = 0;
		ret1.factory = ret;
		return ret1;
	}
	
	private static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandStartCenter(int command_type, int target) {
		var ret1 = new MaimaiScoreSlidePatternFactoryCommandData();
		var ret = new PatternFactoryCommandData ();
		ret.command_type = command_type;
		ret.start = 0;
		ret.target = target;
		ret1.factory = ret;
		return ret1;
	}
	
	public static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandOuterStraightOuter(int start, int target) {
		return CreateFactoryCommand (PatternFactoryCommandData.FACTORY_COMMAND_OUTER_STRAIGHT_OUTER, start, target);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandOuterStraightInner(int start, int target) {
		return CreateFactoryCommand (PatternFactoryCommandData.FACTORY_COMMAND_OUTER_STRAIGHT_INNER, start, target);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandOuterStraightCenter(int start) {
		return CreateFactoryCommandTargetCenter (PatternFactoryCommandData.FACTORY_COMMAND_OUTER_STRAIGHT_CENTER, start);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandInnerStraightOuter(int start, int target) {
		return CreateFactoryCommand (PatternFactoryCommandData.FACTORY_COMMAND_INNER_STRAIGHT_OUTER, start, target);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandInnerStraightInner(int start, int target) {
		return CreateFactoryCommand (PatternFactoryCommandData.FACTORY_COMMAND_INNER_STRAIGHT_INNER, start, target);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandInnerStraightCenter(int start) {
		return CreateFactoryCommandTargetCenter (PatternFactoryCommandData.FACTORY_COMMAND_INNER_STRAIGHT_CENTER, start);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandCenterStraightOuter(int target) {
		return CreateFactoryCommandStartCenter (PatternFactoryCommandData.FACTORY_COMMAND_CENTER_STRAIGHT_OUTER, target);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandCenterStraightInner(int target) {
		return CreateFactoryCommandStartCenter (PatternFactoryCommandData.FACTORY_COMMAND_CENTER_STRAIGHT_INNER, target);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandOuterCurveClockwise(int start, int target) {
		return CreateFactoryCommand (PatternFactoryCommandData.FACTORY_COMMAND_OUTER_CURVE_CLOCKWISE, start, target);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandOuterCurveCounterClockwise(int start, int target) {
		return CreateFactoryCommand (PatternFactoryCommandData.FACTORY_COMMAND_OUTER_CURVE_COUNTERCLOCKWISE, start, target);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandInnerCurveClockwise(int start, int target) {
		return CreateFactoryCommand (PatternFactoryCommandData.FACTORY_COMMAND_INNER_CURVE_CLOCKWISE, start, target);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandInnerCurveCounterClockwise(int start, int target) {
		return CreateFactoryCommand (PatternFactoryCommandData.FACTORY_COMMAND_INNER_CURVE_COUNTERCLOCKWISE, start, target);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandShapeP(int start, int target) {
		return CreateFactoryCommand (PatternFactoryCommandData.FACTORY_COMMAND_SHAPE_P, start, target);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandShapeQ(int start, int target) {
		return CreateFactoryCommand (PatternFactoryCommandData.FACTORY_COMMAND_SHAPE_Q, start, target);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandShapeS(int start, int target) {
		return CreateFactoryCommand (PatternFactoryCommandData.FACTORY_COMMAND_SHAPE_S, start, target);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandShapeZ(int start, int target) {
		return CreateFactoryCommand (PatternFactoryCommandData.FACTORY_COMMAND_SHAPE_Z, start, target);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateFactoryCommandShapeV(int start, int target) {
		return CreateFactoryCommand (PatternFactoryCommandData.FACTORY_COMMAND_SHAPE_V, start, target);
	}
	
	/// <summary>
	/// Creates the straight.
	/// </summary>
	/// <param name="sensor">
	/// <para>センサー番号</para>
	/// <para>中円は0、内周は1～8、外周は9～16</para>
	/// </param>
	private static MaimaiScoreSlidePatternFactoryCommandData CreateStraight(int sensor, float start_x, float start_y, float target_x, float target_y) {
		var ret1 = new MaimaiScoreSlidePatternFactoryCommandData();
		var ret = new StraightPatternData ();
		ret.sensor = sensor;
		ret.start_x = start_x;
		ret.start_y = start_y;
		ret.target_x = target_x;
		ret.target_y = target_y;
		ret1.straight = ret;
		return ret1;
	}
	
	public static MaimaiScoreSlidePatternFactoryCommandData CreateOuterStraight(int button, float start_x, float start_y, float target_x, float target_y) {
		button = button - 1;
		while (button < 0) button += 8;
		button %= 8;
		int sensor = button + SlidePatternFactory.ACTION_ID_OUTER_BASE;
		return CreateStraight (sensor, start_x, start_y, target_x, target_y);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateInnerStraight(int button, float start_x, float start_y, float target_x, float target_y) {
		button = button - 1;
		while (button < 0) button += 8;
		button %= 8;
		int sensor = button + SlidePatternFactory.ACTION_ID_INNER_BASE;
		return CreateStraight (sensor, start_x, start_y, target_x, target_y);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateCenterStraight(float start_x, float start_y, float target_x, float target_y) {
		int sensor = SlidePatternFactory.ACTION_ID_CENTER;
		return CreateStraight (sensor, start_x, start_y, target_x, target_y);
	}
	public static void CalcContinuedStraightPosition(MaimaiScoreNoteData.TapData star, out float start_x, out float start_y) {
		if (star == null) {
			start_x = 0;
			start_y = 0;
		}
		else {
			int button = star.button;
			button -= 1;
			while (button < 0) button += 8;
			button %= 8;
			// 右手.
			var pos = CircleCalculator.PointOnCircle(Vector2.zero, 1, Constants.instance.GetPieceDegree(button));
			// 左手.
			pos = Constants.instance.ToLeftHandedCoordinateSystemPosition(pos);
			start_x = pos.x;
			start_y = pos.y;
		}
	}
	public static bool CalcContinuedStraightPosition(MaimaiScoreSlidePatternFactoryCommandData beforeCommand, out float start_x, out float start_y) {
		// 前回が直線で今回も直線なら前回の終点をそのまま今回の始点に持ってくる.
		if (beforeCommand.straight != null) {
			// 左手.
			start_x = beforeCommand.straight.target_x;
			start_y = beforeCommand.straight.target_y;
			return true;
		}
		// 前回が曲線で今回は直線なら前回の終点を算出して、今回の始点に持ってくる.
		else if (beforeCommand.curve != null) {
			// 右手.
			var pos = CircleCalculator.PointOnEllipse(beforeCommand.curve.center_x, beforeCommand.curve.center_y * Constants.instance.LEFT_HANDED_COORDINATE_SYSTEM, beforeCommand.curve.radius_x, beforeCommand.curve.radius_y, beforeCommand.curve.start_degree + beforeCommand.curve.distance_degree);
			// 左手.
			pos = Constants.instance.ToLeftHandedCoordinateSystemPosition(pos);
			start_x = pos.x;
			start_y = pos.y;
			return true;
		}
		// 前回がボタン基準のファクトリ任せならボタンの位置から前回の終点を算出する.
		else if (beforeCommand.factory != null) {
			Vector2 pos;
			if (beforeCommand.factory.target != 0) {
				int target = beforeCommand.factory.target - 1;
				while (target < 0) target += 8;
				target %= 8;
				float target_radius = 1;
				switch (beforeCommand.factory.command_type) {
				case PatternFactoryCommandData.FACTORY_COMMAND_OUTER_STRAIGHT_CENTER:
				case PatternFactoryCommandData.FACTORY_COMMAND_INNER_STRAIGHT_CENTER:
					target_radius = 0;
					break;
				case PatternFactoryCommandData.FACTORY_COMMAND_OUTER_STRAIGHT_INNER:
				case PatternFactoryCommandData.FACTORY_COMMAND_INNER_STRAIGHT_INNER:
				case PatternFactoryCommandData.FACTORY_COMMAND_CENTER_STRAIGHT_INNER:
				case PatternFactoryCommandData.FACTORY_COMMAND_INNER_CURVE_COUNTERCLOCKWISE:
				case PatternFactoryCommandData.FACTORY_COMMAND_INNER_CURVE_CLOCKWISE:
					target_radius = Constants.instance.MAIMAI_INNER_RADIUS / Constants.instance.MAIMAI_OUTER_RADIUS;
					break;
				}
				// 右手.
				pos = CircleCalculator.PointOnCircle(Vector2.zero, target_radius, Constants.instance.GetPieceDegree(target));
			}
			else {
				pos = Vector2.zero;
			}
			// 左手.
			pos = Constants.instance.ToLeftHandedCoordinateSystemPosition(pos);
			start_x = pos.x;
			start_y = pos.y;
			return true;
		}
		start_x = start_y = 0;
		return false;
	}
	private static MaimaiScoreSlidePatternFactoryCommandData CreateContinuedStraight(MaimaiScoreNoteData.TapData star, MaimaiScoreSlidePatternFactoryCommandData beforeCommand, int actionIdBase, int? button, float target_x, float target_y) {
		int _button = 0;
		if (button.HasValue) {
			_button = button.Value - 1;
			while (_button < 0) _button += 8;
			_button %= 8;
		}
		int sensor = _button + actionIdBase;
		float start_x, start_y;
		if (beforeCommand == null) {
			CalcContinuedStraightPosition (star, out start_x, out start_y);
		}
		else {
			if (!CalcContinuedStraightPosition (beforeCommand, out start_x, out start_y)) {
				return null;
			}
		}
		return CreateStraight (sensor, start_x, start_y, target_x, target_y);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateContinuedOuterStraight(MaimaiScoreNoteData.TapData star, MaimaiScoreSlidePatternFactoryCommandData beforeCommand, int button, float target_x, float target_y) {
		return CreateContinuedStraight (star, beforeCommand, SlidePatternFactory.ACTION_ID_OUTER_BASE, button, target_x, target_y);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateContinuedInnerStraight(MaimaiScoreNoteData.TapData star, MaimaiScoreSlidePatternFactoryCommandData beforeCommand, int button, float target_x, float target_y) {
		return CreateContinuedStraight (star, beforeCommand, SlidePatternFactory.ACTION_ID_INNER_BASE, button, target_x, target_y);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateContinuedCenterStraight(MaimaiScoreNoteData.TapData star, MaimaiScoreSlidePatternFactoryCommandData beforeCommand, float target_x, float target_y) {
		return CreateContinuedStraight (star, beforeCommand, SlidePatternFactory.ACTION_ID_CENTER, null, target_x, target_y);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData[] CreateAutomaticPartisionStraight(MaimaiScoreNoteData.TapData star, MaimaiScoreSlidePatternFactoryCommandData beforeCommand, float target_x, float target_y) {
		float start_x, start_y;
		if (beforeCommand == null) {
			CalcContinuedStraightPosition (star, out start_x, out start_y);
		}
		else {
			if (!CalcContinuedStraightPosition (beforeCommand, out start_x, out start_y)) {
				return null;
			}
		}
		return CreateAutomaticPartisionStraight (start_x, start_y, target_x, target_y);
	}
	// センサーごとに区切ったパターンコマンドを作る.
	public static MaimaiScoreSlidePatternFactoryCommandData[] CreateAutomaticPartisionStraight(float start_x, float start_y, float target_x, float target_y) {
		/*
		sensorは推測する.
		<一個分の大きさを進んでみて、一番近いセンサーを探す.
		前と同じセンサーなら前の終点をこれにする.
		距離を超えそうなら、最後の処理とする.
		*//*
		var ret = new List<MaimaiScoreSlidePatternFactoryCommandData>();
		float realRadius = Constants.instance.MAIMAI_OUTER_RADIUS;
		Vector2 startPos = Constants.instance.ToLeftHandedCoordinateSystemPosition(new Vector2 (start_x * realRadius, start_y * realRadius));
		Vector2 targetPos = Constants.instance.ToLeftHandedCoordinateSystemPosition(new Vector2 (target_x * realRadius, target_y * realRadius));
		float totalDistance = CircleCalculator.PointToPointDistance (startPos, targetPos);
		if (totalDistance < 0) {
			totalDistance *= -1;
			Debug.LogError ("totalDistance is minus");
		}
		float progress = 0;
		float workQuantity = Constants.instance.SLIDE_MARKER_TO_SENSOR_MARGIN; //一度に進む距離.
		bool finalCheck = false;
		while (!finalCheck) {
			if (progress >= totalDistance) {
				finalCheck = true;
				progress = totalDistance;
			}
			var nowTargetPos = CircleCalculator.PointToPointMovePoint(startPos, targetPos, progress);
			float nearestDistance = float.PositiveInfinity;
			int sensor = -1;
			// 中央.
			for (int i = 0; i < 1; i++) {
				var sensorPos = Vector2.zero;
				float toSensorDistance = Vector2.Distance (nowTargetPos, sensorPos);
				if (toSensorDistance < nearestDistance) {
					sensor = SlidePatternFactory.ACTION_ID_CENTER;
					nearestDistance = toSensorDistance;
				}
			}
			// 内周.
			for (int i = 0; i < 8; i++) {
				var sensorPos = Constants.instance.GetInnerPieceAxis(i);
				float toSensorDistance = Vector2.Distance (nowTargetPos, sensorPos);
				if (toSensorDistance < nearestDistance) {
					sensor = i + SlidePatternFactory.ACTION_ID_INNER_BASE;
					nearestDistance = toSensorDistance;
				}
			}
			// 外周.
			for (int i = 0; i < 8; i++) {
				var sensorPos = Constants.instance.GetOuterPieceAxis(i);
				float toSensorDistance = Vector2.Distance (nowTargetPos, sensorPos);
				if (toSensorDistance < nearestDistance) {
					sensor = i + SlidePatternFactory.ACTION_ID_OUTER_BASE;
					nearestDistance = toSensorDistance;
				}
			}

			var nowTargetPosNormalize = Constants.instance.ToLeftHandedCoordinateSystemPosition(new Vector2(nowTargetPos.x / realRadius, nowTargetPos.y / realRadius));
		
			if (ret.Count == 0) {
				var node = CreateStraight (sensor, start_x, start_y, start_x, start_y);
				ret.Add (node);
			}
			else if (ret[ret.Count - 1].straight.sensor != sensor) {
				float o_start_x, o_start_y;
				if (CalcContinuedStraightPosition (ret[ret.Count - 1], out o_start_x, out o_start_y)) {
					var node = CreateStraight (sensor, o_start_x, o_start_y, nowTargetPosNormalize.x, nowTargetPosNormalize.y);
					ret.Add (node);
				}
			}
			else if (finalCheck) {
				if (ret.Count == 1) {
					ret[ret.Count - 1] = CreateStraight (sensor, start_x, start_y, target_x, target_y);
				}
				else {
					float o_start_x, o_start_y;
					if (CalcContinuedStraightPosition (ret[ret.Count - 2], out o_start_x, out o_start_y)) {
						ret[ret.Count - 1] = CreateStraight (sensor, o_start_x, o_start_y, target_x, target_y);
					}
				}
			}
			else {
				if (ret.Count == 1) {
					ret[ret.Count - 1] = CreateStraight (ret[ret.Count - 1].straight.sensor, start_x, start_y, nowTargetPosNormalize.x, nowTargetPosNormalize.y);
				}
				else {
					float o_start_x, o_start_y;
					if (CalcContinuedStraightPosition (ret[ret.Count - 2], out o_start_x, out o_start_y)) {
						ret[ret.Count - 1] = CreateStraight (ret[ret.Count - 1].straight.sensor, o_start_x, o_start_y, nowTargetPosNormalize.x, nowTargetPosNormalize.y);
					}
				}
			}
			progress += workQuantity;
		}
		return ret.ToArray ();
	}
	
	/// <summary>
	/// Creates the curve.
	/// </summary>
	/// <param name="sensor">
	/// <para>センサー番号</para>
	/// <para>中円は0、内周は1～8、外周は9～16</para>
	/// </param>
	private static MaimaiScoreSlidePatternFactoryCommandData CreateCurve(int sensor, float center_x, float center_y, float radius_x, float radius_y, float start_degree, float distance_degree) {
		var ret1 = new MaimaiScoreSlidePatternFactoryCommandData();
		var ret = new CurvePatternData ();
		ret.sensor = sensor;
		ret.center_x = center_x;
		ret.center_y = center_y;
		ret.radius_x = radius_x;
		ret.radius_y = radius_y;
		ret.start_degree = start_degree;
		ret.distance_degree = distance_degree;
		ret1.curve = ret;
		return ret1;
	}
	
	public static MaimaiScoreSlidePatternFactoryCommandData CreateOuterCurve(int button, float center_x, float center_y, float radius_x, float radius_y, float start_degree, float distance_degree) {
		button = button - 1;
		while (button < 0) button += 8;
		button %= 8;
		int sensor = button + SlidePatternFactory.ACTION_ID_OUTER_BASE;
		return CreateCurve (sensor, center_x, center_y, radius_x, radius_y, start_degree, distance_degree);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateInnerCurve(int button, float center_x, float center_y, float radius_x, float radius_y, float start_degree, float distance_degree) {
		button = button - 1;
		while (button < 0) button += 8;
		button %= 8;
		int sensor = button + SlidePatternFactory.ACTION_ID_INNER_BASE;
		return CreateCurve (sensor, center_x, center_y, radius_x, radius_y, start_degree, distance_degree);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateCenterCurve(float center_x, float center_y, float radius_x, float radius_y, float start_degree, float distance_degree) {
		int sensor = SlidePatternFactory.ACTION_ID_CENTER;
		return CreateCurve (sensor, center_x, center_y, radius_x, radius_y, start_degree, distance_degree);
	}
	public static void CalcContinuedCurvePosition(MaimaiScoreNoteData.TapData star, float center_x, float center_y, out float radius_x, out float radius_y, out float start_degree) {
		Vector2 pos;
		// 右手.
		Vector2 center = Constants.instance.ToLeftHandedCoordinateSystemPosition (new Vector2 (center_x, center_y));
		if (star == null) {
			pos = Vector2.zero;
			float radius = CircleCalculator.PointToPointDistance (center, pos);
			radius_x = radius;
			radius_y = radius;
			start_degree = CircleCalculator.PointToDegree (center, pos);
		}
		else {
			int button = star.button;
			button -= 1;
			while (button < 0) button += 8;
			button %= 8;
			// 右手.
			pos = CircleCalculator.PointOnCircle (Vector2.zero, 1, Constants.instance.GetPieceDegree (button));
			float radius = CircleCalculator.PointToPointDistance (center, pos);
			radius_x = radius;
			radius_y = radius;
			start_degree = CircleCalculator.PointToDegree(center, pos);
		}
	}
	public static bool CalcContinuedCurvePosition(MaimaiScoreSlidePatternFactoryCommandData beforeCommand, float center_x, float center_y, out float radius_x, out float radius_y, out float start_degree) {
		// 前回が直線で今回は曲線なら、前回の終点と、今回の回転軸から、今回の半径と始点角度を決める.
		if (beforeCommand.straight != null) {
			// 右手.
			Vector2 center = Constants.instance.ToLeftHandedCoordinateSystemPosition(new Vector2(center_x, center_y));
			var pos = Constants.instance.ToLeftHandedCoordinateSystemPosition(new Vector2(beforeCommand.straight.target_x, beforeCommand.straight.target_y));
			float radius = CircleCalculator.PointToPointDistance(center, pos);
			radius_x = radius;
			radius_y = radius;
			start_degree = CircleCalculator.PointToDegree(center, pos);
			return true;
		}
		// 前回が曲線で今回も曲線なら、前回の回転軸・半径から求めた終点と、今回の回転軸から、今回の半径と始点角度を決める.
		else if (beforeCommand.curve != null) {
			// 右手.
			Vector2 center = Constants.instance.ToLeftHandedCoordinateSystemPosition(new Vector2(center_x, center_y));
			var pos = CircleCalculator.PointOnEllipse(beforeCommand.curve.center_x, beforeCommand.curve.center_y * Constants.instance.LEFT_HANDED_COORDINATE_SYSTEM, beforeCommand.curve.radius_x, beforeCommand.curve.radius_y, beforeCommand.curve.start_degree + beforeCommand.curve.distance_degree);
			float radius = CircleCalculator.PointToPointDistance(center, pos);
			radius_x = radius;
			radius_y = radius;
			start_degree = CircleCalculator.PointToDegree(center, pos);
			return true;
		}
		// 前回がボタン基準のファクトリ任せならボタンの位置から前回の終点を算出する.
		else if (beforeCommand.factory != null) {
			Vector2 pos;
			if (beforeCommand.factory.target != 0) {
				int target = beforeCommand.factory.target - 1;
				while (target < 0) target += 8;
				target %= 8;
				float target_radius = 1;
				switch (beforeCommand.factory.command_type) {
					case PatternFactoryCommandData.FACTORY_COMMAND_OUTER_STRAIGHT_CENTER:
					case PatternFactoryCommandData.FACTORY_COMMAND_INNER_STRAIGHT_CENTER:
						target_radius = 0;
						break;
					case PatternFactoryCommandData.FACTORY_COMMAND_OUTER_STRAIGHT_INNER:
					case PatternFactoryCommandData.FACTORY_COMMAND_INNER_STRAIGHT_INNER:
					case PatternFactoryCommandData.FACTORY_COMMAND_CENTER_STRAIGHT_INNER:
					case PatternFactoryCommandData.FACTORY_COMMAND_INNER_CURVE_COUNTERCLOCKWISE:
					case PatternFactoryCommandData.FACTORY_COMMAND_INNER_CURVE_CLOCKWISE:
					target_radius = Constants.instance.MAIMAI_INNER_RADIUS / Constants.instance.MAIMAI_OUTER_RADIUS;
					break;
				}
				// 右手.
				pos = CircleCalculator.PointOnCircle(Vector2.zero, target_radius, Constants.instance.GetPieceDegree(target));
			}
			else {
				pos = Vector2.zero;
			}
			Vector2 center = Constants.instance.ToLeftHandedCoordinateSystemPosition(new Vector2(center_x, center_y));
			float radius = CircleCalculator.PointToPointDistance(center, pos);
			radius_x = radius;
			radius_y = radius;
			start_degree = CircleCalculator.PointToDegree(center, pos);
			return true;
		}
		radius_x = radius_y = start_degree = 0;
		return false;
	}
	private static MaimaiScoreSlidePatternFactoryCommandData CreateContinuedCurve(MaimaiScoreNoteData.TapData star, MaimaiScoreSlidePatternFactoryCommandData beforeCommand, int actionIdBase, int? button, float center_x, float center_y, float distance_degree) {
		int _button = 0;
		if (button.HasValue) {
			_button = button.Value - 1;
			while (_button < 0) _button += 8;
			_button %= 8;
		}
		int sensor = _button + actionIdBase;
		float radius_x, radius_y, start_degree;
		if (beforeCommand == null) {
			CalcContinuedCurvePosition (star, center_x, center_y, out radius_x, out radius_y, out start_degree);
		}
		else {
			if (!CalcContinuedCurvePosition (beforeCommand, center_x, center_y, out radius_x, out radius_y, out start_degree)) {
				return null;
			}
		}
		return CreateCurve (sensor, center_x, center_y, radius_x, radius_y, start_degree, distance_degree);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateContinuedOuterCurve(MaimaiScoreNoteData.TapData star, MaimaiScoreSlidePatternFactoryCommandData beforeCommand, int button, float center_x, float center_y, float distance_degree) {
		return CreateContinuedCurve (star, beforeCommand, SlidePatternFactory.ACTION_ID_OUTER_BASE, button, center_x, center_y, distance_degree);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateContinuedInnerCurve(MaimaiScoreNoteData.TapData star, MaimaiScoreSlidePatternFactoryCommandData beforeCommand, int button, float center_x, float center_y, float distance_degree) {
		return CreateContinuedCurve (star, beforeCommand, SlidePatternFactory.ACTION_ID_INNER_BASE, button, center_x, center_y, distance_degree);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData CreateContinuedCenterCurve(MaimaiScoreNoteData.TapData star, MaimaiScoreSlidePatternFactoryCommandData beforeCommand, float center_x, float center_y, float distance_degree) {
		return CreateContinuedCurve (star, beforeCommand, SlidePatternFactory.ACTION_ID_CENTER, null, center_x, center_y, distance_degree);
	}
	public static MaimaiScoreSlidePatternFactoryCommandData[] CreateAutomaticPartisionCurve(MaimaiScoreNoteData.TapData star, MaimaiScoreSlidePatternFactoryCommandData beforeCommand, float center_x, float center_y, float distance_degree) {
		float radius_x, radius_y, start_degree;
		if (beforeCommand == null) {
			CalcContinuedCurvePosition (star, center_x, center_y, out radius_x, out radius_y, out start_degree);
		}
		else {
			if (!CalcContinuedCurvePosition (beforeCommand, center_x, center_y, out radius_x, out radius_y, out start_degree)) {
				return null;
			}
		}
		return CreateAutomaticPartisionCurve (center_x, center_y, radius_x, radius_y, start_degree, distance_degree);
	}
	// センサーごとに区切ったパターンコマンドを作る.
	public static MaimaiScoreSlidePatternFactoryCommandData[] CreateAutomaticPartisionCurve(float center_x, float center_y, float radius_x, float radius_y, float start_degree, float distance_degree) {
		if (distance_degree == 0) {
			return new MaimaiScoreSlidePatternFactoryCommandData[0];
		}
		/*
		sensorは推測する.
		<一個分の大きさを進んでみて、一番近いセンサーを探す.
		前と同じセンサーなら前の終点をこれにする.
		距離を超えそうなら、最後の処理とする.
		*//*
		var ret = new List<MaimaiScoreSlidePatternFactoryCommandData>();
		float realRadius = Constants.instance.MAIMAI_OUTER_RADIUS;
		var centerPos = Constants.instance.ToLeftHandedCoordinateSystemPosition(new Vector2 (center_x * realRadius, center_y * realRadius));
		var radiusReal = new Vector2 (radius_x * realRadius, radius_y * realRadius);
		float radiusRealInterim = Mathf.Max(radiusReal.x, radiusReal.y);
		float totalDistance = CircleCalculator.ArcDistance(radiusRealInterim, Mathf.Abs(distance_degree));
		float vec = distance_degree < 0 ? -1 : 1;
		float startDegree = 0;
		float progress = 0;
		float workQuantity = Constants.instance.SLIDE_MARKER_TO_SENSOR_MARGIN; //一度に進む距離.
		bool finalCheck = false;
		while (!finalCheck) {
			if (progress >= totalDistance) {
				finalCheck = true;
				progress = totalDistance;
			}
			var nowProgressDegree = CircleCalculator.ArcDegreeFromArcDistance(radiusRealInterim, progress * vec);
			var nowDistanceDegree = nowProgressDegree - startDegree;
			var nowTargetPos = CircleCalculator.PointOnEllipse(centerPos, radiusReal, start_degree + nowProgressDegree);
			float nearestDistance = float.PositiveInfinity;
			int sensor = -1;
			// 中央.
			for (int i = 0; i < 1; i++) {
				var sensorPos = Vector2.zero;
				float toSensorDistance = Vector2.Distance (nowTargetPos, sensorPos);
				if (toSensorDistance < nearestDistance) {
					sensor = SlidePatternFactory.ACTION_ID_CENTER;
					nearestDistance = toSensorDistance;
				}
			}
			// 内周.
			for (int i = 0; i < 8; i++) {
				var sensorPos = Constants.instance.GetInnerPieceAxis(i);
				float toSensorDistance = Vector2.Distance (nowTargetPos, sensorPos);
				if (toSensorDistance < nearestDistance) {
					sensor = i + SlidePatternFactory.ACTION_ID_INNER_BASE;
					nearestDistance = toSensorDistance;
				}
			}
			// 外周.
			for (int i = 0; i < 8; i++) {
				var sensorPos = Constants.instance.GetOuterPieceAxis(i);
				float toSensorDistance = Vector2.Distance (nowTargetPos, sensorPos);
				if (toSensorDistance < nearestDistance) {
					sensor = i + SlidePatternFactory.ACTION_ID_OUTER_BASE;
					nearestDistance = toSensorDistance;
				}
			}
		
			if (ret.Count == 0) {
				var node = CreateCurve (sensor, center_x, center_y, radius_x, radius_y, start_degree, 0);
				ret.Add (node);
			}
			else if (ret[ret.Count - 1].curve.sensor != sensor) {
				startDegree = nowProgressDegree;
				float o_radius_x, o_radius_y, o_start_degree;
				if (CalcContinuedCurvePosition (ret[ret.Count - 1], center_x, center_y, out o_radius_x, out o_radius_y, out o_start_degree)) {
					var node = CreateCurve (sensor, center_x, center_y, o_radius_x, o_radius_y, o_start_degree, nowDistanceDegree);
					ret.Add (node);
				}
			}
			else if (finalCheck) {
				if (ret.Count == 1) {
					ret[ret.Count - 1] = CreateCurve (sensor, center_x, center_y, radius_x, radius_y, start_degree, distance_degree);
				}
				else {
					float o_radius_x, o_radius_y, o_start_degree;
					if (CalcContinuedCurvePosition (ret[ret.Count - 2], center_x, center_y, out o_radius_x, out o_radius_y, out o_start_degree)) {
						float total = 0;
						for (int j = 0; j < ret.Count - 1; j++) {
							total += ret[j].curve.distance_degree;
						}
						ret[ret.Count - 1] = CreateCurve (sensor, center_x, center_y, o_radius_x, o_radius_y, o_start_degree, distance_degree - total);
					}
				}
			}
			else {
				if (ret.Count == 1) {
					ret[ret.Count - 1] = CreateCurve (ret[ret.Count - 1].curve.sensor, center_x, center_y, radius_x, radius_y, start_degree, nowDistanceDegree);
				}
				else {
					float o_radius_x, o_radius_y, o_start_degree;
					if (CalcContinuedCurvePosition (ret[ret.Count - 2], center_x, center_y, out o_radius_x, out o_radius_y, out o_start_degree)) {
						ret[ret.Count - 1] = CreateCurve (ret[ret.Count - 1].curve.sensor, center_x, center_y, o_radius_x, o_radius_y, o_start_degree, nowDistanceDegree);
					}
				}
			}
			progress += workQuantity;
		}
		return ret.ToArray ();
	}
}

*/