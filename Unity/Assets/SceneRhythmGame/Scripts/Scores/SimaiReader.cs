using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SimaiReader : MaisqScriptReader {

	protected enum ExpandRound {
		GENERAL,
		AA,AB,AC,BA,BB,BC,CA,CB,CC,
	}
	
	private Dictionary<ExpandRound, Dictionary<string, MaisqScriptUserDefine>> _expandDefine;
	protected Dictionary<ExpandRound, Dictionary<string, MaisqScriptUserDefine>> expandDefine {
		get {
			if (_expandDefine == null) {
				_expandDefine = new Dictionary<ExpandRound, Dictionary<string, MaisqScriptUserDefine>>();
				_expandDefine.Add (ExpandRound.GENERAL, new Dictionary<string, MaisqScriptUserDefine>());
				_expandDefine.Add (ExpandRound.AA, new Dictionary<string, MaisqScriptUserDefine>());
				_expandDefine.Add (ExpandRound.AB, new Dictionary<string, MaisqScriptUserDefine>());
				_expandDefine.Add (ExpandRound.AC, new Dictionary<string, MaisqScriptUserDefine>());
				_expandDefine.Add (ExpandRound.BA, new Dictionary<string, MaisqScriptUserDefine>());
				_expandDefine.Add (ExpandRound.BB, new Dictionary<string, MaisqScriptUserDefine>());
				_expandDefine.Add (ExpandRound.BC, new Dictionary<string, MaisqScriptUserDefine>());
				_expandDefine.Add (ExpandRound.CA, new Dictionary<string, MaisqScriptUserDefine>());
				_expandDefine.Add (ExpandRound.CB, new Dictionary<string, MaisqScriptUserDefine>());
				_expandDefine.Add (ExpandRound.CC, new Dictionary<string, MaisqScriptUserDefine>());
			}
			return _expandDefine;
		}
	}
	
	protected virtual bool ReadCommandSimaiExpand(MaisqScriptTree tree) {
		var switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.SIMAI_EXPAND:
		{
			var expand = MaisqScriptUserDefine.FromMaiScriptTree (tree);
			if (expand != null) {
				expandDefine[ExpandRound.GENERAL][expand.name] = expand;
				return true;
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SIMAI_EXPAND_AA:
		{
			var expand = MaisqScriptUserDefine.FromMaiScriptTree (tree);
			if (expand != null) {
				expandDefine[ExpandRound.AA][expand.name] = expand;
				return true;
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SIMAI_EXPAND_AB:
		{
			var expand = MaisqScriptUserDefine.FromMaiScriptTree (tree);
			if (expand != null) {
				expandDefine[ExpandRound.AB][expand.name] = expand;
				return true;
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SIMAI_EXPAND_AC:
		{
			var expand = MaisqScriptUserDefine.FromMaiScriptTree (tree);
			if (expand != null) {
				expandDefine[ExpandRound.AC][expand.name] = expand;
				return true;
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SIMAI_EXPAND_BA:
		{
			var expand = MaisqScriptUserDefine.FromMaiScriptTree (tree);
			if (expand != null) {
				expandDefine[ExpandRound.BA][expand.name] = expand;
				return true;
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SIMAI_EXPAND_BB:
		{
			var expand = MaisqScriptUserDefine.FromMaiScriptTree (tree);
			if (expand != null) {
				expandDefine[ExpandRound.BB][expand.name] = expand;
				return true;
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SIMAI_EXPAND_BC:
		{
			var expand = MaisqScriptUserDefine.FromMaiScriptTree (tree);
			if (expand != null) {
				expandDefine[ExpandRound.BC][expand.name] = expand;
				return true;
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SIMAI_EXPAND_CA:
		{
			var expand = MaisqScriptUserDefine.FromMaiScriptTree (tree);
			if (expand != null) {
				expandDefine[ExpandRound.CA][expand.name] = expand;
				return true;
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SIMAI_EXPAND_CB:
		{
			var expand = MaisqScriptUserDefine.FromMaiScriptTree (tree);
			if (expand != null) {
				expandDefine[ExpandRound.CB][expand.name] = expand;
				return true;
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SIMAI_EXPAND_CC:
		{
			var expand = MaisqScriptUserDefine.FromMaiScriptTree (tree);
			if (expand != null) {
				expandDefine[ExpandRound.CC][expand.name] = expand;
				return true;
			}
			break;
		}
		}
		return false;
	}

	public string ToMaiScript(string simaiScript)  {
		string text1 = RemoveComment(simaiScript, true);
		string text = text1.Replace ("\n", "");
		
		string maiScript = "";
		List<string> buttons = new List<string>();
		List<string> notetypes = new List<string>();
		List<string> bpms = new List<string> ();
		List<string> beats = new List<string> ();
		List<string> notes = new List<string> ();
		string wholeStep = "";
		string beat = "";
		string length = "";
		string interval = "";
		string localBpm = "";
		string waitLocalBpm = "";
		string waitInterval = "";
		int restCount = 0;
		System.Action addRest = ()=> {
			if (restCount > 0) {
				maiScript += MaisqScore.CommandNameDocument.REST + "(" + (restCount > 1 ? restCount.ToString() : string.Empty) + ");\n";
				restCount = 0;
			}
		};
		System.Action addBpms = () => {
			foreach (var newbpm in bpms) {
				maiScript += newbpm;
			}
			bpms.Clear();
		};
		System.Action addBeats = () => {
			foreach (var newbeat in beats) {
				maiScript += newbeat;
			}
			beats.Clear();
		};
		System.Action addNotes = () => {
			foreach (var newnote in notes) {
				maiScript += newnote;
			}
			notes.Clear ();
		};
		// ()
		bool roundBracket = false;
		// {}
		bool curlyBracket = false;
		// []
		bool squareBracket = false;
		// / each
		bool slash = false;
		// # direct input
		bool numbersign = false;
		// {#}
		bool curlyBracketInNumbersign = false;
		// {:}
		bool curlyBracketInColon = false;
		// [#]
		bool squareBracketInNumbersign = false;
		// [()]
		bool squareBracketInRoundBracket = false;
		// [:]
		bool squareBracketInColon = false;
		// [/]
		bool squareBracketInSlash = false;
		// [/#]
		bool squareBracketInSlashAfterNumbersign = false;
		// *
		string head = null;
		
		string readTank = "";
		bool readStringDoubleQuote = false;
		bool readStringSingleQuote = false;
		for (int a = 0; a < text.Length; a++) {
			string subSingle = text.Substring (a, 1);
			string subMulti = "";
			if (a + 1 < text.Length)
				subMulti = text.Substring (a, 2);
			// 文字列読み込みフラグ切り替え.
			if (!readStringSingleQuote && subSingle == "\"") {
				readStringDoubleQuote = !readStringDoubleQuote;
			}
			else if (!readStringDoubleQuote && subSingle == "\'") {
				readStringSingleQuote = !readStringSingleQuote;
			}
			// "文字列"を読み込むとき.
			else if (readStringDoubleQuote || readStringSingleQuote) {
				if (subMulti == "\\\\") { //simaiスクリプトの\\をC#の\\に変える.
					readTank += "\\";
					a++;
				}
				else if (readStringDoubleQuote && subMulti == "\\\"") { //maiスクリプトの\"をC#の\"に変える.
					readTank += "\"";
					a++;
				}
				else if (readStringSingleQuote && subMulti == "\\n") { //maiスクリプトの\nをC#の\nに変える.
					readTank += "\n";
					a++;
				}
				else {
					readTank += subSingle;
				}
			}
			// "文字列"でないとき.
			else {
				if (subSingle == "(" && !roundBracket && !curlyBracket && !squareBracket && !numbersign) {
					roundBracket = true;
				}
				else if (subSingle == ")" && roundBracket && !curlyBracket && !squareBracket) {
					// SetBpm.
					addRest ();
					bpms.Add (MaisqScore.CommandNameDocument.BPM + "(" + readTank + ");\n");
					roundBracket = false;
					readTank = "";
				}
				else if (subSingle == "{" && !roundBracket && !curlyBracket && !squareBracket && !numbersign && !curlyBracketInColon) {
					curlyBracket = true;
				}
				else if (subSingle == "#" && !roundBracket && curlyBracket && !squareBracket && !numbersign && !curlyBracketInColon) {
					curlyBracketInNumbersign = true;
				}
				else if (subSingle == ":" && !roundBracket && curlyBracket && !squareBracket && !curlyBracketInColon) {
					curlyBracketInColon = true;
					wholeStep = readTank;
					readTank = "";
				}
				else if (subSingle == "}" && !roundBracket && curlyBracket && !squareBracket) {
					// SetBeat.
					if (!curlyBracketInNumbersign) {
						addRest ();
						if (!string.IsNullOrEmpty(wholeStep)) wholeStep += ", ";
						beats.Add (MaisqScore.CommandNameDocument.BEAT + "(" + wholeStep + readTank + ");\n");
					}
					else {
						addRest ();
						if (!string.IsNullOrEmpty(wholeStep)) wholeStep += ", ";
						beats.Add (MaisqScore.CommandNameDocument.INTERVAL + "(" + wholeStep + readTank + ");\n");
					}
					curlyBracket = false;
					curlyBracketInNumbersign = false;
					wholeStep = "";
					readTank = "";
				}
				else if (subSingle == "#" && !roundBracket && !curlyBracket && !squareBracket && buttons.Count == 0 && notetypes.Count == 0) {
					numbersign = true;
				}
				else if (IsButtonNumber (subSingle) && !roundBracket && !curlyBracket && !squareBracket && !numbersign) {
					buttons.Add (subSingle);
				}
				else if (IsNoteType (subMulti) && !roundBracket && !curlyBracket && !squareBracket) {
					notetypes.Add (subMulti);
					a++;
				}
				else if (IsNoteType (subSingle) && !roundBracket && !curlyBracket && !squareBracket) {
					notetypes.Add (subSingle);
				}
				// この辺にスライド直接入力の処理を書く.
				else if (IsDirectSlideSensor (subSingle) && !roundBracket && !curlyBracket && !squareBracket && numbersign) {
					if (subSingle == "A" || subSingle == "B") {
						if (subMulti.Length == 2 && IsButtonNumber (subMulti.Substring (1, 1))) {
							buttons.Add (subMulti);
							a++;
						}
					}
					else {
						buttons.Add (subSingle);
					}
				}
				else if (subSingle == "[" && !roundBracket && !curlyBracket && !squareBracket) {
					squareBracket = true;
				}
				else if (subSingle == "#" && !roundBracket && !curlyBracket && squareBracket && !squareBracketInSlash && !squareBracketInSlashAfterNumbersign) {
					squareBracketInNumbersign = true;
				}
				else if (subSingle == "/" && !roundBracket && !curlyBracket && squareBracket && !squareBracketInNumbersign && !squareBracketInColon) {
					squareBracketInSlash = true;
					waitLocalBpm = readTank;
					readTank = "";
				}
				else if (subSingle == "/" && !roundBracket && !curlyBracket && squareBracket && squareBracketInNumbersign && !squareBracketInColon) {
					squareBracketInSlash = true;
					waitInterval = readTank;
					readTank = "";
				}
				else if (subSingle == "#" && !roundBracket && !curlyBracket && squareBracket && squareBracketInSlash && !squareBracketInSlashAfterNumbersign) {
					squareBracketInSlashAfterNumbersign = true;
				}
				else if (subSingle == "(" && !roundBracket && !curlyBracket && squareBracket && !squareBracketInRoundBracket) {
					squareBracketInRoundBracket = true;
				}
				else if (subSingle == ")" && squareBracketInRoundBracket) {
					localBpm = readTank;
					squareBracketInRoundBracket = false;
					readTank = "";
				}
				else if (subSingle == ":" && !roundBracket && !curlyBracket && squareBracket && (!squareBracketInNumbersign || (squareBracketInNumbersign && squareBracketInSlash)) && !squareBracketInRoundBracket && !squareBracketInSlashAfterNumbersign) {
					squareBracketInColon = true;
					beat = readTank;
					readTank = "";
				}
				else if (subSingle == "]" && !roundBracket && !curlyBracket && squareBracket && !squareBracketInRoundBracket && (!squareBracketInNumbersign || (squareBracketInNumbersign && squareBracketInSlash)) && !squareBracketInSlashAfterNumbersign) {
					length = readTank;
					squareBracket = false;
					squareBracketInNumbersign = false;
					squareBracketInRoundBracket = false;
					squareBracketInColon = false;
					squareBracketInSlash = false;
					squareBracketInSlashAfterNumbersign = false;
					readTank = "";
				}
				else if (subSingle == "]" && !roundBracket && !curlyBracket && squareBracket && !squareBracketInRoundBracket && (squareBracketInNumbersign || squareBracketInSlashAfterNumbersign)) {
					interval = readTank;
					squareBracket = false;
					squareBracketInNumbersign = false;
					squareBracketInRoundBracket = false;
					squareBracketInColon = false;
					squareBracketInSlash = false;
					squareBracketInSlashAfterNumbersign = false;
					readTank = "";
				}
				else if (subSingle == "," && !roundBracket && !curlyBracket && !squareBracket && numbersign && buttons.Count == 0 && notetypes.Count == 0 && !slash) {
					var script = RemoveComment (readTank, false);
					var trees = MaisqScriptTree.CreateTreeFromMaiScript(script);
					bool expanded = false;
					foreach (var tree in trees) {
						if (ReadCommandSimaiExpand (tree)) {
							expanded = true;
						}
					}
					if (!expanded) {
						// maiScript埋め込み機能.
						addBpms ();
						addBeats ();
						addRest ();
						maiScript += readTank + "\n";
					}
					numbersign = false;
					readTank = "";
				}
				else if ((subSingle == "," || (subSingle == "/" && !squareBracketInSlash) || subSingle == "*") && !roundBracket && !curlyBracket && !squareBracket) {
					bool keySlash = subSingle == "/";
					bool keyCamma = subSingle == ",";
					bool keyAster = subSingle == "*";
					
					if (buttons.Count == 0) {
						if (keyCamma) {
							addBpms ();
							addBeats ();
							if (slash) {
								notes.Add (");\n");
								addNotes ();
							}
							else {
								restCount++;
							}
						}
					}
					else {
						string notetype = "";
						if (notetypes.Count > 0)
							notetype = notetypes[notetypes.Count - 1];
						
						string additionScript = "";
						string additionScript2 = "";
						bool creatable = false;
						
						if (head == null) {
							if (!slash)
								additionScript += MaisqScore.CommandNameDocument.NOTE + "(";
							else
								additionScript += ", ";
						}
						
						if (notetype == "") {
							int max = buttons.Count;
							for (int i = 0; i < max; i++) {
								additionScript2 += MaisqScore.CommandNameDocument.TAP + "(" + buttons[i] + ")";
								if (i + 1 < max)
									additionScript2 += ", ";
							}
							creatable = true;
						}
						else if (notetype == "h") {
							string button = buttons[buttons.Count - 1];
							if (localBpm != "" && beat != "" && length != "")
								additionScript2 += MaisqScore.CommandNameDocument.HOLD + "(" + button + ", " + MaisqScore.CommandNameDocument.STEP + "(" + localBpm + ", " + beat + ", " + length + "))";
							else if (beat != "" && length != "")
								additionScript2 += MaisqScore.CommandNameDocument.HOLD + "(" + button + ", " + MaisqScore.CommandNameDocument.STEP + "(" + beat + ", " + length + "))";
							else if (interval != "")
								additionScript2 += MaisqScore.CommandNameDocument.HOLD + "(" + button + ", " + MaisqScore.CommandNameDocument.STEP + "(" + interval + "))";
							creatable = true;
						}
						else if (IsNoteTypeSlide (notetype) && (buttons.Count >= 2 || (head != null && buttons.Count >= 1))) {
							int isBreakStar = notetypes[0] == "b" ? 1 : 0; // ノートタイプ詰所の先頭がbなら赤星.
							string slideHeadCommand = MaisqScore.CommandNameDocument.STAR;
							string slideHeadCommandOpen = "(";
							string slideHeadCommandClose = ")";
							if (isBreakStar != 0)
								slideHeadCommand = MaisqScore.CommandNameDocument.BREAK_STAR;
							string star = buttons[0];
							if (star.Length == 2 || star == "C") {
								slideHeadCommand = string.Empty;
								star = string.Empty;
								slideHeadCommandOpen = string.Empty;
								slideHeadCommandClose = string.Empty;
							}
							string additionScript3 = string.Empty;
							if (head == null) {
								if (waitLocalBpm != "" && localBpm != "" && beat != "" && length != "")
									additionScript2 += MaisqScore.CommandNameDocument.SLIDE + "(" + slideHeadCommand + slideHeadCommandOpen + star + slideHeadCommandClose + ", " + MaisqScore.CommandNameDocument.WAIT_CUSTOM_LOCAL_BPM + "(" + waitLocalBpm + "), " + MaisqScore.CommandNameDocument.STEP + "(" + localBpm + ", " + beat + ", " + length + ")";
								else if (waitLocalBpm != "" && beat != "" && length != "")
									additionScript2 += MaisqScore.CommandNameDocument.SLIDE + "(" + slideHeadCommand + slideHeadCommandOpen + star + slideHeadCommandClose + ", " + MaisqScore.CommandNameDocument.WAIT_CUSTOM_LOCAL_BPM + "(" + waitLocalBpm + "), " + MaisqScore.CommandNameDocument.STEP + "(" + beat + ", " + length + ")";
								else if (waitLocalBpm != "" && interval != "")
									additionScript2 += MaisqScore.CommandNameDocument.SLIDE + "(" + slideHeadCommand + slideHeadCommandOpen + star + slideHeadCommandClose + ", " + MaisqScore.CommandNameDocument.WAIT_CUSTOM_LOCAL_BPM + "(" + waitLocalBpm + "), " + MaisqScore.CommandNameDocument.STEP + "(" + interval + ")";
								else if (waitInterval != "" && localBpm != "" && beat != "" && length != "")
									additionScript2 += MaisqScore.CommandNameDocument.SLIDE + "(" + slideHeadCommand + slideHeadCommandOpen + star + slideHeadCommandClose + ", " + MaisqScore.CommandNameDocument.WAIT_CUSTOM_INTERVAL + "(" + waitInterval + "), " + MaisqScore.CommandNameDocument.STEP + "(" + localBpm + ", " + beat + ", " + length + ")";
								else if (waitInterval != "" && beat != "" && length != "")
									additionScript2 += MaisqScore.CommandNameDocument.SLIDE + "(" + slideHeadCommand + slideHeadCommandOpen + star + slideHeadCommandClose + ", " + MaisqScore.CommandNameDocument.WAIT_CUSTOM_INTERVAL + "(" + waitInterval + "), " + MaisqScore.CommandNameDocument.STEP + "(" + beat + ", " + length + ")";
								else if (waitInterval != "" && interval != "")
									additionScript2 += MaisqScore.CommandNameDocument.SLIDE + "(" + slideHeadCommand + slideHeadCommandOpen + star + slideHeadCommandClose + ", " + MaisqScore.CommandNameDocument.WAIT_CUSTOM_INTERVAL + "(" + waitInterval + "), " + MaisqScore.CommandNameDocument.STEP + "(" + interval + ")";
								else if (localBpm != "" && beat != "" && length != "")
									additionScript2 += MaisqScore.CommandNameDocument.SLIDE + "(" + slideHeadCommand + slideHeadCommandOpen + star + slideHeadCommandClose + ", " + MaisqScore.CommandNameDocument.STEP + "(" + localBpm + ", " + beat + ", " + length + ")";
								else if (beat != "" && length != "")
									additionScript2 += MaisqScore.CommandNameDocument.SLIDE + "(" + slideHeadCommand + slideHeadCommandOpen + star + slideHeadCommandClose + ", " + MaisqScore.CommandNameDocument.STEP + "(" + beat + ", " + length + ")";
								else if (interval != "")
									additionScript2 += MaisqScore.CommandNameDocument.SLIDE + "(" + slideHeadCommand + slideHeadCommandOpen + star + slideHeadCommandClose + ", " + MaisqScore.CommandNameDocument.STEP + "(" + interval + ")";
							}
							else {
								if (waitLocalBpm != "" && localBpm != "" && beat != "" && length != "")
									additionScript3 += MaisqScore.CommandNameDocument.WAIT_CUSTOM_LOCAL_BPM + "(" + waitLocalBpm + "), " + MaisqScore.CommandNameDocument.STEP + "(" + localBpm + ", " + beat + ", " + length + ")";
								else if (waitLocalBpm != "" && beat != "" && length != "")
									additionScript3 += MaisqScore.CommandNameDocument.WAIT_CUSTOM_LOCAL_BPM + "(" + waitLocalBpm + "), " + MaisqScore.CommandNameDocument.STEP + "(" + beat + ", " + length + ")";
								else if (waitLocalBpm != "" && interval != "")
									additionScript3 += MaisqScore.CommandNameDocument.WAIT_CUSTOM_LOCAL_BPM + "(" + waitLocalBpm + "), " + MaisqScore.CommandNameDocument.STEP + "(" + interval + ")";
								else if (waitInterval != "" && localBpm != "" && beat != "" && length != "")
									additionScript3 += MaisqScore.CommandNameDocument.WAIT_CUSTOM_INTERVAL + "(" + waitInterval + "), " + MaisqScore.CommandNameDocument.STEP + "(" + localBpm + ", " + beat + ", " + length + ")";
								else if (waitInterval != "" && beat != "" && length != "")
									additionScript3 += MaisqScore.CommandNameDocument.WAIT_CUSTOM_INTERVAL + "(" + waitInterval + "), " + MaisqScore.CommandNameDocument.STEP + "(" + beat + ", " + length + ")";
								else if (waitInterval != "" && interval != "")
									additionScript3 += MaisqScore.CommandNameDocument.WAIT_CUSTOM_INTERVAL + "(" + waitInterval + "), " + MaisqScore.CommandNameDocument.STEP + "(" + interval + ")";
								else if (localBpm != "" && beat != "" && length != "")
									additionScript3 += MaisqScore.CommandNameDocument.STEP + "(" + localBpm + ", " + beat + ", " + length + ")";
								else if (beat != "" && length != "")
									additionScript3 += MaisqScore.CommandNameDocument.STEP + "(" + beat + ", " + length + ")";
								else if (interval != "")
									additionScript3 += MaisqScore.CommandNameDocument.STEP + "(" + interval + ")";
							}
							if (additionScript2 != "" || additionScript3 != "") {
								additionScript2 += ", pattern(";
								if (additionScript3 != "")
									additionScript2 += additionScript3 + ", ";
								additionScript2 += "chain(";
								string start;
								string target;
								string start_rd;
								string target_rd;
								int noteTypesSize = notetypes.Count - isBreakStar;
								int isMultiPattern = head != null ? -1 : 0;
								for (int i = 0; i < noteTypesSize; i++) {
									start = "";
									target = "";
									start_rd = "";
									target_rd = "";
									notetype = notetypes[i + isBreakStar];
									if (i == 0 && head != null) {
										start = head;
									}
									else if (i + isMultiPattern < buttons.Count) {
										start = buttons[i + isMultiPattern];
										if (i == 0 && head == null) {
											head = start;
										}
									}
									if (i + 1 + isMultiPattern < buttons.Count)
										target = buttons[i + 1 + isMultiPattern];
									
									if (start != "" && target != "") {
										if (start.Length == 2) {
											start_rd = start.Substring (0, 1);
											start = start.Substring (1, 1);
										}
										else if (start == "C") {
											start_rd = start;
										}
										else {
											string beforeTarget;
											for (int j = 0; j < buttons.Count; j++) {
												if (i + isMultiPattern >= j) {
													beforeTarget = buttons[i + isMultiPattern - j];
													if (beforeTarget.Length == 2) {
														start_rd = beforeTarget.Substring (0, 1);
														break;
													}
												}
												else {
													start_rd = "A";
												}
											}
										}
										if (target.Length == 2) {
											target_rd = target.Substring (0, 1);
											target = target.Substring (1, 1);
										}
										else if (target == "C") {
											target_rd = target;
										}
										else {
											string beforeStart;
											for (int j = 0; j < buttons.Count; j++) {
												if (i + isMultiPattern >= j) {
													beforeStart = buttons[i + isMultiPattern - j];
													if (beforeStart.Length == 2) {
														target_rd = beforeStart.Substring (0, 1);
														break;
													}
												}
												else {
													target_rd = "A";
												}
											}
										}
										
										string rd1, rd2;
										if (start_rd == "A")
											rd1 = "outer";
										else if (start_rd == "B")
											rd1 = "inner";
										else
											rd1 = "center";
										if (target_rd == "A")
											rd2 = "outer";
										else if (target_rd == "B")
											rd2 = "inner";
										else
											rd2 = "center";
										
										if (notetype == "-") {
											additionScript2 += rd1 + "_straight_" + rd2 + "(";
											if (start_rd == "C")
												additionScript2 += target + ")";
											else if (target_rd == "C")
												additionScript2 += start + ")";
											else
												additionScript2 += start + ", " + target + ")";
										}
										else if (notetype == ">" || notetype == "<") {
											if (start_rd != "C") {
												additionScript2 += rd1 + "_" + LtGtToCurveSlideMaiScript (notetype, start) + "_axis_center" + "(" + start + ", " + target + ")";
											}
											else {
												additionScript2 += rd1 + "_straight_" + rd2 + "(" + target + ")";
											}
										}
										else if (notetype == "p" || notetype == "q" || notetype == "v") {
											if (start_rd != "C") {
												additionScript2 += "shape_" + notetype + "_axis_center" + "(" + start + ", " + target + ")";
											}
											else {
												additionScript2 += rd1 + "_straight_" + rd2 + "(" + target + ")";
											}
										}
										else if (notetype == "s" || notetype == "z" || notetype == "pp" || notetype == "qq") {
											if (start_rd != "C") {
												additionScript2 += "shape_" + notetype + "(" + start + ", " + target + ")";
											}
											else {
												additionScript2 += rd1 + "_straight_" + rd2 + "(" + target + ")";
											}
										}
										else if (notetype == "V1" || notetype == "V2" || notetype == "V3" || notetype == "V4" || notetype == "V5" || notetype == "V6" || notetype == "V7" || notetype == "V8") {
											if (start_rd != "C") {
												additionScript2 += "shape_" + notetype.Substring (0, 1).ToLower () + "_axis_outer" + "(" + start + ", " + notetype.Substring (1, 1) + ", " + target + ")";
											}
											else {
												additionScript2 += rd1 + "_straight_" + rd2 + "(" + target + ")";
											}
										}
										else if (notetype == "w") {
											int tb;
											if (int.TryParse (target, out tb)) {
												while (tb < 0)
													tb += 8;
												additionScript2 += rd1 + "_straight_" + rd2 + "(" + start + ", " + (((tb - 1) + 8 - 1) % 8 + 1).ToString () + ")), " +
													"chain(" + rd1 + "_straight_" + rd2 + "(" + start + ", " + (((tb - 1) + 8 + 0) % 8 + 1).ToString () + ")), " +
														"chain(" + rd1 + "_straight_" + rd2 + "(" + start + ", " + (((tb - 1) + 8 + 1) % 8 + 1).ToString () + ")";
											}
										}
										else if (notetype == "^") {
											string slidemacro = CaretToCurveSlideMaiScript (start, target);
											if (slidemacro != "") {
												if (start_rd != "C") {
													additionScript2 += rd1 + "_" + slidemacro + "_axis_center" + "(" + start + ", " + target + ")";
												}
												else {
													additionScript2 += rd1 + "_straight_" + rd2 + "(" + target + ")";
												}
											}
										}
									}
									if (i >= noteTypesSize - 1)
										additionScript2 += ")"; // "chain(" に対する ")"
									else
										additionScript2 += ", ";
								}
								additionScript2 += ")"; // "pattern(" に対する ")"
							}
							if (!keyAster)
								additionScript2 += ")"; // "slide(" に対する ")"
							creatable = true;
						}
						else if (notetype == "b") {
							string button = buttons[buttons.Count - 1];
							additionScript2 += MaisqScore.CommandNameDocument.BREAK + "(" + button + ")";
							creatable = true;
						}
						
						if (creatable) {
							if (keyCamma && additionScript2 == "") {
								if (!slash)
									additionScript2 = additionScript + ");\n";
								else
									additionScript2 = ");\n";
							}
							else if (additionScript2 != "") {
								additionScript2 = additionScript + additionScript2;
								if (keyCamma)
									additionScript2 += ");\n";
								if (keySlash)
									slash = true;
							}
							addBpms ();
							addBeats ();
							addRest ();
							notes.Add (additionScript2);
						}
						else if (keyCamma) {
							if (!slash)
								additionScript2 = additionScript + ");\n";
							else
								additionScript2 = ");\n";
							addBpms ();
							addBeats ();
							addRest ();
							notes.Add (additionScript2);
						}
						if (keyCamma) {
							addNotes ();
						}
					}
					
					if (keyCamma)
						slash = false;
					if (keyCamma || keySlash)
						head = null;
					numbersign = false;
					buttons.Clear ();
					notetypes.Clear ();
					beat = "";
					length = "";
					interval = "";
					localBpm = "";
					waitLocalBpm = "";
					waitInterval = "";
					readTank = "";
				}
				else if (subSingle == " " || subSingle == "　" || subSingle == "\t") {
					// スペースは無視. ただし"文字列"には入れたいのでReplaceでは処理しない.
				}
				else if (subSingle == "E") {
					// 読み込み終了.
					break;
				}
				else {
					readTank += subSingle;
				}
			}
		}
		
		addBpms ();
		addBeats ();
		addRest ();
		return maiScript;
	}

	public static bool IsButtonNumber(string text) {
		switch (text) {
		case "1":
		case "2":
		case "3":
		case "4":
		case "5":
		case "6":
		case "7":
		case "8":
			return true;
		}
		return false;
	}
	
	public static bool IsNoteType(string text) {
		switch (text) {
		case "h":
		case "b":
		case "-":
		case "^":
		case "<":
		case ">":
		case "p":
		case "q":
		case "s":
		case "z":
		case "v":
		case "pp":
		case "qq":
		case "V1":
		case "V2":
		case "V3":
		case "V4":
		case "V5":
		case "V6":
		case "V7":
		case "V8":
		case "w":
			return true;
		}
		return false;
	}

	public static bool IsDirectSlideSensor(string text) {
		switch (text) {
		case "A":
		case "B":
		case "C":
		case "1":
		case "2":
		case "3":
		case "4":
		case "5":
		case "6":
		case "7":
		case "8":
			return true;
		}
		return false;
	}
	
	public static bool IsNoteTypeSlide(string text) {
		switch (text) {
		case "-":
		case "^":
		case "<":
		case ">":
		case "p":
		case "q":
		case "s":
		case "z":
		case "v":
		case "pp":
		case "qq":
		case "V1":
		case "V2":
		case "V3":
		case "V4":
		case "V5":
		case "V6":
		case "V7":
		case "V8":
		case "w":
			return true;
		}
		return false;
	}
	
	// >の曲線スライドは始点が1,2,7,8であれば時計周り、3,4,5,6,であれば反時計回りになる。<の曲線スライドは扱いが真逆になる.
	public static string LtGtToCurveSlideMaiScript(string ltgt, string start) {
		if (ltgt == ">") {
			return "curve_right";
		}
		else if (ltgt == "<") {
			return "curve_left";
		}
//		if (start == "1" || start == "2" || start == "7" || start == "8") {
//			if (ltgt == ">") {
//				return "curve_clockwise";
//			}
//			else if (ltgt == "<") {
//				return "curve_counterclockwise";
//			}
//		}
//		else if (start == "3" || start == "4" || start == "5" || start == "6") {
//			if (ltgt == ">") {
//				return "curve_counterclockwise";
//			}
//			else if (ltgt == "<") {
//				return "curve_clockwise";
//			}
//		}
		return "";
	}
	
	// ^の曲線スライドは始点と終点の位置から対角線未満の位置となる.
	public static string CaretToCurveSlideMaiScript(string start, string target) {
		int n_start;
		int n_target;
		if (int.TryParse(start, out n_start) && int.TryParse(target, out n_target)) {
			n_start -= 1;
			n_target -= 1;
			while (n_start < 0) n_start += 8;
			n_start %= 8;
			while (n_target < 0) n_target += 8;
			n_target %= 8;
			
			// 時計回り.
			if (n_target == (n_start + 1) % 8 || n_target == (n_start + 2) % 8 || n_target == (n_start + 3) % 8)
				return "curve_clockwise";
			// 反時計回り.
			if (n_target == (n_start + 5) % 8 || n_target == (n_start + 6) % 8 || n_target == (n_start + 7) % 8)
				return "curve_counterclockwise"; 
		}
		// エラー.
		return "";
	}
	
	/// <summary>
	/// &変数も含めたsimaiスクリプトを渡してTrackInfomationを返す.
	/// </summary>
	public TrackInformation Load (string simai) {
		HashSet<string> macroContains;
		return Load (simai, out macroContains);
	}
	/// <summary>
	/// &変数も含めたsimaiスクリプトを渡してTrackInfomationを返す.
	/// </summary>
	public TrackInformation Load (string simai, out HashSet<string> macroContains) {
		macroContains = new HashSet<string> ();
		var ret = TrackInformation.NewInitialize ();
		var text = RemoveComment (simai, true);

		string readTank = string.Empty;
		string macro = string.Empty;
		bool amp = false;
		bool equal = false;
		bool inote = false;
		string inoteId = string.Empty;
		bool lv = false;
		string lvId = string.Empty;
		bool designer = false;
		string designerId = string.Empty;
		Dictionary<string, ScoreInformation> scoreTank = new Dictionary<string, ScoreInformation> ();
		Dictionary<string, string> lvTank = new Dictionary<string, string> ();
		Dictionary<string, string> designerTank = new Dictionary<string, string> ();

		for (int a = 0; a < text.Length; a++) {
			var subSingle = text.Substring(a, 1);
			var subTank = readTank + subSingle;
			if (subSingle == "&" && !amp && !inote && !lv && !designer) {
				amp = true;
				readTank = string.Empty;
			}
			else if (amp && !equal && !inote && !lv && !designer && (
				subTank == "title" ||
				subTank == "titleruby" ||
				subTank == "freemsg" ||
				subTank == "artist" ||
				subTank == "artistruby" ||
				subTank == "wholebpm" ||
				subTank == "bg" ||
				subTank == "track" ||
				subTank == "seek" ||
				subTank == "wait" ||
				subTank == "first"
				)
			         ) {
				macro = subTank;
				//readTank = string.Empty;
				readTank = subTank;
			}
			else if (amp && !equal && !inote && !lv && !designer && (subTank == "inote_")) {
				inote = true;
				macro = subTank;
				readTank = string.Empty;
			}
			else if (amp && !equal && !inote && !lv && !designer && (subTank == "lv_")) {
				lv = true;
				macro = subTank;
				readTank = string.Empty;
			}
			else if (amp && !equal && !inote && !lv && !designer && (subTank == "designer_")) {
				designer = true;
				macro = subTank;
				readTank = string.Empty;
			}
			else if (subSingle == "=" && amp && !equal) {
				equal = true;
				readTank = string.Empty;
			}
			else if (subSingle == "\n" && amp && equal && !inote) {
				switch (macro) {
				case "title":
					ret.title = readTank;
					macroContains.Add (macro);
					break;
				case "titleruby":
					ret.title_ruby = readTank;
					macroContains.Add (macro);
					break;
				case "freemsg":
				case "artist":
					ret.artist = readTank;
					macroContains.Add (macro);
					break;
				case "artistruby":
					ret.artist_ruby = readTank;
					macroContains.Add (macro);
					break;
				case "wholebpm":
					ret.whole_bpm = readTank;
					macroContains.Add (macro);
					break;
				case "bg":
					ret.jacket = readTank;
					macroContains.Add (macro);
					break;
				case "track":
					ret.audio = readTank;
					macroContains.Add (macro);
					break;
				case "seek":
					if (float.TryParse(readTank.Replace(" ", "").Replace("　", ""), out ret.seek)) {
						macroContains.Add (macro);
					}
					break;
				case "wait":
					if (float.TryParse(readTank.Replace(" ", "").Replace("　", ""), out ret.wait)) {
						macroContains.Add (macro);
					}
					break;
				case "first":
					float outSimaiFirst;
					if (float.TryParse(readTank.Replace(" ", "").Replace("　", ""), out outSimaiFirst)) {
						ret.simai_first = outSimaiFirst;
						macroContains.Add (macro);
					}
					break;
				case "lv_":
					if (lv && lvId != string.Empty) {
						string id = lvId.Replace(" ", "").Replace("　", "");
						id = ChangeSimaiDifficultyToMaiscriptDifficulty(id);
						if (scoreTank.ContainsKey(id)) {
							scoreTank[id].level = readTank;
						}
						else {
							lvTank[id] = readTank;
						}
						macroContains.Add (macro + id);
					}
					break;
				case "designer_":
					if (designer && designerId != string.Empty) {
						string id = designerId.Replace(" ", "").Replace("　", "");
						id = ChangeSimaiDifficultyToMaiscriptDifficulty(id);
						if (scoreTank.ContainsKey(id)) {
							scoreTank[id].notes_design = readTank;
						}
						else {
							designerTank[id] = readTank;
						}
						macroContains.Add (macro + id);
					}
					break;
				}
				readTank = string.Empty;
				amp = false;
				equal = false;
				lv = false;
				lvId = string.Empty;
				designer = false;
				designerId = string.Empty;
				macro = string.Empty;
			}
			else if (lv && amp && !equal && !inote && !designer) {
				lvId += subSingle;
			}
			else if (designer && amp && !equal && !inote && !lv) {
				designerId += subSingle;
			}
			else if (inote && amp && !equal && !lv && !designer) {
				inoteId += subSingle;
			}
			else if ((subSingle == "E" || subSingle == "&") && inoteId != string.Empty && amp && equal && inote && !lv && !designer) {
				readTank += "E";
				string id = inoteId.Replace(" ", "").Replace("　", "");
				id = ChangeSimaiDifficultyToMaiscriptDifficulty(id);
				ScoreInformation scoreInfo;
				if (scoreTank.ContainsKey(id)) {
					scoreInfo = scoreTank[id];
				}
				else {
					scoreInfo = ScoreInformation.NewInitialize ();
				}
				macroContains.Add (macro + id);
				scoreInfo.script_type = "simai";
				scoreInfo.score = readTank;
				if (lvTank.ContainsKey(id)) {
					scoreInfo.level = lvTank[id];
				}
				if (designerTank.ContainsKey(id)) {
					scoreInfo.notes_design = designerTank[id];
				}
				scoreTank[id] = scoreInfo;
				readTank = string.Empty;
				inoteId = string.Empty;
				if (subSingle != "&") amp = false;
				equal = false;
				inote = false;
				macro = string.Empty;
			}
			else {
				readTank += subSingle;
			}
		}

		// スコア順並び替え.
		var difficulties = new string[] {
			"EASY", "BASIC", "ADVANCED", "EXPERT", "MASTER", "Re:MASTER"
		};
		System.Action<string> scoresSort = (difficulty) => {
			if (scoreTank.ContainsKey(difficulty)) {
				ret.scores[difficulty] = scoreTank[difficulty];
			}
		};
		// 基本スコアを優先で入れる.
		foreach (string difficulty in difficulties) {
			scoresSort(difficulty);
		}
		// その他のスコアは定義順(作った順).
		foreach (string key in scoreTank.Keys) {
			if (!ret.scores.ContainsKey(key)) {
				scoresSort(key);
			}
		}


		return ret;
	}

	private string ChangeSimaiDifficultyToMaiscriptDifficulty (string inote_id) {
		switch (inote_id) {
		case "1":
			return "EASY";
		case "2":
			return "BASIC";
		case "3":
			return "ADVANCED";
		case "4":
			return "EXPERT";
		case "5":
			return "MASTER";
		case "6":
			return "Re:MASTER";
		default:
			int numid;
			string editId = inote_id;
			if (int.TryParse(inote_id, out numid)) {
				// inote_7以上のキーは、EDIT_1になるようにする.
				// する必要は特にないんだけど、なんとなく気分で.
				if (numid > 6) {
					editId = (numid - 6).ToString();
				}
			}
			return "EDIT_" + editId;
		}
	}
	
}