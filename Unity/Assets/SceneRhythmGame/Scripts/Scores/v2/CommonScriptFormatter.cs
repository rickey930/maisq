﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// TextDetailInfoがスクリプト、コメント、テキスト、のどれに属するかを付与したデータ
/// </summary>
public class CommonScriptFormatter {
	public enum TextType {
		/// <summary>
		/// スクリプト
		/// </summary>
		SCRIPT,
		/// <summary>
		/// コメント
		/// </summary>
		COMMENT,
		/// <summary>
		/// テキスト
		/// </summary>
		TEXT,
		/// <summary>
		/// コメントやテキストと認識させたキー
		/// </summary>
		SPECIAL,
		/// <summary>
		/// 無効化した文字列
		/// </summary>
		INVALID,
	}
	
	public enum ResultType {
		/// <summary>
		/// 変換成功.
		/// </summary>
		SUCCESS,
		/// <summary>
		/// 必須のコメント終了キーが見つからなかった.
		/// </summary>
		REQUIRED_COMMENT_END_SPECIAL_KEY_NOT_FOUND,
		/// <summary>
		/// 必須の文字列化終了キーが見つからなかった.
		/// </summary>
		REQUIRED_TEXT_END_SPECIAL_KEY_NOT_FOUND,
	}
	
	public class DefineSpecialText {
		public string start { get; private set; }
		public string end { get; private set; }
		public bool requiredEnd { get; private set; }
		/// <summary>
		/// startからendまでで囲んである文字列を特殊な文字列とする.
		/// requiredEndをtrueにすると、end記号は必須になる.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		public DefineSpecialText(string start, string end, bool requiredEnd) {
			this.start = start;
			this.end = end;
			this.requiredEnd = requiredEnd;
		}
	}
	
	public TextDetailInfo[] text { get; private set; }
	public TextType type { get; private set; }
	
	public CommonScriptFormatter(TextDetailInfo[] text, TextType type) {
		this.text = text;
		this.type = type;
		this.textString = null;
	}
	
	private string textString;
	public string GetString() {
		if (textString == null) {
			var builder = new System.Text.StringBuilder();
			foreach (var ch in text) {
				builder.Append(ch.entity);
			}
			textString = builder.ToString();
		}
		return textString;
	}
	
	/// <summary>
	/// スクリプト文字列からCommonScriptFormatterインスタンスを作る
	/// </summary>
	/// <param name="text">スクリプト文字列</param>
	/// <param name="commentKeys">挟んでいる文字列をコメントとして認識させるキーペア</param>
	/// <param name="textKeys">挟んでいる文字列をテキストとして認識させるキーペア</param>
	/// <param name="invalidTexts">スクリプトとして認識させない文字列群</param>
	/// <returns></returns>
	public static CommonScriptFormatter[] Analyze(string text, DefineSpecialText[] commentKeys, DefineSpecialText[] textKeys, string[] invalidTexts, out ResultType result) {
		var _ret = new List<CommonScriptFormatter>();
		var analyzedText = TextDetailInfo.Analyze(text);
		int textSize = analyzedText.Length;
		const int NOW_KEY_NOTHING_INDEX = -1;
		int nowCommentKeyIndex = NOW_KEY_NOTHING_INDEX;
		int nowTextKeyIndex = NOW_KEY_NOTHING_INDEX;
		for (int i = 0; i < textSize; i++) {
			var entity = analyzedText[i];
			bool elseThrough = true;
			// コメント始まりであればコメントフラグを立てる.
			if (elseThrough && commentKeys != null && commentKeys.Length > 0 && nowCommentKeyIndex < 0 && nowTextKeyIndex < 0) {
				for (int j = 0; j < commentKeys.Length; j++) {
					if (!string.IsNullOrEmpty(commentKeys[j].start) && AnalyzeHelper(i, commentKeys[j].start.Length, analyzedText) == commentKeys[j].start) {
						nowCommentKeyIndex = j;
						var items = new TextDetailInfo[commentKeys[j].start.Length];
						for (int k = 0; k < items.Length; k++) {
							items[k] = analyzedText[i + k];
						}
						_ret.Add(new CommonScriptFormatter(items, TextType.SPECIAL));
						if (items.Length > 0) {
							i += items.Length - 1;
						}
						elseThrough = false;
						break;
					}
				}
			}
			// コメントフラグが立っている場合はコメントとして文字を読み込む. コメント終わりであればコメントフラグを降ろす.
			if (elseThrough && commentKeys != null && commentKeys.Length > 0 && nowCommentKeyIndex >= 0 && nowTextKeyIndex < 0) {
				if (!string.IsNullOrEmpty(commentKeys[nowCommentKeyIndex].end) && AnalyzeHelper(i, commentKeys[nowCommentKeyIndex].end.Length, analyzedText) == commentKeys[nowCommentKeyIndex].end) {
					var items = new TextDetailInfo[commentKeys[nowCommentKeyIndex].end.Length];
					for (int k = 0; k < items.Length; k++) {
						items[k] = analyzedText[i + k];
					}
					_ret.Add(new CommonScriptFormatter(items, TextType.SPECIAL));
					nowCommentKeyIndex = NOW_KEY_NOTHING_INDEX;
					if (items.Length > 0) {
						i += items.Length - 1;
					}
				}
				else {
					_ret.Add(new CommonScriptFormatter(new TextDetailInfo[] { entity }, TextType.COMMENT));
				}
				elseThrough = false;
			}
			// 文字列始まりであれば文字列フラグを立てる.
			if (elseThrough && textKeys != null && textKeys.Length > 0 && nowCommentKeyIndex < 0 && nowTextKeyIndex < 0) {
				for (int j = 0; j < textKeys.Length; j++) {
					if (!string.IsNullOrEmpty(textKeys[j].start) && AnalyzeHelper(i, textKeys[j].start.Length, analyzedText) == textKeys[j].start) {
						nowTextKeyIndex = j;
						var items = new TextDetailInfo[textKeys[j].start.Length];
						for (int k = 0; k < items.Length; k++) {
							items[k] = analyzedText[i + k];
						}
						_ret.Add(new CommonScriptFormatter(items, TextType.SPECIAL));
						if (items.Length > 0) {
							i += items.Length - 1;
						}
						elseThrough = false;
						break;
					}
				}
			}
			// 文字列フラグが立っている場合は文字列として文字を読み込む. 文字列終わりであれば文字列フラグを降ろす.
			if (elseThrough && textKeys != null && textKeys.Length > 0 && nowCommentKeyIndex < 0 && nowTextKeyIndex >= 0) {
				if (!string.IsNullOrEmpty(textKeys[nowTextKeyIndex].end) && AnalyzeHelper(i, textKeys[nowTextKeyIndex].end.Length, analyzedText) == textKeys[nowTextKeyIndex].end) {
					var items = new TextDetailInfo[textKeys[nowTextKeyIndex].end.Length];
					for (int k = 0; k < items.Length; k++) {
						items[k] = analyzedText[i + k];
					}
					_ret.Add(new CommonScriptFormatter(items, TextType.SPECIAL));
					nowTextKeyIndex = NOW_KEY_NOTHING_INDEX;
					if (items.Length > 0) {
						i += items.Length - 1;
					}
				}
				else {
					_ret.Add(new CommonScriptFormatter(new TextDetailInfo[] { entity }, TextType.TEXT));
				}
				elseThrough = false;
			}
			// 文字列でもコメントでもない場合は、無効文字でなければスクリプトとして文字を読み込む. 
			if (elseThrough) {
				int invalidIndex = NOW_KEY_NOTHING_INDEX;
				if (invalidTexts != null) {
					for (int j = 0; j < invalidTexts.Length; j++) {
						if (!string.IsNullOrEmpty(invalidTexts[j]) && AnalyzeHelper(i, invalidTexts[j].Length, analyzedText) == invalidTexts[j]) {
							invalidIndex = j;
							break;
						}
					}
				}
				if (invalidIndex < 0) {
					_ret.Add(new CommonScriptFormatter(new TextDetailInfo[] { entity }, TextType.SCRIPT));
				}
				else {
					var items = new TextDetailInfo[invalidTexts[invalidIndex].Length];
					for (int k = 0; k < items.Length; k++) {
						items[k] = analyzedText[i + k];
					}
					_ret.Add(new CommonScriptFormatter(items, TextType.INVALID));
					if (items.Length > 0) {
						i += items.Length - 1;
					}
				}
				elseThrough = false;
			}
		}
		// コメント終了キーが必須なのに見当たらなかった.
		if (commentKeys != null && commentKeys.Length > 0 && nowCommentKeyIndex >= 0 && nowTextKeyIndex < 0 && commentKeys[nowCommentKeyIndex].requiredEnd) {
			result = ResultType.REQUIRED_COMMENT_END_SPECIAL_KEY_NOT_FOUND;
		}
		// 文字列化終了キーが必須なのに見当たらなかった.
		else if (textKeys != null && textKeys.Length > 0 && nowCommentKeyIndex < 0 && nowTextKeyIndex >= 0 && textKeys[nowTextKeyIndex].requiredEnd) {
			result = ResultType.REQUIRED_TEXT_END_SPECIAL_KEY_NOT_FOUND;
		}
		else {
			result = ResultType.SUCCESS;
		}
		return _ret.ToArray();
	}
	
	private static string AnalyzeHelper(int index, int cmpTextLength, TextDetailInfo[] src) {
		// i+文字数がanalyzedText.Lengthより小さい
		if (index + cmpTextLength - 1 < src.Length) {
			string ret = string.Empty;
			for (int i = index; i < index + cmpTextLength; i++) {
				ret += src[i].entity;
			}
			return ret;
		}
		return string.Empty;
	}
}

