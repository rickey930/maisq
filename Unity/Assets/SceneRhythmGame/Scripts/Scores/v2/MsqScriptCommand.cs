﻿using System;
using System.Collections.Generic;

namespace MsqScript {
	/// <summary>
	/// コマンドを呼び出したオブジェクトの情報
	/// </summary>
	public class CallerInfo {
		public Formatter script { get; private set; }
		public TextDetailInfo place { get; private set; }
		public Global global { get; private set; }
		public Object field { get; private set; }
		public CallerInfo(Global global) {
			this.script = null;
			this.place = null;
			this.global = global;
			this.field = global;
		}
		private CallerInfo(Formatter script, TextDetailInfo place, Global global, Object field) {
			this.script = script;
			this.place = place;
			this.global = global;
			this.field = field;
		}
		public CallerInfo ExtendGlobal(Formatter script, TextDetailInfo place, Object field) {
			return new CallerInfo(script, place, global, field);
		}
		public CallerInfo CalledGlobal(Formatter script, TextDetailInfo place) {
			return new CallerInfo(script, place, global, global);
		}
	}
	
	/// <summary>
	/// Msqコマンドの定義.
	/// </summary>
	public class Command {
		private Dictionary<Type, Dictionary<string, Func<CallerInfo, Object[], Object>>> commandList;
		
		public Object ExecuteCommand(CallerInfo caller, string name, params Object[] arguments) {
			if (commandList.ContainsKey(caller.field.type) && commandList[caller.field.type].ContainsKey(name)) {
				return commandList[caller.field.type][name](caller, arguments);
			}
			return new CommandNotFoundException(caller, name, arguments);
		}
		
		public Command() {
			commandList = new Dictionary<Type, Dictionary<string, Func<CallerInfo, Object[], Object>>>();
			
			// Global
			var globalUpgrader = commandList[Type.GLOBAL] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			globalUpgrader.Add(Document.BPM, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Global>().Bpm(caller, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.BPM, args);
			});
			globalUpgrader.Add(Document.BEAT, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Global>().Beat(caller, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.BEAT, args);
			});
			globalUpgrader.Add(Document.INTERVAL, (caller, args) => {
				if (args.Length == 1 && !args[0].IsNaN) {
					return caller.field.Cast<Global>().Interval(caller, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.INTERVAL, args);
			});
			globalUpgrader.Add(Document.NOTE, (caller, args) => {
				if (args.Length > 0) {
					var list = new List<INote>();
					bool error = false;
					foreach (var arg in args) {
						if (arg.IsNote) {
							list.Add(arg as INote);
						}
						else {
							error = true;
							break;
						}
					}
					if (!error) {
						return caller.field.Cast<Global>().Note(caller, list.ToArray());
					}
				}
				return new ArgumentsNotMatchException(caller, Document.NOTE, args);
			});
			globalUpgrader.Add(Document.REST, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Rest(caller);
				}
				else if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Rest(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.REST, args);
			});
			globalUpgrader.Add(Document.LOG, (caller, args) => {
				if (args.Length == 1) {
					return caller.field.Cast<Global>().Log(caller, args[0]);
				}
				return new ArgumentsNotMatchException(caller, Document.LOG, args);
			});
			globalUpgrader.Add(Document.GET, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.TEXT) {
					return caller.field.Cast<Global>().Get(caller, args[0].Cast<Text>());
				}
				return new ArgumentsNotMatchException(caller, Document.GET, args);
			});
			globalUpgrader.Add(Document.SET, (caller, args) => {
				if (args.Length == 2 && args[0].type == Type.TEXT) {
					return caller.field.Cast<Global>().Set(caller, args[0].Cast<Text>(), args[1]);
				}
				return new ArgumentsNotMatchException(caller, Document.SET, args);
			});
			globalUpgrader.Add(Document.IS_DEFINED, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.TEXT) {
					return caller.field.Cast<Global>().IsDefined(caller, args[0].Cast<Text>());
				}
				return new ArgumentsNotMatchException(caller, Document.IS_DEFINED, args);
			});
			globalUpgrader.Add(Document.SIMAI, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.TEXT) {
					return caller.field.Cast<Global>().Simai(caller, args[0].Cast<Text>());
				}
				return new ArgumentsNotMatchException(caller, Document.SIMAI, args);
			});
			globalUpgrader.Add(Document.MATH_ADDITION, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Add(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				else if (args.Length == 2 &&
				         !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Add(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_ADDITION, args);
			});
			globalUpgrader.Add(Document.MATH_SUBTRACTION, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Sub(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				else if (args.Length == 2 &&
				         !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Sub(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_SUBTRACTION, args);
			});
			globalUpgrader.Add(Document.MATH_MULTIPLICATION, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Mul(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				else if (args.Length == 2 &&
				         !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Mul(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_MULTIPLICATION, args);
			});
			globalUpgrader.Add(Document.MATH_DIVISION, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Div(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				else if (args.Length == 2 &&
				         !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Div(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_DIVISION, args);
			});
			globalUpgrader.Add(Document.MATH_POWER, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Pow(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				else if (args.Length == 2 &&
				         !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Pow(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_POWER, args);
			});
			globalUpgrader.Add(Document.MATH_MODULO, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Mod(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_MODULO, args);
			});
			globalUpgrader.Add(Document.EQUAL, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Equal(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				else if (args.Length == 2 &&
				         args[0].type == Type.TEXT && args[1].type == Type.TEXT) {
					return caller.field.Cast<Global>().Equal(caller, args[0].Cast<Text>(), args[1].Cast<Text>());
				}
				else if (args.Length == 2 &&
				         args[0].type == Type.POSITION && args[1].type == Type.POSITION) {
					return caller.field.Cast<Global>().Equal(caller, args[0].Cast<Position>(), args[1].Cast<Position>());
				}
				return new ArgumentsNotMatchException(caller, Document.EQUAL, args);
			});
			globalUpgrader.Add(Document.NOT_EQUAL, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().NotEqual(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				else if (args.Length == 2 &&
				         args[0].type == Type.TEXT && args[1].type == Type.TEXT) {
					return caller.field.Cast<Global>().NotEqual(caller, args[0].Cast<Text>(), args[1].Cast<Text>());
				}
				else if (args.Length == 2 &&
				         args[0].type == Type.POSITION && args[1].type == Type.POSITION) {
					return caller.field.Cast<Global>().NotEqual(caller, args[0].Cast<Position>(), args[1].Cast<Position>());
				}
				return new ArgumentsNotMatchException(caller, Document.NOT_EQUAL, args);
			});
			globalUpgrader.Add(Document.GREATER, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Greater(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.GREATER, args);
			});
			globalUpgrader.Add(Document.LESS, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Less(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.LESS, args);
			});
			globalUpgrader.Add(Document.GREATER_EQUAL, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().GreaterEqual(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.GREATER_EQUAL, args);
			});
			globalUpgrader.Add(Document.LESS_EQUAL, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().LessEqual(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.LESS_EQUAL, args);
			});
			globalUpgrader.Add(Document.BIT_AND, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().BitAnd(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_AND, args);
			});
			globalUpgrader.Add(Document.BIT_OR, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().BitOr(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_OR, args);
			});
			globalUpgrader.Add(Document.BIT_XOR, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().BitXor(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_XOR, args);
			});
			globalUpgrader.Add(Document.BIT_NOT, (caller, args) => {
				if (args.Length == 1 &&
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().BitNot(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_NOT, args);
			});
			globalUpgrader.Add(Document.BIT_SHIFT_LEFT, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().BitShiftLeft(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_SHIFT_LEFT, args);
			});
			globalUpgrader.Add(Document.BIT_SHIFT_RIGHT, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().BitShiftRight(caller, args[0].Cast<Number>(), args[1].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BIT_SHIFT_RIGHT, args);
			});
			globalUpgrader.Add(Document.NOT, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.FLAG) {
					return caller.field.Cast<Global>().Not(caller, args[0].Cast<Flag>());
				}
				return new ArgumentsNotMatchException(caller, Document.NOT, args);
			});
			globalUpgrader.Add(Document.CONCATENATE, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.TEXT && args[1].type == Type.TEXT) {
					return caller.field.Cast<Global>().Concat(caller, args[0].Cast<Text>(), args[1].Cast<Text>());
				}
				return new ArgumentsNotMatchException(caller, Document.CONCATENATE, args);
			});
			globalUpgrader.Add(Document.POSITION, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Pos(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.POSITION, args);
			});
			globalUpgrader.Add(Document.RGB255, (caller, args) => {
				if (args.Length == 3 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER && args[2].type == Type.NUMBER) {
					return caller.field.Cast<Global>().RGB255(caller, args[0].Cast<Number>(), args[1].Cast<Number>(), args[2].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.RGB255, args);
			});
			globalUpgrader.Add(Document.RGBA255, (caller, args) => {
				if (args.Length == 4 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER && args[2].type == Type.NUMBER && args[3].type == Type.NUMBER) {
					return caller.field.Cast<Global>().RGBA255(caller, args[0].Cast<Number>(), args[1].Cast<Number>(), args[2].Cast<Number>(), args[3].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.RGBA255, args);
			});
			globalUpgrader.Add(Document.ARGB255, (caller, args) => {
				if (args.Length == 4 &&
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER && args[2].type == Type.NUMBER && args[3].type == Type.NUMBER) {
					return caller.field.Cast<Global>().ARGB255(caller, args[0].Cast<Number>(), args[1].Cast<Number>(), args[2].Cast<Number>(), args[3].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.ARGB255, args);
			});
			globalUpgrader.Add(Document.RGB, (caller, args) => {
				if (args.Length == 3 &&
				    !args[0].IsNaN && !args[1].IsNaN && !args[2].IsNaN) {
					return caller.field.Cast<Global>().RGB(caller, args[0] as IMsqValue, args[1] as IMsqValue, args[2] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.RGB, args);
			});
			globalUpgrader.Add(Document.RGBA, (caller, args) => {
				if (args.Length == 4 &&
				    !args[0].IsNaN && !args[1].IsNaN && !args[2].IsNaN && !args[3].IsNaN) {
					return caller.field.Cast<Global>().RGBA(caller, args[0] as IMsqValue, args[1] as IMsqValue, args[2] as IMsqValue, args[3] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.RGBA, args);
			});
			globalUpgrader.Add(Document.ARGB, (caller, args) => {
				if (args.Length == 4 &&
				    !args[0].IsNaN && !args[1].IsNaN && !args[2].IsNaN && !args[3].IsNaN) {
					return caller.field.Cast<Global>().ARGB(caller, args[0] as IMsqValue, args[1] as IMsqValue, args[2] as IMsqValue, args[3] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.ARGB, args);
			});
			globalUpgrader.Add(Document.BLACK, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Black(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.BLACK, args);
			});
			globalUpgrader.Add(Document.WHITE, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().White(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.WHITE, args);
			});
			globalUpgrader.Add(Document.GRAY, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Gray(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.GRAY, args);
			});
			globalUpgrader.Add(Document.RED, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Red(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.RED, args);
			});
			globalUpgrader.Add(Document.GREEN, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Green(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.GREEN, args);
			});
			globalUpgrader.Add(Document.BLUE, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Blue(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.BLUE, args);
			});
			globalUpgrader.Add(Document.YELLOW, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Yellow(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.YELLOW, args);
			});
			globalUpgrader.Add(Document.MAGENTA, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Magenta(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.MAGENTA, args);
			});
			globalUpgrader.Add(Document.CYAN, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().Cyan(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.CYAN, args);
			});
			globalUpgrader.Add(Document.SENSOR_DEGREE, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().SensorDegree(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.SENSOR_DEGREE, args);
			});
			globalUpgrader.Add(Document.SENSOR_DEGREE_DISTANCE_CLOCKWISE, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().SensorDegreeDistanceClockwise(caller, args[0].Cast<Number>(), args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.SENSOR_DEGREE_DISTANCE_CLOCKWISE, args);
			});
			globalUpgrader.Add(Document.SENSOR_DEGREE_DISTANCE_COUNTERCLOCKWISE, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().SensorDegreeDistanceCounterClockwise(caller, args[0].Cast<Number>(), args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.SENSOR_DEGREE_DISTANCE_COUNTERCLOCKWISE, args);
			});
			globalUpgrader.Add(Document.SENSOR_DEGREE_DISTANCE_RIGHT, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().SensorDegreeDistanceRight(caller, args[0].Cast<Number>(), args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.SENSOR_DEGREE_DISTANCE_RIGHT, args);
			});
			globalUpgrader.Add(Document.SENSOR_DEGREE_DISTANCE_LEFT, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.NUMBER && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().SensorDegreeDistanceLeft(caller, args[0].Cast<Number>(), args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.SENSOR_DEGREE_DISTANCE_LEFT, args);
			});
			globalUpgrader.Add(Document.ANGLE_X, (caller, args) => {
				if (args.Length == 1 && 
				    !args[0].IsNaN) {
					return caller.field.Cast<Global>().AngleX(caller, args[0] as IMsqValue);
				}
				else if (args.Length == 2 && 
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().AngleX(caller, args[0] as IMsqValue, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.ANGLE_X, args);
			});
			globalUpgrader.Add(Document.ANGLE_Y, (caller, args) => {
				if (args.Length == 1 && 
				    !args[0].IsNaN) {
					return caller.field.Cast<Global>().AngleY(caller, args[0] as IMsqValue);
				}
				else if (args.Length == 2 && 
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().AngleY(caller, args[0] as IMsqValue, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.ANGLE_Y, args);
			});
			globalUpgrader.Add(Document.OUTER_SENSOR_POSITION_X, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().OuterSensorPositionX(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.OUTER_SENSOR_POSITION_X, args);
			});
			globalUpgrader.Add(Document.OUTER_SENSOR_POSITION_Y, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().OuterSensorPositionY(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.OUTER_SENSOR_POSITION_Y, args);
			});
			globalUpgrader.Add(Document.INNER_SENSOR_POSITION_X, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().InnerSensorPositionX(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.INNER_SENSOR_POSITION_X, args);
			});
			globalUpgrader.Add(Document.INNER_SENSOR_POSITION_Y, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().InnerSensorPositionY(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.INNER_SENSOR_POSITION_Y, args);
			});
			globalUpgrader.Add(Document.CENTER_SENSOR_POSITION_X, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().CenterSensorPositionX(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.CENTER_SENSOR_POSITION_X, args);
			});
			globalUpgrader.Add(Document.CENTER_SENSOR_POSITION_Y, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().CenterSensorPositionY(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.CENTER_SENSOR_POSITION_Y, args);
			});
			globalUpgrader.Add(Document.OUTER_SENSOR_RADIUS, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().OuterSensorRadius(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.OUTER_SENSOR_RADIUS, args);
			});
			globalUpgrader.Add(Document.INNER_SENSOR_RADIUS, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().InnerSensorRadius(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.INNER_SENSOR_RADIUS, args);
			});
			globalUpgrader.Add(Document.CENTER_SENSOR_RADIUS, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().CenterSensorRadius(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.CENTER_SENSOR_RADIUS, args);
			});
			globalUpgrader.Add(Document.ANGLE, (caller, args) => {
				if (args.Length == 1 && 
				    !args[0].IsNaN) {
					return caller.field.Cast<Global>().Angle(caller, args[0] as IMsqValue);
				}
				else if (args.Length == 2 && 
				         !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Angle(caller, args[0] as IMsqValue, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.ANGLE, args);
			});
			globalUpgrader.Add(Document.OUTER_SENSOR_POSITION, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().OuterSensorPosition(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.OUTER_SENSOR_POSITION, args);
			});
			globalUpgrader.Add(Document.INNER_SENSOR_POSITION, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().InnerSensorPosition(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.INNER_SENSOR_POSITION, args);
			});
			globalUpgrader.Add(Document.CENTER_SENSOR_POSITION, (caller, args) => {
				if (args.Length == 0) {
					return caller.field.Cast<Global>().CenterSensorPosition(caller);
				}
				return new ArgumentsNotMatchException(caller, Document.CENTER_SENSOR_POSITION, args);
			});
			globalUpgrader.Add(Document.TAP, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Tap(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.TAP, args);
			});
			globalUpgrader.Add(Document.HOLD, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.NUMBER && args[0].IsStep) {
					return caller.field.Cast<Global>().Hold(caller, args[0].Cast<Number>(), args[0] as IStep);
				}
				return new ArgumentsNotMatchException(caller, Document.HOLD, args);
			});
			globalUpgrader.Add(Document.SLIDE, (caller, args) => {
				if (args.Length > 0) {
					ISlideHead head = null;
					INote headStead = null;
					ISlideWait wait = null;
					IStep step = null;
					var patterns = new List<ISlidePattern>();
					int index = 0;
					bool error = false;
					if (args[index].IsSlideHead) {
						head = args[index] as ISlideHead;
						index++;
					}
					else if (args[index].IsNote) {
						headStead = args[index] as INote;
						index++;
					}
					if (args[index].IsSlideWait) {
						wait = args[index] as ISlideWait;
						index++;
					}
					if (args[index].IsStep) {
						step = args[index] as IStep;
						index++;
					}
					for (int i = index; i < args.Length; i++) {
						if (args[index].IsSlidePattern) {
							patterns.Add(args[index] as ISlidePattern);
						}
						else {
							error = true;
							break;
						}
					}
					if (patterns.Count > 0 && !error) {
						if (headStead == null) {
							return caller.field.Cast<Global>().Slide(caller, head, wait, step, patterns.ToArray());
						}
						else {
							return caller.field.Cast<Global>().Slide(caller, headStead, wait, step, patterns.ToArray());
						}
					}
				}
				return new ArgumentsNotMatchException(caller, Document.SLIDE, args);
			});
			globalUpgrader.Add(Document.BREAK, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Break(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BREAK, args);
			});
			globalUpgrader.Add(Document.MESSAGE, (caller, args) => {
				if (args.Length == 5 && 
				    args[0].type == Type.TEXT && args[1].type == Type.POSITION && !args[2].IsNaN && args[3].type == Type.COLOR && args[4].IsStep) {
					return caller.field.Cast<Global>().Message(caller, args[0].Cast<Text>(), args[1].Cast<Position>(), args[2] as IMsqValue, args[3].Cast<Color>(), args[4] as IStep);
				}
				return new ArgumentsNotMatchException(caller, Document.MESSAGE, args);
			});
			globalUpgrader.Add(Document.SCROLL_MESSAGE, (caller, args) => {
				if (args.Length == 5 && 
				    args[0].type == Type.TEXT && !args[1].IsNaN && !args[2].IsNaN && args[3].type == Type.COLOR && args[4].IsStep) {
					return caller.field.Cast<Global>().ScrollMessage(caller, args[0].Cast<Text>(), args[1] as IMsqValue, args[2] as IMsqValue, args[3].Cast<Color>(), args[4] as IStep);
				}
				return new ArgumentsNotMatchException(caller, Document.SCROLL_MESSAGE, args);
			});
			globalUpgrader.Add(Document.STEP, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Step(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				else if (args.Length == 1 &&
				         !args[0].IsNaN) {
					return caller.field.Cast<Global>().Step(caller, args[0] as IMsqValue);
				}
				else if (args.Length == 3 &&
				         !args[0].IsNaN && !args[1].IsNaN && !args[2].IsNaN) {
					return caller.field.Cast<Global>().Step(caller, args[0] as IMsqValue, args[1] as IMsqValue, args[2] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.STEP, args);
			});
			globalUpgrader.Add(Document.STAR, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Star(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.STAR, args);
			});
			globalUpgrader.Add(Document.BREAK_STAR, (caller, args) => {
				if (args.Length == 1 && 
				    args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().BreakStar(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.BREAK_STAR, args);
			});
			globalUpgrader.Add(Document.WAIT_CUSTOM_LOCAL_BPM, (caller, args) => {
				if (args.Length == 1 &&
				         !args[0].IsNaN) {
					return caller.field.Cast<Global>().WaitCustomBpm(caller, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.WAIT_CUSTOM_LOCAL_BPM, args);
			});
			globalUpgrader.Add(Document.WAIT_CUSTOM_INTERVAL, (caller, args) => {
				if (args.Length == 1 &&
				    !args[0].IsNaN) {
					return caller.field.Cast<Global>().WaitCustomInterval(caller, args[0] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.WAIT_CUSTOM_INTERVAL, args);
			});
			globalUpgrader.Add(Document.WAIT_CUSTOM_STEP, (caller, args) => {
				if (args.Length == 2 &&
				    !args[0].IsNaN && !args[1].IsNaN) {
					return caller.field.Cast<Global>().WaitCustomStep(caller, args[0] as IMsqValue, args[1] as IMsqValue);
				}
				else if (args.Length == 3 &&
				         !args[0].IsNaN && !args[1].IsNaN && !args[2].IsNaN) {
					return caller.field.Cast<Global>().WaitCustomStep(caller, args[0] as IMsqValue, args[1] as IMsqValue, args[2] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.WAIT_CUSTOM_STEP, args);
			});
			globalUpgrader.Add(Document.PATTERN, (caller, args) => {
				if (args.Length > 0) {
					ISlideWait wait = null;
					IStep step = null;
					var chains = new List<ISlideChain>();
					int index = 0;
					bool error = false;
					if (args[index].IsSlideWait) {
						wait = args[index] as ISlideWait;
						index++;
					}
					if (args[index].IsStep) {
						step = args[index] as IStep;
						index++;
					}
					for (int i = index; i < args.Length; i++) {
						if (args[index].IsSlidePattern) {
							chains.Add(args[index] as ISlideChain);
						}
						else {
							error = true;
							break;
						}
					}
					if (chains.Count > 0 && !error) {
						return caller.field.Cast<Global>().Pattern(caller, wait, step, chains.ToArray());
					}
				}
				return new ArgumentsNotMatchException(caller, Document.PATTERN, args);
			});
			globalUpgrader.Add(Document.CHAIN, (caller, args) => {
				if (args.Length > 0) {
					ISlideWait wait = null;
					IStep step = null;
					var commands = new List<ISlideCommand>();
					int index = 0;
					bool error = false;
					if (args[index].IsSlideWait) {
						wait = args[index] as ISlideWait;
						index++;
					}
					if (args[index].IsStep) {
						step = args[index] as IStep;
						index++;
					}
					for (int i = index; i < args.Length; i++) {
						if (args[index].IsSlidePattern) {
							commands.Add(args[index] as ISlideCommand);
						}
						else {
							error = true;
							break;
						}
					}
					if (commands.Count > 0 && !error) {
						return caller.field.Cast<Global>().Chain(caller, wait, step, commands.ToArray());
					}
				}
				return new ArgumentsNotMatchException(caller, Document.CHAIN, args);
			});
			globalUpgrader.Add(Document.STRAIGHT, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.POSITION && args[1].type == Type.POSITION) {
					return caller.field.Cast<Global>().Straight(caller, args[0].Cast<Position>(), args[1].Cast<Position>());
				}
				return new ArgumentsNotMatchException(caller, Document.STRAIGHT, args);
			});
			globalUpgrader.Add(Document.CURVE, (caller, args) => {
				if (args.Length == 4 &&
				    args[0].type == Type.POSITION && !args[1].IsNaN && !args[2].IsNaN && !args[3].IsNaN) {
					return caller.field.Cast<Global>().Curve(caller, args[0].Cast<Position>(), args[1] as IMsqValue, args[2] as IMsqValue, args[3] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.CURVE, args);
			});
			globalUpgrader.Add(Document.CONTINUED_STRAIGHT, (caller, args) => {
				if (args.Length == 1 &&
				    args[0].type == Type.POSITION) {
					return caller.field.Cast<Global>().ContinuedStraight(caller, args[0].Cast<Position>());
				}
				return new ArgumentsNotMatchException(caller, Document.CONTINUED_STRAIGHT, args);
			});
			globalUpgrader.Add(Document.CONTINUED_CURVE, (caller, args) => {
				if (args.Length == 2 &&
				    args[0].type == Type.POSITION && !args[1].IsNaN) {
					return caller.field.Cast<Global>().ContinuedCurve(caller, args[0].Cast<Position>(), args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.CONTINUED_CURVE, args);
			});




			globalUpgrader.Add(Document.CALL, (caller, args) => {
				if (args.Length > 0) {
					if (args[0].type == Type.FUNCTION) {
						var param = new Object[args.Length - 1];
						for (int i = 0; i < param.Length; i++) {
							param[i] = args[i + 1];
						}
						return caller.field.Cast<Global>().Call(caller, args[0].Cast<Function>(), param);
					}
					else if (args[0].type == Type.TEXT) {
						var param = new Object[args.Length - 1];
						for (int i = 0; i < param.Length; i++) {
							param[i] = args[i + 1];
						}
						return caller.field.Cast<Global>().Call(caller, args[0].Cast<Text>(), param);
					}
				}
				return new ArgumentsNotMatchException(caller, Document.CALL, args);
			});
			globalUpgrader.Add(Document.RETURN, (caller, args) => {
				if (args.Length == 1) {
					return caller.field.Cast<Global>().Return(caller, args[0]);
				}
				else if (args.Length >= 2) {
					return caller.field.Cast<Global>().Return(caller, args);
				}
				return new ArgumentsNotMatchException(caller, Document.RETURN, args);
			});
			globalUpgrader.Add(Document.ARGUMENT, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Arg(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.ARGUMENT, args);
			});
			globalUpgrader.Add(Document.ARRAY, (caller, args) => {
				return caller.field.Cast<Global>().Array(caller, args);
			});
			globalUpgrader.Add(Document.TABLE, (caller, args) => {
				return caller.field.Cast<Global>().Table(caller);
			});
			globalUpgrader.Add(Document.TURN, (caller, args) => {
				if (args.Length == 2 && 
				    args[0] is IButtonTurnable && args[1].type == Type.NUMBER) {
					return caller.field.Cast<Global>().Turn(caller, args[0] as IButtonTurnable, args[1].Cast<Number>());
				}
				else if (args.Length == 2 && 
				         args[0] is IFreeTurnable && !args[1].IsNaN) {
					return caller.field.Cast<Global>().Turn(caller, args[0] as IFreeTurnable, args[1] as IMsqValue);
				}
				return new ArgumentsNotMatchException(caller, Document.TURN, args);
			});
			globalUpgrader.Add(Document.MIRROR_HOLIZONTAL, (caller, args) => {
				if (args.Length == 1 && 
				    args[0] is IMirrorable) {
					return caller.field.Cast<Global>().HMirror(caller, args[0] as IMirrorable);
				}
				return new ArgumentsNotMatchException(caller, Document.MIRROR_HOLIZONTAL, args);
			});
			globalUpgrader.Add(Document.MIRROR_VERTICAL, (caller, args) => {
				if (args.Length == 1 && 
				    args[0] is IMirrorable) {
					return caller.field.Cast<Global>().VMirror(caller, args[0] as IMirrorable);
				}
				return new ArgumentsNotMatchException(caller, Document.MIRROR_VERTICAL, args);
			});
			globalUpgrader.Add(Document.IF, (caller, args) => {
				if (args.Length == 3 && 
				    args[0].type == Type.FLAG && args[1].type == Type.FUNCTION && args[2].type == Type.FUNCTION) {
					return caller.field.Cast<Global>().If(caller, args[0].Cast<Flag>(), args[1].Cast<Function>(), args[2].Cast<Function>());
				}
				return new ArgumentsNotMatchException(caller, Document.IF, args);
			});
			globalUpgrader.Add(Document.WHILE, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.FUNCTION && args[1].type == Type.FUNCTION) {
					return caller.field.Cast<Global>().While(caller, args[0].Cast<Function>(), args[1].Cast<Function>());
				}
				return new ArgumentsNotMatchException(caller, Document.WHILE, args);
			});
			globalUpgrader.Add(Document.UNTIL, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.FUNCTION && args[1].type == Type.FUNCTION) {
					return caller.field.Cast<Global>().Until(caller, args[0].Cast<Function>(), args[1].Cast<Function>());
				}
				return new ArgumentsNotMatchException(caller, Document.UNTIL, args);
			});
			globalUpgrader.Add(Document.DO_WHILE, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.FUNCTION && args[1].type == Type.FUNCTION) {
					return caller.field.Cast<Global>().DoWhile(caller, args[0].Cast<Function>(), args[1].Cast<Function>());
				}
				return new ArgumentsNotMatchException(caller, Document.DO_WHILE, args);
			});
			globalUpgrader.Add(Document.DO_UNTIL, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.FUNCTION && args[1].type == Type.FUNCTION) {
					return caller.field.Cast<Global>().DoUntil(caller, args[0].Cast<Function>(), args[1].Cast<Function>());
				}
				return new ArgumentsNotMatchException(caller, Document.DO_UNTIL, args);
			});
			globalUpgrader.Add(Document.FOR, (caller, args) => {
				if (args.Length == 2 && 
				    args[0].type == Type.NUMBER && args[1].type == Type.FUNCTION) {
					return caller.field.Cast<Global>().For(caller, args[0].Cast<Number>(), args[1].Cast<Function>());
				}
				else if (args.Length == 3 && 
				         args[0].type == Type.TEXT && args[1].type == Type.NUMBER && args[2].type == Type.FUNCTION) {
					return caller.field.Cast<Global>().For(caller, args[0].Cast<Text>(), args[1].Cast<Number>(), args[2].Cast<Function>());
				}
				else if (args.Length == 4 && 
				         args[0].type == Type.FUNCTION && args[1].type == Type.FUNCTION && args[2].type == Type.FUNCTION && args[3].type == Type.FUNCTION) {
					return caller.field.Cast<Global>().For(caller, args[0].Cast<Function>(), args[1].Cast<Function>(), args[2].Cast<Function>(), args[3].Cast<Function>());
				}
				return new ArgumentsNotMatchException(caller, Document.FOR, args);
			});
			
			// Number
			var numberUpgrader = commandList[Type.NUMBER] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			numberUpgrader.Add(Document.MATH_ADDITION, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Number>().Add(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_ADDITION, args);
			});
			
			// Value
			var valueUpgrader = commandList[Type.VALUE] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			valueUpgrader.Add(Document.MATH_ADDITION, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Value>().Add(caller, (args[0] as IMsqValue));
				}
				return new ArgumentsNotMatchException(caller, Document.MATH_ADDITION, args);
			});
			
			// Array
			var arrayUpgrader = commandList[Type.ARRAY] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			arrayUpgrader.Add(Document.GET, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.NUMBER) {
					return caller.field.Cast<Array>().Get(caller, args[0].Cast<Number>());
				}
				return new ArgumentsNotMatchException(caller, Document.GET, args);
			});
			
			// Table
			var tableUpgrader = commandList[Type.TABLE] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			tableUpgrader.Add(Document.GET, (caller, args) => {
				if (args.Length == 1 && args[0].type == Type.TEXT) {
					return caller.field.Cast<Table>().Get(caller, args[0].Cast<Text>());
				}
				return new ArgumentsNotMatchException(caller, Document.GET, args);
			});
			
			// Function
			var functionUpgrader = commandList[Type.FUNCTION] = new Dictionary<string, Func<CallerInfo, Object[], Object>>();
			functionUpgrader.Add(Document.CALL, (caller, args) => {
				return caller.field.Cast<Function>().Call(caller, args);
			});
			
			//など。
		}
	}
}