﻿using UnityEngine;
using System.Collections;

namespace MsqScript {
	public class Document : MonoBehaviour {
		// GLOBAL
		public const string BPM = "bpm";
		public const string BEAT = "beat";
		public const string INTERVAL = "interval";
		public const string NOTE = "note";
		public const string REST = "rest";
		public const string LOG = "log";
		public const string GET = "get";
		public const string SET = "set";
		public const string IS_DEFINED = "is_defined";
		public const string SIMAI = "simai";
		public const string IF = "if";
		public const string WHILE = "while";
		public const string UNTIL = "until";
		public const string DO_WHILE = "do_while";
		public const string DO_UNTIL = "do_until";
		public const string FOR = "for";
		// FUNCTION
		public const string FUNCTION = "function";
		public const string CALL = "call";
		// ARGUMENT
		public const string ARGUMENT = "arg";
		// RETURN
		public const string RETURN = "return";
		// ARRAY
		public const string ARRAY = "array";
		// TABLE
		public const string TABLE = "table";
		// NUMBER
		public const string MATH_ADDITION = "add";
		public const string MATH_SUBTRACTION = "sub";
		public const string MATH_MULTIPLICATION = "mul";
		public const string MATH_DIVISION = "div";
		public const string MATH_POWER = "pow";
		public const string MATH_MODULO = "mod";
		public const string EQUAL = "equal";
		public const string NOT_EQUAL = "not_equal";
		public const string GREATER = "greater";
		public const string LESS = "less";
		public const string GREATER_EQUAL = "greater_equal";
		public const string LESS_EQUAL = "less_equal";
		public const string INCREMENT = "inc";
		public const string DECREMENT = "dec";
		public const string BIT_AND = "bit_and";
		public const string BIT_OR = "bit_or";
		public const string BIT_XOR = "bit_xor";
		public const string BIT_NOT = "bit_not";
		public const string BIT_SHIFT_LEFT = "bit_shift_left";
		public const string BIT_SHIFT_RIGHT = "bit_shift_right";
		// VALUE
		public const string ANGLE_X = "angle_x";
		public const string ANGLE_Y = "angle_y";
		public const string SENSOR_DEGREE = "sensor_deg";
		public const string OUTER_SENSOR_POSITION_X = "outer_sensor_x";
		public const string OUTER_SENSOR_POSITION_Y = "outer_sensor_y";
		public const string INNER_SENSOR_POSITION_X = "inner_sensor_x";
		public const string INNER_SENSOR_POSITION_Y = "inner_sensor_y";
		public const string CENTER_SENSOR_POSITION_X = "center_sensor_x";
		public const string CENTER_SENSOR_POSITION_Y = "center_sensor_y";
		public const string OUTER_SENSOR_RADIUS = "outer_sensor_radius";
		public const string INNER_SENSOR_RADIUS = "inner_sensor_radius";
		public const string CENTER_SENSOR_RADIUS = "center_sensor_radius";
		public const string SENSOR_DEGREE_DISTANCE_CLOCKWISE = "sensor_deg_distance_clockwise";
		public const string SENSOR_DEGREE_DISTANCE_COUNTERCLOCKWISE = "sensor_deg_distance_counterclockwise";
		public const string SENSOR_DEGREE_DISTANCE_RIGHT = "sensor_deg_distance_right";
		public const string SENSOR_DEGREE_DISTANCE_LEFT = "sensor_deg_distance_left";
		// FLAG
		public const string TRUE = "true";
		public const string YES = "yes";
		public const string ON = "on";
		public const string FALSE = "false";
		public const string NO = "no";
		public const string OFF = "off";
		public const string NOT = "not";
		// TEXT
		public const string CONCATENATE = "concat";
		public const string LENGTH = "length";
		// POS
		public const string POSITION = "pos";
		public const string ANGLE = "angle";
		public const string OUTER_SENSOR_POSITION = "outer_sensor_pos";
		public const string INNER_SENSOR_POSITION = "inner_sensor_pos";
		public const string CENTER_SENSOR_POSITION = "center_sensor_pos";
		public const string TURN = "turn";
		public const string MIRROR_HOLIZONTAL = "h_mirror";
		public const string MIRROR_VERTICAL = "v_mirror";
		public const string GET_X = "get_x";
		public const string GET_Y = "get_y";
		public const string SET_X = "set_x";
		public const string SET_Y = "set_y";
		// Color
		public const string RGB = "rgb";
		public const string RGBA = "rgba";
		public const string ARGB = "argb";
		public const string RGB255 = "rgb255";
		public const string RGBA255 = "rgba255";
		public const string ARGB255 = "argb255";
		public const string GET_R = "get_r";
		public const string GET_G = "get_g";
		public const string GET_B = "get_b";
		public const string GET_A = "get_a";
		public const string SET_R = "set_r";
		public const string SET_G = "set_g";
		public const string SET_B = "set_b";
		public const string SET_A = "set_a";
		public const string GET_R255 = "get_r255";
		public const string GET_G255 = "get_g255";
		public const string GET_B255 = "get_b255";
		public const string GET_A255 = "get_a255";
		public const string SET_R255 = "set_r255";
		public const string SET_G255 = "set_g255";
		public const string SET_B255 = "set_b255";
		public const string SET_A255 = "set_a255";
		public const string BLACK = "black";
		public const string WHITE = "white";
		public const string GRAY = "gray";
		public const string RED = "red";
		public const string GREEN = "green";
		public const string BLUE = "blue";
		public const string YELLOW = "yellow";
		public const string MAGENTA = "magenta";
		public const string CYAN = "cyan";
		// NOTE
		public const string TAP = "tap";
		public const string HOLD = "hold";
		public const string SLIDE = "slide";
		public const string BREAK = "break";
		public const string MESSAGE = "message";
		public const string SCROLL_MESSAGE = "scroll_message";
		public const string SOUND_MESSAGE = "sound_message";
		public const string SECRET = "secret";
		// STEP
		public const string STEP = "step";
		// SLIDE_HEAD
		public const string STAR = "star";
		public const string BREAK_STAR = "break_star";
		public const string NOTHING = "nothing";
		// SLIDE_WAIT
		public const string WAIT_DEFAULT = "wait_default";
		public const string WAIT_CUSTOM_LOCAL_BPM = "wait_custom_bpm";
		public const string WAIT_CUSTOM_INTERVAL = "wait_custom_interval";
		public const string WAIT_CUSTOM_STEP = "wait_custom_step";
		// SLIDE_PATTERN
		public const string PATTERN = "pattern";
		// SLIDE_CHAIN
		public const string CHAIN = "chain";
		public const string SHAPE_W = "shape_w";
		// SLIDE_COMMAND
		public const string STRAIGHT = "straight";
		public const string CONTINUED_STRAIGHT = "continued_straight";
		public const string CURVE = "curve";
		public const string CONTINUED_CURVE = "continued_curve";
		// SLIDE_COMMAND_ARRAY
		// [直線]_[始点]_から_[終点]
		// [曲線]_[始点と終点の半径]_[回転方向]_[中心軸]
		public const string OUTER_STRAIGHT_OUTER = "outer_straight_outer";
		public const string OUTER_STRAIGHT_INNER = "outer_straight_inner";
		public const string OUTER_STRAIGHT_CENTER = "outer_straight_center";
		public const string OUTER_CURVE_CLOCKWISE_AXIS_CENTER = "outer_curve_clockwise_axis_center";
		public const string OUTER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER = "outer_curve_counterclockwise_axis_center";
		public const string OUTER_CURVE_RIGHT_AXIS_CENTER = "outer_curve_right_axis_center";
		public const string OUTER_CURVE_LEFT_AXIS_CENTER = "outer_curve_left_axis_center";
		public const string INNER_STRAIGHT_OUTER = "inner_straight_outer";
		public const string INNER_STRAIGHT_INNER = "inner_straight_inner";
		public const string INNER_STRAIGHT_CENTER = "inner_straight_center";
		public const string INNER_CURVE_CLOCKWISE_AXIS_CENTER = "inner_curve_clockwise_axis_center";
		public const string INNER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER = "inner_curve_counterclockwise_axis_center";
		public const string INNER_CURVE_RIGHT_AXIS_CENTER = "inner_curve_right_axis_center";
		public const string INNER_CURVE_LEFT_AXIS_CENTER = "inner_curve_left_axis_center";
		public const string CENTER_STRAIGHT_OUTER = "center_straight_outer";
		public const string CENTER_STRAIGHT_INNER = "center_straight_inner";
		public const string SHAPE_P_AXIS_CENTER = "shape_p_axis_center";
		public const string SHAPE_Q_AXIS_CENTER = "shape_q_axis_center";
		public const string SHAPE_V_AXIS_CENTER = "shape_v_axis_center";
		public const string SHAPE_S = "shape_s";
		public const string SHAPE_Z = "shape_z";
		public const string SHAPE_PP = "shape_pp";
		public const string SHAPE_QQ = "shape_qq";
		public const string SHAPE_V_AXIS_OUTER = "shape_v_axis_outer";

		public const string SIMAI_EXPAND = "simai_expand";
	}

	public static class MaipadSensor {
		public enum Id {
			A1, A2, A3, A4, A5, A6, A7, A8,
			B1, B2, B3, B4, B5, B6, B7, B8,
			C,
			UNKNOWN,
		}
		public enum ShapeType {
			STRAIGHT, RIGHT, LEFT, 
		}
		public static bool IsOuter(this Id id) {
			switch (id) {
			case Id.A1:
			case Id.A2:
			case Id.A3:
			case Id.A4:
			case Id.A5:
			case Id.A6:
			case Id.A7:
			case Id.A8:
				return true;
			}
			return false;
		}
		public static bool IsInner(this Id id) {
			switch (id) {
			case Id.B1:
			case Id.B2:
			case Id.B3:
			case Id.B4:
			case Id.B5:
			case Id.B6:
			case Id.B7:
			case Id.B8:
				return true;
			}
			return false;
		}
		public static bool IsCenter(this Id id) {
			return id == Id.C;
		}
		public static bool Is1(this Id id) {
			return id == Id.A1 || id == Id.B1;
		}
		public static bool Is2(this Id id) {
			return id == Id.A2 || id == Id.B2;
		}
		public static bool Is3(this Id id) {
			return id == Id.A3 || id == Id.B3;
		}
		public static bool Is4(this Id id) {
			return id == Id.A4 || id == Id.B4;
		}
		public static bool Is5(this Id id) {
			return id == Id.A5 || id == Id.B5;
		}
		public static bool Is6(this Id id) {
			return id == Id.A6 || id == Id.B6;
		}
		public static bool Is7(this Id id) {
			return id == Id.A7 || id == Id.B7;
		}
		public static bool Is8(this Id id) {
			return id == Id.A8 || id == Id.B8;
		}
		public static int? ToInt(this Id id) {
			if (id.Is1 ())
				return 1;
			if (id.Is2 ())
				return 2;
			if (id.Is3 ())
				return 3;
			if (id.Is4 ())
				return 4;
			if (id.Is5 ())
				return 5;
			if (id.Is6 ())
				return 6;
			if (id.Is7 ())
				return 7;
			if (id.Is8 ())
				return 8;
			return null;
		}
		public static Id ToMaipadSensorId(string script) {
			switch (script) {
			case "A1":
				return Id.A1;
			case "A2":
				return Id.A2;
			case "A3":
				return Id.A3;
			case "A4":
				return Id.A4;
			case "A5":
				return Id.A5;
			case "A6":
				return Id.A6;
			case "A7":
				return Id.A7;
			case "A8":
				return Id.A8;
			case "B1":
				return Id.B1;
			case "B2":
				return Id.B2;
			case "B3":
				return Id.B3;
			case "B4":
				return Id.B4;
			case "B5":
				return Id.B5;
			case "B6":
				return Id.B6;
			case "B7":
				return Id.B7;
			case "B8":
				return Id.B8;
			case "C":
				return Id.C;
			}
			return Id.UNKNOWN;
		}
	}
}
