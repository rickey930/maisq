﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace MsqScript {
	/// <summary>
	/// CommonScriptFormatしたものからmsq scriptの基本的なフォーマット (){}[]などで分解する
	/// </summary>
	public class Formatter {
		public enum AreaType {
			/// <summary>
			/// 親無し.
			/// </summary>
			GLOBAL,
			/// <summary>
			/// ()内.
			/// </summary>
			IN_ROUND,
			/// <summary>
			/// {}内.
			/// </summary>
			IN_CURLY,
			/// <summary>
			/// <summary>
			/// {}内かつ:後.
			/// </summary>
			IN_CURLY_AFTER_COLON,
			/// []内.
			/// </summary>
			IN_SQUARE,
		}
		
		public enum FormatterResultType {
			/// <summary>
			/// 変換成功.
			/// </summary>
			SUCCESS,
			/// <summary>
			/// 必須のコメント終了キーが見つからなかった.
			/// </summary>
			REQUIRED_COMMENT_END_SPECIAL_KEY_NOT_FOUND,
			/// <summary>
			/// 必須の文字列化終了キーが見つからなかった.
			/// </summary>
			REQUIRED_TEXT_END_SPECIAL_KEY_NOT_FOUND,
			/// <summary>
			/// 「()」の数が合わない.
			/// </summary>
			ERROR_ROUND_COUNT_NOT_MATCH,
			/// <summary>
			/// 「{}」の数が合わない.
			/// </summary>
			ERROR_CURLY_COUNT_NOT_MATCH,
			/// <summary>
			/// 「[]」の数が合わない.
			/// </summary>
			ERROR_SQUARE_COUNT_NOT_MATCH,
			/// <summary>
			/// 不適切な「:」
			/// </summary>
			ERROR_IMPROPER_COLON,
			/// <summary>
			/// 不適切な「;」
			/// </summary>
			ERROR_IMPROPER_SEMICOLON,
			/// <summary>
			/// 不適切な「,」
			/// </summary>
			ERROR_IMPROPER_CAMMA,
			/// <summary>
			/// 不適切な「.」
			/// </summary>
			ERROR_IMPROPER_PERIOD,
			/// <summary>
			/// コマンド名が異常
			/// </summary>
			ERROR_SYNTAX_NAME,
		}
		
		public class FormatterResult {
			public TextDetailInfo place { get; private set; }
			public FormatterResultType type { get; private set; }
			public FormatterResult(TextDetailInfo place, FormatterResultType type) {
				this.place = place;
				this.type = type;
			}
		}
		
		private class CreateHelper {
			/// <summary>
			/// 代表
			/// </summary>
			public TextDetailInfo representative;
			/// <summary>
			/// コマンド名.
			/// </summary>
			public List<CommonScriptFormatter> name;
			/// <summary>
			/// nameが存在する領域.
			/// </summary>
			public AreaType areaType;
			/// <summary>
			/// コマンド名はテキストである.
			/// </summary>
			public int nameIsText;
			/// <summary>
			/// <para>nameが持つ子供</para>
			/// <para>()()が、順に第1子、第2子</para>
			/// <para>子供たちは(,,,)内のパラメーター情報を持つ</para>
			/// </summary>
			private List<CreateHelperChild> childRooms;
			private int childRoomIndex;
			private TextDetailInfo childRoomRepresentative;
			private class CreateHelperChild {
				public TextDetailInfo representative;
				public List<CreateHelper> children;
				public AreaType roomType;
				public CreateHelperChild(TextDetailInfo representative, AreaType roomType) {
					this.children = new List<CreateHelper>();
					this.representative = representative;
					this.roomType = roomType;
				}
			}
			/// <summary>
			/// nameの親.
			/// </summary>
			public CreateHelper parent;
			/// <summary>
			/// ドットアクセス後.
			/// </summary>
			public CreateHelper dotNext;
			/// <summary>
			/// ドットアクセス後.
			/// </summary>
			public CreateHelper dotPrev;
			/// <summary>
			/// テーブル型のときのvalue。keyはname.
			/// </summary>
			public CreateHelper dicValue;
			
			public CreateHelper(CreateHelper parent, AreaType area) {
				name = new List<CommonScriptFormatter>();
				areaType = area;
				nameIsText = 0;
				childRooms = null;
				dotNext = null;
				dotPrev = null;
				this.parent = parent;
			}
			
			public Formatter ToEntity() {
				var ret = new Formatter();
				if (name != null) {
					if (dicValue == null) {
						ret.name = name.ToArray();
					}
					else {
						ret.dicKey = GetNameString();
						ret.dicValue = dicValue.ToEntity();
					}
				}
				else {
					ret.name = new CommonScriptFormatter[0];
				}
				ret.nameIsText = nameIsText != 0;
				if (childRooms != null) {
					int chSize = childRooms.Count;
					ret.childRooms = new MsqScriptFormatterChildRoomInfo[chSize];
					for (int i = 0; i < chSize; i++) {
						int paramSize = childRooms[i].children.Count;
						if (paramSize == 1 && childRooms[i].children[0].name.Count == 0 && childRooms[i].children[0].childRooms == null && childRooms[i].children[0].dotNext == null && childRooms[i].children[0].nameIsText == 0) {
							paramSize = 0;
						}
						var children = new Formatter[paramSize];
						for (int j = 0; j < paramSize; j++) {
							children[j] = childRooms[i].children[j].ToEntity();
						}
						ret.childRooms[i] = new MsqScriptFormatterChildRoomInfo(childRooms[i].representative, children, childRooms[i].roomType);
					}
				}
				if (dotNext != null) {
					ret.upgrade = dotNext.ToEntity();
				}
				ret.areaType = areaType;
				ret.representative = representative;
				return ret;
			}
			
			public void OpenNewChildArea(TextDetailInfo childRoomRepresentative) {
				if (childRooms == null) {
					childRooms = new List<CreateHelperChild>();
					childRoomIndex = 0;
				}
				else {
					childRoomIndex++;
				}
				this.childRoomRepresentative = childRoomRepresentative;
			}
			
			public void AddChild(CreateHelper instance) {
				if (childRooms != null) {
					if (childRooms.Count <= childRoomIndex) {
						childRooms.Add(new CreateHelperChild(childRoomRepresentative, instance.areaType));
					}
					childRooms[childRoomIndex].children.Add(instance);
				}
			}
			
			public int? GetChildRoomCount() {
				return childRooms == null ? (int?)null : childRooms.Count;
			}
			
			public List<CreateHelper> GetNowChildRoomChildren() {
				if (childRooms == null)
					return null;
				if (childRooms.Count <= childRoomIndex)
					return null;
				return childRooms[childRoomIndex].children;
			}
			
			private string nameString;
			public string GetNameString() {
				if (nameString == null && name != null) {
					var builder = new System.Text.StringBuilder();
					foreach (var ch in name) {
						builder.Append(ch.GetString());
					}
					nameString = builder.ToString();
				}
				return nameString;
			}
		}
		
		public class MsqScriptFormatterChildRoomInfo {
			/// <summary>
			/// 子供部屋の代表文字
			/// </summary>
			public TextDetailInfo representative { get; private set; }
			/// <summary>
			/// 部屋に所属する子供
			/// </summary>
			public Formatter[] children { get; private set; }
			/// <summary>
			/// 部屋は何製か
			/// </summary>
			public AreaType roomType { get; private set; }
			public MsqScriptFormatterChildRoomInfo(TextDetailInfo representative, Formatter[] children, AreaType roomType) {
				this.representative = representative;
				this.children = children;
				this.roomType = roomType;
			}
			public string debugRoomTypeString {
				get {
					return roomType.ToString();
				}
			}
		}
		
		public TextDetailInfo representative { get; private set; } // 代表
		public CommonScriptFormatter[] name { get; private set; } //コマンド名.
		public bool nameIsText { get; private set; } //コマンド名はテキストである.
		public string dicKey { get; private set; } // key:value書式である場合はコマンド名ではなくここにkeyが入る.
		public Formatter dicValue { get; private set; } // key:value書式である場合のvalue
		public MsqScriptFormatterChildRoomInfo[] childRooms { get; private set; } //name()()など. 中身はname(param,,,,)(param,,,,)
		public Formatter upgrade { get; private set; } //ドットアクセス.
		public AreaType areaType { get; private set; } //コマンド名が属するエリアタイプ.
		
		private Dictionary<string, Formatter>[] _tables; // 子供のdicKeyとdicValueをDictionaryで取得. key:value書式でない子供はnullになる.
		public Dictionary<string, Formatter>[] tables {
			get {
				if (_tables == null && childRooms != null) {
					_tables = new Dictionary<string, Formatter>[childRooms.Length];
					for (int i = 0; i < _tables.Length; i++) {
						for (int j = 0; j < childRooms[i].children.Length; j++) {
							if (childRooms[i].children[j].dicKey != null) {
								if (_tables[i] == null) {
									_tables[i] = new Dictionary<string, Formatter>();
								}
								_tables[i][childRooms[i].children[j].dicKey] = childRooms[i].children[j].dicValue;
							}
						}
					}
				}
				return _tables;
			}
		}
		
		public Formatter() {
			nameString = null;
		}
		
		private string nameString;
		public string GetNameString() {
			if (nameString == null && name != null) {
				var builder = new System.Text.StringBuilder();
				foreach (var ch in name) {
					builder.Append(ch.GetString());
				}
				nameString = builder.ToString();
			}
			return nameString;
		}
		
		public string debugNameString {
			get {
				return GetNameString();
			}
		}
		public string debugAreaTypeString {
			get {
				return areaType.ToString();
			}
		}
		
		public static Formatter[] Analyze(string text, CommonScriptFormatter.DefineSpecialText[] commentKeys, CommonScriptFormatter.DefineSpecialText[] textKeys, string[] invalidTexts, out FormatterResult result) {
			var _ret = new List<Formatter>();
			CommonScriptFormatter.ResultType analyzedResult;
			var analyzedText = CommonScriptFormatter.Analyze(text, commentKeys, textKeys, invalidTexts, out analyzedResult);
			int textSize = analyzedText.Length;
			
			if (textSize > 0) {
				if (analyzedResult == CommonScriptFormatter.ResultType.REQUIRED_COMMENT_END_SPECIAL_KEY_NOT_FOUND) {
					result = new FormatterResult(analyzedText[analyzedText.Length - 1].text[0], FormatterResultType.REQUIRED_COMMENT_END_SPECIAL_KEY_NOT_FOUND);
					return null;
				}
				if (analyzedResult == CommonScriptFormatter.ResultType.REQUIRED_TEXT_END_SPECIAL_KEY_NOT_FOUND) {
					result = new FormatterResult(analyzedText[analyzedText.Length - 1].text[0], FormatterResultType.REQUIRED_TEXT_END_SPECIAL_KEY_NOT_FOUND);
					return null;
				}
			}
			
			var nowInstance = new CreateHelper(null, AreaType.GLOBAL);
			var topLevelInstances = new List<CreateHelper>();
			CommonScriptFormatter lastCammaScript = null;
			var areaTypeOpenDic = new Dictionary<string, AreaType>() {
				{ "(", AreaType.IN_ROUND },
				{ "{", AreaType.IN_CURLY },
				{ "[", AreaType.IN_SQUARE },
			};
			var areaTypeCloseDic = new Dictionary<string, AreaType>() {
				{ ")", AreaType.IN_ROUND },
				{ "}", AreaType.IN_CURLY },
				{ "]", AreaType.IN_SQUARE },
			};
			var areaTypeCloseDic2 = new Dictionary<string, AreaType>() {
				{ "}", AreaType.IN_CURLY_AFTER_COLON },
			};
			for (int i = 0; i < textSize; i++) {
				var entity = analyzedText[i];
				bool elseThrough = true;
				
				if (entity.type == CommonScriptFormatter.TextType.SCRIPT) {
					string str = entity.GetString();
					// ( { [ である場合.
					if (areaTypeOpenDic.ContainsKey(str)) {
						nowInstance.OpenNewChildArea(entity.text[0]);
						//名前は無いが子供はいるとき、括弧の始めが代表となる
						if (nowInstance.representative == null) {
							nowInstance.representative = entity.text[0];
						}
						var instance = new CreateHelper(nowInstance, areaTypeOpenDic[str]);
						nowInstance.AddChild(instance);
						nowInstance = instance;
						elseThrough = false;
					}
					// ) } ] である場合.
					else if (areaTypeCloseDic.ContainsKey(str)) {
						if ((nowInstance.areaType == areaTypeCloseDic[str] || nowInstance.areaType == areaTypeCloseDic2[str]) && nowInstance.parent != null) {
							// ()内の余分な,はエラー. []や{}には余分にあってもOK.
							var nowChildRoomChildren = nowInstance.parent.GetNowChildRoomChildren();
							if (nowInstance.areaType == AreaType.IN_ROUND && nowInstance.name.Count == 0 && nowInstance.nameIsText == 0 && nowChildRoomChildren != null && nowChildRoomChildren.Count > 0 && nowChildRoomChildren[0].name.Count > 0) {
								// 構文エラー.
								result = new FormatterResult((lastCammaScript ?? entity).text[0], FormatterResultType.ERROR_IMPROPER_CAMMA);
								return null;
							}
							else {
								if (!areaTypeCloseDic2.ContainsKey(str) || nowInstance.areaType != areaTypeCloseDic2[str])
									nowInstance = nowInstance.parent;
								else
									nowInstance = nowInstance.parent.parent;
								elseThrough = false;
							}
						}
						else {
							// )などが(などより多い.
							var errorDic = new Dictionary<string, FormatterResultType>() {
								{ ")", FormatterResultType.ERROR_ROUND_COUNT_NOT_MATCH },
								{ "}", FormatterResultType.ERROR_CURLY_COUNT_NOT_MATCH },
								{ "]", FormatterResultType.ERROR_SQUARE_COUNT_NOT_MATCH },
							};
							result = new FormatterResult(entity.text[0], errorDic[str]);
							return null;
						}
					}
					else if (str == ",") {
						lastCammaScript = entity;
						// ()か[]内である場合、または{}内でkeyもvalueも定義した場合
						if (nowInstance.areaType != AreaType.IN_CURLY && nowInstance.areaType != AreaType.GLOBAL && nowInstance.parent != null) {
							CreateHelper instance;
							if (nowInstance.areaType != AreaType.IN_CURLY_AFTER_COLON) {
								//名前も子供もいないとき、代表は,になる
								if (nowInstance.representative == null) {
									nowInstance.representative = entity.text[0];
								}
								instance = new CreateHelper(nowInstance.parent, nowInstance.areaType);
								nowInstance.parent.AddChild(instance);
								nowInstance = instance;
								elseThrough = false;
							}
							else if (nowInstance.parent.parent != null) {
								if (nowInstance.representative == null) {
									nowInstance.representative = entity.text[0];
								}
								instance = new CreateHelper(nowInstance.parent.parent, nowInstance.parent.areaType);
								nowInstance.parent.parent.AddChild(instance);
								nowInstance = instance;
								elseThrough = false;
							}
						}
						if (elseThrough) {
							// 構文エラー.
							result = new FormatterResult(entity.text[0], FormatterResultType.ERROR_IMPROPER_CAMMA);
							return null;
						}
					}
					else if (str == ".") {
						// 前後のテキストが数字だったら小数点扱いにするためdotNextは無効だが、そうでなければdotNextを作る.
						CommonScriptFormatter before = null;
						CommonScriptFormatter after = null;
						if (i > 0)
							before = analyzedText[i - 1];
						if (i + 1 < textSize)
							after = analyzedText[i + 1];
						int hoge;
						if (before == null || after == null || !int.TryParse(before.GetString(), out hoge) || !int.TryParse(after.GetString(), out hoge)) {
							//名前も子供もいないとき、代表は.になる
							if (nowInstance.representative == null) {
								nowInstance.representative = entity.text[0];
							}
							var instance = new CreateHelper(nowInstance.parent, nowInstance.areaType);
							nowInstance.dotNext = instance;
							instance.dotPrev = nowInstance;
							nowInstance = instance;
							elseThrough = false;
							
							// ピリオドの構文エラーは無い？.
							//result = new MsqScriptFormatterResult (entity.text[0], MsqScriptFormatterResultType.ERROR_IMPROPER_PERIOD);
							//return null;
						}
					}
					else if (str == ":") {
						// {}内であり、:が重複しない場合.
						if (nowInstance.areaType == AreaType.IN_CURLY) {
							//名前も子供もいないとき、代表は:になる
							if (nowInstance.representative == null) {
								nowInstance.representative = entity.text[0];
							}
							// key:valueのvalueの親はkey.
							var instance = new CreateHelper(nowInstance, AreaType.IN_CURLY_AFTER_COLON);
							nowInstance.dicValue = instance;
							nowInstance = instance;
							elseThrough = false;
						}
						else {
							// 構文エラー.
							result = new FormatterResult(entity.text[0], FormatterResultType.ERROR_IMPROPER_COLON);
							return null;
						}
					}
					else if (str == ";") {
						if (nowInstance.areaType == AreaType.GLOBAL && nowInstance.parent == null) {
							//名前も子供もいないとき、代表は;になる
							if (nowInstance.representative == null) {
								nowInstance.representative = entity.text[0];
							}
							// ピリオドより前に戻る.
							while (nowInstance.dotPrev != null) {
								nowInstance = nowInstance.dotPrev;
							}
							topLevelInstances.Add(nowInstance);
							nowInstance = new CreateHelper(null, AreaType.GLOBAL);
							elseThrough = false;
						}
						else {
							// 構文エラー.
							result = new FormatterResult(entity.text[0], FormatterResultType.ERROR_IMPROPER_SEMICOLON);
							return null;
						}
					}
				}
				
				if (elseThrough) {
					if (entity.type == CommonScriptFormatter.TextType.SCRIPT ||
					    entity.type == CommonScriptFormatter.TextType.TEXT) {
						// "n"ame みたいなのはエラー.
						if (nowInstance.nameIsText >= 2) {
							// 構文エラー.
							result = new FormatterResult(entity.text[0], FormatterResultType.ERROR_SYNTAX_NAME);
							return null;
						}
						else if (!nowInstance.GetChildRoomCount().HasValue) {
							nowInstance.name.Add(entity);
							// 名前が1文字でもあったとき、代表がまだ決まっていなければそこを代表とする
							if (nowInstance.representative == null) {
								nowInstance.representative = entity.text[0];
							}
						}
						// 子供部屋があるのに再び親名を定義しようとしたらエラー.
						// name()over; 的なのがダメになるようにする.
						else {
							// 構文エラー.
							result = new FormatterResult(entity.text[0], FormatterResultType.ERROR_SYNTAX_NAME);
							return null;
						}
					}
					else if (entity.type == CommonScriptFormatter.TextType.SPECIAL) {
						if (nowInstance.nameIsText % 2 == 0) {
							bool isTextKey = false;
							if (textKeys != null) {
								foreach (var key in textKeys) {
									if (entity.GetString() == key.start) {
										isTextKey = true;
										break;
									}
								}
							}
							if (isTextKey) {
								// 文字列化キーだった場合、2度目だったらエラー.
								if (nowInstance.nameIsText > 0) {
									// 構文エラー.
									result = new FormatterResult(entity.text[0], FormatterResultType.ERROR_SYNTAX_NAME);
									return null;
								}
								// 文字列化キーだった場合、それがコマンド名の始めにあるならば、コマンド名はテキストですフラグを建てる.
								else if (nowInstance.name.Count == 0) {
									nowInstance.nameIsText++;
									// 代表がまだ決まっていなければそこを代表とする
									if (nowInstance.representative == null) {
										nowInstance.representative = entity.text[0];
									}
								}
								// 始めではないのなら構文エラー.
								else {
									// 構文エラー.
									result = new FormatterResult(entity.text[0], FormatterResultType.ERROR_SYNTAX_NAME);
									return null;
								}
							}
						}
						else {
							bool isTextKey = false;
							if (textKeys != null) {
								foreach (var key in textKeys) {
									if (entity.GetString() == key.end) {
										isTextKey = true;
										break;
									}
								}
							}
							if (isTextKey) {
								nowInstance.nameIsText++;
							}
						}
					}
				}
			}
			
			foreach (var instance in topLevelInstances) {
				_ret.Add(instance.ToEntity());
			}
			result = new FormatterResult(null, FormatterResultType.SUCCESS);
			return _ret.ToArray();
		}
		
		
	}
}
