﻿using System.Collections.Generic;

namespace MsqScript {
	public class Function : Object {
		public override Type type { get { return Type.FUNCTION; } }
		public override Object Clone() { return new Function(entity); }
		public override string Log() {
			return "MsqScript.Function(" + entity.ToString() + ")";
		}
		public Formatter[] entity;
		public Function(params Formatter[] data) {
			entity = data; //.Clone()を使いたいかも。
		}
		public Object Call(CallerInfo caller, params Object[] arguments) {
			foreach (var s in entity) {
				var obj = Analyze(caller, s, arguments);
				if (obj.IsException) {
					return obj;
				}
				else if (obj.type == Type.RETURN) {
					return obj.Cast<Return>().entity;
				}
			}
			return new Void();
		}
		public Object Analyze(CallerInfo caller, Formatter script, params Object[] arguments) {
			string command = script.GetNameString();
			Object ret;
			var self = caller.CalledGlobal(script, script.representative);
			var global = caller.global;
			if (script.childRooms == null || script.childRooms.Length == 0) {
				ret = global.Parse(self);
			}
			else if (script.childRooms[0].roomType == Formatter.AreaType.IN_SQUARE) {
				ret = Nest(self, script.childRooms[0].children, arguments, Document.ARRAY, true);
			}
			else if (script.childRooms[0].roomType == Formatter.AreaType.IN_CURLY) {
				ret = CreateTable(self, script, arguments);
			}
			else if (command == Document.FUNCTION) {
				// functionは子を実行するのではなく単に処理内容を記憶する
				ret = new Function(script.childRooms[0].children);
			}
			else {
				ret = Nest(self, script.childRooms[0].children, arguments, command);
			}
			// アップグレードする.
			// 連続した(){}[]から見てみる
			if (!ret.IsException && script.childRooms != null) {
				for (int roomIndex = 1; roomIndex < script.childRooms.Length; roomIndex++) {
					var roomType = script.childRooms[roomIndex].roomType;
					var commandType = ret.type;
					var place = script.childRooms[roomIndex].representative;
					string commandName = string.Empty;
					if (roomType == Formatter.AreaType.IN_ROUND) {
						if (commandType == Type.FUNCTION) {
							// .call()の省略形となる
							commandName = Document.CALL;
						}
						else {
							// function以外の()アクセスはエラー
							ret = new Exception(caller, ErrorCode.INVALID_ROUND_BRACKET_ACCESS);
							break;
						}
					}
					else if (roomType == Formatter.AreaType.IN_SQUARE) {
						if (commandType == Type.ARRAY || commandType == Type.TABLE) {
							// .get()の省略形となる
							commandName = Document.GET;
						}
						else {
							// arrayかdictionary以外の[]アクセスはエラー
							ret = new Exception(caller, ErrorCode.INVALID_SQUARE_BRACKET_ACCESS);
							break;
						}
					}
					else {
						// index1以降の{}はいかなる場合もエラー
						ret = new Exception(caller, ErrorCode.INVALID_CURLY_BRACKET_ACCESS);
						break;
					}
					if (commandName != string.Empty) {
						var upgrader = caller.ExtendGlobal(script, place, ret);
						ret = Nest(upgrader, script.childRooms[roomIndex].children, arguments, commandName);
					}
				}
			}
			// .アクセスを見てみる
			Formatter upgrading = script.upgrade;
			while (!ret.IsException && upgrading != null) {
				if (upgrading.childRooms != null && upgrading.childRooms.Length > 0) {
					var upgrader = caller.ExtendGlobal(script, upgrading.representative, ret);
					ret = Nest(upgrader, upgrading.childRooms[0].children, arguments, upgrading.GetNameString());
					upgrading = upgrading.upgrade;
				}
				else {
					upgrading = null;
				}
			}
			return ret;
		}
		private Object Nest(CallerInfo nester, Formatter[] children, Object[] callArguments, string commandName, bool sqareArray = false) {
			Object ret = null;
			var global = nester.global;
			var args = new List<Object>();
			int index = 0;
			foreach (var child in children) {
				// []配列生成のとき最後のカンマオンリーは無視
				if (sqareArray) {
					if (index == children.Length - 1 && string.IsNullOrEmpty(child.GetNameString())) {
						index++;
						continue;
					}
				}
				var arg = Analyze(nester, child, callArguments);
				if (!arg.IsException) {
					args.Add(arg);
				}
				else {
					// エラーだった.
					ret = arg;
					break;
				}
				index++;
			}
			if (ret == null) {
				ret = global.ExecuteCommand(nester, commandName, args.ToArray());
				// 引数型であるならば、call時に呼ばれた引数を返す.
				if (ret.type == Type.ARGUMENT) {
					int argIndex = ret.Cast<Argument>().index;
					if (argIndex >= 0 && argIndex < callArguments.Length) {
						ret = callArguments[argIndex];
					}
					else {
						ret = new Exception(nester, ErrorCode.INDEX_OUT_OF_ARGUMENTS);
					}
				}
			}
			return ret;
		}
		private Object CreateTable(CallerInfo customer, Formatter script, Object[] callArguments) {
			Object ret = null;
			var arg = new Dictionary<string, Object>();
			int index = 0;
			foreach (var key in script.tables[0].Keys) {
				// {}連想配列生成のとき最後のカンマオンリーは無視
				if (index == script.tables[0].Keys.Count - 1 && string.IsNullOrEmpty(key) && string.IsNullOrEmpty(script.tables[0][key].GetNameString())) {
					index++;
					continue;
				}
				var value = Analyze(customer, script.tables[0][key], callArguments);
				if (!value.IsException) {
					arg[key] = value;
				}
				else {
					// エラーだった.
					ret = value;
					break;
				}
				index++;
			}
			if (ret == null) {
				ret = new Table(arg);
			}
			return ret;
		}
	}
}