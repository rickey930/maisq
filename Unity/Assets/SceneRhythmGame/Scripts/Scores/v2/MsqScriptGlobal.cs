﻿using System.Collections.Generic;

namespace MsqScript {
	public class Global : Object {
		public override Type type { get { return Type.GLOBAL; } }
		public override Object Clone() { return new Global() { variant = variant }; }
		public override string Log() {
			return "MsqScript.Global()";
		}
		public Dictionary<string, Object> variant;
		private Command command;
		// MaimaiScoreReader はコイツがもつ。または持たずともスコアが作れればいい。
		public Global() {
			variant = new Dictionary<string, Object>();
			command = new Command();
		}
		public void Recycle() {
			variant.Clear();
		}
		public void Release() {
			variant = null;
			command = null;
		}
		public Object ExecuteCommand(CallerInfo caller, string name, params Object[] arguments) {
			return command.ExecuteCommand(caller, name, arguments);
		}
		public Object Parse(CallerInfo caller) {
			string s = caller.script.GetNameString();
			if (caller.script.nameIsText) {
				return new Text(s);
			}
			int number;
			if (int.TryParse(s, out number)) {
				return new Number(number);
			}
			float value;
			if (float.TryParse(s, out value)) {
				return new Value(value);
			}
			switch (s.ToLower()) {
			case Document.TRUE:
			case Document.YES:
			case Document.ON:
				return new Flag(true);
			case Document.FALSE:
			case Document.NO:
			case Document.OFF:
				return new Flag(false);
			}
			return new Exception(caller, ErrorCode.PARSE_ERROR);
		}
		public Object Analyze(string script, out Formatter.FormatterResult formatResult) {
			var detail = Formatter.Analyze(script, //null, null, null, out result);
			                               new CommonScriptFormatter.DefineSpecialText[] {
				new CommonScriptFormatter.DefineSpecialText("/*", "*/", true),
				new CommonScriptFormatter.DefineSpecialText("//", "\n", false),
			},
			new CommonScriptFormatter.DefineSpecialText[] {
				new CommonScriptFormatter.DefineSpecialText("\"", "\"", true),
				new CommonScriptFormatter.DefineSpecialText("\'", "\'", true),
			},
			new string[] { "\r", "\n", " ", "\t", "　" },
			out formatResult);
			var caller = new CallerInfo(this);
			
			if (formatResult.type != Formatter.FormatterResultType.SUCCESS) {
				// フォーマットエラーだった
				return new Exception(caller, ErrorCode.FORMAT_ERROR);
			}
			else if (detail != null && detail.Length == 0) {
				// データが存在しなかった
				return new Exception(caller, ErrorCode.EMPTY);
			}
			var analyzer = new Function(detail);
			var analyzeResult = analyzer.Call(caller);
			// 例外なら例外情報を返す
			if (analyzeResult.IsException) {
				return analyzeResult;
			}
			// returnだったらreturnしたオブジェクトを返す
			else if (analyzeResult.type == Type.RETURN) {
				return analyzeResult.Cast<Return>().entity;
			}
			// それ以外なら成功したとみなしてSuccessを返す
			return new Success();
		}
		
		public Object Bpm(CallerInfo caller, IMsqValue bpm) {
			return new Void();
		}
		public Object Beat(CallerInfo caller, IMsqValue beat) {
			return new Void();
		}
		public Object Interval(CallerInfo caller, IMsqValue interval) {
			return new Void();
		}
		public Object Note(CallerInfo caller, params INote[] notes) {
			return new Void();
		}
		public Object Rest(CallerInfo caller) {
			return Rest(caller, new Number(1));
		}
		public Object Rest(CallerInfo caller, Number amount) {
			return new Void();
		}
		public Object Log(CallerInfo caller, Object target) {
			UnityEngine.Debug.Log(target.Log());
			return new Void();
		}
		public Object Get(CallerInfo caller, Text key) {
			if (variant.ContainsKey(key.entity)) {
				return variant[key.entity];
			}
			return new Exception(caller, ErrorCode.UNDEFINED_VARIANT);
		}
		public Object Set(CallerInfo caller, Text key, Object value) {
			variant[key.entity] = value;
			return new Void();
		}
		public Object IsDefined(CallerInfo caller, Text key) {
			return new Flag(variant.ContainsKey(key.entity));
		}
		public Object Simai(CallerInfo caller, Text script) {
			return new Void();
		}
		public Object Add(CallerInfo caller, Number a0, Number a1) {
			return new Number(a0.entity + a1.entity);
		}
		public Object Add(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Value(a0.value + a1.value);
		}
		public Object Sub(CallerInfo caller, Number a0, Number a1) {
			return new Number(a0.entity - a1.entity);
		}
		public Object Sub(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Value(a0.value - a1.value);
		}
		public Object Mul(CallerInfo caller, Number a0, Number a1) {
			return new Number(a0.entity * a1.entity);
		}
		public Object Mul(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Value(a0.value * a1.value);
		}
		public Object Div(CallerInfo caller, Number a0, Number a1) {
			if (a1.entity != 0) {
				return new Number(a0.entity / a1.entity);
			}
			return new Exception(caller, ErrorCode.DEVIDE_ZERO);
		}
		public Object Div(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			if (a1.value != 0) {
				return new Value(a0.value / a1.value);
			}
			return new Exception(caller, ErrorCode.DEVIDE_ZERO);
		}
		public Object Pow(CallerInfo caller, Number a0, Number a1) {
			return new Number((int)UnityEngine.Mathf.Pow(a0.entity, a1.entity));
		}
		public Object Pow(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Value(UnityEngine.Mathf.Pow(a0.value, a1.value));
		}
		public Object Mod(CallerInfo caller, Number a0, Number a1) {
			if (a1.entity != 0) {
				return new Number(a0.entity % a1.entity);
			}
			return new Exception(caller, ErrorCode.DEVIDE_ZERO);
		}
		public Object Equal(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Flag(a0.value == a1.value);
		}
		public Object NotEqual(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Flag(a0.value != a1.value);
		}
		public Object Greater(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Flag(a0.value > a1.value);
		}
		public Object Less(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Flag(a0.value < a1.value);
		}
		public Object GreaterEqual(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Flag(a0.value >= a1.value);
		}
		public Object LessEqual(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Flag(a0.value <= a1.value);
		}
		public Object BitAnd(CallerInfo caller, Number a0, Number a1) {
			return new Number(a0.entity & a1.entity);
		}
		public Object BitOr(CallerInfo caller, Number a0, Number a1) {
			return new Number(a0.entity | a1.entity);
		}
		public Object BitXor(CallerInfo caller, Number a0, Number a1) {
			return new Number(a0.entity ^ a1.entity);
		}
		public Object BitNot(CallerInfo caller, Number a0) {
			return new Number(~a0.entity);
		}
		public Object BitShiftLeft(CallerInfo caller, Number a0, Number a1) {
			return new Number(a0.entity << a1.entity);
		}
		public Object BitShiftRight(CallerInfo caller, Number a0, Number a1) {
			return new Number(a0.entity >> a1.entity);
		}
		public Object Not(CallerInfo caller, Flag a0) {
			return new Flag(!a0.entity);
		}
		public Object Concat(CallerInfo caller, Text a0, Text a1) {
			return new Text(a0.entity + a1.entity);
		}
		public Object Equal(CallerInfo caller, Text a0, Text a1) {
			return new Flag(a0.entity == a1.entity);
		}
		public Object NotEqual(CallerInfo caller, Text a0, Text a1) {
			return new Flag(a0.entity != a1.entity);
		}
		public Object Pos(CallerInfo caller, IMsqValue a0, IMsqValue a1) {
			return new Position(a0, a1);
		}
		public Object Equal(CallerInfo caller, Position a0, Position a1) {
			return new Flag(a0.entity == a1.entity);
		}
		public Object NotEqual(CallerInfo caller, Position a0, Position a1) {
			return new Flag(a0.entity != a1.entity);
		}
		public Object RGB255(CallerInfo caller, Number r, Number g, Number b) {
			return new Color(r.entity, g.entity, b.entity, 255);
		}
		public Object ARGB255(CallerInfo caller, Number a, Number r, Number g, Number b) {
			return new Color(r.entity, g.entity, b.entity, a.entity);
		}
		public Object RGBA255(CallerInfo caller, Number r, Number g, Number b, Number a) {
			return new Color(r.entity, g.entity, b.entity, a.entity);
		}
		public Object RGB(CallerInfo caller, IMsqValue r, IMsqValue g, IMsqValue b) {
			return new Color(r.value, g.value, b.value, 1.0f);
		}
		public Object ARGB(CallerInfo caller, IMsqValue a, IMsqValue r, IMsqValue g, IMsqValue b) {
			return new Color(r.value, g.value, b.value, a.value);
		}
		public Object RGBA(CallerInfo caller, IMsqValue r, IMsqValue g, IMsqValue b, IMsqValue a) {
			return new Color(r.value, g.value, b.value, a.value);
		}
		public Object Black(CallerInfo caller) {
			return new Color(UnityEngine.Color.black);
		}
		public Object White(CallerInfo caller) {
			return new Color(UnityEngine.Color.white);
		}
		public Object Gray(CallerInfo caller) {
			return new Color(UnityEngine.Color.gray);
		}
		public Object Red(CallerInfo caller) {
			return new Color(UnityEngine.Color.red);
		}
		public Object Green(CallerInfo caller) {
			return new Color(UnityEngine.Color.green);
		}
		public Object Blue(CallerInfo caller) {
			return new Color(UnityEngine.Color.blue);
		}
		public Object Yellow(CallerInfo caller) {
			return new Color(UnityEngine.Color.yellow);
		}
		public Object Magenta(CallerInfo caller) {
			return new Color(UnityEngine.Color.magenta);
		}
		public Object Cyan(CallerInfo caller) {
			return new Color(UnityEngine.Color.cyan);
		}
		public Object SensorDegree(CallerInfo caller, Number buttonId) {
			var button = buttonId.ToButton();
			return new Value(Constants.instance.GetPieceDegree (button.GetIndex()));
		}
		public Object SensorDegreeDistanceClockwise(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			var button1 = fromButtonId.ToButton();
			var button2 = toButtonId.ToButton();
			int startSensor = button1.GetIndex();
			int targetSensor = button2.GetIndex();
			if (startSensor > targetSensor) {
				float startDeg = Constants.instance.GetPieceDegree (startSensor);
				float targetDeg = Constants.instance.GetPieceDegree (targetSensor) + 360f;
				return new Value(targetDeg - startDeg);
			}
			else if (startSensor < targetSensor) {
				float startDeg = Constants.instance.GetPieceDegree (startSensor);
				float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
				return new Value(targetDeg - startDeg);
			}
			return new Value(360.0f);
		}
		public Object SensorDegreeDistanceCounterClockwise(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			var button1 = fromButtonId.ToButton();
			var button2 = toButtonId.ToButton();
			int startSensor = button1.GetIndex();
			int targetSensor = button2.GetIndex();
			if (startSensor > targetSensor) {
				float startDeg = Constants.instance.GetPieceDegree (startSensor);
				float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
				return new Value(-(startDeg - targetDeg));
			}
			else if (startSensor < targetSensor) {
				float startDeg = Constants.instance.GetPieceDegree (startSensor) + 360f;
				float targetDeg = Constants.instance.GetPieceDegree (targetSensor);
				return new Value(-(startDeg - targetDeg));
			}
			return new Value(-360.0f);
		}
		public Object SensorDegreeDistanceRight(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			var button1 = fromButtonId.ToButton();
			if (button1.IsTopSide()) {
				return SensorDegreeDistanceClockwise(caller, fromButtonId, toButtonId);
			}
			return SensorDegreeDistanceCounterClockwise(caller, fromButtonId, toButtonId);
		}
		public Object SensorDegreeDistanceLeft(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			var button1 = fromButtonId.ToButton();
			if (button1.IsTopSide()) {
				return SensorDegreeDistanceCounterClockwise(caller, fromButtonId, toButtonId);
			}
			return SensorDegreeDistanceClockwise(caller, fromButtonId, toButtonId);
		}
		public Object AngleX(CallerInfo caller, IMsqValue degree, IMsqValue radius) {
			var pos = Angle(caller, degree, radius).Cast<Position>();
			return new Value(pos.entity.x);
		}
		public Object AngleY(CallerInfo caller, IMsqValue degree, IMsqValue radius) {
			var pos = Angle(caller, degree, radius).Cast<Position>();
			return new Value(pos.entity.y);
		}
		public Object AngleX(CallerInfo caller, IMsqValue degree) {
			var pos = Angle(caller, degree).Cast<Position>();
			return new Value(pos.entity.x);
		}
		public Object AngleY(CallerInfo caller, IMsqValue degree) {
			var pos = Angle(caller, degree).Cast<Position>();
			return new Value(pos.entity.y);
		}
		public Object OuterSensorPositionX(CallerInfo caller, Number buttonId) {
			var pos = OuterSensorPosition(caller, buttonId).Cast<Position>();
			return new Value(pos.entity.x);
		}
		public Object OuterSensorPositionY(CallerInfo caller, Number buttonId) {
			var pos = OuterSensorPosition(caller, buttonId).Cast<Position>();
			return new Value(pos.entity.y);
		}
		public Object InnerSensorPositionX(CallerInfo caller, Number buttonId) {
			var pos = InnerSensorPosition(caller, buttonId).Cast<Position>();
			return new Value(pos.entity.x);
		}
		public Object InnerSensorPositionY(CallerInfo caller, Number buttonId) {
			var pos = InnerSensorPosition(caller, buttonId).Cast<Position>();
			return new Value(pos.entity.y);
		}
		public Object CenterSensorPositionX(CallerInfo caller) {
			var pos = CenterSensorPosition(caller).Cast<Position>();
			return new Value(pos.entity.x);
		}
		public Object CenterSensorPositionY(CallerInfo caller) {
			var pos = CenterSensorPosition(caller).Cast<Position>();
			return new Value(pos.entity.y);
		}
		public Object OuterSensorRadius(CallerInfo caller) {
			return new Value(1.0f);
		}
		public Object InnerSensorRadius (CallerInfo caller) {
			var clossExLines = CircleCalculator.LinesIntersect(
				CircleCalculator.PointOnCircle(UnityEngine.Vector2.zero, 1, Constants.instance.GetPieceDegree(0)),
				CircleCalculator.PointOnCircle(UnityEngine.Vector2.zero, 1, Constants.instance.GetPieceDegree(3)),
				CircleCalculator.PointOnCircle(UnityEngine.Vector2.zero, 1, Constants.instance.GetPieceDegree(6)),
				CircleCalculator.PointOnCircle(UnityEngine.Vector2.zero, 1, Constants.instance.GetPieceDegree(2))
				);
			var radius = CircleCalculator.PointToPointDistance(UnityEngine.Vector2.zero, clossExLines);
			return new Value(radius);
		}
		public Object CenterSensorRadius(CallerInfo caller) {
			return new Value(0.0f);
		}
		public Object SimaiPPTurnDegree(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			var start = fromButtonId.ToButton();
			var target = toButtonId.ToButton();
			int complementary1 = ((target.GetIndex() + 16 - (start.GetIndex() + 3)) % 8 - 7) * -1;
			
			int complementary2 = (complementary1 - 4) * -1;
			int complementary3;
			if (complementary2 == 0) {
				complementary3 = 0;
			}
			else {
				complementary3 = (int)(UnityEngine.Mathf.Pow (2, UnityEngine.Mathf.Abs (complementary2) - 1));
			}
			
			float ret = -270.0f - (22.5f * (0xf & complementary3) * (complementary2 < 0 ? 1 : -1)) - (45.0f / 8.0f * complementary1);
			
			return new Value(ret);
		}
		public Object SimaiQQTurnDegree(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			var start = fromButtonId.ToButton();
			var target = toButtonId.ToButton();
			int complementary1 = ((target.GetIndex() + 16 - (start.GetIndex() - 2)) % 8);
			
			int complementary2 = (complementary1 - 4) * -1;
			int complementary3;
			if (complementary2 == 0) {
				complementary3 = 0;
			}
			else {
				complementary3 = (int)(UnityEngine.Mathf.Pow (2, UnityEngine.Mathf.Abs (complementary2) - 1));
			}
			
			float ret = 270.0f - (22.5f * (0xf & complementary3) * (complementary2 < 0 ? -1 : 1)) + (45.0f / 8.0f * complementary1);
			
			return new Value(ret);
		}
		public Object Angle(CallerInfo caller, IMsqValue degree, IMsqValue radius) {
			// 引数は右手座標だが、Unityでは回転が左手座標系だとか0度は(-1.0,0)であることだとかを考慮して、90度回したりy値をマイナスにしたりして加工する.
			return new Position(
				UnityEngine.Mathf.Sin(CircleCalculator.ToRadian(degree.value + 90)) * radius.value, 
				-UnityEngine.Mathf.Cos(CircleCalculator.ToRadian(degree.value + 270)) * radius.value
				);
		}
		public Object Angle(CallerInfo caller, IMsqValue degree) {
			return Angle(caller, degree, new Value(1.0f));
		}
		public Object OuterSensorPosition(CallerInfo caller, Number buttonId) {
			var button = buttonId.ToButton();
			var pos = CircleCalculator.PointOnCircle(UnityEngine.Vector2.zero, 1, Constants.instance.GetPieceDegree(button.GetIndex()));
			return new Position(pos.x, -pos.y);
		}
		public Object InnerSensorPosition(CallerInfo caller, Number buttonId) {
			var button = buttonId.ToButton();
			var radius = InnerSensorRadius(caller).Cast<Value>().entity;
			var pos = CircleCalculator.PointOnCircle(UnityEngine.Vector2.zero, radius, Constants.instance.GetPieceDegree(button.GetIndex()));
			return new Position(new UnityEngine.Vector2(pos.x, -pos.y));
		}
		public Object CenterSensorPosition(CallerInfo caller) {
			return new Position(UnityEngine.Vector2.zero);
		}
		/// <summary>
		/// ppスライドにおいて始点から直線を引く際に目指す位置.
		/// </summary>
		/// <param name="start">始点</param>
		public Object SimaiPPFirstStraightTargetPosition(CallerInfo caller, Number fromButtonId) {
			return Angle(caller, SensorDegree(caller, fromButtonId).Cast<Value>().Sub(caller, new Value(SimaiPPQQFirstStraightTargetPositionAdjustDegree)).Cast<Value>(), new Value(SimaiPPQQFirstStraightTargetPositionRadius));
		}
		/// <summary>
		/// qqスライドにおいて始点から直線を引く際に目指す位置.
		/// </summary>
		/// <param name="start">始点</param>
		public Object SimaiQQFirstStraightTargetPosition(CallerInfo caller, Number fromButtonId) {
			return Angle(caller, SensorDegree(caller, fromButtonId).Cast<Value>().Add(caller, new Value(SimaiPPQQFirstStraightTargetPositionAdjustDegree)).Cast<Value>(), new Value(SimaiPPQQFirstStraightTargetPositionRadius));
		}
		/// <summary>
		/// simai内部におけるppスライドやqqスライドの中心点のA～H。 □に8つの頂点を均等に並べたとき、1番が右上で、時計回りに番号が増える.
		/// </summary>
		/// <param name="button">始点</param>
		public Object SimaiPPQQCenterPosition(CallerInfo caller, Number buttonId) {
			var button = buttonId.ToButton();
			var radius = 0.5f; //(205.5f - 140.0f) / 140.0f;
			var pos = CircleCalculator.PointOnCircle(UnityEngine.Vector2.zero, radius, 45.0f * button.GetId ());
			return new Position(pos.x, -pos.y);
		}
		/// <summary>
		/// ppスライドにおいて円を描く際に必要とする中心点.
		/// </summary>
		/// <param name="button">始点</param>
		public Object SimaiPPCenterPosition(CallerInfo caller, Number buttonId) {
			var button = buttonId.ToButton();
			return SimaiPPQQCenterPosition(caller, new Number((button.GetIndex() + 1) % 8 + 1));
		}
		/// <summary>
		/// qqスライドにおいて円を描く際に必要とする中心点.
		/// </summary>
		/// <param name="button">始点</param>
		public Object SimaiQQCenterPosition(CallerInfo caller, Number buttonId) {
			var button = buttonId.ToButton();
			return SimaiPPQQCenterPosition(caller, new Number((button.GetIndex() + 6) % 8 + 1));
		}
		public Object Tap(CallerInfo caller, Number buttonId) {
			return new NoteTap(buttonId.entity);
		}
		public Object Hold(CallerInfo caller, Number buttonId, IStep step) {
			return new NoteHold(buttonId.entity, step);
		}
		public Object Slide(CallerInfo caller, ISlideHead head, ISlideWait wait, IStep step, params ISlidePattern[] patterns) {
			var ret = new NoteSlide(head, wait, step, patterns);
			if (!ret.IsException) {
				if (ret.IsRisky()) {
					if (ret.isMissingHeadOfFirstContinuedSlideCommand) {
						return new Exception(caller, ErrorCode.HEAD_OF_FIRST_CONTINUED_SLIDE_COMMAND_IS_MISSING);
					}
					else {
						return new Exception(caller, ErrorCode.LACK_OF_SLIDE_INFORMATION);
					}
				}
				return ret.trimmed;
			}
			return ret;
		}
		public Object Slide(CallerInfo caller, params ISlidePattern[] patterns) {
			return Slide(caller, (ISlideHead)null, null, null, patterns);
		}
		public Object Slide(CallerInfo caller, ISlideHead head, params ISlidePattern[] patterns) {
			return Slide(caller, head, null, null, patterns);
		}
		public Object Slide(CallerInfo caller, ISlideHead head, ISlideWait wait, params ISlidePattern[] patterns) {
			return Slide(caller, head, wait, null, patterns);
		}
		public Object Slide(CallerInfo caller, ISlideHead head, IStep step, params ISlidePattern[] patterns) {
			return Slide(caller, head, null, step, patterns);
		}
		public Object Slide(CallerInfo caller, ISlideWait wait, params ISlidePattern[] patterns) {
			return Slide(caller, (ISlideHead)null, wait, null, patterns);
		}
		public Object Slide(CallerInfo caller, ISlideWait wait, IStep step, params ISlidePattern[] patterns) {
			return Slide(caller, (ISlideHead)null, wait, step, patterns);
		}
		public Object Slide(CallerInfo caller, IStep step, params ISlidePattern[] patterns) {
			return Slide(caller, (ISlideHead)null, null, step, patterns);
		}
		public Object Slide(CallerInfo caller, INote note, ISlideWait wait, IStep step, params ISlidePattern[] patterns) {
			ISlideHead head = null;
			if (note.type == Type.NOTE_TAP) 
				head = new SlideHeadStar(note.Cast<NoteTap>().button.GetId());
			else if (note.type == Type.NOTE_BREAK)
				head = new SlideHeadBreak(note.Cast<NoteBreak>().button.GetId());
			if (head != null) {
				return Slide (caller, head, wait, step, patterns);
			}
			return new Exception(caller, ErrorCode.INVALID_SLIDE_HEAD);
		}
		public Object Slide(CallerInfo caller, INote note, params ISlidePattern[] patterns) {
			return Slide (caller, note, null, null, patterns);
		}
		public Object Slide(CallerInfo caller, INote note, ISlideWait wait, params ISlidePattern[] patterns) {
			return Slide (caller, note, wait, null, patterns);
		}
		public Object Slide(CallerInfo caller, INote note, IStep step, params ISlidePattern[] patterns) {
			return Slide (caller, note, null, step, patterns);
		}
		public Object Break(CallerInfo caller, Number buttonId) {
			return new NoteBreak(buttonId.entity);
		}
		public Object Message(CallerInfo caller, Text message, Position position, IMsqValue scale, Color color, IStep step) {
			return new NoteMessage(message, position, scale, color, step);
		}
		public Object ScrollMessage(CallerInfo caller, Text message, IMsqValue y, IMsqValue scale, Color color, IStep step) {
			return new NoteScrollMessage(message, y, scale, color, step);
		}
		public Object SoundMessage(CallerInfo caller, Text soundId) {
			return new NoteSoundMessage(soundId);
		}
		public Object Step(CallerInfo caller, IMsqValue beat, IMsqValue length) {
			return new StepBasic(beat, length);
		}
		public Object Step(CallerInfo caller, IMsqValue interval) {
			return new StepInterval(interval);
		}
		public Object Step(CallerInfo caller, IMsqValue bpm, IMsqValue beat, IMsqValue length) {
			return new StepLocalBpm(bpm, beat, length);
		}
		public Object Star(CallerInfo caller, Number buttonId) {
			return new SlideHeadStar(buttonId);
		}
		public Object BreakStar(CallerInfo caller, Number buttonId) {
			return new SlideHeadBreak(buttonId);
		}
		public Object WaitCustomBpm(CallerInfo caller, IMsqValue bpm) {
			return new SlideWaitLocalBpm(bpm);
		}
		public Object WaitCustomInterval(CallerInfo caller, IMsqValue interval) {
			return new SlideWaitInterval(interval);
		}
		public Object WaitCustomStep(CallerInfo caller, IMsqValue beat, IMsqValue length) {
			return new SlideWaitStepBasic(beat, length);
		}
		public Object WaitCustomStep(CallerInfo caller, IMsqValue bpm, IMsqValue beat, IMsqValue length) {
			return new SlideWaitStepLocalBpm(bpm, beat, length);
		}
		public Object Pattern(CallerInfo caller, ISlideWait wait, IStep step, params ISlideChain[] chains) {
			return new SlidePattern(wait, step, chains);
		}
		public Object Pattern(CallerInfo caller, params ISlideChain[] chains) {
			return new SlidePattern(null, null, chains);
		}
		public Object Pattern(CallerInfo caller, ISlideWait wait, params ISlideChain[] chains) {
			return new SlidePattern(wait, null, chains);
		}
		public Object Pattern(CallerInfo caller, IStep step, params ISlideChain[] chains) {
			return new SlidePattern(null, step, chains);
		}
		public Object Chain(CallerInfo caller, ISlideWait wait, IStep step, params ISlideCommand[] commands) {
			return new SlideChain(wait, step, commands);
		}
		public Object Chain(CallerInfo caller, params ISlideCommand[] commands) {
			return new SlideChain(null, null, commands);
		}
		public Object Chain(CallerInfo caller, ISlideWait wait, params ISlideCommand[] commands) {
			return new SlideChain(wait, null, commands);
		}
		public Object Chain(CallerInfo caller, IStep step, params ISlideCommand[] commands) {
			return new SlideChain(null, step, commands);
		}
		public Object Straight(CallerInfo caller, Position start, Position target) {
			return new SlideCommandStraight(start, target);
		}
		public Object Curve(CallerInfo caller, Position center, IMsqValue radius, IMsqValue startDegree, IMsqValue distanceDegree) {
			return new SlideCommandCurve(center, new Position(radius, radius), startDegree, distanceDegree);
		}
		public Object ContinuedStraight(CallerInfo caller, Position target) {
			return new SlideCommandContinuedStraight(target);
		}
		public Object ContinuedCurve(CallerInfo caller, Position center, IMsqValue distanceDegree) {
			return new SlideCommandContinuedCurve(center, distanceDegree);
		}
		public Object OuterStraightOuter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return new SlideCommandStraight(
				OuterSensorPosition(caller, fromButtonId).Cast<Position>(),
				OuterSensorPosition(caller, toButtonId).Cast<Position>()
			);
		}
		public Object OuterStraightInner(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return new SlideCommandStraight(
				OuterSensorPosition(caller, fromButtonId).Cast<Position>(),
				InnerSensorPosition(caller, toButtonId).Cast<Position>()
			);
		}
		public Object OuterStraightCenter(CallerInfo caller, Number fromButtonId) {
			return new SlideCommandStraight(
				OuterSensorPosition(caller, fromButtonId).Cast<Position>(),
				CenterSensorPosition(caller).Cast<Position>()
			);
		}
		public Object InnerStraightOuter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return new SlideCommandStraight(
				InnerSensorPosition(caller, fromButtonId).Cast<Position>(),
				OuterSensorPosition(caller, toButtonId).Cast<Position>()
			);
		}
		public Object InnerStraightInner(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return new SlideCommandStraight(
				InnerSensorPosition(caller, fromButtonId).Cast<Position>(),
				InnerSensorPosition(caller, toButtonId).Cast<Position>()
			);
		}
		public Object InnerStraightCenter(CallerInfo caller, Number fromButtonId) {
			return new SlideCommandStraight(
				InnerSensorPosition(caller, fromButtonId).Cast<Position>(),
				CenterSensorPosition(caller).Cast<Position>()
			);
		}
		public Object CenterStraightOuter(CallerInfo caller, Number toButtonId) {
			return new SlideCommandStraight(
				CenterSensorPosition(caller).Cast<Position>(),
				OuterSensorPosition(caller, toButtonId).Cast<Position>()
			);
		}
		public Object CenterStraightInner(CallerInfo caller, Number toButtonId) {
			return new SlideCommandStraight(
				CenterSensorPosition(caller).Cast<Position>(),
				InnerSensorPosition(caller, toButtonId).Cast<Position>()
			);
		}
		public Object CenterStraightCenter(CallerInfo caller) {
			return new SlideCommandStraight(
				CenterSensorPosition(caller).Cast<Position>(),
				CenterSensorPosition(caller).Cast<Position>()
				);
		}
		public Object OuterCurveClockwiseOuterAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return new SlideCommandCurve(
				CenterSensorPosition(caller).Cast<Position>(),
				OuterSensorRadius(caller).Cast<Value>(),
				SensorDegree(caller, fromButtonId).Cast<Value>(),
				SensorDegreeDistanceClockwise(caller, fromButtonId, toButtonId).Cast<Value>()
			);
		}
		public Object OuterCurveCounterClockwiseOuterAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return new SlideCommandCurve(
				CenterSensorPosition(caller).Cast<Position>(),
				OuterSensorRadius(caller).Cast<Value>(),
				SensorDegree(caller, fromButtonId).Cast<Value>(),
				SensorDegreeDistanceCounterClockwise(caller, fromButtonId, toButtonId).Cast<Value>()
				);
		}
		public Object OuterCurveRightOuterAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return new SlideCommandCurve(
				CenterSensorPosition(caller).Cast<Position>(),
				OuterSensorRadius(caller).Cast<Value>(),
				SensorDegree(caller, fromButtonId).Cast<Value>(),
				SensorDegreeDistanceRight(caller, fromButtonId, toButtonId).Cast<Value>()
				);
		}
		public Object OuterCurveLeftOuterAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return new SlideCommandCurve(
				CenterSensorPosition(caller).Cast<Position>(),
				OuterSensorRadius(caller).Cast<Value>(),
				SensorDegree(caller, fromButtonId).Cast<Value>(),
				SensorDegreeDistanceLeft(caller, fromButtonId, toButtonId).Cast<Value>()
				);
		}
		public Object InnerCurveClockwiseInnerAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return new SlideCommandCurve(
				CenterSensorPosition(caller).Cast<Position>(),
				InnerSensorRadius(caller).Cast<Value>(),
				SensorDegree(caller, fromButtonId).Cast<Value>(),
				SensorDegreeDistanceClockwise(caller, fromButtonId, toButtonId).Cast<Value>()
				);
		}
		public Object InnerCurveCounterClockwiseInnerAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return new SlideCommandCurve(
				CenterSensorPosition(caller).Cast<Position>(),
				InnerSensorRadius(caller).Cast<Value>(),
				SensorDegree(caller, fromButtonId).Cast<Value>(),
				SensorDegreeDistanceCounterClockwise(caller, fromButtonId, toButtonId).Cast<Value>()
				);
		}
		public Object InnerCurveRightInnerAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return new SlideCommandCurve(
				CenterSensorPosition(caller).Cast<Position>(),
				InnerSensorRadius(caller).Cast<Value>(),
				SensorDegree(caller, fromButtonId).Cast<Value>(),
				SensorDegreeDistanceRight(caller, fromButtonId, toButtonId).Cast<Value>()
				);
		}
		public Object InnerCurveLeftInnerAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			return new SlideCommandCurve(
				CenterSensorPosition(caller).Cast<Position>(),
				InnerSensorRadius(caller).Cast<Value>(),
				SensorDegree(caller, fromButtonId).Cast<Value>(),
				SensorDegreeDistanceLeft(caller, fromButtonId, toButtonId).Cast<Value>()
				);
		}
		public Object ShapeQAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			//if (t==(s+4)%8) As-B((s+2)%8)-At
			//else As-B((s+2)%8)>B((t+6)%8)-At
			var ret = new List<ISlideCommandArrayContent>();
			ISlideCommandArrayContent instance;
			var button1 = fromButtonId.ToButton();
			var continueButtonId1 = MsqHelperShapeQStartTo(button1);
			instance = OuterStraightInner(caller, fromButtonId, continueButtonId1).Cast<SlideCommandStraight>();
			ret.Add(instance);
			var button2 = toButtonId.ToButton();
			if (MsqHelperShapePOrShapeQIsStraightToTarget(button1, button2)) {
				instance = InnerStraightOuter(caller, continueButtonId1, toButtonId).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			else {
				var continueButtonId2 = MsqHelperShapeQFromTarget(button2);
				instance = InnerCurveClockwiseInnerAxisCenter(caller, continueButtonId1, continueButtonId2).Cast<SlideCommandCurve>();
				ret.Add (instance);
				instance = InnerStraightOuter(caller, continueButtonId2, toButtonId).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			return new SlideCommandArray(ret);
		}
		public Object ShapePAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			//if (t==(s+4)%8) As-B((s+6)%8)-At
			//else As-B((s+6)%8)<B((t+2)%8)-At
			var ret = new List<ISlideCommandArrayContent>();
			ISlideCommandArrayContent instance;
			var button1 = fromButtonId.ToButton();
			var continueButtonId1 = MsqHelperShapePStartTo(button1);
			instance = OuterStraightInner(caller, fromButtonId, continueButtonId1).Cast<SlideCommandStraight>();
			ret.Add(instance);
			var button2 = toButtonId.ToButton();
			if (MsqHelperShapePOrShapeQIsStraightToTarget(button1, button2)) {
				instance = InnerStraightOuter(caller, continueButtonId1, toButtonId).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			else {
				var continueButtonId2 = MsqHelperShapePFromTarget(button2);
				instance = InnerCurveCounterClockwiseInnerAxisCenter(caller, continueButtonId1, continueButtonId2).Cast<SlideCommandCurve>();;
				ret.Add (instance);
				instance = InnerStraightOuter(caller, continueButtonId2, toButtonId).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			return new SlideCommandArray(ret);
		}
		public Object ShapeZAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			//As-B((s+6)%8)-B((t+2)%8)-At
			var button1 = fromButtonId.ToButton();
			var button2 = toButtonId.ToButton();
			var continueButtonId1 = new Number((button1.GetIndex() + 8 - 2) % 8 + 1);
			var continueButtonId2 = new Number((button2.GetIndex() + 8 - 2) % 8 + 1);
			return new SlideCommandArray(
				OuterStraightInner(caller, fromButtonId, continueButtonId1).Cast<SlideCommandStraight>(),
				InnerStraightInner(caller, continueButtonId1, continueButtonId2).Cast<SlideCommandStraight>(),
				InnerStraightOuter(caller, continueButtonId2, toButtonId).Cast<SlideCommandStraight>()
			);
		}
		public Object ShapeVAxisCenter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			//As-C-At
			return new SlideCommandArray(
				OuterStraightCenter(caller, fromButtonId).Cast<SlideCommandStraight>(),
				CenterStraightOuter(caller, toButtonId).Cast<SlideCommandStraight>()
			);
		}
		public Object ShapeQQ(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			var ret = new List<ISlideCommandArrayContent>();
			ISlideCommandArrayContent instance;
			//As-C
			instance = new SlideCommandStraight(
				OuterSensorPosition(caller, fromButtonId).Cast<Position>(), // Start A
				SimaiQQFirstStraightTargetPosition(caller, fromButtonId).Cast<Position>() // ほぼ中心.
				);
			ret.Add(instance);
			// Turn
			instance = new SlideCommandContinuedCurve(
				SimaiQQCenterPosition(caller, fromButtonId).Cast<Position>(),
				SimaiQQTurnDegree(caller, fromButtonId, toButtonId).Cast<Value>()
				);
			ret.Add (instance);
			// - At
			instance = new SlideCommandContinuedStraight(
				OuterSensorPosition(caller, toButtonId).Cast<Position>() // Target A
				);
			ret.Add (instance);
			return new SlideCommandArray(ret);
		}
		public Object ShapePP(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			var ret = new List<ISlideCommandArrayContent>();
			ISlideCommandArrayContent instance;
			//As-C
			instance = new SlideCommandStraight(
				OuterSensorPosition(caller, fromButtonId).Cast<Position>(), // Start A
				SimaiPPFirstStraightTargetPosition(caller, fromButtonId).Cast<Position>() // ほぼ中心.
			);
			ret.Add(instance);
			// Turn
			instance = new SlideCommandContinuedCurve(
				SimaiPPCenterPosition(caller, fromButtonId).Cast<Position>(),
				SimaiPPTurnDegree(caller, fromButtonId, toButtonId).Cast<Value>()
			);
			ret.Add (instance);
			// - At
			instance = new SlideCommandContinuedStraight(
				OuterSensorPosition(caller, toButtonId).Cast<Position>() // Target A
			);
			ret.Add (instance);
			return new SlideCommandArray(ret);
		}
		public Object ShapeVAxisOuter(CallerInfo caller, Number fromButtonId, Number turnButtonId, Number toButtonId) {
			//As-Ap-At
			return new SlideCommandArray(
				OuterStraightOuter(caller, fromButtonId, turnButtonId).Cast<SlideCommandStraight>(),
				OuterStraightOuter(caller, turnButtonId, toButtonId).Cast<SlideCommandStraight>()
			);
		}
		public Object ShapeW(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			//As-A(t-1),As-At,As-A(t+1)
			return new SlideChainArray(
				OuterStraightOuter(caller, fromButtonId, toButtonId.ToButton().Turn(caller, new Number(-1)).Cast<Button>().ToNumber()).Cast<SlideCommandStraight>(),
				OuterStraightOuter(caller, fromButtonId, toButtonId).Cast<SlideCommandStraight>(),
				OuterStraightOuter(caller, fromButtonId, toButtonId.ToButton().Turn(caller, new Number(1)).Cast<Button>().ToNumber()).Cast<SlideCommandStraight>()
			);
		}
#region 要検証
		public Object OuterCurveClockwiseInner(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			//if (t==(s+4)%8) As-B((s+2)%8)-Bt
			//else As-B((s+2)%8)>Bt
			var ret = new List<ISlideCommandArrayContent>();
			ISlideCommandArrayContent instance;
			var button1 = fromButtonId.ToButton();
			var button2 = MsqHelperShapePFromTarget(toButtonId.ToButton()).ToButton();
			var As = fromButtonId;
			var Bs = MsqHelperShapeQStartTo(button1);
			var Bt = toButtonId;
			instance = OuterStraightInner(caller, As, Bs).Cast<SlideCommandStraight>();
			ret.Add(instance);
			if (MsqHelperShapePOrShapeQIsStraightToTarget(button1, button2)) {
				instance = InnerStraightOuter(caller, Bs, Bt).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			else {
				instance = InnerCurveClockwiseInnerAxisCenter(caller, Bs, Bt).Cast<SlideCommandCurve>();
				ret.Add (instance);
			}
			return new SlideCommandArray(ret);
		}
		public Object OuterCurveCounterClockwiseInner(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			//if (t==(s+4)%8) As-B((s+6)%8)-Bt
			//else As-B((s+6)%8)<B((t+2)%8)
			var ret = new List<ISlideCommandArrayContent>();
			ISlideCommandArrayContent instance;
			var button1 = fromButtonId.ToButton();
			var button2 = MsqHelperShapeQFromTarget(toButtonId.ToButton()).ToButton();
			var As = fromButtonId;
			var Bs = MsqHelperShapePStartTo(button1);
			var Bt = toButtonId;
			instance = OuterStraightInner(caller, As, Bs).Cast<SlideCommandStraight>();
			ret.Add(instance);
			if (MsqHelperShapePOrShapeQIsStraightToTarget(button1, button2)) {
				instance = InnerStraightOuter(caller, Bs, Bt).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			else {
				instance = InnerCurveCounterClockwiseInnerAxisCenter(caller, Bs, Bt).Cast<SlideCommandCurve>();;
				ret.Add (instance);
			}
			return new SlideCommandArray(ret);
		}
		public Object OuterCurveRightInner(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			if (fromButtonId.ToButton ().IsTopSide ()) {
				return OuterCurveClockwiseInner(caller, fromButtonId, toButtonId);
			}
			return OuterCurveCounterClockwiseInner(caller, fromButtonId, toButtonId);
		}
		public Object OuterCurveLeftInner(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			if (fromButtonId.ToButton ().IsTopSide ()) {
				return OuterCurveCounterClockwiseInner(caller, fromButtonId, toButtonId);
			}
			return OuterCurveClockwiseInner(caller, fromButtonId, toButtonId);
		}
		public Object InnerCurveClockwiseOuter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			//if (t==(s+4)%8) Bs-At
			//else Bs>B((t+6)%8)-At
			var ret = new List<ISlideCommandArrayContent>();
			ISlideCommandArrayContent instance;
			var button1 = MsqHelperShapePStartTo(fromButtonId.ToButton()).ToButton();
			var button2 = toButtonId.ToButton();
			var Bs = fromButtonId;
			var At = toButtonId;
			if (MsqHelperShapePOrShapeQIsStraightToTarget(button1, button2)) {
				instance = InnerStraightOuter(caller, Bs, At).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			else {
				var Bt = MsqHelperShapeQFromTarget(button2);
				instance = InnerCurveClockwiseInnerAxisCenter(caller, Bs, Bt).Cast<SlideCommandCurve>();
				ret.Add (instance);
				instance = InnerStraightOuter(caller, Bt, At).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			return new SlideCommandArray(ret);
		}
		public Object InnerCurveCounterClockwiseOuter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			//if (t==(s+4)%8) Bs-At
			//else Bs<B((t+2)%8)-At
			var ret = new List<ISlideCommandArrayContent>();
			ISlideCommandArrayContent instance;
			var button1 = MsqHelperShapeQStartTo(fromButtonId.ToButton()).ToButton();
			var button2 = toButtonId.ToButton();
			var Bs = fromButtonId;
			var At = toButtonId;
			if (MsqHelperShapePOrShapeQIsStraightToTarget(button1, button2)) {
				instance = InnerStraightOuter(caller, Bs, At).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			else {
				var Bt = MsqHelperShapePFromTarget(button2);
				instance = InnerCurveCounterClockwiseInnerAxisCenter(caller, Bs, Bt).Cast<SlideCommandCurve>();;
				ret.Add (instance);
				instance = InnerStraightOuter(caller, Bt, At).Cast<SlideCommandStraight>();
				ret.Add (instance);
			}
			return new SlideCommandArray(ret);
		}
		public Object InnerCurveRightOuter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			if (fromButtonId.ToButton ().IsTopSide ()) {
				return InnerCurveClockwiseOuter(caller, fromButtonId, toButtonId);
			}
			return InnerCurveCounterClockwiseOuter(caller, fromButtonId, toButtonId);
		}
		public Object InnerCurveLeftOuter(CallerInfo caller, Number fromButtonId, Number toButtonId) {
			if (fromButtonId.ToButton ().IsTopSide ()) {
				return InnerCurveCounterClockwiseOuter(caller, fromButtonId, toButtonId);
			}
			return InnerCurveClockwiseOuter(caller, fromButtonId, toButtonId);
		}
		public Object OuterCurveClockwiseCenter(CallerInfo caller, Number fromButtonId) {
			var ret = new List<ISlideCommandArrayContent>();
			ISlideCommandArrayContent instance;
			//As-C
			instance = new SlideCommandStraight(
				OuterSensorPosition(caller, fromButtonId).Cast<Position>(), // Start A
				CenterSensorPosition(caller).Cast<Position>()
				);
			ret.Add(instance);
			// Turn
			instance = new SlideCommandContinuedCurve(
				SimaiQQCenterPosition(caller, fromButtonId).Cast<Position>(),
				new Value(360)
				);
			ret.Add (instance);
			return new SlideCommandArray(ret);
		}
		public Object OuterCurveCounterClockwiseCenter(CallerInfo caller, Number fromButtonId) {
			var ret = new List<ISlideCommandArrayContent>();
			ISlideCommandArrayContent instance;
			//As-C
			instance = new SlideCommandStraight(
				OuterSensorPosition(caller, fromButtonId).Cast<Position>(), // Start A
				CenterSensorPosition(caller).Cast<Position>()
				);
			ret.Add(instance);
			// Turn
			instance = new SlideCommandContinuedCurve(
				SimaiPPCenterPosition(caller, fromButtonId).Cast<Position>(),
				new Value(-360)
				);
			ret.Add (instance);
			return new SlideCommandArray(ret);
		}
		public Object OuterCurveRightCenter(CallerInfo caller, Number fromButtonId) {
			if (fromButtonId.ToButton ().IsTopSide ()) {
				return OuterCurveClockwiseCenter(caller, fromButtonId);
			}
			return OuterCurveCounterClockwiseCenter(caller, fromButtonId);
		}
		public Object OuterCurveLeftCenter(CallerInfo caller, Number fromButtonId) {
			if (fromButtonId.ToButton ().IsTopSide ()) {
				return OuterCurveCounterClockwiseCenter(caller, fromButtonId);
			}
			return OuterCurveClockwiseCenter(caller, fromButtonId);
		}
		public Object CenterCurveClockwiseOuter(CallerInfo caller, Number toButtonId) {
			var ret = new List<ISlideCommandArrayContent>();
			ISlideCommandArrayContent instance;
			// Turn
			var center = SimaiQQCenterPosition(caller, toButtonId).Cast<Position>();
			instance = new SlideCommandCurve(
				center,
				new Value(CircleCalculator.PointToPointDistance(center.entity, UnityEngine.Vector2.zero)),
				new Value(CircleCalculator.PointToDegree(center.entity, UnityEngine.Vector2.zero)),
				SimaiQQTurnDegree(caller, toButtonId, toButtonId).Cast<Value>()
				);
			ret.Add (instance);
			// - At
			instance = new SlideCommandContinuedStraight(
				OuterSensorPosition(caller, toButtonId).Cast<Position>() // Target A
				);
			ret.Add (instance);
			return new SlideCommandArray(ret);
		}
		public Object CenterCurveCounterClockwiseOuter(CallerInfo caller, Number toButtonId) {
			var ret = new List<ISlideCommandArrayContent>();
			ISlideCommandArrayContent instance;
			// Turn
			var center = SimaiPPCenterPosition(caller, toButtonId).Cast<Position>();
			instance = new SlideCommandCurve(
				center,
				new Value(CircleCalculator.PointToPointDistance(center.entity, UnityEngine.Vector2.zero)),
				new Value(CircleCalculator.PointToDegree(center.entity, UnityEngine.Vector2.zero)),
				SimaiPPTurnDegree(caller, toButtonId, toButtonId).Cast<Value>()
				);
			ret.Add (instance);
			// - At
			instance = new SlideCommandContinuedStraight(
				OuterSensorPosition(caller, toButtonId).Cast<Position>() // Target A
				);
			ret.Add (instance);
			return new SlideCommandArray(ret);
		}
		public Object CenterCurveRightOuter(CallerInfo caller, Number toButtonId) {
			if (toButtonId.ToButton ().IsTopSide ()) {
				return CenterCurveClockwiseOuter(caller, toButtonId);
			}
			return CenterCurveCounterClockwiseOuter(caller, toButtonId);
		}
		public Object CenterCurveLeftOuter(CallerInfo caller, Number toButtonId) {
			if (toButtonId.ToButton ().IsTopSide ()) {
				return CenterCurveCounterClockwiseOuter(caller, toButtonId);
			}
			return CenterCurveClockwiseOuter(caller, toButtonId);
		}
		public Object InnerCurveClockwiseCenter(CallerInfo caller, Number fromButtonId) {
			var ret = new List<ISlideCommandArrayContent>();
			ISlideCommandArrayContent instance;
			//Bs-C
			instance = new SlideCommandStraight(
				InnerSensorPosition(caller, fromButtonId).Cast<Position>(), // Start A
				CenterSensorPosition(caller).Cast<Position>()
				);
			ret.Add(instance);
			// Turn
			instance = new SlideCommandContinuedCurve(
				SimaiQQCenterPosition(caller, fromButtonId).Cast<Position>(),
				new Value(360)
				);
			ret.Add (instance);
			return new SlideCommandArray(ret);
		}
		public Object InnerCurveCounterClockwiseCenter(CallerInfo caller, Number fromButtonId) {
			var ret = new List<ISlideCommandArrayContent>();
			ISlideCommandArrayContent instance;
			//Bs-C
			instance = new SlideCommandStraight(
				InnerSensorPosition(caller, fromButtonId).Cast<Position>(), // Start A
				CenterSensorPosition(caller).Cast<Position>()
				);
			ret.Add(instance);
			// Turn
			instance = new SlideCommandContinuedCurve(
				SimaiPPCenterPosition(caller, fromButtonId).Cast<Position>(),
				new Value(-360)
				);
			ret.Add (instance);
			return new SlideCommandArray(ret);
		}
		public Object InnerCurveRightCenter(CallerInfo caller, Number fromButtonId) {
			if (fromButtonId.ToButton ().IsTopSide ()) {
				return InnerCurveClockwiseCenter(caller, fromButtonId);
			}
			return InnerCurveCounterClockwiseCenter(caller, fromButtonId);
		}
		public Object InnerCurveLeftCenter(CallerInfo caller, Number fromButtonId) {
			if (fromButtonId.ToButton ().IsTopSide ()) {
				return InnerCurveCounterClockwiseCenter(caller, fromButtonId);
			}
			return InnerCurveClockwiseCenter(caller, fromButtonId);
		}
		public Object CenterCurveClockwiseInner(CallerInfo caller, Number toButtonId) {
			var ret = new List<ISlideCommandArrayContent>();
			ISlideCommandArrayContent instance;
			// Turn
			var center = SimaiQQCenterPosition(caller, toButtonId).Cast<Position>();
			instance = new SlideCommandCurve(
				center,
				new Value(CircleCalculator.PointToPointDistance(center.entity, UnityEngine.Vector2.zero)),
				new Value(CircleCalculator.PointToDegree(center.entity, UnityEngine.Vector2.zero)),
				SimaiQQTurnDegree(caller, toButtonId, toButtonId).Cast<Value>()
				);
			ret.Add (instance);
			// - At
			instance = new SlideCommandContinuedStraight(
				InnerSensorPosition(caller, toButtonId).Cast<Position>() // Target A
				);
			ret.Add (instance);
			return new SlideCommandArray(ret);
		}
		public Object CenterCurveCounterClockwiseInner(CallerInfo caller, Number toButtonId) {
			var ret = new List<ISlideCommandArrayContent>();
			ISlideCommandArrayContent instance;
			// Turn
			var center = SimaiPPCenterPosition(caller, toButtonId).Cast<Position>();
			instance = new SlideCommandCurve(
				center,
				new Value(CircleCalculator.PointToPointDistance(center.entity, UnityEngine.Vector2.zero)),
				new Value(CircleCalculator.PointToDegree(center.entity, UnityEngine.Vector2.zero)),
				SimaiPPTurnDegree(caller, toButtonId, toButtonId).Cast<Value>()
				);
			ret.Add (instance);
			// - At
			instance = new SlideCommandContinuedStraight(
				InnerSensorPosition(caller, toButtonId).Cast<Position>() // Target A
				);
			ret.Add (instance);
			return new SlideCommandArray(ret);
		}
		public Object CenterCurveRightInner(CallerInfo caller, Number toButtonId) {
			if (toButtonId.ToButton ().IsTopSide ()) {
				return CenterCurveClockwiseInner(caller, toButtonId);
			}
			return CenterCurveCounterClockwiseInner(caller, toButtonId);
		}
		public Object CenterCurveLeftInner(CallerInfo caller, Number toButtonId) {
			if (toButtonId.ToButton ().IsTopSide ()) {
				return CenterCurveCounterClockwiseInner(caller, toButtonId);
			}
			return CenterCurveClockwiseInner(caller, toButtonId);
		}
#endregion
		public Object ShapeMaipadDirectStraight(CallerInfo caller, MaipadSensor.Id start, MaipadSensor.Id target) {
			if (start.IsOuter()) {
				if (target.IsOuter())
					return OuterStraightOuter(caller, new Number(start.ToInt().Value), new Number(target.ToInt().Value));
				if (target.IsInner())
					return OuterStraightInner(caller, new Number(start.ToInt().Value), new Number(target.ToInt().Value));
				if (target.IsCenter())
					return OuterStraightCenter(caller, new Number(start.ToInt().Value));
			}
			if (start.IsInner ()) {
				if (target.IsOuter())
					return InnerStraightOuter(caller, new Number(start.ToInt().Value), new Number(target.ToInt().Value));
				if (target.IsInner())
					return InnerStraightInner(caller, new Number(start.ToInt().Value), new Number(target.ToInt().Value));
				if (target.IsCenter())
					return InnerStraightCenter(caller, new Number(start.ToInt().Value));
			}
			if (start.IsCenter ()) {
				if (target.IsOuter())
					return CenterStraightOuter(caller, new Number(target.ToInt().Value));
				if (target.IsInner())
					return CenterStraightInner(caller, new Number(target.ToInt().Value));
			}
			return CenterStraightCenter(caller);
		}
		public Object ShapeMaipadDirectCurveRight(CallerInfo caller, MaipadSensor.Id start, MaipadSensor.Id target) {
			if (start.IsOuter()) {
				if (target.IsOuter())
					return OuterCurveRightOuterAxisCenter(caller, new Number(start.ToInt().Value), new Number(target.ToInt().Value));
				if (target.IsInner())
					return OuterCurveRightInner(caller, new Number(start.ToInt().Value), new Number(target.ToInt().Value));
				if (target.IsCenter())
					return OuterCurveRightCenter(caller, new Number(start.ToInt().Value));
			}
			if (start.IsInner ()) {
				if (target.IsOuter())
					return InnerCurveRightOuter(caller, new Number(start.ToInt().Value), new Number(target.ToInt().Value));
				if (target.IsInner())
					return InnerCurveRightInnerAxisCenter(caller, new Number(start.ToInt().Value), new Number(target.ToInt().Value));
				if (target.IsCenter())
					return InnerCurveRightCenter(caller, new Number(start.ToInt().Value));
			}
			if (start.IsCenter ()) {
				if (target.IsOuter())
					return CenterCurveRightOuter(caller, new Number(target.ToInt().Value));
				if (target.IsInner())
					return CenterCurveRightInner(caller, new Number(target.ToInt().Value));
			}
			return CenterStraightCenter(caller);
		}
		public Object ShapeMaipadDirectCurveLeft(CallerInfo caller, MaipadSensor.Id start, MaipadSensor.Id target) {
			if (start.IsOuter()) {
				if (target.IsOuter())
					return OuterCurveLeftOuterAxisCenter(caller, new Number(start.ToInt().Value), new Number(target.ToInt().Value));
				if (target.IsInner())
					return OuterCurveLeftInner(caller, new Number(start.ToInt().Value), new Number(target.ToInt().Value));
				if (target.IsCenter())
					return OuterCurveLeftCenter(caller, new Number(start.ToInt().Value));
			}
			if (start.IsInner ()) {
				if (target.IsOuter())
					return InnerCurveLeftOuter(caller, new Number(start.ToInt().Value), new Number(target.ToInt().Value));
				if (target.IsInner())
					return InnerCurveLeftInnerAxisCenter(caller, new Number(start.ToInt().Value), new Number(target.ToInt().Value));
				if (target.IsCenter())
					return InnerCurveLeftCenter(caller, new Number(start.ToInt().Value));
			}
			if (start.IsCenter ()) {
				if (target.IsOuter())
					return CenterCurveLeftOuter(caller, new Number(target.ToInt().Value));
				if (target.IsInner())
					return CenterCurveLeftInner(caller, new Number(target.ToInt().Value));
			}
			return CenterStraightCenter(caller);
		}
		public Object ShapeMaipadDirectSlide(CallerInfo caller, MaipadSensor.Id start, MaipadSensor.ShapeType type, MaipadSensor.Id target) {
			if (type == MaipadSensor.ShapeType.RIGHT)
				return ShapeMaipadDirectCurveRight(caller, start, target);
			if (type == MaipadSensor.ShapeType.LEFT)
				return ShapeMaipadDirectCurveLeft(caller, start, target);
			return ShapeMaipadDirectStraight(caller, start, target);
		}
		public Object Call(CallerInfo caller, Function function, params Object[] arguments) {
			return function.Call(caller, arguments);
		}
		public Object Call(CallerInfo caller, Text functionName, params Object[] arguments) {
			var saved = Get(caller, functionName);
			if (saved.IsException)
				return saved;
			if (saved.type == Type.FUNCTION)
				return saved.Cast<Function>().Call(caller, arguments);
			return new Exception(caller, ErrorCode.TYPE_MISMATCH);
		}
		public Object Return(CallerInfo caller, Object returnObject) {
			return new Return(returnObject);
		}
		public Object Return(CallerInfo caller, Object[] returnObjects) {
			return new Return(new Array(returnObjects));
		}
		public Object Arg(CallerInfo caller, Number index) {
			return new Argument(index.entity);
		}
		public Object Array(CallerInfo caller, Object[] objects) {
			return new Array(objects);
		}
		public Object Table(CallerInfo caller) {
			return new Table();
		}
		public Object Turn(CallerInfo caller, IButtonTurnable turnable, Number amount) {
			return turnable.Turn(caller, amount);
		}
		public Object Turn(CallerInfo caller, IFreeTurnable turnable, IMsqValue degree) {
			return turnable.Turn (caller, degree);
		}
		public Object HMirror(CallerInfo caller, IMirrorable mirrable) {
			return mirrable.MirrorHolizontal(caller);
		}
		public Object VMirror(CallerInfo caller, IMirrorable mirrable) {
			return mirrable.MirrorVertical(caller);
		}
		public Object If(CallerInfo caller, Flag condition, Function yes, Function no) {
			if (condition.entity) {
				yes.Call(caller);
			}
			else {
				no.Call(caller);
			}
			return new Void();
		}
		public Object While(CallerInfo caller, Function condition, Function content) {
			int counter = 0;
			bool checkerError;
			while (MsqHelperWhileConditionChecker(caller, condition, out checkerError)) {
				counter++;
				content.Call(caller);
				if (counter == int.MaxValue) {
					return new Exception(caller, ErrorCode.INFINITE_LOOP);
				}
			}
			if (checkerError)
				return new Exception(caller, ErrorCode.LOOP_CONDITION_TYPE_MISMATCH);
			return new Void();
		}
		public Object Until(CallerInfo caller, Function condition, Function content) {
			int counter = 0;
			bool checkerError;
			while (MsqHelperUntilConditionChecker(caller, condition, out checkerError)) {
				counter++;
				content.Call(caller);
				if (counter == int.MaxValue) {
					return new Exception(caller, ErrorCode.INFINITE_LOOP);
				}
			}
			if (checkerError)
				return new Exception(caller, ErrorCode.LOOP_CONDITION_TYPE_MISMATCH);
			return new Void();
		}
		public Object DoWhile(CallerInfo caller, Function condition, Function content) {
			int counter = 0;
			bool checkerError;
			do {
				counter++;
				content.Call(caller);
				if (counter == int.MaxValue) {
					return new Exception(caller, ErrorCode.INFINITE_LOOP);
				}
			} while (MsqHelperWhileConditionChecker(caller, condition, out checkerError));
			if (checkerError)
				return new Exception(caller, ErrorCode.LOOP_CONDITION_TYPE_MISMATCH);
			return new Void();
		}
		public Object DoUntil(CallerInfo caller, Function condition, Function content) {
			int counter = 0;
			bool checkerError;
			do {
				counter++;
				content.Call(caller);
				if (counter == int.MaxValue) {
					return new Exception(caller, ErrorCode.INFINITE_LOOP);
				}
			} while (MsqHelperUntilConditionChecker(caller, condition, out checkerError));
			if (checkerError)
				return new Exception(caller, ErrorCode.LOOP_CONDITION_TYPE_MISMATCH);
			return new Void();
		}
		// 変数にアクセスできない.
		public Object For(CallerInfo caller, Number times, Function content) {
			for (int i = 0; i < times.entity; i++) {
				content.Call(caller);
			}
			return new Void();
		}
		// 変数にアクセスできる.
		public Object For(CallerInfo caller, Text variantName, Number times, Function content) {
			int counter = 0;
			var index = new Number(0);
			for (variant[variantName.entity] = index; index.entity < times.entity; index.entity++) {
				counter++;
				content.Call(caller);
				if (counter == int.MaxValue) {
					return new Exception(caller, ErrorCode.INFINITE_LOOP);
				}
			}
			return new Void();
		}
		// for条件を改造できる.
		public Object For(CallerInfo caller, Function initialize, Function condition, Function update, Function content) {
			int counter = 0;
			bool checkerError;
			for (initialize.Call(caller); MsqHelperWhileConditionChecker(caller, condition, out checkerError); update.Call(caller)) {
				counter++;
				content.Call(caller);
				if (counter == int.MaxValue) {
					return new Exception(caller, ErrorCode.INFINITE_LOOP);
				}
			}
			if (checkerError)
				return new Exception(caller, ErrorCode.LOOP_CONDITION_TYPE_MISMATCH);
			return new Void();
		}
		
		private const float SimaiPPQQFirstStraightTargetPositionAdjustDegree = 22.5f * 0.65f;
		private const float SimaiPPQQFirstStraightTargetPositionRadius = 0.15f;
		/// <summary>
		/// start - 2 のボタンを得る.
		/// </summary>
		private static Number MsqHelperShapePStartTo(Button start) {
			return new Number((start.GetIndex() + 8 - 2) % 8 + 1);
		}
		/// <summary>
		/// start + 2 のボタンを得る.
		/// </summary>
		private static Number MsqHelperShapePFromTarget(Button target) {
			return new Number((target.GetIndex() + 8 + 2) % 8 + 1);
		}
		/// <summary>
		/// start + 2 のボタンを得る.
		/// </summary>
		private static Number MsqHelperShapeQStartTo(Button start) {
			return MsqHelperShapePFromTarget(start);
		}
		/// <summary>
		/// start - 2 のボタンを得る.
		/// </summary>
		private static Number MsqHelperShapeQFromTarget(Button target) {
			return MsqHelperShapePStartTo(target);
		}
		/// <summary>
		/// 回転せず直線のみで構成する条件を達成するか.
		/// </summary>
		private static bool MsqHelperShapePOrShapeQIsStraightToTarget(Button start, Button target) {
			return target.GetIndex () == (start.GetIndex () + 4) % 8;
		}
		/// <summary>
		/// Functionの返り値がFlagでありそれがtrueであるか.
		/// </summary>
		private static bool MsqHelperWhileConditionChecker(CallerInfo caller, Function func, out bool notFlagType) {
			var data = func.Call(caller);
			notFlagType = data.type != Type.FLAG;
			if (!notFlagType)
				return data.Cast<Flag>().entity;
			return false;
		}
		/// <summary>
		/// Functionの返り値がFlagでありそれがfalseであるか.
		/// </summary>
		private static bool MsqHelperUntilConditionChecker(CallerInfo caller, Function func, out bool notFlagType) {
			var data = func.Call(caller);
			notFlagType = data.type != Type.FLAG;
			if (!notFlagType)
				return !data.Cast<Flag>().entity;
			return false;
		}
	}
}
