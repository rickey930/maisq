﻿using System;
using System.Collections.Generic;

namespace MsqScript {
	public enum Type {
		GLOBAL,
		FUNCTION,
		ARGUMENT,
		RETURN,
		VOID,
		ARRAY,
		TABLE,
		NUMBER,
		VALUE,
		FLAG,
		TEXT,
		POSITION,
		COLOR,
		BUTTON,
		NOTE_TAP,
		NOTE_HOLD,
		NOTE_SLIDE,
		NOTE_BREAK,
		NOTE_MESSAGE,
		NOTE_SCROLL_MESSAGE,
		NOTE_SOUND_MESSAGE,
		STEP_BASIC,
		STEP_INTERVAL,
		STEP_LOCALBPM,
		SLIDE_HEAD_STAR,
		SLIDE_HEAD_BREAK,
		SLIDE_HEAD_NOTHING,
		SLIDE_WAIT_DEFAULT,
		SLIDE_WAIT_INTERVAL,
		SLIDE_WAIT_LOCALBPM,
		SLIDE_WAIT_STEP_BASIC,
		SLIDE_WAIT_STEP_LOCALBPM,
		SLIDE_PATTERN,
		SLIDE_CHAIN,
		SLIDE_CHAIN_ARRAY,
		SLIDE_COMMAND_STRAIGHT,
		SLIDE_COMMAND_CURVE,
		SLIDE_COMMAND_CONTINUED_STRAIGHT,
		SLIDE_COMMAND_CONTINUED_CURVE,
		SLIDE_COMMAND_ARRAY,
		SUCCESS,
		EXCEPTION,
		ARGUMENTS_NOT_MATCH_EXCEPTION,
		COMMAND_NOT_FOUND_EXCEPTION,
	}
	public enum ErrorCode {
		/// <summary>
		/// データが無い
		/// </summary>
		EMPTY,
		/// <summary>
		/// フォーマットエラーなのでMsqScriptFormatter.MsqScriptFormatterResultを参照せよ
		/// </summary>
		FORMAT_ERROR,
		/// <summary>
		/// 値に変換できなかった
		/// </summary>
		PARSE_ERROR,
		/// <summary>
		/// コマンド名が見つからなかった
		/// </summary>
		COMMAND_NOT_FOUND,
		/// <summary>
		/// 引数が適切なコマンドが見つからなかった
		/// </summary>
		COMMAND_AUGUMENTS_NOT_MATCH,
		/// <summary>
		/// 無効な()アクセス
		/// </summary>
		INVALID_ROUND_BRACKET_ACCESS,
		/// <summary>
		/// 無効な{}アクセス
		/// </summary>
		INVALID_CURLY_BRACKET_ACCESS,
		/// <summary>
		/// 無効な[]アクセス
		/// </summary>
		INVALID_SQUARE_BRACKET_ACCESS,
		/// <summary>
		/// 0で割り算しようとした
		/// </summary>
		DEVIDE_ZERO,
		/// <summary>
		/// Arrayアクセスのインデックスが範囲外である
		/// </summary>
		INDEX_OUT_OF_RANGE,
		/// <summary>
		/// Tableアクセスのキーが見つからなかった
		/// </summary>
		KEY_NOT_FOUND,
		/// <summary>
		/// Argumentアクセスのインデックスが範囲外である
		/// </summary>
		INDEX_OUT_OF_ARGUMENTS,
		/// <summary>
		/// 変数が定義されていなかった
		/// </summary>
		UNDEFINED_VARIANT,
		/// <summary>
		/// 型が合わない
		/// </summary>
		TYPE_MISMATCH,
		/// <summary>
		/// スライドを作る上での情報が不足している
		/// </summary>
		/// slideコマンドとしては正しいが、stepが親に存在しなかったなどでエラーにもしたい
		/// スライドコマンドを作ったあと、チェックしてみて、だめだったらexception型を返す。
		LACK_OF_SLIDE_INFORMATION,
		/// <summary>
		/// スライドヘッドが不適切(ヘッドの代わりに書いていいノートはTapかBreakのみ).
		/// </summary>
		INVALID_SLIDE_HEAD,
		/// <summary>
		/// 最初のスライドコマンドがContinued系なのにHeadが存在しない
		/// </summary>
		HEAD_OF_FIRST_CONTINUED_SLIDE_COMMAND_IS_MISSING,
		/// <summary>
		/// 無限ループに陥った.
		/// </summary>
		INFINITE_LOOP,
		/// <summary>
		/// ループ条件チェックにはFlag型を返すFunctionが必要である
		/// </summary>
		LOOP_CONDITION_TYPE_MISMATCH,
	}
	
	public interface IObject {
		Type type { get; }
		Object Clone();
		T Cast<T>() where T : Object;
		string Log();
	}
	
	public interface IMsqValue : IObject {
		float value { get; set; }
	}
	public interface IMirrorable : IObject {
		Object MirrorHolizontal(CallerInfo caller);
		Object MirrorVertical(CallerInfo caller);
	}
	public interface IButtonTurnable : IObject {
		Object Turn(CallerInfo caller, Number amount);
	}
	public interface IFreeTurnable : IObject {
		Object Turn(CallerInfo caller, IMsqValue amount);
	}
	public interface INote : IObject {
		bool secret { get; set; }
		Object Secret(CallerInfo caller);
	}
	public interface IStep : IObject {
		
	}
	public interface ISlideHead : IObject, IButtonTurnable, IMirrorable {
		int? star { get; }
	}
	public interface ISlideWait : IObject {
		
	}
	public interface ISlidePattern : IObject, IButtonTurnable, IFreeTurnable, IMirrorable {
	}
	public interface ISlideChain : ISlidePattern {
	}
	public interface ISlideCommand : ISlideChain {
		bool IsStraight { get; }
		bool IsCurve { get; }
		bool IsArray { get; }
	}
	public interface ISlideCommandArrayContent : ISlideCommand {
		bool IsSlideContinuedCommand { get; }
	}
	public interface ISlideContinuedCommand : ISlideCommandArrayContent {
		ISlideTrimmedCommand GetEntity(ISlideHead head, ISlideTrimmedCommand beforeCommand);
	}
	public interface ISlideTrimmedCommand : ISlideCommandArrayContent {
	}
	
	public abstract class Object : IObject {
		public abstract Type type { get; }
		public abstract Object Clone();
		public T Cast<T>() where T : Object {
			return (T)this;
		}
		public virtual string Log() {
			return "MsqScript.Object()";
		}
		public virtual bool IsNaN { get { return true; } }
		public virtual bool IsException { get { return false; } }
		public virtual bool IsVoid { get { return false; } }
		public virtual bool IsSuccess { get { return false; } }
		public virtual bool IsNote { get { return false; } }
		public virtual bool IsStep { get { return false; } }
		public virtual bool IsSlideHead { get { return false; } }
		public virtual bool IsSlideWait { get { return false; } }
		public virtual bool IsSlidePattern { get { return false; } }
		public virtual bool IsSlideChain { get { return false; } }
		public virtual bool IsSlideCommand { get { return false; } }
	}
	
	public class Void : Object {
		public override Type type { get { return Type.VOID; } }
		public override Object Clone() { return new Void(); }
		public override string Log() {
			return "MsqScript.Void()";
		}
		public override bool IsVoid { get { return true; } }
	}
	
	public class Return : Object {
		public override Type type { get { return Type.RETURN; } }
		public override Object Clone() { return new Return(entity); }
		public override string Log() {
			return "MsqScript.Return(" + entity.Log() + ")";
		}
		public Object entity;
		public Return(Object data) {
			entity = data;
		}
	}
	
	public class Argument : Object {
		public override Type type { get { return Type.ARGUMENT; } }
		public override Object Clone() { return new Argument(index); }
		public override string Log() {
			return "MsqScript.Argument()";
		}
		public int index;
		public Argument(int data) {
			index = data;
		}
	}
	
	public class Number : Object, IMsqValue {
		public override Type type { get { return Type.NUMBER; } }
		public override Object Clone() { return new Number(entity); }
		public override string Log() {
			return "MsqScript.Number(" + entity.ToString() + ")";
		}
		public override bool IsNaN { get { return false; } }
		public int entity;
		public float value { get { return entity; } set { entity = (int)value; } }
		public Number(int data) {
			entity = data;
		}
		public Object Add(CallerInfo caller, Number a0) {
			entity += a0.entity;
			return this;
		}
		public Object Sub(CallerInfo caller, Number a0) {
			entity -= a0.entity;
			return this;
		}
		public Object Mul(CallerInfo caller, Number a0) {
			entity *= a0.entity;
			return this;
		}
		public Object Div(CallerInfo caller, Number a0) {
			if (a0.entity != 0) {
				entity /= a0.entity;
				return this;
			}
			return new Exception(caller, ErrorCode.DEVIDE_ZERO);
		}
		public Object Pow(CallerInfo caller, Number a0) {
			entity = (int)UnityEngine.Mathf.Pow(entity, a0.entity);
			return this;
		}
		public Object Mod(CallerInfo caller, Number a0) {
			if (a0.entity != 0) {
				entity %= a0.entity;
				return this;
			}
			return new Exception(caller, ErrorCode.DEVIDE_ZERO);
		}
		public Object Equal(CallerInfo caller, Number cmp) {
			return new Flag(entity == cmp.entity);
		}
		public Object NotEqual(CallerInfo caller, Number cmp) {
			return new Flag(entity != cmp.entity);
		}
		public Object Greater(CallerInfo caller, Number cmp) {
			return new Flag(entity > cmp.entity);
		}
		public Object Less(CallerInfo caller, Number cmp) {
			return new Flag(entity < cmp.entity);
		}
		public Object GreaterEqual(CallerInfo caller, Number cmp) {
			return new Flag(entity >= cmp.entity);
		}
		public Object LessEqual(CallerInfo caller, Number cmp) {
			return new Flag(entity <= cmp.entity);
		}
		public Object Inc(CallerInfo caller) {
			entity++;
			return this;
		}
		public Object Dec(CallerInfo caller) {
			entity--;
			return this;
		}
		public Object BitAnd(CallerInfo caller, Number a0) {
			entity &= a0.entity;
			return this;
		}
		public Object BitOr(CallerInfo caller, Number a0) {
			entity |= a0.entity;
			return this;
		}
		public Object BitXor(CallerInfo caller, Number a0) {
			entity ^= a0.entity;
			return this;
		}
		public Object BitNot(CallerInfo caller) {
			entity = ~entity;
			return this;
		}
		public Object BitShiftLeft(CallerInfo caller, Number a0) {
			entity <<= a0.entity;
			return this;
		}
		public Object BitShiftRight(CallerInfo caller, Number a0) {
			entity >>= a0.entity;
			return this;
		}
		public Object ToText() {
			return new Text(entity.ToString());
		}

		public Button ToButton() {
			return new Button(this);
		}
	}
	
	public class Value : Object, IMsqValue {
		public override Type type { get { return Type.VALUE; } }
		public override Object Clone() { return new Value(entity); }
		public override string Log() {
			return "MsqScript.Value(" + entity.ToString() + ")";
		}
		public override bool IsNaN { get { return false; } }
		public float entity;
		public float value { get { return entity; } set { entity = value; } }
		public Value(float data) {
			entity = data;
		}
		public Object Add(CallerInfo caller, IMsqValue a0) {
			entity += a0.value;
			return this;
		}
		public Object Sub(CallerInfo caller, IMsqValue a0) {
			entity -= a0.value;
			return this;
		}
		public Object Mul(CallerInfo caller, IMsqValue a0) {
			entity *= a0.value;
			return this;
		}
		public Object Div(CallerInfo caller, IMsqValue a0) {
			if (a0.value != 0) {
				entity /= a0.value;
				return this;
			}
			return new Exception(caller, ErrorCode.DEVIDE_ZERO);
		}
		public Object Pow(CallerInfo caller, IMsqValue a0) {
			entity = (int)UnityEngine.Mathf.Pow(entity, a0.value);
			return this;
		}
		public Object ToNumber(CallerInfo caller) {
			return new Number(UnityEngine.Mathf.FloorToInt(entity));
		}
		public Object Equal(CallerInfo caller, IMsqValue cmp) {
			return new Flag(entity == cmp.value);
		}
		public Object NotEqual(CallerInfo caller, IMsqValue cmp) {
			return new Flag(entity != cmp.value);
		}
		public Object Greater(CallerInfo caller, IMsqValue cmp) {
			return new Flag(entity > cmp.value);
		}
		public Object Less(CallerInfo caller, IMsqValue cmp) {
			return new Flag(entity < cmp.value);
		}
		public Object GreaterEqual(CallerInfo caller, IMsqValue cmp) {
			return new Flag(entity >= cmp.value);
		}
		public Object LessEqual(CallerInfo caller, IMsqValue cmp) {
			return new Flag(entity <= cmp.value);
		}
		public Object Inc(CallerInfo caller) {
			entity++;
			return this;
		}
		public Object Dec(CallerInfo caller) {
			entity--;
			return this;
		}
		public Object ToText() {
			return new Text(entity.ToString());
		}
	}
	
	public class Flag : Object {
		public override Type type { get { return Type.FLAG; } }
		public override Object Clone() { return new Flag(entity); }
		public override string Log() {
			return "MsqScript.Flag(" + entity.ToString() + ")";
		}
		public bool entity;
		public Flag(bool data) {
			entity = data;
		}
		public Flag Not(CallerInfo caller) {
			entity = !entity;
			return this;
		}
	}
	
	public class Text : Object {
		public override Type type { get { return Type.TEXT; } }
		public override Object Clone() { return new Text(entity); }
		public override string Log() {
			return "MsqScript.Text(" + entity + ")";
		}
		public string entity;
		public Text(string data) {
			entity = data;
		}
		public Object Concat(CallerInfo caller, Text a0) {
			entity += a0.entity;
			return this;
		}
		public Object Equal(CallerInfo caller, Text cmp) {
			return new Flag(entity == cmp.entity);
		}
		public Object NotEqual(CallerInfo caller, Text cmp) {
			return new Flag(entity != cmp.entity);
		}
		public Object Length(CallerInfo caller) {
			return new Number(entity.Length);
		}
	}
	
	public class Position : Object, IButtonTurnable, IFreeTurnable, IMirrorable {
		public override Type type { get { return Type.POSITION; } }
		public override Object Clone() { return new Position(entity); }
		public override string Log() {
			return "MsqScript.Position(x = " + entity.x.ToString() + ", y = " + entity.y.ToString() + ")";
		}
		public UnityEngine.Vector2 entity;
		public Position(float x, float y) {
			entity = new UnityEngine.Vector2(x, y);
		}
		public Position(UnityEngine.Vector2 data) {
			entity = data;
		}
		public Position(IMsqValue x, IMsqValue y) : this(x.value, y.value) {
		}
		public Object Set(CallerInfo caller, Value x, Value y) {
			entity.x = x.entity;
			entity.y = y.entity;
			return this;
		}
		public Object GetX() {
			return new Value(entity.x);
		}
		public Object GetY() {
			return new Value(entity.y);
		}
		public Object SetX(Value x) {
			entity.x = x.entity;
			return this;
		}
		public Object SetY(Value y) {
			entity.y = y.entity;
			return this;
		}
		public Object Equal(CallerInfo caller, Position cmp) {
			return new Flag(entity == cmp.entity);
		}
		public Object NotEqual(CallerInfo caller, Position cmp) {
			return new Flag(entity != cmp.entity);
		}
		public Object Turn(CallerInfo caller, Number amount) {
			return Turn(caller, new Value(Constants.instance.GetPieceDegree (amount.entity) - 22.5f));
		}
		public Object Turn(CallerInfo caller, IMsqValue degree) {
			// 左手座標.
			var pos = entity;
			// 右手座標.
			pos = Constants.instance.ToLeftHandedCoordinateSystemPosition (pos);
			float addDeg = degree.value;
			float deg = CircleCalculator.PointToDegree (pos) + 180.0f;
			float radius = CircleCalculator.PointToPointDistance (UnityEngine.Vector2.zero, pos);
			var turned = CircleCalculator.PointOnCircle (UnityEngine.Vector2.zero, radius, deg + addDeg);
			// 左手座標.
			var ret = Constants.instance.ToLeftHandedCoordinateSystemPosition (turned);
			entity = ret;
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			entity.x *= -1;
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			entity.y *= -1;
			return this;
		}
	}
	
	public class Color : Object {
		public override Type type { get { return Type.COLOR; } }
		public override Object Clone() { return new Color(entity); }
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			float[] data = new float[] { entity.r, entity.g, entity.b, entity.a };
			string[] names = new string[] { "r", "g", "b", "a" };
			builder.Append("MsqScript.Color(");
			for (int i = 0; i < data.Length; i++) {
				if (i > 0) {
					builder.Append(", ");
				}
				int output = (int)(data[i] * 255.0f);
				builder.Append(names[i]);
				builder.Append(" = ");
				builder.Append(output.ToString());
			}
			builder.Append(")");
			return builder.ToString();
		}
		public UnityEngine.Color entity;
		public Color(UnityEngine.Color data) {
			entity = data;
		}
		public Color(float r, float g, float b, float a) {
			float[] elms = new float[] { r, g, b, a };
			for (int i = 0; i < elms.Length; i++) {
				if (elms[i] < 0.0f)
					elms[i] = 0.0f;
				if (elms[i] > 1.0f)
					elms[i] = 1.0f;
			}
			entity = new UnityEngine.Color(r, g, b, a);
		}
		public Color(int r, int g, int b, int a) {
			float[] elms = new float[] { r, g, b, a };
			for (int i = 0; i < elms.Length; i++) {
				if (elms[i] < 0.0f)
					elms[i] = 0.0f;
				if (elms[i] > 255.0f)
					elms[i] = 255.0f;
			}
			entity = new UnityEngine.Color(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f);
		}
		public Object SetR(CallerInfo caller, Value r) {
			entity.r = r.entity;
			return this;
		}
		public Object SetG(CallerInfo caller, Value g) {
			entity.g = g.entity;
			return this;
		}
		public Object SetB(CallerInfo caller, Value b) {
			entity.b = b.entity;
			return this;
		}
		public Object SetA(CallerInfo caller, Value a) {
			entity.a = a.entity;
			return this;
		}
		public Object GetR(CallerInfo caller) {
			return new Value(entity.r);
		}
		public Object GetG(CallerInfo caller) {
			return new Value(entity.g);
		}
		public Object GetB(CallerInfo caller) {
			return new Value(entity.b);
		}
		public Object GetA(CallerInfo caller) {
			return new Value(entity.a);
		}
		public Object SetR255(CallerInfo caller, Number r) {
			float v = r.entity;
			if (v < 0) v = 0;
			if (v > 255) v = 255;
			entity.r = v / 255.0f;
			return this;
		}
		public Object SetG255(CallerInfo caller, Number g) {
			float v = g.entity;
			if (v < 0)
				v = 0;
			if (v > 255)
				v = 255;
			entity.g = v / 255.0f;
			return this;
		}
		public Object SetB255(CallerInfo caller, Number b) {
			float v = b.entity;
			if (v < 0)
				v = 0;
			if (v > 255)
				v = 255;
			entity.b = v / 255.0f;
			return this;
		}
		public Object SetA255(CallerInfo caller, Number a) {
			float v = a.entity;
			if (v < 0)
				v = 0;
			if (v > 255)
				v = 255;
			entity.a = v / 255.0f;
			return this;
		}
		public Object GetR255(CallerInfo caller) {
			return new Number((int)(entity.r * 255.0f));
		}
		public Object GetG255(CallerInfo caller) {
			return new Number((int)(entity.g * 255.0f));
		}
		public Object GetB255(CallerInfo caller) {
			return new Number((int)(entity.b * 255.0f));
		}
		public Object GetA255(CallerInfo caller) {
			return new Number((int)(entity.a * 255.0f));
		}
	}
	
	public class Button : Object, IButtonTurnable, IMirrorable {
		public override Type type { get { return Type.BUTTON; } }
		public override Object Clone() { return new Button(entity); }
		public override string Log() {
			return "MsqScript.Button(" + entity.ToString() + ")";
		}
		public int entity;
		public Button(int data) {
			int b = data - 1;
			if (b < 0)
				b = 0;
			if (b > 7)
				b = 7;
			entity = b + 1;
		}
		public Button(Number data) : this(data.entity) { }
		public int GetId() {
			return entity;
		}
		public int GetIndex() {
			return entity - 1;
		}
		public bool IsTopSide() {
			return entity < 3 || entity > 6;
		}
		public bool IsBottomSide() {
			return entity > 2 && entity < 7;
		}
		public bool IsLeftSide() {
			return entity < 5;
		}
		public bool IsRightSide() {
			return entity > 4;
		}
		public int CalcTurn(int amount, int? buttonId = null) {
			if (!buttonId.HasValue) {
				buttonId = GetId();
			}
			if (amount < 0) {
				int div = -amount / 8;
				int mod = -amount % 8;
				amount = 8 * (div + 1) - mod;
			}
			return (buttonId.Value - 1 + amount) % 8 + 1;
		}
		public int CalcMirrorHolizontal(int? buttonId = null) {
			if (!buttonId.HasValue) {
				buttonId = GetId();
			}
			return 9 - buttonId.Value;
		}
		public int CalcMirrorVertical(int? buttonId = null) {
			if (!buttonId.HasValue) {
				buttonId = GetId();
			}
			return CalcMirrorHolizontal(CalcTurn(buttonId.Value, 4));
		}
		
		public Object Turn(CallerInfo caller, Number amount) {
			entity = CalcTurn(amount.entity, entity);
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			entity = CalcMirrorHolizontal(entity);
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			entity = CalcMirrorVertical(entity);
			return this;
		}
		public Number ToNumber() {
			return new Number(entity);
		}
	}
	
	public class NoteTap : Object, INote, IButtonTurnable, IMirrorable {
		public override Type type { get { return Type.NOTE_TAP; } }
		public override Object Clone() { return new NoteTap(button.entity); }
		public override string Log() {
			return "MsqScript.NoteTap(" + button.Log() + ")";
		}
		public override bool IsNote { get { return true; } }
		public NoteTap(int buttonId) { button = new Button(buttonId); }
		public Button button;
		public bool secret { get; set; }
		public Object Secret(CallerInfo caller) {
			secret = true;
			return this;
		}
		public Object Turn(CallerInfo caller, Number amount) {
			button.Turn(caller, amount);
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			button.MirrorHolizontal(caller);
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			button.MirrorVertical(caller);
			return this;
		}
	}
	
	public class NoteHold : Object, INote, IButtonTurnable, IMirrorable {
		public override Type type { get { return Type.NOTE_HOLD; } }
		public override Object Clone() { return new NoteHold(button.entity, (IStep)step.Clone()); }
		public override string Log() {
			return "MsqScript.NoteHold(" + button.Log() + ", " + step.Log()+ ")";
		}
		public override bool IsNote { get { return true; } }
		public NoteHold(int buttonId, IStep step) { button = new Button(buttonId); this.step = step; }
		public Button button;
		public IStep step;
		public bool secret { get; set; }
		public Object Secret(CallerInfo caller) {
			secret = true;
			return this;
		}
		public Object Turn(CallerInfo caller, Number amount) {
			button.Turn(caller, amount);
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			button.MirrorHolizontal(caller);
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			button.MirrorVertical(caller);
			return this;
		}
	}
	
	public class NoteSlide : Object, INote, IButtonTurnable, IMirrorable {
		public override Type type { get { return Type.NOTE_SLIDE; } }
		public override Object Clone() {
			ISlideHead h = null;
			ISlideWait w = null;
			IStep s = null;
			var os = new ISlidePattern[orders.Length];
			
			if (head != null)
				h = (ISlideHead)head.Clone();
			if (wait != null)
				w = (ISlideWait)wait.Clone();
			if (step != null)
				s = (IStep)step.Clone();
			if (orders != null) {
				for (int i = 0; i < os.Length; i++) {
					os[i] = (ISlidePattern)orders[i].Clone();
				}
			}
			return new NoteSlide(h, w, s, os);
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.NoteSlide(head = ");
			builder.Append(head != null ? head.Log() : "null");
			builder.Append(", wait = ");
			builder.Append(wait != null ? wait.Log() : "null");
			builder.Append(", step = ");
			builder.Append(step != null ? step.Log() : "null");
			builder.Append(", orders = ");
			if (orders == null) {
				builder.Append("null");
			}
			else {
				builder.Append("[");
				for (int i = 0; i < orders.Length; i++) {
					if (i > 0) {
						builder.Append(", ");
					}
					else {
						builder.Append(" ");
					}
					builder.Append(orders[i].Log());
				}
				builder.Append(" ]");
			}
			builder.Append(")");
			return builder.ToString();
		}
		public override bool IsNote { get { return true; } }
		public NoteSlide(ISlideHead head, ISlideWait wait, IStep step, params ISlidePattern[] orders) { this.head = head; this.wait = wait; this.step = step; this.orders = orders; }
		public ISlideHead head;
		public ISlideWait wait;
		public IStep step;
		public ISlidePattern[] orders;
		public bool secret { get; set; }
		public Object Secret(CallerInfo caller) {
			secret = true;
			return this;
		}
		public Object Turn(CallerInfo caller, Number amount) {
			if (head != null) {
				head.Turn(caller, amount);
			}
			if (orders != null) {
				foreach (var order in orders) {
					order.Turn(caller, amount);
				}
			}
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			if (head != null) {
				head.MirrorHolizontal(caller);
			}
			if (orders != null) {
				foreach (var order in orders) {
					order.MirrorHolizontal(caller);
				}
			}
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			if (head != null) {
				head.MirrorVertical(caller);
			}
			if (orders != null) {
				foreach (var order in orders) {
					order.MirrorHolizontal(caller);
				}
			}
			return this;
		}
		
		private NoteSlide _trimmed;
		public NoteSlide trimmed { get { return _trimmed; } }
		/// <summary>
		/// IsRisky()を実行し、それがtrueのとき、最初のスライドコマンドがContinued系なのにheadが無い、という状況であるためのリスクならtrueになる
		/// </summary>
		public bool isMissingHeadOfFirstContinuedSlideCommand { get; private set; }
		/// <summary>
		/// スライドコマンドが実行時にエラーになる可能性があるならtrueを返す
		/// </summary>
		public bool IsRisky() {
			if (_trimmed == null && !isMissingHeadOfFirstContinuedSlideCommand) {
				var ret = Clone().Cast<NoteSlide>();
				// データが無ければデフォルトで上書き
				if (ret.head == null)
					ret.head = new SlideHeadNothing();
				if (ret.wait == null)
					ret.wait = new SlideWaitDefault();
				if (ret.orders == null || ret.orders.Length == 0)
					return true; // patternがひとつもない
				var patterns = new List<ISlidePattern>(ret.orders);
				for (int i = 0; i < patterns.Count; i++) {
					var order = patterns[i];
					// ret.ordersの中身はpattern/chain/commandのどれか
					SlidePattern pattern;
					if (order.type == Type.SLIDE_PATTERN) {
						// パターンだったらそのまま使う
						pattern = order.Cast<SlidePattern>();
					}
					else {
						// パターンを作って、チェインかコマンドを子供にして、パターンをスライドの子供とする。
						pattern = new SlidePattern(null, null, null);
						pattern.orders = new ISlideChain[] { (ISlideChain)order };
						patterns[i] = (ISlidePattern)pattern;
					}
					// データが無ければ親で上書き
					if (pattern.wait == null)
						pattern.wait = ret.wait;
					if (pattern.step == null)
						pattern.step = ret.step;
					if (pattern.orders == null || pattern.orders.Length == 0)
						return true; // chainがひとつもない
					var chains = new List<ISlideChain>(pattern.orders);
					// pattern.ordersの中身はchain/commandのどれか
					for (int j = 0; j < chains.Count; j++) {
						var pOrder = chains[j];
						// Array型なら展開する
						if (pOrder.type == Type.SLIDE_CHAIN_ARRAY) {
							chains.RemoveAt(j);
							chains.InsertRange(j, pOrder.Cast<SlideChainArray>().entity.ToArray());
							pOrder = chains[j];
						}
						SlideChain chain;
						if (pOrder.type == Type.SLIDE_CHAIN) {
							// チェインだったらそのまま使う
							chain = pOrder.Cast<SlideChain>();
						}
						else {
							// チェインを作って、コマンドを子供にして、チェインをパターンの子供とする。
							chain = new SlideChain(null, null, null);
							chain.orders = new ISlideCommand[] { (ISlideCommand)pOrder };
							chains[j] = (ISlideChain)chain;
						}
						// データが無ければ親で上書き
						if (chain.wait == null)
							chain.wait = pattern.wait;
						if (chain.step == null)
							chain.step = pattern.step;
						if (chain.orders == null || chain.orders.Length == 0 || chain.step == null)
							return true; // commandがひとつもない
						var commands = new List<ISlideCommand>(chain.orders);
						ISlideTrimmedCommand beforeCommand = null;
						for (int k = 0; k < commands.Count; k++) {
							var cOrder = commands[k];
							// SlideCommandArray型なら展開する
							if (cOrder.IsArray) {
								commands.RemoveAt(k);
								commands.InsertRange(k, cOrder.Cast<SlideCommandArray>().entity.ToArray());
								cOrder = commands[k];
							}
							// SlideCommandArray型ではないことは確定しているのでSlideCommandArray型の要素になりえる型ではある.
							var cOrderArrayContent = (ISlideCommandArrayContent)cOrder;
							// continue系であるならばheadをつぎ込んでいく
							if (cOrderArrayContent.IsSlideContinuedCommand) {
								var continuedCommand = (ISlideContinuedCommand)cOrderArrayContent;
								// continue系で続きが作れない(一番最初のスライドコマンドがContinue系なのにstarが無い)場合はリスキーとする.
								if (ret.head.type == Type.SLIDE_HEAD_NOTHING && beforeCommand == null) {
									isMissingHeadOfFirstContinuedSlideCommand = true;
									return true;
								}
								else {
									commands[k] = continuedCommand.GetEntity(ret.head, beforeCommand);
								}
							}
							// SlideCommandArray型でもContinue系でもない(もう展開した)のでISlideTrimmedCommandであることが確定した.
							beforeCommand = (ISlideTrimmedCommand)commands[k];
						}
						chain.orders = commands.ToArray();
					}
					pattern.orders = chains.ToArray();
				}
				ret.orders = patterns.ToArray();
				_trimmed = ret;
			}
			return _trimmed == null;
		}
	}
	
	public class NoteBreak : NoteTap {
		public override Type type { get { return Type.NOTE_BREAK; } }
		public override Object Clone() { return new NoteBreak(button.entity); }
		public override string Log() {
			return "MsqScript.NoteBreak(" + button.Log() + ")";
		}
		public NoteBreak(int buttonId) : base (buttonId) { }
	}
	
	public class NoteMessage : Object, INote {
		public override Type type { get { return Type.NOTE_MESSAGE; } }
		public override Object Clone() { return new NoteMessage(message.Clone().Cast<Text>(), position.Clone().Cast<Position>(), (IMsqValue)scale.Clone(), color.Clone().Cast<Color>(), (IStep)step.Clone()); }
		public override string Log() {
			return "MsqScript.NoteMessage()";
		}
		public Text message;
		public Position position;
		public IMsqValue scale;
		public Color color;
		public IStep step;
		public bool secret { get; set; }
		public Object Secret(CallerInfo caller) {
			secret = true;
			return this;
		}
		public NoteMessage(Text message, Position position, IMsqValue scale, Color color, IStep step) {
			this.message = message;
			this.position = position;
			this.scale = scale;
			this.color = color;
			this.step = step;
		}
	}
	
	public class NoteScrollMessage : Object, INote {
		public override Type type { get { return Type.NOTE_SCROLL_MESSAGE; } }
		public override Object Clone() { return new NoteScrollMessage(message.Clone().Cast<Text>(), (IMsqValue)y.Clone(), (IMsqValue)scale.Clone(), color.Clone().Cast<Color>(), (IStep)step.Clone()); }
		public override string Log() {
			return "MsqScript.NoteScrollMessage()";
		}
		public Text message;
		public IMsqValue y;
		public IMsqValue scale;
		public Color color;
		public IStep step;
		public bool secret { get; set; }
		public Object Secret(CallerInfo caller) {
			secret = true;
			return this;
		}
		public NoteScrollMessage(Text message, IMsqValue y, IMsqValue scale, Color color, IStep step) {
			this.message = message;
			this.y = y;
			this.scale = scale;
			this.color = color;
			this.step = step;
		}
	}
	
	public class NoteSoundMessage : Object, INote {
		public override Type type { get { return Type.NOTE_SOUND_MESSAGE; } }
		public override Object Clone() { return new NoteSoundMessage(soundId); }
		public override string Log() {
			return "MsqScript.NoteSoundMessage()";
		}
		public Text soundId;
		public bool secret { get; set; }
		public Object Secret(CallerInfo caller) {
			secret = true;
			return this;
		}
		public NoteSoundMessage(Text soundId) {
			this.soundId = soundId;
		}
	}
	
	public class StepBasic : Object, IStep {
		public override Type type { get { return Type.STEP_BASIC; } }
		public override Object Clone() { return new StepBasic((IMsqValue)beat.Clone(), (IMsqValue)length.Clone()); }
		public override string Log() {
			return "MsqScript.StepBasic(beat = " + beat.ToString() + ", length = " + length.ToString() + ")";
		}
		public override bool IsStep { get { return true; } }
		public StepBasic(IMsqValue beat, IMsqValue length) { this.beat = beat; this.length = length; }
		public IMsqValue beat;
		public IMsqValue length;
	}
	
	public class StepInterval : Object, IStep {
		public override Type type { get { return Type.STEP_INTERVAL; } }
		public override Object Clone() { return new StepInterval((IMsqValue)interval.Clone()); }
		public override string Log() {
			return "MsqScript.StepInterval(" + interval.ToString() + ")";
		}
		public override bool IsStep { get { return true; } }
		public StepInterval(IMsqValue data) { interval = data; }
		public IMsqValue interval;
	}
	
	public class StepLocalBpm : StepBasic, IStep {
		public override Type type { get { return Type.STEP_LOCALBPM; } }
		public override Object Clone() { return new StepLocalBpm((IMsqValue)bpm.Clone(), (IMsqValue)beat.Clone(), (IMsqValue)length.Clone()); }
		public override string Log() {
			return "MsqScript.StepLocalBpm(bpm = " + bpm.ToString() + ", beat = " + beat.ToString() + ", length = " + length.ToString() + ")";
		}
		public StepLocalBpm(IMsqValue bpm, IMsqValue beat, IMsqValue length) : base (beat, length) { this.bpm = bpm; }
		public IMsqValue bpm;
	}
	
	public class SlideHeadStar : Button, ISlideHead {
		public override Type type { get { return Type.SLIDE_HEAD_STAR; } }
		public override Object Clone() { return new SlideHeadStar(entity); }
		public override string Log() {
			return "MsqScript.SlideHeadStar(" + entity.ToString() + ")";
		}
		public override bool IsSlideHead { get { return true; } }
		public SlideHeadStar(int data) : base(data) { }
		public SlideHeadStar(Number data) : base(data) { }
		public int? star { get { return entity; } }
	}
	
	public class SlideHeadBreak : SlideHeadStar, ISlideHead {
		public override Type type { get { return Type.SLIDE_HEAD_BREAK; } }
		public override Object Clone() { return new SlideHeadBreak(entity); }
		public override string Log() {
			return "MsqScript.SlideHeadBreak(" + entity.ToString() + ")";
		}
		public SlideHeadBreak(int data) : base(data) { }
		public SlideHeadBreak(Number data) : base(data) { }
	}
	
	public class SlideHeadNothing : Object, ISlideHead {
		public override Type type { get { return Type.SLIDE_HEAD_NOTHING; } }
		public override Object Clone() { return new SlideHeadNothing(); }
		public override string Log() {
			return "MsqScript.SlideHeadNothing()";
		}
		public override bool IsSlideHead { get { return true; } }
		public SlideHeadNothing() { }
		public int? star { get { return null; } }
		public Object Turn(CallerInfo caller, Number amount) {
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			return this;
		}
	}
	
	public class SlideWaitDefault : Object, ISlideWait {
		public override Type type { get { return Type.SLIDE_WAIT_DEFAULT; } }
		public override Object Clone() { return new SlideWaitDefault(); }
		public override string Log() {
			return "MsqScript.SlideWaitDefault()";
		}
		public override bool IsSlideWait { get { return true; } }
		public SlideWaitDefault() { }
	}
	
	public class SlideWaitInterval : Object, ISlideWait {
		public override Type type { get { return Type.SLIDE_WAIT_INTERVAL; } }
		public override Object Clone() { return new SlideWaitInterval((IMsqValue)time.Clone()); }
		public override string Log() {
			return "MsqScript.SlideWaitInterval(" + time.ToString() + ")";
		}
		public override bool IsSlideWait { get { return true; } }
		public SlideWaitInterval(IMsqValue data) { time = data; }
		public IMsqValue time;
	}
	
	public class SlideWaitLocalBpm : Object, ISlideWait {
		public override Type type { get { return Type.SLIDE_WAIT_LOCALBPM; } }
		public override Object Clone() { return new SlideWaitLocalBpm((IMsqValue)bpm.Clone()); }
		public override string Log() {
			return "MsqScript.SlideWaitLocalBpm(" + bpm.ToString() + ")";
		}
		public override bool IsSlideWait { get { return true; } }
		public SlideWaitLocalBpm(IMsqValue data) { bpm = data; }
		public IMsqValue bpm;
	}
	
	public class SlideWaitStepBasic : Object, ISlideWait {
		public override Type type { get { return Type.SLIDE_WAIT_STEP_BASIC; } }
		public override Object Clone() { return new SlideWaitStepBasic((IMsqValue)beat.Clone(), (IMsqValue)length.Clone()); }
		public override string Log() {
			return "MsqScript.SlideWaitStepBasic(beat = " + beat.ToString() + ", length = " + length.ToString() + ")";
		}
		public override bool IsSlideWait { get { return true; } }
		public SlideWaitStepBasic(IMsqValue beat, IMsqValue length) { this.beat = beat; this.length = length; }
		public IMsqValue beat;
		public IMsqValue length;
	}
	
	public class SlideWaitStepLocalBpm : SlideWaitStepBasic, ISlideWait {
		public override Type type { get { return Type.SLIDE_WAIT_STEP_LOCALBPM; } }
		public override Object Clone() { return new SlideWaitStepLocalBpm((IMsqValue)bpm.Clone(), (IMsqValue)beat.Clone(), (IMsqValue)length.Clone()); }
		public override string Log() {
			return "MsqScript.SlideWaitStepLocalBpm(bpm = " + bpm.ToString() + ", beat = " + beat.ToString() + ", length = " + length.ToString() + ")";
		}
		public override bool IsSlideWait { get { return true; } }
		public SlideWaitStepLocalBpm(IMsqValue bpm, IMsqValue beat, IMsqValue length) : base (beat, length) { this.bpm = bpm; }
		public IMsqValue bpm;
	}
	
	public class SlidePattern : Object, ISlidePattern {
		public override Type type { get { return Type.SLIDE_PATTERN; } }
		public override Object Clone() {
			ISlideWait w = null;
			IStep s = null;
			ISlideChain[] os = new ISlideChain[orders.Length];
			if (wait != null)
				w = (ISlideWait)wait.Clone();
			if (step != null)
				s = (IStep)step.Clone();
			if (orders != null) {
				for (int i = 0; i < os.Length; i++) {
					os[i] = (ISlideChain)orders[i].Clone();
				}
			}
			return new SlidePattern(w, s, os);
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.SlidePattern(wait = ");
			builder.Append(wait != null ? wait.Log() : "null");
			builder.Append(", step = ");
			builder.Append(step != null ? step.Log() : "null");
			builder.Append(", orders = ");
			if (orders == null) {
				builder.Append("null");
			}
			else {
				builder.Append("[");
				for (int i = 0; i < orders.Length; i++) {
					if (i > 0) {
						builder.Append(", ");
					}
					else {
						builder.Append(" ");
					}
					builder.Append(orders[i].Log());
				}
				builder.Append(" ]");
			}
			builder.Append(")");
			return builder.ToString();
		}
		public override bool IsSlidePattern { get { return true; } }
		public SlidePattern(ISlideWait wait, IStep step, ISlideChain[] orders) {
			this.wait = wait;
			this.step = step;
			this.orders = orders;
		}
		public ISlideWait wait { get; set; }
		public IStep step { get; set; }
		public ISlideChain[] orders { get; set; }
		public Object Turn(CallerInfo caller, Number amount) {
			if (orders != null) {
				foreach (var order in orders) {
					order.Turn(caller, amount);
				}
			}
			return this;
		}
		public Object Turn(CallerInfo caller, IMsqValue degree) {
			if (orders != null) {
				foreach (var order in orders) {
					order.Turn(caller, degree);
				}
			}
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			if (orders != null) {
				foreach (var order in orders) {
					order.MirrorHolizontal(caller);
				}
			}
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			if (orders != null) {
				foreach (var order in orders) {
					order.MirrorVertical(caller);
				}
			}
			return this;
		}
	}
	
	public class SlideChain : Object, ISlideChain {
		public override Type type { get { return Type.SLIDE_CHAIN; } }
		public override Object Clone() {
			ISlideWait w = null;
			IStep s = null;
			ISlideCommand[] os = new ISlideCommand[orders.Length];
			if (wait != null)
				w = (ISlideWait)wait.Clone();
			if (step != null)
				s = (IStep)step.Clone();
			if (orders != null) {
				for (int i = 0; i < os.Length; i++) {
					os[i] = (ISlideCommand)orders[i].Clone();
				}
			}
			return new SlideChain(w, s, os);
		}
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.SlideChain(wait = ");
			builder.Append(wait != null ? wait.Log() : "null");
			builder.Append(", step = ");
			builder.Append(step != null ? step.Log() : "null");
			builder.Append(", orders = ");
			if (orders == null) {
				builder.Append("null");
			}
			else {
				builder.Append("[");
				for (int i = 0; i < orders.Length; i++) {
					if (i > 0) {
						builder.Append(", ");
					}
					else {
						builder.Append(" ");
					}
					builder.Append(orders[i].Log());
				}
				builder.Append(" ]");
			}
			builder.Append(")");
			return builder.ToString();
		}
		public override bool IsSlidePattern { get { return true; } }
		public override bool IsSlideChain { get { return true; } }
		public SlideChain(ISlideWait wait, IStep step, ISlideCommand[] orders) {
			this.wait = wait;
			this.step = step;
			this.orders = orders;
		}
		public ISlideWait wait { get; set; }
		public IStep step { get; set; }
		public ISlideCommand[] orders { get; set; }
		public Object Turn(CallerInfo caller, Number amount) {
			if (orders != null) {
				foreach (var order in orders) {
					order.Turn(caller, amount);
				}
			}
			return this;
		}
		public Object Turn(CallerInfo caller, IMsqValue degree) {
			if (orders != null) {
				foreach (var order in orders) {
					order.Turn(caller, degree);
				}
			}
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			if (orders != null) {
				foreach (var order in orders) {
					order.MirrorHolizontal(caller);
				}
			}
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			if (orders != null) {
				foreach (var order in orders) {
					order.MirrorVertical(caller);
				}
			}
			return this;
		}
	}
	
	public class SlideChainArray : Object, ISlideChain {
		public override Type type { get { return Type.SLIDE_CHAIN_ARRAY; } }
		public override Object Clone() { return new SlideChainArray(entity.ToArray()); }
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.SlideChainArray[");
			int size = entity.Count;
			for (int i = 0; i < size; i++) {
				if (i > 0) {
					builder.Append(", ");
				}
				else {
					builder.Append(" ");
				}
				builder.Append(entity[i].Log());
			}
			builder.Append(" ]");
			return builder.ToString();
		}
		public override bool IsSlidePattern { get { return true; } }
		public List<ISlideChain> entity;
		public SlideChainArray(params ISlideChain[] data) {
			entity = new List<ISlideChain>(data);
		}
		public SlideChainArray(IEnumerable<ISlideChain> data) {
			entity = new List<ISlideChain>(data);
		}
		/// <summary>
		/// Arrayから作る。成功したらtrueが返る
		/// </summary>
		public bool FromArray(CallerInfo caller, Array array) {
			entity.Clear();
			int size = array.Length(caller).Cast<Number>().entity;
			for (int i = 0; i < size; i++) {
				var data = array.Get(caller, new Number(i));
				if (data.IsSlideChain) {
					entity.Add((ISlideChain)data);
				}
				else {
					return false;
				}
			}
			return true;
		}
		public Object Turn(CallerInfo caller, Number amount) {
			if (entity != null) {
				foreach (var order in entity) {
					order.Turn(caller, amount);
				}
			}
			return this;
		}
		public Object Turn(CallerInfo caller, IMsqValue degree) {
			if (entity != null) {
				foreach (var order in entity) {
					order.Turn(caller, degree);
				}
			}
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			if (entity != null) {
				foreach (var order in entity) {
					order.MirrorHolizontal(caller);
				}
			}
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			if (entity != null) {
				foreach (var order in entity) {
					order.MirrorVertical(caller);
				}
			}
			return this;
		}
	}
	
	public class SlideCommandStraight : Object, ISlideTrimmedCommand {
		public override Type type { get { return Type.SLIDE_COMMAND_STRAIGHT; } }
		public override Object Clone() { return new SlideCommandStraight(start.Clone().Cast<Position>(), target.Clone().Cast<Position>()); }
		public override string Log() {
			return "MsqScript.SlideCommandStraight(start = " + start.Log() + ", target = " + target.Log() + ")";
		}
		public override bool IsSlidePattern { get { return true; } }
		public override bool IsSlideChain { get { return true; } }
		public override bool IsSlideCommand { get { return true; } }
		public bool IsSlideContinuedCommand { get { return false; } }
		public bool IsStraight { get { return true; } }
		public bool IsCurve { get { return false; } }
		public bool IsArray { get { return false; } }
		public SlideCommandStraight(Position start, Position target) {
			this.start = start;
			this.target = target;
		}
		public Position start;
		public Position target;
		public Object Turn(CallerInfo caller, Number amount) {
			start.Turn(caller, amount);
			target.Turn(caller, amount);
			return this;
		}
		public Object Turn(CallerInfo caller, IMsqValue degree) {
			start.Turn(caller, degree);
			target.Turn(caller, degree);
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			start.MirrorHolizontal(caller);
			target.MirrorHolizontal(caller);
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			start.MirrorVertical(caller);
			target.MirrorVertical(caller);
			return this;
		}
	}
	
	public class SlideCommandCurve : Object, ISlideTrimmedCommand {
		public override Type type { get { return Type.SLIDE_COMMAND_CURVE; } }
		public override Object Clone() { return new SlideCommandCurve(center.Clone().Cast<Position>(), radius.Clone().Cast<Position>(), (IMsqValue)start_degree.Clone(), (IMsqValue)distance_degree.Clone()); }
		public override string Log() {
			return "MsqScript.SlideCommandCurve(" + ")";
		}
		public override bool IsSlidePattern { get { return true; } }
		public override bool IsSlideChain { get { return true; } }
		public override bool IsSlideCommand { get { return true; } }
		public bool IsSlideContinuedCommand { get { return false; } }
		public bool IsStraight { get { return false; } }
		public bool IsCurve { get { return true; } }
		public bool IsArray { get { return false; } }
		public SlideCommandCurve(Position center, Position radius, IMsqValue start_degree, IMsqValue distance_degree) {
			this.center = center;
			this.radius = radius;
			this.start_degree = start_degree;
			this.distance_degree = distance_degree;
		}
		public SlideCommandCurve(Position center, IMsqValue radius, IMsqValue start_degree, IMsqValue distance_degree)
			: this (center, new Position(radius, radius), start_degree, distance_degree) {
		}
		public Position center;
		public Position radius;
		public IMsqValue start_degree;
		public IMsqValue distance_degree;
		public Object Turn(CallerInfo caller, Number amount) {
			center.Turn(caller, amount);
			float sd = start_degree.value;
			sd += amount.entity * (360.0f / 8.0f);
			while (sd > 360.0f)
				sd -= 360.0f;
			while (sd < 0)
				sd += 360.0f;
			start_degree.value = sd;
			return this;
		}
		public Object Turn(CallerInfo caller, IMsqValue degree) {
			center.Turn(caller, degree);
			float sd = start_degree.value;
			sd += degree.value;
			while (sd > 360.0f)
				sd -= 360.0f;
			while (sd < 0)
				sd += 360.0f;
			start_degree.value = sd;
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			center.MirrorHolizontal(caller);
			float sd = start_degree.value;
			sd = 360.0f - sd;
			while (sd > 360.0f)
				sd -= 360.0f;
			while (sd < 0)
				sd += 360.0f;
			start_degree.value = sd;
			distance_degree.value *= -1;
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			center.MirrorVertical(caller);
			float sd = start_degree.value;
			sd = 180.0f - sd;
			while (sd > 360.0f)
				sd -= 360.0f;
			while (sd < 0)
				sd += 360.0f;
			start_degree.value = sd;
			distance_degree.value *= -1;
			return this;
		}
	}
	
	public class SlideCommandContinuedStraight : Object, ISlideContinuedCommand {
		public override Type type { get { return Type.SLIDE_COMMAND_CONTINUED_STRAIGHT; } }
		public override Object Clone() { return new SlideCommandContinuedStraight(target.Clone().Cast<Position>()); }
		public override string Log() {
			return "MsqScript.SlideCommandContinuedStraight(" + ")";
		}
		public override bool IsSlidePattern { get { return true; } }
		public override bool IsSlideChain { get { return true; } }
		public override bool IsSlideCommand { get { return true; } }
		public bool IsSlideContinuedCommand { get { return true; } }
		public bool IsStraight { get { return true; } }
		public bool IsCurve { get { return false; } }
		public bool IsArray { get { return false; } }
		public SlideCommandContinuedStraight(Position target) {
			this.target = target;
		}
		public Position target;
		public ISlideTrimmedCommand GetEntity (ISlideHead head, ISlideTrimmedCommand beforeCommand) {
			// SlideCommandStraightを取得する
			float sx, sy;
			if (beforeCommand == null) {
				CalcContinuedStraightPosition (head, out sx, out sy);
			}
			else {
				CalcContinuedStraightPosition (beforeCommand, out sx, out sy);
			}
			return new SlideCommandStraight(new Position (sx, sy), target);
		}
		public Object Turn(CallerInfo caller, Number amount) {
			target.Turn(caller, amount);
			return this;
		}
		public Object Turn(CallerInfo caller, IMsqValue degree) {
			target.Turn(caller, degree);
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			target.MirrorHolizontal(caller);
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			target.MirrorVertical(caller);
			return this;
		}
		public static void CalcContinuedStraightPosition(ISlideHead head, out float start_x, out float start_y) {
			if (head == null) {
				head = new SlideHeadNothing();
			}
			if (head is SlideHeadNothing) {
				start_x = 0;
				start_y = 0;
			}
			int? star = null;
			if (head.type == Type.SLIDE_HEAD_STAR) {
				star = (head as SlideHeadStar).GetIndex();
			}
			else if (head.type == Type.SLIDE_HEAD_BREAK) {
				star = (head as SlideHeadBreak).GetIndex();
			}
			if (star.HasValue) {
				int button = star.Value;
				// 右手.
				var pos = CircleCalculator.PointOnCircle(UnityEngine.Vector2.zero, 1, Constants.instance.GetPieceDegree(button));
				// 左手.
				pos = Constants.instance.ToLeftHandedCoordinateSystemPosition(pos);
				start_x = pos.x;
				start_y = pos.y;
			}
			else {
				start_x = 0;
				start_y = 0;
			}
		}
		public static bool CalcContinuedStraightPosition(ISlideTrimmedCommand beforeCommand, out float start_x, out float start_y) {
			// 前回が直線で今回も直線なら前回の終点をそのまま今回の始点に持ってくる.
			if (beforeCommand.type == Type.SLIDE_COMMAND_STRAIGHT) {
				var command = beforeCommand as SlideCommandStraight;
				// 左手.
				start_x = command.target.entity.x;
				start_y = command.target.entity.y;
				return true;
			}
			// 前回が曲線で今回は直線なら前回の終点を算出して、今回の始点に持ってくる.
			else if (beforeCommand.type == Type.SLIDE_COMMAND_CURVE) {
				var command = beforeCommand as SlideCommandCurve;
				// 右手.
				var pos = CircleCalculator.PointOnEllipse(Constants.instance.ToLeftHandedCoordinateSystemPosition(command.center.entity), command.radius.entity, command.start_degree.value + command.distance_degree.value);
				// 左手.
				pos = Constants.instance.ToLeftHandedCoordinateSystemPosition(pos);
				start_x = pos.x;
				start_y = pos.y;
				return true;
			}
			start_x = start_y = 0;
			return false;
		}
	}
	
	public class SlideCommandContinuedCurve : Object, ISlideContinuedCommand {
		public override Type type { get { return Type.SLIDE_COMMAND_CONTINUED_CURVE; } }
		public override Object Clone() { return new SlideCommandContinuedCurve(center.Clone().Cast<Position>(), (IMsqValue)distance_degree.Clone()); }
		public override string Log() {
			return "MsqScript.SlideCommandContinuedCurve(" + ")";
		}
		public override bool IsSlidePattern { get { return true; } }
		public override bool IsSlideChain { get { return true; } }
		public override bool IsSlideCommand { get { return true; } }
		public bool IsSlideContinuedCommand { get { return true; } }
		public bool IsStraight { get { return false; } }
		public bool IsCurve { get { return true; } }
		public bool IsArray { get { return false; } }
		public SlideCommandContinuedCurve(Position center, IMsqValue distance_degree) {
			this.center = center;
			this.distance_degree = distance_degree;
		}
		public Position center;
		public IMsqValue distance_degree;
		public ISlideTrimmedCommand GetEntity(ISlideHead head, ISlideTrimmedCommand beforeCommand) {
			// SlideCommandCurveを取得する
			float rx, ry, sd;
			if (beforeCommand == null) {
				CalcContinuedCurvePosition (head, center.entity, out rx, out ry, out sd);
			}
			else {
				CalcContinuedCurvePosition (beforeCommand, center.entity, out rx, out ry, out sd);
			}
			return new SlideCommandCurve(center, new Position(rx, ry), new Value(sd), distance_degree);
		}
		public Object Turn(CallerInfo caller, Number amount) {
			center.Turn(caller, amount);
			return this;
		}
		public Object Turn(CallerInfo caller, IMsqValue degree) {
			center.Turn(caller, degree);
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			center.MirrorHolizontal(caller);
			distance_degree.value *= -1;
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			center.MirrorVertical(caller);
			distance_degree.value *= -1;
			return this;
		}
		public static void CalcContinuedCurvePosition(ISlideHead head, UnityEngine.Vector2 center, out float radius_x, out float radius_y, out float start_degree) {
			UnityEngine.Vector2 pos;
			center = Constants.instance.ToLeftHandedCoordinateSystemPosition (center);
			// 右手.
			int? star = null;
			if (head == null) {
				head = new SlideHeadNothing();
			}
			if (head.type == Type.SLIDE_HEAD_STAR) {
				star = (head as SlideHeadStar).GetIndex();
			}
			else if (head.type == Type.SLIDE_HEAD_BREAK) {
				star = (head as SlideHeadBreak).GetIndex();
			}
			if (star.HasValue) {
				int button = star.Value;
				// 右手.
				pos = CircleCalculator.PointOnCircle (UnityEngine.Vector2.zero, 1, Constants.instance.GetPieceDegree (button));
				float radius = CircleCalculator.PointToPointDistance (center, pos);
				radius_x = radius;
				radius_y = radius;
				start_degree = CircleCalculator.PointToDegree(center, pos);
			}
			else {
				pos = UnityEngine.Vector2.zero;
				float radius = CircleCalculator.PointToPointDistance (center, pos);
				radius_x = radius;
				radius_y = radius;
				start_degree = CircleCalculator.PointToDegree (center, pos);
			}
		}
		public static bool CalcContinuedCurvePosition(ISlideTrimmedCommand beforeCommand, UnityEngine.Vector2 center, out float radius_x, out float radius_y, out float start_degree) {
			// 前回が直線で今回は曲線なら、前回の終点と、今回の回転軸から、今回の半径と始点角度を決める.
			if (beforeCommand.type == Type.SLIDE_COMMAND_STRAIGHT) {
				var command = beforeCommand as SlideCommandStraight;
				// 右手.
				center = Constants.instance.ToLeftHandedCoordinateSystemPosition(center);
				UnityEngine.Vector2 pos = Constants.instance.ToLeftHandedCoordinateSystemPosition(command.target.entity);
				float radius = CircleCalculator.PointToPointDistance(center, pos);
				radius_x = radius;
				radius_y = radius;
				start_degree = CircleCalculator.PointToDegree(center, pos);
				return true;
			}
			// 前回が曲線で今回も曲線なら、前回の回転軸・半径から求めた終点と、今回の回転軸から、今回の半径と始点角度を決める.
			else if (beforeCommand.type == Type.SLIDE_COMMAND_CURVE) {
				var command = beforeCommand as SlideCommandCurve;
				// 右手.
				center = Constants.instance.ToLeftHandedCoordinateSystemPosition(center);
				UnityEngine.Vector2 pos = CircleCalculator.PointOnEllipse(Constants.instance.ToLeftHandedCoordinateSystemPosition(command.center.entity), command.radius.entity, command.start_degree.value + command.distance_degree.value);
				float radius = CircleCalculator.PointToPointDistance(center, pos);
				radius_x = radius;
				radius_y = radius;
				start_degree = CircleCalculator.PointToDegree(center, pos);
				return true;
			}
			radius_x = radius_y = start_degree = 0;
			return false;
		}
	}
	
	public class SlideCommandArray : Object, ISlideCommand {
		public override Type type { get { return Type.SLIDE_COMMAND_ARRAY; } }
		public override Object Clone() { return new SlideChainArray(entity.ToArray()); }
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.SlideCommandArray[");
			int size = entity.Count;
			for (int i = 0; i < size; i++) {
				if (i > 0) {
					builder.Append(", ");
				}
				else {
					builder.Append(" ");
				}
				builder.Append(entity[i].Log());
			}
			builder.Append(" ]");
			return builder.ToString();
		}
		public override bool IsSlidePattern { get { return true; } }
		public override bool IsSlideChain { get { return true; } }
		public bool IsStraight { get { return false; } }
		public bool IsCurve { get { return false; } }
		public bool IsArray { get { return true; } }
		public List<ISlideCommandArrayContent> entity;
		public SlideCommandArray(params ISlideCommandArrayContent[] data) {
			entity = new List<ISlideCommandArrayContent>(data);
		}
		public SlideCommandArray(IEnumerable<ISlideCommandArrayContent> data) {
			entity = new List<ISlideCommandArrayContent>(data);
		}
		/// <summary>
		/// Arrayから作る。成功したらtrueが返る
		/// </summary>
		public bool FromArray(CallerInfo caller, Array array) {
			entity.Clear();
			int size = array.Length(caller).Cast<Number>().entity;
			for (int i = 0; i < size; i++) {
				var data = array.Get(caller, new Number(i));
				if (data.IsSlideCommand) {
					entity.Add((ISlideCommandArrayContent)data);
				}
				else {
					return false;
				}
			}
			return true;
		}
		public Object Turn(CallerInfo caller, Number amount) {
			if (entity != null) {
				foreach (var order in entity) {
					order.Turn(caller, amount);
				}
			}
			return this;
		}
		public Object Turn(CallerInfo caller, IMsqValue degree) {
			if (entity != null) {
				foreach (var order in entity) {
					order.Turn(caller, degree);
				}
			}
			return this;
		}
		public Object MirrorHolizontal(CallerInfo caller) {
			if (entity != null) {
				foreach (var order in entity) {
					order.MirrorHolizontal(caller);
				}
			}
			return this;
		}
		public Object MirrorVertical(CallerInfo caller) {
			if (entity != null) {
				foreach (var order in entity) {
					order.MirrorVertical(caller);
				}
			}
			return this;
		}
	}
	
	public class Array : Object {
		public override Type type { get { return Type.ARRAY; } }
		public override Object Clone() { return new Array(entity.ToArray()); }
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.Array[");
			int size = entity.Count;
			for (int i = 0; i < size; i++) {
				if (i > 0) {
					builder.Append(", ");
				}
				else {
					builder.Append(" ");
				}
				builder.Append(entity[i].Log());
			}
			builder.Append(" ]");
			return builder.ToString();
		}
		public List<Object> entity;
		public Array(params Object[] data) {
			entity = new List<Object>(data);
		}
		public Object Get(CallerInfo caller, Number index) {
			if (index.entity >= 0 && index.entity < entity.Count) {
				return entity[index.entity];
			}
			return new Exception(caller, ErrorCode.INDEX_OUT_OF_RANGE);
		}
		public Object Add(CallerInfo caller, params Object[] data) {
			entity.AddRange(data);
			return this;
		}
		public Object Set(CallerInfo caller, Number index, Object data) {
			if (index.entity >= 0 && index.entity < entity.Count) {
				entity[index.entity] = data;
				return this;
			}
			else if (index.entity == entity.Count) {
				entity.Add(data);
				return this;
			}
			return new Exception(caller, ErrorCode.INDEX_OUT_OF_RANGE);
		}
		public Object Insert(CallerInfo caller, Number index, params Object[] data) {
			if (index.entity >= 0 && index.entity < entity.Count) {
				entity.InsertRange(index.entity, data);
				return this;
			}
			else if (index.entity == entity.Count) {
				entity.AddRange(data);
				return this;
			}
			return new Exception(caller, ErrorCode.INDEX_OUT_OF_RANGE);
		}
		public Object Remove(CallerInfo caller, Number index) {
			if (index.entity >= 0 && index.entity < entity.Count) {
				entity.RemoveAt(index.entity);
				return this;
			}
			return new Exception(caller, ErrorCode.INDEX_OUT_OF_RANGE);
		}
		public Object Length(CallerInfo caller) {
			return new Number(entity.Count);
		}
		public Object IsEmpty(CallerInfo caller) {
			return new Flag(entity.Count == 0);
		}
	}
	
	public class Table : Object {
		public override Type type { get { return Type.TABLE; } }
		public override Object Clone() { return new Table(entity); }
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			builder.Append("MsqScript.Table{");
			int index = 0;
			foreach (var key in entity.Keys) {
				if (index > 0) {
					builder.Append(", ");
				}
				else {
					builder.Append(" ");
				}
				builder.Append("MsqScript.Pair(MsqScript.Text(");
				builder.Append(key);
				builder.Append("), ");
				builder.Append(entity[key].Log());
				builder.Append(")");
				index++;
			}
			builder.Append(" }");
			return builder.ToString();
		}
		public Dictionary<string, Object> entity;
		public Table() : this(null) { }
		public Table(Dictionary<string, Object> data) {
			if (data == null)
				entity = new Dictionary<string, Object>();
			else
				entity = new Dictionary<string, Object>(data);
		}
		public Object Get(CallerInfo caller, Text key) {
			if (entity.ContainsKey(key.entity)) {
				return entity[key.entity];
			}
			return new Exception(caller, ErrorCode.KEY_NOT_FOUND);
		}
		public Object Set(CallerInfo caller, Text key, Object value) {
			entity[key.entity] = value;
			return this;
		}
		public Object Remove(CallerInfo caller, Text key) {
			if (entity.ContainsKey(key.entity)) {
				entity.Remove(key.entity);
				return this;
			}
			return new Exception(caller, ErrorCode.KEY_NOT_FOUND);
		}
		public Object HasKey(CallerInfo caller, Text key) {
			return new Flag(entity.ContainsKey(key.entity));
		}
		public Object GetKeys(CallerInfo caller) {
			var keys = new Text[entity.Count];
			int index = 0;
			foreach (var key in entity.Keys) {
				keys[index] = new Text(key);
			}
			return new Array(keys);
		}
		public Object Length(CallerInfo caller) {
			return new Number(entity.Count);
		}
		public Object IsEmpty(CallerInfo caller) {
			return new Flag(entity.Count == 0);
		}
	}

	public class Success : Object {
		public override Type type { get { return Type.SUCCESS; } }
		public override Object Clone() { return new Success(); }
		public override string Log() {
			return "MsqScript.Success()";
		}
		public override bool IsSuccess { get { return true; } }
		public Success() { }
	}
	
	public class Exception : Object {
		public override Type type { get { return Type.EXCEPTION; } }
		public override Object Clone() { return new Exception(caller, code); } //place.Clone()を使いたいかも。
		public override string Log() {
			return "MsqScript.Exception(" + code.ToString() + ", " + GetPlaceLog() + ")";
		}
		public override bool IsException { get { return true; } }
		protected string GetPlaceLog() {
			if (place != null) {
				return "{ \"row\":" + place.row.ToString() +
					", \"column\":" + place.column.ToString() +
						" }";
			}
			else {
				return "unknown_place";
			}
		}
		protected CallerInfo caller;
		public TextDetailInfo place;
		public ErrorCode code;
		public Exception(CallerInfo caller, ErrorCode code) {
			this.caller = caller;
			this.place = caller.place;
			this.code = code;
		}
	}
	
	public class ArgumentsNotMatchException : Exception {
		public override Type type { get { return Type.ARGUMENTS_NOT_MATCH_EXCEPTION; } }
		public override Object Clone() { return new ArgumentsNotMatchException(caller, command, args); } //place.Clone()を使いたいかも。
		public override string Log() {
			var builder = new System.Text.StringBuilder();
			for (int i = 0; i < args.Length; i++) {
				if (i > 0) {
					builder.Append(", ");
				}
				builder.Append(args[i].type.ToString());
			}
			return "MsqScript.ArgumentsNotMatchException(" + field.ToString() + "." + command + "(" + builder.ToString() + ")" + ", " + GetPlaceLog() + ")";
		}
		public Type field;
		public string command;
		public Object[] args;
		public ArgumentsNotMatchException(CallerInfo caller, string command, Object[] args) 
		: base (caller, ErrorCode.COMMAND_AUGUMENTS_NOT_MATCH) {
			this.field = caller.field.type;
			this.command = command;
			this.args = args;
		}
	}
	
	public class CommandNotFoundException : ArgumentsNotMatchException {
		public override Type type { get { return Type.COMMAND_NOT_FOUND_EXCEPTION; } }
		public override Object Clone() { return new CommandNotFoundException(caller, command, args); } //place.Clone()を使いたいかも。
		public override string Log() {
			return "MsqScript.CommandNotFoundException(" + command + " in " + field.ToString() + ", " + GetPlaceLog() + ")";
		}
		public CommandNotFoundException(CallerInfo caller, string command, Object[] args) 
		: base (caller, command, args) {
			code = ErrorCode.COMMAND_NOT_FOUND;
		}
	}
	
}