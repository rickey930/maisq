﻿Shader "Custom/BackgroundImageShader" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Color", Color) = (1.0,1.0,1.0,1.0) // colors
	}
	SubShader {
		Pass {
			Lighting Off
			Cull Off
			Tags { "RenderType" = "Background" "Queue"="Background" }
            Blend One Zero
            ZWrite Off
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"

			uniform sampler2D _MainTex;
			uniform float4 _Color;
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};
			
			float4 _MainTex_ST;
			
			v2f vert (appdata_base v)
			{
				v2f o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
				return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
				half4 texcol = tex2D (_MainTex, i.uv).rgba;
				texcol.r = texcol.r * _Color.a;
				texcol.g = texcol.g * _Color.a;
				texcol.b = texcol.b * _Color.a;
				//texcol.a = texcol.a * _Color.a;
				return texcol;
			}

			ENDCG
		}
	}
	FallBack "Diffuse"
}
