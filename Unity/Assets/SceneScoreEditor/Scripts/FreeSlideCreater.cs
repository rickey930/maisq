﻿using UnityEngine;
using System.Collections;

public class FreeSlideCreater : MonoBehaviour {

	public System.Action<Vector2> onTouched { get; set; }

	// Use this for initialization
	void Start () {
		if (gameObject.GetComponent<CircleCollider2D> () == null) {
			var collider = gameObject.AddComponent<CircleCollider2D> ();
			collider.radius = 192;
		}
	}
	
	// Update is called once per frame
	void Update () {
		bool isPush;
		Vector3 touchPos;
		if (Input.touchSupported) {
			var touch = Input.GetTouch(0);
			isPush = touch.phase == TouchPhase.Began;
			touchPos = touch.position;
		}
		else {
			isPush = Input.GetMouseButtonDown(0);
			touchPos = Input.mousePosition;
		}
		if (isPush) {
			Vector2 tapPoint = Camera.main.ScreenToWorldPoint (touchPos);
			Collider2D[] colliders = Physics2D.OverlapPointAll (tapPoint);
			if (colliders != null) {
				foreach (var collider in colliders) {
					if (collider == collider2D) {
						Vector2 normalized = new Vector2(tapPoint.x / Constants.instance.MAIMAI_OUTER_RADIUS, tapPoint.y / Constants.instance.MAIMAI_OUTER_RADIUS);
						if (onTouched != null) {
							//Debug.Log (normalized);
							onTouched (normalized);
						}
					}
				}
			}
		}
	}
}
