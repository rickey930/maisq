﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreEditorBpmInputDialogController : MonoBehaviour {
	[SerializeField]
	private InputField textBox;

	public System.Action<float> onValueChanged { get; set; }
	public System.Action onClosing { get; set; }

	public void Show (float initValue) {
		gameObject.SetActive (true);
		textBox.text = initValue.ToString();
	}

	public void Hide () {
		if (onClosing != null) {
			onClosing ();
		}
		gameObject.SetActive (false);
	}

	public void OnOKButtonClick () {
		if (onValueChanged != null) {
			float value;
			if (float.TryParse (textBox.text, out value)) {
				if (value > 0) {
					onValueChanged (value);
				}
			}
		}
		Hide ();
	}
}
