﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreEditorDirectScriptInputDialogController : MonoBehaviour {
	[SerializeField]
	private InputField textBox;

	private string firstText { get; set; }

	public System.Action<string> onValueChanged { get; set; }
	public System.Action onClosing { get; set; }

	public void Show (string initValue) {
		gameObject.SetActive (true);
		firstText = initValue;
		textBox.text = initValue;
	}

	public void Hide () {
		if (onClosing != null) {
			onClosing ();
		}
		gameObject.SetActive (false);
	}

	public void OnOKButtonClick () {
		if (onValueChanged != null && textBox.text != firstText) {
			onValueChanged (textBox.text);
		}
		Hide ();
	}
}
