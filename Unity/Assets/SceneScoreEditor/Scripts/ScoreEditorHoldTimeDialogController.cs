﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreEditorHoldTimeDialogController : MonoBehaviour {
	[SerializeField]
	private InputField beatTextBox;
	[SerializeField]
	private InputField lengthTextBox;
	[SerializeField]
	private InputField intervalTextBox;
	[SerializeField]
	private Toggle useIntervalCheckBox;
	[SerializeField]
	private GameObject beatInputObj;
	[SerializeField]
	private GameObject intervalInputObj;

	public string dialogTab { get; set; }
	public System.Action<string, float, float> onBeatValueChanged { get; set; }
	public System.Action<string, float> onIntervalValueChanged { get; set; }
	public System.Action onClosing { get; set; }
	
	public void Show (string tag) {
		dialogTab = tag;
		gameObject.SetActive (true);
	}
	
	public void Hide () {
		if (onClosing != null) {
			onClosing ();
		}
		gameObject.SetActive (false);
	}
	
	public void OnOKButtonClick () {
		if (!useIntervalCheckBox.isOn) {
			if (onBeatValueChanged != null) {
				float value1, value2;
				if (float.TryParse(beatTextBox.text, out value1) &&
				    float.TryParse(lengthTextBox.text, out value2)) {
					onBeatValueChanged (dialogTab, value1, value2);
				}
			}
		}
		else {
			if (onIntervalValueChanged != null) {
				float value;
				if (float.TryParse(intervalTextBox.text, out value)) {
					onIntervalValueChanged (dialogTab, value);
				}
			}
		}
		Hide ();
	}
	
	public void OnIntervalCheckedChanged (bool value) {
		beatInputObj.SetActive (!value);
		intervalInputObj.SetActive (value);
	}
}
