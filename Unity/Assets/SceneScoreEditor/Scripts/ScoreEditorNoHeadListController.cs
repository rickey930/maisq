﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoreEditorNoHeadListController : MonoBehaviour, IInfiniteScrollSetup {

	ScoreEditorNoHeadListInformation[] contents;
	
	[SerializeField]
	ScoreEditorSceneManager manager;
	[SerializeField]
	private GameObject deleteButtonObj;
	[SerializeField]
	private GameObject dialogRoot;

	public int selectedIndex { get; set; }
	private List<MaisqScriptTree> starNothingSlideStarNoteCommands;
	
	
	public void Show (List<MaisqScriptTree> starNothingSlideStarNoteCommands) {
		this.starNothingSlideStarNoteCommands = starNothingSlideStarNoteCommands;

		contents = new ScoreEditorNoHeadListInformation[starNothingSlideStarNoteCommands.Count];
		for (int i = 0; i < contents.Length; i++) {
			contents[i] = new ScoreEditorNoHeadListInformation(starNothingSlideStarNoteCommands[i]);
		}
		if (contents.Length == 0) {
			selectedIndex = -1;
			HideSelectAfterButton ();
		}
		else {
			selectedIndex = 0;
			ShowSelectAfterButton ();
		}
		forceUpdateContents = true;
		ResizeScrollView ();

		dialogRoot.gameObject.SetActive (true);
	}
	
	public void Hide () {
		manager.UpdateNoteObjectsVision ();
		manager.UpdateSlideMarkersVision ();
		manager.OnNoHeadSlideListDialogClosing ();
		dialogRoot.gameObject.SetActive (false);
	}

	private void ShowSelectAfterButton () {
		deleteButtonObj.SetActive (true);
	}
	
	private void HideSelectAfterButton () {
		deleteButtonObj.SetActive (false);
	}

	public void OnDoubleTappedCell (ScoreEditorNoHeadListInformation info) {

	}

	public void OnTappedCell (ScoreEditorNoHeadListInformation info) {
		ShowSelectAfterButton ();
	}

	public void OnAddButtonClick () {
		manager.NoHeadSlidePutStartStateStart ();
		Hide ();
	}

	public void OnDeleteButtonClick () {
		manager.OnDeleteNoHeadSlideElement ();
		starNothingSlideStarNoteCommands.Remove (contents [selectedIndex].tree);
		if (selectedIndex > starNothingSlideStarNoteCommands.Count - 1) 
			selectedIndex = starNothingSlideStarNoteCommands.Count - 1;
		contents = new ScoreEditorNoHeadListInformation[starNothingSlideStarNoteCommands.Count];
		for (int i = 0; i < contents.Length; i++) {
			contents[i] = new ScoreEditorNoHeadListInformation(starNothingSlideStarNoteCommands[i]);
		}
		if (contents.Length == 0) {
			selectedIndex = -1;
			HideSelectAfterButton ();
		}
		forceUpdateContents = true;
	}

	#region IInfiniteScrollSetup
	public bool forceUpdateContents { get; set;	}
	public bool setupCompleted { get; set; }
	
	public IEnumerator OnPostSetupItems() {
		// リストの初期化.
		while (contents == null)
			yield return null;
		ResizeScrollView ();
		setupCompleted = true;
	}
	
	public void ResizeScrollView () {
		var infiniteScroll = GetComponent<InfiniteScroll> ();
		var rectTransform = GetComponent<RectTransform> ();
		var delta = rectTransform.sizeDelta;
		delta.y = (infiniteScroll.ItemScale * (contents.Length)) + (infiniteScroll.AnchordMargin * 2);
		rectTransform.sizeDelta = delta;
	}
	
	public void OnUpdateItem (int index, GameObject obj) {
		if (contents == null || index < 0 || index > contents.Length - 1) {
			if (obj.activeSelf) {
				obj.SetActive (false);
			}
		}
		else {
			if (!obj.activeSelf) {
				obj.SetActive (true);
			}
			var item = obj.GetComponent<ScoreEditorNoHeadNodeController> ();
			item.UpdateItem (index, contents [index]);
		}
	}
	#endregion
}
