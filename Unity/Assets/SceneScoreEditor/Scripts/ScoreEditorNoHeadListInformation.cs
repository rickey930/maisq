﻿using UnityEngine;
using System.Collections;

public class ScoreEditorNoHeadListInformation {
	public MaisqScriptTree tree;
	public string script { get; set; }

	public ScoreEditorNoHeadListInformation (MaisqScriptTree tree) {
		this.tree = tree;
		this.script = tree.Deploy();
	}
}
