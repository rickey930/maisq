﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreEditorNoHeadNodeController : MonoBehaviour {
	
	public ScoreEditorNoHeadListInformation info { get; set; }
	public ScoreEditorNoHeadListController controller;
	
	public int index { get; private set; }
	public Image button;
	public Text scriptLabel;

	public void UpdateItem (int index, ScoreEditorNoHeadListInformation info) {
		this.index = index;
		this.info = info;
		if (controller.selectedIndex == index) {
			float alpha = button.color.a;
			button.color = MaimaiStyleDesigner.ByteToPercentRGBA(255, 150, 0, (255 * alpha).ToInt());
		}
		else {
			float alpha = button.color.a;
			button.color = MaimaiStyleDesigner.ByteToPercentRGBA(255, 255, 255, (255 * alpha).ToInt());
		}
		scriptLabel.text = info.script;
	}
	
	public void OnButtonClick () {
		if (controller.selectedIndex == index) {
			controller.OnDoubleTappedCell (info);
		}
		else {
			controller.selectedIndex = index;
			controller.forceUpdateContents = true;
			controller.OnTappedCell (info);
		}
	}
}
