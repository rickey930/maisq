using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreEditorNoteObjectController : MonoBehaviour {
	[SerializeField]
	protected RectTransform translateHierarchy;
	[SerializeField]
	protected RectTransform rotateHierarchy;
	[SerializeField]
	protected Image image;

	public void Setup (int button) {
		translateHierarchy.anchoredPosition = new Vector3 (0, Constants.instance.MAIMAI_OUTER_RADIUS);
		rotateHierarchy.localRotation = Quaternion.Euler (Constants.instance.ToLeftHandedCoordinateSystemRotation(new Vector3 (0, 0, Constants.instance.GetPieceDegree (button))));
	}

	public void ChangeSprite (bool isEach, MaimaiNoteType noteType) {
		if (noteType == MaimaiNoteType.BREAK_CIRCLE) {
			image.sprite = SpriteTank.Get ("note_break");
		}
		else if (noteType == MaimaiNoteType.BREAK_STAR) {
			image.sprite = SpriteTank.Get ("note_star_break");
		}
		else if (noteType == MaimaiNoteType.TAP_CIRCLE) {
			if (!isEach) {
				image.sprite = SpriteTank.Get ("note_tap");
			}
			else {
				image.sprite = SpriteTank.Get ("note_tap_each");
			}
		}
		else if (noteType == MaimaiNoteType.TAP_STAR ||
		         noteType == MaimaiNoteType.SLIDE) {
			if (!isEach) {
				image.sprite = SpriteTank.Get ("note_star");
			}
			else {
				image.sprite = SpriteTank.Get ("note_star_each");
			}
		}
		else if (noteType == MaimaiNoteType.HOLD_HEAD ||
		         noteType == MaimaiNoteType.HOLD_FOOT) {
			if (!isEach) {
				image.sprite = SpriteTank.Get ("note_hold_corn_long");
			}
			else {
				image.sprite = SpriteTank.Get ("note_hold_each_corn_long");
			}
		}
	}
}
