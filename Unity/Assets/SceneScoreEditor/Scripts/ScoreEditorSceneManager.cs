using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ScoreEditorSceneManager : MonoBehaviour {
	/// <summary>
	/// undoで選択中インデックスも戻したい場合はtrueへ.
	/// </summary>
	private readonly bool UNDO_BACK_INDEX_HORMING = false;
	
	[SerializeField]
	private ScoreEditorTimeListController listCtrl;
	[SerializeField]
	private UnityEngine.UI.Image guideCircle;
	[SerializeField]
	private UnityEngine.UI.Image guideCircleInner;
	[SerializeField]
	private UnityEngine.UI.Image guideCircleCenter;
	[SerializeField]
	private MeshRenderer backgroundImage;
	[SerializeField]
	private Shader backgroundImageShader;
	[SerializeField]
	private GameObject sensorUIPrefab;
	[SerializeField]
	private RectTransform sensorUIParent;
	[SerializeField]
	private RectTransform innerSensorUIParent;
	[SerializeField]
	private RectTransform noteObjectsParent;
	[SerializeField]
	private RectTransform slideMarkersParent;
	[SerializeField]
	private GameObject tapNoteObjectPrefab;
	[SerializeField]
	private GameObject holdNoteObjectPrefab;
	[SerializeField]
	private GameObject starNoteObjectPrefab;
	[SerializeField]
	private GameObject slideMarkerParentPrefab;
	[SerializeField]
	private ScoreEditorBpmInputDialogController bpmInputDialog;
	[SerializeField]
	private ScoreEditorStepInputDialogController stepInputDialog;
	[SerializeField]
	private ScoreEditorHoldTimeDialogController holdTimeDialog;
	[SerializeField]
	private ScoreEditorNoHeadListController noHeadSlideListDialog;
	[SerializeField]
	private ScoreEditorDirectScriptInputDialogController directScriptInputDialog;
	[SerializeField]
	private Toggle bpmToogle;
	[SerializeField]
	private Toggle stepToogle;
	[SerializeField]
	private Text bpmToogleLabel;
	[SerializeField]
	private Text stepToogleLabel;
	[SerializeField]
	private Toggle multiSelectableToogle;
	[SerializeField]
	private GameObject timelineObj;
	[SerializeField]
	private GameObject noteTypeMenu;
	[SerializeField]
	private Kender.uGUI.ComboBox noteTypePulldown;
	[SerializeField]
	private GameObject slidePatternMenu;
	[SerializeField]
	private Kender.uGUI.ComboBox slidePatternPulldown;
	[SerializeField]
	private Slider slidePatternFreeCurveDegreeSlider;
	[SerializeField]
	private Button slidePatternContinueButton;
	[SerializeField]
	private Button slidePatternEndButton;
	[SerializeField]
	private Button slidePatternNextPatternButton;
	[SerializeField]
	private Button slidePatternNextChainButton;
	[SerializeField]
	private FreeSlideCreater freeSlider;
	[SerializeField]
	private InputField autoSaveIntervalSettingTextBox;
	
	public List<ScoreEditorTimeListInformation> nodeContents { get; set; }
	public ScoreEditorTimeListInformation[] contents { get; set; }
	public bool forceUpdateContents { get; set; }
	public ScoreEditorTimeListInformation currentContent { get; set; }
	
	public int selectedIndex { 
		get { 
			if (!multiSelectableMode)
				return selectionStartIndex;
			else
				return selectionEndIndex;
		} 
		set {
			if (!multiSelectableMode) {
				multiSelectedIndex1 = value;
				multiSelectedIndex2 = multiSelectedIndex1;
			}
			else {
				multiSelectedIndex2 = value;
			}
		}
	}
	private int biggestSelectedIndex { get; set; }
	public int selectionStartIndex { get { return Mathf.Min (multiSelectedIndex1, multiSelectedIndex2); } }
	public int selectionEndIndex { get { return Mathf.Max (multiSelectedIndex1, multiSelectedIndex2); } }
	private int multiSelectedIndex1;
	private int multiSelectedIndex2;
	private bool _multiSelectableMode; // 複数選択モード.
	public bool multiSelectableMode {
		get { return _multiSelectableMode; }
		set {
			if (!_multiSelectableMode && value)
				multiSelectedIndex2 = multiSelectedIndex1;
			else if (_multiSelectableMode && !value)
				multiSelectedIndex1 = multiSelectedIndex2;
			_multiSelectableMode = value;
		}
	}
	public int scoreEndMarkIndex { get; set; }
	private float timelineScrollAmount { get { return listCtrl.transform.RectCast ().anchoredPosition.x; } }
	private int lastSelectedSensorNumber { get; set; }
	private Constants.Circumference lastSelectedSensorCircum { get; set; }

	// スライド情報記憶.
	private int slideStartSensorNumber { get; set; }
	private Constants.Circumference slideStartSensorCircum { get; set; }
	private int slideTargetSensorNumber { get; set; }
	private Constants.Circumference slideTargetSensorCircum { get; set; }
	private Vector2 slideLastTouchPos { get; set; }
	private MaisqScriptTree slideCurrentTree { get; set; }
	private MaisqScriptTree slidePatternCurrentTree { get; set; }
	private MaisqScriptTree slideChainCurrentTree { get; set; }
	private MaisqScriptTree slideCommandInputingTree { get; set; }
	private int slideCurrentTreeCurrentIndex { get; set; } // slide(この中のindex));
	private int slideCurrentTreePatternCurrentIndex { get; set; } // slide(star(),step(),pattern(この中のindex));
	private bool slideFreeCurveCenterDecided { get; set; }
	private bool slideLastInputTypeIsFree { get; set; }
	private bool slideLastInputTypeIsFreeReservation { get; set; } //仮入力でのフリー入力か否か.
	private bool slideHeadInputed { get; set; } // スライドヘッドはNothingではないか.
	private Vector2 slideNoHeadStartPos { get; set; }
	private int slideHeadSensorNumber { get; set; }
	private Constants.Circumference slideHeadSensorCircum { get; set; }
	private List<SlideUndoRedoCommandProvider> slideUndoList;
	private List<SlideUndoRedoCommandProvider> slideRedoList;
	
	private SensorUIImageController[] outerSensor { get; set; }
	private SensorUIImageController[] innerCenterSensor { get; set; }
	private ScoreEditorScriptReader reader { get; set; }
	private bool bpmStepToggleProgramChanged { get; set; }
	private List<UndoRedoCommandProvider<ScoreEditorTimeListInformation>> undoList { get; set; }
	private List<UndoRedoCommandProvider<ScoreEditorTimeListInformation>> redoList { get; set; }
	private int firstSelectionStartIndex { get; set; }
	private int firstSelectionEndIndex { get; set; }
	private float firstTimelineScrollAmount { get; set; }
	private bool openedDialog { get; set; } //なんらかのダイアログが開いている状態である.
	private float keyTurboTimer { get; set; }
	private float keyTurboTime { get { return 0.3f; } }
	private bool initialized { get; set; }

	private class NoteObjectManager {
		private GameObject parent;
		public GameObject tapObj;
		public GameObject tapEachObj;
		public GameObject holdObj;
		public GameObject holdEachObj;
		public GameObject starObj;
		public GameObject starEachObj;
		public GameObject starBreakObj;
		public GameObject breakObj;
		public NoteObjectManager (int button, RectTransform parent, GameObject tapPrefab, GameObject holdPrefab) {
			this.parent = new GameObject ("Note Objects " + (1 + button).ToString());
			this.parent.SetParentEx(parent);
			tapObj = CreateNoteObject (tapPrefab, button, false, MaimaiNoteType.TAP_CIRCLE);
			tapEachObj = CreateNoteObject (tapPrefab, button, true, MaimaiNoteType.TAP_CIRCLE);
			holdObj = CreateNoteObject (holdPrefab, button, false, MaimaiNoteType.HOLD_HEAD);
			holdEachObj = CreateNoteObject (holdPrefab, button, true, MaimaiNoteType.HOLD_HEAD);
			starObj = CreateNoteObject (tapPrefab, button, false, MaimaiNoteType.TAP_STAR);
			starEachObj = CreateNoteObject (tapPrefab, button, true, MaimaiNoteType.TAP_STAR);
			starBreakObj = CreateNoteObject (tapPrefab, button, false, MaimaiNoteType.BREAK_STAR);
			breakObj = CreateNoteObject (tapPrefab, button, false, MaimaiNoteType.BREAK_CIRCLE);
		}

		private GameObject CreateNoteObject (GameObject prefab, int button, bool isEach, MaimaiNoteType noteType) {
			var noteObject = Instantiate (prefab) as GameObject;
			noteObject.SetParentEx (parent);
			var ctrl = noteObject.GetComponent<ScoreEditorNoteObjectController> ();
			ctrl.Setup (button);
			ctrl.ChangeSprite (isEach, noteType);
			noteObject.SetActive (false);
			return noteObject;
		}

		public void AllHide () {
			tapObj.SetActive (false);
			tapEachObj.SetActive (false);
			holdObj.SetActive (false);
			holdEachObj.SetActive (false);
			starObj.SetActive (false);
			starEachObj.SetActive (false);
			starBreakObj.SetActive (false);
			breakObj.SetActive (false);
		}
	}
	private NoteObjectManager[] noteObjectManager { get; set; }
	private List<ScoreEditorSlideMarkerPlaceController> slideMarkerManager { get; set; }
	private List<ScoreEditorStarNoteObjectController> starNoteManagers { get; set; }

	private enum ScoreEditorInputState {
		NOTE, SLIDE, NON_HEAD_START_POS,
	}
	private ScoreEditorInputState state { get; set; }

	private enum ScoreEditorSlidePositionInputType {
		ALL_SENSOR, OUTER_ONLY, INNER_ONLY, FREE, 
	}
	private ScoreEditorSlidePositionInputType slidePositionInputType { get; set; }

	private const int NOTETYPE_PULLDOWN_INDEX_TAP = 0;
	private const int NOTETYPE_PULLDOWN_INDEX_HOLD = 1;
	private const int NOTETYPE_PULLDOWN_INDEX_SLIDE = 2;
	private const int NOTETYPE_PULLDOWN_INDEX_BREAK = 3;
	private const int NOTETYPE_PULLDOWN_INDEX_BREAKSTAR = 4;
	private const int SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_STRAIGHT = 0;
	private const int SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_TURN_RIGHT = 1;
	private const int SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_TURN_LEFT = 2;
	private const int SLIDEPATTERN_PULLDOWN_INDEX_FREE_STRAIGHT = 3;
	private const int SLIDEPATTERN_PULLDOWN_INDEX_FREE_CURVE = 4;
	private const int SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_P = 5;
	private const int SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_Q = 6;
	private const int SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_S = 7;
	private const int SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_Z = 8;
	private const int SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_V = 9;

	// Use this for initialization
	IEnumerator Start () {
		initialized = false;

		undoList = new List<UndoRedoCommandProvider<ScoreEditorTimeListInformation>> ();
		redoList = new List<UndoRedoCommandProvider<ScoreEditorTimeListInformation>> ();

		while (EditorInformationController.instance == null) {
			yield return null;
		}
		firstSelectionStartIndex = EditorInformationController.instance.selectionStartIndex;
		firstSelectionEndIndex = EditorInformationController.instance.selectionEndIndex;
		firstTimelineScrollAmount = EditorInformationController.instance.timelineScrolledAmount;
		multiSelectedIndex1 = firstSelectionStartIndex;
		multiSelectedIndex2 = firstSelectionEndIndex;
		_multiSelectableMode = multiSelectedIndex1 != multiSelectedIndex2;
		scoreEndMarkIndex = EditorInformationController.instance.scoreEndMarkIndex;

		multiSelectableToogle.isOn = multiSelectableMode;

		MiscInformationController.instance.lastLoadedBgmKey = string.Empty;
		AudioManagerLite.Pause ();
		
		while (MaipadDynamicCreatedSpriteTank.instance.guideCircle6 == null ||
		       MaipadDynamicCreatedSpriteTank.instance.guideCircleInner == null ||
		       MaipadDynamicCreatedSpriteTank.instance.guideCircleCenter == null) {
			yield return null;
		}
		float radius = (Constants.instance.MAIMAI_OUTER_RADIUS + 5) * 2;
		guideCircle.transform.RectCast ().sizeDelta = new Vector2 (radius, radius);
		guideCircle.sprite = MaipadDynamicCreatedSpriteTank.instance.guideCircle6;
		radius = (Constants.instance.MAIMAI_INNER_RADIUS + 5) * 2;
		guideCircleInner.transform.RectCast ().sizeDelta = new Vector2 (radius, radius);
		guideCircleInner.sprite = MaipadDynamicCreatedSpriteTank.instance.guideCircleInner;
		radius = 8;
		guideCircleCenter.transform.RectCast ().sizeDelta = new Vector2 (radius, radius);
		guideCircleCenter.sprite = MaipadDynamicCreatedSpriteTank.instance.guideCircleCenter;
		CreateSensors ();

		if (EditorInformationController.instance.jacket != null) {
			//backgroundImage.sprite = EditorInformationController.instance.jacket;
			var mat = new Material(backgroundImageShader);
			mat.mainTexture = EditorInformationController.instance.jacket.texture;
			mat.SetColor("_Color", new Color(1, 1, 1, 1 - UserDataController.instance.config.brightness));
			backgroundImage.sharedMaterial = mat;
		}
		else {
			// ジャケットが無ければデフォ画像を表示.
			//backgroundImage.sprite = SpriteTank.Get ("bg_default");
			var mat = new Material(backgroundImageShader);
			mat.mainTexture = SpriteTank.Get ("bg_default").texture;
			mat.SetColor("_Color", new Color(1, 1, 1, 1 - UserDataController.instance.config.brightness));
			backgroundImage.sharedMaterial = mat;
		}

		bpmInputDialog.onValueChanged = (value) => { 
			undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectedIndex, new ScoreEditorTimeListInformation[] { currentContent.Clone() }));
			redoList.Clear ();
			if (selectedIndex > scoreEndMarkIndex && scoreEndMarkIndex >= 0) scoreEndMarkIndex = -1;
			currentContent.bpm = value;
			forceUpdateContents = true;
			UpdateBpmToggleText ();
		};
		bpmInputDialog.onClosing = () => { openedDialog = false; };
		stepInputDialog.onValueChanged = (flag, value) => {
			undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectedIndex, new ScoreEditorTimeListInformation[] { currentContent.Clone() }));
			redoList.Clear ();
			if (selectedIndex > scoreEndMarkIndex && scoreEndMarkIndex >= 0) scoreEndMarkIndex = -1;
			currentContent.step = RhythmGameLibrary.Score.ScoreStepData.Create(value, flag);
			forceUpdateContents = true;
			UpdateStepToggleText ();
		};
		stepInputDialog.onClosing = () => { openedDialog = false; };
		holdTimeDialog.onBeatValueChanged = (tag, beat, length) => {
			if (tag == "hold") {
				var tree = MaisqScriptTree.CreateRoot(MaisqScore.CommandNameDocument.HOLD);
				tree.AddChild (lastSelectedSensorNumber + 1);
				var step = tree.AddChild (MaisqScore.CommandNameDocument.STEP);
				step.AddChildren (beat, length);
				undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectedIndex, new ScoreEditorTimeListInformation[] { currentContent.Clone() }));
				redoList.Clear ();
				if (selectedIndex > scoreEndMarkIndex && scoreEndMarkIndex >= 0) scoreEndMarkIndex = -1;
				currentContent.noteCommands[lastSelectedSensorNumber].Clear ();
				currentContent.noteCommands[lastSelectedSensorNumber].Add (tree);
				currentContent.noteType[lastSelectedSensorNumber] = ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.HOLD;
			}
			else if (tag == "slide" || tag == "breakstar" || tag == "nohead") {
				undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectedIndex,  new ScoreEditorTimeListInformation[] { currentContent.Clone() }));
				redoList.Clear ();
				slideStartSensorNumber = lastSelectedSensorNumber;
				slideStartSensorCircum = lastSelectedSensorCircum;
				slideHeadSensorNumber = slideStartSensorNumber;
				slideHeadSensorCircum = slideStartSensorCircum;
				slideCurrentTree = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.SLIDE);
				slideCurrentTreeCurrentIndex = -1;
				if (tag == "slide") {
					slideHeadInputed = true;
					var head = slideCurrentTree.AddChild (MaisqScore.CommandNameDocument.STAR);
					slideCurrentTreeCurrentIndex++;
					head.AddChild (slideStartSensorNumber + 1);
					currentContent.noteType[lastSelectedSensorNumber] = ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.EDITING_SLIDE;
				}
				else if (tag == "breakstar") {
					slideHeadInputed = true;
					var head = slideCurrentTree.AddChild (MaisqScore.CommandNameDocument.BREAK_STAR);
					slideCurrentTreeCurrentIndex++;
					head.AddChild (slideStartSensorNumber + 1);
					currentContent.noteType[lastSelectedSensorNumber] = ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.EDITING_BREAKSTAR;
				}
				else if (tag == "nohead") {
					slideHeadInputed = false;
				}
				var step = slideCurrentTree.AddChild (MaisqScore.CommandNameDocument.STEP);
				slideCurrentTreeCurrentIndex++;
				step.AddChildren (beat, length);
				slidePatternCurrentTree = slideCurrentTree.AddChild (MaisqScore.CommandNameDocument.PATTERN);
				slideCurrentTreeCurrentIndex++;
				slideCurrentTreePatternCurrentIndex = -1;
				slideChainCurrentTree = slidePatternCurrentTree.AddChild (MaisqScore.CommandNameDocument.CHAIN);
				slideCurrentTreePatternCurrentIndex++;
				if (selectedIndex > scoreEndMarkIndex && scoreEndMarkIndex >= 0) scoreEndMarkIndex = -1;
				SlidePatternInputModeStart ();
			}
			UpdateNoteObjectsVision ();
			UpdateSlideMarkersVision ();
			forceUpdateContents = true;
		};
		holdTimeDialog.onIntervalValueChanged = (tag, interval) => {
			if (tag == "hold") {
				var tree = MaisqScriptTree.CreateRoot(MaisqScore.CommandNameDocument.HOLD);
				tree.AddChild (lastSelectedSensorNumber + 1);
				var step = tree.AddChild (MaisqScore.CommandNameDocument.STEP);
				step.AddChildren (interval);
				undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectedIndex, new ScoreEditorTimeListInformation[] { currentContent.Clone() }));
				redoList.Clear ();
				if (selectedIndex > scoreEndMarkIndex && scoreEndMarkIndex >= 0) scoreEndMarkIndex = -1;
				currentContent.noteCommands[lastSelectedSensorNumber].Clear ();
				currentContent.noteCommands[lastSelectedSensorNumber].Add (tree);
				currentContent.noteType[lastSelectedSensorNumber] = ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.HOLD;
			}
			else if (tag == "slide" || tag == "breakstar" || tag == "nohead") {
				undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectedIndex, new ScoreEditorTimeListInformation[] { currentContent.Clone() }));
				redoList.Clear ();
				slideStartSensorNumber = lastSelectedSensorNumber;
				slideStartSensorCircum = lastSelectedSensorCircum;
				slideHeadSensorNumber = slideStartSensorNumber;
				slideHeadSensorCircum = slideStartSensorCircum;
				slideCurrentTree = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.SLIDE);
				slideCurrentTreeCurrentIndex = -1;
				if (tag == "slide") {
					slideHeadInputed = true;
					var head = slideCurrentTree.AddChild (MaisqScore.CommandNameDocument.STAR);
					slideCurrentTreeCurrentIndex++;
					head.AddChild (slideStartSensorNumber + 1);
					currentContent.noteType[lastSelectedSensorNumber] = ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.EDITING_SLIDE;
				}
				else if (tag == "breakstar") {
					slideHeadInputed = true;
					var head = slideCurrentTree.AddChild (MaisqScore.CommandNameDocument.BREAK_STAR);
					slideCurrentTreeCurrentIndex++;
					head.AddChild (slideStartSensorNumber + 1);
					currentContent.noteType[lastSelectedSensorNumber] = ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.EDITING_BREAKSTAR;
				}
				else if (tag == "nohead") {
					slideHeadInputed = false;
				}
				var step = slideCurrentTree.AddChild (MaisqScore.CommandNameDocument.STEP);
				slideCurrentTreeCurrentIndex++;
				step.AddChild (interval);
				slidePatternCurrentTree = slideCurrentTree.AddChild (MaisqScore.CommandNameDocument.PATTERN);
				slideCurrentTreeCurrentIndex++;
				slideChainCurrentTree = slidePatternCurrentTree.AddChild (MaisqScore.CommandNameDocument.CHAIN);
				slideCurrentTreePatternCurrentIndex = 0;
				if (selectedIndex > scoreEndMarkIndex && scoreEndMarkIndex >= 0) scoreEndMarkIndex = -1;
				SlidePatternInputModeStart ();
			}
			UpdateNoteObjectsVision ();
			UpdateSlideMarkersVision ();
			forceUpdateContents = true;
		};
		holdTimeDialog.onClosing = () => {
			openedDialog = false;
			if (!noteTypeMenu.activeSelf && !slidePatternMenu.activeSelf) {
				noteTypeMenu.SetActive (true);
			}
		};
		slidePatternPulldown.OnItemSelected = OnPatternComboboxValueChanged;
		freeSlider.onTouched = (pos) => {
			if (state == ScoreEditorInputState.SLIDE && slidePositionInputType == ScoreEditorSlidePositionInputType.FREE && !slidePatternPulldown.GetOverlayGO().activeSelf && !openedDialog) {
				slideLastTouchPos = pos;
				slideTargetSensorNumber = slideStartSensorNumber;
				slideTargetSensorCircum = slideStartSensorCircum;
				if (SlidePatternTreeInput ()) {
					slidePatternContinueButton.gameObject.SetActive (true);
					slidePatternEndButton.gameObject.SetActive (true);
					slidePatternNextPatternButton.gameObject.SetActive (true);
					slidePatternNextChainButton.gameObject.SetActive (true);
				}
				UpdateSlideMarkersVision ();
			}
			else if (state == ScoreEditorInputState.NON_HEAD_START_POS && !openedDialog) {
				openedDialog = true;
				holdTimeDialog.Show ("nohead");
				slideNoHeadStartPos = pos;
			}
		};
		directScriptInputDialog.onValueChanged = (script) => {
			undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (GetUndoSelectionStartIndex (), GetUndoSelectionEndIndex (), GetUndoTimelineScrollAmount (), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectedIndex, new ScoreEditorTimeListInformation[] { currentContent.Clone () }));
			redoList.Clear ();
			script = MaisqScore.CommandNameDocument.NOTE + "(" + script + ");";
			var tree = MaisqScriptTree.CreateTreeFromMaiScript (script);
			var info = new ScoreEditorTimeListInformation();
			info.bpm = nodeContents[selectedIndex].bpm;
			info.step = nodeContents[selectedIndex].step;
			if (tree.Count > 0) {
				reader.ExtractMaisqScriptTree(info, tree[0]);
			}
			nodeContents[selectedIndex] = info;
			contents = nodeContents.ToArray ();
			forceUpdateContents = true;
			OnCellTapped (selectedIndex, nodeContents [selectedIndex]);
		};
		directScriptInputDialog.onClosing = () => {
			openedDialog = false;
		};

		slideMarkerManager = new List<ScoreEditorSlideMarkerPlaceController> ();
		starNoteManagers = new List<ScoreEditorStarNoteObjectController> ();
		noteObjectManager = new NoteObjectManager[8];
		for (int i = 0; i < noteObjectManager.Length; i++) {
			noteObjectManager[i] = new NoteObjectManager(i, noteObjectsParent, tapNoteObjectPrefab, holdNoteObjectPrefab);
		}

		NoteTypeInputModeStart ();

		reader = new ScoreEditorScriptReader ();

		if (EditorInformationController.instance.calledSetup) {
			nodeContents = EditorInformationController.instance.score;
		}
		else {
			nodeContents = new List<ScoreEditorTimeListInformation>();
		}

		if (nodeContents.Count == 0) {
			var firstContent = new ScoreEditorTimeListInformation ();
			firstContent.bpm = 100;
			firstContent.step = RhythmGameLibrary.Score.ScoreStepData.Create(4, false);
			nodeContents.Add (firstContent);
		}
		contents = nodeContents.ToArray ();
		if (selectedIndex >= 0 && selectedIndex < contents.Length) {
			currentContent = contents [selectedIndex];
		}
		else {
			currentContent = contents [0];
		}
		bpmStepToggleProgramChanged = true;
		bpmToogle.isOn = currentContent.bpm.HasValue;
		stepToogle.isOn = currentContent.step != null;
		bpmStepToggleProgramChanged = false;
		UpdateBpmToggleText ();
		UpdateStepToggleText ();
		UpdateNoteObjectsVision ();
		UpdateSlideMarkersVision ();

		listCtrl.transform.RectCast().anchoredPosition = new Vector2 (firstTimelineScrollAmount, 0);
		keyTurboTimer = 0;

		if (EditorInformationController.instance.calledSetup && !UserDataController.instance.user.score_editor_processing) {
			UserDataController.instance.user.score_editor_processing = true;
			UserDataController.instance.SaveUserInfo ();
		}
		// 一定時間ごとにオートセーブ.
		int autoSaveMinutes = UserDataController.instance.user.score_editor_auto_save_interval_minutes;
		autoSaveIntervalSettingTextBox.text = autoSaveMinutes.ToString();
		float autoSaveInterval = autoSaveMinutes * 60.0f;
		if (autoSaveInterval > 0) {
			InvokeRepeating ("AutoSave", autoSaveInterval, autoSaveInterval);
		}

		initialized = true;



//		// test.
//		string simai = "(180){4}1b-5[4:1],35662,,,E"; //UserDataController.instance.GetScore("創作(朧)_カラフル", "MASTER").score;
//		var simaiConv = new SimaiConverter();
//		string maiScript = simaiConv.ToMaiScript (simai);
//		reader.ReadMaiScript (maiScript);
//		var score = new List<ScoreEditorTimeListInformation> (reader.GetEditorScore ());
//		Debug.Log (maiScript);
//		Debug.Log (reader.ToMaiScript (reader.GetEditorScore (), -1));
	}

	private void CreateSensors() {
		outerSensor = new SensorUIImageController[8];
		for (int i = 0; i < 8; i++) {
			var sensorImageObj = Instantiate(sensorUIPrefab) as GameObject;
			sensorImageObj.name = "Outer Sensor for UI " + (i + 1).ToString();
			sensorImageObj.SetParentEx (sensorUIParent);
			var sensorImage = sensorImageObj.GetComponent<SensorUIImageController>();
			sensorImage.SetPosition (Constants.instance.GetPieceDegree(i), Constants.instance.MAIMAI_OUTER_RADIUS);
			sensorImage.Setup (SpriteTank.Get("icon_dammy"));
			outerSensor[i] = sensorImage;
		}
		innerCenterSensor = new SensorUIImageController[9];
		for (int i = 0; i < 8; i++) {
			var sensorImageObj = Instantiate(sensorUIPrefab) as GameObject;
			sensorImageObj.name = "Inner Sensor for UI " + (i + 1).ToString();
			sensorImageObj.SetParentEx (innerSensorUIParent);
			var sensorImage = sensorImageObj.GetComponent<SensorUIImageController>();
			sensorImage.SetPosition (Constants.instance.GetPieceDegree(i), Constants.instance.MAIMAI_INNER_RADIUS);
			sensorImage.Setup (SpriteTank.Get("icon_dammy"));
			sensorImage.effectOff = true;
			innerCenterSensor[i] = sensorImage;
		}
		for (int i = 8; i < 9; i++) {
			var sensorImageObj = Instantiate(sensorUIPrefab) as GameObject;
			sensorImageObj.name = "Center Sensor for UI";
			sensorImageObj.SetParentEx (innerSensorUIParent);
			var sensorImage = sensorImageObj.GetComponent<SensorUIImageController>();
			sensorImage.SetPosition (0, 0);
			sensorImage.Setup (SpriteTank.Get("icon_dammy"));
			sensorImage.effectOff = true;
			innerCenterSensor[i] = sensorImage;
		}
		outerSensor[0].onSensorClick = ()=>OnSensorButtonClick(Constants.Circumference.OUTER, 0);
		outerSensor[1].onSensorClick = ()=>OnSensorButtonClick(Constants.Circumference.OUTER, 1);
		outerSensor[2].onSensorClick = ()=>OnSensorButtonClick(Constants.Circumference.OUTER, 2);
		outerSensor[3].onSensorClick = ()=>OnSensorButtonClick(Constants.Circumference.OUTER, 3);
		outerSensor[4].onSensorClick = ()=>OnSensorButtonClick(Constants.Circumference.OUTER, 4);
		outerSensor[5].onSensorClick = ()=>OnSensorButtonClick(Constants.Circumference.OUTER, 5);
		outerSensor[6].onSensorClick = ()=>OnSensorButtonClick(Constants.Circumference.OUTER, 6);
		outerSensor[7].onSensorClick = ()=>OnSensorButtonClick(Constants.Circumference.OUTER, 7);
		innerCenterSensor[0].onSensorClick = ()=>OnSensorButtonClick(Constants.Circumference.INNER, 0);
		innerCenterSensor[1].onSensorClick = ()=>OnSensorButtonClick(Constants.Circumference.INNER, 1);
		innerCenterSensor[2].onSensorClick = ()=>OnSensorButtonClick(Constants.Circumference.INNER, 2);
		innerCenterSensor[3].onSensorClick = ()=>OnSensorButtonClick(Constants.Circumference.INNER, 3);
		innerCenterSensor[4].onSensorClick = ()=>OnSensorButtonClick(Constants.Circumference.INNER, 4);
		innerCenterSensor[5].onSensorClick = ()=>OnSensorButtonClick(Constants.Circumference.INNER, 5);
		innerCenterSensor[6].onSensorClick = ()=>OnSensorButtonClick(Constants.Circumference.INNER, 6);
		innerCenterSensor[7].onSensorClick = ()=>OnSensorButtonClick(Constants.Circumference.INNER, 7);
		innerCenterSensor[8].onSensorClick = ()=>OnSensorButtonClick(Constants.Circumference.CENTER, 0);
	}
	
	// Update is called once per frame
	void Update () {
		if (!initialized)
			return;

		if ((state == ScoreEditorInputState.NOTE) && !openedDialog) {
			if (Input.GetKeyDown (KeyCode.Z)) {
				Undo ();
			}
			else if (Input.GetKeyDown (KeyCode.Y)) {
				Redo ();
			}
			else if (Input.GetKeyDown (KeyCode.P)) {
				Play ();
			}
			else if (Input.GetKeyDown (KeyCode.Escape)) {
				Exit ();
			}
			else if (Input.GetKeyDown (KeyCode.X)) {
				Cut ();
			}
			else if (Input.GetKeyDown (KeyCode.C)) {
				Copy ();
			}
			else if (Input.GetKeyDown (KeyCode.V)) {
				InsertPaste ();
			}
			else if (Input.GetKeyDown (KeyCode.M)) {
				multiSelectableToogle.isOn = !multiSelectableToogle.isOn;
			}
			else if (Input.GetKeyDown (KeyCode.T)) {
				noteTypePulldown.SelectedIndex = NOTETYPE_PULLDOWN_INDEX_TAP;
			}
			else if (Input.GetKeyDown (KeyCode.H)) {
				noteTypePulldown.SelectedIndex = NOTETYPE_PULLDOWN_INDEX_HOLD;
			}
			else if (Input.GetKeyDown (KeyCode.S)) {
				noteTypePulldown.SelectedIndex = NOTETYPE_PULLDOWN_INDEX_SLIDE;
			}
			else if (Input.GetKeyDown (KeyCode.D)) {
				noteTypePulldown.SelectedIndex = NOTETYPE_PULLDOWN_INDEX_BREAKSTAR;
			}
			else if (Input.GetKeyDown (KeyCode.B)) {
				noteTypePulldown.SelectedIndex = NOTETYPE_PULLDOWN_INDEX_BREAK;
			}
			else if (Input.GetKeyDown (KeyCode.Q)) {
				Turn ();
			}
			else if (Input.GetKeyDown (KeyCode.W)) {
				Mirror ();
			}
			else if (Input.GetKeyDown (KeyCode.Insert)) {
				Insert ();
			}
			else if (Input.GetKeyDown (KeyCode.Delete) || Input.GetKeyDown (KeyCode.Backspace)) {
				Remove ();
			}
			else if (Input.GetKeyDown (KeyCode.E)) {
				SetScoreEndMark ();
			}
			else if (Input.GetKeyDown (KeyCode.F1)) {
				ShowNoHeadSlideListDialog ();
			}
			else if (Input.GetKeyDown (KeyCode.F2)) {
				ShowDirectScriptInputDialog ();
			}
			else if (Input.GetKeyDown (KeyCode.Alpha1) || Input.GetKeyDown (KeyCode.Keypad1)) {
				OnSensorButtonClick (Constants.Circumference.OUTER, 0);
			}
			else if (Input.GetKeyDown (KeyCode.Alpha2) || Input.GetKeyDown (KeyCode.Keypad2)) {
				OnSensorButtonClick (Constants.Circumference.OUTER, 1);
			}
			else if (Input.GetKeyDown (KeyCode.Alpha3) || Input.GetKeyDown (KeyCode.Keypad3)) {
				OnSensorButtonClick (Constants.Circumference.OUTER, 2);
			}
			else if (Input.GetKeyDown (KeyCode.Alpha4) || Input.GetKeyDown (KeyCode.Keypad4)) {
				OnSensorButtonClick (Constants.Circumference.OUTER, 3);
			}
			else if (Input.GetKeyDown (KeyCode.Alpha5) || Input.GetKeyDown (KeyCode.Keypad5)) {
				OnSensorButtonClick (Constants.Circumference.OUTER, 4);
			}
			else if (Input.GetKeyDown (KeyCode.Alpha6) || Input.GetKeyDown (KeyCode.Keypad6)) {
				OnSensorButtonClick (Constants.Circumference.OUTER, 5);
			}
			else if (Input.GetKeyDown (KeyCode.Alpha7) || Input.GetKeyDown (KeyCode.Keypad7)) {
				OnSensorButtonClick (Constants.Circumference.OUTER, 6);
			}
			else if (Input.GetKeyDown (KeyCode.Alpha8) || Input.GetKeyDown (KeyCode.Keypad8)) {
				OnSensorButtonClick (Constants.Circumference.OUTER, 7);
			}
			else if (Input.GetKeyDown (KeyCode.RightArrow) || (Input.GetKey (KeyCode.RightArrow) && keyTurboTimer > keyTurboTime)) {
				if (!multiSelectableMode) {
					multiSelectedIndex1++;
					multiSelectedIndex2 = multiSelectedIndex1;
				}
				else {
					multiSelectedIndex2++;
				}
				const float timelineItemWidth = 30.0f;
				if (!multiSelectableMode || multiSelectedIndex2 > multiSelectedIndex1) {
					listCtrl.transform.RectCast().anchoredPosition = new Vector2 (listCtrl.transform.RectCast().anchoredPosition.x - timelineItemWidth, 0);
				}
				OnCellTapped (selectedIndex, contents[selectedIndex]);
				forceUpdateContents = true;
			}
			else if (Input.GetKeyDown (KeyCode.LeftArrow) || (Input.GetKey (KeyCode.LeftArrow) && keyTurboTimer > keyTurboTime)) {
				int i;
				if (!multiSelectableMode) {
					i = multiSelectedIndex1 - 1;
				}
				else {
					i = multiSelectedIndex2 - 1;
				}
				if (i < 0) i = 0;
				if (!multiSelectableMode) {
					multiSelectedIndex1 = i;
					multiSelectedIndex2 = multiSelectedIndex1;
				}
				else {
					multiSelectedIndex2 = i;
				}
				const float timelineItemWidth = 30.0f;
				if (!multiSelectableMode || multiSelectedIndex1 > multiSelectedIndex2) {
					listCtrl.transform.RectCast().anchoredPosition = new Vector2 (listCtrl.transform.RectCast().anchoredPosition.x + timelineItemWidth, 0);
				}
				OnCellTapped (selectedIndex, contents[selectedIndex]);
				forceUpdateContents = true;
			}

			if (Input.GetKeyUp (KeyCode.RightArrow)) {
				keyTurboTimer = 0;
			}
			else if (Input.GetKeyUp (KeyCode.LeftArrow)) {
				keyTurboTimer = 0;
			}
			else if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.LeftArrow)) {
				if (keyTurboTimer <= keyTurboTime) {
					keyTurboTimer += Time.deltaTime;
				}
			}
		}
		else if ((state == ScoreEditorInputState.SLIDE) && !openedDialog) {
			if (Input.GetKeyDown (KeyCode.Z)) {
				SlideUndo ();
			}
			else if (Input.GetKeyDown (KeyCode.Y)) {
				SlideRedo ();
			}
			else if (Input.GetKeyDown (KeyCode.S)) {
				if (!slidePatternPulldown.Items[SLIDEPATTERN_PULLDOWN_INDEX_FREE_STRAIGHT].IsDisabled) {
					slidePatternPulldown.SelectedIndex = SLIDEPATTERN_PULLDOWN_INDEX_FREE_STRAIGHT;
				}
			}
			else if (Input.GetKeyDown (KeyCode.C)) {
				if (!slidePatternPulldown.Items[SLIDEPATTERN_PULLDOWN_INDEX_FREE_CURVE].IsDisabled) {
					slidePatternPulldown.SelectedIndex = SLIDEPATTERN_PULLDOWN_INDEX_FREE_CURVE;
				}
			}
			else if (Input.GetKeyDown (KeyCode.Q)) {
				if (slidePatternContinueButton.gameObject.activeSelf) {
					SlidePatternInputContinue ();
				}
			}
			else if (Input.GetKeyDown (KeyCode.W)) {
				if (slidePatternEndButton.gameObject.activeSelf) {
					SlidePatternInputEnd ();
				}
			}
			else if (Input.GetKeyDown (KeyCode.E)) {
				if (slidePatternNextChainButton.gameObject.activeSelf) {
					ChangeCurrentChain ();
				}
			}
			else if (Input.GetKeyDown (KeyCode.R)) {
				if (slidePatternNextPatternButton.gameObject.activeSelf) {
					ChangeCurrentPattern ();
				}
			}
		}
	}

	public void CreateNewContent () {
		nodeContents.Add (new ScoreEditorTimeListInformation ());
		contents = nodeContents.ToArray ();
	}

	public int GetNearBpmChangedIndex () { return GetNearBpmChangedIndex (selectedIndex); }
	public int GetNearBpmChangedIndex (int nowIndex) {
		for (int i = nowIndex; i >= 0; i--) {
			if (nodeContents[i].bpm.HasValue) {
				return i;
			}
		}
		return 0;
	}
	
	public int GetNearStepChangedIndex () { return GetNearStepChangedIndex (selectedIndex); }
	public int GetNearStepChangedIndex (int nowIndex) {
		for (int i = nowIndex; i >= 0; i--) {
			if (nodeContents[i].step != null) {
				return i;
			}
		}
		return 0;
	}

	public void OnCellTapped (int index, ScoreEditorTimeListInformation info) {
		biggestSelectedIndex = Mathf.Max (selectionEndIndex, biggestSelectedIndex);
		currentContent = contents [index];
		bpmStepToggleProgramChanged = true;
		bpmToogle.isOn = currentContent.bpm.HasValue;
		stepToogle.isOn = currentContent.step != null;
		bpmStepToggleProgramChanged = false;
		UpdateBpmToggleText ();
		UpdateStepToggleText ();
		UpdateNoteObjectsVision ();
		UpdateSlideMarkersVision ();
	}

	public void OnCellDoubleTapped (int index, ScoreEditorTimeListInformation info) {
		forceUpdateContents = true;
	}

	public void OnSensorButtonClick (Constants.Circumference circum, int button) {
		if (state == ScoreEditorInputState.NOTE) {
			lastSelectedSensorNumber = button;
			lastSelectedSensorCircum = circum;
			bool deleteflg = false;
			if (noteTypePulldown.SelectedIndex == NOTETYPE_PULLDOWN_INDEX_TAP) { // TAP.
				if (circum == Constants.Circumference.OUTER) {
					if (currentContent.noteType[button] != ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.TAP) {
						var tree = MaisqScriptTree.CreateRoot(MaisqScore.CommandNameDocument.TAP);
						tree.AddChild(button + 1);
						undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectedIndex, new ScoreEditorTimeListInformation[] { currentContent.Clone() }));
						redoList.Clear ();
						if (selectedIndex > scoreEndMarkIndex && scoreEndMarkIndex >= 0) scoreEndMarkIndex = -1;
						currentContent.noteCommands[button].Clear ();
						currentContent.noteCommands[button].Add (tree);
						currentContent.noteType[lastSelectedSensorNumber] = ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.TAP;
						UpdateNoteObjectsVision ();
						UpdateSlideMarkersVision ();
						forceUpdateContents = true;
					}
					else {
						deleteflg = true;
					}
				}
			}
			else if (noteTypePulldown.SelectedIndex == NOTETYPE_PULLDOWN_INDEX_HOLD) { // HOLD.
				if (circum == Constants.Circumference.OUTER) {
					if (currentContent.noteType[button] != ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.HOLD) {
						openedDialog = true;
						holdTimeDialog.Show ("hold");
					}
					else {
						deleteflg = true;
					}
				}
			}
			else if (noteTypePulldown.SelectedIndex == NOTETYPE_PULLDOWN_INDEX_SLIDE) { // SLIDE.
				if (circum == Constants.Circumference.OUTER) {
					if (currentContent.noteType[button] != ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.SLIDE) {
						openedDialog = true;
						holdTimeDialog.Show ("slide");
					}
					else {
						deleteflg = true;
					}
				}
			}
			else if (noteTypePulldown.SelectedIndex == NOTETYPE_PULLDOWN_INDEX_BREAK) { // BREAK.
				if (circum == Constants.Circumference.OUTER) {
					if (currentContent.noteType[button] != ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.BREAK) {
						var tree = MaisqScriptTree.CreateRoot(MaisqScore.CommandNameDocument.BREAK);
						tree.AddChild(button + 1);
						undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectedIndex, new ScoreEditorTimeListInformation[] { currentContent.Clone() }));
						redoList.Clear ();
						if (selectedIndex > scoreEndMarkIndex && scoreEndMarkIndex >= 0) scoreEndMarkIndex = -1;
						currentContent.noteCommands[button].Clear ();
						currentContent.noteCommands[button].Add (tree);
						currentContent.noteType[lastSelectedSensorNumber] = ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.BREAK;
						UpdateNoteObjectsVision ();
						UpdateSlideMarkersVision ();
						forceUpdateContents = true;
					}
					else {
						deleteflg = true;
					}
				}
			}
			else if (noteTypePulldown.SelectedIndex == NOTETYPE_PULLDOWN_INDEX_BREAKSTAR) { // BREAK STAR SLIDE.
				if (circum == Constants.Circumference.OUTER) {
					if (currentContent.noteType[button] != ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.BREAKSTAR_SLIDE) {
						openedDialog = true;
						holdTimeDialog.Show ("breakstar");
					}
					else {
						deleteflg = true;
					}
				}
			}

			if (deleteflg) {
				undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectedIndex, new ScoreEditorTimeListInformation[] { currentContent.Clone() }));
				redoList.Clear ();
				currentContent.noteCommands[button].Clear ();
				currentContent.noteType[lastSelectedSensorNumber] = ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.NONE;
				UpdateNoteObjectsVision ();
				UpdateSlideMarkersVision ();
				forceUpdateContents = true;
			}
		}
		else if (state == ScoreEditorInputState.SLIDE) {
			if (circum == Constants.Circumference.OUTER) {
				if (slidePositionInputType == ScoreEditorSlidePositionInputType.ALL_SENSOR ||
				    slidePositionInputType == ScoreEditorSlidePositionInputType.OUTER_ONLY) {
					slideTargetSensorNumber = button;
					slideTargetSensorCircum = circum;
					if (SlidePatternTreeInput ()) {
						slidePatternContinueButton.gameObject.SetActive (true);
						slidePatternEndButton.gameObject.SetActive (true);
						slidePatternNextPatternButton.gameObject.SetActive (true);
						slidePatternNextChainButton.gameObject.SetActive (true);
					}
					UpdateSlideMarkersVision ();
				}
			}
			else if (circum == Constants.Circumference.INNER) {
				if (slidePositionInputType == ScoreEditorSlidePositionInputType.ALL_SENSOR ||
				    slidePositionInputType == ScoreEditorSlidePositionInputType.INNER_ONLY) {
					slideTargetSensorNumber = button;
					slideTargetSensorCircum = circum;
					if (SlidePatternTreeInput ()) {
						slidePatternContinueButton.gameObject.SetActive (true);
						slidePatternEndButton.gameObject.SetActive (true);
						slidePatternNextPatternButton.gameObject.SetActive (true);
						slidePatternNextChainButton.gameObject.SetActive (true);
					}
					UpdateSlideMarkersVision ();
				}
			}
			else if (circum == Constants.Circumference.CENTER) {
				if (slidePositionInputType == ScoreEditorSlidePositionInputType.ALL_SENSOR) {
					slideTargetSensorNumber = button;
					slideTargetSensorCircum = circum;
					if (SlidePatternTreeInput ()) {
						slidePatternContinueButton.gameObject.SetActive (true);
						slidePatternEndButton.gameObject.SetActive (true);
						slidePatternNextPatternButton.gameObject.SetActive (true);
						slidePatternNextChainButton.gameObject.SetActive (true);
					}
					UpdateSlideMarkersVision ();
				}
			}
		}
	}

	public void OnBpmToogleChecked (bool mark) {
		if (!bpmStepToggleProgramChanged) {
			forceUpdateContents = true;
			if ((mark && !currentContent.bpm.HasValue) || (!mark && selectedIndex == 0)) {
				float initValue = nodeContents[GetNearBpmChangedIndex ()].bpm.Value;
				openedDialog = true;
				bpmInputDialog.Show(initValue);
				if (!mark) {
					bpmToogle.isOn = true;
				}
			}
			else if (!mark && currentContent.bpm.HasValue && selectedIndex > 0) {
				undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectedIndex, new ScoreEditorTimeListInformation[] { currentContent.Clone() }));
				redoList.Clear ();
				if (selectedIndex > scoreEndMarkIndex && scoreEndMarkIndex >= 0) scoreEndMarkIndex = -1;
				currentContent.bpm = null;
			}
		}
	}

	public void OnStepToogleChecked (bool mark) {
		if (!bpmStepToggleProgramChanged) {
			forceUpdateContents = true;
			if ((mark && currentContent.step == null) || (!mark && selectedIndex == 0)) {
				int changedIndex = GetNearStepChangedIndex ();
				bool initCheck = nodeContents[changedIndex].step.is_interval;
				float initValue = nodeContents[changedIndex].step.step;
				openedDialog = true;
				stepInputDialog.Show(initCheck, initValue);
				if (!mark) {
					stepToogle.isOn = true;
				}
			}
			else if (!mark && currentContent.step != null && selectedIndex > 0) {
				undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectedIndex, new ScoreEditorTimeListInformation[] { currentContent.Clone() }));
				redoList.Clear ();
				if (selectedIndex > scoreEndMarkIndex && scoreEndMarkIndex >= 0) scoreEndMarkIndex = -1;
				currentContent.step = null;
			}
		}
	}

	private void UpdateBpmToggleText () {
		float value = nodeContents[GetNearBpmChangedIndex()].bpm.Value;
		bpmToogleLabel.text = "bpm(" + value + ")";
	}

	private void UpdateStepToggleText () {
		int changedIndex = GetNearStepChangedIndex ();
		bool flag = nodeContents[changedIndex].step.is_interval;
		float value = nodeContents[changedIndex].step.step;
		stepToogleLabel.text = (flag ? "interval" : "beat") + "{" + value + "}";
	}

	public void UpdateNoteObjectsVision (bool isEach = false) {
		bool objectActived = false;
		for (int i = 0; i < currentContent.noteType.Length; i++) {
			noteObjectManager[i].AllHide();
			if (currentContent.noteType[i] == ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.TAP) {
				if (isEach) {
					noteObjectManager[i].tapEachObj.SetActive (true);
				}
				else if (objectActived) {
					UpdateNoteObjectsVision (true);
					return;
				}
				else {
					noteObjectManager[i].tapObj.SetActive (true);
				}
				objectActived = true;
			}
			else if (currentContent.noteType[i] == ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.HOLD) {
				if (isEach) {
					noteObjectManager[i].holdEachObj.SetActive (true);
				}
				else if (objectActived) {
					UpdateNoteObjectsVision (true);
					return;
				}
				else {
					noteObjectManager[i].holdObj.SetActive (true);
				}
				objectActived = true;
			}
			else if (currentContent.noteType[i] == ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.SLIDE ||
			         currentContent.noteType[i] == ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.EDITING_SLIDE){
				if (isEach) {
					noteObjectManager[i].starEachObj.SetActive (true);
				}
				else if (objectActived) {
					UpdateNoteObjectsVision (true);
					return;
				}
				else {
					noteObjectManager[i].starObj.SetActive (true);
				}
				objectActived = true;
			}
			else if (currentContent.noteType[i] == ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.BREAKSTAR_SLIDE ||
			         currentContent.noteType[i] == ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.EDITING_BREAKSTAR) {
				if (!isEach && objectActived) {
					UpdateNoteObjectsVision (true);
					return;
				}
				else {
					noteObjectManager[i].starBreakObj.SetActive (true);
				}
				objectActived = true;
			}
			else if (currentContent.noteType[i] == ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.BREAK) {
				if (!isEach && objectActived) {
					UpdateNoteObjectsVision (true);
					return;
				}
				else {
					noteObjectManager[i].breakObj.SetActive (true);
				}
				objectActived = true;
			}
		}
	}

	public void UpdateSlideMarkersVision () {
		var managers = slideMarkerManager.ToArray ();
		foreach (var manager in managers) {
			Destroy (manager.gameObject);
		}
		managers = null;
		slideMarkerManager.Clear ();
		var managers2 = starNoteManagers.ToArray ();
		foreach (var manager in managers2) {
			Destroy (manager.gameObject);
		}
		managers2 = null;
		starNoteManagers.Clear ();

		int starNoteCount = 0;
		int someTypeEachCount = 0;
		for (int i = 0; i < currentContent.noteType.Length && starNoteCount < 2; i++) {
			if (currentContent.noteType[i] == ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.SLIDE ||
			    currentContent.noteType[i] == ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.BREAKSTAR_SLIDE){
				starNoteCount++;
			}
			if (currentContent.noteType[i] != ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.NONE) {
				someTypeEachCount++;
			}
		}
		for (int i = 0; i < currentContent.starNothingSlideStarNoteCommands.Count && starNoteCount < 2; i++) {
			starNoteCount++;
		}
		if (slideCurrentTree != null) {
			starNoteCount++;
		}

		if (starNoteCount > 0) {
			var each = new List<MaimaiScore.INote>();
			for (int i = 0; i < currentContent.noteType.Length; i++) {
				if (currentContent.noteType[i] == ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.SLIDE ||
				    currentContent.noteType[i] == ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.BREAKSTAR_SLIDE){
					foreach (var command in currentContent.noteCommands[i]) {
						var note = reader.ReadCreateCommandTypeOfNote (command);
						each.Add (note);
					}
				}
			}
			for (int i = 0; i < currentContent.starNothingSlideStarNoteCommands.Count; i++) {
				var note = reader.ReadCreateCommandTypeOfNote(currentContent.starNothingSlideStarNoteCommands[i]);
				each.Add (note);
			}

			if (slideCurrentTree != null) {
				MaisqScriptTree copy = slideCurrentTree.Clone ();
				if (slideCommandInputingTree != null) {
					// slide(pattern(chain(の子供とする.
					copy.children[slideCurrentTreeCurrentIndex].children[slideCurrentTreePatternCurrentIndex].children.Add (slideCommandInputingTree);
				}
				var note = reader.ReadCreateCommandTypeOfNote (copy);
				if (note != null) {
					each.Add (note);
				}
			}

			bool isEach = starNoteCount > 1;
			var r = new MaimaiScoreReader ();
			r.SetBpm (nodeContents [GetNearBpmChangedIndex ()].bpm.Value);
			var stepData = nodeContents [GetNearStepChangedIndex ()].step;
			if (stepData != null) {
				if (!stepData.is_interval) r.SetBeat (stepData.step);
				else r.SetInterval (stepData.step);
			}
			r.SetNote (each.ToArray ());
			var converter = new MaimaiScoreConverter (r.GetMaimaiScore ());
			var notes = converter.ReadScore ();

			// 複数パターンスライドであるかのチェック.
			var hash = new HashSet<MaimaiSlidePattern>();
			foreach (var note in notes) {
				if (note.GetNoteType() == MaimaiNoteType.SLIDE) {
					var sNote = note as MaimaiSlideNote;
					hash.Add (sNote.pattern);
				}
			}
			bool isMultiPattern = hash.Count > 1;
			hash.Clear();

			foreach (var note in notes) {
				if (note.GetNoteType() == MaimaiNoteType.TAP_STAR || note.GetNoteType() == MaimaiNoteType.BREAK_STAR) {
					var starObj = Instantiate (starNoteObjectPrefab) as GameObject;
					starObj.SetParentEx (slideMarkersParent);
					var starManager = starObj.GetComponent<ScoreEditorStarNoteObjectController> ();
					var sNote = note as MaimaiStarNote;
					starManager.Setup (sNote.GetButtonId());
					noteObjectManager[sNote.GetButtonId()].AllHide();
					starManager.ChangeSprite (someTypeEachCount > 1, note.GetNoteType() == MaimaiNoteType.BREAK_STAR, sNote.GetRelativeNote().Length > 1);
					starNoteManagers.Add (starManager);
				}
				else if (note.GetNoteType() == MaimaiNoteType.SLIDE_HEAD) {
//					var hNote = note as MaimaiSlideNoteHead;
//					bool hEach = hNote.GetEachNotes().Count > 0;
//					bool multiPattern = hNote.GetRelativeNote().Length > 1;
//					var starObj = Instantiate (starNoteObjectPrefab) as GameObject;
//					starObj.SetParentEx (slideMarkersParent);
//					var starManager = starObj.GetComponent<ScoreEditorStarNoteObjectController> ();
//					starManager.Setup ((note as IMaimaiSlideNoteHead).GetRelativeNote()[0].chains[0].commands[0].startPos);
//					starManager.ChangeSprite (hEach, false, multiPattern);
//					starNoteManagers.Add (starManager);
				}
				else if (note.GetNoteType() == MaimaiNoteType.SLIDE) {
					var sNote = note as MaimaiSlideNote;
					if (sNote != null && hash.Add (sNote.pattern)) {
						var markerManagerObj = Instantiate (slideMarkerParentPrefab) as GameObject;
						markerManagerObj.SetParentEx (slideMarkersParent);
						var markerManager = markerManagerObj.GetComponent<ScoreEditorSlideMarkerPlaceController> ();
						markerManager.slidePatternInfo = sNote.pattern;
						markerManager.isEach = isEach || isMultiPattern;
						slideMarkerManager.Add (markerManager);
					}
				}
			}
			// ☆をマーカーより上に描画する
			foreach (var s in starNoteManagers) {
				s.SetAsLastSibling ();
			}
			hash.Clear ();
		}
	}

	private void NoteTypeInputModeStart () {
		state = ScoreEditorInputState.NOTE;
		slidePatternMenu.SetActive (false);
		timelineObj.SetActive (true);
		noteTypeMenu.SetActive (true);

		slideCurrentTree = null;
		slideCommandInputingTree = null;
		slideFreeCurveCenterDecided = false;
		slideLastInputTypeIsFree = false;
		slideLastInputTypeIsFreeReservation = false;
		SlidePatternPulldownAllValid ();
	}

	private void SlidePatternInputModeStart () {
		state = ScoreEditorInputState.SLIDE;
		noteTypeMenu.SetActive (false);
		timelineObj.SetActive (false);
		slidePatternMenu.SetActive (true);
		slidePatternContinueButton.gameObject.SetActive (false);
		slidePatternEndButton.gameObject.SetActive (false);
		slidePatternNextPatternButton.gameObject.SetActive (false);
		slidePatternNextChainButton.gameObject.SetActive (false);
		if (!slideHeadInputed) {
			SlidePatternPulldownInvalidElementsAfterFree ();
		}
		SetSlidePositionInputType (slidePatternPulldown.SelectedIndex);
	}

	private void SetSlidePositionInputType (int slidePatternPulldownSelectedIndex) {
		if (slidePatternPulldownSelectedIndex == SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_STRAIGHT) {
			slidePositionInputType = ScoreEditorSlidePositionInputType.ALL_SENSOR;
		}
		else if (slidePatternPulldownSelectedIndex == SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_TURN_RIGHT ||
		         slidePatternPulldownSelectedIndex == SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_TURN_LEFT) {
			if (slideStartSensorCircum == Constants.Circumference.OUTER) {
				slidePositionInputType = ScoreEditorSlidePositionInputType.OUTER_ONLY;
			}
			else if (slideStartSensorCircum == Constants.Circumference.INNER) {
				slidePositionInputType = ScoreEditorSlidePositionInputType.INNER_ONLY;
			}
		}
		else if (slidePatternPulldownSelectedIndex == SLIDEPATTERN_PULLDOWN_INDEX_FREE_STRAIGHT ||
		         slidePatternPulldownSelectedIndex == SLIDEPATTERN_PULLDOWN_INDEX_FREE_CURVE) {
			slidePositionInputType = ScoreEditorSlidePositionInputType.FREE;
		}
		else {
			slidePositionInputType = ScoreEditorSlidePositionInputType.OUTER_ONLY;
		}
	}

	public void OnPatternComboboxValueChanged (int index) {
		if (index == SLIDEPATTERN_PULLDOWN_INDEX_FREE_CURVE) { // free curve.
			slidePatternFreeCurveDegreeSlider.gameObject.SetActive (true);
		}
		else {
			slidePatternFreeCurveDegreeSlider.gameObject.SetActive (false);
		}
		SetSlidePositionInputType (index);
	}

	public void SlidePatternInputContinue () {
		if (slideCommandInputingTree != null) {
			CreateSlideHistory ();
			slideCommandInputingTree.parent = slideChainCurrentTree;
			slideChainCurrentTree.children.Add (slideCommandInputingTree);
			slideCommandInputingTree = null;
			slideFreeCurveCenterDecided = false;
			slideStartSensorNumber = slideTargetSensorNumber;
			slideStartSensorCircum = slideTargetSensorCircum;
			slidePatternContinueButton.gameObject.SetActive (false);
			slidePatternNextPatternButton.gameObject.SetActive (false);
			slidePatternNextChainButton.gameObject.SetActive (false);
			
			// コンボボックスのアイテムの非セレクタブル化 (フリーの後はフリー以外できない、センターの後はフリーとストレートしかできないなど).
			if (slideLastInputTypeIsFreeReservation) {
				SlidePatternPulldownInvalidElementsAfterFree ();
				slideLastInputTypeIsFree = true;
			}
			else {
				if (slideStartSensorCircum == Constants.Circumference.CENTER) {
					SlidePatternPulldownInvalidElementsAfterCenterSensor ();
				}
				else if (slideStartSensorCircum == Constants.Circumference.INNER) {
					SlidePatternPulldownInvalidElementsAfterInnerSensor ();
				}
				else {
					SlidePatternPulldownAllValid ();
				}
				slideLastInputTypeIsFree = false;
			}
		}
	}

	public void SlidePatternInputEnd () {
		if (slideCommandInputingTree != null) {
			slideCommandInputingTree.parent = slideChainCurrentTree;
			slideChainCurrentTree.children.Add (slideCommandInputingTree);
		}
		if (slideCurrentTree != null) {
			if (slideHeadInputed) {
				if (slideUndoList != null) slideUndoList.Clear ();
				if (slideRedoList != null) slideRedoList.Clear ();
				currentContent.noteCommands[lastSelectedSensorNumber].Clear ();
				currentContent.noteCommands[lastSelectedSensorNumber].Add (slideCurrentTree);
				if (currentContent.noteType[lastSelectedSensorNumber] == ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.EDITING_SLIDE) {
					currentContent.noteType[lastSelectedSensorNumber] = ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.SLIDE;
				}
				else if (currentContent.noteType[lastSelectedSensorNumber] == ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.EDITING_BREAKSTAR) {
					currentContent.noteType[lastSelectedSensorNumber] = ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.BREAKSTAR_SLIDE;
				}
			}
			else {
				currentContent.starNothingSlideStarNoteCommands.Add (slideCurrentTree);
			}
		}
		NoteTypeInputModeStart ();
	}
	
	public void OnSlidePatternFreeCurveDegreeSliderValueChanged (float value) {
		if (slideFreeCurveCenterDecided) {
			slideTargetSensorNumber = slideStartSensorNumber;
			slideTargetSensorCircum = slideStartSensorCircum;
			if (SlidePatternTreeInput ()) {
				slidePatternContinueButton.gameObject.SetActive (true);
				slidePatternEndButton.gameObject.SetActive (true);
				slidePatternNextPatternButton.gameObject.SetActive (true);
				slidePatternNextChainButton.gameObject.SetActive (true);
			}
			UpdateSlideMarkersVision ();
		}
	}

	private bool SlidePatternTreeInput () {
		slideFreeCurveCenterDecided = false;
		if (slidePatternPulldown.SelectedIndex == SLIDEPATTERN_PULLDOWN_INDEX_FREE_STRAIGHT) { // straight (free)
			if (!slideHeadInputed && slideChainCurrentTree.children.Count == 0) {
				slideCommandInputingTree = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.STRAIGHT);
				var startPosTree = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.POSITION);
				startPosTree.AddChildren (slideNoHeadStartPos.x, slideNoHeadStartPos.y);
				slideCommandInputingTree.children.Add (startPosTree);
			}
			else {
				slideCommandInputingTree = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.CONTINUED_STRAIGHT);
			}
			var posTree = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.POSITION);
			posTree.AddChildren (slideLastTouchPos.x, slideLastTouchPos.y);
			slideCommandInputingTree.children.Add (posTree);
			slideLastInputTypeIsFreeReservation = true;
			return true;
		}
		else if (slidePatternPulldown.SelectedIndex == SLIDEPATTERN_PULLDOWN_INDEX_FREE_CURVE) { // curve (free)
			if (!slideHeadInputed && slideChainCurrentTree.children.Count == 0) {
				slideCommandInputingTree = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.CURVE);
				float rx, ry, sd;
				MaimaiScore.SlideCommandCurve.CalcContinuedCurvePosition (
					MaimaiScore.SlideCommandStraight.Create (Vector2.zero, slideNoHeadStartPos),
					slideLastTouchPos,
					out rx, out ry, out sd);
				var centerPosTree = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.POSITION);
				centerPosTree.AddChildren (slideLastTouchPos.x, slideLastTouchPos.y);
				slideCommandInputingTree.children.Add (centerPosTree);
				var radiusPosTree = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.POSITION);
				radiusPosTree.AddChildren (rx, ry);
				slideCommandInputingTree.children.Add (radiusPosTree);
				slideCommandInputingTree.AddChildren (sd, (slidePatternFreeCurveDegreeSlider.value * 360.0f).ToString());
			}
			else {
				slideCommandInputingTree = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.CONTINUED_CURVE);
				var posTree = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.POSITION);
				posTree.AddChildren (slideLastTouchPos.x, slideLastTouchPos.y);
				slideCommandInputingTree.children.Add (posTree);
				slideCommandInputingTree.AddChild ((slidePatternFreeCurveDegreeSlider.value * 360.0f).ToString());
			}
			slideFreeCurveCenterDecided = true;
			slideLastInputTypeIsFreeReservation = true;
			return true;
		}
		else {
			string startButton = (slideStartSensorNumber + 1).ToString ();
			string startCircum = slideStartSensorCircum == Constants.Circumference.OUTER ? "outer" :
				slideStartSensorCircum == Constants.Circumference.INNER ? "inner" : "center";
			string targetButton = (slideTargetSensorNumber + 1).ToString ();
			string targetCircum = slideTargetSensorCircum == Constants.Circumference.OUTER ? "outer" :
				slideTargetSensorCircum == Constants.Circumference.INNER ? "inner" : "center";

			if (slidePatternPulldown.SelectedIndex == SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_STRAIGHT) { // straight (sensor)
				if (!slideLastInputTypeIsFree && slideStartSensorCircum != slideTargetSensorCircum || slideStartSensorNumber != slideTargetSensorNumber) {
					slideCommandInputingTree = MaisqScriptTree.CreateRoot (startCircum + "_straight_" + targetCircum);
					if (slideStartSensorCircum == Constants.Circumference.CENTER) {
						slideCommandInputingTree.AddChildren (targetButton);
					}
					else if (slideTargetSensorCircum == Constants.Circumference.CENTER) {
						slideCommandInputingTree.AddChildren (startButton);
					}
					else {
						slideCommandInputingTree.AddChildren (startButton, targetButton);
					}
					slideLastInputTypeIsFreeReservation = false;
					return true;
				}
			}
			else if (slidePatternPulldown.SelectedIndex == SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_TURN_RIGHT) { // turn right (sensor)
				if (!slideLastInputTypeIsFree && slideStartSensorCircum == slideTargetSensorCircum && slideStartSensorCircum != Constants.Circumference.CENTER) {
					slideCommandInputingTree = MaisqScriptTree.CreateRoot (startCircum + "_curve_right_axis_center");
					slideCommandInputingTree.AddChildren (startButton, targetButton);
					slideLastInputTypeIsFreeReservation = false;
					return true;
				}
			}
			else if (slidePatternPulldown.SelectedIndex == SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_TURN_LEFT) { //turn left (sensor)
				if (!slideLastInputTypeIsFree && slideStartSensorCircum == slideTargetSensorCircum && slideStartSensorCircum != Constants.Circumference.CENTER) {
					slideCommandInputingTree = MaisqScriptTree.CreateRoot (startCircum + "_curve_left_axis_center");
					slideCommandInputingTree.AddChildren (startButton, targetButton);
					slideLastInputTypeIsFreeReservation = false;
					return true;
				}
			}
			else if (slidePatternPulldown.SelectedIndex == SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_P) { //shape p (sensor)
				if (!slideLastInputTypeIsFree && slideStartSensorCircum == Constants.Circumference.OUTER && slideTargetSensorCircum == Constants.Circumference.OUTER) {
					slideCommandInputingTree = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.SHAPE_P_AXIS_CENTER);
					slideCommandInputingTree.AddChildren (startButton, targetButton);
					slideLastInputTypeIsFreeReservation = false;
					return true;
				}
			}
			else if (slidePatternPulldown.SelectedIndex == SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_Q) { //shape q (sensor)
				if (!slideLastInputTypeIsFree && slideStartSensorCircum == Constants.Circumference.OUTER && slideTargetSensorCircum == Constants.Circumference.OUTER) {
					slideCommandInputingTree = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.SHAPE_Q_AXIS_CENTER);
					slideCommandInputingTree.AddChildren (startButton, targetButton);
					slideLastInputTypeIsFreeReservation = false;
					return true;
				}
			}
			else if (slidePatternPulldown.SelectedIndex == SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_S) { //shape s (sensor)
				if (!slideLastInputTypeIsFree && slideStartSensorCircum == Constants.Circumference.OUTER && slideTargetSensorCircum == Constants.Circumference.OUTER) {
					slideCommandInputingTree = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.SHAPE_S);
					slideCommandInputingTree.AddChildren (startButton, targetButton);
					slideLastInputTypeIsFreeReservation = false;
					return true;
				}
			}
			else if (slidePatternPulldown.SelectedIndex == SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_Z) { //shape z (sensor)
				if (!slideLastInputTypeIsFree && slideStartSensorCircum == Constants.Circumference.OUTER && slideTargetSensorCircum == Constants.Circumference.OUTER) {
					slideCommandInputingTree = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.SHAPE_Z);
					slideCommandInputingTree.AddChildren (startButton, targetButton);
					slideLastInputTypeIsFreeReservation = false;
					return true;
				}
			}
			else if (slidePatternPulldown.SelectedIndex == SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_V) { //shape v (sensor)
				if (!slideLastInputTypeIsFree && slideStartSensorCircum == Constants.Circumference.OUTER && slideTargetSensorCircum == Constants.Circumference.OUTER) {
					slideCommandInputingTree = MaisqScriptTree.CreateRoot (MaisqScore.CommandNameDocument.SHAPE_V_AXIS_CENTER);
					slideCommandInputingTree.AddChildren (startButton, targetButton);
					slideLastInputTypeIsFreeReservation = false;
					return true;
				}
			}
		}
		return false;
	}

	public void SlidePatternPulldownInvalidElementsAfterFree () {
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_STRAIGHT].IsDisabled = true;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_TURN_RIGHT].IsDisabled = true;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_TURN_LEFT].IsDisabled = true;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_P].IsDisabled = true;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_Q].IsDisabled = true;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_S].IsDisabled = true;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_Z].IsDisabled = true;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_V].IsDisabled = true;

		if (slidePatternPulldown.SelectedIndex != SLIDEPATTERN_PULLDOWN_INDEX_FREE_STRAIGHT &&
		    slidePatternPulldown.SelectedIndex != SLIDEPATTERN_PULLDOWN_INDEX_FREE_CURVE) {
			slidePatternPulldown.SelectedIndex = SLIDEPATTERN_PULLDOWN_INDEX_FREE_STRAIGHT;
			SetSlidePositionInputType (slidePatternPulldown.SelectedIndex);
		}
	}
	
	public void SlidePatternPulldownInvalidElementsAfterInnerSensor () {
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_STRAIGHT].IsDisabled = false;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_TURN_RIGHT].IsDisabled = false;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_TURN_LEFT].IsDisabled = false;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_P].IsDisabled = true;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_Q].IsDisabled = true;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_S].IsDisabled = true;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_Z].IsDisabled = true;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_V].IsDisabled = true;

		if (slidePatternPulldown.SelectedIndex != SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_STRAIGHT &&
		    slidePatternPulldown.SelectedIndex != SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_TURN_RIGHT &&
		    slidePatternPulldown.SelectedIndex != SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_TURN_LEFT &&
		    slidePatternPulldown.SelectedIndex != SLIDEPATTERN_PULLDOWN_INDEX_FREE_STRAIGHT &&
		    slidePatternPulldown.SelectedIndex != SLIDEPATTERN_PULLDOWN_INDEX_FREE_CURVE) {
			slidePatternPulldown.SelectedIndex = SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_STRAIGHT;
			SetSlidePositionInputType (slidePatternPulldown.SelectedIndex);
		}
	}
	
	public void SlidePatternPulldownInvalidElementsAfterCenterSensor () {
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_STRAIGHT].IsDisabled = false;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_TURN_RIGHT].IsDisabled = true;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_TURN_LEFT].IsDisabled = true;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_P].IsDisabled = true;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_Q].IsDisabled = true;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_S].IsDisabled = true;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_Z].IsDisabled = true;
		slidePatternPulldown.Items [SLIDEPATTERN_PULLDOWN_INDEX_SHAPE_V].IsDisabled = true;

		if (slidePatternPulldown.SelectedIndex != SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_STRAIGHT &&
		    slidePatternPulldown.SelectedIndex != SLIDEPATTERN_PULLDOWN_INDEX_FREE_STRAIGHT &&
		    slidePatternPulldown.SelectedIndex != SLIDEPATTERN_PULLDOWN_INDEX_FREE_CURVE) {
			slidePatternPulldown.SelectedIndex = SLIDEPATTERN_PULLDOWN_INDEX_SENSOR_STRAIGHT;
			SetSlidePositionInputType (slidePatternPulldown.SelectedIndex);
		}
	}

	public void SlidePatternPulldownAllValid () {
		foreach (var item in slidePatternPulldown.Items) {
			item.IsDisabled = false;
		}
	}

	/*
	 * 入力途中のスライドの表示方法を考える.
	 * タップしたら入力候補を表示する.
	 * パターンタイプを選びなおしたらタップしたフラグを場合によって解除する.
	 * inputedフラグがtrueならcontinueボタンを表示する.
	 */




	private ScoreEditorTimeListInformation[] clipboard;

	public void ChangeMultiSelectableMode (bool isMultiSelectableMode) {
		this.multiSelectableMode = isMultiSelectableMode;
	}

	public void Cut () {
		Copy ();
		Remove ();
	}

	public void Clip () {
		Copy ();
		Erase ();
	}

	public void Copy () {
		var ret = new List<ScoreEditorTimeListInformation> ();
		for (int i = selectionStartIndex; i <= selectionEndIndex && i < contents.Length; i++) {
			ret.Add (contents[i].Clone());
		}
		if (ret.Count > 0) {
			ret[0].bpm = nodeContents[GetNearBpmChangedIndex(selectionStartIndex)].bpm;
			ret[0].step = nodeContents[GetNearStepChangedIndex (selectionStartIndex)].step;
			if (ret[0].step != null) ret[0].step = ret[0].step.Clone();
		}
		clipboard = ret.ToArray ();
	}

	public void InsertPaste () {
		if (clipboard == null) return;
		
		undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateRemoveHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectionStartIndex, clipboard.Length));
		redoList.Clear ();
		if (scoreEndMarkIndex >= 0) {
			if (selectionStartIndex > scoreEndMarkIndex) {
				scoreEndMarkIndex = -1;
			}
			else {
				scoreEndMarkIndex += clipboard.Length;
			}
		}

		var ret = new List<ScoreEditorTimeListInformation> ();
		for (int i = 0; i < clipboard.Length; i++) {
			ret.Add (clipboard[i].Clone());
		}
		nodeContents.InsertRange(selectionStartIndex, ret);
		// bpm/stepの設定が変わらなければ元に戻す.
		if (selectionStartIndex > 0) {
			var near = nodeContents [GetNearBpmChangedIndex (selectionStartIndex - 1)];
			if (clipboard [0].bpm == near.bpm) {
				nodeContents [selectionStartIndex].bpm = null;
			}
			near = nodeContents [GetNearStepChangedIndex (selectionStartIndex - 1)];
			if (clipboard [0].step.step == near.step.step && clipboard [0].step.is_interval == near.step.is_interval) {
				nodeContents [selectionStartIndex].step = null;
			}
		}
		contents = nodeContents.ToArray ();
		listCtrl.ResizeScrollView ();
		forceUpdateContents = true;
		OnCellTapped (selectedIndex, contents[selectedIndex]);
		
		multiSelectedIndex1 = selectionStartIndex;
		multiSelectedIndex2 = multiSelectedIndex1;
	}

	public void OverwritePaste () {
		OverwritePaste (false);
	}
	
	public void MixPaste () {
		OverwritePaste (true);
	}

	public void OverwritePaste (bool mix) {
		if (clipboard == null) return;
		var undoCommands = new List<ScoreEditorTimeListInformation> ();
		for (int i = selectionStartIndex; selectionStartIndex != selectionEndIndex ? i <= selectionEndIndex : i - selectionStartIndex < clipboard.Length; i++) {
			if (i < nodeContents.Count) {
				if (i - selectionStartIndex < clipboard.Length) {
					if (!mix) {
						undoCommands.Add (nodeContents[i].Clone());
						nodeContents[i] = clipboard[i - selectionStartIndex].Clone();
					}
					else {
						undoCommands.Add (nodeContents[i].Clone());
						nodeContents[i] = clipboard[i - selectionStartIndex].Clone(nodeContents[i]);
					}
				}
				else {
					undoCommands.Add (nodeContents[i].Clone());
					nodeContents[i] = new ScoreEditorTimeListInformation();
				}
			}
			else {
				if (i - selectionStartIndex < clipboard.Length) {
					undoCommands.Add (new ScoreEditorTimeListInformation());
					nodeContents.Add (clipboard[i - selectionStartIndex].Clone());
				}
				else {
					undoCommands.Add (new ScoreEditorTimeListInformation());
					nodeContents.Add (new ScoreEditorTimeListInformation());
				}
			}
		}
		undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectionStartIndex, undoCommands.ToArray()));
		redoList.Clear ();
		if ((selectionStartIndex + clipboard.Length - 1 > scoreEndMarkIndex || selectionEndIndex > scoreEndMarkIndex) && scoreEndMarkIndex >= 0) scoreEndMarkIndex = -1;
		// bpm/stepの設定が変わらなければ元に戻す.
		if (selectionStartIndex > 0) {
			var near = nodeContents [GetNearBpmChangedIndex (selectionStartIndex - 1)];
			if (clipboard [0].bpm == near.bpm) {
				nodeContents [selectionStartIndex].bpm = null;
			}
			near = nodeContents [GetNearStepChangedIndex (selectionStartIndex - 1)];
			if (clipboard [0].step.step == near.step.step && clipboard [0].step.is_interval == near.step.is_interval) {
				nodeContents [selectionStartIndex].step = null;
			}
		}
		contents = nodeContents.ToArray ();
		listCtrl.ResizeScrollView ();
		forceUpdateContents = true;
		OnCellTapped (selectedIndex, contents[selectedIndex]);
	}

	public void Insert () {
		var ret = new List<ScoreEditorTimeListInformation> ();
		for (int i = selectionStartIndex; i <= selectionEndIndex; i++) {
			var newContent = new ScoreEditorTimeListInformation();
			if (i == 0) {
				newContent.bpm = contents[0].bpm;
				newContent.step = contents[0].step;
				if (newContent.step != null) newContent.step = newContent.step.Clone();
			}
			ret.Add (newContent);
		}
		undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateRemoveHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectionStartIndex, ret.Count));
		redoList.Clear ();
		if (scoreEndMarkIndex >= 0) {
			if (selectionEndIndex <= scoreEndMarkIndex) {
				scoreEndMarkIndex += ret.Count;
			}
		}
		nodeContents.InsertRange(selectionStartIndex, ret);
		contents = nodeContents.ToArray ();
		listCtrl.ResizeScrollView ();
		forceUpdateContents = true;
		OnCellTapped (selectedIndex, contents[selectedIndex]);
	}

	public void Remove () {
		var undoCommands = new List<ScoreEditorTimeListInformation> ();
		for (int i = selectionStartIndex; i <= selectionEndIndex && i < nodeContents.Count; i++) {
			undoCommands.Add (nodeContents[i].Clone());
		}
		undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateInsertHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectionStartIndex, undoCommands.ToArray()));
		redoList.Clear ();

		// リムーブ範囲より後ろのノートのBPM/Step設定
		if (selectionEndIndex + 1 < nodeContents.Count) {
			nodeContents[selectionEndIndex + 1].bpm = nodeContents[GetNearBpmChangedIndex(selectionEndIndex + 1)].bpm;
			nodeContents[selectionEndIndex + 1].step = nodeContents[GetNearStepChangedIndex(selectionEndIndex + 1)].step;
			if (nodeContents[selectionEndIndex + 1].step != null) nodeContents[selectionEndIndex + 1].step = nodeContents[selectionEndIndex + 1].step.Clone();
		}

		int count1 = selectionEndIndex + 1 - selectionStartIndex;
		int count2 = nodeContents.Count - selectionStartIndex;
		int count = Mathf.Min (count1, count2);
		nodeContents.RemoveRange (selectionStartIndex, count);
		if (scoreEndMarkIndex >= 0) {
			if (selectionStartIndex <= scoreEndMarkIndex) {
				scoreEndMarkIndex -= count;
			}
		}

		if (selectionStartIndex < nodeContents.Count) {
			if (selectionStartIndex > 0) {
				// bpm/stepの設定が変わらなければ元に戻す.
				var near = nodeContents [GetNearBpmChangedIndex (selectionStartIndex - 1)];
				if (nodeContents [selectionStartIndex].bpm == near.bpm) {
					nodeContents[selectionStartIndex].bpm = null;
				}
				near = nodeContents [GetNearStepChangedIndex (selectionStartIndex - 1)];
				if (nodeContents [selectionStartIndex].step.step == near.step.step && nodeContents [selectionStartIndex].step.is_interval == near.step.is_interval) {
					nodeContents[selectionStartIndex].step = null;
				}
			}
		}
		else {
			multiSelectedIndex1 = nodeContents.Count - 1;
			multiSelectedIndex2 = multiSelectedIndex1;
		}

		multiSelectedIndex1 = selectionStartIndex;
		multiSelectedIndex2 = multiSelectedIndex1;

		contents = nodeContents.ToArray ();
		listCtrl.ResizeScrollView ();
		forceUpdateContents = true;
		OnCellTapped (selectedIndex, contents[selectedIndex]);
	}

	public void Erase () {
		var undoCommands = new List<ScoreEditorTimeListInformation> ();
		for (int i = selectionStartIndex; i <= selectionEndIndex && i < nodeContents.Count; i++) {
			undoCommands.Add (nodeContents[i].Clone());
		}
		undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectionStartIndex, undoCommands.ToArray()));
		redoList.Clear ();

		for (int i = selectionStartIndex; i <= selectionEndIndex && i < contents.Length; i++) {
			var newContent = new ScoreEditorTimeListInformation();
			if (i == 0) {
				newContent.bpm = contents[0].bpm;
				newContent.step = contents[0].step;
			}
			nodeContents[i] = newContent;
		}
		contents = nodeContents.ToArray ();
		forceUpdateContents = true;
		OnCellTapped (selectedIndex, contents[selectedIndex]);
	}
	
	public void Turn () {
		AddTurnRefMirror (1, false);
	}
	
	public void Mirror () {
		AddTurnRefMirror (0, true);
	}

	public void AddTurnRefMirror (int turn, bool mirror) {
		var undoCommands = new List<ScoreEditorTimeListInformation> ();
		for (int i = selectionStartIndex; i <= selectionEndIndex && i < nodeContents.Count; i++) {
			undoCommands.Add (nodeContents[i].Clone());
		}
		undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectionStartIndex, undoCommands.ToArray()));
		redoList.Clear ();

		for (int i = selectionStartIndex; i <= selectionEndIndex && i < nodeContents.Count; i++) {
			nodeContents[i].AddTurnRefMirror (reader, turn, mirror);
		}

		contents = nodeContents.ToArray ();
		forceUpdateContents = true;
		OnCellTapped (selectedIndex, contents[selectedIndex]);
	}


	public void Undo () {
		if (undoList.Count <= 0) return;

		var undoCommand = undoList [undoList.Count - 1];
		if (undoCommand.type == ScoreEditorUndoRedoCommandProvider.CommandType.CHANGE) {
			var redoData = new List<ScoreEditorTimeListInformation> ();
			for (int i = 0; i < undoCommand.data.Length; i++) {
				redoData.Add (nodeContents[undoCommand.headIndex + i]);
				nodeContents[undoCommand.headIndex + i] = undoCommand.data[i].Clone();
			}
			redoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (undoCommand.afterSelectionStartIndex, undoCommand.afterSelectionEndIndex, undoCommand.afterTimelineScrollAmount, undoCommand.selectionStartIndex, undoCommand.selectionEndIndex, undoCommand.timelineScrollAmount, scoreEndMarkIndex, undoCommand.headIndex, redoData.ToArray()));
		}
		else if (undoCommand.type == ScoreEditorUndoRedoCommandProvider.CommandType.INSERT) {
			var insertData = new List<ScoreEditorTimeListInformation> ();
			for (int i = 0; i < undoCommand.data.Length; i++) {
				insertData.Add (undoCommand.data[i].Clone());
			}
			nodeContents.InsertRange (undoCommand.headIndex, insertData);
			redoList.Add (ScoreEditorUndoRedoCommandProvider.CreateRemoveHistory(undoCommand.afterSelectionStartIndex, undoCommand.afterSelectionEndIndex, undoCommand.afterTimelineScrollAmount, undoCommand.selectionStartIndex, undoCommand.selectionEndIndex, undoCommand.timelineScrollAmount, scoreEndMarkIndex, undoCommand.headIndex, undoCommand.data.Length));
		}
		else if (undoCommand.type == ScoreEditorUndoRedoCommandProvider.CommandType.REMOVE) {
			var redoData = new List<ScoreEditorTimeListInformation> ();
			for (int i = undoCommand.headIndex; i < undoCommand.headIndex + undoCommand.removeLength; i++) {
				redoData.Add (nodeContents[i].Clone());
			}
			nodeContents.RemoveRange (undoCommand.headIndex, undoCommand.removeLength);
			redoList.Add (ScoreEditorUndoRedoCommandProvider.CreateInsertHistory(undoCommand.afterSelectionStartIndex, undoCommand.afterSelectionEndIndex, undoCommand.afterTimelineScrollAmount, undoCommand.selectionStartIndex, undoCommand.selectionEndIndex, undoCommand.timelineScrollAmount, scoreEndMarkIndex, undoCommand.headIndex, redoData.ToArray()));
		}
		else if (undoCommand.type == ScoreEditorUndoRedoCommandProvider.CommandType.ONLY_ENDMARK_MOVE) {
			redoList.Add (ScoreEditorUndoRedoCommandProvider.CreateOnlyEndMarkMoveHistory(undoCommand.afterSelectionStartIndex, undoCommand.afterSelectionEndIndex, undoCommand.afterTimelineScrollAmount, undoCommand.selectionStartIndex, undoCommand.selectionEndIndex, undoCommand.timelineScrollAmount, scoreEndMarkIndex));
		}
		undoList.RemoveAt (undoList.Count - 1);
		contents = nodeContents.ToArray ();
		listCtrl.ResizeScrollView ();
		OnCellTapped (undoCommand.headIndex, nodeContents[undoCommand.headIndex]);
		multiSelectedIndex1 = undoCommand.selectionStartIndex;
		multiSelectedIndex2 = undoCommand.selectionEndIndex;
		_multiSelectableMode = multiSelectedIndex1 != multiSelectedIndex2;
		multiSelectableToogle.isOn = _multiSelectableMode;
		scoreEndMarkIndex = undoCommand.scoreEndMarkIndex;
		listCtrl.transform.RectCast ().anchoredPosition = new Vector2 (undoCommand.timelineScrollAmount, 0);
		forceUpdateContents = true;
	}

	public void Redo () {
		if (redoList.Count <= 0) return;

		var redoCommand = redoList [redoList.Count - 1];
		if (redoCommand.type == ScoreEditorUndoRedoCommandProvider.CommandType.CHANGE) {
			var undoData = new List<ScoreEditorTimeListInformation> ();
			for (int i = 0; i < redoCommand.data.Length; i++) {
				undoData.Add (nodeContents[redoCommand.headIndex + i]);
				nodeContents[redoCommand.headIndex + i] = redoCommand.data[i].Clone();
			}
			undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (redoCommand.afterSelectionStartIndex, redoCommand.afterSelectionEndIndex, redoCommand.afterTimelineScrollAmount, redoCommand.selectionStartIndex, redoCommand.selectionEndIndex, redoCommand.timelineScrollAmount, scoreEndMarkIndex, redoCommand.headIndex, undoData.ToArray()));
		}
		else if (redoCommand.type == ScoreEditorUndoRedoCommandProvider.CommandType.INSERT) {
			var insertData = new List<ScoreEditorTimeListInformation> ();
			for (int i = 0; i < redoCommand.data.Length; i++) {
				insertData.Add (redoCommand.data[i].Clone());
			}
			nodeContents.InsertRange (redoCommand.headIndex, insertData);
			undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateRemoveHistory(redoCommand.afterSelectionStartIndex, redoCommand.afterSelectionEndIndex, redoCommand.afterTimelineScrollAmount, redoCommand.selectionStartIndex, redoCommand.selectionEndIndex, redoCommand.timelineScrollAmount, scoreEndMarkIndex, redoCommand.headIndex, redoCommand.data.Length));
		}
		else if (redoCommand.type == ScoreEditorUndoRedoCommandProvider.CommandType.REMOVE) {
			var undoData = new List<ScoreEditorTimeListInformation> ();
			for (int i = redoCommand.headIndex; i < redoCommand.headIndex + redoCommand.removeLength; i++) {
				undoData.Add (nodeContents[i].Clone());
			}
			nodeContents.RemoveRange (redoCommand.headIndex, redoCommand.removeLength);
			undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateInsertHistory(redoCommand.afterSelectionStartIndex, redoCommand.afterSelectionEndIndex, redoCommand.afterTimelineScrollAmount, redoCommand.selectionStartIndex, redoCommand.selectionEndIndex, redoCommand.timelineScrollAmount, scoreEndMarkIndex, redoCommand.headIndex, undoData.ToArray()));
		}
		else if (redoCommand.type == ScoreEditorUndoRedoCommandProvider.CommandType.ONLY_ENDMARK_MOVE) {
			undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateOnlyEndMarkMoveHistory(redoCommand.afterSelectionStartIndex, redoCommand.afterSelectionEndIndex, redoCommand.afterTimelineScrollAmount, redoCommand.selectionStartIndex, redoCommand.selectionEndIndex, redoCommand.timelineScrollAmount, scoreEndMarkIndex));
		}
		redoList.RemoveAt (redoList.Count - 1);
		contents = nodeContents.ToArray ();
		listCtrl.ResizeScrollView ();
		OnCellTapped (redoCommand.headIndex, nodeContents[redoCommand.headIndex]);
		multiSelectedIndex1 = redoCommand.selectionStartIndex;
		multiSelectedIndex2 = redoCommand.selectionEndIndex;
		_multiSelectableMode = multiSelectedIndex1 != multiSelectedIndex2;
		multiSelectableToogle.isOn = _multiSelectableMode;
		scoreEndMarkIndex = redoCommand.scoreEndMarkIndex;
		listCtrl.transform.RectCast ().anchoredPosition = new Vector2 (redoCommand.timelineScrollAmount, 0);
		forceUpdateContents = true;
	}

	private int GetUndoSelectionStartIndex () {
		if (!UNDO_BACK_INDEX_HORMING)
			return selectionStartIndex;

		if (undoList.Count == 0)
			return firstSelectionStartIndex;
		return undoList [undoList.Count - 1].afterSelectionStartIndex;
	}
	
	private int GetUndoSelectionEndIndex () {
		if (!UNDO_BACK_INDEX_HORMING)
			return selectionEndIndex;

		if (undoList.Count == 0)
			return firstSelectionEndIndex;
		return undoList [undoList.Count - 1].afterSelectionEndIndex;
	}
	
	private float GetUndoTimelineScrollAmount () {
		if (!UNDO_BACK_INDEX_HORMING)
			return timelineScrollAmount;
		if (undoList.Count == 0)
			return firstTimelineScrollAmount;
		return undoList [undoList.Count - 1].afterTimelineScrollAmount;
	}

	public void Play () {
		if (!EditorInformationController.instance.calledSetup) return;

		CancelInvoke ("AutoSave");
		StartCoroutine (PlayCotoutine ());
	}

	private IEnumerator PlayCotoutine () {
		EditorInformationController.instance.timelineScrolledAmount = timelineScrollAmount;
		EditorInformationController.instance.score = nodeContents;
		yield return StartCoroutine (TrackInformationController.instance.SetupForEditor (
			EditorInformationController.instance.trackId,
			EditorInformationController.instance.scoreId,
			"maisq",
			reader.ToMaisqScript (nodeContents.ToArray(), scoreEndMarkIndex)
			));
		MiscInformationController.instance.rhythmGameRequesterSceneName = "editor";
		AutoSave ();
		Application.LoadLevel ("SceneRhythmGame");
	}

	public void Exit () {
		if (!EditorInformationController.instance.calledSetup) return;

		CancelInvoke ("AutoSave");
		EditorInformationController.instance.scoreScript = reader.ToSimaiScript (nodeContents.ToArray (), scoreEndMarkIndex);
		EditorInformationController.instance.scriptType = "simai";
		EditorInformationController.instance.selectionStartIndex = 0;
		EditorInformationController.instance.selectionEndIndex = 0;
		EditorInformationController.instance.timelineScrolledAmount = 0;
		EditorInformationController.instance.scoreEndMarkIndex = -1;
		EditorInformationController.instance.Save ();
		if (UserDataController.instance.user.score_editor_processing) {
			UserDataController.instance.user.score_editor_processing = false;
			UserDataController.instance.SaveUserInfo ();
		}
		// SceneImportTrackに、エディタに来る前の状態を再現して戻りたい.
		MiscInformationController.instance.importTrackRequesterSceneName = "editor";
		Application.LoadLevel ("SceneImportTrack");
	}

	public void AutoSave () {
		EditorInformationController.instance.scoreScript = reader.ToMaisqScript(nodeContents.ToArray(), nodeContents.Count - 1);
		EditorInformationController.instance.scriptType = "maisq";
		EditorInformationController.instance.selectionStartIndex = selectionStartIndex;
		EditorInformationController.instance.selectionEndIndex = selectionEndIndex;
		EditorInformationController.instance.timelineScrolledAmount = timelineScrollAmount;
		EditorInformationController.instance.scoreEndMarkIndex = scoreEndMarkIndex;
		EditorInformationController.instance.Save ();
	}

	public void SetScoreEndMark () {
		undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateOnlyEndMarkMoveHistory (GetUndoSelectionStartIndex(), GetUndoSelectionEndIndex(), GetUndoTimelineScrollAmount(), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex));
		redoList.Clear ();
		if (selectedIndex != scoreEndMarkIndex) {
			scoreEndMarkIndex = selectedIndex;
		}
		else {
			scoreEndMarkIndex = -1;
		}
		forceUpdateContents = true;
	}

	/// <summary>
	/// UnityのApplicationポーズメッセージ.
	/// </summary>
	void OnApplicationPause(bool pause) {
		if (!initialized) return;

		// 保存と、オートセーブの中断および再開.
		if (pause) {
			CancelInvoke ("AutoSave");
			AutoSave ();
		}
		else {
			int autoSaveInterval = UserDataController.instance.user.score_editor_auto_save_interval_minutes;
			if (autoSaveInterval > 0) {
				InvokeRepeating ("AutoSave", autoSaveInterval, autoSaveInterval);
			}
		}
	}

	public void OnAutoSaveIntervalInputTextChanged (string text) {
		if (!initialized) return;

		int autoSaveInterval;
		if (int.TryParse (text, out autoSaveInterval)) {
			if (UserDataController.instance.user.score_editor_auto_save_interval_minutes != autoSaveInterval) {
				UserDataController.instance.user.score_editor_auto_save_interval_minutes = autoSaveInterval;
				UserDataController.instance.SaveUserInfo ();
				CancelInvoke ("AutoSave");
				if (autoSaveInterval > 0) {
					InvokeRepeating ("AutoSave", autoSaveInterval, autoSaveInterval);
				}
			}
		}
	}

	public void MenuButtonClickPlaySE () {
		Utility.SoundEffectManager.Play ("se_decide");
	}

	// ノートスクリプトを展開.
	private string DeployNoteScript (ScoreEditorTimeListInformation info, int button) {
		string ret = string.Empty;
		foreach (var command in info.noteCommands[button]) {
			if (ret.Length > 0) {
				ret += ",";
			}
			ret += command.Deploy ();
		}
		return ret;
	}

	// 新規ノートスクリプトを適用したタイムラインデータを返す.
	private ScoreEditorTimeListInformation PatchedNoteScript (ScoreEditorTimeListInformation info, string script) {
		ScoreEditorTimeListInformation ret = new ScoreEditorTimeListInformation ();
		if (info.bpm.HasValue) ret.bpm = info.bpm.Value;
		if (info.step != null) ret.step = info.step.Clone ();
		var tree = MaisqScriptTree.CreateTreeFromMaiScript (null, script);
		reader.ExtractMaisqScriptTree (ret, tree);
		return ret;
	}

	// == ☆無しスライドダイアログ関連. ==
	public void ShowNoHeadSlideListDialog () {
		openedDialog = true;
		noHeadSlideListDialog.Show (nodeContents[selectedIndex].starNothingSlideStarNoteCommands);
	}

	public void OnNoHeadSlideListDialogClosing () {
		openedDialog = false;
	}

	public void OnDeleteNoHeadSlideElement () {
		undoList.Add (ScoreEditorUndoRedoCommandProvider.CreateChangeHistory (GetUndoSelectionStartIndex (), GetUndoSelectionEndIndex (), GetUndoTimelineScrollAmount (), selectionStartIndex, selectionEndIndex, timelineScrollAmount, scoreEndMarkIndex, selectedIndex, new ScoreEditorTimeListInformation[] { currentContent.Clone () }));
		redoList.Clear ();
		forceUpdateContents = true;
		UpdateNoteObjectsVision ();
		UpdateSlideMarkersVision ();
	}
	
	public void NoHeadSlidePutStartStateStart () {
		openedDialog = false;
		state = ScoreEditorInputState.NON_HEAD_START_POS;
		slidePatternMenu.SetActive (false);
		timelineObj.SetActive (false);
		noteTypeMenu.SetActive (false);
	}

	// == ダイレクトスクリプト入力関連 ==
	public void ShowDirectScriptInputDialog () {
		openedDialog = true;
		directScriptInputDialog.Show (reader.ToMaisqScriptNoteCommand (nodeContents [selectedIndex]));
	}

	// == スライドのチェイン切り替え ==
	public void ChangeCurrentChain () {
		if (slideCommandInputingTree != null) {
			CreateSlideHistory ();
			slideCommandInputingTree.parent = slideChainCurrentTree;
			slideChainCurrentTree.children.Add (slideCommandInputingTree);
			slideCommandInputingTree = null;
			slideFreeCurveCenterDecided = false;
			slideStartSensorNumber = slideHeadSensorNumber;
			slideStartSensorCircum = slideHeadSensorCircum;
			slidePatternContinueButton.gameObject.SetActive (false);
			slidePatternNextPatternButton.gameObject.SetActive (false);
			slidePatternNextChainButton.gameObject.SetActive (false);
			
			SlidePatternPulldownAllValid ();
			slideLastInputTypeIsFree = false;

			slideChainCurrentTree = slidePatternCurrentTree.AddChild (MaisqScore.CommandNameDocument.CHAIN);
			slideCurrentTreePatternCurrentIndex++;
		}
	}


	// == スライドのパターン切り替え ==
	public void ChangeCurrentPattern () {
		if (slideCommandInputingTree != null) {
			CreateSlideHistory ();
			slideCommandInputingTree.parent = slideChainCurrentTree;
			slideChainCurrentTree.children.Add (slideCommandInputingTree);
			slideCommandInputingTree = null;
			slideFreeCurveCenterDecided = false;
			slideStartSensorNumber = slideHeadSensorNumber;
			slideStartSensorCircum = slideHeadSensorCircum;
			slidePatternContinueButton.gameObject.SetActive (false);
			slidePatternNextPatternButton.gameObject.SetActive (false);
			slidePatternNextChainButton.gameObject.SetActive (false);
			
			SlidePatternPulldownAllValid ();
			slideLastInputTypeIsFree = false;

			slidePatternCurrentTree = slideCurrentTree.AddChild (MaisqScore.CommandNameDocument.PATTERN);
			slideCurrentTreeCurrentIndex++;
			slideChainCurrentTree = slidePatternCurrentTree.AddChild (MaisqScore.CommandNameDocument.CHAIN);
			slideCurrentTreePatternCurrentIndex = 0;
		}
	}
	
	// == スライドの入力履歴を作成.
	private void CreateSlideHistory () {
		if (slideUndoList == null) {
			slideUndoList = new List<SlideUndoRedoCommandProvider>();
		}
		if (slideRedoList == null) {
			slideRedoList = new List<SlideUndoRedoCommandProvider>();
		}
		if (slideCurrentTree != null) {
			slideUndoList.Add (new SlideUndoRedoCommandProvider(slideStartSensorNumber, slideStartSensorCircum, slideLastInputTypeIsFree, slideCurrentTree));
			slideRedoList.Clear ();
		}
	}
	
	// == スライドの元に戻す.
	public void SlideUndo () {
		if (slideUndoList == null) {
			slideUndoList = new List<SlideUndoRedoCommandProvider>();
		}
		if (slideRedoList == null) {
			slideRedoList = new List<SlideUndoRedoCommandProvider>();
		}
		if (slideUndoList.Count > 0) {
			slideRedoList.Add (new SlideUndoRedoCommandProvider(slideStartSensorNumber, slideStartSensorCircum, slideLastInputTypeIsFree, slideCurrentTree));
			var data = slideUndoList[slideUndoList.Count - 1];
			slideStartSensorNumber = data.slideStartSensorNumber;
			slideStartSensorCircum = data.slideStartSensorCircum;
			slideLastInputTypeIsFree = data.slideLastInputTypeIsFree;
			slideCurrentTree = data.slideCurrentTree;
			slideUndoList.RemoveAt (slideUndoList.Count - 1);

			slideCurrentTreeCurrentIndex = slideCurrentTree.children.Count - 1;
			slidePatternCurrentTree = slideCurrentTree.children[slideCurrentTreeCurrentIndex];
			slideCurrentTreePatternCurrentIndex = slidePatternCurrentTree.children.Count - 1;
			slideChainCurrentTree = slidePatternCurrentTree.children[slideCurrentTreePatternCurrentIndex];

			slideCommandInputingTree = null;
			slideFreeCurveCenterDecided = false;
			slidePatternContinueButton.gameObject.SetActive (false);
			slidePatternNextPatternButton.gameObject.SetActive (false);
			slidePatternNextChainButton.gameObject.SetActive (false);
			if (slideUndoList.Count == 0) {
				slidePatternEndButton.gameObject.SetActive (false);
			}
			slideLastInputTypeIsFreeReservation = slideLastInputTypeIsFree;
			
			if (slideLastInputTypeIsFree) {
				SlidePatternPulldownInvalidElementsAfterFree ();
			}
			else {
				if (slideStartSensorCircum == Constants.Circumference.CENTER) {
					SlidePatternPulldownInvalidElementsAfterCenterSensor ();
				}
				else if (slideStartSensorCircum == Constants.Circumference.INNER) {
					SlidePatternPulldownInvalidElementsAfterInnerSensor ();
				}
				else {
					SlidePatternPulldownAllValid ();
				}
			}

			UpdateNoteObjectsVision ();
			UpdateSlideMarkersVision ();
		}
	}

	// == スライドのやり直し.
	public void SlideRedo () {
		if (slideUndoList == null) {
			slideUndoList = new List<SlideUndoRedoCommandProvider>();
		}
		if (slideRedoList == null) {
			slideRedoList = new List<SlideUndoRedoCommandProvider>();
		}
		if (slideRedoList.Count > 0) {
			slideUndoList.Add (new SlideUndoRedoCommandProvider(slideStartSensorNumber, slideStartSensorCircum, slideLastInputTypeIsFree, slideCurrentTree));
			var data = slideRedoList[slideRedoList.Count - 1];
			slideStartSensorNumber = data.slideStartSensorNumber;
			slideStartSensorCircum = data.slideStartSensorCircum;
			slideLastInputTypeIsFree = data.slideLastInputTypeIsFree;
			slideCurrentTree = data.slideCurrentTree;
			slideRedoList.RemoveAt (slideRedoList.Count - 1);
			
			slideCurrentTreeCurrentIndex = slideCurrentTree.children.Count - 1;
			slidePatternCurrentTree = slideCurrentTree.children[slideCurrentTreeCurrentIndex];
			slideCurrentTreePatternCurrentIndex = slidePatternCurrentTree.children.Count - 1;
			slideChainCurrentTree = slidePatternCurrentTree.children[slideCurrentTreePatternCurrentIndex];

			slideCommandInputingTree = null;
			slideFreeCurveCenterDecided = false;
			slidePatternContinueButton.gameObject.SetActive (false);
			slidePatternNextPatternButton.gameObject.SetActive (false);
			slidePatternNextChainButton.gameObject.SetActive (false);
			slidePatternEndButton.gameObject.SetActive (true);
			slideLastInputTypeIsFreeReservation = slideLastInputTypeIsFree;
			
			if (slideLastInputTypeIsFree) {
				SlidePatternPulldownInvalidElementsAfterFree ();
			}
			else {
				if (slideStartSensorCircum == Constants.Circumference.CENTER) {
					SlidePatternPulldownInvalidElementsAfterCenterSensor ();
				}
				else if (slideStartSensorCircum == Constants.Circumference.INNER) {
					SlidePatternPulldownInvalidElementsAfterInnerSensor ();
				}
				else {
					SlidePatternPulldownAllValid ();
				}
			}

			UpdateNoteObjectsVision ();
			UpdateSlideMarkersVision ();
		}
	}


}
