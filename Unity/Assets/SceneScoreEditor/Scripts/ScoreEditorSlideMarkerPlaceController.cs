﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ScoreEditorSlideMarkerPlaceController : MonoBehaviour {
	[SerializeField]
	private GameObject markerPrefab;
	[SerializeField]
	private GameObject chainMarkerPrefab;
	public MaimaiSlidePattern slidePatternInfo { get; set; }
	public bool isEach { get; set; }

	// Use this for initialization
	void Start () {
		// == スライドマーカー ==
		float markerSize = 1.0f;
		
		Vector2 drawloc;
		//画像の大きさ変更に伴う距離の変更のためのスケールサイズ取得.
		float scale = markerSize;
		//スライドマーカーを☆の移動に合わせて消す. Unity版ではSlideMarkerAutoDeleteAlongOnStarControllerクラスに任せた.
		float autoMarkerDeleteDistance = 0;
//		if (this.isAutoDeleteSlideMarker()) {
//			autoMarkerDeleteDistance = this.onStarMovedDistance;
//		}
		
		if (slidePatternInfo.chains.Length == 1) {
			var chain = slidePatternInfo.chains[0];

			Sprite sprite;
			
			if (isEach) {
				sprite = SpriteTank.Get("note_slide_each");
			}
			else {
				sprite = SpriteTank.Get("note_slide");
			}
			
			GameObject markerObj = null;
			
			//スライド先の方から描画.
			var distance = chain.GetTotalDistance() - (Constants.instance.SLIDE_MARKER_TO_SENSOR_MARGIN * scale) * 2; // 始点・終点分で*2.
			for (var i = chain.commands.Length - 1; i >= 0; i--) {
				var command = chain.commands[i];
				
				var onStarToTotalDistance = autoMarkerDeleteDistance - command.GetTotalDistanceOfUntilThisMoveStart(); //スライドマーカーを☆の移動に合わせて消す.
				if (onStarToTotalDistance < 0) onStarToTotalDistance = 0;
				
				while (distance >= command.GetTotalDistanceOfUntilThisMoveStart() + onStarToTotalDistance) {
					if (i == 0 && distance < command.GetTotalDistanceOfUntilThisMoveStart() + Constants.instance.SLIDE_MARKER_TO_SENSOR_MARGIN * scale) {
						//始点でマージン以下なら描くのやめる.
						break;
					}
					var pDistance = distance - command.GetTotalDistanceOfUntilThisMoveStart(); //パターンからの距離.
					if (command.GetCommandType() == MaimaiSlideCommandType.STRAIGHT) { 
						//直線.
						//2点間の角度を求めて距離を半径にして移動する.
						drawloc = CircleCalculator.PointToPointMovePoint(command.startPos, command.targetPos, pDistance);
						
						// 前ループのスライドマーカーを今回生成するマーカーの反対方向に向かせる (スライド先から描くから、反対方向).
						if (markerObj != null) {
							var oldloc = Constants.instance.ToLeftHandedCoordinateSystemPosition(markerObj.transform.localPosition);
							float deg = CircleCalculator.PointToDegree(oldloc, drawloc);
							markerObj.transform.localRotation = Quaternion.Euler(0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(deg + 180));
						}
						
						markerObj = Instantiate(markerPrefab) as GameObject;
						markerObj.SetParentEx (gameObject);
						markerObj.transform.localPosition = Constants.instance.ToLeftHandedCoordinateSystemPosition(drawloc);
						markerObj.transform.localRotation = Quaternion.Euler(0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(command.GetImageDegree()));
						Image renderer = markerObj.GetComponentInChildren<Image>();
						renderer.sprite = sprite;
					}
					else { 
						var cc = command as MaimaiSlideCurveCommand;
						//曲線.
						var pDeg = CircleCalculator.ArcDegreeFromArcDistance(Mathf.Max(cc.radius.x, cc.radius.y), pDistance);
						drawloc = CircleCalculator.PointOnEllipse(cc.centerPos, cc.radius.x, cc.radius.y, cc.startDeg + pDeg * cc.vec);
						
						// 前ループのスライドマーカーを今回生成するマーカーの反対方向に向かせる (スライド先から描くから、反対方向).
						if (markerObj != null) {
							var oldloc = Constants.instance.ToLeftHandedCoordinateSystemPosition(markerObj.transform.localPosition);
							float deg = CircleCalculator.PointToDegree(oldloc, drawloc);
							markerObj.transform.localRotation = Quaternion.Euler(0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree(deg + 180));
						}
						
						markerObj = Instantiate(markerPrefab) as GameObject;
						markerObj.SetParentEx (gameObject);
						markerObj.transform.localPosition = Constants.instance.ToLeftHandedCoordinateSystemPosition(drawloc);
						markerObj.transform.localRotation = Quaternion.Euler(0, 0, Constants.instance.ToLeftHandedCoordinateSystemDegree((cc.startDeg + 90.0f + pDeg * cc.vec + (cc.vec > 0 ? 0 : 180))));
						Image renderer = markerObj.GetComponentInChildren<Image>();
						renderer.sprite = sprite;
					}
					distance -= Constants.instance.SLIDE_MARKER_TO_MARKER_INTERVAL * scale;
				}
			}
		}
		else {
			// チェインが複数あるとき.

			int vertexAmount = 10; // 頂点数(分割数).
			var chainsPos = new Vector2[slidePatternInfo.chains.Length, vertexAmount];
			var chainsDistance = new float[slidePatternInfo.chains.Length, vertexAmount];
			
			GameObject markerObj = null;
			int chainCount = 0;
			
			foreach (var chain in slidePatternInfo.chains) {
				var distance = chain.GetTotalDistance() - (Constants.instance.SLIDE_MARKER_TO_SENSOR_MARGIN * scale) * 2; // 始点・終点分で*2.
				float progressAmount = distance / (float)vertexAmount;
				int vertexCount = 0;
				
				for (var i = chain.commands.Length - 1; i >= 0; i--) {
					var command = chain.commands[i];
					while (distance >= command.GetTotalDistanceOfUntilThisMoveStart()) {
						if (vertexCount >= vertexAmount) {
							break;
						}
						var pDistance = distance - command.GetTotalDistanceOfUntilThisMoveStart(); //パターンからの距離.
						if (command.GetCommandType() == MaimaiSlideCommandType.STRAIGHT) { 
							//直線.
							//2点間の角度を求めて距離を半径にして移動する.
							drawloc = CircleCalculator.PointToPointMovePoint(command.startPos, command.targetPos, pDistance);
							chainsPos[chainCount, vertexCount] = drawloc;
						}
						else {
							var cc = command as MaimaiSlideCurveCommand;
							//曲線.
							var pDeg = CircleCalculator.ArcDegreeFromArcDistance(Mathf.Max(cc.radius.x, cc.radius.y), pDistance);
							drawloc = CircleCalculator.PointOnEllipse(cc.centerPos, cc.radius.x, cc.radius.y, cc.startDeg + pDeg * cc.vec);
							chainsPos[chainCount, vertexCount] = drawloc;
						}
						chainsDistance[chainCount, vertexCount] = distance;
						distance -= progressAmount;
						vertexCount++;
					}
				}
				chainCount++;
			}
			
			int chainsLength = chainsPos.GetLength(0);
			int vertexsLength = chainsPos.GetLength(1);
			Mesh chainMesh = SlideMeshCreater.Create (chainsLength);
			Sprite sprite;
			if (isEach) {
				sprite = SpriteTank.Get("note_slide_chain_each");
			}
			else {
				sprite = SpriteTank.Get("note_slide_chain");
			}
			
			for (int j = 0; j < vertexsLength; j++) {
				markerObj = Instantiate(chainMarkerPrefab) as GameObject;
				markerObj.SetParentEx (gameObject);
				var filter = markerObj.GetComponentInChildren<MeshFilter>();
				filter.sharedMesh = chainMesh;
				float chainSlideMarkerSize = 16 * scale; //チェインスライド画像の縦の大きさが16.
				var vertices = filter.mesh.vertices;
				for (int i = 0; i < chainsLength; i++) {
					MaimaiSlideCommand comdata = null;
					// 描画しようとしているマーカーの位置を管理しているスライドコマンドを取得.
					foreach (var command in slidePatternInfo.chains[i].commands) {
						if (command.GetTotalDistanceOfUntilThisMoveStart() < chainsDistance[i, j]) {
							comdata = command;
						}
					}
					if (comdata == null) continue;
					float angle = 0;
					// スライドコマンドがストレートならスライドコマンドの始点を見て、それを角度とする.
					if (comdata.GetCommandType() == MaimaiSlideCommandType.STRAIGHT || j >= vertexsLength - 1) {
						angle = CircleCalculator.PointToDegree (chainsPos[i, j], comdata.startPos);
					}
					// スライドコマンドがカーブでインデックスオーバーしないなら始点がある方向で一番近いマーカーの位置を見て、それを角度とする.
					else {
						angle = CircleCalculator.PointToDegree (chainsPos[i, j], chainsPos[i, j + 1]);
					}
					// 上.
					var up = CircleCalculator.PointOnCircle(chainsPos[i, j], chainSlideMarkerSize / 2, angle).ChangePosHandSystem();
					vertices[i].x = up.x;
					vertices[i].y = up.y;
					// 下.
					var down = CircleCalculator.PointOnCircle(chainsPos[i, j], chainSlideMarkerSize / 2, angle + 180).ChangePosHandSystem();
					vertices[vertices.Length - 1 - i].x = down.x;
					vertices[vertices.Length - 1 - i].y = down.y;
				}
				filter.mesh.vertices = vertices;
				// その他の情報.
				var renderer = markerObj.GetComponentInChildren<ScoreEditorSlideMesh>();
				renderer.slideMesh = filter.mesh;
				renderer.slideTexture = sprite.texture;
			}
		}
	}

}
