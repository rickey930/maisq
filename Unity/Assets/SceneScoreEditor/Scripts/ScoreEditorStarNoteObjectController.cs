﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreEditorStarNoteObjectController : ScoreEditorNoteObjectController {
	[SerializeField]
	protected Image image2;

	public void Setup (Vector2 posRate) {
		translateHierarchy.anchoredPosition = new Vector3 (posRate.x * Constants.instance.MAIMAI_OUTER_RADIUS, posRate.y * Constants.instance.MAIMAI_OUTER_RADIUS);
		rotateHierarchy.localRotation = Quaternion.Euler (Constants.instance.ToLeftHandedCoordinateSystemRotation(new Vector3 (0, 0, CircleCalculator.PointToDegree(posRate).ChangeDegHandSystem())));
	}

	public void SetAsLastSibling () {
		transform.RectCast ().SetAsLastSibling ();
	}
	
	public void ChangeSprite (bool isEach, bool isBreak, bool isMultiPattern) {
		if (isBreak) {
			image.sprite = SpriteTank.Get ("note_star_break");
		}
		else if (!isEach) {
			image.sprite = SpriteTank.Get ("note_star");
		}
		else {
			image.sprite = SpriteTank.Get ("note_star_each");
		}
		if (isMultiPattern) {
			image2.sprite = image.sprite;
			image2.gameObject.SetActive (true);
		}
		else {
			image2.gameObject.SetActive (false);
		}
	}
}
