﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreEditorStepInputDialogController : MonoBehaviour {
	[SerializeField]
	private InputField textBox;
	[SerializeField]
	private Toggle useIntervalCheckBox;
	
	public System.Action<bool, float> onValueChanged { get; set; }
	public System.Action onClosing { get; set; }
	
	public void Show (bool isInterval, float initValue) {
		gameObject.SetActive (true);
		textBox.text = initValue.ToString();
		useIntervalCheckBox.isOn = isInterval;
	}
	
	public void Hide () {
		if (onClosing != null) {
			onClosing ();
		}
		gameObject.SetActive (false);
	}
	
	public void OnOKButtonClick () {
		if (onValueChanged != null) {
			float value;
			if (float.TryParse(textBox.text, out value)) {
				if (value > 0) {
					onValueChanged (useIntervalCheckBox.isOn, value);
				}
			}
		}
		Hide ();
	}
}
