﻿using UnityEngine;
using System.Collections;

public class ScoreEditorTimeListController : MonoBehaviour, IInfiniteScrollSetup {
	
	[SerializeField]
	public ScoreEditorSceneManager manager;
	
	#region IInfiniteScrollSetup
	public bool forceUpdateContents { 
		get { return manager.forceUpdateContents; } 
		set { manager.forceUpdateContents = value; }
	}
	public bool setupCompleted { get; set; }
	
	public IEnumerator OnPostSetupItems() {
		// リストの初期化.
		while (manager.contents == null)
			yield return null;
		ResizeScrollView ();
		setupCompleted = true;
	}
	
	public void ResizeScrollView () {
		var infiniteScroll = GetComponent<InfiniteScroll> ();
		var rectTransform = GetComponent<RectTransform> ();
		var delta = rectTransform.sizeDelta;
		delta.x = (infiniteScroll.ItemScale * (manager.contents.Length)) + (infiniteScroll.AnchordMargin * 2);
		rectTransform.sizeDelta = delta;
	}
	
	public void OnUpdateItem (int index, GameObject obj) {
		if (manager.contents == null || index < 0) {
			if (obj.activeSelf) {
				obj.SetActive (false);
			}
		}
		else {
			if (index >= manager.contents.Length) {
				while (index >= manager.contents.Length) {
					manager.CreateNewContent ();
				}
				ResizeScrollView ();
			}
			if (!obj.activeSelf) {
				obj.SetActive (true);
			}
			var item = obj.GetComponent<ScoreEditorTimeNodeController> ();
			item.UpdateItem (index, manager.contents [index]);
		}
	}
	#endregion
}
