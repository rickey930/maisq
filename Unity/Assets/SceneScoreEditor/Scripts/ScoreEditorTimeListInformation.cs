using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoreEditorTimeListInformation {
	public float? bpm;
	public RhythmGameLibrary.Score.ScoreStepData step;
	public List<MaisqScriptTree>[] noteCommands;
	public List<MaisqScriptTree> starNothingSlideStarNoteCommands;
	public List<MaisqScriptTree> otherCommands;
	public ScoreEditorMaimaiNoteType[] noteType; //noteCommands[i]の中身とnoteType[i]はプログラム中で一致させること.
	public ScoreEditorTimeListInformation () {
		const int size = 8;
		noteCommands = new List<MaisqScriptTree>[size]; //OuterSensorの数だけ用意.
		noteType = new ScoreEditorMaimaiNoteType[size];
		for (int i = 0; i < size; i++) {
			noteCommands[i] = new List<MaisqScriptTree>();
			noteType [i] = ScoreEditorMaimaiNoteType.NONE;
		}
		starNothingSlideStarNoteCommands = new List<MaisqScriptTree> ();
		otherCommands = new List<MaisqScriptTree> ();
	}

	public bool HasAnyNoteCommand () {
		for (int i = 0; i < noteCommands.Length; i++) {
			if (noteCommands[i].Count > 0) {
				return true;
			}
		}
		for (int i = 0; i < starNothingSlideStarNoteCommands.Count; i++) {
			if (starNothingSlideStarNoteCommands[i] != null) {
				return true;
			}
		}
		for (int i = 0; i < otherCommands.Count; i++) {
			if (otherCommands[i] != null) {
				return true;
			}
		}
		return false;
	}

	public enum ScoreEditorMaimaiNoteType {
		NONE, TAP, HOLD, SLIDE, BREAK, BREAKSTAR_SLIDE, EDITING_SLIDE, EDITING_BREAKSTAR, 
	}

	public enum ScoreEditorNoteIndicator {
		NONE, TAP, HOLD, SLIDE, BREAK, EACH_TAP, EACH_HOLD, EACH_SLIDE, STARBREAK_SLIDE, NOHEAD_SLIDE, EACH_NOHEAD_SLIDE, 
	}

	public ScoreEditorNoteIndicator GetIndicator () {
		int tapCount = 0;
		int holdCount = 0;
		int slideCount = 0;
		ScoreEditorNoteIndicator ret = ScoreEditorNoteIndicator.NONE;
		for (int i = 0; i < noteType.Length; i++) {
			if (noteType[i] == ScoreEditorMaimaiNoteType.BREAK) {
				return ScoreEditorNoteIndicator.BREAK;
			}
			if (noteType[i] == ScoreEditorMaimaiNoteType.BREAKSTAR_SLIDE) {
				ret = ScoreEditorNoteIndicator.STARBREAK_SLIDE;
			}
			if (noteType[i] != ScoreEditorMaimaiNoteType.NONE && ret != ScoreEditorNoteIndicator.STARBREAK_SLIDE) {
				switch (noteType[i]) {
				case ScoreEditorMaimaiNoteType.TAP:
					tapCount++;
					break;
				case ScoreEditorMaimaiNoteType.HOLD:
					holdCount++;
					break;
				case ScoreEditorMaimaiNoteType.SLIDE:
					slideCount++;
					break;
				}
			}
		}
		if (ret == ScoreEditorNoteIndicator.NONE && starNothingSlideStarNoteCommands.Count > 0) {
			if (starNothingSlideStarNoteCommands.Count > 1) {
				ret = ScoreEditorNoteIndicator.EACH_NOHEAD_SLIDE;
			}
			else {
				ret = ScoreEditorNoteIndicator.NOHEAD_SLIDE;
			}
		}
		else if (ret != ScoreEditorNoteIndicator.STARBREAK_SLIDE) {
			if (tapCount + holdCount + slideCount == 0) {
				return ScoreEditorNoteIndicator.NONE;
			}
			if (tapCount + holdCount + slideCount == 1) {
				if (tapCount > 0) 
					ret = ScoreEditorNoteIndicator.TAP;
				else if (holdCount > 0) 
					ret = ScoreEditorNoteIndicator.HOLD;
				else if (slideCount > 0)
					ret = ScoreEditorNoteIndicator.SLIDE;
			}
			else {
				if (holdCount > tapCount && holdCount > slideCount)
					ret = ScoreEditorNoteIndicator.EACH_HOLD;
				else if ((slideCount > tapCount && slideCount > holdCount) || tapCount == 0)
					ret = ScoreEditorNoteIndicator.EACH_SLIDE;
				else
					ret = ScoreEditorNoteIndicator.EACH_TAP;
			}
		}
		return ret;
	}

	public ScoreEditorTimeListInformation Clone () {
		return Clone (null);
	}

	// ノートの空き部分はsrcで埋める.
	public ScoreEditorTimeListInformation Clone (ScoreEditorTimeListInformation src) {
		var c = this;
		var data = new ScoreEditorTimeListInformation ();
		data.bpm = c.bpm;
		data.step = c.step != null ? c.step.Clone () : null;
		for (int j = 0; j < c.noteType.Length; j++) {
			if (src == null || c.noteType[j] != ScoreEditorMaimaiNoteType.NONE) {
				data.noteType[j] = c.noteType[j];
			}
			else {
				data.noteType[j] = src.noteType[j];
			}
		}
		for (int j = 0; j < c.noteCommands.Length; j++) {
			if (src == null || c.noteCommands[j].Count > 0) {
				data.noteCommands[j] = new List<MaisqScriptTree>();
				for (int k = 0; k < c.noteCommands[j].Count; k++) {
					data.noteCommands[j].Add (c.noteCommands[j][k].Clone());
				}
			}
			else if (src != null) {
				data.noteCommands[j] = new List<MaisqScriptTree>();
				for (int k = 0; k < src.noteCommands[j].Count; k++) {
					data.noteCommands[j].Add (src.noteCommands[j][k].Clone());
				}
			}
		}
		for (int j = 0; j < c.starNothingSlideStarNoteCommands.Count; j++) {
			if (c.starNothingSlideStarNoteCommands[j] != null) {
				data.starNothingSlideStarNoteCommands.Add (c.starNothingSlideStarNoteCommands[j].Clone ());
			}
		}
		for (int j = 0; j < c.otherCommands.Count; j++) {
			if (c.otherCommands[j] != null) {
				data.otherCommands.Add (c.otherCommands[j].Clone ());
			}
		}
		if (src != null) {
			for (int j = 0; j < src.starNothingSlideStarNoteCommands.Count; j++) {
				if (src.starNothingSlideStarNoteCommands [j] != null) {
					data.starNothingSlideStarNoteCommands.Add (src.starNothingSlideStarNoteCommands [j].Clone());
				}
			}
			for (int j = 0; j < src.otherCommands.Count; j++) {
				if (src.otherCommands[j] != null) {
					data.otherCommands.Add (src.otherCommands[j].Clone ());
				}
			}
		}
		return data;
	}

	public void AddTurnRefMirror (ScoreEditorScriptReader reader, int turn, bool mirror) {
		var work = Clone ();
		for (int i = 0; i < 8; i++) {
			int b = (i + turn) % 8;
			b = mirror ? 7 - b : b;
			this.noteType[b] = work.noteType[i];
			this.noteCommands[b] = work.noteCommands[i];
		}
		for (int i = 0; i < noteCommands.Length; i++) {
			for (int j = 0; j < this.noteCommands [i].Count; j++) {
				reader.MaisqScriptAddTurnRefMirror (this.noteCommands [i][j], turn, mirror);
			}
		}
		foreach (var tree in this.starNothingSlideStarNoteCommands) {
			if (tree != null) {
				reader.MaisqScriptAddTurnRefMirror (tree, turn, mirror);
			}
		}
	}


}

public class UndoRedoCommandProvider<T> {
	public enum CommandType {
		CHANGE, INSERT, REMOVE, ONLY_ENDMARK_MOVE, 
	}

	public int headIndex;
	public CommandType type;

	public T[] data;
	public int removeLength;

	public int selectionStartIndex;
	public int selectionEndIndex;
	public float timelineScrollAmount;
	public int afterSelectionStartIndex;
	public int afterSelectionEndIndex;
	public float afterTimelineScrollAmount;
	public int scoreEndMarkIndex;

	public static UndoRedoCommandProvider<T> CreateChangeHistory (int selectionStartIndex, int selectionEndIndex, float timelineScrollAmount, int afterSelectionEndIndex, int afterSelectionStartIndex, float afterTimelineScrollAmount, int scoreEndMarkIndex, int headIndex, T[] changeData) {
		var ret = new UndoRedoCommandProvider<T> ();
		ret.selectionStartIndex = selectionStartIndex;
		ret.selectionEndIndex = selectionEndIndex;
		ret.timelineScrollAmount = timelineScrollAmount;
		ret.afterSelectionStartIndex = afterSelectionStartIndex;
		ret.afterSelectionEndIndex = afterSelectionEndIndex;
		ret.afterTimelineScrollAmount = afterTimelineScrollAmount;
		ret.scoreEndMarkIndex = scoreEndMarkIndex;
		ret.headIndex = headIndex;
		ret.data = changeData;
		ret.type = CommandType.CHANGE;
		return ret;
	}
	
	public static UndoRedoCommandProvider<T> CreateInsertHistory (int selectionStartIndex, int selectionEndIndex, float timelineScrollAmount, int afterSelectionEndIndex, int afterSelectionStartIndex, float afterTimelineScrollAmount, int scoreEndMarkIndex, int headIndex, T[] insertData) {
		var ret = new UndoRedoCommandProvider<T> ();
		ret.selectionStartIndex = selectionStartIndex;
		ret.selectionEndIndex = selectionEndIndex;
		ret.timelineScrollAmount = timelineScrollAmount;
		ret.afterSelectionStartIndex = afterSelectionStartIndex;
		ret.afterSelectionEndIndex = afterSelectionEndIndex;
		ret.afterTimelineScrollAmount = afterTimelineScrollAmount;
		ret.scoreEndMarkIndex = scoreEndMarkIndex;
		ret.headIndex = headIndex;
		ret.data = insertData;
		ret.type = CommandType.INSERT;
		return ret;
	}
	
	public static UndoRedoCommandProvider<T> CreateRemoveHistory (int selectionStartIndex, int selectionEndIndex, float timelineScrollAmount, int afterSelectionEndIndex, int afterSelectionStartIndex, float afterTimelineScrollAmount, int scoreEndMarkIndex, int headIndex, int removeLength) {
		var ret = new UndoRedoCommandProvider<T> ();
		ret.selectionStartIndex = selectionStartIndex;
		ret.selectionEndIndex = selectionEndIndex;
		ret.timelineScrollAmount = timelineScrollAmount;
		ret.afterSelectionStartIndex = afterSelectionStartIndex;
		ret.afterSelectionEndIndex = afterSelectionEndIndex;
		ret.afterTimelineScrollAmount = afterTimelineScrollAmount;
		ret.scoreEndMarkIndex = scoreEndMarkIndex;
		ret.headIndex = headIndex;
		ret.removeLength = removeLength;
		ret.type = CommandType.REMOVE;
		return ret;
	}

	public static UndoRedoCommandProvider<T> CreateOnlyEndMarkMoveHistory (int selectionStartIndex, int selectionEndIndex, float timelineScrollAmount, int afterSelectionEndIndex, int afterSelectionStartIndex, float afterTimelineScrollAmount, int scoreEndMarkIndex) {
		var ret = new UndoRedoCommandProvider<T> ();
		ret.selectionStartIndex = selectionStartIndex;
		ret.selectionEndIndex = selectionEndIndex;
		ret.timelineScrollAmount = timelineScrollAmount;
		ret.afterSelectionStartIndex = afterSelectionStartIndex;
		ret.afterSelectionEndIndex = afterSelectionEndIndex;
		ret.afterTimelineScrollAmount = afterTimelineScrollAmount;
		ret.scoreEndMarkIndex = scoreEndMarkIndex;
		ret.type = CommandType.ONLY_ENDMARK_MOVE;
		return ret;
	}
}

public class ScoreEditorUndoRedoCommandProvider : UndoRedoCommandProvider<ScoreEditorTimeListInformation> {

}

public class SlideUndoRedoCommandProvider {
	public int slideStartSensorNumber { get; set; }
	public Constants.Circumference slideStartSensorCircum { get; set; }
	public bool slideLastInputTypeIsFree { get; set; }
	public MaisqScriptTree slideCurrentTree { get; set; }

	public SlideUndoRedoCommandProvider (int slideStartSensorNumber,
	                                      Constants.Circumference slideStartSensorCircum,
	                                      bool slideLastInputTypeIsFree,
	                                      MaisqScriptTree slideCurrentTree) {
		this.slideStartSensorNumber = slideStartSensorNumber;
		this.slideStartSensorCircum = slideStartSensorCircum;
		this.slideLastInputTypeIsFree = slideLastInputTypeIsFree;
		this.slideCurrentTree = slideCurrentTree.Clone ();
	}
}

public class ScoreEditorScriptReader : MaisqScriptReader {

	public ScoreEditorScriptReader() : base() {
		editorScores = new List<ScoreEditorTimeListInformation> ();
	}

	List<ScoreEditorTimeListInformation> editorScores;

	public ScoreEditorTimeListInformation[] GetEditorScore () {
		return editorScores.ToArray ();
	}

	protected override void ReadCreateCommandTypeOfGlobal (MaisqScriptTree tree) {
		var switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.NOTE: {
			SetEditorData (tree);
			break;
		}
		case MaisqScore.CommandNameDocument.REST: {
			if (tree.children.Count == 0) {
				SetEditorData (null);
			}
			else if (tree.children.Count == 1) {
				int value;
				if (int.TryParse(tree.children[0].data, out value)) {
					if (value > 0) {
						for (int i = 0; i < value; i++) {
							SetEditorData (null);
						}
					}
				}
			}
			break;
		}
		default: {
			base.ReadCreateCommandTypeOfGlobal(tree);
			break;
		}
		}
	}

	public void MaisqScriptAddTurnRefMirror (MaisqScriptTree tree, int turn, bool mirror) {
		var switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.TAP:
		case MaisqScore.CommandNameDocument.HOLD:
		case MaisqScore.CommandNameDocument.BREAK: {
			if (tree.children.Count >= 1) {
				var button = ReadCreateCommandTypeOfButton (tree.children[0]);
				if (button != null) {
					if (turn > 0) button.Turn (turn);
					if (mirror) button.MirrorHolizontal ();
					tree.children[0].data = button.GetValue().ToString();
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.SLIDE: {
			if (tree.children.Count >= 1) {
				var head = ReadCreateCommandTypeOfSlideHead (tree.children[0]);
				if (turn > 0 && head != null) head.Turn (turn);
				if (mirror && head != null) head.MirrorHolizontal ();
				if (head is MaimaiScore.SlideHeadStar) {
					tree.children[0].children[0].data = (head as MaimaiScore.SlideHeadStar).button.GetValue().ToString();
				}
				else if (head is MaimaiScore.SlideHeadBreakStar) {
					tree.children[0].children[0].data = (head as MaimaiScore.SlideHeadBreakStar).button.GetValue().ToString();
				}
				for (int i = 1; i < tree.children.Count; i++) {
					switch (tree.children[i].data) {
					case MaisqScore.CommandNameDocument.PATTERN:
						for (int j = 0; j < tree.children[i].children.Count; j++) {
							switch (tree.children[i].children[j].data) {
							case MaisqScore.CommandNameDocument.CHAIN:
								MaimaiScore.ISlideCommand beforeCommand = null;
								for (int k = 0; k < tree.children[i].children[j].children.Count; k++) {
									var slideCommandTree = tree.children[i].children[j].children[k];
									MaisqScriptSlidePatternAddTurnRefMirror(slideCommandTree, turn, mirror, head, beforeCommand);
									var commands = ReadCreateCommandTypeOfSlideCommand (slideCommandTree, head, beforeCommand);
									beforeCommand = commands[commands.Length - 1];
								}
								break;
							}
						}
						break;
					}
				}
			}
			break;
		}
		}
	}
	
	private void MaisqScriptSlidePatternAddTurnRefMirror (MaisqScriptTree tree, int turn, bool mirror, MaimaiScore.ISlideHead head, MaimaiScore.ISlideCommand beforeCommand) {
		var switchKey = tree.data;
		switch (switchKey) {
		case MaisqScore.CommandNameDocument.OUTER_STRAIGHT_OUTER:
		case MaisqScore.CommandNameDocument.OUTER_STRAIGHT_INNER:
		case MaisqScore.CommandNameDocument.OUTER_CURVE_CLOCKWISE_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.OUTER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.OUTER_CURVE_RIGHT_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.OUTER_CURVE_LEFT_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.INNER_STRAIGHT_OUTER:
		case MaisqScore.CommandNameDocument.INNER_STRAIGHT_INNER:
		case MaisqScore.CommandNameDocument.INNER_CURVE_CLOCKWISE_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.INNER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.INNER_CURVE_RIGHT_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.INNER_CURVE_LEFT_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.SHAPE_P_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.SHAPE_Q_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.SHAPE_V_AXIS_CENTER:
		case MaisqScore.CommandNameDocument.SHAPE_S:
		case MaisqScore.CommandNameDocument.SHAPE_Z:
		{
			if (tree.children.Count == 2) {
				var button1 = ReadCreateCommandTypeOfButton (tree.children[0]);
				var button2 = ReadCreateCommandTypeOfButton (tree.children[1]);
				if (button1 == null || button2 == null) {
					return;
				}
				var start = button1.Clone();
				var target = button2.Clone();
				if (turn > 0) {
					start.Turn(turn);
					target.Turn (turn);
				}
				if (mirror) {
					start.MirrorHolizontal();
					target.MirrorHolizontal ();
				}
				tree.children[0].data = start.GetValue().ToString();
				tree.children[1].data = target.GetValue().ToString();
				int b = button1.GetValue();
				int s = start.GetValue();
				if (mirror) {
					if (switchKey == MaisqScore.CommandNameDocument.SHAPE_P_AXIS_CENTER) {
						tree.data = MaisqScore.CommandNameDocument.SHAPE_Q_AXIS_CENTER;
					}
					else if (switchKey == MaisqScore.CommandNameDocument.SHAPE_Q_AXIS_CENTER) {
						tree.data = MaisqScore.CommandNameDocument.SHAPE_P_AXIS_CENTER;
					}
					else if (switchKey == MaisqScore.CommandNameDocument.SHAPE_S) {
						tree.data = MaisqScore.CommandNameDocument.SHAPE_Z;
					}
					else if (switchKey == MaisqScore.CommandNameDocument.SHAPE_Z) {
						tree.data = MaisqScore.CommandNameDocument.SHAPE_S;
					}
					else if (switchKey == MaisqScore.CommandNameDocument.OUTER_CURVE_CLOCKWISE_AXIS_CENTER) {
						tree.data = MaisqScore.CommandNameDocument.OUTER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER;
					}
					else if (switchKey == MaisqScore.CommandNameDocument.OUTER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER) {
						tree.data = MaisqScore.CommandNameDocument.OUTER_CURVE_CLOCKWISE_AXIS_CENTER;
					}
					else if (switchKey == MaisqScore.CommandNameDocument.OUTER_CURVE_RIGHT_AXIS_CENTER) {
						int vec = 0;
						if (b == 7 || b == 8 || b == 1 || b == 2) {
							vec = 1; // 時計回り.
						}
						else if (b == 6 || b == 5 || b == 4 || b == 3) {
							vec = -1; // 反時計回り.
						}
						if ((vec > 0 && (s == 7 || s == 8 || s == 1 || s == 2)) ||
						    (vec < 0 && (s == 6 || s == 5 || s == 4 || s == 3)) ){
							tree.data = MaisqScore.CommandNameDocument.OUTER_CURVE_LEFT_AXIS_CENTER;
						}
					}
					else if (switchKey == MaisqScore.CommandNameDocument.OUTER_CURVE_LEFT_AXIS_CENTER) {
						int vec = 0;
						if (b == 7 || b == 8 || b == 1 || b == 2) {
							vec = 1; // 時計回り.
						}
						else if (b == 6 || b == 5 || b == 4 || b == 3) {
							vec = -1; // 反時計回り.
						}
						if ((vec > 0 && (s == 7 || s == 8 || s == 1 || s == 2)) || 
						    (vec < 0 && (s == 6 || s == 5 || s == 4 || s == 3)) ){
							tree.data = MaisqScore.CommandNameDocument.OUTER_CURVE_RIGHT_AXIS_CENTER;
						}
					}
					else if (switchKey == MaisqScore.CommandNameDocument.INNER_CURVE_CLOCKWISE_AXIS_CENTER) {
						tree.data = MaisqScore.CommandNameDocument.INNER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER;
					}
					else if (switchKey == MaisqScore.CommandNameDocument.INNER_CURVE_COUNTERCLOCKWISE_AXIS_CENTER) {
						tree.data = MaisqScore.CommandNameDocument.INNER_CURVE_CLOCKWISE_AXIS_CENTER;
					}
					else if (switchKey == MaisqScore.CommandNameDocument.INNER_CURVE_RIGHT_AXIS_CENTER) {
						int vec = 0;
						if (b == 7 || b == 8 || b == 1 || b == 2) {
							vec = 1; // 時計回り.
						}
						else if (b == 6 || b == 5 || b == 4 || b == 3) {
							vec = -1; // 反時計回り.
						}
						if ((vec > 0 && (s == 7 || s == 8 || s == 1 || s == 2)) ||
						    (vec < 0 && (s == 6 || s == 5 || s == 4 || s == 3)) ){
							tree.data = MaisqScore.CommandNameDocument.INNER_CURVE_LEFT_AXIS_CENTER;
						}
					}
					else if (switchKey == MaisqScore.CommandNameDocument.INNER_CURVE_LEFT_AXIS_CENTER) {
						int vec = 0;
						if (b == 7 || b == 8 || b == 1 || b == 2) {
							vec = 1; // 時計回り.
						}
						else if (b == 6 || b == 5 || b == 4 || b == 3) {
							vec = -1; // 反時計回り.
						}
						if ((vec > 0 && (s == 7 || s == 8 || s == 1 || s == 2)) ||
						    (vec < 0 && (s == 6 || s == 5 || s == 4 || s == 3)) ){
							tree.data = MaisqScore.CommandNameDocument.INNER_CURVE_RIGHT_AXIS_CENTER;
						}
					}
				}
				else {
					if (switchKey == MaisqScore.CommandNameDocument.OUTER_CURVE_RIGHT_AXIS_CENTER) {
						int vec = 0;
						if (b == 7 || b == 8 || b == 1 || b == 2) {
							vec = 1; // 時計回り.
						}
						else if (b == 6 || b == 5 || b == 4 || b == 3) {
							vec = -1; // 反時計回り.
						}
						if ((vec > 0 && (s == 6 || s == 5 || s == 4 || s == 3)) ||
						    (vec < 0 && (s == 7 || s == 8 || s == 1 || s == 2)) ){
							tree.data = MaisqScore.CommandNameDocument.OUTER_CURVE_LEFT_AXIS_CENTER;
						}
					}
					else if (switchKey == MaisqScore.CommandNameDocument.OUTER_CURVE_LEFT_AXIS_CENTER) {
						int vec = 0;
						if (b == 7 || b == 8 || b == 1 || b == 2) {
							vec = 1; // 時計回り.
						}
						else if (b == 6 || b == 5 || b == 4 || b == 3) {
							vec = -1; // 反時計回り.
						}
						if ((vec > 0 && (s == 6 || s == 5 || s == 4 || s == 3)) ||
						    (vec < 0 && (s == 7 || s == 8 || s == 1 || s == 2)) ){
							tree.data = MaisqScore.CommandNameDocument.OUTER_CURVE_RIGHT_AXIS_CENTER;
						}
					}
					else if (switchKey == MaisqScore.CommandNameDocument.INNER_CURVE_RIGHT_AXIS_CENTER) {
						int vec = 0;
						if (b == 7 || b == 8 || b == 1 || b == 2) {
							vec = 1; // 時計回り.
						}
						else if (b == 6 || b == 5 || b == 4 || b == 3) {
							vec = -1; // 反時計回り.
						}
						if ((vec > 0 && (s == 6 || s == 5 || s == 4 || s == 3)) ||
						    (vec < 0 && (s == 7 || s == 8 || s == 1 || s == 2)) ){
							tree.data = MaisqScore.CommandNameDocument.INNER_CURVE_LEFT_AXIS_CENTER;
						}
					}
					else if (switchKey == MaisqScore.CommandNameDocument.INNER_CURVE_LEFT_AXIS_CENTER) {
						int vec = 0;
						if (b == 7 || b == 8 || b == 1 || b == 2) {
							vec = 1; // 時計回り.
						}
						else if (b == 6 || b == 5 || b == 4 || b == 3) {
							vec = -1; // 反時計回り.
						}
						if ((vec > 0 && (s == 6 || s == 5 || s == 4 || s == 3)) ||
						    (vec < 0 && (s == 7 || s == 8 || s == 1 || s == 2)) ){
							tree.data = MaisqScore.CommandNameDocument.INNER_CURVE_RIGHT_AXIS_CENTER;
						}
					}
				}
			}
			
			break;
		}
		case MaisqScore.CommandNameDocument.OUTER_STRAIGHT_CENTER:
		case MaisqScore.CommandNameDocument.INNER_STRAIGHT_CENTER:
		case MaisqScore.CommandNameDocument.CENTER_STRAIGHT_OUTER:
		case MaisqScore.CommandNameDocument.CENTER_STRAIGHT_INNER:
		{
			if (tree.children.Count >= 1) {
				var button = ReadCreateCommandTypeOfButton (tree.children[0]);
				if (button != null) {
					if (turn > 0) button.Turn (turn);
					if (mirror) button.MirrorHolizontal ();
					tree.children[0].data = button.GetValue().ToString();
				}
			}
			break;
		}
		case MaisqScore.CommandNameDocument.STRAIGHT:
		case MaisqScore.CommandNameDocument.CONTINUED_STRAIGHT:
		case MaisqScore.CommandNameDocument.CURVE:
		case MaisqScore.CommandNameDocument.CONTINUED_CURVE: {
			var commandlist = ReadCreateCommandTypeOfSlideCommand (tree, head, beforeCommand);
			foreach (var command in commandlist) {
				if (turn > 0) command.Turn (turn);
				if (mirror) command.MirrorHolizontal ();
				switch (switchKey) {
				case MaisqScore.CommandNameDocument.STRAIGHT:
				{
					tree.children.Clear();
					var straight = command as MaimaiScore.SlideCommandStraight;
					var pos1tree = MaisqScriptTree.CreateRoot(MaisqScore.CommandNameDocument.POSITION);
					pos1tree.AddChildren (straight.start.x, straight.start.y);
					var pos2tree = MaisqScriptTree.CreateRoot(MaisqScore.CommandNameDocument.POSITION);
					pos2tree.AddChildren (straight.target.x, straight.target.y);
					tree.children.Add (pos1tree);
					tree.children.Add (pos2tree);
					break;
				}
				case MaisqScore.CommandNameDocument.CONTINUED_STRAIGHT:
				{
					tree.children.Clear();
					var straight = command as MaimaiScore.SlideCommandStraight;
					var pos1tree = MaisqScriptTree.CreateRoot(MaisqScore.CommandNameDocument.POSITION);
					pos1tree.AddChildren (straight.target.x, straight.target.y);
					tree.children.Add (pos1tree);
					break;
				}
				case MaisqScore.CommandNameDocument.CURVE:
				{
					tree.children.Clear();
					var curve = command as MaimaiScore.SlideCommandCurve;
					var pos1tree = MaisqScriptTree.CreateRoot(MaisqScore.CommandNameDocument.POSITION);
					pos1tree.AddChildren (curve.center.x, curve.center.y);
					var pos2tree = MaisqScriptTree.CreateRoot(MaisqScore.CommandNameDocument.POSITION);
					pos2tree.AddChildren (curve.radius.x, curve.radius.y);
					tree.children.Add (pos1tree);
					tree.children.Add (pos2tree);
					tree.AddChildren (curve.start_degree, curve.distance_degree);
					break;
				}
				case MaisqScore.CommandNameDocument.CONTINUED_CURVE:
				{
					tree.children.Clear();
					var curve = command as MaimaiScore.SlideCommandCurve;
					var pos1tree = MaisqScriptTree.CreateRoot(MaisqScore.CommandNameDocument.POSITION);
					pos1tree.AddChildren (curve.center.x, curve.center.y);
					tree.children.Add (pos1tree);
					tree.AddChild (curve.distance_degree);
					break;
				}
				}
			}
			break;
		}
			
		}
	}

	public void SetEditorData(MaisqScriptTree tree) {
		if (!(bpm > 0) || !(beat > 0 || interval > 0)) {
			return;
		}
		
		ScoreEditorTimeListInformation info = new ScoreEditorTimeListInformation ();
		if (oldBpm != bpm && oldBeat != beat) {
			info.bpm = bpm;
			info.step = RhythmGameLibrary.Score.ScoreStepData.Create(beat, false);
		}
		else if (oldBpm != bpm && oldInterval != interval) {
			info.bpm = bpm;
			info.step = RhythmGameLibrary.Score.ScoreStepData.Create(interval, true);
		}
		else if (oldBpm != bpm) {
			info.bpm = bpm;
		}
		else if (oldBeat != beat) {
			info.step = RhythmGameLibrary.Score.ScoreStepData.Create(beat, false);
		}
		else if (oldInterval != interval) {
			info.step = RhythmGameLibrary.Score.ScoreStepData.Create(interval, true);
		}
		ExtractMaisqScriptTree (info, tree);

		editorScores.Add(info);
		
		oldBpm = bpm;
		oldBeat = beat;
		oldInterval = interval;
	}
	
	/// <summary>
	/// MaiScriptTreeの展開.
	/// </summary>
	/// <param name="outdata">outdataには予めbpmやstepをセットしておく.(過去のインスタンスのそれらの情報を引き継いだ新規インスタンスを渡す)</param>
	/// <param name="tree">Tree.</param>
	public void ExtractMaisqScriptTree (ScoreEditorTimeListInformation outdata, MaisqScriptTree tree) {
		if (outdata == null) return;
		
		if (tree != null) {
			foreach (var child in tree.children) {
				var note = ReadCreateCommandTypeOfNote(child);
				if (note is MaimaiScore.NoteTap) {
					int button = (note as MaimaiScore.NoteTap).button.GetIndex();
					outdata.noteCommands[button].Add (child);
					outdata.noteType[button] = ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.TAP;
				}
				else if (note is MaimaiScore.NoteHold) {
					int button = (note as MaimaiScore.NoteHold).button.GetIndex();
					outdata.noteCommands[button].Add (child);
					outdata.noteType[button] = ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.HOLD;
				}
				else if (note is MaimaiScore.NoteSlide) {
					var slide = note as MaimaiScore.NoteSlide;
					if (slide.head is MaimaiScore.SlideHeadStar) {
						int button = (slide.head as MaimaiScore.SlideHeadStar).button.GetIndex();
						outdata.noteCommands[button].Add (child);
						outdata.noteType[button] = ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.SLIDE;
					}
					else if (slide.head is MaimaiScore.SlideHeadBreakStar) {
						int button = (slide.head as MaimaiScore.SlideHeadBreakStar).button.GetIndex();
						outdata.noteCommands[button].Add (child);
						outdata.noteType[button] = ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.BREAKSTAR_SLIDE;
					}
					else {
						outdata.starNothingSlideStarNoteCommands.Add (child);
					}
				}
				else if (note is MaimaiScore.NoteBreak) {
					int button = (note as MaimaiScore.NoteBreak).button.GetIndex();
					outdata.noteCommands[button].Add (child);
					outdata.noteType[button] = ScoreEditorTimeListInformation.ScoreEditorMaimaiNoteType.BREAK;
				}
				else if (note is MaimaiScore.NoteMessage || note is MaimaiScore.NoteScrollMessage || note is MaimaiScore.NoteSoundMessage) {
					outdata.otherCommands.Add (child);
				}
			}
		}
	}

	public string ToMaisqScript (ScoreEditorTimeListInformation[] editorScore, int scoreEndIndex) {
		string maiScript = string.Empty;
		int restCount = 0;
		System.Action addRest = ()=> {
			if (restCount > 0) {
				maiScript += MaisqScore.CommandNameDocument.REST + "(" + (restCount > 1 ? restCount.ToString() : string.Empty) + ");\n";
				restCount = 0;
			}
		};
		for (int i = 0; (i <= scoreEndIndex || scoreEndIndex < 0) && i < editorScore.Length; i++) {
			var now = editorScore[i];
			if (now.bpm.HasValue && now.bpm.Value > 0) {
				addRest ();
				maiScript += MaisqScore.CommandNameDocument.BPM + "(" + now.bpm.Value.ToString() + ");\n";
			}
			if (now.step != null && now.step.step > 0) {
				addRest ();
				if (!now.step.is_interval) {
					maiScript += MaisqScore.CommandNameDocument.BEAT + "(" + now.step.step + ");\n";
				}
				else {
					maiScript += MaisqScore.CommandNameDocument.INTERVAL + "(" + now.step.step + ");\n";
				}
			}

			bool firstCommand = false;
			bool each = false;
			for (int j = 0; j < now.noteCommands.Length; j++) {
				for (int k = 0; k < now.noteCommands[j].Count; k++) {
					var command = now.noteCommands[j][k];
					if (command != null) {
						if (!firstCommand) {
							addRest ();
							maiScript += MaisqScore.CommandNameDocument.NOTE + "(";
							firstCommand = true;
						}
						if (each) {
							maiScript += ", ";
						}
						maiScript += command.Deploy(); //展開.
						if (!each) each = true;
					}
				}
			}
			for (int j = 0; j < now.starNothingSlideStarNoteCommands.Count; j++) {
				var command = now.starNothingSlideStarNoteCommands[j];
				if (command != null) {
					if (!firstCommand) {
						addRest ();
						maiScript += MaisqScore.CommandNameDocument.NOTE + "(";
						firstCommand = true;
					}
					if (each) {
						maiScript += ", ";
					}
					maiScript += command.Deploy();
					if (!each) each = true;
				}
			}
			for (int j = 0; j < now.otherCommands.Count; j++) {
				var command = now.otherCommands[j];
				if (command != null) {
					if (!firstCommand) {
						addRest ();
						maiScript += MaisqScore.CommandNameDocument.NOTE + "(";
						firstCommand = true;
					}
					if (each) {
						maiScript += ", ";
					}
					maiScript += command.Deploy();
					if (!each) each = true;
				}
			}
			if (firstCommand) {
				maiScript += ");\n";
			}
			else {
				restCount++;
			}
		}
		addRest ();
		return maiScript;
	}

	public string ToMaisqScriptNoteCommand (ScoreEditorTimeListInformation editorScore) {
		string maiScript = string.Empty;
		var now = editorScore;
		bool each = false;
		for (int j = 0; j < now.noteCommands.Length; j++) {
			for (int k = 0; k < now.noteCommands[j].Count; k++) {
				var command = now.noteCommands[j][k];
				if (command != null) {
					if (each) {
						maiScript += ", ";
					}
					maiScript += command.Deploy(); //展開.
					if (!each) each = true;
				}
			}
		}
		for (int j = 0; j < now.starNothingSlideStarNoteCommands.Count; j++) {
			var command = now.starNothingSlideStarNoteCommands[j];
			if (command != null) {
				if (each) {
					maiScript += ", ";
				}
				maiScript += command.Deploy();
				if (!each) each = true;
			}
		}
		for (int j = 0; j < now.otherCommands.Count; j++) {
			var command = now.otherCommands[j];
			if (command != null) {
				if (each) {
					maiScript += ", ";
				}
				maiScript += command.Deploy();
				if (!each) each = true;
			}
		}
		return maiScript;
	}

	public string ToSimaiScript (ScoreEditorTimeListInformation[] editorScore, int scoreEndIndex) {
		string maiScript = ToMaisqScript (editorScore, scoreEndIndex);
		// maiScriptで文字列を""で囲むなら、simaiのmaiScript記述は""ではないほうがいいのではなかろうか.
		// エディタとしては、simaiのmaiScript記述は""で囲み、maiScriptの文字列は''で囲むことにする.
		string ret = string.Empty;
		bool readStringDoubleQuote = false;
		bool readStringSingleQuote = false;
		for (int i = 0; i < maiScript.Length; i++) {
			string subSingle = maiScript.Substring (i, 1);
			bool multiProcessed = false;
			if (i < maiScript.Length - 1) {
				string subMulti = maiScript.Substring (i, 2);
				//""内であるとき、\"は\'に変換.
				//''内であるとき、\'は\'に変換.
				if ((readStringDoubleQuote && subMulti == "\\\"") || (readStringSingleQuote && subMulti == "\\\'")) {
					ret += "\\\'";
					multiProcessed = true;
					i++;
				}
			}
			if (!multiProcessed) {
				//""内であるとき、'は\'に変換.
				//''内であるとき、"は\'に変換.
				if ((readStringDoubleQuote && subSingle == "\'") || (readStringSingleQuote && subSingle == "\"")) {
					ret += "\\\'";
				}
				else if (!readStringDoubleQuote && subSingle == "\'") {
					readStringSingleQuote = !readStringSingleQuote;
					ret += subSingle;
				}
				else if (!readStringSingleQuote && subSingle == "\"") {
					readStringDoubleQuote = !readStringDoubleQuote;
					// "は'に変換.
					ret += "\'";
				}
				else {
					ret += subSingle;
				}
			}
		}
		ret = "#\"" + ret + "\",E";
		return ret;
	}
}
