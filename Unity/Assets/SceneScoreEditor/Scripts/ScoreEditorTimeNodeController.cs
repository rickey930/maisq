﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreEditorTimeNodeController : MonoBehaviour {
	public ScoreEditorTimeListInformation info { get; set; }
	public ScoreEditorTimeListController controller;
	public ScoreEditorSceneManager manager { get { return controller.manager; } }
	public int index { get; private set; }
	public Image button;
	public Image bpmChangedIndicator;
	public Image stepChangedIndicator;
	public Image noteIndicator;
	public GameObject scoreEndMark;
	
	public void UpdateItem (int index, ScoreEditorTimeListInformation info) {
		this.index = index;
		this.info = info;

		if (manager.selectionStartIndex <= index && index <= manager.selectionEndIndex) {
			float alpha = button.color.a;
			button.color = MaimaiStyleDesigner.ByteToPercentRGBA(255, 150, 0, (255 * alpha).ToInt());
		}
		else {
			float alpha = button.color.a;
			button.color = MaimaiStyleDesigner.ByteToPercentRGBA(255, 255, 255, (255 * alpha).ToInt());
		}
		scoreEndMark.SetActive (manager.scoreEndMarkIndex == index);

		if (!info.bpm.HasValue) {
			bpmChangedIndicator.gameObject.SetActive (false);
		}
		else {
			bpmChangedIndicator.gameObject.SetActive (true);
		}
		if (info.step == null) {
			stepChangedIndicator.gameObject.SetActive (false);
		}
		else {
			stepChangedIndicator.gameObject.SetActive (true);
			if (!info.step.is_interval) {
				stepChangedIndicator.sprite = SpriteTank.Get ("center_blue");
			}
			else {
				stepChangedIndicator.sprite = SpriteTank.Get ("center_yellow");
			}
		}
		ScoreEditorTimeListInformation.ScoreEditorNoteIndicator indicator = info.GetIndicator ();
		if (indicator == ScoreEditorTimeListInformation.ScoreEditorNoteIndicator.TAP) {
			noteIndicator.sprite = SpriteTank.Get ("note_tap");
		}
		else if (indicator == ScoreEditorTimeListInformation.ScoreEditorNoteIndicator.HOLD) {
			noteIndicator.sprite = SpriteTank.Get ("note_hold_corn_long");
		}
		else if (indicator == ScoreEditorTimeListInformation.ScoreEditorNoteIndicator.SLIDE) {
			noteIndicator.sprite = SpriteTank.Get ("note_star");
		}
		else if (indicator == ScoreEditorTimeListInformation.ScoreEditorNoteIndicator.BREAK) {
			noteIndicator.sprite = SpriteTank.Get ("note_break");
		}
		else if (indicator == ScoreEditorTimeListInformation.ScoreEditorNoteIndicator.EACH_TAP) {
			noteIndicator.sprite = SpriteTank.Get ("note_tap_each");
		}
		else if (indicator == ScoreEditorTimeListInformation.ScoreEditorNoteIndicator.EACH_HOLD) {
			noteIndicator.sprite = SpriteTank.Get ("note_hold_each_corn_long");
		}
		else if (indicator == ScoreEditorTimeListInformation.ScoreEditorNoteIndicator.EACH_SLIDE) {
			noteIndicator.sprite = SpriteTank.Get ("note_star_each");
		}
		else if (indicator == ScoreEditorTimeListInformation.ScoreEditorNoteIndicator.STARBREAK_SLIDE) {
			noteIndicator.sprite = SpriteTank.Get ("note_star_break");
		}
		else if (indicator == ScoreEditorTimeListInformation.ScoreEditorNoteIndicator.NOHEAD_SLIDE) {
			noteIndicator.sprite = SpriteTank.Get ("note_slide");
		}
		else if (indicator == ScoreEditorTimeListInformation.ScoreEditorNoteIndicator.EACH_NOHEAD_SLIDE) {
			noteIndicator.sprite = SpriteTank.Get ("note_slide_each");
		}
		else if (indicator == ScoreEditorTimeListInformation.ScoreEditorNoteIndicator.NONE) {
			noteIndicator.sprite = SpriteTank.Get ("icon_dammy");
		}
	}
	
	public void OnButtonClick () {
		if (!manager.multiSelectableMode && manager.selectedIndex == index) {
			manager.OnCellDoubleTapped (index, info);
		}
		else {
			manager.selectedIndex = index;
			manager.forceUpdateContents = true;
			manager.OnCellTapped (index, info);
		}
	}
}
