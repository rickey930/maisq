using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectTrackSceneManager : MonoBehaviour {
	
	[SerializeField]
	private TrackListController listCtrl;
	[SerializeField]
	private UnityEngine.UI.Image guideCircle;
	[SerializeField]
	private GameObject sensorUIPrefab;
	[SerializeField]
	private RectTransform sensorUIParent;
	[SerializeField]
	private UnityEngine.UI.Image jacketPictureFrame;
	[SerializeField]
	private GameObject loadingScreen;
	[SerializeField]
	private FadeController fade;
	[SerializeField]
	private GameObject syncWaitDialog;
	[SerializeField]
	private GameObject syncServerDisconnectDialogObj;
	[SerializeField]
	private GameObject musicLoadWaitWall;
	[SerializeField]
	private GameObject nextSceneLoadWaitWall;

	private Sprite[] sensorIcons;

	public Dictionary<string, TrackListInformation>[] trackList { get; set; }
	public TrackListInformation[] scoreList { get; set; }
	private Dictionary<string, string> difficulties { get; set; } // <trackId, difficulty>
	public bool useScoreList { get; protected set; }
	public bool forceUpdateContents { get; set; }
	private SensorUIImageController[] sensorImages { get; set; }
	public string selectedTrackId { get; set; }
	public string selectedScoreId { get; set; }
	private float sensorTurboSpeed0 { get; set; }
	private float sensorTurboSpeed3 { get; set; }
	private float sensorTurboFirstSpeed { get { return 65.0f; } }
	private float sensorTurboAddSpeed { get { return 180.0f * Time.deltaTime; } }
	private float sensorTurboMaxSpeed { get { return sensorTurboFirstSpeed * 1.5f; } }

	// Use this for initialization
	IEnumerator Start () {
		loadingScreen.SetActive (true);
		musicLoadWaitWall.SetActive (false);
		nextSceneLoadWaitWall.SetActive (false);
		while (UserDataController.instance == null)
			yield return null;

		difficulties = UserDataController.instance.user.last_played_difficulties;
		//Test ();

		sensorIcons = new Sprite[] {
			SpriteTank.Get ("icon_allow_up"),
			SpriteTank.Get ("icon_level_plus"),
			SpriteTank.Get ("icon_level_minus"),
			SpriteTank.Get ("icon_allow_down"),
			SpriteTank.Get ("icon_step_decide"),
			SpriteTank.Get ("icon_step_cancel"),
			SpriteTank.Get ("icon_profile"),
			SpriteTank.Get ("icon_option"),
		};

		ConfigTypes.SortLevels sortLevel = UserDataController.instance.user.sort_type.ToSortLevels ();
		useScoreList = !(sortLevel == ConfigTypes.SortLevels.IMPORTED || sortLevel == ConfigTypes.SortLevels.TITLE || sortLevel == ConfigTypes.SortLevels.ARTIST);
		string refineSearchLevel = UserDataController.instance.user.refine_level;
		string refineSearchDifficulty = UserDataController.instance.user.refine_difficulty;
		string refineSearchDesigner = UserDataController.instance.user.refine_designer;
		int refineSearchNoMiss = UserDataController.instance.user.refine_no_miss;
		int refineSearchFC = UserDataController.instance.user.refine_fc;
		int refineSearchAP = UserDataController.instance.user.refine_ap;
		if (!useScoreList) {
			trackList = Sort (FindAll(CreateTrackList (), TrackListInformation.FindMultiConditions(refineSearchLevel, refineSearchDifficulty, refineSearchDesigner, refineSearchNoMiss, refineSearchFC, refineSearchAP)), TrackListInformation.SortCondition (sortLevel));
			
			foreach (var track in trackList) {
				foreach (var scoreId in track.Keys) {
					if (!difficulties.ContainsKey(track[scoreId].trackId)) {
						difficulties[track[scoreId].trackId] = scoreId;
					}
					break;
				}
			}
		}
		else {
			scoreList = Sort (FindAll(CreateAllScoreList (), TrackListInformation.FindMultiConditions(refineSearchLevel, refineSearchDifficulty, refineSearchDesigner, refineSearchNoMiss, refineSearchFC, refineSearchAP)), TrackListInformation.SortCondition (sortLevel));
		}

		float radius = (Constants.instance.MAIMAI_OUTER_RADIUS + 5) * 2;
		guideCircle.transform.RectCast ().sizeDelta = new Vector2 (radius, radius);
		while (MaipadDynamicCreatedSpriteTank.instance.guideCircleMenu == null) {
			yield return null;
		}
		guideCircle.sprite = MaipadDynamicCreatedSpriteTank.instance.guideCircleMenu;
		CreateSensors ();

		PlayMainBGM ();

		while (!listCtrl.setupCompleted) {
			yield return null;
		}
		(listCtrl.transform as RectTransform).anchoredPosition = new Vector2 (0, UserDataController.instance.user.last_played_track_list_scroll);

		// シンクモードなら信号を送る.
		if (MiscInformationController.instance.syncMode > 0) {
			SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateHostSelectingTrack", RPCMode.OthersBuffered);
		}
		
		yield return StartCoroutine (UserDataController.instance.CreateResourceCache (
			UserDataController.instance.config.is_load_jacket_when_select_track,
			UserDataController.instance.config.is_load_audio_when_select_track));
		yield return new WaitForSeconds (0.3f);
		loadingScreen.SetActive (false);
		if (MiscInformationController.instance.selectMusicVoiceCallRequest) {
			Utility.SoundEffectManager.Play ("maisq_voice_pleaseselectmusic");
			MiscInformationController.instance.selectMusicVoiceCallRequest = false;
		}
	}

	private void PlayMainBGM () {
		if (MiscInformationController.instance.lastLoadedBgmKey != "maisq_bgm_main") {
			AudioManagerLite.Pause ();
			AudioManagerLite.Load (Utility.SoundEffectManager.GetClip ("maisq_bgm_main"));
			MiscInformationController.instance.lastLoadedBgmKey = "maisq_bgm_main";
			AudioManagerLite.Seek (0);
			AudioManagerLite.Play (true);
		}
	}

	private void CreateSensors() {
		sensorImages = new SensorUIImageController[8];
		for (int i = 0; i < 8; i++) {
			var sensorImageObj = Instantiate(sensorUIPrefab) as GameObject;
			sensorImageObj.name = "Sensor for UI " + (i + 1).ToString();
			sensorImageObj.SetParentEx (sensorUIParent);
			var sensorImage = sensorImageObj.GetComponent<SensorUIImageController>();
			sensorImage.SetPosition (Constants.instance.GetPieceDegree(i), Constants.instance.MAIMAI_OUTER_RADIUS);
			sensorImage.Setup(sensorIcons[i]);
			sensorImages[i] = sensorImage;
		}
		
		// Scroll Up.
		sensorImages [0].onSensorHoldDownRepeat = () => {
			Utility.SoundEffectManager.Play ("se_select");
			float speed;
			if (sensorTurboSpeed0 == 0) {
				speed = sensorTurboFirstSpeed;
			}
			else {
				speed = sensorTurboSpeed0;
			}
			sensorTurboSpeed0 += sensorTurboAddSpeed;
			if (speed > sensorTurboMaxSpeed) speed = sensorTurboMaxSpeed;
			var rect = listCtrl.transform.RectCast ();
			rect.anchoredPosition = new Vector2 (0, rect.anchoredPosition.y - speed);
		};
		sensorImages [0].onSensorHoldUp = () => {
			sensorTurboSpeed0 = 0;
		};
		// Difficulty Up.
		sensorImages [1].onSensorClick = OnNextDifficultyButtonClick;
		// Difficulty Down.
		sensorImages [2].onSensorClick = OnPrevDifficultyButtonClick;
		// Scroll Down.
		sensorImages [3].onSensorHoldDownRepeat = () => {
			Utility.SoundEffectManager.Play ("se_select");
			float speed;
			if (sensorTurboSpeed3 == 0) {
				speed = sensorTurboFirstSpeed;
			}
			else {
				speed = sensorTurboSpeed3;
			}
			sensorTurboSpeed3 += sensorTurboAddSpeed;
			if (speed > sensorTurboMaxSpeed) speed = sensorTurboMaxSpeed;
			var rect = listCtrl.transform.RectCast ();
			rect.anchoredPosition = new Vector2 (0, rect.anchoredPosition.y + speed);
		};
		sensorImages [3].onSensorHoldUp = () => {
			sensorTurboSpeed3 = 0;
		};
		// OK.
		sensorImages [4].onSensorClick = () => {
			OnGoNextSceneButtonClick(selectedTrackId, selectedScoreId);
		};
		// Cancel.
		sensorImages [5].onSensorClick = () => {
			Utility.SoundEffectManager.Play ("se_decide");
			PlayMainBGM ();
			if (MiscInformationController.instance.syncMode == 1) {
				syncServerDisconnectDialogObj.SetActive (false);
				SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateHostDisconnectOrder", RPCMode.OthersBuffered);
			}
			fade.Activate (()=>{
				Application.LoadLevel ("SceneMainMenu");
			});
		};
		// Profile.
		sensorImages [6].onSensorClick = OnGoProfileSceneButtonClick;
		// Config.
		sensorImages [7].onSensorClick = OnGoConfigSceneButtonClick;
	}
	
	public void OnGoNextSceneButtonClick (string trackId, string scoreId) {
		if (nextSceneLoadWaitWall.activeSelf || musicLoadWaitWall.activeSelf)
			return;

		Utility.SoundEffectManager.Play ("se_decide");
		if (!string.IsNullOrEmpty(selectedTrackId) && !string.IsNullOrEmpty(selectedScoreId) && 
		    ((useScoreList && selectedTrackId == trackId && selectedScoreId == scoreId) || (!useScoreList && selectedTrackId == trackId))
		    ) {
			AudioManagerLite.Pause ();
			nextSceneLoadWaitWall.SetActive (true);
			StartCoroutine (OnGoNextSceneButtonClickSubRoutine (trackId, scoreId));
		}
		else {
			if (!string.IsNullOrEmpty(trackId) && !string.IsNullOrEmpty(scoreId)) {
				StartCoroutine (OnSelectedTrackCoroutine (trackId, scoreId));
			}
		}
	}
	public void OnGoNextSceneButtonClick (TrackNodeKeyHolder keyHolder) {
		if (keyHolder != null) {
			OnGoNextSceneButtonClick (keyHolder.trackId, keyHolder.scoreId);
		}
	}
	private IEnumerator OnGoNextSceneButtonClickSubRoutine (string trackId, string scoreId) {
		yield return null;

		if (!string.IsNullOrEmpty(trackId) && !string.IsNullOrEmpty(scoreId)) {

			yield return StartCoroutine(TrackInformationController.instance.Setup(trackId, scoreId));

			// シンクモードなら信号を送ってクライアントのDL完了を待つ.
			if (MiscInformationController.instance.syncMode > 0) {
				var trackInfo = UserDataController.instance.GetTrack(trackId);
				var jacketExtension = System.IO.Path.GetExtension(trackInfo.jacket);
				var audioExtension = System.IO.Path.GetExtension(trackInfo.audio);
				byte[] jacketData = new byte[0x10]; // RPCだとnew byte[0]は送れないらしい.
				byte[] audioData = new byte[0x10];
				bool useJacket = false;
				bool useAudio = false;
				if (System.IO.File.Exists (DataPath.GetJacketPath (trackId, jacketExtension))) {
					jacketData = System.IO.File.ReadAllBytes (DataPath.GetJacketPath (trackId, jacketExtension));
					useJacket = true;
				}
				if (System.IO.File.Exists (DataPath.GetAudioPath (trackId, audioExtension))) {
					audioData = System.IO.File.ReadAllBytes (DataPath.GetAudioPath (trackId, audioExtension));
					useAudio = true;
				}
				SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateHostTrackDecided", RPCMode.OthersBuffered);
				while (!SyncLobbyNetworkStateController.instance.clientDownloadStartPrepared) {
					// クライアントのUIが「ダウンロード中です」になるのを待つ.
					yield return null;
				}
				SyncLobbyNetworkStateController.instance.networkView.RPC ("NotificateHostTrackDownload", RPCMode.OthersBuffered, 
				                                                          System.Text.Encoding.UTF8.GetBytes(Newtonsoft.Json.JsonConvert.SerializeObject (trackInfo)),
				                                                          scoreId,
				                                                          jacketExtension,
				                                                          jacketData,
				                                                          useJacket,
				                                                          audioExtension,
				                                                          audioData,
				                                                          useAudio);
				while (!SyncLobbyNetworkStateController.instance.clientDownloadCompleted) {
					// 相手のDL完了を待っています.
					if (!syncWaitDialog.activeSelf) {
						syncWaitDialog.SetActive (true);
					}
					yield return null;
				}
				syncWaitDialog.SetActive (false);
			}


			// debug code.
			//TrackInfomationController.instance.calledSetup = false;

			UserDataController.instance.user.last_played_difficulties = difficulties;
			UserDataController.instance.user.last_played_track_list_scroll = (listCtrl.transform as RectTransform).anchoredPosition.y;
			UserDataController.instance.SaveUserInfo ();
			MiscInformationController.instance.rhythmGameRequesterSceneName = "select_track";
			AudioManagerLite.Pause ();
			fade.Activate (()=>{
				Application.LoadLevel("SceneRhythmGame");
			});
		}
	}
	private IEnumerator OnSelectedTrackCoroutine (string trackId, string scoreId) {
#if !UNITY_STANDALONE
		if (!UserDataController.instance.config.is_load_audio_when_select_track &&
		    (UserDataController.instance.config.user_recources_cache_type.ToResourcesCacheType() == ConfigTypes.ResourcesCacheType.UNUSED || !UserDataController.instance.IsContainAudioCache (trackId))) {
			musicLoadWaitWall.SetActive (true);
			yield return null;
		}
#endif
		selectedTrackId = trackId;
		selectedScoreId = scoreId;
		string cmpTrackId = selectedTrackId;
		if (UserDataController.instance.config.is_load_audio_when_select_track) {
			AudioManagerLite.Pause ();
		}
		System.Action<AudioClip> loadCallback = (clip)=>{
			if (selectedTrackId == cmpTrackId) {  
				AudioManagerLite.Load (clip);
				MiscInformationController.instance.lastLoadedBgmKey = "track_audio_trial";
				long seek = 0L;
				TrackInformation tInfo = UserDataController.instance.GetTrack (trackId);
				if (tInfo != null) seek = (tInfo.seek * 1000.0f).ToLong ();
				AudioManagerLite.Seek (seek);
//						AudioManagerLite.Seek (AudioManagerLite.GetDuration() / 2);
//						AudioManagerLite.FadeIn (
//						AudioManagerLite.FadeOut(5, 1, ()=>{
//							
//						});
				AudioManagerLite.Play (true);
			}
		};
		System.Action<Sprite> jacketCallback = (jacket)=>{
			// 背景を設定.
			if (selectedTrackId == cmpTrackId) {
				if (jacket != null) {
					jacketPictureFrame.sprite = jacket;
				}
				else {
					// ジャケットが無ければデフォ画像を表示.
					jacketPictureFrame.sprite = SpriteTank.Get ("bg_default");
				}
			}
		};
		if (!useScoreList) {
			var e1 = new List<Dictionary<string, TrackListInformation>>(trackList).Find (
				(data)=> {
				var e2 = data.Values.GetEnumerator();
				e2.MoveNext();
				return e2.Current.trackId == selectedTrackId;
			}).GetEnumerator();
			e1.MoveNext();
			if (UserDataController.instance.config.is_load_audio_when_select_track) {
				StartCoroutine (e1.Current.Value.LoadAudio (this, loadCallback));
			}
			if (UserDataController.instance.config.is_load_jacket_when_select_track) {
				StartCoroutine (e1.Current.Value.LoadJacket (this, jacketCallback));
			}
			else {
				// select trackシーンでは読み込まない設定ならデフォ背景.
				jacketPictureFrame.sprite = SpriteTank.Get ("bg_default");
			}
		}
		else {
			var track = new List<TrackListInformation>(scoreList).Find ((data)=>data.trackId == selectedTrackId);
			if (UserDataController.instance.config.is_load_audio_when_select_track) {
				StartCoroutine (track.LoadAudio (this, loadCallback));
			}
			if (UserDataController.instance.config.is_load_jacket_when_select_track) {
				StartCoroutine (track.LoadJacket (this, jacketCallback));
			}
			else {
				// select trackシーンでは読み込まない設定ならデフォ背景.
				jacketPictureFrame.sprite = SpriteTank.Get ("bg_default");
			}
		}
		forceUpdateContents = true;
		musicLoadWaitWall.SetActive (false);
		yield break;
	}

	public void OnNextDifficultyButtonClick () {
		if (!useScoreList && trackList != null) {
			bool changed = false;
			foreach (var track in trackList) {
				string trackId = string.Empty;
				bool find = false;
				bool final = false;
				string nextKey = string.Empty;
				string finalKey = string.Empty;
				// nextキーを探す.
				foreach (var scoreId in track.Keys) {
					if (string.IsNullOrEmpty(trackId)) {
						trackId = track[scoreId].trackId;
					}
					if (find) {
						final = false;
						nextKey = scoreId;
						break;
					}
					if (GetDifficulty(trackId) == scoreId) {
						find = true;
						final = true;
					}
					finalKey = scoreId;
				}
				// nextが無ければ最後のキーに留まる.
				if (final) {
					nextKey = finalKey;
					find = true;
				}

				if (find) {
					difficulties[trackId] = nextKey;
					changed = true;
				}
			}
			if (changed) {
				Utility.SoundEffectManager.Play ("se_select");
				ForceUpdateContents ();
			}
		}
	}

	public void OnPrevDifficultyButtonClick () {
		if (!useScoreList && trackList != null) {
			bool changed = false;
			foreach (var track in trackList) {
				string trackId = string.Empty;
				bool find = false;
				bool first = true;
				string prevKey = string.Empty;
				string previousKey = string.Empty;
				// prevキーを探す.
				foreach (var scoreId in track.Keys) {
					if (string.IsNullOrEmpty(trackId)) {
						trackId = track[scoreId].trackId;
					}
					if (GetDifficulty(trackId) == scoreId) {
						find = true;
						// prevが無ければ最初のキーに留まる.
						if (first) {
							previousKey = scoreId;
						}
						prevKey = previousKey;
						break;
					}
					first = false;
					previousKey = scoreId;
				}
				
				if (find) {
					difficulties[trackId] = prevKey;
					changed = true;
				}
			}
			if (changed) {
				Utility.SoundEffectManager.Play ("se_select");
				ForceUpdateContents ();
			}
		}
	}

	public void OnGoConfigSceneButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		MiscInformationController.instance.optionSceneCallbackSceneName = "SceneSelectTrack";
		PlayMainBGM ();
		UserDataController.instance.user.last_played_difficulties = difficulties;
		UserDataController.instance.user.last_played_track_list_scroll = (listCtrl.transform as RectTransform).anchoredPosition.y;
		fade.Activate (() => {
			Application.LoadLevel ("SceneConfig");
		});
	}

	public void OnGoProfileSceneButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		MiscInformationController.instance.optionSceneCallbackSceneName = "SceneSelectTrack";
		PlayMainBGM ();
		UserDataController.instance.user.last_played_difficulties = difficulties;
		UserDataController.instance.user.last_played_track_list_scroll = (listCtrl.transform as RectTransform).anchoredPosition.y;
		fade.Activate (() => {
			Application.LoadLevel ("SceneProfile");
		});
	}

	public void ForceUpdateContents() {
		forceUpdateContents = true;
	}

	public void Test () {
		for (int i = 0; i < 20; i++) {
			string number = i.ToString("00");
			TrackInformation info = TrackInformation.NewInitialize ();
			info.title = "music title " + number;
			info.artist = "music artist " + (300 - i).ToString();
			info.whole_bpm = "1" + number;
			info.imported_at = Datetime.Now();
			info.scores = new Dictionary<string, ScoreInformation>();
			var score = ScoreInformation.NewInitialize ();
			score.level = i.ToString();
			info.scores ["EASY"] = score;
			score = ScoreInformation.NewInitialize ();
			score.level = (99 - i).ToString();
			if (i < 18) {
				info.scores ["BASIC"] = score;
			}else {
				info.scores ["ADVANCED"] = score;
			}
			UserDataController.instance.AddTrack (
				"test" + number,
				info
			);
		}
	}

	public string GetDifficulty (string trackId) {
		bool exist = true;
		if (!difficulties.ContainsKey (trackId)) {
			exist = false;
			var track = UserDataController.instance.GetTrack(trackId);
			if (track != null) {
				foreach (var scoreId in track.scores.Keys) {
					difficulties[trackId] = scoreId;
					break;
				}
			}
		}
		if (exist || difficulties.ContainsKey (trackId)) {
			return difficulties [trackId];
		}
		return string.Empty;
	}
	
	private Dictionary<string, TrackListInformation>[] CreateTrackList () {
		var trackList = new List<Dictionary<string, TrackListInformation>>();
		foreach (string trackId in UserDataController.instance.tracks.Keys) {
			var trackListInfo = new Dictionary<string, TrackListInformation>();
			foreach (string scoreId in UserDataController.instance.tracks[trackId].scores.Keys) {
				var scoreInfo = UserDataController.instance.GetScore(trackId, scoreId);
				if (scoreInfo.full_score > 0) {
					trackListInfo[scoreId] = new TrackListInformation(trackId, scoreId);
				}
			}
			
			// スコア順並び替え.
			var scoreSortedTrackListInfo = new Dictionary<string, TrackListInformation>();
			var scoreIdSortKeys = new string[] {
				"EASY", "BASIC", "ADVANCED", "EXPERT", "MASTER", "Re:MASTER"
			};
			System.Action<string> scoresSort = (scoreId) => {
				if (trackListInfo.ContainsKey(scoreId)) {
					scoreSortedTrackListInfo[scoreId] = trackListInfo[scoreId];
				}
			};
			// 基本スコアを優先的に入れる.
			foreach (string difficulty in scoreIdSortKeys) {
				scoresSort(difficulty);
			}
			// その他のスコアの並び替え.
			foreach (string key in trackListInfo.Keys) {
				if (!scoreSortedTrackListInfo.ContainsKey(key)) {
					scoresSort(key);
				}
			}

			if (scoreSortedTrackListInfo.Count > 0) {
				trackList.Add (scoreSortedTrackListInfo);
			}
		}
		return trackList.ToArray ();
	}

	private TrackListInformation[] CreateAllScoreList () {
		List<TrackListInformation> scoreList = new List<TrackListInformation> ();
		foreach (string trackId in UserDataController.instance.tracks.Keys) {
			foreach (string scoreId in UserDataController.instance.tracks[trackId].scores.Keys) {
				var scoreInfo = UserDataController.instance.GetScore(trackId, scoreId);
				if (scoreInfo.full_score > 0) {
					var item = new TrackListInformation(trackId, scoreId);
					scoreList.Add (item);
				}
			}
		}
		return scoreList.ToArray ();
	}

	private Dictionary<string, TrackListInformation>[] Sort (Dictionary<string, TrackListInformation>[] list, System.Comparison<TrackListInformation> match) {
		System.Comparison<Dictionary<string, TrackListInformation>> dicmatch = (data1, data2) => {
			var e1 = data1.Keys.GetEnumerator();
			var e2 = data2.Keys.GetEnumerator();
			e1.MoveNext ();
			e2.MoveNext ();
			if (e1.Current != null && e2.Current != null) {
				return match (data1 [e1.Current], data2 [e2.Current]);
			}
			return 0;
		};
		var _list = (Dictionary<string, TrackListInformation>[])list.Clone ();
		System.Array.Sort<Dictionary<string, TrackListInformation>> (_list, dicmatch);
		return _list;
	}

	private Dictionary<string, TrackListInformation>[] FindAll (Dictionary<string, TrackListInformation>[] list, System.Predicate<TrackListInformation> match) {
		System.Predicate<Dictionary<string, TrackListInformation>> dicmatch = (data) => {
			var e = data.Keys.GetEnumerator();
			e.MoveNext ();
			if (e.Current != null) {
				return match (data [e.Current]);
			}
			return false;
		};
		return System.Array.FindAll<Dictionary<string, TrackListInformation>> (list, dicmatch);
	}

	private TrackListInformation[] Sort (TrackListInformation[] list, System.Comparison<TrackListInformation> match) {
		var _list = (TrackListInformation[])list.Clone ();
		System.Array.Sort<TrackListInformation> (_list, match);
		return _list;
	}
	
	private TrackListInformation[] FindAll (TrackListInformation[] list, System.Predicate<TrackListInformation> match) {
		return System.Array.FindAll<TrackListInformation> (list, match);
	}

}
