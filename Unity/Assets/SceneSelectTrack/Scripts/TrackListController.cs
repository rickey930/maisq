using UnityEngine;
using System.Collections;

public class TrackListController : MonoBehaviour, IInfiniteScrollSetup {

	[SerializeField]
	private SelectTrackSceneManager manager;

	public bool setupCompleted { get; set; }
	public bool forceUpdateContents { 
		get { return manager.forceUpdateContents; } 
		set { manager.forceUpdateContents = value; }
	}
	
	public IEnumerator OnPostSetupItems() {
		// リストの初期化.
		while (UserDataController.instance == null || (manager.trackList == null && manager.scoreList == null))
			yield return null;
		setupCompleted = true;
	}
	
	public void OnUpdateItem (int index, GameObject obj) {
		if ((!manager.useScoreList && (manager.trackList == null || manager.trackList.Length == 0)) ||
			(manager.useScoreList && (manager.scoreList == null || manager.scoreList.Length == 0))) {
			if (obj.activeSelf) {
				obj.SetActive (false);
			}
		}
		else {
			if (!obj.activeSelf) {
				obj.SetActive (true);
			}
			int listLen = manager.useScoreList ? manager.scoreList.Length : manager.trackList.Length;
			if (index < 0) {
				int mul = (index * -1) / listLen;
				index = (index + listLen * (mul + 1)) % listLen;
			}
			else {
				index = index % listLen;
			}
			TrackListInformation track;
			if (!manager.useScoreList) {
				var e = manager.trackList[index].GetEnumerator();
				e.MoveNext();
				string trackId = e.Current.Value.trackId;
				track = manager.trackList[index][manager.GetDifficulty(trackId)];
			}
			else {
				track = manager.scoreList[index];
			}
			TrackNodeController item = obj.GetComponent<TrackNodeController> ();
			item.UpdateItem (track, manager.selectedTrackId, manager.selectedScoreId);
		}
	}
}
