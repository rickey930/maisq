using UnityEngine;
using System.Collections;
using System;

public class TrackListInformation {
	public string trackId;
	public Sprite trackIcon;
	public string jacket;
	public AudioClip audioClip;
	public string audio;
	public int playCount;
	public string bpm;
	public string difficulty;
	public string level;
	public string notesDesigner;
	public string title;
	public string titleRuby;
	public string artist;
	public string artistRuby;
	public string rank;
	public int achievement;
	public int score;
	public bool noMiss;
	public bool fc;
	public bool ap;
	public bool ms;
	public int sync;
	public DateTime importedAt;

	public TrackListInformation(string trackId, string scoreId) {
		this.trackId = trackId;
		var track = UserDataController.instance.GetTrack(trackId);
		var result = UserDataController.instance.GetResult(trackId, scoreId);
		
		if (result == null) {
			result = ResultInformation.NewInitialize ();
		}
		jacket = track.jacket;
		audio = track.audio;
		playCount = result.play_count;
		bpm = track.whole_bpm;
		difficulty = scoreId;
		var scoreInfo = track.scores [scoreId];
		level = scoreInfo.level;
		notesDesigner = scoreInfo.notes_design;
		title = track.title;
		titleRuby = track.title_ruby;
		artist = track.artist;
		artistRuby = track.artist_ruby;
		if (UserDataController.instance.config.rank_version_type.ToRankVersionType() == ConfigTypes.RankVersion.PINK) {
			rank = MaimaiStyleDesigner.GetRankString (result.achievement / 100.0f);
		}
		else if (UserDataController.instance.config.rank_version_type.ToRankVersionType() == ConfigTypes.RankVersion.CLASSIC) {
			rank = MaimaiStyleDesigner.GetRankStringClassic (result.achievement / 100.0f);
		}
		achievement = result.achievement;
		score = result.score;
		noMiss = result.no_miss;
		fc = result.fc;
		ap = result.ap;
		ms = result.full_ap;
		importedAt = track.imported_at.ToSystem ();
		sync = result.sync;
	}

	private int? _level_int;
	private bool _level_int_casted;
	public int? level_int {
		get {
			if (!_level_int_casted) {
				int to;
				string lvstr = levelIntHelper;
				string sublvstr = string.IsNullOrEmpty(lvstr) ? string.Empty : lvstr.Substring(lvstr.Length - 1, 1);
				int pluslevel = 0;
				if (sublvstr == "+" || sublvstr == "＋") {
					lvstr = lvstr.Substring(0, lvstr.Length - 1);
					pluslevel = 1;
				}
				if (int.TryParse(lvstr, out to)) {
					_level_int = to * 2 + pluslevel;
				}
				else {
					_level_int = null;
				}
			}
			_level_int_casted = true;
			return _level_int;
		}
	}

	private string levelIntHelper {
		get {
			switch (level) {
			case "０": case "⓪": return "0";
			case "０+": case "⓪+": case "0＋": case "０＋": case "⓪＋": return "0+";
			case "１": case "①": return "1";
			case "１+": case "①+": case "1＋": case "１＋": case "①＋": return "1+";
			case "２": case "②": return "2";
			case "２+": case "②+": case "2＋": case "２＋": case "②＋": return "2+";
			case "３": case "③": return "3";
			case "３+": case "③+": case "3＋": case "３＋": case "③＋": return "3+";
			case "４": case "④": return "4";
			case "４+": case "④+": case "4＋": case "４＋": case "④＋": return "4+";
			case "５": case "⑤": return "5";
			case "５+": case "⑤+": case "5＋": case "５＋": case "⑤＋": return "5+";
			case "６": case "⑥": return "6";
			case "６+": case "⑥+": case "6＋": case "６＋": case "⑥＋": return "6+";
			case "７": case "⑦": return "7";
			case "７+": case "⑦+": case "7＋": case "７＋": case "⑦＋": return "7+";
			case "８": case "⑧": return "8";
			case "８+": case "⑧+": case "8＋": case "８＋": case "⑧＋": return "8+";
			case "９": case "⑨": return "9";
			case "９+": case "⑨+": case "9＋": case "９＋": case "⑨＋": return "9+";
			case "１０": case "⑩": return "10";
			case "１０+": case "⑩+": case "10＋": case "１０＋": case "⑩＋": return "10+";
			case "１１": case "⑪": return "11";
			case "１１+": case "⑪+": case "11＋": case "１１＋": case "⑪＋": return "11+";
			case "１２": case "⑫": return "12";
			case "１２+": case "⑫+": case "12＋": case "１２＋": case "⑫＋": return "12+";
			case "１３": case "⑬": return "13";
			case "１３+": case "⑬+": case "13＋": case "１３＋": case "⑬＋": return "13+";
			case "１４": case "⑭": return "14";
			case "１４+": case "⑭+": case "14＋": case "１４＋": case "⑭＋": return "14+";
			case "１５": case "⑮": return "15";
			case "１５+": case "⑮+": case "15＋": case "１５＋": case "⑮＋": return "15+";
			case "１６": case "⑯": return "16";
			case "１６+": case "⑯+": case "16＋": case "１６＋": case "⑯＋": return "16+";
			case "１７": case "⑰": return "17";
			case "１７+": case "⑰+": case "17＋": case "１７＋": case "⑰＋": return "17+";
			case "１８": case "⑱": return "18";
			case "１８+": case "⑱+": case "18＋": case "１８＋": case "⑱＋": return "18+";
			case "１９": case "⑲": return "19";
			case "１９+": case "⑲+": case "19＋": case "１９＋": case "⑲＋": return "19+";
			case "２０": case "⑳": return "20";
			case "２０+": case "⑳+": case "20＋": case "２０＋": case "⑳＋": return "20+";
			default:
				return level;
			}
		}
	}

	public int difficultyIntHelper {
		get {
			string diff = difficulty.ToLower ();
			switch (diff) {
			case "easy":
				return 1;
			case "basic":
				return 2;
			case "advanced":
				return 3;
			case "expert":
				return 4;
			case "master":
				return 5;
			case "re_master":
			case "remaster":
			case "re master":
			case "re:master":
				return 6;
			default:
				return 7;
			}
		}
	}

	public IEnumerator LoadJacket (MonoBehaviour component, System.Action<Sprite> callback) {
		if (trackIcon == null) {
			yield return component.StartCoroutine (UserDataController.instance.LoadJacket (trackId, System.IO.Path.GetExtension (jacket), (data)=>trackIcon = data));
		}
		callback (trackIcon);
	}

	public IEnumerator LoadAudio (MonoBehaviour component, System.Action<AudioClip> callback) {
		if (audioClip == null) {
			yield return component.StartCoroutine (UserDataController.instance.LoadAudio (trackId, System.IO.Path.GetExtension (audio), (data)=>audioClip = data));
		}
		callback (audioClip);
	}

	public static Comparison<TrackListInformation> SortCondition (ConfigTypes.SortLevels condition) {
		var conditions = new TrackListSortCondition[] { 
			condition == ConfigTypes.SortLevels.TITLE ? new TrackListSortCondition(TrackListSortCondition.SortTitle) :
			condition == ConfigTypes.SortLevels.ARTIST ? new TrackListSortCondition(TrackListSortCondition.SortArtist) :
			condition == ConfigTypes.SortLevels.SCORE ? new TrackListSortCondition(TrackListSortCondition.SortScore) :
			condition == ConfigTypes.SortLevels.ACHIEVEMENT ? new TrackListSortCondition(TrackListSortCondition.SortAchievement) :
			condition == ConfigTypes.SortLevels.LEVEL ? new TrackListSortCondition(TrackListSortCondition.SortLevel) :
			condition == ConfigTypes.SortLevels.PLAYCOUNT ? new TrackListSortCondition(TrackListSortCondition.SortPlayCount) :
			new TrackListSortCondition(TrackListSortCondition.SortImportedAt),
			// ソートの第2要素はID順にしとく.
			new TrackListSortCondition(TrackListSortCondition.SortId),
			// ソートの第3要素は難易度順にしとく.
			new TrackListSortCondition(TrackListSortCondition.SortDifficulty),
		};
		return TrackListSortCondition.GetMultiConditionsComparison (conditions);
	}

	public static Predicate<TrackListInformation> FindMultiConditions (string level, string difficulty, string designer, int isNoMiss, int isFC, int isAP) {
		var conditions = new TrackListRefineCondition[] {
			new TrackListRefineCondition(TrackListRefineCondition.FindLevel, level),
			new TrackListRefineCondition(TrackListRefineCondition.FindDifficulty, difficulty),
			new TrackListRefineCondition(TrackListRefineCondition.FindDesigner, designer),
			new TrackListRefineCondition(TrackListRefineCondition.FindNoMiss, isNoMiss),
			new TrackListRefineCondition(TrackListRefineCondition.FindFC, isFC),
			new TrackListRefineCondition(TrackListRefineCondition.FindAP, isAP),
		};
		return TrackListRefineCondition.GetMultiConditionsPredicate (conditions);
	}
}

public class TrackListSortCondition : SortConditionUtility<TrackListInformation> {
	public TrackListSortCondition (Func<TrackListInformation, TrackListInformation, int> func) 
	: base (func) {
	}
	public static int SortScore(TrackListInformation data1, TrackListInformation data2) {
		return data1.score.CompareTo(data2.score);
	}
	public static int SortAchievement(TrackListInformation data1, TrackListInformation data2) {
		return data1.achievement.CompareTo(data2.achievement);
	}
	public static int SortTitle(TrackListInformation data1, TrackListInformation data2) {
		string text1 = string.IsNullOrEmpty (data1.titleRuby) ? data1.title : data1.titleRuby;
		string text2 = string.IsNullOrEmpty (data2.titleRuby) ? data2.title : data2.titleRuby;
		return text1.CompareTo(text2);
	}
	public static int SortArtist(TrackListInformation data1, TrackListInformation data2) {
		string text1 = string.IsNullOrEmpty (data1.artistRuby) ? data1.artist : data1.artistRuby;
		string text2 = string.IsNullOrEmpty (data2.artistRuby) ? data2.artist : data2.artistRuby;
		return text1.CompareTo(text2);
	}
	public static int SortImportedAt(TrackListInformation data1, TrackListInformation data2) {
		return data1.importedAt.CompareTo(data2.importedAt);
	}
	public static int SortId(TrackListInformation data1, TrackListInformation data2) {
		return data1.trackId.CompareTo(data2.trackId);
	}
	public static int SortDifficulty(TrackListInformation data1, TrackListInformation data2) {
		return data1.difficultyIntHelper.CompareTo(data2.difficultyIntHelper);
	}
	public static int SortLevel(TrackListInformation data1, TrackListInformation data2) {
		int a = data1.level_int.HasValue ? data1.level_int.Value : int.MaxValue;
		int b = data2.level_int.HasValue ? data2.level_int.Value : int.MaxValue;
		return a.CompareTo (b);
	}
	public static int SortPlayCount(TrackListInformation data1, TrackListInformation data2) {
		return data1.playCount.CompareTo(data2.playCount);
	}
}

public class TrackListRefineCondition : RefineConditionUtility<TrackListInformation> {
	/// <summary>
	/// <para>※パラメータfuncに使うパラメータvalueの型は一致させること</para>
	/// </summary>
	public TrackListRefineCondition (Func<TrackListInformation, object, bool> func, object value) 
	: base (func, value) {
	}
	public static bool FindLevel(TrackListInformation info, object value) {
		string word = (string)value;
		if (string.IsNullOrEmpty (word))
			return true;
		return info.level == word;
	}
	public static bool FindDifficulty(TrackListInformation info, object value) {
		string word = (string)value;
		if (string.IsNullOrEmpty (word))
			return true;
		return info.difficulty == word;
	}
	public static bool FindDesigner(TrackListInformation info, object value) {
		string word = (string)value;
		if (string.IsNullOrEmpty (word))
			return true;
		return info.notesDesigner.Contains(word);
	}
	public static bool FindRank(TrackListInformation info, object value) {
		string word = (string)value;
		if (string.IsNullOrEmpty (word))
			return true;
		return info.rank == word;
	}
	public static bool FindMS(TrackListInformation info, object value) {
		int type = (int)value; // type: 0(OFF), 1(達成), 2(未達成).
		if (type == 0)
			return true;
		return info.ms == (type == 1);
	}
	public static bool FindAP(TrackListInformation info, object value) {
		int type = (int)value; // type: 0(OFF), 1(達成), 2(未達成).
		if (type == 0)
			return true;
		return info.ap == (type == 1);
	}
	public static bool FindFC(TrackListInformation info, object value) {
		int type = (int)value; // type: 0(OFF), 1(達成), 2(未達成).
		if (type == 0)
			return true;
		return info.fc == (type == 1);
	}
	public static bool FindNoMiss(TrackListInformation info, object value) {
		int type = (int)value; // type: 0(OFF), 1(達成), 2(未達成).
		if (type == 0)
			return true;
		return info.noMiss == (type == 1);
	}
}
