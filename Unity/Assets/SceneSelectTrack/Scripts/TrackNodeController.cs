using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TrackNodeController : MonoBehaviour {
	public TrackNodeKeyHolder keyHolder;
	public Image trackIcon;
	public Text playCountLabel;
	public Text bpmLabel;
	public Text difficultyLabel;
	public Text levelLabel;
	public Text notesDesignerLabel;
	public Text titleLabel;
	public Text artistLabel;
	public Text rankNameLabel;
	public Text rankLabel;
	public Text achievementLabel;
	public Text scoreLabel;
	public Text fcMarkerLabel;
	public Button button;
	public Image difficultyIcon;

	public void UpdateItem (TrackListInformation track, string selectedTrackId, string selectedScoreId) {
		keyHolder.trackId = track.trackId;
		keyHolder.scoreId = track.difficulty;
		if (!string.IsNullOrEmpty(selectedTrackId) && !string.IsNullOrEmpty(selectedScoreId) && selectedTrackId == keyHolder.trackId && selectedScoreId == keyHolder.scoreId) {
			float alpha = button.image.color.a;
			button.image.color = MaimaiStyleDesigner.ByteToPercentRGBA(255, 150, 0, (255 * alpha).ToInt());
		}
		else {
			float alpha = button.image.color.a;
			button.image.color = MaimaiStyleDesigner.ByteToPercentRGBA(255, 255, 255, (255 * alpha).ToInt());
		}
		string cmpTrackId = keyHolder.trackId;
		if (UserDataController.instance.config.is_load_jacket_when_select_track) {
			StartCoroutine (track.LoadJacket (this, (data)=> {
				if (keyHolder.trackId == cmpTrackId) {
					if (data != null) {
						trackIcon.sprite = data;
					}
					else {
						// ジャケットが無ければデフォ画像を表示.
						trackIcon.sprite = SpriteTank.Get ("icon_track_failed");
					}
				}
			}));
		}
		else {
			// select trackシーンでは読み込まない設定なら読み込み失敗画像を表示.
			trackIcon.sprite = SpriteTank.Get ("icon_track_forbidden");
		}
		playCountLabel.text = track.playCount.ToString ();
		bpmLabel.text = track.bpm;
		difficultyLabel.text = track.difficulty;
		levelLabel.text = track.level;
		notesDesignerLabel.text = track.notesDesigner;
		titleLabel.text = track.title;
		artistLabel.text = track.artist;
		titleLabel.color = artistLabel.color = MaimaiStyleDesigner.GetLevelColor(track.difficulty);
		//difficultyLabel.color = levelLabel.color = titleLabel.color;
		rankLabel.color = rankNameLabel.color = MaimaiStyleDesigner.GetAchievementColor(track.achievement / 100.0f);
		rankLabel.text = track.rank;
		if (MiscInformationController.instance.syncMode > 0) {
			achievementLabel.text = track.sync.ToString() + "%";
			achievementLabel.color = MaimaiStyleDesigner.GetSyncColor ();
		}
		else {
			achievementLabel.text = (track.achievement / 100.0f).ToString ("F2") + "%";
			achievementLabel.color = rankLabel.color;
		}
		scoreLabel.text = track.score.ToString ("#,0");
		scoreLabel.color = track.ms ? MaimaiStyleDesigner.GetFullScoreColor () : MaimaiStyleDesigner.GetScoreColor ();
		fcMarkerLabel.text = track.ap ? "AP" : track.noMiss ? "FC" : "";
		fcMarkerLabel.color = track.ap ? Color.red : track.fc ? Color.yellow : track.noMiss ? Color.blue : Color.black;
		difficultyIcon.sprite = MaimaiStyleDesigner.GetDifficultyOctagonSprite(track.difficulty);
	}
}
