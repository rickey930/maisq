﻿using UnityEngine;
using System.Collections;

public class TrackNodeKeyHolder : MonoBehaviour {
	public string trackId { get; set; }
	public string scoreId { get; set; }
}
