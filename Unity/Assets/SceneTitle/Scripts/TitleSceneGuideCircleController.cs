﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TitleSceneGuideCircleController : Image {

	IEnumerator StartCustomCoroutine () {
		while (MaipadDynamicCreatedSpriteTank.instance == null || MaipadDynamicCreatedSpriteTank.instance.guideCircleTitle == null) {
			yield return null;
		}
		this.sprite = MaipadDynamicCreatedSpriteTank.instance.guideCircleTitle;
	}
	
	public void StartCustom () {
		StartCoroutine (StartCustomCoroutine ());
	}
	
	protected override void Start () {
		base.Start ();
		StartCustom ();
	}
}
