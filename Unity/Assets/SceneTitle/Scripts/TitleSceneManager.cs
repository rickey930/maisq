﻿using UnityEngine;
using System.Collections;

public class TitleSceneManager : MonoBehaviour {

	[SerializeField]
	private FadeController fade;

	public void OnNextSceneButtonClick () {
		Utility.SoundEffectManager.Play ("se_decide");
		fade.Activate (() => {
			Application.LoadLevel ("SceneMainMenu");
		});
	}

	void Start () {
		AudioManagerLite.Pause ();
		AudioManagerLite.Load (Utility.SoundEffectManager.GetClip ("maisq_bgm_title"));
		MiscInformationController.instance.lastLoadedBgmKey = "maisq_bgm_title";
		AudioManagerLite.Seek (0);
		AudioManagerLite.Play (false);
		Utility.SoundEffectManager.Play ("maisq_voice_welcome");
	}

}
