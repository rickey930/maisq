﻿using UnityEngine;
using System.Collections;

public class TitleSceneProfileFrameController : ProfileFrameController {
	protected override IEnumerator StartCustomCoroutine () {
		while (ProfileInformationController.instance == null || !ProfileInformationController.instance.loaded_player_frame) {
			yield return null;
		}
		if (ProfileInformationController.instance.player_frame != ProfileInformationController.instance.default_player_frame) {
			this.sprite = ProfileInformationController.instance.player_frame;
		}
		else {
			this.sprite = SpriteTank.Get ("title_back");
		}
		ProfileInformationController.instance.profileFrameCtrl = this;
	}
}
