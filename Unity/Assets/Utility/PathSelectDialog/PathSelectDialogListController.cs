using UnityEngine;
using System.Collections;
using System;
using System.IO;
using UnityEngine.UI;

public class PathSelectDialogListController : MonoBehaviour, IInfiniteScrollSetup {

	#region Inspector
	[SerializeField]
	public bool directoryOnly;
	public InputField pathTextbox;
	public PathSelectDialogErrorDialogController errorDialog;
	public GameObject mainDialogObject;
	#endregion
	
	#region Properties
	public string currentDirectory { get; set; } // UIを使って指定したパス.
	public string inputPath { get { return pathTextbox.text; } set { pathTextbox.text = value; } } // 最終的な決定パス.
	public string defaultDirectory { get { return Application.persistentDataPath; } }
	public PathSelectDialogListInformation[] contents { get; set; }
	public Action<string> onDecide { get; set; }
	public int selectedIndex { get; set; }
	private bool completedSetup { get; set; }
	#endregion

	public void Setup (string firstDirectory, Action<string> onDecide) {
		OpenDirectory (firstDirectory);
		this.onDecide = onDecide;
		completedSetup = true;
	}
	
	// Use this for initialization
	void Start () {
		if (!completedSetup) {
			Setup (null, null);
		}
	}
	
	public void OpenDirectory (string directoryPath) {
		if (string.IsNullOrEmpty (directoryPath) || !Directory.Exists (directoryPath)) {
			if (!string.IsNullOrEmpty (directoryPath) && File.Exists (directoryPath)) {
				directoryPath = Path.GetDirectoryName (directoryPath);
			}
			else {
				directoryPath = defaultDirectory;
			}
		}
		try {
			directoryPath = directoryPath.Replace("\\", "/");
			var info = new DirectoryInfo (directoryPath);
			var directories = info.GetDirectories();
			var files = directoryOnly ? new FileInfo[0] : info.GetFiles();
			contents = new PathSelectDialogListInformation[directories.Length + files.Length];
			for (int i = 0; i < directories.Length; i++) {
				contents[i] = new PathSelectDialogListInformation(i, directories[i].FullName.Replace("\\", "/"), directories[i].Name, true);
			}
			if (!directoryOnly) {
				for (int i = 0; i < files.Length; i++) {
					contents[i + directories.Length] = new PathSelectDialogListInformation(i + directories.Length, files[i].FullName.Replace("\\", "/"), files[i].Name, false);
				}
			}
			currentDirectory = directoryPath;
			inputPath = currentDirectory;
			selectedIndex = -1;
			ResizeScrollView ();
			
			forceUpdateContents = true;
		}
		catch (Exception e) {
			// アクセスエラー.
			OpenErrorDialog ("Failed open...");
			Debug.LogException (e);
		}
	}
	
	public void GoParentDirectory () {
		if (string.IsNullOrEmpty(currentDirectory)) {
			currentDirectory = defaultDirectory;
		}
		try {
			var currentInfo = new DirectoryInfo (currentDirectory);
			if (currentInfo.Parent != null) {
				string parent = currentInfo.Parent.FullName;
				if (!string.IsNullOrEmpty (parent)) {
					OpenDirectory (parent);
				}
			}
		}
		catch (Exception e) {
			// 親ディレクトリへのアクセスエラー.
			OpenErrorDialog ("Failed open...");
			Debug.LogException (e);
		}
	}

	public void GoInputDirectory () {
		string goPath = inputPath;
		bool error = false;
		if (string.IsNullOrEmpty (inputPath)) {
			error = true;
		}
		else if (!Directory.Exists (goPath)) {
			if (File.Exists (goPath)) {
				goPath = Path.GetDirectoryName (goPath);
				goPath = goPath.Replace ("\\", "/");
			}
			if (!Directory.Exists (goPath)) {
				error = true;
			}
		}
		if (!error) {
			OpenDirectory (goPath);
		}
		else {
			// 入力されたディレクトリが見つからないよ.
			OpenErrorDialog ("Path not found!");
		}
	}

	public void DecidePath () {
		bool exists;
		string lastOpenedDir;
		if (directoryOnly) {
			exists = Directory.Exists (inputPath);
		}
		else {
			exists = File.Exists (inputPath);
		}
		if (exists) {
			if (!directoryOnly) {
				lastOpenedDir = inputPath;
			}
			else {
				lastOpenedDir = Path.GetDirectoryName (inputPath);
				lastOpenedDir = lastOpenedDir.Replace ("\\", "/");
			}
			if (onDecide != null) {
				onDecide (lastOpenedDir);
			}
			Close ();
		}
		else {
			// 最終決定したファイルが見つからないよ.directoryOnlyであればフォルダが見つからないよ.
			if (!directoryOnly) {
				OpenErrorDialog ("File not found!");
			}
			else {
				OpenErrorDialog ("Directory not found!");
			}
		}
	}

	public void Close () {
		Destroy (mainDialogObject);
	}
	
	public void OpenErrorDialog (string message) {
		errorDialog.SetMessage (message);
		errorDialog.Show ();
	}

	#region IInfiniteScrollSetup
	public bool forceUpdateContents { get; set; }
	public bool setupCompleted { get; set; }
	
	public IEnumerator OnPostSetupItems() {
		// リストの初期化.
		while (contents == null)
			yield return null;
		ResizeScrollView ();
		setupCompleted = true;
	}

	private void ResizeScrollView () {
		var infiniteScroll =GetComponent<InfiniteScroll> ();
		var rectTransform = GetComponent<RectTransform> ();
		var delta = rectTransform.sizeDelta;
		delta.y = (infiniteScroll.ItemScale * (contents.Length)) + (infiniteScroll.AnchordMargin * 2);
		rectTransform.sizeDelta = delta;
	}
	
	public void OnUpdateItem (int index, GameObject obj) {
		if (contents == null || index < 0 || index >= contents.Length) {
			if (obj.activeSelf) {
				obj.SetActive (false);
			}
		}
		else {
			if (!obj.activeSelf) {
				obj.SetActive (true);
			}
			PathSelectDialogNodeController item = obj.GetComponent<PathSelectDialogNodeController> ();
			item.UpdateItem (contents [index]);
		}
	}
	#endregion
}
