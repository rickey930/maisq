﻿public class PathSelectDialogListInformation {
	public int index { get; set; }
	public string path { get; private set; }
	public string name { get; private set; }
	public bool isDirectory { get; private set; }
	public PathSelectDialogListInformation (int index, string path, string name, bool isDirectory) {
		this.index = index;
		this.path = path;
		this.name = name;
		this.isDirectory = isDirectory;
	}
}