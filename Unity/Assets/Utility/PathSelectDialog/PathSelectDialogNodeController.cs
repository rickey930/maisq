using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PathSelectDialogNodeController : MonoBehaviour {
	public PathSelectDialogListInformation info { get; set; }
	public PathSelectDialogListController manager;
	public Image icon;
	public Text pathLabel;
	public Image wall;
	private readonly Color orangeColor = new Color(1.0f, 0.75f, 0.0f, 1.0f);
	public Sprite folderIconSource;
	public Sprite fileIconSource;

	public void UpdateItem (PathSelectDialogListInformation info) {
		this.info = info;
		icon.sprite = info.isDirectory ? folderIconSource : fileIconSource;
		pathLabel.text = info.name;
		if (manager.selectedIndex == info.index) {
			wall.color = orangeColor;
		}
		else {
			wall.color = Color.white;
		}
	}

	public void PathButtonClick () {
		if (!manager.directoryOnly) {
			if (info.isDirectory) {
				manager.OpenDirectory (info.path);
			}
			else {
				manager.inputPath = info.path;
				if (manager.selectedIndex != info.index) {
					manager.selectedIndex = info.index;
					manager.forceUpdateContents = true;
				}
			}
		}
		else {
			// ディレクトリ選択モードのとき、二回タップでディレクトリ移動.
			if (manager.inputPath == info.path) {
				manager.OpenDirectory (info.path);
			}
			else {
				manager.inputPath = info.path;
				if (manager.selectedIndex != info.index) {
					manager.selectedIndex = info.index;
					manager.forceUpdateContents = true;
				}
			}
		}
	}
}
