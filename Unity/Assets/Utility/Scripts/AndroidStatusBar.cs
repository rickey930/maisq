﻿using UnityEngine;
using System.Collections;
#if !UNITY_ANDROID
using System;
using System.Text;
#endif

public class AndroidStatusBar : MonoBehaviour {
	// ステータスバーの表示と非表示の切り替え動作クラス.

	public static AndroidStatusBar instance;

	/// <summary>
	/// 再非表示までの時間.
	/// </summary>
	public float reHideTime = 1.0f;
	private float reHideTimer = 0;
	private bool reHideBoot = false;

	private void Awake () {
		if (instance == null) {
			instance = this;
		}
		else {
			Destroy (this);
		}
	}

	private void Update () {
		if (reHideBoot) {
			reHideTimer += Time.unscaledDeltaTime;
		}

		if (!reHideBoot && Input.touchCount > 0) {
			reHideTimer = 0;
			reHideBoot = true;
		}

		// 一定時間が経過したら再度非表示.
		if (reHideBoot && reHideTimer > reHideTime) {
			Hide ();
			reHideTimer = reHideTime;
			reHideBoot = false;
		}
	}


	/// <summary>
	/// ステータスバーを表示します.
	/// </summary>
	public void Show () {
#if UNITY_ANDROID && !UNITY_EDITOR
		//Debug.Log ("android OS VERSION:: " + androidOsVirsion); 
		if (androidOsVirsion < 19) {
			activity.Call ("runOnUiThread", showRunnable);
		}
		else {
			activity.Call ("runOnUiThread", showRunnable2);
		}
#endif
	}

	/// <summary>
	/// ステータスバーを隠します.
	/// </summary>
	public void Hide () {
#if UNITY_ANDROID && !UNITY_EDITOR
		//Debug.Log ("android OS VERSION:: " + androidOsVirsion); 
		if (androidOsVirsion < 19) {
			activity.Call ("runOnUiThread", hideRunnable);
		}
		else {
			activity.Call ("runOnUiThread", hideRunnable2);
		}
#endif
	}

//	// デバッグ用 GUIボタンでのステータスバー表示切り替え.
//	bool kirikae = false;
//	public void OnGUI () {
//		if (GUI.Button (new Rect(0, 0, 300, 300), "StatusBar")) {
//			if (kirikae) Hide();
//			else Show ();
//			kirikae = !kirikae;
//		}
//	}

// ===========================================================================================
//
//		Android OS 4.3 未満なら.
//		activity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//				// ステータスバー表示.
//				activity.getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
//				// ステータスバー非表示.
//				//activity.getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);
//            }
//        });
//		っていうJavaコードを、Unity-C#から実行したい.
//
// ===========================================================================================

	private AndroidJavaObject _activity;
	private AndroidJavaObject activity {
		get {
			if (_activity == null) {
				AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
				_activity = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
			}
			return _activity;
		}
	}

	private AndroidJavaObject _window;
	private AndroidJavaObject window {
		get {
			if (_window == null) {
				_window = activity.Call<AndroidJavaObject> ("getWindow");
			}
			return _window;
		}
	}

	private int? _androidStatusBarFlag = null;
	private int androidStatusBarFlag {
		get {
			if (!_androidStatusBarFlag.HasValue) {
				AndroidJavaClass layoutParams = new AndroidJavaClass ("android/view/WindowManager$LayoutParams");
				_androidStatusBarFlag = layoutParams.GetStatic<int> ("FLAG_FULLSCREEN");
			}
			return _androidStatusBarFlag.Value;
		}
	}

	private AndroidJavaRunnable _hideRunnable;
	private AndroidJavaRunnable hideRunnable {
		get {
			if (_hideRunnable == null) {
				_hideRunnable = new AndroidJavaRunnable ( delegate(){
					window.Call ("addFlags", androidStatusBarFlag);
				});
			}
			return _hideRunnable;
		}
	}

	private AndroidJavaRunnable _showRunnable;
	private AndroidJavaRunnable showRunnable {
		get {
			if (_showRunnable == null) {
				_showRunnable = new AndroidJavaRunnable ( delegate(){
					window.Call ("clearFlags", androidStatusBarFlag);
				});
			}
			return _showRunnable;
		}
	}



// ===========================================================================================
//
//		Android OS 4.3 以上なら.
//		activity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//				// ステータスバー表示.
//				activity.getCurrentFocus().setSystemUiVisibility(
//		            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//		            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//		            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//		            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
//		            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
//		            | View.SYSTEM_UI_FLAG_IMMERSIVE);
//				// ステータスバー非表示.
//				//activity.getCurrentFocus().setSystemUiVisibility(
//			    //		View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//			    //		| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//			    //		| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//            }
//        });
//		っていうJavaコードを、Unity-C#から実行したい.
//
// ===========================================================================================
	
	private AndroidJavaObject _view;
	private AndroidJavaObject view {
		get {
			if (_view == null) {
				_view = activity.Call<AndroidJavaObject> ("getCurrentFocus");
			}
			return _view;
		}
	}

	private int[] _systemUiViewVisiblityFlags = null;
	private int[] systemUiViewVisiblityFlags {
		get {
			if (_systemUiViewVisiblityFlags == null) {
				AndroidJavaClass viewClass = new AndroidJavaClass ("android/view/View");
				_systemUiViewVisiblityFlags = new int[] { 
					viewClass.GetStatic<int> ("SYSTEM_UI_FLAG_LAYOUT_STABLE"),
					viewClass.GetStatic<int> ("SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION"),
					viewClass.GetStatic<int> ("SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN"),
					viewClass.GetStatic<int> ("SYSTEM_UI_FLAG_HIDE_NAVIGATION"),
					viewClass.GetStatic<int> ("SYSTEM_UI_FLAG_FULLSCREEN"),
					viewClass.GetStatic<int> ("SYSTEM_UI_FLAG_IMMERSIVE"),
				};
			}
			return _systemUiViewVisiblityFlags;
		}
	}
	
#if UNITY_ANDROID && !UNITY_EDITOR
	private int? _androidOsVirsion = null;
#endif
	public int androidOsVirsion {
		get {
#if UNITY_ANDROID && !UNITY_EDITOR
			if (!_androidOsVirsion.HasValue) {
				AndroidJavaClass version = new AndroidJavaClass ("android/os/Build$VERSION");
				_androidOsVirsion = version.GetStatic<int> ("SDK_INT");
			}
			return _androidOsVirsion.Value;
#endif
			return 0;
		}
	}

	private AndroidJavaRunnable _hideRunnable2;
	private AndroidJavaRunnable hideRunnable2 {
		get {
			if (_hideRunnable2 == null) {
				_hideRunnable2 = new AndroidJavaRunnable ( delegate(){
					int[] f = systemUiViewVisiblityFlags;
					view.Call ("setSystemUiVisibility", f[0] | f[1] | f[2] | f[3] | f[4] | f[5]);
				});
			}
			return _hideRunnable2;
		}
	}
	
	private AndroidJavaRunnable _showRunnable2;
	private AndroidJavaRunnable showRunnable2 {
		get {
			if (_showRunnable2 == null) {
				_showRunnable2 = new AndroidJavaRunnable ( delegate(){
					int[] f = systemUiViewVisiblityFlags;
					view.Call ("setSystemUiVisibility", f[0] | f[1] | f[2] );
				});
			}
			return _showRunnable2;
		}
	}
}


#if !UNITY_ANDROID && !UNITY_EDITOR
namespace UnityEngine
{
	public class AndroidJavaObject : IDisposable
	{
		public AndroidJavaObject () {
		}
		public AndroidJavaObject (string className, params object[] args) : this () {
		}
		public ReturnType Call<ReturnType> (string methodName, params object[] args) {
			return default(ReturnType);
		}
		public void Call (string methodName, params object[] args) {
		}
		public ReturnType CallStatic<ReturnType> (string methodName, params object[] args) {
			return default(ReturnType);
		}
		public void CallStatic (string methodName, params object[] args) {
		}
		public void Dispose () {
		}
		public FieldType Get<FieldType> (string fieldName) {
			return default(FieldType);
		}
		public IntPtr GetRawClass () {
			return default(IntPtr);
		}
		public IntPtr GetRawObject () {
			return default(IntPtr);
		}
		public FieldType GetStatic<FieldType> (string fieldName) {
			return default(FieldType);
		}
		public void Set<FieldType> (string fieldName, FieldType val) {
		}
		public void SetStatic<FieldType> (string fieldName, FieldType val) {
		}
	}
	public delegate void AndroidJavaRunnable ();
	public class AndroidJavaClass : AndroidJavaObject {
		public AndroidJavaClass (string className) {
		}
	}
}
#endif