﻿using System;
using System.Collections;
using System.IO;
using IUtility;
using UnityEngine;

namespace Utility {
	public class AudioManager : MonoBehaviour, IAudioManager {
		private AudioSource player;
		public string audioFileName { get; set; }
		public AudioClipMaker clipMaker;
		private WavFileInfo	wavInfo = new WavFileInfo();

		public AudioManager() {
			audioFileName = "";
		}

		public void load(AudioClip clip) {
			if (player != null)
				dispose();
			player = gameObject.GetComponent<AudioSource>();
			player.clip = clip;
		}

		public void load(string fileName) {
			audioFileName = fileName;
			load();
		}

		private void load() {
			if (player != null)
				dispose();
			if (!File.Exists(audioFileName)) return;

			string path = audioFileName; //string.Format("{0}/Sounds/{1}.wav", Application.dataPath, m_WavFileName);
			byte[] buf = File.ReadAllBytes( path );
			
			// analyze wav file
			wavInfo.Analyze(buf);
			
			// create audio clip
			player = gameObject.GetComponent<AudioSource>();
			player.clip = clipMaker.Create(
				"making_clip",
				buf,
				wavInfo.TrueWavBufIdx,
				wavInfo.BitPerSample,
				wavInfo.TrueSamples,
				wavInfo.Channels,
				wavInfo.Frequency,
				false,
				false
				);
			
			Debug.Log("read " + audioFileName);
		}

		public void start(bool loop = false) {
			if (player != null && !player.isPlaying) {
				player.Play();
				player.volume = 1;
				player.loop = loop;
			}
		}

		private bool in_otherFading; // 他がフェードしてる.
		private bool in_newFading; //フェード割り込み.
		public IEnumerator fadeIn(float time) {
			while (in_otherFading) {
				in_newFading = true;
				yield return null;
			}
			in_newFading = false;
			in_otherFading = true;
			float timer = 0;
			if (player != null && !player.isPlaying) {
				player.volume = 0;
				player.Play ();
				yield return null;
			}
			while (timer < time && !in_newFading) {
				float percent = timer / time;
				if (percent < 0) percent = 0;
				if (percent > 1) percent = 1;
				player.volume = percent;
				timer += Time.deltaTime;
				yield return null;
			}
			in_otherFading = false;
		}
		
		private bool out_otherFading; // 他がフェードしてる.
		private bool out_newFading; //フェード割り込み.
		public IEnumerator fadeOut(float delay, float time, UnityEngine.Events.UnityAction onFadeEnd) {
			while (out_otherFading) {
				out_newFading = true;
				yield return null;
			}
			out_newFading = false;
			out_otherFading = true;
			float timer = 0;
			if (player != null && player.isPlaying) {
				while (timer < delay && !out_newFading) {
					timer += Time.deltaTime;
					yield return null;
				}
				timer = 0;
				while (timer < time && !out_newFading) {
					float percent = timer / time;
					if (percent < 0) percent = 0;
					if (percent > 1) percent = 1;
					if (1 - percent < player.volume) player.volume = 1 - percent;
					timer += Time.deltaTime;
					yield return null;
				}
				if (!out_otherFading) pause ();
			}
			if (!out_otherFading) player.volume = 1.0f;
			if (!out_newFading && onFadeEnd != null) onFadeEnd();
			out_otherFading = false;
		}

		public void pause() {
			if (player != null && player.isPlaying)
				player.Pause();
		}

		public void seek(long time) {
			if (player != null)
				player.time = (float)time / 1000.0f;
		}

		public void setVolume(float volume = 1.0f) {
			if (player != null)
				player.volume = volume;
		}

		public long getDuration() {
			if (player != null && player.clip != null)
				return (player.clip.length * 1000.0f).ToLong();
			return 0L;
		}

		public long getTime() {
			if (player != null)
				return (player.time * 1000.0f).ToLong();
			return 0L;
		}

		public void dispose() {
			if (player != null) {
				player.Stop();
				player.clip = null;
				//player.Dispose();
				player = null;
			}
		}

		public bool useAudio() {
			return player != null && player.clip != null;
		}

		public bool isPlaying() {
			if (player != null)
				return player.isPlaying;
			return false;
		}

	}
}