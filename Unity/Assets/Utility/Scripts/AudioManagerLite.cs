﻿using UnityEngine;
using System.Collections;

public class AudioManagerLite : MonoBehaviour {
	private static AudioManagerLite instance;
	private Utility.AudioManager _parent;
	private Utility.AudioManager parent {
		get {
			if (_parent == null) {
				_parent = GetComponent<Utility.AudioManager>();
			}
			return _parent;
		}
	}
	private void Awake() {
		if (instance == null) {
			instance = this;
		}
		else {
			Destroy(this);
		}
	}
	public static Utility.AudioManager GetManager() {
		return instance.parent;
	}

	public static void Load(AudioClip clip) {
		if (instance != null && instance.parent != null) {
			instance.parent.load(clip);
		}
	}
	public static void Play(bool loop = false) {
		if (instance != null && instance.parent != null) {
			instance.parent.start(loop);
		}
	}
	public static void Pause() {
		if (instance != null && instance.parent != null) {
			instance.parent.pause();
		}
	}
	public static void Seek(long time) {
		if (instance != null && instance.parent != null) {
			instance.parent.seek(time);
		}
	}
	public static long GetDuration() {
		if (instance != null && instance.parent != null) {
			return instance.parent.getDuration();
		}
		return default(long);
	}
	public static long GetTime() {
		if (instance != null && instance.parent != null) {
			return instance.parent.getTime();
		}
		return default(long);
	}
	public static bool UsedAudio() {
		if (instance != null && instance.parent != null) {
			return instance.parent.useAudio();
		}
		return default(bool);
	}
	public static bool IsPlaying() {
		if (instance != null && instance.parent != null) {
			return instance.parent.isPlaying();
		}
		return default(bool);
	}
	public static void SetVolume(float volume = 1.0f) {
		if (instance != null && instance.parent != null) {
			instance.parent.setVolume(volume);
		}
	}
	public static void FadeIn(float time) {
		if (instance != null && instance.parent != null) {
			instance.StartCoroutine(instance.parent.fadeIn(time));
		}
	}
	public static void FadeOut(float delay, float time, UnityEngine.Events.UnityAction onFadeEnd) {
		if (instance != null && instance.parent != null) {
			instance.StartCoroutine(instance.parent.fadeOut(delay, time, onFadeEnd));
		}
	}

}
