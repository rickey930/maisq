﻿using UnityEngine;
using System.Collections;

public class FadeController : MonoBehaviour {

	public float fadeTime;
	private float fadeTimer;
	private UnityEngine.UI.Image fadeObj;
	private bool delegateProcessed;
	private UnityEngine.Events.UnityAction action;
	private bool downFade;

	// Use this for initialization
	void Start () {
		fadeObj = gameObject.GetComponent<UnityEngine.UI.Image>();
		fadeTimer = 0;
		delegateProcessed = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (fadeTimer <= fadeTime) {
			float percent = fadeTimer / fadeTime;
			if (percent < 0) percent = 0;
			if (percent > 1) percent = 1;
			Color col = fadeObj.color;
			col.a = downFade ? 1.0f - percent : percent;
			fadeObj.color = col;
			fadeTimer += Time.deltaTime;
		}
		else if (!delegateProcessed) {
			delegateProcessed = true;
			if (action != null) {
				action();
			}
			if (downFade) gameObject.SetActive(false);
		}
	}

	public void Activate(UnityEngine.Events.UnityAction action = null) {
		this.action = action;
		gameObject.SetActive(true);
		downFade = false;
		fadeTimer = 0;
		delegateProcessed = false;
	}

	public void Deactivate() {
		this.action = null;
		downFade = true;
		fadeTimer = 0;
		delegateProcessed = false;
	}

}
