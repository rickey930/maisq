﻿using UnityEngine;
using UnityEngine.UI;

public class IgnoreTouch : Button, ICanvasRaycastFilter {

	[HideInInspector]
	public int editorPopupSelectedIndex;
	[HideInInspector][SerializeField]
	public bool ignoreOutCircle;
	[HideInInspector][SerializeField]
	public bool ignoreInCircle;
	[HideInInspector][SerializeField]
	public float radius;

	GameObject _canvasObj;
	Canvas _canvasComp;
	CanvasScaler _canvasScalerComp;
	GameObject canvasObj {
		get {
			if (_canvasObj == null) {
				_canvasObj = GameObject.Find ("Canvas");
			}
			return _canvasObj;
		}
	}
	Canvas canvas {
		get {
			if (canvasObj != null && _canvasComp == null) {
				_canvasComp = canvasObj.GetComponent<Canvas> ();
			}
			return _canvasComp;
		}
	}
	CanvasScaler canvasScaler {
		get {
			if (canvasObj != null && _canvasScalerComp == null) {
				_canvasScalerComp = canvasObj.GetComponent<CanvasScaler> ();
			}
			return _canvasScalerComp;
		}
	}

	public bool IsRaycastLocationValid (Vector2 sp, Camera eventCamera)
	{
		float r = radius / canvasScaler.referenceResolution.x * canvas.pixelRect.width;
		if (ignoreOutCircle) {
			return Vector2.Distance(sp, transform.position) > r;
		}
		if (ignoreInCircle) {
			return Vector2.Distance(sp, transform.position) < r;
		}
		return false;
	}
}

