﻿namespace IUtility {
	public interface IAudioManager {
		void start(bool loop = false);
		void pause();
		void seek(long time);
		long getDuration();
		long getTime();
		bool useAudio();
		bool isPlaying();
	}
}
