﻿namespace IUtility {
	public interface ISoundEffectManager {
		void Play(int id);
	}
}
