﻿namespace IUtility {
	public interface IStopwatch {
		void start();
		void pause();
		void stop();
		void reset();
		void restart();
		long now();
		bool isRunning();
	}
}
