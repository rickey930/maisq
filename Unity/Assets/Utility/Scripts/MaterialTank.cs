﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SerializeDictionary;

[System.Serializable]
public class StringMaterialDictionary : TableBase<string, Material, StringMaterialPair> { }
[System.Serializable]
public class StringMaterialPair : KeyAndValue<string, Material> { 
	public StringMaterialPair (string key, Material value) : base (key, value) { }
}

public class MaterialTank : SingletonMonoBehaviour<MaterialTank> {
	public StringMaterialDictionary materials;
	public static Material Get(string key) {
		if (instance != null) {
			if (instance.materials.GetTable().ContainsKey(key)) {
				return instance.materials.GetTable()[key];
			}
			else {
				Debug.LogError(key + " is not found in Sprite tank.");
			}
		}
		return null;
	}
}
