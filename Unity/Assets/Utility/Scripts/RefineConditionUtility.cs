﻿using System;
using System.Collections.Generic;

public class RefineConditionUtility<TFindSource> {
	private Func<TFindSource, object, bool> func;
	private object value;
	public RefineConditionUtility (Func<TFindSource, object, bool> func, object value) {
		this.func = (info, v)=>func(info, v);
		this.value = value;
	}
	private bool Check (TFindSource info) {
		return func (info, value);
	}

	public static Predicate<TFindSource> GetMultiConditionsPredicate (RefineConditionUtility<TFindSource>[] conditions) {
		return (data) => {
			for (int i = 0; i < conditions.Length; i++) {
				if (!conditions[i].Check (data)) {
					return false;
				}
			}
			return true;
		};
	}
}
