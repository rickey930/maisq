﻿using UnityEngine;
using System.Collections;

public class SingletonMonoBehaviour<T> : MonoBehaviour where T : SingletonMonoBehaviour<T> {
	protected static T _instance;
	public static T instance {
		get {
			if (_instance == null) {
				_instance = (T)FindObjectOfType (typeof(T));
			}
			return _instance;
		}
	}
	
	protected virtual void Awake() {
		if (_instance == null) {
			_instance = (T)this;
		}
		else if (instance != this) {
			Destroy(this);
		}
	}

	public static void DestoryInstance () {
		if (_instance != null) {
			_instance = null;
		}
	}
}
