﻿using System;
using System.Collections.Generic;

public class SortConditionUtility<TFindSource> {
	private Func<TFindSource, TFindSource, int> func;
	public SortConditionUtility (Func<TFindSource, TFindSource, int> func) {
		this.func = (info1, info2)=>func(info1, info2);
	}
	private int Check (TFindSource info1, TFindSource info2) {
		return func (info1, info2);
	}
	
	public static Comparison<TFindSource> GetMultiConditionsComparison (SortConditionUtility<TFindSource>[] conditions) {
		return (data1, data2) => {
			int result;
			for (int i = 0; i < conditions.Length; i++) {
				if ((result = conditions[i].Check (data1, data2)) != 0) {
					return result;
				}
			}
			return 0;
		};
	}
}
