﻿using UnityEngine;
using System.Collections;
using IUtility;
using SerializeDictionary;

[System.Serializable]
public class StringAudioClipDictionary : TableBase<string, AudioClip, StringAudioClipPair> { }
[System.Serializable]
public class StringAudioClipPair : KeyAndValue<string, AudioClip> { 
	public StringAudioClipPair (string key, AudioClip value) : base (key, value) { }
}

namespace Utility {
	public class SoundEffectManager : MonoBehaviour, ISoundEffectManager {
		private AudioSource player;
		public StringAudioClipDictionary clips;
		private static SoundEffectManager instance;

		public static void Play(string key) {
			if (instance != null) {
				if (Contains(key)) {
					instance.player.PlayOneShot(GetClip(key));
				}
				else {
					Debug.LogError(key + " is not found in Sound Effect Clips.");
				}
			}
		}

		public static AudioClip GetClip (string key) {
			return instance.clips.GetTable () [key];
		}

		public static bool Contains (string key) {
			return instance.clips.GetTable ().ContainsKey (key);
		}
		
		private void Awake () {
			if (instance == null) {
				instance = this;
				player = GetComponent<AudioSource>();
			}
			else {
				Destroy(this);
			}
		}
		
		public void Play(int index) {
			player.PlayOneShot(instance.clips.GetList()[index].Value);
		}
	}
}