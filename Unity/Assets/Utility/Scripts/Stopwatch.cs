﻿using IUtility;

namespace Utility {
	public class Stopwatch : IStopwatch {
		System.Diagnostics.Stopwatch sw;
		public Stopwatch() {
			sw = new System.Diagnostics.Stopwatch();
		}

		public void start() {
			if (!sw.IsRunning)
				sw.Start();
		}

		public void pause() {
			if (sw.IsRunning)
				sw.Stop();
		}

		public void reset() {
			sw.Reset();
		}

		public void stop() {
			if (sw.IsRunning) {
				sw.Stop();
				sw.Reset();
			}
		}

		public void restart() {
			sw.Stop();
			sw.Reset();
			sw.Start();
		}

		public long now() {
			return sw.ElapsedMilliseconds;
		}

		public bool isRunning() {
			return sw.IsRunning;
		}
	}
}