using UnityEngine;
using System.Collections;

public class TimeAnimation : MonoBehaviour {
	[System.Serializable]
	public class TimeAnimationData {
		public float time;
		public Vector3Control translate;
		public Vector3Control rotate;
		public Vector3Control scale;
		public FloatControl alpha;

		public class ValueControl<T> {
			public T start;
			public T target;
			public PercentUnlimitedFlag unlimited;
		}
		
		[System.Serializable]
		public class Vector3Control : ValueControl<Vector3> {
		}

		[System.Serializable]
		public class FloatControl : ValueControl<float>{
		}
		
		[System.Serializable]
		public class PercentUnlimitedFlag {
			public bool under;
			public bool over;
		}

		public void GetValues(float percent, out Vector3 translateValue, out Vector3 rotateValue, out Vector3 scaleValue, out float alphaValue) {
			var ctrls1 = new Vector3Control[] {
				translate, rotate, scale,
			};
			var results1 = new Vector3[ctrls1.Length];

			for (int i = 0; i < ctrls1.Length; i++) {
				var ctrl = ctrls1[i];
				float rate = percent;
				if (!ctrl.unlimited.under && rate < 0) rate = 0;
				if (!ctrl.unlimited.over && rate > 1) rate = 1;
				var range = ctrl.target - ctrl.start;
				Vector3 value = ctrl.start + range * percent;
				results1[i] = value;
			}
			translateValue = results1[0];
			rotateValue = results1[1];
			scaleValue = results1[2];


			var ctrls2 = new FloatControl[] {
				alpha,
			};
			var results2 = new float[ctrls2.Length];
			
			for (int i = 0; i < ctrls2.Length; i++) {
				var ctrl = ctrls2[i];
				float rate = percent;
				if (!ctrl.unlimited.under && rate < 0) rate = 0;
				if (!ctrl.unlimited.over && rate > 1) rate = 1;
				var range = ctrl.target - ctrl.start;
				float value = ctrl.start + range * percent;
				results2[i] = value;
			}
			alphaValue = results2[0];
		}
	}

	public enum ClearanceType {
		NOTHING, DESTROY, DEACTIVE, 
	}

	///<summary>あと何回ループするか. 負の数で無限にループ</summary>
	[SerializeField]
	private int loop = -1;
//	///<summary>初期値は前回の目標値を受け継ぎ、連続アニメーションにする</summary>
//	[SerializeField]
//	private bool continuity;
	///<summary>ループ終了時に毎回実行されるイベント</summary>
	public System.Action<GameObject> loopEndEvent { protected get; set; }
	///<summary>アニメーションが終わると実行されるイベント</summary>
	public System.Action<GameObject> animationEndEvent  { protected get; set; }
	[SerializeField]
	private ClearanceType clearanceType;
	[SerializeField]
	public Transform clearanceRoot;
	[SerializeField]
	private bool translateInvalid;
	[SerializeField]
	private bool rotateInvalid;
	[SerializeField]
	private bool scaleInvalid;
	[SerializeField]
	private bool alphaInvalid;

	[SerializeField]
	private TimeAnimationData[] data;
	public TimeAnimationData[] GetData { get { return data; } }
	private float timer;
	private int index;
	private SpriteRenderer spRenderer;

	private void Start () {
		spRenderer = GetComponent<SpriteRenderer> ();
		Update ();
//		if (continuity) {
//			data[0].translate.start = transform.localPosition;
//			data[0].rotate.start = transform.localRotation.eulerAngles;
//			data[0].scale.start = transform.localScale;
//			for (int i = 1; i < data.Length; i++) {
//				data[i].translate.start = data[i - 1].translate.start + data[i - 1].translate.target;
//				data[i].rotate.start = data[i - 1].rotate.start + data[i - 1].rotate.target;
//				data[i].scale.start = data[i - 1].scale.start + data[i - 1].scale.target;
//			}
//		}
	}
	
	private void Update () {
		if (index >= data.Length) {
			bool end = false;
			if (loop > 0 || loop < 0) {
				if (loopEndEvent != null) loopEndEvent(gameObject);
				if (loop > 0) loop--;
				if (loop == 0) {
					end = true;
				}
				else {
					index = 0;
				}
			}
			else {
				end = true;
			}
			if (end) {
				if (animationEndEvent != null) animationEndEvent(gameObject);
				Clearance();
				return;
			}
		}
		
		var info = data[index];

		if (info.time != 0) {
			float percent = timer / info.time;
			var values1 = new Vector3[3];
			var values2 = new float[1];
			info.GetValues (percent, out values1 [0], out values1 [1], out values1 [2], out values2[0]);
			if (!translateInvalid) {
				transform.localPosition = values1 [0];
			}
			if (!rotateInvalid) {
				transform.localRotation = Quaternion.Euler (Constants.instance.ToLeftHandedCoordinateSystemRotation(values1 [1]));
			}
			if (!scaleInvalid) {
				transform.localScale = values1 [2];
			}
			if (!alphaInvalid && spRenderer != null) {
				Color temp = spRenderer.color;
				temp.a = values2[0];
				spRenderer.color = temp;
			}
		}
		else {
			Debug.LogError(clearanceRoot.name + " of TimeAnimationData[" + index.ToString() + "].time is 0");
		}
		timer += Time.deltaTime;
		if (timer >= info.time) {
			timer = 0;
			index++;
		}
	}

	private void Clearance() {
		switch (clearanceType) {
		case ClearanceType.DESTROY:
			if (clearanceRoot == null) {
				clearanceRoot = transform;
			}
			Destroy(clearanceRoot.gameObject);
			break;
		case ClearanceType.DEACTIVE:
			if (clearanceRoot == null) {
				clearanceRoot = transform;
			}
			clearanceRoot.gameObject.SetActive(false);
			break;
		}
	}
}