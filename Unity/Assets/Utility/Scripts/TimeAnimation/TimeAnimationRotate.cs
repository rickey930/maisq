﻿using UnityEngine;
using System.Collections;

public class TimeAnimationRotate : TimeAnimationBase<Vector3, Vector3AnimationData> {
	protected override void ReferenceValue (Vector3 value) {
		transform.localRotation = Quaternion.Euler(Constants.instance.ToLeftHandedCoordinateSystemRotation(value));
	}
}