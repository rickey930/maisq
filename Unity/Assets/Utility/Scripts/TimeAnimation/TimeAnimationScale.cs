﻿using UnityEngine;
using System.Collections;

public class TimeAnimationScale : TimeAnimationBase<Vector3, Vector3AnimationData> {
	protected override void ReferenceValue (Vector3 value) {
		transform.localScale = value;
	}
}