﻿using UnityEngine;
using System.Collections;

public class TimeAnimationTranslate : TimeAnimationBase<Vector3, Vector3AnimationData> {
	protected override void ReferenceValue (Vector3 value) {
		transform.localPosition = value;
	}
}

[System.Serializable]
public class Vector3AnimationData : TimeAnimationData<Vector3> {
	protected override Vector3 TargetMinusStart (Vector3 start, Vector3 target) {
		return target - start;
	}
	protected override Vector3 StartPlusRangeMultiplyPercent (Vector3 start, Vector3 range, float percent) {
		return start + range * percent;
	}
}