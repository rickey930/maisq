﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UICanvasAutoScaler : MonoBehaviour {

	// Use this for initialization
	IEnumerator Start () {
		while (TwoDimensionCamera.screenType == TwoDimensionCamera.RealScreenVectorType.NONE) {
			yield return null;
		}
		CanvasScaler scaler = GetComponent<CanvasScaler> ();
		if (TwoDimensionCamera.screenType == TwoDimensionCamera.RealScreenVectorType.PORTRAIT) {
			scaler.matchWidthOrHeight = 0.0f;
		}
		else {
			scaler.matchWidthOrHeight = 1.0f;
		}
	}

}
