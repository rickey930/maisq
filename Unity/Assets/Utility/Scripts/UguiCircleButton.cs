﻿using UnityEngine;
using UnityEngine.UI;

public class UguiCircleButton : Button, ICanvasRaycastFilter {
	GameObject _canvasObj;
	Canvas _canvasComp;
	CanvasScaler _canvasScalerComp;
	RectTransform _rectTransform;
	GameObject canvasObj {
		get {
			if (_canvasObj == null) {
				_canvasObj = GameObject.Find ("Canvas");
			}
			return _canvasObj;
		}
	}
	Canvas canvas {
		get {
			if (canvasObj != null && _canvasComp == null) {
				_canvasComp = canvasObj.GetComponent<Canvas> ();
			}
			return _canvasComp;
		}
	}
	CanvasScaler canvasScaler {
		get {
			if (canvasObj != null && _canvasScalerComp == null) {
				_canvasScalerComp = canvasObj.GetComponent<CanvasScaler> ();
			}
			return _canvasScalerComp;
		}
	}
	RectTransform rectTransform {
		get {
			if (_rectTransform == null) {
				_rectTransform = transform as RectTransform;
			}
			return _rectTransform;
		}
	}

	public bool IsRaycastLocationValid (Vector2 sp, Camera eventCamera) {
		float radius = rectTransform.sizeDelta.x / canvasScaler.referenceResolution.x * canvas.pixelRect.width;
		return Vector2.Distance (sp, transform.position) < radius;
	}
}